<?php

error_reporting(E_ALL ^ E_STRICT);

@include_once('Krumo/class.krumo.php');
if (!class_exists('Krumo')) {
	function krumo($vars='') {
		print_r($vars);
	}
}

function k($vars='') {
	krumo($vars);
}

// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$config=dirname(__FILE__).'/protected/backend/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
require_once(dirname(__FILE__).'/protected/components/WebApplication.php');
// Yii::createWebApplication($config)->run();
$app = new WebApplication($config);
// die(var_dump($app));
$app->run();