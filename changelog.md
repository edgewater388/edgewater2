Change Log
==========================
v2.4.0.0
----------
* Bug: Wrong spaces(text are cut) on tablet view. @rk 
* Task #7702755: Reduce size of sign up and trial pages. @rk
* Task #7653797: Timer on home-page @dr
* Task #7646021: Signed in page. CSS animation for loading additional videos on clicking "Show All" button. @rk
* Task #7642920: Markup for sign up page. @rk
* Task #7630783: Hiding sections in workout-regimens starting from the 7th. Popup and button styles in account settings. @rk
* Task #7604837: Change the filtering logic, of video @dr
* Task #7594311: Videos page (new logged in home) @yv
* Task #7594304: Make a second version of the sign up page that gives people a free trial without entering their credit card number @yv
* Task #7594302: Create the option that if someone wants to cancel after 6 months then they get a popup that offers them a lower price if they stay @yv
* Task #7594300: Add a sign up with google and facebook to the sign up page @yv
* Task #7564501: Video's page, video popup, the 2d version of sign up page. @rk
* Task #7688568: Plan-info block (register page) displaying on screens with small height. @rk
* Bug: social sign in doesn't work correctly @yv
* Bug: counter/reset queries on each page @dr
* Bug: missing video filter of type @yv
* Task #7712324: Create settings in the admin panel for the trial sign up page  @yv
* Task #7730521: Extend JWPlayer import videos to 1000  @yv
* Bug: videos on production disappeared @yv @rk
* Bug: add to favorite button does not work on the new logged-in home on production @ob
* Task #7891843: Markup of video popup. @rk
* Task #8043521: Style label for new video. @ib
* Task #8059780: Style social links in popups. @ib
* Task #8065381: Fix gradient and text color for video popup. @ib
* Task #8075208: Style responsive image for Facebook page and fix responsive for SignUp. @ib
* Task #8036682: Youtube api . @vb
* Task #8036791: Ability to select new video like new . @vb
* Task #8037111: Social sharring (twitter, facebook) . @vb
* Task #8037108: Popup's for end of trial, first user video congrat's, start workout program  . @vb
* Task #8087012: Customer Feedback. Separately new videos to new videos/mashup section . @vb
* Task #8087514: Product's images preview for admin panel and changing crop size  . @vb
* Task #8171009: Trial users get charged as soon as they enter their card . @vb
* Task #8185459: Popup for trial should only show for peoples which do not have a credit card on file. @vb
* Task #8304912: Special offer plans (promo pages). Admin management. @vb
* Task #8162093: REST API for users
* Task #8214403: Adding userId to google analytic . @vb
