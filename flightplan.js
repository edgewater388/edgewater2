var plan = require('flightplan');

devConfig = {
    host: 'physique57.com',
    username: 'physique-vod-dev',
    agent: process.env.SSH_AUTH_SOCK,
    webroot: 'www',
    gitRepo: 'git@bitbucket.org:climber-interactive/p57-vod.git',
    branch: 'stage'
}

prodConfig = {
    host: 'p57vod.stage.realpie.com',
    username: 'physique-vod',
    agent: process.env.SSH_AUTH_SOCK,
    webroot: 'www',
    gitRepo: 'git@bitbucket.org:climber-interactive/p57-vod.git',
    branch: 'master'
}
plan.target('dev', devConfig);
plan.target('prod', prodConfig);

plan.local(function (local) {
    if (plan.runtime.target === 'prod') {
        var input = local.prompt('Ready for deploying to production? [yes]');
        if (input.indexOf('yes') === -1) {
            plan.abort('User canceled flight');
        }
    }
    //local.exec('git pull origin master')
    //local.failsafe();
    //local.exec('git commit -am "deploy"')
    //local.exec('git push origin master')
});

plan.remote(function (remote) {
    remote.with("cd " + plan.runtime.hosts[0].webroot, function () {
        //remote.exec("git reset --hard");
        remote.exec("git pull origin " + plan.runtime.hosts[0].branch);
    })
});