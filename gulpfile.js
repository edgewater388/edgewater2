var gulp = require('gulp');
var $ = require('gulp-load-plugins')({pattern: '*'});
var path = require('path');
var minifyHTML = require('gulp-minify-html');
var concat = require('gulp-concat');
var amdOptimize = require('amd-optimize');
//var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var notify = require('gulp-notify');
var rename = require('gulp-rename');
var minifycss = require('gulp-minify-css');
var browserSync = require('browser-sync');

var SRC = 'themes/basic';
var DEST = 'www';

gulp.task('createVendorCSS', function () {
    var scss = $.mainBowerFiles({filter: "**/*.scss"});
    var importStr = '';
    $.underscore.each(scss, function (val, key) {
        console.log(val.split('bower_components')[1]);
        importStr += '@import "../../../bower_components' + val.split('bower_components')[1] + '";\n';
    });
    return gulp.src([])
        .pipe($.file('_vendor.scss', ''))
        .pipe($.insert.prepend(importStr))
        .pipe(gulp.dest(SRC + '/css/app'))
});

gulp.task('compileVendorJS', function () {
    return gulp.src($.mainBowerFiles({filter: "**/*.js"}))
        .pipe($.filelog())
        .pipe($.concat('vendor.js'))
        .pipe($.uglify({preserveComments:'some'}))
        .pipe(gulp.dest(SRC + '/js/vendor'))
});

gulp.task('minifyGlobalJS', function () {
    return gulp.src([SRC + '/js/global.js'])
        .pipe($.uglify({preserveComments: 'some'}))
        .pipe(gulp.dest(SRC + '/js'))
});

gulp.task('minifyCSS', function () {
    return gulp.src(SRC + '/css/main.css')
        .pipe($.minifyCss({keepSpecialComments: 0}))
        .pipe(gulp.dest(SRC + '/css'))
});

gulp.task('bs', function() {
    browserSync.init(["themes/basic/css/**/*.css", 'protected/views/**/*.php', 'themes/basic/js/*.js'], {
        proxy: "vod.app:8000"
    });
});

