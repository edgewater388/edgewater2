<?php

function k($vars, $terminate=true)
{
	echo '<pre>';
	print_r($vars);
	echo '</pre>';

	if ($terminate)
		exit;
}

error_reporting(E_ALL ^ E_STRICT);
ini_set('session.cookie_lifetime', 3600*24);
date_default_timezone_set('America/New_York');

// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
require_once(dirname(__FILE__).'/protected/components/WebApplication.php');
// Yii::createWebApplication($config)->run();
$app = new WebApplication($config);
$app->run();