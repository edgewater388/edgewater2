<?php

class AdminUser extends WebUser {
	
	protected function beforeLogin($id,$states,$fromCookie)
	{
		// Check if the user can access the backend
		if (!Yii::app()->authManager->checkAccess('accessBackend', $id))
			throw new CHttpException(403, 'Access denied');

		return true;
	}

}
