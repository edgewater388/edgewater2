<?php

class BaseController extends Controller
{
	public $urlParams = array();
	public $count = 0;

	public function getClass()
	{
		return ucfirst($this->getId());
	}

	public function init()
	{
		parent::init();
	}

	public function getViewFile($viewName)
	{
		if (file_exists($viewFile = self::getViewPath($viewName).'/'.$viewName.'.php'))
			return $viewFile;
		return parent::getViewFile($viewName);
	}

	public function getViewPath($viewName = null)
	{
		// Default to the action id when no view name is given
		if (!$viewName)
			$viewName = $this->action->id;
		
		$viewPath = parent::getViewPath();
		$templatePath = $viewPath.DIRECTORY_SEPARATOR.$viewName.'.php';

		// check to make sure the view path exists, if not, use the base view path
		$basePath = lcfirst(str_ireplace('Controller', '', get_class()));

		if (!file_exists($templatePath))
			$viewPath = dirname($viewPath).DIRECTORY_SEPARATOR.$basePath;

		return $viewPath;


		// $viewPath = parent::getViewPath();

		// if ($viewName != 'update')
		// {
		// 	var_dump($viewPath);
		// 	die(var_dump($viewName));
		// }

		// // If the default template is using the base template, see if there is a custom post
		// // template available instead
		// if (basename($viewPath) == 'base')
		// {
		// 	$basePath = lcfirst(str_ireplace('Controller', '', get_class()));
		// 	$templatePath = dirname($viewPath).DIRECTORY_SEPARATOR.$basePath.DIRECTORY_SEPARATOR.$viewName.'.php';

		// 	if (file_exists($templatePath))
		// 		$viewPath = dirname($viewPath).DIRECTORY_SEPARATOR.$basePath;
		// }

		// return $viewPath;
	}

	// public function getViewPath()
	// {

	// 	$viewPath = parent::getViewPath();
	// 	$templatePath = $viewPath.DIRECTORY_SEPARATOR.$this->action->id.'.php';

	// 	// check to make sure the view path exists, if not, use the base view path
	// 	$basePath = lcfirst(str_ireplace('Controller', '', get_class()));

	// 	if (!file_exists($templatePath))
	// 		$viewPath = dirname($viewPath).DIRECTORY_SEPARATOR.$basePath;

	// 	var_dump($this->action->id);
	// 	if ($this->count > 0)
	// 		die(var_dump($viewPath));

	// 	$this->count++;
	// 	return $viewPath;
	// }

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('*'),
			// 	'roles'=>array('admin'),
			// ),
			// array('deny',  // deny all users
			// 	'users'=>array('*'),
			// ),
		);
	}

	public function beforeAction($action)
	{
		// $model = new $this->class;
		$model = $this->getModel();
		$this->pageTitle = $model->classLabel.'s';
		$this->breadcrumbs = $this->getDefaultBreadcrumbs($model);
		$this->menu = $this->getDefaultMenu($model);

		return parent::beforeAction($action);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$id = Yii::app()->request->getParam($this->primaryKeyName);

		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new $this->class;
		
		if ($model->columnExists('status'))
			$model->status = 1;

		// check if there are any query params set for default values
		foreach ($model->attributes as $column=>$defaultValue)
		{
			if ($value = Yii::app()->request->getParam($column))
				$model->$column = $value;
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST[$this->class]))
		{
			$model->attributes=$_POST[$this->class];
			if($model->save())
				$this->redirect(array('view',$this->primaryKeyName=>$model->primaryKey));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST[$this->class]))
		{
			$model->attributes=$_POST[$this->class];

			if($model->save())
				$this->redirect(array('view',$this->primaryKeyName=>$model->primaryKey));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		if ($model->delete())
			Flash::setFlash('success', '<strong>'.$model->displayTitle.'</strong> has been deleted');
		else
			Flash::setFlash('error', '<strong>'.$model->displayTitle.'</strong> could not be deleted');

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	public function actionApi($q = null)
	{
		$model=new $this->class;
		$model->name = $q;
		// $model->search();
		$this->renderData(array(
			'dataProvider'=>$model->search(),
			'columns'=>array(
				'id',
				'name',
			),
		));
	}

	public function actionAutocomplete($q = null)
	{
		$model=new $this->class;
		$model->q = $q;
		$dataProvider = $model->search();

		// die(var_dump($q));

		$rows = array();
		foreach ($dataProvider->getData() as $k)
		{
			// var_dump($k);
			$rows[] = array(
				'id'=>$k->primaryKey,
				'name'=>$k->displayTitle,
			);
		}

		$this->renderData($rows);
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new $this->class;
		$model->scenario = 'search';
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET[$this->class]))
			$model->attributes=$_GET[$this->class];

		// if(isset($_GET[$this->class]))
		// 	$model->post->attributes=$_GET[$this->class];

		$model->attributes = $_GET;

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return {ClassName} the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$className = $this->class;
		$model = $className::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function getModel()
	{
		$className = $this->class;
		if ($model = $className::model()->findByPk(Yii::app()->request->getParam($this->primaryKeyName)))
			return $model;
		return new $className;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Blog $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='base-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function getDefaultBreadcrumbs($model = null)
	{
		if (!$model)
			$model = new $this->class;

		$breadcrumb = $this->breadcrumbs;
		$breadcrumb += array($model->classLabel.'s' => array('index')+$this->urlParams);

		switch ($this->action->id) {
			case 'create':
				$breadcrumb += array('New '. $model->classLabel);
				break;
			case 'update':
				$breadcrumb += array(CString::substr($model->displayTitle, 30) => array('view', 'id'=>$model->primaryKey)+$this->urlParams);
				$breadcrumb += array('Edit');
				break;
			case 'view':
				$breadcrumb += array(CString::substr($model->displayTitle, 30) => array('view', 'id'=>$model->primaryKey)+$this->urlParams);
				break;
				// $model->primaryKey
				// ($this->action->Id == 'view')
				break;
			default:
				$breadcrumb += array(CString::substr($model->displayTitle, 30) => array('view', 'id'=>$model->primaryKey)+$this->urlParams);
				$breadcrumb += array($model->getAttributeLabel($this->action->id));
				break;
		} 

		return $breadcrumb;
	}

	public function getDefaultMenu($model = null)
	{
		if (!$model)
			$model = new $this->class;

		$menu = $this->menu;

		if (!$model->isNewRecord)
		{
			$menu['view'] = array('label'=>'<span class="fa fa-eye"></span>', 'url'=>array('view', 'id'=>$model->primaryKey)+$this->urlParams, 'linkOptions'=>array('data-toggle'=>'tooltip', 'title'=>'View'), 'active'=>$this->action->id == 'view');
			$menu['update'] = array('label'=>'<span class="fa fa-pencil"></span>', 'url'=>array('update', 'id'=>$model->primaryKey)+$this->urlParams, 'linkOptions'=>array('data-toggle'=>'tooltip', 'title'=>'Edit'), 'active'=>$this->action->id == 'update');
			$menu['delete'] = array('label'=>'<span class="fa fa-close text-red"></span>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->primaryKey),'confirm'=>'Are you sure you want to delete this item?', 'data-toggle'=>'tooltip', 'title'=>'Delete'));
		}

		if (!empty($menu))
			$menu['separator_0'] = array('label'=>'<span class="separator"></span>', 'url'=>null);

		// $menu[] = array('label'=>'<span class="fa fa-list"></span>', 'url'=>array('index'), 'active'=>$this->action->id == 'index');
		$menu['create'] = array('label'=>'New '.$model->classLabel, 'url'=>array('create')+$this->urlParams, 'active'=>$this->action->id == 'create');

		return $menu;
	}

	public function renderData($data, $terminate=true)
	{
		$data = DataHandler::render($data);
		
		header('Content-Type: application/json');
		echo CJSON::encode($data);

		if ($terminate)
			exit;
	}

	public function getPrimaryKeyName()
	{
		$class = new $this->class;
		return $class->getTableSchema()->primaryKey;
	}
}
