<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */

class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/admin';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	public $toolbarMenu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public $css = array();
	public $js = array();

	public $classes = array();
	
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function getToolbarMenu()
	{
		$items = $this->toolbarMenu;
		if ($this->clips['filters'])
		{
			// Add a reset button if the filter is 'active'
			if (!empty($_GET))
			{
				$item = array(
					'label'=>'Reset', 
					'url'=>'/'.Yii::app()->request->pathInfo,
				);
				array_unshift($items, $item);
			}

			$item = array(
				'label'=>'<span class="glyphicon glyphicon-filter"></span> Filters', 
				'url'=>'#',
				'linkOptions'=>array(
					'data-toggle'=>'filters',
				),
			);
			array_unshift($items, $item);
		}

		return $items;
	}

	/**
	 * Deletes a particular model.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		if ($model->delete())
			Flash::setFlash('success', $model->getAttributeLabel($model->tableName()).' #'.$id.' has been deleted');

		$this->redirect(array('index'));
	}
}