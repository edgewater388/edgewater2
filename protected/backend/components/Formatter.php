<?php

class Formatter extends CFormatter
{
	public function formatStatus($value)
	{
		return $value ? 'Active' : 'Inactive';
	}

	public function formatFile($file)
	{
		return Html::link($file->filename, $file->url);
	}

	public function formatTimeAgo($value)
	{
		return Time::timeAgo($value);
	}

	public function formatCredits($value)
	{
		return Yii::t('app', '{n} Credit|{n} Credits', $value);
	}

	public function formatCurrency($value)
	{
		return '$'.number_format(CString::currency($value), 2);
	}

	public function formatTouches($touches, $d=null)
	{
		$array = array();
		if (!empty($touches))
		{
			foreach ($touches as $touch)
				$array[] = '<a href="'.Yii::app()->createUrl('/touch/update', array('id'=>$touch->id)).'">'.$touch->user->profile->name.' ('.$touch->category->title.')</a>';
		}
		else
			$array[] = 'None';
		return implode('<br>', $array);
	}

	public function formatDelete($value)
	{
		return 'foo';
		die(var_dump($this->owner));
		return $this->getOwner();
		return 'foo';
	}
}