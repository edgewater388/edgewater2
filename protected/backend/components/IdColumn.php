<?php

Yii::import('zii.widgets.grid.CGridColumn');

class IdColumn extends CGridColumn
{
	public $htmlOptions=array('class'=>'id-column');

	/**
	 * Renders the data cell content.
	 * This method renders the view, update and delete buttons in the data cell.
	 * @param integer $row the row number (zero-based)
	 * @param mixed $data the data associated with the row
	 */
	protected function renderDataCellContent($row,$data)
	{
		echo $data->primaryKey;
	}
}