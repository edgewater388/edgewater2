<?php

Yii::import('zii.widgets.grid.CGridColumn');

class InfoColumn extends CGridColumn
{
	public $htmlOptions=array('class'=>'info-column icon-column text-light');

	public function init()
	{
		
	}

	/**
	 * Renders the data cell content.
	 * This method renders the view, update and delete buttons in the data cell.
	 * @param integer $row the row number (zero-based)
	 * @param mixed $data the data associated with the row
	 */
	protected function renderDataCellContent($row,$data)
	{
		$info = array();
		$info[] = Yii::t('app', 'n<1#No Touches|n==1#{n} Touch|n>1#{n} Touches', $data->touchCount);

		$controller = lcfirst(get_class($data));
		
		$str = '';
		$str .= '<a href="'.Yii::app()->createUrl($controller.'/view', array('id'=>$data->primaryKey)).'">';
		$str .= '<span class="fa fa-user" data-toggle="tooltip" title="'.implode('<br>', $info).'"></span>';
		$str .= '</a>';
		echo $str;
	}
}
