<?php

Yii::import('zii.widgets.grid.CGridColumn');

class LinkColumn extends CGridColumn
{
	public $htmlOptions=array('class'=>'link-column');

	public $module;
	public $controller;

	public $action = 'view';

	/**
	 * Renders the data cell content.
	 * This method renders the view, update and delete buttons in the data cell.
	 * @param integer $row the row number (zero-based)
	 * @param mixed $data the data associated with the row
	 */
	protected function renderDataCellContent($row,$data)
	{
		$controller = lcfirst(get_class($data));
		$module = $this->module ? $this->module.'/' : '';

		$primaryKey = $data->getTableSchema()->primaryKey;

		echo CHtml::link('<span class="text-light">›</span>',Yii::app()->createUrl($module.$controller.'/'.$this->action, array($primaryKey=>$data->primaryKey)));
	}
}
