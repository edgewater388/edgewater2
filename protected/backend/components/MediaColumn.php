<?php

Yii::import('zii.widgets.grid.CGridColumn');

class MediaColumn extends CGridColumn
{
	public $htmlOptions=array('class'=>'media-column icon-column');
	public $name;
	public $relation;
	public $emptyText = 'No File';

	public function init()
	{
		$this->relation = ActiveRecord::getRelationFromName($this->name);
	}

	/**
	 * Renders the data cell content.
	 * This method renders the view, update and delete buttons in the data cell.
	 * @param integer $row the row number (zero-based)
	 * @param mixed $data the data associated with the row
	 */
	protected function renderDataCellContent($row,$data)
	{
		if (isset($data->{$this->relation}))
		{
			echo '<a href="'.$data->{$this->relation}->url.'"><span class="glyphicon glyphicon-paperclip"></span></a>';
		}
		else
			echo '<span class="glyphicon glyphicon-exclamation-sign text-red" data-toggle="tooltip" title="'.$this->emptyText.'"></span>';
		
		// $classes = $data->status ? 'glyphicon-ok-circle text-success' : 'glyphicon-remove-circle text-danger';
		// echo '<span class="glyphicon  '.$classes.'" data-toggle="tooltip" title="'.$data->statusText.'"></span>';
		// echo CHtml::link('<span class="glyphicon glyphicon glyphicon-certificate"></span>',Yii::app()->controller->createUrl('view', array('id'=>$data->primaryKey)));
	}
}