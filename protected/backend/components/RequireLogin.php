<?php
class RequireLogin extends CBehavior
{
	public function attach($owner)
	{
        // @todo fix error with virtual localhost 3000/8000 not loading backend properly
        if (substr($_SERVER['HTTP_HOST'], strlen($_SERVER['HTTP_HOST'])-4) == "8000")
            return;

        $owner->attachEventHandler('onBeginRequest', array($this, 'handleBeginRequest'));
	}

	public function handleBeginRequest($event)
	{
		// var_dump(Yii::app()->request->requestUri);
	    if (Yii::app()->user->isGuest && Yii::app()->request->requestUri != '/backend/login') {
	        Yii::app()->user->loginRequired();
	    }
	}
}
