<?php

Yii::import('zii.widgets.grid.CGridColumn');

class StatusColumn extends CGridColumn
{
	public $htmlOptions=array('class'=>'status-column');

	public $style = 'text'; // text, icon

	/**
	 * Renders the data cell content.
	 * This method renders the view, update and delete buttons in the data cell.
	 * @param integer $row the row number (zero-based)
	 * @param mixed $data the data associated with the row
	 */
	protected function renderDataCellContent($row,$data)
	{
		if ($this->style == 'icon')
		{
			$classes = $data->status ? 'glyphicon-ok-circle text-success' : 'glyphicon-remove-circle text-danger';
			echo '<span class="glyphicon  '.$classes.'" data-toggle="tooltip" title="'.$data->statusText.'"></span>';
		}
		else
		{
			$text = $data->status ? 'Active' : 'Inactive';
			echo '<span class="text-light">'.$text.'</span>';
		}
	}
}