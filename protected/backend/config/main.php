<?php

$backend=dirname(dirname(__FILE__));
$frontend=dirname($backend);
$api=dirname($frontend).'/protected/api';

Yii::setPathOfAlias('backend', $backend);
Yii::setPathOfAlias('frontend', $frontend);
Yii::setPathOfAlias('api', $api);

$frontendArray=require($frontend.'/config/main.php');

unset($frontendArray['components']['urlManager']['rules']);
unset($frontendArray['behaviors']);

// This is the main Web application backend configuration. Any writable
// CWebApplication properties can be configured here.
$backendArray=array(
	'basePath' => $frontend,
	// 'basePath' => $backend,
	// 'name'=>'Admin',
	'theme'=>'backend',

	'controllerPath' => $backend.'/controllers',
	'viewPath' => $backend.'/views',
	'runtimePath' => $backend.'/runtime',

	// autoloading model and component classes
	'import'=>array(
		// 'application.models.*',
		// 'application.components.*',
		// 'application.helpers.*',
		// 'application.extensions.*',        
		// 'application.extensions.s3.*',
		'backend.models.*',
		'backend.helpers.*',
		'backend.components.*',
		'backend.vendors',
		'backend.modules.*',
		'backend.modules.commerce.models.*',
		'frontend.modules.commerce.components.*',
		'frontend.components.DataHandler',
	),
	// application components
	'components'=>array(
		'user'=>array(
			'loginUrl' => '/backend/login',
			'class' => 'AdminUser',
			'allowAutoLogin'=>true,
			'autoRenewCookie' => true,
			'authTimeout' => 3600*24,
		),

		'authManager'=>array(
			'class'=>'CDbAuthManager',
			'connectionID'=>'db',
		),

		'urlManager'=>array(
			// 'urlFormat'=>'path',
			// 'showScriptName'=>false,
			// 'caseSensitive'=>false,
			// 'baseUrl'=>'/backend',
			'rules'=>require(dirname(__FILE__).'/routes.php'),
		),

		'frontUrlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'class'=>'UrlManager',
            // 'rules'=>require(dirname(__FILE__).'/../../config/routes.php'),
            'rules'=>require(Yii::getPathOfAlias('frontend.config.routes').'.php'),
        ),

		'assetManager'=>array(
            // 'forceCopy'=>true,
        ),

		'db'=>require($frontend.'/config/database.php'),

		'widgetFactory'=>array(
            'widgets'=>array(
                'LinkPager'=>array(
                    'header'=>false,
                    'maxButtonCount'=>25,
                    'cssFile'=>false,
                ),
				'CDetailView'=>array(
					'cssFile'=>false,
					'htmlOptions'=>array('class'=>'table table-condensed table-striped detail-view'),
				),
				'CListView'=>array(
					'template'=>'{items} <div class="grid-footer">{summary} {pager} <div class="clearfix"></div></div>',
				),
				'CGridView'=>array(
					'cssFile'=>false,
					'hideHeader'=>true,
					'itemsCssClass'=>'table',
					'template'=>'{items} <div class="grid-footer">{summary} {pager} <div class="clearfix"></div></div>',
					'ajaxUpdate'=>false,
				),
				'CBreadcrumbs'=>array(
					'separator'=>'',
					'homeLink'=>false,
					'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
					'inactiveLinkTemplate'=>'<li>{label}</li>',
					'tagName'=>'ol',
					'htmlOptions'=>array('class'=>'breadcrumb'),
				),
			),
		),

		'format' => array(
	        'class' => 'backend.components.Formatter',
	    ),
	),

	'behaviors'=>array(
		'onBeginRequest'=>array(
			'class'=>'application.backend.components.RequireLogin',
		),
	),

	'modules'=>array(
		'user'=>array(
			// 'basePath'=>$backend.'/modules/user',
			'class'=>'backend.modules.user.UserModule',
		),
		'commerce'=>array(
			// 'basePath'=>$backend.'/modules/commerce',
			'class'=>'backend.modules.commerce.CommerceModule',
		),
		'files'=>array(
			'basePath'=>$backend.'/modules/files',
		),
	),
	// main is the default layout
	//'layout'=>'main',
	// alternate layoutPath
	//'layoutPath'=>dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'_layouts'.DIRECTORY_SEPARATOR,

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName'] and MParams class
	'params'=>require($frontend.'/config/params.php'),
);

$config = CMap::mergeArray($frontendArray, $backendArray);
return $config;
?>