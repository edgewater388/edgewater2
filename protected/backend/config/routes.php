<?php

return array(

	// Backend Base API
	'backend/<_c>/api/<q>'=>'<_c>/api',
	'backend/user/autocomplete/<q>'=>'user/users/autocomplete',
	'backend/<_c>/autocomplete/<q>'=>'<_c>/autocomplete',

	'backend'=>'site/index',
	'backend/login'=>'user/users/login',
	'backend/logout'=>'user/users/logout',

	// users
	'backend/users'=>'user/users/index',
	'backend/users/<id:\d+>'=>'user/users/view',
	'backend/users/<action>'=>'user/users/<action>',
	'backend/users/<action>/<id:\d+>'=>'user/users/<action>',
	'backend/users/<user_id:\d+>/<controller:\w+>'=>'<controller>/view',
	// 'backend/users/<controller:\w+>/<id:\d+>'=>'user/<controller>/view',
	// 'backend/users/<controller:\w+>/<action>/<id:\d+>'=>'user/<controller>/<action>',
	// 'backend/<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',	
	// 'backend/<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<module>/<controller>/<action>',

	// file paths
	'backend/files/<type:(pdf|image)>'=>'files/default/index',
	'backend/files/default/view/<id:\d+>'=>'files/default/view',
	'backend/files/default/browser/<type:(pdf|image)>'=>'files/default/browser',

	// Module routes
	'backend/<module:\w+>/<controller:\w+>/<id:\d+>'=>'<module>/<controller>/view',
	'backend/<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<module>/<controller>/<action>',
	'backend/<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',

	// Generic controller/pages
	'backend/<controller:\w+>/<id:\d+>'=>'<controller>/view',
	'backend/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
	'backend/<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
	

	'backend/<_c>'=>'<_c>',
	'backend/<_c>/<_a>'=>'<_c>/<_a>',
);