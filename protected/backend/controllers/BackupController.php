<?php

class BackupController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex($backup=false, $download=false)
	{
		if ($backup)
		{
			$filepath = Yii::app()->dbManager->backup();

			if ($filepath)
			{
				if ($download)
				{
					header('Content-Description: File Transfer');
					header('Content-Type: application/x-gzip');
					header('Content-Disposition: attachment; filename="'.basename($filepath).'"'); //<<< Note the " " surrounding the file name
					header('Content-Transfer-Encoding: binary');
					header('Connection: Keep-Alive');
					header('Expires: 0');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Pragma: public');
					header('Content-Length: ' . filesize($filepath));

					ob_clean();
					flush();
					readfile($filepath);

					unlink($filepath);
					exit;
				}

				Flash::setFlash('success', 'Database successfully backed up ('.$filepath.')');
			}
		}

		$this->redirect(array('/site/settings'));
		// $this->render('index', array(
		// ));
	}
}