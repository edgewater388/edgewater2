<?php

class MembershipController extends BaseController
{
	public function getDefaultMenu($model=null)
	{
		$menu = parent::getDefaultMenu($model);

		// unset($menu['create']);
		// unset($menu['delete']);

		return $menu;
	}

	public function actionCreate($user_id=null)
	{
		$plan = Plan::getDefaultPlan();

		$model = new Membership;
		$model->user_id = $user_id;
		$model->plan_id = null;
		$model->subtotal = 0;
		$model->join_date = date('Y-m-d');
		$model->next_billing_date = null;
		$model->interval = null;
		$model->auto_renew = 0;
		$model->status = 1;

		if(isset($_POST[$this->class]))
		{
			$model->attributes=$_POST[$this->class];
			if($model->save())
				$this->redirect(array('view',$this->primaryKeyName=>$model->primaryKey));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id=null, $user_id=null)
	{
		$model = null;

		if ($id)
			$model=$this->loadModel($id);
		elseif ($user_id)
		{
			$models = Membership::model()->byAttribute('user_id', $user_id)->findAll();
			if (count($models) > 1)
			{
				Flash::setFlash('info', 'This user has more than 1 membership.');
				$this->redirect(array('index', 'user_id'=>$user_id));
			}
			$model = isset($models[0]) ? $models[0] : null;
		}
		
		if (!$model)
		{
			$this->redirect(array('create', 'user_id'=>$user_id));
			exit;
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST[$this->class]))
		{
			$model->attributes=$_POST[$this->class];

			if($model->save())
				$this->redirect(array('view',$this->primaryKeyName=>$model->primaryKey));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionRetention($export=false)
	{
		$model = new Membership('search');
		$model->unsetAttributes();
		$model->attributes = isset($_GET['Membership']) ? $_GET['Membership'] : null;

		if ($export)
			$model->exportRetention();

		$this->render('retention', array(
			'model'=>$model,
		));
	}

	// @todo the id used should really be membership_id, but thit is only available
	// in the serialized data atm. Add a column membership_id, start tracking membership_id, 
	// extract the membership id from the serialized array, lookup each log by membership_id.
	public function actionLog($id)
	{
		$model = MembershipLog::model()->findByPk($id);

		$this->render('log', array(
			'model'=>$model,
		));
	}
}
