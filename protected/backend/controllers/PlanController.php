<?php

class PlanController extends BaseController
{
	public function getDefaultMenu($model = null)
	{
		if (!$model)
			$model = new $this->class;

		$menu = $this->menu;

		if (!$model->isNewRecord)
		{
			$menu['view'] = array('label'=>'<span class="fa fa-eye"></span>', 'url'=>array('view', 'id'=>$model->primaryKey)+$this->urlParams, 'linkOptions'=>array('data-toggle'=>'tooltip', 'title'=>'View'), 'active'=>$this->action->id == 'view');
			$menu['update'] = array('label'=>'<span class="fa fa-pencil"></span>', 'url'=>array('update', 'id'=>$model->primaryKey)+$this->urlParams, 'linkOptions'=>array('data-toggle'=>'tooltip', 'title'=>'Edit'), 'active'=>$this->action->id == 'update');
			if ($model->is_special) {
				$menu['delete'] = array('label'=>'<span class="fa fa-close text-red"></span>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->primaryKey),'confirm'=>'Are you sure you want to delete this item?', 'data-toggle'=>'tooltip', 'title'=>'Delete'));
			}	
		}

		if (!empty($menu))
			$menu['separator_0'] = array('label'=>'<span class="separator"></span>', 'url'=>null);

		// $menu[] = array('label'=>'<span class="fa fa-list"></span>', 'url'=>array('index'), 'active'=>$this->action->id == 'index');
		$menu['create'] = array('label'=>'New '.$model->classLabel, 'url'=>array('create')+$this->urlParams, 'active'=>$this->action->id == 'create');

		return $menu;
	}
}
