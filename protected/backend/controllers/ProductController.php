<?php

class ProductController extends BaseController {
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id );
		
		if ( isset( $_POST[ $this->class ] ) ) {
			$model->attributes = $_POST[ $this->class ];
			
			if ( $model->save() ) {
				$this->redirect( array( 'view', $this->primaryKeyName => $model->primaryKey ) );
			}
		}
		
		$this->render( 'update', array(
			'model' => $model,
		) );
	}
}