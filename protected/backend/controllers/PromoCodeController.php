<?php

class PromoCodeController extends BaseController
{
	public function actionReport($export=null)
	{
		$model = new PromoCode('search');
		$model->unsetAttributes();

		if (isset($_GET['PromoCode']))
			$model->attributes = $_GET['PromoCode'];

		$model->with('redemptionCount');

		if ($export)
			$model->exportReport();

		$this->render('report', array(
			'model'=>$model,
		));
	}
}