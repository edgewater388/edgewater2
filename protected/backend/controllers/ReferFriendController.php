<?php

class ReferFriendController extends Controller
{
	public function actionSettings()
	{
		if (isset($_POST['reward_amount']))
			if (Content::setValue('refer_friend_reward_amount', $_POST['reward_amount']))
				Flash::setFlash('success', 'Reward amount successfully updated');

		if (isset($_POST['promo_code']))
			if (Content::setValue('refer_friend_promo_code', $_POST['promo_code']))
				Flash::setFlash('success', 'Promo Code successfully updated');

		$this->render('settings', array(
			'reward_amount'=>Content::getValue('refer_friend_reward_amount'),
			'promo_code'=>Content::getValue('refer_friend_promo_code'),
		));
	}

	public function actionInvites($export=null)
	{
		$model = new ReferralInvite('search');
		$model->unsetAttributes();

		if (isset($_GET['ReferralInvite']))
			$model->attributes = $_GET['ReferralInvite'];

		if ($export)
			$model->export();
		
		$this->render('invites', array(
			'model'=>$model,
		));
	}

	public function actionRewards($export=null)
	{
		$model = new ReferralBank('search');
		$model->unsetAttributes();

		if (isset($_GET['ReferralBank']))
			$model->attributes = $_GET['ReferralBank'];

		if ($export)
			$model->export();

		$this->render('rewards', array(
			'model'=>$model,
		));
	}
}