<?php

class RegimenController extends BaseController
{

	public function getDefaultMenu($model=null)
	{
		$menu = parent::getDefaultMenu($model);

		if ($this->action->id == 'view')
		{
			array_unshift($menu, array('label'=>'<span class="separator"></span>', 'url'=>null));
			array_unshift($menu, array('label'=>'Videos', 'url'=>array('regimenVideo/index', 'regimen_id'=>$model->id)));
			array_unshift($menu, array('label'=>'Meal plans', 'url'=>array('regimenMealPlan/index', 'regimen_id'=>$model->id)));
		}

		return $menu;
	}
}