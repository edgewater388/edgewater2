<?php

class RegimenMealPlanController extends BaseController
{
	public function getDefaultMenu($model=null)
	{
		$menu = parent::getDefaultMenu($model);

		$regimen_id = $model->regimen_id ? $model->regimen_id : Yii::app()->request->getParam('regimen_id');
		if ($regimen_id)
		{
			$create = array_pop($menu);
			$create['url'] = array('regimenMealPlan/create', 'regimen_id'=>$regimen_id);
			array_push($menu, $create);
		}

		return $menu;
	}
}