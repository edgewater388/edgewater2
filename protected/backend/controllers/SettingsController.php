<?php

class SettingsController extends BaseController
{
	public function getDefaultMenu($menu=null)
	{
		return array();
	}

	public function actionSave()
	{
		if (!isset($_POST['Settings'])) return $this->redirect(['index']);

		$model = new Settings();
		$model->saveSettings($_POST['Settings']);
		return $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
	}
}