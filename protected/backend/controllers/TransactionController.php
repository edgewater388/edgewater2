<?php

class TransactionController extends BaseController
{
	public function getDefaultMenu($model = null)
	{
		$menu = parent::getDefaultMenu();

		unset($menu['update']);
		unset($menu['delete']);
		unset($menu['separator_0']);
		unset($menu['create']);

		return $menu;
	}

	public function actionIndex($export=false)
	{
		$model=new $this->class;
		$model->scenario = 'search';
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET[$this->class]))
			$model->attributes=$_GET[$this->class];

		$model->attributes = $_GET;
		$model->approved = 1;

		if ($export)
			$model->export();

		$this->render('index',array(
			'model'=>$model,
		));
	}
}