<?php

class VideoController extends BaseController
{
	public function actionIndex()
	{
		$this->menu = [
			['label' => '<span class="fa fa-refresh"></span> Retrieve JW Player Videos', 'url' => $this->createUrl('retrieve')],
		];
		parent::actionIndex();
	}

	public function actionView($id)
	{
		// $menu = array('label'=>'<span class="fa fa-refresh"></span>', 'url'=>$this->createUrl('retrieve'));
		$menu = ['label' => '<span class="fa fa-shopping-cart"></span>', 'url' => ['products', 'id' => $id] + $this->urlParams, 'linkOptions' => ['data-toggle' => 'tooltip', 'title' => 'Related Products'], 'active' => $this->action->id == 'products'];
		array_unshift($this->menu, $menu);

		parent::actionView($id);
	}

	public function actionRetrieve()
	{
		$result = Video::retrieveAll();

		Flash::setFlash('info', $result['saved'] . '/' . $result['count'] . ' videos have been retrieved and updated');

		$this->redirect($this->createUrl('index'));
	}

	/**
	 * Creates a new video.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new $this->class;

		if ($model->columnExists('status'))
			$model->status = 1;

		// check if there are any query params set for default values
		foreach ($model->attributes as $column => $defaultValue) {
			if ($value = Yii::app()->request->getParam($column))
				$model->$column = $value;
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST[$this->class])) {
			$model->attributes = $_POST[$this->class];
			$model->body_part = !empty($_POST[$this->class]['body_part']) ?
				implode(',', $_POST[$this->class]['body_part']) : null;
			$model->equipment = !empty($_POST[$this->class]['equipment']) ?
				implode(',', $_POST[$this->class]['equipment']) : null;

			if ($model->save())
				$this->redirect(['view', $this->primaryKeyName => $model->primaryKey]);
		}

		$this->render('create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates a particular Video model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST[$this->class])) {
			$model->attributes = $_POST[$this->class];
			$model->body_part = !empty($_POST[$this->class]['body_part']) ?
				implode(',', $_POST[$this->class]['body_part']) : null;
			$model->equipment = !empty($_POST[$this->class]['equipment']) ?
				implode(',', $_POST[$this->class]['equipment']) : null;

			if ($model->save())
				$this->redirect(['view', $this->primaryKeyName => $model->primaryKey]);
		}

		$this->render('update', [
			'model' => $model,
		]);
	}

	public function actionProducts($id)
	{
		$model = $this->loadModel($id);

		if (isset($_POST['Video']['product_ids'])) {
			$existing = [];
			$selected = is_array($_POST['Video']['product_ids']) ? $_POST['Video']['product_ids'] : [];
			$added = [];
			$removed = [];
			
			if (count($selected) > 3) {
				Flash::setFlash('error', 'Unable to attach more than 3 products');
				$this->redirect(['video/products', 'id' => $model->id]);
			}
			
			foreach ($model->products as $product)
				$existing[] = $product->id;

			$added = array_diff($selected, $existing);
			$removed = array_diff($existing, $selected);
			
			foreach ($added as $product_id)
				VideoProduct::add($model->id, $product_id);

			foreach ($removed as $product_id)
				VideoProduct::remove($model->id, $product_id);
			
			
			Flash::setFlash('success', 'Related products have been updated');
			$this->redirect(['view', 'id' => $model->id]);
		}

		$model->product_ids = [];
		foreach ($model->products as $product)
			$model->product_ids[] = $product->id;

		$this->render('products', [
			'model' => $model,
		]);
	}
}