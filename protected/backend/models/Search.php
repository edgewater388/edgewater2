<?php

class Search
{
	protected $limit = 10;

	function searchUsers($q)
	{
		$criteria=new CDbCriteria;

		$criteria->compare('profile.first_name',$q,true,'OR');
		$criteria->compare('profile.last_name',$q,true,'OR');
		$criteria->compare('email',$q,true,'OR');
		$criteria->limit = $this->limit;

		$models = Users::model()->with('profile')->findAll($criteria);

		$rows = array();
		if ($models) {
			foreach ($models as $model) {
				$name = trim($model->profile['first_name'].' '.$model->profile['last_name']);
				if (!$name)
					$name = '[Unknown]';

				$rows[] = array(
					'id'=>$model->id,
					'first_name'=>$model->profile['first_name'],
					'last_name'=>$model->profile['last_name'],
					'email'=>$model->email,
					'title'=>'<a href="/backend/users/'.$model->id.'">'.$name.'</a>',
					'description'=>$model->email,
				);
			}
		}

		return $rows;
	}
}