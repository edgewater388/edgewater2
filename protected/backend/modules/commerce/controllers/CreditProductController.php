<?php

class CreditProductController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('batchUpdate'),
				'roles'=>array('super admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	// Load and update all credit products
	public function actionBatchUpdate()
	{
		ini_set('memory_limit', '256M');

		$classes = array(
			'Track', 
			'BedTrack',
			'TrackSet',
			'ClinicVideo',
			'Clinic',
			'LessonVideo',
			'Lesson',
		);

		try {
			foreach ($classes as $class)
				foreach ($class::model()->findAll() as $model)
					if (!$model->save())
					{
						$message = '';
						foreach ($model->getErrors() as $errors)
							foreach ($errors as $error)
								$message .= $error.'<br>';
						throw new Exception('Class: '.$class.' could not be saved (id: '.$model->id.'). Errors: <br>'. $message);
					}

		} catch (Exception $e) {
			die(var_dump($e->getMessage()));
			// echo $e->getMessage();
			// exit;
		}

		echo 'complete';
		exit;
	}
}