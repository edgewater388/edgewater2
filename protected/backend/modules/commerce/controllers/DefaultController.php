<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function beforeAction($action)
	{
		$this->pageTitle = 'Commerce';
		return parent::beforeAction($action);
	}
}