<?php
/* @var $this CCreditPurchaseController */
/* @var $model CCreditPurchase */

$this->breadcrumbs=array(
	// 'Ccredit Purchases'=>array('index'),
	// 'Manage',
);

$this->menu=array(
	array('label'=>'<span class="glyphicon glyphicon-arrow-down"></span> Export', 'url'=>Export::getUrl()),
);
?>

<div class="page-header text-center" style="padding-bottom: 30px; margin-bottom: 0;">
	<h1>In-App Purchases</h1>
	<h2>Purchases made with credits</h2>
</div>

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'filters')); ?>
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
<?php $this->endWidget();?>

<div class="flush">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ccredit-purchase-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array('class'=>'IdColumn'),
		'description',
		'user.profile.name',
		// 'credit_product_id',
		// 'credits',
		// 'rate',
		// 'amount',
		/*
		'host',
		'status',
		'created',
		'updated',
		*/
		array('class'=>'StatusColumn'),
		array('class'=>'LinkColumn', 'module'=>'commerce'),
	),
)); ?>	
</div>

