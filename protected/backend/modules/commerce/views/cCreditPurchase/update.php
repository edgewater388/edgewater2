<?php
/* @var $this CCreditPurchaseController */
/* @var $model CCreditPurchase */

$this->breadcrumbs=array(
	'Ccredit Purchases'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CCreditPurchase', 'url'=>array('index')),
	array('label'=>'Create CCreditPurchase', 'url'=>array('create')),
	array('label'=>'View CCreditPurchase', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CCreditPurchase', 'url'=>array('admin')),
);
?>

<h1>Update CCreditPurchase <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>