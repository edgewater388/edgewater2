<?php
/* @var $this CCreditPurchaseController */
/* @var $model CCreditPurchase */

$this->breadcrumbs=array(
	'Purchases'=>array('index'),
	$model->id,
);

$this->menu=array(
	// array('label'=>'Update', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'<span class="text-red">Delete</span>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<div class="page-header">
	<h1>Purchase #<?php echo $model->id; ?></h1>
</div>

<div class="list-wrapper">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'credit_product_id',
		'credits',
		'rate',
		'amount:currency',
		'description',
		// 'host',
		'royaltiesSum:currency',
		'royaltiesCount',
		'status',
		'created',
		'updated',
	),
)); ?>
</div>

<div class="divider line flush"></div>
<div class="list-wrapper">
	<h4 class="heading">Account Info</h4>
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'user.id',
			'user.profile.name',
		),
	)); ?>
</div>

<div class="divider line flush"></div>
<?php $this->renderPartial('commerce.views.creditProduct._list', array('model'=>$model->creditProduct)); ?>

<div class="divider line flush"></div>
<?php $this->renderPartial('commerce.views.cRoyalty._list', array('model'=>$model->cRoyalties)); ?>