<?php
/* @var $this COrderController */
/* @var $model COrder */

$this->breadcrumbs=array(
	'Corders'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List COrder', 'url'=>array('index')),
	array('label'=>'Manage COrder', 'url'=>array('admin')),
);
?>

<h1>Create COrder</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>