<?php
/* @var $this COrderController */
/* @var $model COrder */

$this->breadcrumbs=array(
	'Corders'=>array('index'),
	'Manage',
);

$this->menu=array(
	// array('label'=>'Create COrder', 'url'=>array('create')),
	array('label'=>'<span class="glyphicon glyphicon-arrow-down"></span> Export', 'url'=>Export::getUrl()),
);
?>

<div class="page-header text-center" style="padding-bottom: 30px; margin-bottom: 0;">
	<h1>Orders</h1>
	<h2>Purchases made with credit cards (plans, top-ups)</h2>
</div>

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'filters')); ?>
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
<?php $this->endWidget();?>

<div class="flush">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'corder-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array('class'=>'IdColumn'),
		'user.profile.name',
		// 'confirm_code',
		// 'name',
		'total:currency',
		'order_status',
		// 'street',
		// 'street1',
		/*
		'city',
		'state',
		'country',
		'zipcode',
		'email',
		'phone',
		'subtotal',
		'tax_percent',
		'tax',
		'currency',
		'comment',
		'host',
		'status',
		'created',
		'updated',
		*/
		array('class'=>'StatusColumn'),
		array('class'=>'LinkColumn', 'module'=>'commerce'),
	),
)); ?>
</div>