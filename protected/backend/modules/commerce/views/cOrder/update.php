<?php
/* @var $this COrderController */
/* @var $model COrder */

$this->breadcrumbs=array(
	'Corders'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List COrder', 'url'=>array('index')),
	array('label'=>'Create COrder', 'url'=>array('create')),
	array('label'=>'View COrder', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage COrder', 'url'=>array('admin')),
);
?>

<h1>Update COrder <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>