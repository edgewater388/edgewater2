<?php
/* @var $this COrderController */
/* @var $model COrder */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->id,
	$model->name,
);

$this->menu=array(
	// array('label'=>'Update COrder', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'<span class="text-red">Delete</span>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<div class="page-header">
	<h1>Order #<?php echo $model->id; ?></h1>
</div>

<div class="list-wrapper">
	<h4 class="heading">Order Details</h4>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		// 'user_id',
		'confirm_code',
		'subtotal',
		'tax_percent',
		'tax',
		'total',
		'currency',
		'order_status',
		'comment',
		'host',
		'status:status',
		'created',
		'updated',
	),
)); ?>
</div>

<div class="list-wrapper">
	<h4 class="heading">Account Info</h4>
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'user.id',
			'user.profile.name',
			'user.email',
		),
	)); ?>
</div>

<div class="list-wrapper">
	<h4 class="heading">Billing Information</h4>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'street',
		'street1',
		'city',
		'state',
		'country',
		'zipcode',
		'email',
		'phone',
	),
)); ?>
</div>

<div class="list-wrapper">
	<h4 class="heading">Product Details</h4>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model->cOrderProduct,
	'attributes'=>array(
		'product_id',
		'type',
		'title',
		'description',
		'price',
		'quantity',
		'subtotal',
	),
)); ?>
</div>

<div class="list-wrapper">
	<h4 class="heading">Transaction Details</h4>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model->cTransaction,
	'attributes'=>array(
		'id',
		'type',
		'payment_method',
		'customer_ref_num',
		'amount',
		'card_number',
		'card_expiry',
		'card_type',
		'service',
		'approved:boolean',
		'trans_ref',
		'message',
		'live:boolean',
	),
)); ?>
</div>
