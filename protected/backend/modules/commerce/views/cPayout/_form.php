<?php
/* @var $this CPayoutController */
/* @var $model CPayout */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'id'=>'cpayout-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<div class="controls">
			<?php //echo $form->textField($model,'user_id',array('size'=>10,'maxlength'=>10)); ?>
			<?php $textValue = ($model->user_id) ? $model->user->profile->name.' ('.$model->user->email.')' : ''; ?>
			<?php echo $form->typeahead($model,'user_id',array('size'=>'60', 'textValue'=>$textValue, 'data-url'=>'/backend/users/search?Users[name_or_email]=')); ?>
			<?php echo $form->error($model,'user_id'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'amount'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'amount',array('size'=>8,'maxlength'=>8)); ?>
			<?php echo $form->error($model,'amount'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'trans_ref'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'trans_ref',array('size'=>60,'maxlength'=>128)); ?>
			<?php echo $form->error($model,'trans_ref'); ?>
		</div>
	</div>

	<div class="form-actions buttons">
		<?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->