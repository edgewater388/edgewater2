<?php
/* @var $this CPayoutController */
/* @var $model CPayout */

$this->breadcrumbs=array(
	'Payouts'=>array('index'),
	'New Payout',
);

$this->menu=array(
	
);
?>

<div class="page-header">
	<h1>Create Payout</h1>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>