<?php
/* @var $this CPayoutController */
/* @var $model CPayout */

$this->breadcrumbs=array(
	'Cpayouts'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'<span class="fa fa-plus-circle"></span> New Payout', 'url'=>array('create')),
	array('label'=>'<span class="glyphicon glyphicon-arrow-down"></span> Export', 'url'=>Export::getUrl()),
);
?>

<div class="page-header text-center" style="padding-bottom: 10px; margin-bottom: 0;">
	<h1>Royalty Payouts</h1>
</div>

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'filters')); ?>
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
<?php $this->endWidget();?>

<div class="flush">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cpayout-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array('class'=>'IdColumn'),
		'user.profile.name',
		'amount:currency',
		'trans_ref',
		array('class'=>'StatusColumn'),
		array('class'=>'LinkColumn', 'module'=>'commerce'),
	),
)); ?>
</div>