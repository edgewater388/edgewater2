<?php
/* @var $this CPayoutController */
/* @var $model CPayout */

$this->breadcrumbs=array(
	'Payouts'=>array('index'),
	$model->user->profile->name,
	$model->id=>array('view','id'=>$model->id),
	'Edit Payout',
);

$this->menu=array(
	// array('label'=>'List CPayout', 'url'=>array('index')),
	// array('label'=>'Create CPayout', 'url'=>array('create')),
	// array('label'=>'View CPayout', 'url'=>array('view', 'id'=>$model->id)),
	// array('label'=>'Manage CPayout', 'url'=>array('admin')),
);
?>

<div class="page-header">
	<h1><?php echo $model->user->profile->name; ?></h1>
	<h2>Payout #<?php echo $model->id; ?></h2>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>