<?php
/* @var $this CPayoutController */
/* @var $model CPayout */

$this->breadcrumbs=array(
	'Payouts'=>array('index'),
	$model->user->profile->name,
	$model->id,
);

$this->menu=array(
	array('label'=>'<span class="fa fa-cog"></span> Edit Payout', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'<span class="text-red">Delete</span>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<div class="page-header">
	<h1><?php echo $model->user->profile->name; ?></h1>
	<h2>Payout #<?php echo $model->id; ?></h2>
</div>

<div class="list-wrapper">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'user.profile.name',
		// 'name',
		// 'email',
		'amount:currency',
		'trans_ref',
		'status:status',
		'created',
		'updated',
	),
)); ?>
</div>
