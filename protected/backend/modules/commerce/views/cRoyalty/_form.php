<?php
/* @var $this CRoyaltyController */
/* @var $model CRoyalty */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'id'=>'croyalty-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'percent'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'percent',array('size'=>16,'maxlength'=>16)); ?>
			<?php echo $form->error($model,'percent'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'amount'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'amount',array('size'=>8,'maxlength'=>8)); ?>
			<?php echo $form->error($model,'amount'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'status'); ?>
		<div class="controls">
			<?php echo $form->statusField($model,'status'); ?>
			<?php echo $form->error($model,'status'); ?>
		</div>
	</div>

	<div class="form-actions buttons">
		<?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->