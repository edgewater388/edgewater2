<div class="list-wrapper">
	<h4 class="heading">Royalties</h4>
	<?php $dataProvider = new CArrayDataProvider($model); ?>
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'track-grid',
		'dataProvider'=>$dataProvider,
		'columns'=>array(
			array('class'=>'IdColumn'),
			'touch.category.title',
			'touch.user.profile.name',
			'amount:currency',			
			array(
				'class'=>'LinkColumn',
				'module'=>'commerce',
			),
		),
	)); ?>
</div>

