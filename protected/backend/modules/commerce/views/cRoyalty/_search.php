<?php
/* @var $this CRoyaltyController */
/* @var $model CRoyalty */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-content">
		<div class="row">
			<div class="col-xs-8">
				<div class="form-group">
					<?php echo $form->labelEx($model, 'user_id'); ?>
					<div class="controls">
						<?php $textValue = $model->searchUser ? $model->searchUser->profile->name.' ('.$model->searchUser->email.')' : null; ?>
						<?php echo $form->typeahead($model,'user_id',array('class'=>'form-control input-sm', 'textValue'=>$textValue, 'placeholder'=>$model->getAttributeLabel('user_id'), 'data-url'=>'/backend/users/search?Users[name_or_email]=')); ?>
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="form-group">
					<?php echo $form->labelEx($model, 'id'); ?>
					<div class="controls">
						<?php echo $form->textField($model,'id',array('maxlength'=>11,'class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('id'))); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model, 'created'); ?>
			<div class="controls">
				<div class="row">
					<div class="col-xs-6">
						<?php echo $form->dateField($model,'created_from',array('class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('created_from'))); ?>
					</div>
					<div class="col-xs-6">
						<?php echo $form->dateField($model,'created_to',array('class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('created_to'))); ?>
					</div>
				</div>
			</div>
		</div>

		<?php /*
		<div class="row">
			<div class="col-xs-8">
				<div class="form-group">
					<?php echo $form->labelEx($model, 'credit_purchase_id'); ?>
					<div class="controls">
						<?php echo $form->textField($model,'credit_purchase_id',array('maxlength'=>128,'class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('credit_purchase_id'))); ?>
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="form-group">
					<?php echo $form->labelEx($model, 'touch_id'); ?>
					<div class="controls">
						<?php echo $form->textField($model,'touch_id',array('maxlength'=>11,'class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('touch_id'))); ?>
					</div>
				</div>
			</div>
		</div>
		*/ ?>

		<div class="form-actions">
			<?php echo Html::button('Clear', array('class'=>'btn btn-sm btn-default', 'data-toggle'=>'clearForm')); ?>
			<?php echo Html::submitButton('Search', array('class'=>'btn btn-sm btn-primary pull-right')); ?>
			<div class="clearfix"></div>
		</div>

	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->