<?php
/* @var $this CRoyaltyController */
/* @var $model CRoyalty */

$this->breadcrumbs=array(
	'Croyalties'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'<span class="glyphicon glyphicon-arrow-down"></span> Export', 'url'=>Export::getUrl()),
);
?>

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'filters')); ?>
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
<?php $this->endWidget();?>

<div class="flush">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'croyalty-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array('class'=>'IdColumn'),
		'creditPurchase.description',
		'touch.category.title',
		'touch.user.profile.name',
		'amount:currency',
		array('class'=>'StatusColumn'),
		array('class'=>'LinkColumn', 'module'=>'commerce'),
	),
)); ?>
</div>