<?php
/* @var $this CRoyaltyController */
/* @var $model CRoyalty */

$this->breadcrumbs=array(
	'Royalties'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	// array('label'=>'View', 'url'=>array('view', 'id'=>$model->id)),
);
?>

<div class="page-header">
	<h1>Update Royalty #<?php echo $model->id; ?></h1>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>