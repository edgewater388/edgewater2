<?php
/* @var $this CRoyaltyController */
/* @var $model CRoyalty */

$this->breadcrumbs=array(
	'Royalties'=>array('index'),
	$model->id,
);

$this->menu=array(
	// array('label'=>'Update CRoyalty', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'<span class="text-red">Delete</span>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<div class="page-header">
	<h1>Royalty #<?php echo $model->id; ?></h1>
</div>

<div class="list-wrapper">
	<h4 class="heading">Royalty</h4>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		// 'credit_purchase_id:text:Purchase ID',
		// 'touch_id:text:Touch ID',
		// 'creditPurchase.amount:text:Purchase Amount',
		'percent',
		'amount:currency:Royalty',
		'status:status',
		'created',
		// 'updated',
	),
)); ?>
</div>

<div class="divider line flush" style="margin-top: 3em; margin-bottom: 3em;"></div>

<div class="list-wrapper">
	<h4 class="heading">Touch Details</h4>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'touch.id',
		'touch.category.title',
		'touch.category.percent',
		'touch.user_id',
		'touch.user.profile.name',
		'touch.user.email',
	),
)); ?>
</div>

<div class="list-wrapper">
	<h4 class="heading">Purchase Details</h4>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'creditPurchase.id',
		'creditPurchase.credit_product_id',
		'creditPurchase.creditProduct.title',
		'creditPurchase.credits',
		'creditPurchase.rate',
		'creditPurchase.amount:currency:Total',
		'creditPurchase.creditProduct.class',
		'creditPurchase.product.id',
	),
)); ?>
</div>
