<div class="list-wrapper">
	<h4 class="heading">Product Info</h4>
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'id',
			'title',
			'description',
			'credits',
			// 'quantity',
			'status:status',
			'created',
			'updated',
		),
	)); ?>
</div>