<?php $this->classes[] = 'detail-page'; ?>

<div class="page-header">
	<h1>Overview</h1>
</div>

<div class="list-wrapper">
	<h4 class="heading">Store Details</h4>
	<table class="table table-condensed table-striped detail-view">
		<tbody>
			<tr>
				<th>Payment Service</th>
				<td><?php echo (Yii::app()->getModule('commerce')->paymentService->service) ? Yii::app()->getModule('commerce')->paymentService->service : 'Not Set'; ?></td>
			</tr>
			<tr>
				<th>Dry Run</th>
				<td><?php echo (Yii::app()->getModule('commerce')->paymentService->dryRun) ? '<span class="text-green">Enabled</span>' : '<span class="text-red">Disabled</span>'; ?></td>
			</tr>
			<tr>
				<th>Dry Run Auto-Approve</th>
				<td><?php echo (Yii::app()->getModule('commerce')->paymentService->dryRunApproved) ? '<span class="text-green">Enabled</span>' : '<span class="text-red">Disabled</span>'; ?></td>
			</tr>
			<tr>
				<th>Live</th>
				<td><?php echo (Yii::app()->getModule('commerce')->paymentService->live) ? '<span class="text-green">Yes</span>' : '<span class="text-red">No</span>'; ?></td>
			</tr>
		</tbody>
	</table>
</div>
