<div class="list-wrapper">
	<h4 class="heading">Product Info</h4>
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'id',
			// 'title',
			'price',
			// 'quantity',
			'type',
			// 'description',
			'status:status',
			'created',
			'updated',
		),
	)); ?>
</div>