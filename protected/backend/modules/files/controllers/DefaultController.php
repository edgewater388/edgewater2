<?php
// die('here');
class DefaultController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','create','view','browser','inlineUpload'),
				'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('purge','purgeAll','update','getInfo','resetTimes'),
				'roles'=>array('super admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionPurge($style)
	{	
		$dest = Yii::app()->basePath.'/../';
		$dest .= Yii::app()->getModule('files')->file->getFileDestination();
		$dest .= '/thumb/'.$style;

		$files = scandir($dest);

		foreach ($files as $file)
		{
			if ($file != '.' && $file != '..') {
				$filepath = $dest.'/'.$file;
				@unlink($filepath);
			}
		}
		
		exit;
	}

	public function actionPurgeAll()
	{	
		$dest = Yii::app()->basePath.'/../';
		$dest .= Yii::app()->getModule('files')->file->getFileDestination();
		$dest .= '/thumb/';

		$styles = Yii::app()->getModule('files')->thumb->styles;
		foreach ($styles as $style=>$options)
		{
			$style_dest = $dest.$style;
			if (is_dir($style_dest))
			{
				$files = scandir($style_dest);
				foreach ($files as $file)
				{
					if ($file != '.' && $file != '..') {
						$filepath = $style_dest.'/'.$file;
						@unlink($filepath);
					}
				}
			}
		}

		exit;
	}

	public function actionIndex($type='image')
	{
		$model = Files::model()->published();
		$dataProvider = new CActiveDataProvider($model, array(
			'criteria'=>array(
				'condition'=>'filemime LIKE :filemime AND inline=1',
				'params'=>array(
					':filemime'=>'%'.$type.'%',
				),
			),
		));

		$view = ($type == 'image') ? 'images' : 'files';
		$this->render($view, array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionBrowser($type='image')
	{
		$this->layout = '/layouts/browser';
		$model = Files::model()->published();
		$dataProvider = new CActiveDataProvider($model, array(
			'criteria'=>array(
				'condition'=>'filemime LIKE :filemime AND inline=1',
				'params'=>array(
					':filemime'=>'%'.$type.'%',
				),
			),
		));

		$view = ($type == 'image') ? 'images' : 'files';
		$this->render($view, array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionInlineUpload()
	{
		$this->layout = '/layouts/browser';
		$private = false;
		$file = Yii::app()->getModule('files')->file->upload('upload', $private);
		$model = Files::saveFile($file, $private);
		$model->inline = 1;
		$model->update();

		$CKECallback = isset($_GET['CKEditorFuncNum']) ? $_GET['CKEditorFuncNum'] : '';

		if ($model)
		{
			// Minified version of the document.domain automatic fix script (#1919).
	// The original script can be found at _dev/domain_fix_template.js
			echo <<<EOF
			<script type="text/javascript">
			(function(){var d=document.domain;while (true){try{var A=window.parent.document.domain;break;}catch(e) {};d=d.replace(/.*?(?:\.|$)/,'');if (d.length==0) break;try{document.domain=d;}catch (e){break;}}})();
EOF;

			$rpl = array( '\\' => '\\\\', '"' => '\\"' ) ;
			echo 'window.parent.CKEDITOR.tools.callFunction("'. $CKECallback. '","'. strtr($model->url, $rpl). '");' ; 
			echo '</script>';
	  		exit;
			// $this->render('upload', array(
			// 	'model'=>$model,
			// ));	
			exit;
		}

		echo 'Error uploading file. Please try again.';
		exit;
	}

	public function actionCreate()
    {
        $model=new Files;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_FILES) && !empty($_FILES))
        {
        	$private = false;
        	$attribute = 'image';

			$file = Yii::app()->getModule('files')->file->upload(CHtml::resolveName($model, $attribute), $private);
			$fileModel = Files::saveFile($file, $private);

			if ($fileModel) {
				// there's a bug when uploading a file inside the files model, it'll try to upload twice
				// and product an error on the second attempt.
				Flash::getFlashes('error');
				$this->redirect(array('/files/default/view', 'id'=>$fileModel->id));
			}
			else {
				$model = new Files;
				Flash::setFlash('error', 'Error uploading file. Please try again.');
			}
        }
        // if(isset($_POST['Files']))
        // {
        //     // $model->attributes=$_POST['Files'];
        //     // if($model->save())
        //     //     $this->redirect(array('view','id'=>$model->id));
        // }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (isset($_FILES) && !empty($_FILES))
        {
        	$private = false;
        	$attribute = 'image';

			$file = Yii::app()->getModule('files')->file->upload(CHtml::resolveName($model, $attribute), $private);
			if ($file && $file->exists)
			{
				$model->service = $file->service;
				$model->filename = $file->basename;
				$model->filepath = $file->filepath;
				$model->filesize = $file->getSize(false);
				$model->filemime = $file->mimeType;
				$model->filetype = $file->filetype;
				$model->private = ($private) ? 1 : 0;
				$model->status = 1;
				
				if ($model->save()) {
					// there's a bug when uploading a file inside the files model, it'll try to upload twice
					// and produce an error on the second attempt.
					Flash::getFlashes('error');
					$this->redirect(array('/files/default/view', 'id'=>$model->id));
				}
				else {
					Flash::setFlash('error', 'Error uploading file. Please try again.');
				}
			}
			else {
				Flash::setFlash('error', 'Error uploading file. Please try again.');
			}
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    // update files to add getID3 info
    public function actionGetInfo()
    {
    	$models = Files::model()->limit(20)->published()->findAll(array('condition'=>'(filemime LIKE "audio%" OR filemime LIKE "video%") AND info IS NULL'));
    	// $models = Files::model()->limit(20)->findAll(array('condition'=>'(filemime LIKE "video%") AND info IS NULL'));

    	foreach ($models as $model)
    	{
    		$file = Yii::app()->getModule('files')->file->retrieve($model->filepath, ($model->service=='s3'));
    		@unlink($file->realpath);
    		$model->setInfo($file->info);
    		$model->save();
    	}

    	echo 'Records retrieved: '.count($models);
    	exit;
    }

    public function actionResetTimes()
    {
    	$classes = array('BedTrack', 'ClinicVideo', 'LessonVideo', 'Track', 'TrackPlusyou', 'UserPlusyou');

    	foreach ($classes as $class)
    	{
    		$models = $class::model()->findAll(array('condition'=>'time IS NULL OR time = ""'));

		foreach ($models as $model)
			$model->save();
    	}
    }

    public function loadModel($id)
    {
        $model=Files::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}