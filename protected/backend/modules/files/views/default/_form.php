<?php
/* @var $this FilesController */
/* @var $model Files */
/* @var $form CActiveForm */
?>

<div class="form form-horizontal">

<?php $form=$this->beginWidget('ActiveForm', array(
    'id'=>'files-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
    'htmlOptions'=>array(
        'enctype' => 'multipart/form-data'
    ),
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model,'image'); ?>
        <div class="controls">
            <?php echo $form->fileField($model,'image',array('size'=>10,'maxlength'=>10)); ?>
            <?php echo $form->error($model,'image'); ?>
        </div>
    </div>

    <div class="form-actions buttons">
        <?php echo Html::submitButton($model->isNewRecord ? 'Upload' : 'Save'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->