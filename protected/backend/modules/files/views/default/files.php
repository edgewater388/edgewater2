<div class="page-header">
	<h1>Files</h1>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'files-grid',
	'dataProvider'=>$dataProvider,
	// 'filter'=>$model,
	'enableSorting'=>false,
	'columns'=>array(
		'id',
		array(
			'header'=>'Filename',
			'value'=>'"<a href=\"".$data->url."\" data-toggle=\"populate-url\">".$data->filename."</a>"',
			'type'=>'raw',
		),
		// 'filename',
		'url',
		// 'filepath',
		'filemime',
		'status',
		// 'tagline',
		// 'summary',
		// 'description',
		/*
		'facebook_url',
		'twitter',
		'opentable_url',
		'reservation_url',
		'menu_fid',
		'created',
		'updated',
		*/
		// array(
		// 	'class'=>'CButtonColumn',
		// ),
	),
)); ?>

<script>
	jQuery(document).ready(function($) {
		window.resizeTo(1050, 480);

		$('[data-toggle="populate-url"]').on('click', function(e) {
			var url = $(this).attr('href');
			window.opener.CKEDITOR.tools.callFunction(<?php echo isset($_GET['CKEditorFuncNum']) ? $_GET['CKEditorFuncNum'] : null; ?>,url);
			window.close();

			e.preventDefault();
		});
	});
</script>