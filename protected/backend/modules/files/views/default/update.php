<?php
/* @var $this FilesController */
/* @var $model Files */

$this->breadcrumbs=array(
    'Files'=>array('index'),
    'Create',
);

$this->menu=array(
    // array('label'=>'List Files', 'url'=>array('index')),
    // array('label'=>'Manage Files', 'url'=>array('admin')),
);
?>

<div class="page-header">
	<h1>Upload File</h1>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>