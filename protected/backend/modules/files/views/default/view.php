<?php
/* @var $this FilesController */
/* @var $model Files */

$this->breadcrumbs=array(
    'Files'=>array('index'),
    $model->id,
);

$this->menu=array(
    // array('label'=>'List Files', 'url'=>array('index')),
    // array('label'=>'Create Files', 'url'=>array('create')),
    array('label'=>'Edit', 'url'=>array('update', 'id'=>$model->id)),
    // array('label'=>'Delete Files', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    // array('label'=>'Manage Files', 'url'=>array('admin')),
);
?>

<div class="page-header">
    <h1>File #<?php echo $model->id; ?></h1>
</div>

<div class="list-wrapper">
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
            'id',
            'user_id',
            'service',
            'private',
            'absoluteUrl:url',
            'filename',
            'filepath',
            'filemime',
            'filesize',
            'filetype',
            'status',
            'created',
            'updated',
        ),
    )); ?>
</div>