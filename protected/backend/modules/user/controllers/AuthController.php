<?php

class AuthController extends Controller
{
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'roles'=>array('accessBackend'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{
		$auth=Yii::app()->authManager;
		
		$auth->clearAll();
		$auth->createOperation('createPost','create a post');		
		$auth->createOperation('readPost','read a post');
		$auth->createOperation('updatePost','update a post');
		$auth->createOperation('deletePost','delete a post');
		$auth->createOperation('accessBackend','access backend');
		 
		$bizRule='return Yii::app()->user->id==$params["post"]->authID;';
		$task=$auth->createTask('updateOwnPost','update a post by author himself',$bizRule);
		$task->addChild('updatePost');

		// $bizRule='return Yii::app()->user->id==$params["corder"]->user_id;';
		// $task=$auth->createTask('viewOwnOrder','view own order',$bizRule);

		// Rule for playing tracks: Must exist in purchase table
		//$bizRule='return Yii::app()->user->id==$params["purchases"]->user_id;';
		//$task=$auth->createTask('play_track','play a track',$bizRule);
		//$task->addChild('play_all_tracks');
		 
		$role=$auth->createRole('reader');
		$role->addChild('readPost');
		 
		$role=$auth->createRole('author');
		$role->addChild('reader');
		$role->addChild('createPost');
		$role->addChild('updateOwnPost');
		 
		$role=$auth->createRole('editor');
		$role->addChild('reader');
		$role->addChild('updatePost');
		$role->addChild('accessBackend');
		//$role->addChild('play_all_tracks');
		 
		$role=$auth->createRole('admin');
		$role->addChild('editor');
		$role->addChild('author');
		$role->addChild('deletePost');

		// Assign permissions
		$auth->assign('admin', 1);
		$auth->assign('admin', 5);

		$this->render('index',array());
		return;

	}

}
