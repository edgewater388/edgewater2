<?php

class UsersController extends BaseController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow anonymous users to register
				'actions'=>array('login'),
				'users'=>array('*'),
			),
			array('allow',
				'actions'=>array('logout'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('view', 'index', 'create', 'update','delete','reports','search','autocomplete','api','importHolds'),
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function beforeAction($action)
	{
		$this->pageTitle = 'User Accounts';

		return parent::beforeAction($action);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = Users::model()->with('profile')->findByPk($id);
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Users;
		$model->scenario = 'create';
		$model->profile = new Profile;
		$model->profile->scenario = 'create';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];
			$model->profile->attributes = $_POST['Profile'];
			$model->terms_accepted = 0;

			$valid = $model->validate();
			$valid &= $model->profile->validate();

			$model->addErrors($model->profile->getErrors());

			if($valid)
			{
				$model->save(false);
				if(isset($_POST['roles']))
					$this->assignRoles($_POST['roles'], $model->id);

				// $model->profile->birthday = $model->profile->formatBirthday($_POST['Profile']['dob']);
				$model->profile->user_id = $model->id;
				$model->profile->status = $model->status;
				$model->profile->save();

				if (isset($_POST['notify']))
					Flash::setFlash('info', '@todo: Send email notification');

				Flash::setFlash('success', 'User <a href="'.Yii::app()->createUrl('/user/users/view',array('id'=>$model->id)).'">account</a> has been created');
				//$this->redirect(array('view','id'=>$model->id));

				// view user, unless 'create & add another' button is pressed
				if (isset($_POST['yt1']))
					$this->redirect(array('create'));
				else
					$this->redirect(array('view', 'id'=>$model->id));

			} else {
				Flash::setFlash('error', 'User account could not be created');
			}
		}
		else
		{
			// Set default radio options
			$model->status = 1;
		}

		$this->render('create',array(
			'model'=>$model,
			'roles'=>$this->getAuthRoles(),
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->scenario = 'adminUpdate';
		$model->profile->scenario = 'adminUpdate';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];
			$model->profile->attributes = $_POST['Profile'];

			if($model->validate())
			{
				$model->save(false);
				$roles = (isset($_POST['roles'])) ? $_POST['roles'] : array();
				$this->assignRoles($roles, $model->id);

        // Currently we don't ask the users for their birthday.  There is a DB column for this.
				//$model->profile->birthday = $model->profile->formatBirthday($_POST['Profile']['dob']);
				//$model->profile->user_id = $model->id;
				$model->profile->save();

				Flash::setFlash('success', 'User account has been updated');
				$this->redirect(array('view', 'id'=>$model->id));
			} else {
				Flash::setFlash('error', 'User account could not be updated');
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'roles'=>$this->getAuthRoles(),
			'assignedRoles'=>$this->getAssignedRoles($id),
		));
	}

	public function actionReports()
	{
		$this->layout='//layouts/reports';
		$model=new Users('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['Users']))
			$model->attributes=$_GET['Users'];

		$this->render('reports',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex($export=false)
	{
		$model=new Users('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['Users']))
			$model->attributes=$_GET['Users'];

		if ($export)
			$model->export();

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Users::model()->with('profile')->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout = '//layouts/main';
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
			// display the login form
		if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
			$model->email = 'davidtiede@gmail.com';
			$model->password = 'gl0bal';
		}
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionSearch()
	{
		$results = array();

		$className = 'Users';

		$searchModel = new $className('search');
		$searchModel->unsetAttributes();
		if(isset($_GET[$className]))
			$searchModel->attributes=$_GET[$className];

		$attributes = $searchModel->attributes;
		$attributes['name'] = isset($_GET[$className]['name']) ? $_GET[$className]['name'] : null;
		$attributes['name_or_email'] = isset($_GET[$className]['name_or_email']) ? $_GET[$className]['name_or_email'] : null;

		// Save the state for page refresh..
		Yii::app()->user->setState('Search['.$className.']', $attributes);

		$dataProvider = $searchModel->with('profile')->search();
		$data = $dataProvider->getData();

		foreach ($data as $model)
		{
			$results[] = array(
				'id'=>$model->id,
				'title'=>$model->profile->name.' ('.$model->email.')',
				'url'=>Yii::app()->createUrl('/user/users/view',array('id'=>$model->id)),
				'description'=>$model->email,
			);
		}

		echo CJSON::encode($results);
		exit;
	}

	public function actionImportAddress()
	{
		if (isset($_FILES['import']))
		{
			$saved = 0;

			$filepath = $_FILES['import']['tmp_name'];
			$rows = Import::parseCSV($filepath);
			
			foreach ($rows as $row)
			{
				$user_id = Yii::app()->db->createCommand('SELECT id FROM users WHERE email = :email')->bindParam(':email', $row['email'])->queryScalar();

				if ($user_id)
				{
					if ($customer = Customer::model()->byAttribute('user_id', $user_id)->find())
					{
						$customer->scenario = 'import';
						$customer->street = $row['billing_address_1'];
						$customer->street1 = $row['billing_address_2'];
						$customer->city = $row['billing_city'];
						$customer->state = $row['billing_state'];
						$customer->country = $row['billing_country'];
						try {
							if ($customer->save())
								$saved++;
						} catch (Exception $e) {
							die(var_dump($row['email']));
						}
					}
				}
			}

			echo 'Saved '.$saved.'/'.count($rows).' customer profiles';
			exit;
		}

		$this->render('import');
	}

	/*
	// on hold accounts
	DROP VIEW IF EXISTS customers_on_hold;
CREATE VIEW customers_on_hold AS
(SELECT p.id, p.post_date,
	(SELECT a1.meta_value FROM wp_postmeta a1 WHERE a1.post_id = p.id AND a1.meta_key = "_billing_period" ) billing_period,
	(SELECT a2.meta_value FROM wp_postmeta a2 WHERE a2.post_id = p.id AND a2.meta_key = "_billing_interval" ) billing_interval,
	(SELECT a3.meta_value FROM wp_postmeta a3 WHERE a3.post_id = p.id AND a3.meta_key = "_customer_user" ) customer_user,
	(SELECT a4.meta_value FROM wp_postmeta a4 WHERE a4.post_id = p.id AND a4.meta_key = "_billing_first_name" ) billing_first_name,
	(SELECT a5.meta_value FROM wp_postmeta a5 WHERE a5.post_id = p.id AND a5.meta_key = "_billing_last_name" ) billing_last_name,
	(SELECT a7a.meta_value FROM wp_postmeta a7a WHERE a7a.post_id = p.id AND a7a.meta_key = "_billing_address_1" ) billing_address_1,
	(SELECT a7b.meta_value FROM wp_postmeta a7b WHERE a7b.post_id = p.id AND a7b.meta_key = "_billing_address_2" ) billing_address_2,
	(SELECT a7c.meta_value FROM wp_postmeta a7c WHERE a7c.post_id = p.id AND a7c.meta_key = "_billing_city" ) billing_city,
	(SELECT a7d.meta_value FROM wp_postmeta a7d WHERE a7d.post_id = p.id AND a7d.meta_key = "_billing_country" ) billing_country,
	(SELECT a7e.meta_value FROM wp_postmeta a7e WHERE a7e.post_id = p.id AND a7e.meta_key = "_billing_state" ) billing_state,
	(SELECT a6.meta_value FROM wp_postmeta a6 WHERE a6.post_id = p.id AND a6.meta_key = "_billing_postcode" ) billing_postcode,
	(SELECT a7.meta_value FROM wp_postmeta a7 WHERE a7.post_id = p.id AND a7.meta_key = "_billing_email" ) billing_email,
	(SELECT a8.meta_value FROM wp_postmeta a8 WHERE a8.post_id = p.id AND a8.meta_key = "_schedule_trial_end" ) schedule_trial_end,
	(SELECT a9.meta_value FROM wp_postmeta a9 WHERE a9.post_id = p.id AND a9.meta_key = "_schedule_next_payment" ) schedule_next_payment,
	(SELECT b1.meta_value FROM wp_postmeta b1 WHERE b1.post_id = p.id AND b1.meta_key = "_schedule_end" ) schedule_end,
	(SELECT b2.meta_value FROM wp_postmeta b2 WHERE b2.post_id = p.id AND b2.meta_key = "_requires_manual_renewal" ) requires_manual_renewal,
	(SELECT b3.meta_value FROM wp_postmeta b3 WHERE b3.post_id = p.id AND b3.meta_key = "_order_total" ) order_total,
	(SELECT b5.meta_value FROM wp_postmeta b5 WHERE b5.post_id = p.id AND b5.meta_key = "_wc_authorize_net_cim_credit_card_customer_id" ) customer_id,
	(SELECT b4.meta_value FROM wp_postmeta b4 WHERE b4.post_id = p.id AND b4.meta_key = "_wc_authorize_net_cim_credit_card_payment_token" ) customer_payment_id
	FROM wp_posts p
	WHERE p.post_type = "shop_subscription"
	AND p.post_status = "wc-on-hold")
	*/

	public function actionImportHolds()
	{
		if (isset($_FILES['import']))
		{
			$saved = 0;

			$filepath = $_FILES['import']['tmp_name'];
			$users = Import::parseCSV($filepath);

			$filepath = $_FILES['subscriptions']['tmp_name'];
			$rows = Import::parseCSV($filepath);
			
			foreach ($rows as $row)
			{
				$user_id = Yii::app()->db->createCommand('SELECT id FROM users WHERE email = :email')->bindParam(':email', $row['billing_email'])->queryScalar();

				if ($user_id)
				{
					if ($customer = Customer::model()->byAttribute('user_id', $user_id)->find())
					{
						// echo $customer->user->email.'<br>';
						$saved++;

					// 	$customer->scenario = 'import';
					// 	$customer->street = $row['billing_address_1'];
					// 	$customer->street1 = $row['billing_address_2'];
					// 	$customer->city = $row['billing_city'];
					// 	$customer->state = $row['billing_state'];
					// 	$customer->country = $row['billing_country'];
					// 	try {
					// 		if ($customer->save())
					// 			$saved++;
					// 	} catch (Exception $e) {
					// 		die(var_dump($row['email']));
					// 	}
					}
					else
					{
						echo '<pre>';
						print_r($row);
						echo '</pre>';
						echo '<br>';
					}


					if ($membership = Membership::model()->byAttribute('user_id', $user_id)->find())
					{
						// echo $membership->valid_to.'<br>';
					}
					else
					{
						echo 'no membership<br>';
					}

					if (!$customer || !$membership)
						echo '<br>--<br>';
				}
				else
				{
					die(var_dump($row));
				}
			}

			echo 'Saved '.$saved.'/'.count($rows).' customer profiles';
			exit;
		}

		$this->render('import');
	}

	public function actionImport()
	{
		set_time_limit(24*60*60);

	/*
	*** USERS ***
	*** user 11515 has an issue with the subscription and serialized data **
	SELECT users.id user_id, users.user_pass `password`, users.user_email email, users.user_registered created
, m1.meta_value authorize_customer_profile_id, m2.meta_value first_name, m3.meta_value last_name, m4.meta_value billing_first_name, m5.meta_value billing_last_name, m8.meta_value billing_zipcode, m6.meta_value subscriptions,
	(SELECT m7.meta_value
		FROM wp_usermeta m7
		WHERE m7.meta_key = "_wc_authorize_net_cim_credit_card_payment_tokens"
		AND m7.meta_value IS NOT NULL
		AND m7.user_id = m1.user_id
		LIMIT 0,1) authorize_card,
	(SELECT m7.meta_value
		FROM wp_usermeta m7
		WHERE m7.meta_key = "_wc_authorize_net_cim_credit_card_payment_tokens"
		AND m7.meta_value IS NOT NULL
		AND m7.user_id = m1.user_id
		LIMIT 1,1) authorize_card_1,
	(SELECT m7.meta_value
		FROM wp_usermeta m7
		WHERE m7.meta_key = "_wc_authorize_net_cim_credit_card_payment_tokens"
		AND m7.meta_value IS NOT NULL
		AND m7.user_id = m1.user_id
		LIMIT 2,1) authorize_card_2,
	(SELECT m7.meta_value
		FROM wp_usermeta m7
		WHERE m7.meta_key = "_wc_authorize_net_cim_credit_card_payment_tokens"
		AND m7.meta_value IS NOT NULL
		AND m7.user_id = m1.user_id
		LIMIT 3,1) authorize_card_3,
	(SELECT m7.meta_value
		FROM wp_usermeta m7
		WHERE m7.meta_key = "_wc_authorize_net_cim_credit_card_payment_tokens"
		AND m7.meta_value IS NOT NULL
		AND m7.user_id = m1.user_id
		LIMIT 4,1) authorize_card_4
	FROM wp_users users
	LEFT JOIN wp_usermeta m1 ON m1.user_id = users.id AND m1.meta_key = "wc_authorize_net_cim_customer_profile_id"
	LEFT JOIN wp_usermeta m2 ON m2.user_id = users.id AND m2.meta_key = "first_name"
	LEFT JOIN wp_usermeta m3 ON m3.user_id = users.id AND m3.meta_key = "last_name"
	LEFT JOIN wp_usermeta m4 ON m4.user_id = users.id AND m4.meta_key = "billing_first_name"
	LEFT JOIN wp_usermeta m5 ON m5.user_id = users.id AND m5.meta_key = "billing_last_name"
	LEFT JOIN wp_usermeta m6 ON m6.user_id = users.id AND m6.meta_key = "_wp_woocommerce_subscriptions"
	LEFT JOIN wp_usermeta m8 ON m8.user_id = users.id AND m8.meta_key = "billing_postcode"
	ORDER BY users.id ASC
	
	*** COUNTS ***
SELECT count(*) count, o2.meta_value `status`, o3.meta_value period, o4.meta_value recurring_amount, o5.meta_value length, o6.meta_value `interval`
	FROM wp_woocommerce_order_itemmeta o1 
	INNER JOIN wp_woocommerce_order_itemmeta o2 ON o2.order_item_id = o1.order_item_id AND o2.meta_key = "_wcs_migrated_subscription_status"
	INNER JOIN wp_woocommerce_order_itemmeta o3 ON o3.order_item_id = o1.order_item_id AND o3.meta_key = "_wcs_migrated_subscription_period"
	INNER JOIN wp_woocommerce_order_itemmeta o4 ON o4.order_item_id = o1.order_item_id AND o4.meta_key = "_wcs_migrated_subscription_recurring_amount"
	INNER JOIN wp_woocommerce_order_itemmeta o5 ON o5.order_item_id = o1.order_item_id AND o5.meta_key = "_wcs_migrated_subscription_length"
		INNER JOIN wp_woocommerce_order_itemmeta o6 ON o6.order_item_id = o1.order_item_id AND o6.meta_key = "_wcs_migrated_subscription_interval"
	WHERE o1.meta_key = "_wcs_migrated_recurring_line_subtotal"
	AND o2.meta_value = "active"
	GROUP BY o1.meta_value, o3.meta_value, o4.meta_value, o5.meta_value, o6.meta_value

	*** SUBSCRIPTIONS ***
SELECT p.id, p.post_date,
	(SELECT a1.meta_value FROM wp_postmeta a1 WHERE a1.post_id = p.id AND a1.meta_key = "_billing_period" ) billing_period,
	(SELECT a2.meta_value FROM wp_postmeta a2 WHERE a2.post_id = p.id AND a2.meta_key = "_billing_interval" ) billing_interval,
	(SELECT a3.meta_value FROM wp_postmeta a3 WHERE a3.post_id = p.id AND a3.meta_key = "_customer_user" ) customer_user,
	(SELECT a4.meta_value FROM wp_postmeta a4 WHERE a4.post_id = p.id AND a4.meta_key = "_billing_first_name" ) billing_first_name,
	(SELECT a5.meta_value FROM wp_postmeta a5 WHERE a5.post_id = p.id AND a5.meta_key = "_billing_last_name" ) billing_last_name,
	(SELECT a6.meta_value FROM wp_postmeta a6 WHERE a6.post_id = p.id AND a6.meta_key = "_billing_postcode" ) billing_postcode,
	(SELECT a7.meta_value FROM wp_postmeta a7 WHERE a7.post_id = p.id AND a7.meta_key = "_billing_email" ) billing_email,
	(SELECT a8.meta_value FROM wp_postmeta a8 WHERE a8.post_id = p.id AND a8.meta_key = "_schedule_trial_end" ) schedule_trial_end,
	(SELECT a9.meta_value FROM wp_postmeta a9 WHERE a9.post_id = p.id AND a9.meta_key = "_schedule_next_payment" ) schedule_next_payment,
	(SELECT b1.meta_value FROM wp_postmeta b1 WHERE b1.post_id = p.id AND b1.meta_key = "_schedule_end" ) schedule_end,
	(SELECT b2.meta_value FROM wp_postmeta b2 WHERE b2.post_id = p.id AND b2.meta_key = "_requires_manual_renewal" ) requires_manual_renewal,
	(SELECT b3.meta_value FROM wp_postmeta b3 WHERE b3.post_id = p.id AND b3.meta_key = "_order_total" ) order_total,
	(SELECT b5.meta_value FROM wp_postmeta b5 WHERE b5.post_id = p.id AND b5.meta_key = "_wc_authorize_net_cim_credit_card_customer_id" ) customer_id,
	(SELECT b4.meta_value FROM wp_postmeta b4 WHERE b4.post_id = p.id AND b4.meta_key = "_wc_authorize_net_cim_credit_card_payment_token" ) customer_payment_id
	FROM wp_posts p
	WHERE p.post_type = "shop_subscription"
	AND p.post_status = "wc-active"

	******************************************************************
	**** NOTE : WHEN EXPORTING, SET NULL TO EMPTY AND ESCAPE TO "
	******************************************************************
*/ 
	
		function v($v, $t=true) {
			echo '<pre>';
			print_r($v);
			echo '</pre>';

			if ($t)
				exit;
		}

		function wp_unserialize($s) {
			return unserialize(unserialize(serialize($s)));
		}
		
		function wp_str_to_array($s) {
			return is_array($s) ? $s : array();
		}

		function wp_array_get_value($a, $k) {
			return array_key_exists($k, $a) ? $a[$k] : null;
		}

		function wp_get_subscription($subscriptions, $customer_user)
		{
			foreach ($subscriptions as $subscription)
			{
				if ($subscription['customer_user'] == $customer_user)
					return $subscription;
			}
		}

		$statuses = array();

		if (Yii::app()->user->id !== '1')
			throw new HttpException('403', 'Access denied');

		if (isset($_FILES['import']))
		{
			$filepath = $_FILES['import']['tmp_name'];
			$rows = Import::parseCSV($filepath);

			$filepath = $_FILES['subscriptions']['tmp_name'];
			$results = Import::parseCSV($filepath);

			$subscriptions = array();
			foreach ($results as $result)
				$subscriptions[$result['customer_user']] = $result;
			
			$subscriptionCount = 0;

			foreach ($rows as $row)
			{
				$user = new Users;
				$user->email = $row['email'];
				$user->password = $row['password'];
				$user->created = $row['created'];
				$user->settings = serialize(array('imported'=>'wordpress'));
				$user->status = 1;
				if (!$user->save())
					v($user);

				$profile = new Profile;
				$profile->user_id = $user->id;
				$profile->first_name = $row['first_name'];
				$profile->last_name = $row['last_name'];
				$profile->status = 1;
				if (!$profile->save())
					v($profile);
			
				if ($subscription = wp_get_subscription($subscriptions, $row['user_id']))
				{
					$payment_profiles = CMap::mergeArray(
						wp_str_to_array(wp_unserialize($row['authorize_card'])), 
						wp_str_to_array(wp_unserialize($row['authorize_card_1'])), 
						wp_str_to_array(wp_unserialize($row['authorize_card_2'])), 
						wp_str_to_array(wp_unserialize($row['authorize_card_3'])), 
						wp_str_to_array(wp_unserialize($row['authorize_card_4'])));

					$customer_id = $subscription['customer_id'];
					$customer_payment_id = $subscription['customer_payment_id'];
					$payment_profile = array();

					if (!$profile->first_name || !$profile->last_name)
					{
						$profile->first_name = $subscription['billing_first_name'];
						$profile->last_name = $subscription['billing_last_name'];
						$profile->update();
					}

					if (array_key_exists($customer_payment_id, $payment_profiles))
					{
						$payment_profile = $payment_profiles[$customer_payment_id];

						if (!$customer_id)
							$customer_id = $payment_profile['customer_profile_id'];
					}

					if ($customer_id && $customer_payment_id)
					{
						$customer = new Customer;
						$customer->scenario = 'import';
						$customer->user_id = $user->id;
						$customer->customer_profile_id = $customer_id;
						$customer->customer_payment_profile_id = $customer_payment_id;
						$customer->first_name = $subscription['billing_first_name'];
						$customer->last_name = $subscription['billing_last_name'];
						$customer->card_number = wp_array_get_value($payment_profile, 'last_four');
						$customer->card_month = wp_array_get_value($payment_profile, 'exp_month');
						$customer->card_year = '20'.substr(wp_array_get_value($payment_profile, 'exp_year'), 0, 2);
						$customer->card_type = wp_array_get_value($payment_profile, 'card_type');
						$customer->zipcode = $subscription['billing_postcode'];
						$customer->status = 1;
						if (!$customer->save())
							v($customer);
					}

					$plan_id = null;
					if ($subscription['order_total'] == '57')
						$plan_id = 1;
					elseif ($subscription['order_total'] == '31')
						$plan_id = 1;
					elseif ($subscription['order_total'] == '5')
						$plan_id = 2;

					$interval = null;
					if ($subscription['billing_period'] == 'month')
						$interval = 'monthly';
					elseif ($subscription['billing_period'] == 'day')
						$interval = 'daily';

					$membership = new Membership;
					$membership->user_id = $user->id;
					$membership->plan_id = $plan_id;
					$membership->subtotal = $subscription['order_total'];
					$membership->tax = 0;
					$membership->total = $subscription['order_total'];
					$membership->join_date = date('Y-m-d', strtotime($subscription['post_date']));
					$membership->trial_end_date = null;
					$membership->prev_billing_date = null;
					$membership->next_billing_date = $subscription['schedule_next_payment'] ? date('Y-m-d', strtotime($subscription['schedule_next_payment'])) : null;
					$membership->interval = $interval;
					$membership->auto_renew = $subscription['schedule_next_payment'] ? 1 : 0;
					$membership->valid_from = date('Y-m-d');
					$membership->valid_to = $subscription['schedule_next_payment'] ? $membership->next_billing_date : date('Y-m-d', strtotime($subscription['schedule_end']));
					$membership->status = 1;
					if (!$membership->save())
						v($membership);
					
					$subscriptionCount++;
				}
			}

			var_dump($subscriptionCount);
			die('import');
		}

		$this->render('import');
	}

	protected function getAuthRoles()
	{
		$auth = Yii::app()->AuthManager;
		if ($auth)
			$roles = $auth->getRoles();

		// Prevent super admin from being added from the ui
		if (isset($roles['super admin']))
			unset($roles['super admin']);

		return ($roles) ? $roles : array();
	}

	protected function assignRoles($roles=array(), $user_id)
	{
		if (!is_array($roles))
			$roles = (array) $roles;

		// Retrieve all auth roles so that any roles that are not checked off (ie. not in $roles)
		// can be removed
		$authRoles = $this->getAuthRoles();

		// Prevent roles from being added twice (causing a duplicate entry error)
		$assignedRoles = $this->getAssignedRoles($user_id);

		$auth = Yii::app()->AuthManager;
		foreach ($authRoles as $roleName => $role)
		{
			// Assign role, only if role has not be assigned previously
			if (in_array($roleName, $roles))
			{
				if (!in_array($roleName, $assignedRoles))
					$auth->assign($roleName, $user_id);
			}
			// Remove role, only if role had previously been assigned
			else if (in_array($roleName, $assignedRoles))
			{
				$auth->revoke($roleName, $user_id);
			}
			else {
				// Nothing to do here
			}
		}
	}

	protected function getAssignedRoles($user_id)
	{
		$auth = Yii::app()->AuthManager;

		$assignedRoles = array();
		if ($auth) {
			$assignments = $auth->getAuthAssignments($user_id);
			if ($assignments)
				foreach ($assignments as $itemName => $assignment) $assignedRoles[] = $itemName;
		}

		return $assignedRoles;
	}
}
