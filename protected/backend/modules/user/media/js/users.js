$(document).ready(function() { 

	$('#generate-password').click(function() {
		var password = generate_password(6,true,true,true,false);
		$('input[name="Users[password]"]').val(password);
		$('input[name="Users[new_password]"]').val(password);
		return false;
	});
	
	function generate_password(length, lowercase, uppercase, numbers, symbols) {
		var str = '';
		if (length == null || length < 4)
			length = 4;
		if (lowercase)
			str += 'abcdefghijklmnopqrstuvwxyz';
		if (uppercase)
			str += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		if (numbers)
			str += '0123456789';
		if (symbols)
			str += '!@#$%&*';
		
		var password = '';
		for (i=0; i<length; i++) {
			var random = Math.random()*1000;
			password += str.charAt(random % str.length);
		}
		
		return password;
	}

}); 