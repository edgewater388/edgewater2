<?php
/* @var $this ProfileController */
/* @var $model Profile */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'id'=>'profile-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype' => 'multipart/form-data'),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'street'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'street',array('size'=>60,'maxlength'=>128)); ?>
			<?php echo $form->error($model,'street'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'street2'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'street2',array('size'=>60,'maxlength'=>128)); ?>
			<?php echo $form->error($model,'street2'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'city'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'city',array('size'=>60,'maxlength'=>128)); ?>
			<?php echo $form->error($model,'city'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'state'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'state',array('size'=>60,'maxlength'=>64)); ?>
			<?php echo $form->error($model,'state'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'country'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'country',array('size'=>2,'maxlength'=>2)); ?>
			<?php echo $form->error($model,'country'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'zip_code'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'zip_code',array('size'=>12,'maxlength'=>12)); ?>
			<?php echo $form->error($model,'zip_code'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'phone'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'phone',array('size'=>32,'maxlength'=>32)); ?>
			<?php echo $form->error($model,'phone'); ?>
		</div>
	</div>

	<div class="form-actions buttons">
		<?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->