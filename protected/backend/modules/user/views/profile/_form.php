<?php
/* @var $this ProfileController */
/* @var $model Profile */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'id'=>'profile-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype' => 'multipart/form-data'),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php /*
	<div class="form-group">
		<?php echo $form->labelEx($model,'gender'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'gender',array('size'=>1,'maxlength'=>1)); ?>
			<?php echo $form->error($model,'gender'); ?>
		</div>
	</div>
	*/ ?>


	<div class="form-group">
		<?php echo $form->labelEx($model,'photo'); ?>
		<div class="controls">
			<?php echo $form->fileField($model,'photo',array('size'=>10,'maxlength'=>10)); ?>
			<?php echo $form->error($model,'photo'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'banner'); ?>
		<div class="controls">
			<?php echo $form->fileField($model,'banner',array('size'=>10,'maxlength'=>10)); ?>
			<?php echo $form->error($model,'banner'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'description'); ?>
		<div class="controls">
			<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
			<?php echo $form->error($model,'description'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'facebook'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'facebook',array('size'=>60,'maxlength'=>64)); ?>
			<?php echo $form->error($model,'facebook'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'twitter'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'twitter',array('size'=>60,'maxlength'=>64)); ?>
			<?php echo $form->error($model,'twitter'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'youtube'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'youtube',array('size'=>60,'maxlength'=>64)); ?>
			<?php echo $form->error($model,'youtube'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'status'); ?>
		<div class="controls">
			<?php echo $form->statusField($model,'status'); ?>
			<?php echo $form->error($model,'status'); ?>
		</div>
	</div>


	<div class="form-actions buttons">
		<?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->