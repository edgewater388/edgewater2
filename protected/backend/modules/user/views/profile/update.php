<?php
/* @var $this ProfileController */
/* @var $model Profile */

$this->breadcrumbs=array(
	'Users'=>array('/user/users/index'),
	$model->name=>array('/user/users/view','id'=>$model->user_id),
	'Update',
);

$this->menu=array(
	// array('label'=>'List Profile', 'url'=>array('index')),
	// array('label'=>'Create Profile', 'url'=>array('create')),
	// array('label'=>'View Profile', 'url'=>array('view', 'id'=>$model->user_id)),
	// array('label'=>'Manage Profile', 'url'=>array('admin')),
);
?>

<div class="page-header">
	<h1><?php echo $model->name; ?></h1>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>