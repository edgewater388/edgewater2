<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('backend.modules.user.media.js').'/users.js'), CClientScript::POS_HEAD); ?>
<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model->profile,'first_name'); ?>
		<div class="controls">
			<?php echo $form->textField($model->profile,'first_name',array('size'=>60,'maxlength'=>128)); ?>
			<?php echo $form->error($model,'first_name'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model->profile,'last_name'); ?>
		<div class="controls">
			<?php echo $form->textField($model->profile,'last_name',array('size'=>60,'maxlength'=>128)); ?>
			<?php echo $form->error($model,'last_name'); ?>
		</div>
	</div>

	<?php /*
	<div class="form-group">
		<?php echo $form->labelEx($model->profile, 'birthday'); ?>
		<div class="controls inline-select">
			<?php echo $form->dropDownList($model->profile,'dob[m]',$model->profile->getMonthOptions('abbreviated'),array()); ?>
			<?php //echo $form->error($profile,'dob[m]'); ?>
			<?php echo $form->dropDownList($model->profile,'dob[d]',$model->profile->getDayOptions(),array()); ?>
			<?php //echo $form->error($profile,'dob[d]'); ?>
			<?php echo $form->dropDownList($model->profile,'dob[y]',$model->profile->getYearOptions(),array()); ?>
			<?php //echo $form->error($profile,'dob[y]'); ?>
		</div>
	</div>
	*/ ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'email'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
			<?php echo $form->error($model,'email'); ?>		
		</div>
	</div>

	<div class="form-group">
		<?php if ($model->isNewRecord): ?>
		<?php echo $form->labelEx($model,'password'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'password',array('size'=>60,'maxlength'=>128)); ?> 
			<div class="note"><a href="#" id="generate-password">Generate Password</a></div>
			<?php echo $form->error($model,'password'); ?>			
		</div>
		<?php else: ?>
		<?php echo $form->labelEx($model,'new_password'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'new_password',array('size'=>60,'maxlength'=>128)); ?> 
			<div class="note"><a href="#" id="generate-password">Generate Password</a></div>
			<?php echo $form->error($model,'new_password'); ?>
		</div>
		<?php endif; ?>
	</div>

	<div class="form-group">
		<?php echo CHtml::label('Roles', ''); ?>
		<div class="controls">	
			<div class="options">
				<?php $rolesOptions = CHtml::listData($roles, 'name', 'name'); ?>
				<?php echo CHtml::checkboxList('roles',$assignedRoles, $rolesOptions); ?>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'status'); ?>
		<div class="controls">
			<div class="options">
				<?php echo $form->statusField($model,'status'); ?>
			</div>
		</div>
		<?php //echo $form->error($model,'status'); ?>
	</div>
	
	<?php if ($model->isNewRecord): ?>
	<div class="form-group">
		<?php echo CHtml::label('', ''); ?>
		<div class="controls">
			<div class="options">
				<?php echo CHtml::checkbox('notify'); ?>
				<?php echo CHtml::label('Notify user of new account', 'notify'); ?>
			</div>
		</div>
		<?php //echo $form->error($model,'status'); ?>
	</div>
	<?php endif; ?>

	<div class="form-actions">
		<?php echo Html::submitButton(($model->isNewRecord)?'Create':'Update', array('class'=>'btn btn-primary')); ?>
		<?php if ($model->isNewRecord): ?>
			<?php echo Html::submitButton('Create & Add Another', array('class'=>'btn btn-primary')); ?>
		<?php endif; ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->