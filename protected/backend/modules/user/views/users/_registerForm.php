<div class="form form-stacked">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

<!-- 	<h3>Account</h3> -->
	
	<div class="yrow">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="yrow">
		<?php echo $form->labelEx($model,'email2'); ?>
		<?php echo $form->textField($model,'email2',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email2'); ?>
	</div>

	<div class="yrow">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="yrow">
		<?php echo $form->labelEx($model, 'password2'); ?>
		<?php echo $form->passwordField($model, 'password2',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model, 'password2'); ?>
	</div>
	
<!-- 	<h3>Profile</h3> -->
	
	<div class="yrow">
		<?php echo $form->labelEx($profile,'firstname'); ?>
		<?php echo $form->textField($profile,'firstname',array('size'=>60,'maxlength'=>50)); ?>
		<?php echo $form->error($profile,'firstname'); ?>
	</div>

	<div class="yrow">
		<?php echo $form->labelEx($profile,'lastname'); ?>
		<?php echo $form->textField($profile,'lastname',array('size'=>60,'maxlength'=>50)); ?>
		<?php echo $form->error($profile,'lastname'); ?>
	</div>

	<div class="yrow">
		<div class="inline-select">
			<?php echo $form->labelEx($profile, 'birthday'); ?>
			<?php echo $form->dropDownList($profile,'dob[m]',$profile->getMonthOptions('abbreviated'),array()); ?>
			<?php //echo $form->error($profile,'dob[m]'); ?>
			<?php echo $form->dropDownList($profile,'dob[d]',$profile->getDayOptions(),array()); ?>
			<?php //echo $form->error($profile,'dob[d]'); ?>
			<?php echo $form->dropDownList($profile,'dob[y]',$profile->getYearOptions(),array()); ?>
			<?php //echo $form->error($profile,'dob[y]'); ?>
		</div>
	</div>

	<div class="yrow">
		<div class="options">
			<?php echo $form->checkBox($model,'terms_accepted'); ?>
			<?php echo CHtml::label('I accept the <a href="">terms and conditions</a>', 'Users_terms_accepted', array('uncheckValue' => '')); ?>
		</div>
		<?php echo $form->error($model,'terms_accepted'); ?>
	</div>

	<div class="yrow buttons">
		<?php echo CHtml::submitButton('Register', array('class'=>'btn info')); ?>
	</div>
	
<div id="fb-root"></div>
<script src="https://connect.facebook.net/en_US/all.js#appId=<?php echo Yii::app()->params['fbAppId']; ?>&xfbml=1"></script>

<fb:login-button
registration-url="/users/fbregister" />

<?php $this->endWidget(); ?>

</div><!-- form -->