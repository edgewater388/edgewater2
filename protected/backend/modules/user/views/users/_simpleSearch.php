<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'action'=>Yii::app()->controller->createUrl('index'),
	'method'=>'get',
)); ?>
	
	<div class="form-group">
		<div class="controls">
			<?php echo $form->textField($model,'name_or_email',array('maxlength'=>128,'class'=>'form-control input-sm', 'autocomplete'=>'off', 'placeholder'=>$model->getAttributeLabel('Search users'))); ?>
			<?php if ($model->name_or_email): ?>
			<a href="/<?php echo Yii::app()->request->pathInfo; ?>" data-toggle="clear"><span class="glyphicon glyphicon-remove-circle"></span></a>
			<?php endif; ?>
		</div>
	</div>

<?php $this->endWidget(); ?>
</div><!-- search-form -->