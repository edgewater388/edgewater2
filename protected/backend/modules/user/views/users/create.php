<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);
?>

<div class="page-header">
	<h1>Create New User</h1>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model,'assignedRoles'=>array(),'roles'=>$roles)); ?>