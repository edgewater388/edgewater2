<br>

<?php $form=$this->beginWidget('ActiveForm', array(
    'htmlOptions'=>array(
		'enctype' => 'multipart/form-data'
	),
)); ?>

<div class="form-group">
	<label for="">Subscriptions</label>
	<div class="controls">
		<?php echo Html::fileField('subscriptions'); ?>
	</div>
</div>

<div class="form-group">
	<label for="">Users</label>
	<div class="controls">
		<?php echo Html::fileField('import'); ?>
	</div>
</div>

<input type="submit" class="btn btn-primary" value="Upload & Import">

<?php $this->endWidget(); ?>