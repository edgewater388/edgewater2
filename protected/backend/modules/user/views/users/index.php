<?php
$this->breadcrumbs=array(
	'Users',
);

$this->toolbarMenu=array();

$this->menu=array(
	array('label'=>'<span class="glyphicon glyphicon-arrow-down"></span> Export', 'url'=>Export::getUrl()),
	array('label'=>'<span class="glyphicon glyphicon-plus-sign"></span> New', 'url'=>array('/user/users/create')),
);
?>

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'filters')); ?>
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
<?php $this->endWidget();?>

<div class="flush">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->with('profile')->search(25),
	'columns'=>array(
		array('class'=>'IdColumn'),
		array(
			'name'=>'Name',
			'value'=>'isset($data->profile) ? $data->profile->name : null',
			'type'=>'raw',
		),
		'email',
		// array(
		// 	'name'=>'created',
		// 	'value'=>'($data->created) ? date("M j, Y", strtotime($data->created)) : \'\'',
		// ),
		// array(
		// 	'name'=>'lastvisit',
		// 	'value'=>'($data->lastvisit) ? date("M j, Y", $data->lastvisit) : \'\'',
		// ),
		array(
			'class'=>'StatusColumn',
		),
		array(
			'class'=>'LinkColumn',
		)
	),
)); ?>
</div>