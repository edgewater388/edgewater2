<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
)); ?>

	<div class="form-group">
		<?php echo $form->textField($model,'email',array('class'=>'form-control input-sm', 'placeholder'=>'Email')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->passwordField($model,'password',array('class'=>'form-control input-sm', 'placeholder'=>'Password')); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="form-actions text-center buttons">
		<?php echo CHtml::submitButton('Login', array('class'=>'btn btn-primary btn-block btn-sm')); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
