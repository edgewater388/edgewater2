<?php
$this->breadcrumbs=array(
	'Users',
);

$this->menu=array(
	array('label'=>'List Users', 'url'=>array('index')),
	array('label'=>'Create Users', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('users-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="page-header">
	<h1>Accounts</h1>
</div>

	<h4>Overview</h4>
	<div class="row-fluid">
		<div class="span3">
			<div class="info-box">
				<div class="description">Total</div>
				<div class="title"><?php echo number_format(Users::model()->count()); ?></div>
			</div>
		</div>
		<div class="span3">
			<div class="info-box">
				<div class="description">Active / 30 days</div>
				<div class="title"><?php echo number_format(Users::model()->count('lastvisit >= :time',array(':time'=>time()-(60*60*24*30)))); ?></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="row-fluid">
		<div class="span2">
			<div class="info-box">
				<div class="description">New / 30 days</div>
				<div class="title"><?php echo number_format(Users::model()->count('created >= :time',array(':time'=>time()-(60*60*24*30)))); ?></div>
			</div>
		</div>
		<div class="span2">
			<div class="info-box">
				<div class="description">New / 7 days</div>
				<div class="title"><?php echo number_format(Users::model()->count('created >= :time',array(':time'=>time()-(60*60*24*7)))); ?></div>
			</div>
		</div>
		<div class="span2">
			<div class="info-box">
				<div class="description">New / 24 hrs</div>
				<div class="title"><?php echo number_format(Users::model()->count('created >= :time',array(':time'=>time()-(60*60*24)))); ?></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>	

	<div class="divider"></div>

<?php return; ?>






<?php // $this->renderPartial('_simpleSearch',array()); ?>
<!-- <?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?> -->
<div class="search-form" style="display:none;">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->with('profiles')->search(),
	//'filter'=>$model,
	'skin'=>false,
	//'cssFile'=>false,
	'itemsCssClass'=>'table table-condensed',
	'pager'=>array('cssFile'=>false,'header'=>''),
	'pagerCssClass'=>'pagination',
	//'template'=>'{summary} {items} {pager}',
	'columns'=>array(
		'id',
		array(
			'name'=>'Name',
			'value'=>'(isset($data->profiles->firstname)) ? "<a href=\"/backend/users/".$data->id."\">".$data->profiles->firstname." ".$data->profiles->lastname."</a>" : \'\'',
			'type'=>'raw',
		),
		'email',
		array(
			'name'=>'created',
			'value'=>'($data->created) ? date("M j, Y", $data->created) : \'\'',
		),
		array(
			'name'=>'lastvisit',
			'value'=>'($data->lastvisit) ? date("M j, Y", $data->lastvisit) : \'\'',
		),
		array(
			'name'=>'status',
			'value'=>'($data->status) ? \'Active\' : \'Inactive\'',
		),
		array(
			'name'=>'terms_accepted',
			'value'=>'($data->terms_accepted) ? \'Yes\' : \'No\'',
		),
	),
)); ?>

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'sidebar')); ?>

<?php $this->endWidget();?>

