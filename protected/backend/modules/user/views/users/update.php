<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->profile->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	// array('label'=>'List Users', 'url'=>array('index')),
	// array('label'=>'<span class="glyphicon glyphicon-eye-open"></span> View', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'<span class="text-red">Delete</span>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<div class="page-header">
	<h1><?php echo $model->profile->first_name.' '.$model->profile->last_name; ?></h1>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model,'roles'=>$roles,'assignedRoles'=>$assignedRoles)); ?>