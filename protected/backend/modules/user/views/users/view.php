<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->profile->name,
);

$this->menu=array(
	// array('label'=>'List Users', 'url'=>array('index')),
	array('label'=>'<span class="glyphicon glyphicon-cog"></span> Edit Account', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'<span class="glyphicon glyphicon-cog"></span> Edit Profile', 'url'=>array('/user/profile/update', 'id'=>$model->id)),
	array('label'=>'<span class="glyphicon glyphicon-cog"></span> Edit Address', 'url'=>array('/user/profile/address', 'id'=>$model->id)),
	// array('label'=>'Delete Account', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	'separator_0'=>array('label'=>'<span class="separator"></span>', 'url'=>null),
	array('label'=>'<span class="glyphicon glyphicon-user"></span> Membership', 'url'=>array('/membership/update', 'user_id'=>$model->id)),
);
?>

<div class="page-header">
	<h1><?php echo $model->profile->name; ?></h1>
</div>

<div class="list-wrapper">
	<h4 class="heading">Account</h4>
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'id',
			'profile.name',
			'email',
			'lastvisit:timeAgo',
			'status:status',
			'created',
			'updated',
		),
	)); ?>
</div>

<div class="list-wrapper">
	<h4 class="heading">Profile</h4>
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model->profile,
		'attributes'=>array(
			'photoF:file',
			'bannerF:file',
			'description:ntext',
			'status:status',
			'updated',
		),
	)); ?>
</div>

<div class="list-wrapper">
	<h4 class="heading">External</h4>
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model->profile,
		'attributes'=>array(
			'website:url',
			'facebook:url',
			'twitter:url',
			'youtube:url',
		),
	)); ?>
</div>

<div class="list-wrapper">
	<h4 class="heading">Address</h4>
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model->profile,
		'attributes'=>array(
			'street',
			'street2',
			'city',
			'state',
			'country',
			'zip_code',
			'phone',
		),
	)); ?>
</div>
