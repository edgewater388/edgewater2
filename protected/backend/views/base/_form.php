<?php foreach ($model->columns as $k=>$column): ?>
	<?php if (!$model->isNewRecord && $column->name == $model->getTableSchema()->primaryKey): ?>
		<?php // don't let primary key be changed ?>
	<?php else: ?>
		<?php if (!in_array($k, $model->baseColumns)): ?>
			<?php if ($column->type == 'string'): ?>
				<div class="form-group">
					<?php echo $form->labelEx($model,$k); ?>
					<div class="controls">
						<?php 
							if (array_key_exists($column->name, $model->getTableSchema()->foreignKeys))
							{
								$fk_table = $model->tableSchema->foreignKeys[$column->name][0];
								$fk_pk = $model->tableSchema->foreignKeys[$column->name][1];
								$relation = lcfirst(str_ireplace(' ', '', ucwords(str_ireplace('_', ' ', str_ireplace(array('_fid', '_id'), '', $column->name)))));

								if ($fk_table == 'files')
								{
									// file
									echo $form->fileField($model, $relation);
								}
								else
								{
									// autocomplete
									// $relation = substr($column->name, 0, strlen($column->name)-3);
									// var_dump($relation);
									// $path = lcfirst(str_ireplace(' ', '', ucwords(str_ireplace('_', ' ', $relation))));
									// $path = $fk_table;
									$path = $relation;
									$path = ($path == 'users') ? 'user' : $path;
									$textValue = ($model->$relation) ? $model->$relation->getDisplayTitle() : null;
									echo $form->typeahead($model,$k,array('data-url'=>Yii::app()->createUrl($path.'/autocomplete').'/', 'textValue'=>$textValue));
								}
								// var_dump($fk_table);
								// if (substr($column->name, strlen($column->name) - 4) == '_fid')
								// {
								// 	// file
								// 	$relation = substr($column->name, 0, strlen($column->name)-4);
								// 	echo $form->fileField($model, $relation);
								// }
								// else if (substr($column->name, strlen($column->name) - 3) == '_id')
								// {
								// 	// autocomplete
								// 	$relation = substr($column->name, 0, strlen($column->name)-3);
								// 	$path = lcfirst(str_ireplace(' ', '', ucwords(str_ireplace('_', ' ', $relation))));
								// 	$path = ($path == 'users') ? 'user' : $path;
								// 	$textValue = ($model->$relation) ? $model->$relation->getDisplayTitle() : null;
								// 	echo $form->typeahead($model,$k,array('data-url'=>Yii::app()->createUrl($path.'/autocomplete').'/', 'textValue'=>$textValue));
								// }
								// else
								// {
								// 	echo '@todo';
								// 	var_dump();
								// }
							}
							else
							{
								if ($column->dbType == 'text')
									echo $form->textarea($model,$k,array('rows'=>6));
								else if ($column->dbType == 'date')
									echo $form->dateField($model,$k);
								else
									echo $form->textField($model,$k,array('maxlength'=>$column->size));
							}
						?>
						<?php echo $form->error($model,$k); ?>
					</div>
				</div>
			<?php elseif ($column->type == 'integer'): ?>
				<div class="form-group">
					<?php if (substr($column->name, 0, 3) != 'is_'): ?>
					<?php echo $form->labelEx($model,$k); ?>
					<?php endif; ?>
					<div class="controls">
						<?php if ($column->name == 'weight'): ?>
							<?php echo $form->dropDownList($model,$k,array(''=>'')+CString::range(-20, 100)); ?>
							<div class="note">Weight sets the default ordering for listing records</div>
						<?php elseif (substr($column->name, 0, 3) == 'is_'): ?>
							<?php echo $form->checkbox($model,$k); ?>
							<?php echo $form->labelEx($model,$k); ?>
						<?php else: ?>
							<?php echo $form->textField($model,$k,array('maxlength'=>$column->size)); ?>
						<?php endif; ?>
						<?php echo $form->error($model,$k); ?>
					</div>
				</div>
			<?php endif; ?>
		<?php endif; ?>
	<?php endif; ?>
<?php endforeach; ?>