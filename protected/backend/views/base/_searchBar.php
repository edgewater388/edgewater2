<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'action'=>Yii::app()->controller->createUrl('index'),
	'method'=>'get',
)); ?>

	<div class="form-group">
		<div class="controls">
			<?php echo $form->textField($model,'title',array('maxlength'=>128,'class'=>'form-control input-sm', 'autocomplete'=>'off', 'placeholder'=>'Search '.$model->classLabel.'s')); ?>
			<?php if ($model->title): ?>
			<a href="/<?php echo Yii::app()->request->pathInfo; ?>" data-toggle="clear"><span class="glyphicon glyphicon-remove-circle"></span></a>
			<?php endif; ?>
		</div>
	</div>

<?php $this->endWidget(); ?>
</div><!-- search-form -->