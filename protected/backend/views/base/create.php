<div class="page-header">
	<h1><?php echo $model->classLabel; ?></h1>
	<h2>Create New</h2>
</div>

<?php $this->renderPartial('form', array('model'=>$model)); ?>
