<div class="page-header">
	<h1><?php echo $model->displayTitle; ?></h1>
	<h2><?php echo $model->classLabel; ?></h2>
</div>

<?php // Render a custom _form file, if it exists, otherwise the base form ?>
<?php if (file_exists(dirname($this->viewPath).DIRECTORY_SEPARATOR.lcfirst(get_class($model)).DIRECTORY_SEPARATOR.'_viewHeader.php')): ?>
	<?php $this->renderPartial('//'.lcfirst(get_class($model)).'/_viewHeader', array('model'=>$model)); ?>
	<div class="divider"></div>
<?php endif; ?>

<div class="list-wrapper">
	<h4 class="heading"><?php echo $model->getAttributeLabel(get_class($model)); ?></h4>
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>$model->columnNames,
	)); ?>
</div>

<?php // Render a custom _form file, if it exists, otherwise the base form ?>
<?php if (file_exists(dirname($this->viewPath).DIRECTORY_SEPARATOR.lcfirst(get_class($model)).DIRECTORY_SEPARATOR.'_view.php')): ?>
	<div class="divider"></div>
	<?php $this->renderPartial('//'.lcfirst(get_class($model)).'/_view', array('model'=>$model)); ?>
<?php endif; ?>
