<?php 
	if ($this->id == 'users')
	{
		$model = new Users('search');
		$model->unsetAttributes();
		$model->attributes = isset($_GET['Users']) ? $_GET['Users'] : array();
		$this->renderPartial('backend.modules.user.views.users._simpleSearch', array('model'=>$model));
	}
	elseif ($this->id == 'trackSet' || $this->id == 'track')
	{
		$model = new TrackSet('search');
		$model->unsetAttributes();
		$model->attributes = isset($_GET['TrackSet']) ? $_GET['TrackSet'] : array();
		$this->renderPartial('backend.views.trackSet._simpleSearch', array('model'=>$model));
	}
	elseif ($this->id == 'clinic' || $this->id == 'clinicVideo')
	{
		$model = new Clinic('search');
		$model->unsetAttributes();
		$model->attributes = isset($_GET['Clinic']) ? $_GET['Clinic'] : array();
		$this->renderPartial('backend.views.clinic._simpleSearch', array('model'=>$model));
	}
	elseif ($this->id == 'lesson' || $this->id == 'lessonVideo')
	{
		$model = new Lesson('search');
		$model->unsetAttributes();
		$model->attributes = isset($_GET['Lesson']) ? $_GET['Lesson'] : array();
		$this->renderPartial('backend.views.lesson._simpleSearch', array('model'=>$model));
	}
	elseif ($this->id == 'instrument')
	{
		$model = new Instrument('search');
		$model->unsetAttributes();
		$model->attributes = isset($_GET['Instrument']) ? $_GET['Instrument'] : array();
		$this->renderPartial('backend.views.instrument._simpleSearch', array('model'=>$model));
	}
	elseif ($this->id == 'genre')
	{
		$model = new Genre('search');
		$model->unsetAttributes();
		$model->attributes = isset($_GET['Genre']) ? $_GET['Genre'] : array();
		$this->renderPartial('backend.views.genre._simpleSearch', array('model'=>$model));
	}
?>