<?php $filters = $this->clips['filters']; ?>

<div class="toolbar">
	<div class="toolbar-inner">
		<div class="">
			<div class="pull-right">
				<?php $this->widget('zii.widgets.CMenu', array(
					'items'=>$this->menu,
					'htmlOptions'=>array('class'=>'nav nav-pills pull-right'),
					'encodeLabel'=>false,
				)); ?>
			</div>
			<div class="">
				<?php if ($toolbarMenu = $this->getToolbarMenu()): ?>
				<?php $this->widget('zii.widgets.CMenu', array(
					'items'=>$toolbarMenu,
					'htmlOptions'=>array('class'=>'nav nav-pills'),
					'encodeLabel'=>false,
				)); ?>
				<?php else: ?>
					<?php $this->widget('zii.widgets.CBreadcrumbs', array(
						'links'=>$this->breadcrumbs,
					)); ?>
				<?php endif; ?>
			</div>
			<div class="clearfix"></div>
		</div>

		<?php if ($filters): ?>
			<div class="filters hidden col-sm-6">
				<div class="filters-inner">
					<?php echo $filters; ?>
				</div>
			</div>

			<script>
				jQuery(document).ready(function($) {
					var on = false;

					$('[data-toggle="filters"]').on('click', function(e) {
						toggleFilters();
						e.stopPropagation();
						e.preventDefault();
					});

					$('.filters .form-actions .btn-primary').on('click', function(e) {
						toggleFilters();
					});

					var mouseOver = false;
					$('.filters').hover(function(e) {
						mouseOver = true;
					}, function(e) {
						mouseOver = false;
					});

					$('#ui-datepicker-div').live('mouseover mouseout', function(e) {
						mouseOver = (e.type == 'mouseover') ? true : false;
					});

					$(document).bind('click', function(e) {
						if (on && !mouseOver)
							toggleFilters();
					});

					function toggleFilters()
					{
						if (!on)
							$('.filters').removeClass('hidden');
						else
							$('.filters').addClass('hidden');
						on = !on;
					}
				});
			</script>
		<?php endif; ?>
	</div>
</div>
