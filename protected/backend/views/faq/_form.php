<?php
/* @var $this FaqController */
/* @var $model Faq */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'id'=>'faq-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'question'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'question',array('size'=>60,'maxlength'=>255)); ?>
			<?php echo $form->error($model,'question'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'answer'); ?>
		<div class="controls">
			<?php echo $form->textArea($model,'answer',array('rows'=>6, 'cols'=>50)); ?>
			<?php echo $form->error($model,'answer'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'weight'); ?>
		<div class="controls">
			<?php echo $form->dropDownList($model,'weight',CString::range(-20,20)); ?>
			<?php echo $form->error($model,'weight'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'status'); ?>
		<div class="controls">
			<?php echo $form->statusField($model,'status'); ?>
			<?php echo $form->error($model,'status'); ?>
		</div>
	</div>

	<div class="form-actions buttons">
		<?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->