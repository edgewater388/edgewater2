<?php $dataProvider = new CArrayDataProvider($model); ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'track-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		array('class'=>'IdColumn'),
		'question',
		array('class'=>'StatusColumn'),
		array(
			'class'=>'LinkColumn',
			'action'=>'view',
		),
	),
)); ?>