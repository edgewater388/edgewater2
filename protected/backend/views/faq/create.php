<?php
/* @var $this FaqController */
/* @var $model Faq */

$this->breadcrumbs=array(
	'Faq Categories'=>array('/faqCategory/index'),
	$model->faqCategory->category=>array('/faqCategory/view', 'id'=>$model->faq_category_id),
	'New Question',
);

$this->menu=array(
	// array('label'=>'List Faq', 'url'=>array('index')),
	// array('label'=>'Manage Faq', 'url'=>array('admin')),
);
?>

<div class="page-header">
	<h1><?php echo $model->faqCategory->category; ?></h1>
	<h2>New Question</h2>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>