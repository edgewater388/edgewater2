<?php
/* @var $this FaqController */
/* @var $model Faq */

$this->breadcrumbs=array(
	'Faq Categories'=>array('/faqCategory/index'),
	$model->faqCategory->category=>array('/faqCategory/view', 'id'=>$model->faq_category_id),
	CString::substr($model->question, 30)=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	// array('label'=>'List Faq', 'url'=>array('index')),
	// array('label'=>'Create Faq', 'url'=>array('create')),
	// array('label'=>'View Faq', 'url'=>array('view', 'id'=>$model->id)),
	// array('label'=>'Manage Faq', 'url'=>array('admin')),
	array('label'=>'<span class="text-red">Delete</span>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<div class="page-header">
	<h1><?php echo $model->faqCategory->category; ?></h1>
	<h2><?php echo $model->question; ?></h2>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>