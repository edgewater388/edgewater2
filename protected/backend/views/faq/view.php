<?php
/* @var $this FaqController */
/* @var $model Faq */

$this->breadcrumbs=array(
	'Faq Categories'=>array('/faqCategory/index'),
	$model->faqCategory->category=>array('/faqCategory/view', 'id'=>$model->faq_category_id),
	CString::substr($model->question, 30),
);

$this->menu=array(
	array('label'=>'<span class="glyphicon glyphicon-cog"></span> Edit', 'url'=>array('update', 'id'=>$model->id)),
);
?>

<div class="page-header">
	<h1><?php echo $model->question; ?></h1>
</div>

<div class="list-wrapper">
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'id',
			'question',
			'faq_category_id',
			'answer',
			'weight',
			'status',
			'created',
			'updated',
		),
	)); ?>
</div>
