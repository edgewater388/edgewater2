<?php
/* @var $this FaqCategoryController */
/* @var $model FaqCategory */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'id'=>'faq-category-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'category'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'category',array('size'=>32,'maxlength'=>32)); ?>
			<?php echo $form->error($model,'category'); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'weight'); ?>
		<div class="controls">
			<?php echo $form->dropDownList($model,'weight',CString::range(-20,20)); ?>
			<?php echo $form->error($model,'weight'); ?>
		</div>
	</div>

	<div class="form-actions buttons">
		<?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->