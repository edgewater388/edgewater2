<?php
/* @var $this FaqCategoryController */
/* @var $model FaqCategory */

$this->breadcrumbs=array(
	'Faq Categories'=>array('index'),
	'New Category',
);

$this->menu=array(
	// array('label'=>'List FaqCategory', 'url'=>array('index')),
	// array('label'=>'Manage FaqCategory', 'url'=>array('admin')),
);
?>

<div class="page-header">
	<h1>New Category</h1>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>