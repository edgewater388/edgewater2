<?php
/* @var $this FaqCategoryController */
/* @var $model FaqCategory */

$this->breadcrumbs=array(
	// 'Faq Categories',
	// 'Manage',
);

$this->menu=array(
	array('label'=>'<span class="glyphicon glyphicon-plus-sign"></span> New', 'url'=>array('create')),
);
?>

<?php /*
<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'filters')); ?>
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
<?php $this->endWidget();?>
*/ ?>

<div class="flush">
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'faq-category-grid',
		'dataProvider'=>$model->search(),
		'columns'=>array(
			array('class'=>'IdColumn'),
			'category',
			// 'weight',
			array('class'=>'LinkColumn'),
		),
	)); ?>
</div>
