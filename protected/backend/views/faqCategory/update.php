<?php
/* @var $this FaqCategoryController */
/* @var $model FaqCategory */

$this->breadcrumbs=array(
	'Faq Categories'=>array('index'),
	$model->category=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	// array('label'=>'List FaqCategory', 'url'=>array('index')),
	// array('label'=>'Create FaqCategory', 'url'=>array('create')),
	// array('label'=>'View FaqCategory', 'url'=>array('view', 'id'=>$model->id)),
	// array('label'=>'Manage FaqCategory', 'url'=>array('admin')),
	array('label'=>'<span class="text-red">Delete</span>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<div class="page-header">
	<h1><?php echo $model->category; ?></h1>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>