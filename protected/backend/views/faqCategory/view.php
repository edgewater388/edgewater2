<?php
/* @var $this FaqCategoryController */
/* @var $model FaqCategory */

$this->breadcrumbs=array(
	'Faq Categories'=>array('index'),
	$model->category,
);

$this->menu=array(
	array('label'=>'<span class="glyphicon glyphicon-plus-sign"></span> New Question', 'url'=>array('/faq/create', 'faq_category_id'=>$model->id)),
	array('label'=>'<span class="glyphicon glyphicon-cog"></span> Edit', 'url'=>array('update', 'id'=>$model->id)),
);
?>

<div class="page-header">
	<h1><?php echo $model->category; ?></h1>
</div>

<div class="list-wrapper">
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'id',
			'category',
			'weight',
		),
	)); ?>
</div>

<div class="divider line flush"></div>

<div class="list-wrapper">
	<div class="pull-right small"><a href="<?php echo Yii::app()->createUrl('/sheetMusic/create', array('type'=>get_class($model), 'type_id'=>$model->id)); ?>"><span class="glyphicon glyphicon-plus-sign"></span> Add</a></div>
	<h4 class="heading">Sheet Music</h4>
	<?php $this->renderPartial('//faq/_list', array('model'=>$model->faqs)); ?>
</div>


<?php return; ?>

<div class="divider line flush"></div>

<h4 class="heading">Questions</h4>
<div class="list-wrapper">
	<?php $dataProvider = new CArrayDataProvider($model->faqs); ?>
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'track-grid',
		'dataProvider'=>$dataProvider,
		'columns'=>array(
			array('class'=>'IdColumn'),
			'question',
			array('class'=>'StatusColumn'),
			array(
				'class'=>'LinkColumn',
				// 'action'=>'update',
			),
		),
	)); ?>
</div>