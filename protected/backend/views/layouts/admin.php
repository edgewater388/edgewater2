<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'sidebar_first')); ?>
<div class="sidebar-menu">
	<?php $this->renderPartial('//menu/admin'); ?>
</div>
<?php $this->endWidget();?>

<?php $this->beginContent('//layouts/fluid'); ?>
	<?php echo $content; ?>
<?php $this->endContent(); ?>
