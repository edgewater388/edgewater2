<?php $this->beginContent('//layouts/main'); ?>

<?php 
	$cols = 12;
	$leftCol = ($this->clips['sidebar_first']) ? 3 : 0;
	$rightCol = ($this->clips['sidebar_second']) ? 3 : 0;
	$middleCol = $cols - $leftCol -$rightCol;
?>

<div class="container">
	<div class="content-wrapper">
		<header id="content-header" class="page-header">
			<div class="inner">
				<div class="row">
					<div class="col-sm-3">
						<div class="jump-menu">
							<?php $this->renderPartial('//menu/primary'); ?>
						</div>
					</div>
					<div class="col-sm-6">
						<h1><?php echo $this->pageTitle; ?></h1>
					</div>
					<div class="col-sm-3">
						<?php $this->renderPartial('//components/searchBar'); ?>
					</div>
				</div>
			</div>
		</header>
		<div class="row">
			<?php if ($leftCol): ?>
			<div id="sidebar-first" class="sidebar col-sm-<?php echo $leftCol; ?>">
				<div class="sidebar-inner">
					<?php echo $this->clips['sidebar_first']; ?>
				</div>
			</div><!-- sidebar -->
			<?php endif; ?>
			<div id="main-content" class="content col-sm-<?php echo $middleCol; ?>">
				<?php if ($header = $this->clips['header']): ?>
				<div class="main-header">
					<div class="inner">
						<?php echo $header; ?>
					</div>
				</div>
				<?php endif; ?>
				
				<?php $this->renderPartial('//components/toolbar'); ?>

				<div class="inner">
					<?php /*
					<div class="breadcrumb">			  	
						<?php if(isset($this->breadcrumbs)):?>
							<?php $this->widget('zii.widgets.CBreadcrumbs', array(
								'links'=>$this->breadcrumbs,
								'separator'=>'<span class="separator">/</span>',
								'homeLink'=>'<a href="/backend/site/admin">Home</a>',
							)); ?>
							<!-- breadcrumbs -->
						<?php endif; ?>
					</div>
					*/ ?>
					
					<section id="messages">
						<?php Flash::displayFlashes(); ?>
					</section>

					<?php echo $content; ?>
				</div>
			</div>
			<?php if ($rightCol): ?>
			<div id="sidebar-second" class="sidebar col-sm-<?php echo $rightCol; ?>">
				<div class="sidebar-inner">
					<div class="divider divider-mini"></div>
					<?php echo $this->clips['sidebar_second']; ?>
				</div>
			</div><!-- sidebar -->
			<?php endif; ?>
		</div>
	</div>
</div>

<script>
	jQuery(document).ready(function($) {
		var height = Math.max(
			$('.sidebar:eq(0)').height(),
			$('.sidebar:eq(1)').height(),
			$('#main-content .inner').height()
		);
		$('#main-content > .inner').css('min-height', height);
	});
</script>

<?php $this->endContent(); ?>
