<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<meta name="language" content="en" />
    <meta name="description" content="">
    <meta name="author" content="">

	<?php
		$themeUrl = Yii::app()->theme->getBaseUrl();
		$cs = Yii::app()->getClientScript();

		$cs->registerCssFile($themeUrl . '/css/bootstrap.min.css');
		$cs->registerCssFile($themeUrl . '/css/font-awesome.min.css');
		$cs->registerCssFile($themeUrl . '/css/form.css');
		$cs->registerCssFile($themeUrl . '/css/styles.css');

		// Allow views/controllers to add css styles with $this->css[] = [filepath];
		foreach ($this->css as $filepath)
			$cs->registerCSSFile($filepath);

		$cs->registerCoreScript('jquery');
		$cs->registerScriptFile($themeUrl . '/js/bootstrap.min.js');
		$cs->registerScriptFile($themeUrl . '/js/autoresize.jquery.min.js');
		$cs->registerScriptFile($themeUrl . '/js/jquery.color.min.js');
		$cs->registerScriptFile($themeUrl . '/js/jquery.easing.1.3.js');
		$cs->registerScriptFile($themeUrl . '/js/scripts.js');

		// Allow views/controllers to add js files with $this->js[] = [filepath];
		foreach ($this->js as $filepath)
			$cs->registerScriptFile($filepath);
	?>

    <?php if ($this->clips['head']): ?>
		<?php echo $this->clips['head']; ?>
	<?php endif; ?>

	<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	
    <!-- <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet"> -->
    <!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
</head>

<body class="section-<?php echo $this->id; ?> page-<?php echo $this->action->id; ?> <?php echo implode(' ', $this->classes); ?>">

	<div class="page-container">
		<?php echo $content; ?>
	</div>

    <!--Scripts-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript">
      $('select').select2();
    </script>
</body>
</html>