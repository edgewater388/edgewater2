<?php $this->beginContent('//layouts/html'); ?>

    <!-- Topbar
    ================================================== -->
    <?php if (!Yii::app()->user->isGuest): ?>
    <div class="navbar navbar-static-top anavbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a id="logo" class="navbar-brand" href="/backend"><?php echo CHtml::encode(Yii::app()->name); ?></a>
        </div>

		<?php //$this->renderPartial('//menu/primary'); ?>
		<?php $this->renderPartial('//menu/account'); ?>
      </div>
    </div>
    <?php endif; ?>
    
    <!-- Subbar
    ================================================== -->
	<?php /*
	<div class="container">
		<div class="breadcrumb">			  	
			<?php if(isset($this->breadcrumbs)):?>
				<?php $this->widget('zii.widgets.CBreadcrumbs', array(
					'links'=>$this->breadcrumbs,
					'separator'=>'',
					'homeLink'=>'<a href="/backend/site/admin">Home</a>',
				)); ?>
				<!-- breadcrumbs -->
			<?php endif; ?>
		</div>
	</div>
	*/ ?>

	<!-- Content Area
	================================================== -->
	<section id="content">
		<?php echo $content; ?>
	</section>

	<footer id="footer">

	</footer>

<?php $this->renderPartial('//components/modal'); ?>

<?php $this->endContent(); ?>