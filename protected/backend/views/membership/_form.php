<div class="form-group">
	<?php echo $form->label($model, 'user_id'); ?>
	<div class="controls">
		<?php if ($model->isNewRecord): ?>
			<?php echo $form->typeahead($model, 'user_id', array('data-url'=>Yii::app()->createUrl('/user/autocomplete').'/', 'textValue'=>$model->user ? $model->user->profile->name : '')); ?>
		<?php else: ?>
			<?php echo $model->user->profile->name; ?>
		<?php endif; ?>
		<?php echo $form->error($model, 'user_id'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'valid_from'); ?>
	<div class="controls">
		<?php echo $form->dateField($model, 'valid_from'); ?>
		<?php echo $form->error($model, 'valid_from'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'valid_to'); ?>
	<div class="controls">
		<?php echo $form->dateField($model, 'valid_to'); ?>
		<?php echo $form->error($model, 'valid_to'); ?>
	</div>
</div>

<?php if (($model->isNewRecord == false) && ($model->user && $model->user->customer)): ?>
<div class="form-group">
	<?php echo $form->label($model, 'subtotal'); ?>
	<div class="controls">
		<?php echo $form->textField($model, 'subtotal'); ?>
		<?php echo $form->error($model, 'subtotal'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'next_billing_date'); ?>
	<div class="controls">
		<?php echo $form->dateField($model, 'next_billing_date'); ?>
		<?php echo $form->error($model, 'next_billing_date'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'auto_renew'); ?>
	<div class="controls">
		<?php echo $form->checkbox($model, 'auto_renew'); ?>
		<?php echo $form->error($model, 'auto_renew'); ?>
	</div>
</div>

<?php endif; ?>