<br>
<?php /*
<div class="page-header">
	<h1>Log <?php echo $model->id; ?></h1>
</div>
*/ ?>

<?php /*
<div class="list-wrapper">
	<h4 class="heading"><?php echo $model->getAttributeLabel(get_class($model)); ?></h4>
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>$model->columnNames,
	)); ?>
</div>
*/ ?>

<div class="pull-right">
	<div class="pull-left">
		<a href="<?php echo (Yii::app()->request->getParam('id')-1); ?>"><span class="fa fa-angle-left"></span> Prev</a> 
	</div>
	<div class="pull-right" style="margin-left: 16px;">
		<a href="<?php echo (Yii::app()->request->getParam('id')+1); ?>">Next <span class="fa fa-angle-right"></span></a>
	</div>
	<div class="clearfix"></div>
</div>

<?php if ($model): ?>

<?php $membership = new Membership; ?>
<div class="list-wrapper">
	<h4 class="heading"><?php echo $model->getAttributeLabel(get_class($model)); ?></h4>
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data'=>unserialize($model->data),
		'attributes'=>$membership->columnNames,
	)); ?>
</div>

<?php endif; ?>