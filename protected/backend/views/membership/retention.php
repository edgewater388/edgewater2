<?php 
	$this->menu=array(
		array('label'=>'<span class="glyphicon glyphicon-arrow-down"></span> Export', 'url'=>Export::getUrl()),
	);
?>

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'filters')); ?>
	<?php $this->renderPartial('_searchRetention',array(
		'model'=>$model,
	)); ?>
<?php $this->endWidget(); ?>

<?php $counts = $model->getRetentionCounts(); ?>
<?php
	$query_url = '/'.Yii::app()->request->pathInfo.'?';
	
	if (isset($_GET['Membership']))
	{
		foreach ($_GET['Membership'] as $k=>$v)
			$query_url .= 'Membership['.$k.']='.$v.'&';
	}
?>
<div class="flush">
	<div class="grid-header text-center">
		<div class="row">
			<div class="col-xs-2">
				<a href="<?php echo $query_url; ?>">
					Count <br>
					<span style="font-weight: normal; font-size: 20px;"><?php echo number_format($counts['count']); ?></span>
				</a>
			</div>
			<div class="col-xs-2">
				<a href="<?php echo $query_url; ?>query=active">
					Active <br>
					<span style="font-weight: normal; font-size: 20px;"><?php echo number_format($counts['active']); ?></span>
				</a>
			</div>
			<div class="col-xs-2">
				<a href="<?php echo $query_url; ?>query=expired">
					Expired <br>
					<span style="font-weight: normal; font-size: 20px;"><?php echo number_format($counts['inactive']); ?></span>
				</a>
			</div>
			<div class="col-xs-2">
				<a href="<?php echo $query_url; ?>query=retained">
					Retained <br>
					<span style="font-weight: normal; font-size: 20px;"><?php echo number_format($counts['retained']); ?></span>
				</a>
			</div>
			<?php /*
			<div class="col-xs-2">
				<a href="<?php echo $query_url; ?>query=lost">
					Lost <br>
					<span style="font-weight: normal; font-size: 20px;"><?php echo number_format($counts['lost']); ?></span>
				</a>
			</div>
			*/ ?>
			<div class="col-xs-2">
				<a href="<?php echo $query_url; ?>query=cancelled">
					Cancelled <br>
					<span style="font-weight: normal; font-size: 20px;"><?php echo number_format($counts['cancelled']); ?></span>
				</a>
			</div>
		</div>
	</div>
</div>

<div class="flush">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(25),
	'hideHeader'=>false,
	'columns'=>array(
		array(
			'value'=>'($row + 1)',
			'type'=>'raw',
		),
		// array('class'=>'IdColumn'),
		// 'displayTitle',
		'user.profile.name',
		'subtotal',
		// 'user.renewalCount',
		'join_date',
		'trial_end_date',
		'valid_from',
		'valid_to',
		'cancelled_date',
		// array(
		// 	'name'=>'created',
		// 	'value'=>'date("Y-m-d", strtotime($data->created))',
		// 	'type'=>'raw',
		// ),
		// array(
		// 	'class'=>'StatusColumn',
		// ),
		array(
			'class'=>'LinkColumn',
		)
	),
)); ?>
</div>
