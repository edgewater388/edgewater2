<?php
$this->widget('zii.widgets.CMenu', array(
    'activateParents' => true,
    'htmlOptions'=>array('class'=>'nav navbar-nav navbar-right'),
    'encodeLabel'=>false,
    'items'=>array(
        array('label'=>Yii::app()->user->name.' <b class="caret"></b>', 'url'=>'#', 'submenuOptions'=>array('class'=>'dropdown-menu'), 'itemOptions'=>array('class'=>'dropdown'), 'linkOptions'=>array('class'=>'dropdown-toggle', 'data-toggle'=>'dropdown'), 'items'=>array(
            array('label'=>'My Account', 'url'=>array('/user/users/view','id'=>Yii::app()->user->id)),
            array('label'=>'<div class="divider"></div>'),
            array('label'=>'Go to Front-end', 'url'=>'/'),
            array('label'=>'<div class="divider"></div>'),
            array('label'=>'Logout', 'url'=>array('/user/users/logout')),
        )),
    ),
));
?>