<?php

$items = array();

if ($this->id == 'users' || $this->id == 'profile' || $this->id == 'membership' || $this->id == 'transaction' || $this->id == 'promoCode' || $this->id == 'referFriend')
{
    $items[] = array('label'=>'User Accounts', 'url'=>array('/user/users/index'), 'active'=>($this->id=='users'||$this->id=='profile'));
    $items[] = array('label'=>'Memberships', 'url'=>array('/membership/index'), 'active'=>($this->id=='membership' && $this->action->id=='index'));
    $items[] = array('label'=>'Promo Codes', 'url'=>array('/promoCode/index'), 'active'=>($this->id=='promoCode' && $this->action->id=='index'));
    $items[] = array('label'=>'Refer a Friend', 'url'=>array('/referFriend/settings'), 'active'=>($this->id=='referFriend'), 'items'=>array(
        array('label'=>'Invites', 'url'=>array('/referFriend/invites'), 'active'=>($this->id=='referFriend' && $this->action->id=='invites')),
        array('label'=>'Rewards', 'url'=>array('/referFriend/rewards'), 'active'=>($this->id=='referFriend' && $this->action->id=='rewards')),
    ));

    $items[] = array('label'=>'Reports', 'itemOptions'=>array('class'=>'dropdown-header'));
    $items[] = array('label'=>'Customer Retention', 'url'=>array('/membership/retention'), 'active'=>($this->id=='membership' && $this->action->id=='retention'));
    $items[] = array('label'=>'Transactions', 'url'=>array('/transaction/index'), 'active'=>($this->id=='transaction'));
    $items[] = array('label'=>'Promo Codes', 'url'=>array('/promoCode/report'), 'active'=>($this->id=='promoCode' && $this->action->id == 'report'));
}
else if ($this->id == 'site' && $this->action->id == 'settings' || in_array($this->id, array()))
{
    $items[] = array('label'=>'Overview', 'url'=>array('/site/settings'));
    $items[] = array('label'=>'Settings', 'itemOptions'=>array('class'=>'dropdown-header'));
}
else
{
    $items[] = array('label'=>'Dashboard', 'url'=>array('/site/index'));

    $items[] = array('label'=>'Site Content', 'itemOptions'=>array('class'=>'dropdown-header'));
    // $items[] = array('label'=>'FAQ', 'url'=>array('/faqCategory/index'), 'active'=>($this->id=='faq'||$this->id=='faqCategory'));
    $items[] = array('label'=>'Playlists', 'url'=>array('/playlist/index'), 'active'=>($this->id=='playlist'));
    $items[] = array('label'=>'Products', 'url'=>array('/product/index'), 'active'=>($this->id=='product'));
    $items[] = array('label'=>'Videos', 'url'=>array('/video/index'), 'active'=>($this->id=='video'));
    $items[] = array('label'=>'Regimens', 'url'=>array('/regimen/index'), 'active'=>($this->id=='regimen'));
    $items[] = array('label'=>'Stream', 'url'=>array('/stream/index'), 'active'=>($this->id=='stream'));
	$items[] = array('label'=>'Plan', 'url'=>array('/plan/index'), 'active'=>($this->id=='plan'));
    $items[] = array('label'=>'Settings', 'url'=>array('/settings/index'), 'active'=>($this->id=='settings'));
}

$this->widget('zii.widgets.CMenu', array(
    'activateParents' => true,
    'htmlOptions'=>array('class'=>'nav nav-stacked'),
    'encodeLabel'=>false,
    'items'=>$items,
));
?>