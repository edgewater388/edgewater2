<?php

// if ($this->id == 'users')
// 	$label = 'Account Management';
// elseif (in_array($this->id, array('trackSet', 'track', 'clinic', 'clinicVideo', 'lesson', 'lessonVideo', 'instrument', 'genre', 'level')))
// 	$label = 'Content Management';
// elseif ($this->id == 'site' && $this->action->id == 'settings')
// 	$label = 'Site Settings';
// else
// 	$label = 'Dashboard';

$label = 'Jump to...';

$this->widget('zii.widgets.CMenu', array(
    'activateParents' => true,
    'htmlOptions'=>array('class'=>'nav'),
    'encodeLabel'=>false,
    'items'=>array(
        array('label'=>$label.' <b class="caret"></b>', 'url'=>'#', 'submenuOptions'=>array('class'=>'dropdown-menu'), 'itemOptions'=>array('class'=>'dropdown'), 'linkOptions'=>array('class'=>'', 'onClick'=>'return false;'), 'items'=>array(
        	array('label'=>'Content Management', 'url'=>array('/site/index')),
            array('label'=>'Account Management', 'url'=>array('/user/users/index')),
            // array('label'=>'Content Management', 'url'=>array('/trackSet/index')),
            // array('label'=>'Commerce', 'url'=>array('/commerce')),
            array('label'=>'Site Settings', 'url'=>array('/site/settings')),
            // array('label'=>'Reports', 'url'=>array('/report/index')),
        )),
    ),
));
?>