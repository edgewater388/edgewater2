<?php
$this->widget('zii.widgets.CMenu', array(
    'activateParents' => true,
    'htmlOptions'=>array('class'=>'nav nav-pills nav-stacked'),
    'encodeLabel'=>false,
    'items'=>array(
        // 'Products' menu item will be selected no matter which tag parameter value is since it's not specified.
        array('label'=>'Section <b class="caret pull-right"></b>', 'url'=>'#', 'submenuOptions'=>array('class'=>'dropdown-menu'), 'itemOptions'=>array('class'=>'dropdown'), 'linkOptions'=>array('class'=>'dropdown-toggle', 'data-toggle'=>'dropdown'), 'items'=>array(
            // array('label'=>'All', 'url'=>array('track/index'), 'itemOptions'=>array('class'=>'hidden')),
            // array('label'=>'New', 'url'=>array('track/index', 'state'=>'1')),
            // array('label'=>'Transcribing', 'url'=>array('track/index', 'state'=>'2')),
            // array('label'=>'Reviewing', 'url'=>array('track/index', 'state'=>'3')),
            // array('label'=>'Approved', 'url'=>array('track/index', 'state'=>'4')),
        )),
    ),
));
?>

<?php
$this->widget('zii.widgets.CMenu', array(
    'activateParents' => true,
    'htmlOptions'=>array('class'=>'nav nav-list'),
    'encodeLabel'=>false,
    'items'=>array(
        array('label'=>'Users', 'itemOptions'=>array('class'=>'nav-header')),
        array('label'=>'Accounts', 'url'=>array('users/reports')),
    	// array('label'=>'Heading', 'itemOptions'=>array('class'=>'nav-header')),
     //    array('label'=>'Orders', 'url'=>array('orders/admin')),
     //    array('label'=>'Purchases', 'url'=>array('purchases/admin')),
    ),
));
?>