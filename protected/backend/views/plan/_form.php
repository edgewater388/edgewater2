<div class="form-group">
	<?php echo $form->labelEx($model, 'description'); ?>
	<div class="controls">
		<?php echo $form->richTextarea($model, 'description', array('rows'=>6)); ?>
		<?php echo $form->error($model, 'description'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model,'amount'); ?>
	<div class="controls">
		<?php echo $form->textField($model,'amount'); ?>
		<?php echo $form->error($model,'amount'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model,'old_amount'); ?>
	<div class="controls">
		<?php echo $form->textField($model,'old_amount'); ?>
		<?php echo $form->error($model,'old_amount'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'min_term'); ?>
	<div class="controls">
		<?php echo $form->textField($model,'min_term'); ?>
		<?php echo $form->error($model,'min_term'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'interval'); ?>
	<div class="controls">
		<?php echo $form->dropDownList($model, 'interval', Plan::getIntervals()); ?>
		<?php echo $form->error($model, 'interval'); ?>
	</div>
</div>

<div class="form-group">
	<div class="controls">
		<?php echo $form->checkbox($model,'is_default'); ?>
		<?php echo $form->label($model, 'is_default'); ?>
		<?php echo $form->error($model,'is_default'); ?>
	</div>
</div>

<div class="form-group">
	<div class="controls">
		<?php echo $form->checkbox($model,'is_main_default'); ?>
		<?php echo $form->label($model, 'is_main_default'); ?>
		<?php echo $form->error($model,'is_main_default'); ?>
	</div>
</div>

<div class="form-group">
	<div class="controls">
		<?php echo $form->checkbox($model,'is_auto_renew'); ?>
		<?php echo $form->label($model, 'is_auto_renew'); ?>
		<?php echo $form->error($model,'is_auto_renew'); ?>
	</div>
</div>
<?php if (!$model->isNewRecord && $model->is_special || $model->isNewRecord): ?>
	<?php if (($image = $model->getThumbUrl()) != null): ?>
		<img src="<?php echo $image; ?>" width="500" height="350">
	<?php endif; ?>
	<div class="form-group">
		<?php echo $form->labelEx($model, 'image'); ?>
		<div class="controls">
			<?php echo $form->fileField($model, 'image'); ?>
			<div class="note">Image will be cropped automatically to 1051 x 700</div>
			<?php echo $form->error($model, 'image'); ?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'slug'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'slug', array('maxlength'=>255)); ?>
			<?php echo $form->error($model,'slug'); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="controls">
			<?php echo $form->checkbox($model,'is_special'); ?>
			<?php echo $form->label($model, 'is_special'); ?>
			<?php echo $form->error($model,'is_special'); ?>
		</div>
	</div>
	<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<?php echo $form->labelEx($model, 'promo_start_date', array('label'=>'Promo Offer Date')); ?>
			<div class="controls">
				<?php echo $form->dateField($model, 'promo_start_date', array('placeholder'=>$model->getPlaceholder('promo_start_date'))); ?>
				<?php echo $form->error($model, 'promo_start_date'); ?>
			</div>
		</div>		
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<?php echo $form->labelEx($model, 'promo_end_date', array('label'=>'&nbsp;')); ?>
			<div class="controls">
				<?php echo $form->dateField($model, 'promo_end_date', array('placeholder'=>$model->getPlaceholder('promo_end_date'))); ?>
				<?php echo $form->error($model, 'promo_end_date'); ?>
			</div>
		</div>
	</div>
</div>
<?php endif;
