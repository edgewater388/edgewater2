<div class="row">
	<div class="col-xs-6">
		<div class="form-group">
			<?php echo $form->label($model,'is_public'); ?>
			<div class="controls">
				<?php echo $form->dropDownList($model,'is_public',array(''=>'All','1'=>'Yes','0'=>'No')); ?>
			</div>
		</div>
	</div>
	<div class="col-xs-6">
		<div class="form-group">
			<?php echo $form->label($model,'is_featured'); ?>
			<div class="controls">
				<?php echo $form->dropDownList($model,'is_featured',array(''=>'All','1'=>'Yes','0'=>'No')); ?>
			</div>
		</div>
	</div>
</div>
