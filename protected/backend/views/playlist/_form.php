<div class="form-group">
	<?php echo $form->label($model, 'strength'); ?>
	<div class="controls">
		<?php echo $form->dropDownList($model,'strength',array(''=>'')+Video::getLevels()); ?>
		<?php echo $form->error($model,'strength'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'cardio'); ?>
	<div class="controls">
		<?php echo $form->dropDownList($model,'cardio',array(''=>'')+Video::getLevels()); ?>
		<?php echo $form->error($model,'cardio'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'restorative'); ?>
	<div class="controls">
		<?php echo $form->dropDownList($model,'restorative',array(''=>'')+Video::getLevels()); ?>
		<?php echo $form->error($model,'restorative'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'weight'); ?>
	<div class="controls">
		<?php echo $form->dropDownList($model,'weight',array(''=>'')+CString::range(-20, 100)); ?>
		<div class="note">Weight sets the default ordering for listing records</div>
		<?php echo $form->error($model,'weight'); ?>
	</div>
</div>

<div class="form-group">
	<div class="controls">
		<?php echo $form->checkbox($model,'is_public'); ?>
		<?php echo $form->label($model, 'is_public'); ?>
		<?php echo $form->error($model,'is_public'); ?>
	</div>
</div>

<div class="form-group">
	<div class="controls">
		<?php echo $form->checkbox($model,'is_featured'); ?>
		<?php echo $form->label($model, 'is_featured'); ?>
		<?php echo $form->error($model,'is_featured'); ?>
	</div>
</div>
