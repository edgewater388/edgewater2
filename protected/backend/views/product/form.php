<div class="form">
	
	<?php
	$form=$this->beginWidget('ActiveForm', array(
		'id'=>'post-form',
		'enableAjaxValidation'=>false,
		'htmlOptions'=>array('enctype' => 'multipart/form-data'),
	));
	?>
	
	<div class="row">
		<div class="col-md-8">
			<?php if ($model->columnExists('title')): ?>
				<div class="form-group">
					<?php echo $form->labelEx($model,'title'); ?>
					<div class="controls">
						<?php echo $form->textField($model,'title',array('maxlength'=>255)); ?>
						<?php echo $form->error($model,'title'); ?>
					</div>
				</div>
			<?php endif; ?>
			
			<?php if($model->columnExists('body')): ?>
				<div class="form-group">
					<?php echo $form->labelEx($model,'body'); ?>
					<div class="controls">
						<?php echo $form->richTextArea($model,'body',array('rows'=>6, 'class'=>'span12')); ?>
						<?php echo $form->error($model,'body'); ?>
					</div>
				</div>
			<?php endif; ?>
			
			<?php // Render a custom _form file, if it exists, otherwise the general post form ?>
			<?php if (file_exists(dirname($this->viewPath).DIRECTORY_SEPARATOR.lcfirst(get_class($model)).DIRECTORY_SEPARATOR.'_form.php')): ?>
				<div class="divider"></div>
				<?php $this->renderPartial('//'.lcfirst(get_class($model)).'/_form', array('form'=>$form, 'model'=>$model)); ?>
			<?php else: ?>
				<div class="divider"></div>
				<?php $this->renderPartial('//base/_form', array('form'=>$form, 'model'=>$model)); ?>
			<?php endif; ?>
		</div>
		
		<div class="col-md-4">
			<div class="well">
				<div class="form-group">
					<?php echo $form->labelEx($model,'status'); ?>
					<div class="controls">
						<?php echo $form->dropDownList($model,'status', array('0'=>'Unpublished','1'=>'Published'), array('class'=>'span12')); ?>
						<?php echo $form->error($model,'status'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<div class="form-actions buttons">
		<?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
	
	<?php $this->endWidget(); ?>

</div><!-- form -->