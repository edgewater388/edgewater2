<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<?php echo $form->labelEx($model, 'start_date', array('label'=>'Promotion Date')); ?>
			<div class="controls">
				<?php echo $form->dateField($model, 'start_date', array('placeholder'=>$model->getPlaceholder('start_date'))); ?>
				<?php echo $form->error($model, 'start_date'); ?>
			</div>
		</div>		
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<?php echo $form->labelEx($model, 'end_date', array('label'=>'&nbsp;')); ?>
			<div class="controls">
				<?php echo $form->dateField($model, 'end_date', array('placeholder'=>$model->getPlaceholder('end_date'))); ?>
				<?php echo $form->error($model, 'end_date'); ?>
			</div>
		</div>
	</div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'code'); ?>
	<div class="controls">
		<?php echo $form->textField($model, 'code', array('maxlength'=>32, 'style'=>'width: 200px;')); ?>
		<div class="note">Leave blank to autogenerate</div>
		<?php echo $form->error($model, 'code'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'description'); ?>
	<div class="controls">
		<?php echo $form->textarea($model, 'description', array('rows'=>3)); ?>
		<?php echo $form->error($model, 'description'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'quantity'); ?>
	<div class="controls">
		<?php echo $form->textField($model, 'quantity', array('maxlength'=>32, 'style'=>'width: 200px;')); ?>
		<div class="note">Leave blank for unlimited</div>
		<?php echo $form->error($model, 'quantity'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'plans_list'); ?>
	<div class="controls">
		<?php echo $form->checkboxList($model, 'plans_list', Plan::getList()); ?>
		<?php echo $form->error($model, 'plans_list'); ?>
	</div>
</div>

<div class="divider"></div>
<div class="divider line"></div>
<div class="divider"></div>

<h3>Discount</h3>
<div class="row">
	<div class="col-sm-3">
		<div class="form-group">
			<?php echo $form->labelEx($model, 'discount_amount', array('label'=>'Amount')); ?>
			<div class="controls">
				<?php echo $form->textField($model, 'discount_amount', array('maxlength'=>15)); ?>
				<?php echo $form->error($model, 'discount_amount'); ?>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			<?php echo $form->labelEx($model, 'discount_type', array('label'=>'&nbsp;')); ?>
			<div class="controls">
				<?php echo $form->dropDownList($model, 'discount_type', PromoCode::getDiscountTypes()); ?>
				<?php echo $form->error($model, 'discount_type'); ?>
			</div>
		</div>
	</div>
</div>

<div class="form-group">
	<div class="row">
		<div class="col-sm-3">
			<?php echo $form->labelEx($model, 'discount_length', array('label'=>'Length')); ?>
			<div class="controls">
				<?php echo $form->textField($model, 'discount_length', array('maxlength'=>15, 'placeholder'=>'')); ?>
				<?php echo $form->error($model, 'discount_length'); ?>
			</div>
		</div>
		<div class="col-sm-3">
			<?php echo $form->labelEx($model, 'discount_interval', array('label'=>'&nbsp;')); ?>
			<div class="controls">
				<?php echo $form->dropDownList($model, 'discount_interval', PromoCode::getDiscountIntervals()); ?>
				<?php echo $form->error($model, 'discount_interval'); ?>
			</div>
		</div>
	</div>
	<div class="note">Enter a number or leave blank for unlimited</div>
</div>

<?php /*
<div class="form-group">
	<?php echo $form->labelEx($model, 'quantity'); ?>
	<div class="controls">
		<?php echo $form->textField($model, 'quantity', array('maxlength'=>10, 'style'=>'width: 60px;')); ?>
		<div class="note">Leave blank for unlimited</div>
		<?php echo $form->error($model, 'quantity'); ?>
	</div>
</div>
*/ ?>