<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-content">
		<div class="row">
			<div class="col-xs-8">
				<div class="form-group">
					<?php echo $form->labelEx($model, 'code'); ?>
					<div class="controls">
						<?php echo $form->textField($model,'code',array('maxlength'=>128,'class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('code'))); ?>
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="form-group">
					<?php echo $form->labelEx($model, 'id'); ?>
					<div class="controls">
						<?php echo $form->textField($model,'id',array('maxlength'=>11,'class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('id'))); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model, 'date'); ?>
			<div class="controls">
				<div class="row">
					<div class="col-xs-6">
						<?php echo $form->dateField($model,'start_date',array('class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('start_date'))); ?>
					</div>
					<div class="col-xs-6">
						<?php echo $form->dateField($model,'end_date',array('class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('end_date'))); ?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<?php echo $form->labelEx($model, 'created'); ?>
			<div class="controls">
				<div class="row">
					<div class="col-xs-6">
						<?php echo $form->dateField($model,'created_from',array('class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('created_from'))); ?>
					</div>
					<div class="col-xs-6">
						<?php echo $form->dateField($model,'created_to',array('class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('created_to'))); ?>
					</div>
				</div>
			</div>
		</div>
						
		<div class="form-group">
			<?php echo $form->labelEx($model, 'status'); ?>
			<div class="controls">
				<?php echo $form->dropDownList($model,'status',array(''=>'','1'=>'Active','0'=>'Inactive'),array('class'=>'')); ?>
			</div>
		</div>
	</div>

	<div class="form-actions">
		<?php echo Html::button('Clear', array('class'=>'btn btn-sm btn-default', 'data-toggle'=>'clearForm')); ?>
		<?php echo Html::submitButton('Search', array('class'=>'btn btn-sm btn-primary pull-right')); ?>
		<div class="clearfix"></div>
	</div>

<?php $this->endWidget(); ?>
</div><!-- search-form -->