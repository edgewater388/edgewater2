<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'filters')); ?>
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
<?php $this->endWidget();?>

<div class="flush">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(25),
	'columns'=>array(
		array('class'=>'IdColumn'),
		'displayTitle',
		'description',
		array(
			'class'=>'StatusColumn',
		),
		array(
			'class'=>'LinkColumn',
		)
	),
)); ?>
</div>
