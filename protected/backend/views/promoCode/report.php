<?php
	$this->menu = array(
		array('label'=>'<span class="glyphicon glyphicon-arrow-down"></span> Export', 'url'=>Export::getUrl()),
	);
?>


<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'filters')); ?>
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
<?php $this->endWidget();?>

<div class="flush">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(25),
	'hideHeader'=>false,
	'columns'=>array(
		array('class'=>'IdColumn'),
		'code',
		'redemptionCount',
		'transactionCount',
		// 'created',
		// array(
		// 	'class'=>'StatusColumn',
		// ),
		array(
			'class'=>'LinkColumn',
		)
	),
)); ?>
</div>

<div class="divider divider mini line"></div>

<p>
	<span class="bold">Redemption Count</span><br>
	The number of times a promo code has been activated.
</p>
<p>
	<span class="bold">Transaction Count</span><br>
	The number of times a promo code has been used to discount a payment.

</p>