<?php
	$this->menu = array(
		array('label'=>'<span class="glyphicon glyphicon-arrow-down"></span> Export', 'url'=>Export::getUrl()),
	);
?>

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'filters')); ?>
	<?php $this->renderPartial('_searchRewards',array(
		'model'=>$model,
	)); ?>
<?php $this->endWidget();?>

<div class="flush">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(25),
	'hideHeader'=>false,
	'columns'=>array(
		array('class'=>'IdColumn'),
		'referredUser.profile.name',
		'user.profile.name',
		'inviteType',
		'amount',
		'released',
		'redeemed',
		// 'created',
		// array(
		// 	'class'=>'LinkColumn',
		// )
	),
)); ?>
</div>
