<div class="page-header">
	<h1>Refer a Friend</h1>
	<h2>Settings</h2>
</div>

<div class="form">
	<?php 
		$form=$this->beginWidget('ActiveForm', array(
			'id'=>'settings-form',
			'enableAjaxValidation'=>false,
			'htmlOptions'=>array('enctype' => 'multipart/form-data'),
		)); 
	?>

	<div class="form-group">
		<?php echo Html::label('Reward Amount', 'reward_amount'); ?>
		<div class="controls">
			<?php echo Html::textField('reward_amount', $reward_amount, array('maxlength'=>32, 'style'=>'width: 200px;', 'placeholder'=>'10')); ?>
			<div class="note">The amount awarded to the account after 2 weeks since the referred friend makes their first payment.</div>
		</div>
	</div>

	<div class="form-group">
		<?php echo Html::label('Promo Code', 'promo_code'); ?>
		<div class="controls">
			<?php echo Html::dropDownList('promo_code', $promo_code, array(''=>'')+PromoCode::getList(), array('maxlength'=>32, 'style'=>'width: 200px;', 'placeholder'=>'')); ?>
		</div>
	</div>

	<div class="form-actions buttons">
		<?php echo Html::submitButton('Save'); ?>
	</div>

	<?php $this->endWidget(); ?>
</div>