<div class="form-group">
	<?php echo $form->labelEx($model, 'description'); ?>
	<div class="controls">
		<?php echo $form->richTextarea($model, 'description', array('rows'=>6)); ?>
		<?php echo $form->error($model, 'description'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'summary'); ?>
	<div class="controls">
		<?php echo $form->textarea($model, 'summary', array('rows'=>6)); ?>
		<?php echo $form->error($model, 'summary'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'weight'); ?>
	<div class="controls">
		<?php echo $form->dropDownList($model, 'weight', CString::range(-20, 20)); ?>
		<?php echo $form->error($model, 'weight'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'image'); ?>
	<div class="controls">
		<?php echo $form->fileField($model, 'image'); ?>
		<div class="note">Image will be cropped automatically to 792 x 392</div>
		<?php echo $form->error($model, 'image'); ?>
	</div>
</div>
