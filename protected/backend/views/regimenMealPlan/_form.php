<div class="form-group">
    <?php echo $form->labelEx($model, 'week_num'); ?>
	<div class="controls">
		<?php echo $form->textField($model, 'week_num'); ?>
		<?php echo $form->error($model, 'week_num'); ?>
	</div>
</div>

<div class="form-group">
    <?php echo $form->labelEx($model, 'path_to_file'); ?>
    <div class="controls">
        <?php echo $form->fileField($model, 'path_to_file', array('accept' => 'application/pdf')); ?>
        <?php echo $form->error($model, 'path_to_file'); ?>
    </div>
</div>