<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'filters')); ?>
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
<?php $this->endWidget();?>

<div class="flush">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(25),
	'columns'=>array(
		// array('class'=>'IdColumn'),
		'dayTitle',
		// 'displayTitle',
		array(
			'value'=>'$data->video ? $data->video->displayTitle. ($data->video->status ? "" : " <span class=\"text-red bold\">(Inactive)</span>") : "<span class=\"text-light\">(Deleted)</span>"',
			'type'=>'raw',
		),
		array(
			'class'=>'StatusColumn',
		),
		array(
			'class'=>'LinkColumn',
		),
	),
)); ?>
</div>
