<?php
$form = $this->beginWidget('ActiveForm', [
	'id'                   => 'post-form',
	'enableAjaxValidation' => false,
	'htmlOptions'          => ['enctype' => 'multipart/form-data'],
	'action'               => $this->createUrl('settings/save'),
]);
?>
<div class="row">
    <div class="col-md-8">
        <div class="form-group">
			<?php echo $form->labelEx($model, 'demo_access'); ?>
            <div class="controls">
				<?php echo $form->dropDownList($model, 'demo_access',
					$model->switchOptions,
					['options' =>
						 [
							 Settings::getSettingValue(Settings::SETTING_DEMO_ACCESS) => ['selected' => true],
						 ],
					]);
				?>
				<?php echo $form->error($model, 'demo_access'); ?>
            </div>
        </div>
        <div class="form-group">
			<?php echo $form->labelEx($model, 'stream_popup_title'); ?>
            <div class="controls">
				<?php echo $form->textField($model, 'stream_popup_title', [
					'value' => Settings::getSettingValue(Settings::SETTING_STREAM_POPUP_TITLE),
				]); ?>
				<?php echo $form->error($model, 'stream_popup_title'); ?>
            </div>
        </div>
        <div class="form-group">
			<?php echo $form->labelEx($model, 'stream_popup_text'); ?>
            <div class="controls">
				<?php echo $form->textField($model, 'stream_popup_text', [
					'value' => Settings::getSettingValue(Settings::SETTING_STREAM_POPUP_TEXT),
				]); ?>
				<?php echo $form->error($model, 'stream_popup_text'); ?>
            </div>
        </div>
        <div class="form-group">
			<?php echo $form->labelEx($model, 'trial_duration'); ?>
            <div class="controls">
				<?php echo $form->textField($model, 'trial_duration', [
					'value' => Settings::getSettingValue(Settings::SETTING_TRIAL_DURATION),
				]); ?>
				<?php echo $form->error($model, 'trial_duration'); ?>
            </div>
        </div>
        <div class="form-group">
			<?php echo $form->labelEx($model, 'cancel_discount'); ?>
            <div class="controls">
				<?php echo $form->textField($model, 'cancel_discount', [
					'value' => Settings::getSettingValue(Settings::SETTING_CANCEL_DISCOUNT),
				]); ?>
				<?php echo $form->error($model, 'cancel_discount'); ?>
            </div>
        </div>

        <div class="form-group">
			<?php echo $form->labelEx($model, 'trial_sign_up_price'); ?>
            <div class="controls">
				<?php echo $form->textField($model, 'trial_sign_up_price', [
					'value' => Settings::getSettingValue(Settings::SETTING_TRIAL_SIGN_UP_PRICE),
				]); ?>
				<?php echo $form->error($model, 'trial_sign_up_price'); ?>
            </div>
        </div>

        <div class="form-group">
			<?php echo $form->labelEx($model, 'trial_sign_up_duration'); ?>
            <div class="controls">
				<?php echo $form->textField($model, 'trial_sign_up_duration', [
					'value' => Settings::getSettingValue(Settings::SETTING_TRIAL_SIGN_UP_DURATION),
				]); ?>
				<?php echo $form->error($model, 'trial_sign_up_duration'); ?>
            </div>
        </div>

        <div class="form-actions buttons">
			<?php echo Html::submitButton('Save'); ?>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
