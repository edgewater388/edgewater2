<?php $this->classes[] = 'detail-page'; ?>

<div class="page-header">
	<h1>Overview</h1>
</div>

<div class="list-wrapper">
	<h4 class="heading">User Accounts</h4>
	<table class="table table-condensed table-striped detail-view">
		<tr>
			<th>Total</th>
			<td><?php echo number_format(Users::model()->count()); ?></td>
		</tr>
		<tr>
			<th>Active / 30 days</th>
			<td><?php echo number_format(Users::model()->count('lastvisit >= :time',array(':time'=>date('Y-m-d', time()-(60*60*24*30))))); ?></td>
		</tr>
		<tr>
			<th>New / 30 days</th>
			<td><?php echo number_format(Users::model()->count('created >= :time',array(':time'=>date('Y-m-d', time()-(60*60*24*30))))); ?></td>
		</tr>
		<tr>
			<th>New / 7 days</th>
			<td><?php echo number_format(Users::model()->count('created >= :time',array(':time'=>date('Y-m-d', time()-(60*60*24*7))))); ?></td>
		</tr>
		<tr>
			<th>New / 24 hours</th>
			<td><?php echo number_format(Users::model()->count('created >= :time',array(':time'=>date('Y-m-d', time()-(60*60*24))))); ?></td>
		</tr>
	</table>
</div>
