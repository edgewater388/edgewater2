<?php $this->classes[] = 'detail-page'; ?>

<div class="page-header">
	<h1>Settings</h1>
</div>

<div class="list-wrapper">
	<h4 class="heading">Environment</h4>
	<table class="table table-condensed table-striped detail-view">
		<tbody>
			<tr>
				<th>Environment</th>
				<td><?php echo Yii::app()->environment; ?></td>
			</tr>
			<tr>
				<th>File Storage</th>
				<td><?php echo (Yii::app()->getModule('files')->file->service) ? Yii::app()->getModule('files')->file->service : 'Local'; ?></td>
			</tr>
		</tbody>
	</table>
</div>

<div class="list-wrapper">
	<h4 class="heading">Outgoing Email</h4>
	<table class="table table-condensed table-striped detail-view">
		<tbody>
			<tr>
				<th>From Name</th>
				<td><?php echo (Yii::app()->mail->fromName) ? Yii::app()->mail->fromName : 'Not Set'; ?></td>
			</tr>
			<tr>
				<th>From Email</th>
				<td><?php echo (Yii::app()->mail->fromEmail) ? Yii::app()->mail->fromEmail : 'Not Set'; ?></td>
			</tr>
			<tr>
				<th>Catch All Enabled</th>
				<td><?php echo (Yii::app()->mail->catchAll) ? '<span class="text-green">Yes</span>' : '<span class="text-red">No</span>'; ?></td>
			</tr>
			<?php if (Yii::app()->user->checkAccess('super admin')): ?>
			<tr>
				<th>Catch All Email</th>
				<td><?php echo Yii::app()->mail->catchAllEmail; ?></td>
			</tr>
			<?php endif; ?>
			<tr>
				<th>Logging</th>
				<td><?php echo (Yii::app()->mail->logging) ? '<span class="text-green">Enabled</span>' : '<span class="text-red">Disabled</span>'; ?></td>
			</tr>
			<tr>
				<th>Dry Run</th>
				<td><?php echo (Yii::app()->mail->dryRun) ? '<span class="text-green">Enabled</span>' : '<span class="text-red">Disabled</span>'; ?></td>
			</tr>
			<tr>
				<th>Host</th>
				<td><?php echo (Yii::app()->mail->transportOptions['host']) ? Yii::app()->mail->transportOptions['host'] : 'Not Set'; ?></td>
			</tr>
			<tr>
				<th>Username</th>
				<td><?php echo (Yii::app()->mail->transportOptions['username']) ? Yii::app()->mail->transportOptions['username'] : 'Not Set'; ?></td>
			</tr>
			<tr>
				<th>Password</th>
				<td><?php echo (Yii::app()->mail->transportOptions['password']) ? str_pad('', strlen(Yii::app()->mail->transportOptions['password']), '*') : 'Not Set'; ?></td>
			</tr>
		</tbody>
	</table>
</div>

<?php /*
<div class="list-wrapper">
	<h4 class="heading">Data Management</h4>
	<table class="table table-condensed table-striped detail-view">
		<tr>
			<th>Database</th>
			<td>
				<?php if (Yii::app()->user->checkAccess('admin')): ?>
					<a href="<?php echo Yii::app()->createUrl('backup/index', array('backup'=>1, 'download'=>1)); ?>">Download</a>
				<?php else: ?>
					Not Available
				<?php endif; ?>
			</td>
		</tr>
		<tr>
			<th>Backup</th>
			<td>
				<?php if (Yii::app()->user->checkAccess('admin')): ?>
					<?php if (Yii::app()->user->checkAccess('super admin')): ?>
						<div><a href="<?php echo Yii::app()->createUrl('backup/index', array('backup'=>1)); ?>">Backup Now</a></div>
					<?php endif; ?>
					<div class="note">Latest Backup: <?php echo Yii::app()->dbManager->latestBackup(); ?></div>
				<?php else: ?>
					Not Available
				<?php endif; ?>
			</td>
		</tr>
	</table>
</div>
*/ ?>
