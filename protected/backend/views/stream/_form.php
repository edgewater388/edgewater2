<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<?php echo $form->labelEx($model, 'time_start'); ?>
			<div class="controls">
				<?php echo $form->textField($model, 'time_start', ['class' => 'date-time']); ?>
				<?php echo $form->error($model, 'time_start'); ?>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<?php echo $form->labelEx($model, 'time_end'); ?>
			<div class="controls">
				<?php echo $form->textField($model, 'time_end', ['class' => 'date-time']); ?>
				<?php echo $form->error($model, 'time_end'); ?>
			</div>
		</div>
	</div>
    <div class="col-md-4">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'timer'); ?>
            <div class="controls">
                <?php echo $form->textField($model, 'timer'); ?>
                <?php echo $form->error($model, 'timer'); ?>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'ready_message'); ?>
    <div class="controls">
		<?php echo $form->richTextArea($model, 'ready_message'); ?>
		<?php echo $form->error($model, 'ready_message'); ?>
    </div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'ready_background'); ?>
    <div class="controls">
		<?php echo $form->textField($model, 'ready_background'); ?>
		<?php echo $form->error($model, 'ready_background'); ?>
    </div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'countdown_message'); ?> (Use {{timer}} to include countdown)
	<div class="controls">
		<?php echo $form->richTextArea($model, 'countdown_message'); ?>
		<?php echo $form->error($model, 'countdown_message'); ?>
	</div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'countdown_background'); ?>
    <div class="controls">
		<?php echo $form->textField($model, 'countdown_background'); ?>
		<?php echo $form->error($model, 'countdown_background'); ?>
    </div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'embed_js'); ?> (On live page at admin panel: EMBED -> JAVASCRIPT)
    <div class="controls">
		<?php echo $form->textArea($model, 'embed_js'); ?>
		<?php echo $form->error($model, 'embed_js'); ?>
    </div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'livestream_iframe'); ?> 
    <div class="controls">
		<?php echo $form->textArea($model, 'livestream_iframe'); ?>
		<?php echo $form->error($model, 'livestream_iframe'); ?>
    </div>
</div>

<script>
    $('.date-time').datetimepicker({
      format: 'Y-MM-DD HH:mm'
    });
</script>