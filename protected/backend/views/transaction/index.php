<?php
	$this->menu = array(
		array('label'=>'<span class="glyphicon glyphicon-arrow-down"></span> Export', 'url'=>Export::getUrl()),
	);
?>

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'filters')); ?>
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
<?php $this->endWidget();?>

<?php $counts = $model->getCounts(); ?>
<div class="flush">
	<div class="grid-header text-right">
		<div class="row">
			<div class="col-xs-3">
				Count <br>
				<span style="font-weight: normal; font-size: 20px;"><?php echo $counts['count']; ?></span>
			</div>
			<div class="col-xs-3">
				Subtotal <br>
				<span style="font-weight: normal; font-size: 20px;">$<?php echo number_format($counts['subtotal'], 2); ?></span>
			</div>
			<div class="col-xs-3">
				Tax <br>
				<span style="font-weight: normal; font-size: 20px;">$<?php echo number_format($counts['tax'], 2); ?></span>
			</div>
			<div class="col-xs-3">
				Total <br>
				<span style="font-weight: normal; font-size: 20px;">$<?php echo number_format($counts['total'], 2); ?></span>
			</div>
		</div>
	</div>
</div>

<div class="flush">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(25),
	'hideHeader'=>false,
	'columns'=>array(
		array('class'=>'IdColumn'),
		'displayTitle',
		'total',
		'promo_code',
		// array(
		// 	'value'=>'$data->approved ? "Approved" : "Denied"',
		// 	'type'=>'raw',
		// ),
		'created',
		// array(
		// 	'class'=>'StatusColumn',
		// ),
		array(
			'class'=>'LinkColumn',
		)
	),
)); ?>
</div>
