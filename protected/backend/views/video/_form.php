<div class="form-group">
	<?php echo $form->label($model, 'jwplayer_video_id'); ?>
    <div class="controls">
		<?php echo $form->textField($model, 'jwplayer_video_id'); ?>
		<?php echo $form->error($model, 'jwplayer_video_id'); ?>
    </div>
</div>

<div class="form-group">
	<?php echo $form->label($model, 'instructor_id'); ?>
    <div class="controls">
		<?php echo $form->dropDownList($model, 'instructor_id', ['' => ''] + Users::getInstructors()); ?>
		<?php echo $form->error($model, 'instructor_id'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
			<?php echo $form->label($model, 'strength'); ?>
            <div class="controls">
				<?php echo $form->dropDownList($model, 'strength', ['' => ''] + Video::getLevels()); ?>
				<?php echo $form->error($model, 'strength'); ?>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
			<?php echo $form->label($model, 'cardio'); ?>
            <div class="controls">
				<?php echo $form->dropDownList($model, 'cardio', ['' => ''] + Video::getLevels()); ?>
				<?php echo $form->error($model, 'cardio'); ?>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
			<?php echo $form->label($model, 'restorative'); ?>
            <div class="controls">
				<?php echo $form->dropDownList($model, 'restorative', ['' => ''] + Video::getLevels()); ?>
				<?php echo $form->error($model, 'restorative'); ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
			<?php echo $form->label($model, 'body_part'); ?>
            <div class="controls">
				<?php echo $form->dropDownList($model, 'body_part', Video::getBodyParts(), [
					'multiple' => 'multiple',
					'options'  => !empty($model->body_part) ?
						array_fill_keys(explode(',', $model->body_part), ['selected' => 'selected']) :
						null,
				]); ?>
				<?php echo $form->error($model, 'body_part'); ?>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
			<?php echo $form->label($model, 'equipment'); ?>
            <div class="controls">
				<?php echo $form->dropDownList($model, 'equipment', Video::getEquipment(), [
					'multiple' => 'multiple',
					'options'  => !empty($model->equipment) ?
						array_fill_keys(explode(',', $model->equipment), ['selected' => 'selected']) :
						null,
				]); ?>
				<?php echo $form->error($model, 'equipment'); ?>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="controls">
		<?php echo $form->checkBox($model, 'is_live_class'); ?>
		<?php echo $form->label($model, 'is_live_class'); ?>
		<?php echo $form->error($model, 'is_live_class'); ?>
    </div>
</div>
<div class="form-group">
    <div class="controls">
		<?php echo $form->checkBox($model, 'is_new'); ?>
		<?php echo $form->label($model, 'is_new'); ?>
		<?php echo $form->error($model, 'is_new'); ?>
    </div>
</div>

<div class="form-group">
    <div class="controls">
		<?php echo $form->label($model, 'is_public', ['label' => 'Video Restrictions']); ?>
        <div class="inline">
			<?php echo $form->radioButtonList($model, 'is_public', ['0' => 'Member Only', '1' => 'Preview']); ?>
        </div>
		<?php echo $form->error($model, 'is_public'); ?>
    </div>
</div>

<div class="form-group">
	<?php echo $form->labelEx($model, 'weight'); ?>
    <div class="controls">
		<?php echo $form->dropDownList($model, 'weight', CString::range(-20, 20)); ?>
        <div class="note">Set the weight to change the order of preview videos. Lower weight = ranked higher</div>
		<?php echo $form->error($model, 'weight'); ?>
    </div>
</div>
