<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-content">
		<?php if ($model->columnExists('title')): ?>
		<div class="form-group">
			<?php echo $form->label($model,'title'); ?>
			<div class="controls">
				<?php echo $form->textField($model,'title',array('class'=>'form-control','maxlength'=>11)); ?>
			</div>
		</div>
		<?php endif; ?>

		<div class="form-group">
			<div class="controls">
				<div class="row">
					<div class="col-xs-6">
						<?php echo $form->labelEx($model, 'type'); ?>
						<?php echo $form->dropDownList($model,'type',array(''=>'')+Video::getTypes()); ?>
					</div>
					<div class="col-xs-6">
						<?php echo $form->labelEx($model, 'level'); ?>
						<?php echo $form->dropDownList($model,'level',array(''=>'')+Video::getLevels()); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model, 'is_public'); ?>
			<div class="controls">
				<?php echo $form->dropDownList($model,'is_public',array(''=>'', '1'=>'Yes (Preview)', '0'=>'No (Member Only)')); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model, 'instructor_id'); ?>
			<div class="controls">
				<?php echo $form->dropDownList($model,'instructor_id',array(''=>'')+Users::getInstructors()); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->label($model,'status'); ?>
			<div class="controls">
				<?php echo $form->dropDownList($model,'status',array(''=>'All','1'=>'Published','0'=>'Unpublished')); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model, 'created'); ?>
			<div class="controls">
				<div class="row">
					<div class="col-xs-6">
						<?php echo $form->dateField($model,'created_from',array('class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('created_from'))); ?>
					</div>
					<div class="col-xs-6">
						<?php echo $form->dateField($model,'created_to',array('class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('created_to'))); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="form-actions buttons">
		<?php echo Html::button('Clear', array('class'=>'btn btn-sm btn-default', 'data-toggle'=>'clearForm')); ?>
		<?php echo Html::submitButton('Search', array('class'=>'btn btn-sm btn-primary pull-right')); ?>
		<div class="clearfix"></div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->