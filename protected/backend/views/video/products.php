<div class="page-header">
	<h1><?php echo $model->displayTitle; ?></h1>
	<h2><?php echo $model->classLabel; ?></h2>
</div>

<div class="form">
<?php 
	$form=$this->beginWidget('ActiveForm', array(
		'id'=>'post-form',
		'enableAjaxValidation'=>false,
		'htmlOptions'=>array('enctype' => 'multipart/form-data'),
	)); 
?>

	<div class="form-group">
		<?php echo $form->label($model, 'product_ids', array('label'=>'Related Products')); ?>
		<div class="controls">
			<?php echo $form->checkboxList($model, 'product_ids', Product::getList()); ?>
			<?php echo $form->error($model,'product_ids'); ?>
		</div>
	</div>

	<div class="form-actions buttons">
		<?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
