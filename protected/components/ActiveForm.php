<?php

class ActiveForm extends CActiveForm
{
	public function init()
	{
		parent::init();
		// Yii::app()->clientScript->registerCssFile(Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application').'/../css/form.css'), CClientScript::POS_HEAD);
	}

	public function getDefaultHtmlOptions($htmlOptions)
	{
		if (!isset($htmlOptions['class']))
			$htmlOptions['class'] = '';

		// set some default classes
		$htmlOptions['class'] .= ' form-control ';

		return $htmlOptions;
	}

	public function textField($model,$attribute,$htmlOptions=array())
	{
		$htmlOptions = $this->getDefaultHtmlOptions($htmlOptions);
		return parent::textField($model, $attribute, $htmlOptions);
	}

	public function textArea($model,$attribute,$htmlOptions=array())
	{
		$htmlOptions = $this->getDefaultHtmlOptions($htmlOptions);
		return parent::textArea($model, $attribute, $htmlOptions);
	}

	public function numberField($model,$attribute,$htmlOptions=array())
	{
		return XHtml::activeInputTextField('number',$model,$attribute,$htmlOptions);
	}	

	public function emailField($model,$attribute,$htmlOptions=array())
	{
		return XHtml::activeInputTextField('email',$model,$attribute,$htmlOptions);
	}	

	public function phoneField($model,$attribute,$htmlOptions=array())
	{
		return XHtml::activeInputTextField('tel',$model,$attribute,$htmlOptions);
	}

	public function radioButtonList($model,$attribute,$data,$htmlOptions=array())
	{
		$htmlOptions = CMap::mergeArray(array(
			'separator'=>'',
			'template'=>'<div class="form-inline">{input} {label}</div>',
		), $htmlOptions);
		foreach ($data as $k=>$v)
			$data[$k] = '<span class="checkbox inline">'.$v.'</span>';
		return parent::radioButtonList($model, $attribute, $data, $htmlOptions);
	}

	public function checkBoxList($model,$attribute,$data,$htmlOptions=array())
	{
		$htmlOptions = CMap::mergeArray(array(
			'separator'=>' ',
			'template'=>'<div class="nowrap" style="margin-right: 8px;">{input} {label}</div>',
		), $htmlOptions);
		foreach ($data as $k=>$v)
			$data[$k] = $v;
			// $data[$k] = '<span class="checkbox inline">'.$v.'</span>';
		return parent::checkBoxList($model, $attribute, $data, $htmlOptions);
	}

	public function dropDownList($model, $attribute, $data=array(), $htmlOptions=array())
	{
		$htmlOptions = $this->getDefaultHtmlOptions($htmlOptions);
		return parent::dropDownList($model, $attribute, $data, $htmlOptions);
	}

	public function toggleField($model,$attribute,$data=array(),$htmlOptions=array())
	{
		// if (!$data) {
			$data = array(
				'1'=>'On',
				'0'=>'Off',
			);
		// }
		return $this->dropDownList($model, $attribute, $data, $htmlOptions);
	}

	public function statusField($model,$attribute,$data=array(),$htmlOptions=array())
	{
		$data = array(
			'1'=>'Published',
			'0'=>'Unpublished',
		);
		return $this->dropDownList($model, $attribute, $data, $htmlOptions);
	}

	public function labelEx($model,$attribute,$htmlOptions=array())
	{
		$htmlOptions = array(
			'class'=>'control-label',
		) + $htmlOptions;
		return parent::labelEx($model, $attribute, $htmlOptions);
	}

	public function errorSummary($model, $header=null, $footer=null, $htmlOptions=array())
	{
		// $close_button = '<a class="close" data-dismiss="alert" href="#">&times;</a>';
		// $header = $close_button . $header;
		$htmlOptions = CMap::mergeArray($htmlOptions, array(
			'class'=>'alert alert-danger',
		));
		return parent::errorSummary($model, $header, $footer, $htmlOptions);
	}

	public function error($model, $attribute, $htmlOptions=array(), $enableAjaxValidation=true, $enableClientValidation=true)
	{
		// $close_button = '<a class="close" data-dismiss="alert" href="#">&times;</a>';
		// $header = $close_button . $header;
		$htmlOptions = CMap::mergeArray($htmlOptions, array(
			'class'=>'text-danger',
		));
		return parent::error($model, $attribute, $htmlOptions, $enableAjaxValidation, $enableClientValidation);
	}

	public function dateField($model,$attribute,$htmlOptions=array())
	{
		$htmlOptions = $this->getDefaultHtmlOptions($htmlOptions);

		// Register core UI script
		Yii::app()->getClientScript()->registerCoreScript('jquery.ui');

		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
		    // 'name'=>get_class($model).'['.$attribute.']',
		    'name'=>Html::resolveName($model, $attribute),
		    'model'=>$model,
		    'value'=>Html::resolveValue($model, $attribute),
		    'skin'=>false,
		    // 'cssFile'=>'css/date-picker.css',
		    'cssFile'=>false,
	    	'theme'=>'basic',
	    	'themeUrl'=>'/themes',
		    // additional javascript options for the date picker plugin
		    'options'=>array(
		    	'dateFormat'=>'yy-mm-dd',
		        // 'showAnim'=>'fade',
		    ),
		    'htmlOptions'=>CMap::mergeArray(
		    	$htmlOptions,
		    	array(
		    		'autocomplete'=>'off',
		    		'class'=>'calendar-input '.(isset($htmlOptions['class'])?$htmlOptions['class']:''),
		    	)
		    ),
		));
		$wrapper_classes = isset($htmlOptions['wrapperClass']) ? $htmlOptions['wrapperClass'] : '';
		echo '<span class="calendar-input-wrapper '.$wrapper_classes.'" style="position:relative;"><i class="icon-calendar" style="position:absolute; margin: 8px 0 0 -18px;"></i></span>';
		return '';
	}

	/*
	 * Change the filefile so it also displays a link and filename to the current file.
	 */ 
	public function fileField($model,$attribute,$htmlOptions=array())
	{
		$str = parent::fileField($model, $attribute,$htmlOptions);

		$relation = ActiveRecord::getRelationFromName($attribute.'_fid');
		// $current_file = isset($model->$relation) ? '<a href="'.$model->$relation->url.'">'.$model->$relation->filename.'</a>' : 'none';
		// $str .= '<div class="note">Current File: '.$current_file.'</div>';

		return $str;
	}

	public function typeahead($model,$attribute,$htmlOptions=array())
	{
		$htmlOptions = $this->getDefaultHtmlOptions($htmlOptions);
		
		if (isset($htmlOptions['class']))
			$htmlOptions['class'] .= ' typeahead';

		$htmlOptions = CMap::mergeArray(array(
			'class'=>'typeahead',
			'data-url'=>'',
			'autocomplete'=>'off',
		), $htmlOptions);
		
		if (isset($htmlOptions['name']))
			$name = $htmlOptions['name'];
		else {
			// pass a copy of the attribute since resolveName will modify $attribute
			$attributeName = $attribute;
			$name = Html::resolveName($model,$attributeName);
		}

		$textValue = isset($htmlOptions['textValue']) ? $htmlOptions['textValue'] : null;
		$value = isset($_REQUEST['typeahead'][$attribute]) ? $_REQUEST['typeahead'][$attribute] : $textValue;
		$html = Html::textField('typeahead['.$attribute.']', $value, $htmlOptions);
		$html .= $this->hiddenField($model, $attribute, array('name'=>$name));
		$html .= $this->_typeahead($name, $attribute);
		return $html;
	}

	protected function _typeahead($name, $attribute)
	{
		Yii::app()->clientScript->registerScriptFile('/themes/backend/js/typeahead.js', CClientScript::POS_HEAD);

		$js = <<<EOL
<script>
	jQuery(document).ready(function($) {
		$('input[name="typeahead[$attribute]"].typeahead').typeahead({
			source: function(query, process) {
				var data_url = $(this)[0].\$element.attr('data-url');
				data_url += query;

				$.getJSON(data_url, function(data) {
					items = [];
					map = {};

					$.each(data, function(i, elem) {
						map[elem.name] = elem;
						items.push(elem.name);
					});
					
					process(items);
				});
			},
			updater: function(item) {
				$('input[name="$name"]')
					.val(map[item].id)
					.trigger('change');
				return item;
			}
		});
	
		// clear id if text field is empty
		$('input[name="typeahead[$attribute]"]').on('blur', function(e) {
			if ($(this).val().trim() == '')
				$('input[name="$name"]').val('');
		});
	});
</script>
EOL;
		return $js;
	}

	public function richTextArea($model,$attribute,$htmlOptions=array())
	{
		Yii::app()->getClientScript()->registerScriptFile('/js/ckeditor/ckeditor.js');

		$htmlOptions = $this->getDefaultHtmlOptions($htmlOptions);
		$htmlOptions['class'] .= 'ckeditor';

		return parent::textArea($model,$attribute,$htmlOptions);
	}

	public function multiselect($model,$attribute,$data=array(),$htmlOptions=array())
	{
		// Docs
		// http://zellerda.com/projects/tokenize
		
		Yii::app()->clientScript->registerScriptFile('/js/tokenize/jquery.tokenize.js', CClientScript::POS_HEAD);

		$htmlOptions = $this->getDefaultHtmlOptions($htmlOptions);
		$htmlOptions['class'] .= 'tokenize';
		$htmlOptions['multiple'] = 'multiple';

		$html = parent::listBox($model, $attribute, $data, $htmlOptions);

		$dataUrl = isset($htmlOptions['data-url']) ? $htmlOptions['data-url'] : null;

		$html .= <<<EOL
<script type="text/javascript">
	$('.tokenize:not(".tokenized")').tokenize({
		datas: "$dataUrl",
		valueField: "id",
		textField: "name",
		searchParam: "name",
		onAddToken: function(value, text) {
			if (value == text)
			{
				$('option[value="'+value+'"][data-type="custom"]').attr('value', '_'+value);
				$('li.Token[data-value="'+value+'"]').attr('data-value', '_'+value);
			}
		}
	});

	$('.tokenize').addClass('tokenized');
</script>
EOL;
		return $html;
	}
}