<?php

class ActiveRecord extends CActiveRecord
{
	private $_files;
	private $_privateFiles;
	public $statusText;
	protected $_oldAttributes = array(); // old attributes
	public $hardDelete = false;
	public $restrictToActiveRecords = true;

	public function getClass()
	{
		return get_class($this);
	}

	public function beforeSave()
	{
		$valid = parent::beforeSave();

		// Set the created / updated timestamps, if the model has these columns
		if ($this->columnExists('created') && ($this->created == null))//$this->isNewRecord || $this->created == null))
			$this->created = $this->isDatetime('created') ? date('Y-m-d H:i:s') : time();

		if ($this->columnExists('updated'))
			$this->updated = $this->isDatetime('updated') ? date('Y-m-d H:i:s') : time();

		// if ($this->columnExists('user_id') && $this->user_id == null)
		// 	$this->user_id = Yii::app()->user->id;
		
		$className = get_class($this);
		if (isset($_FILES[$className]))
		{
			foreach ($_FILES[$className]['name'] as $attribute=>$filename)
			{
				// Only update files when user has selected a file to upload
				// ie. do not upload files if file field is empty
				if ($_FILES[$className]['size'][$attribute])
				{
					// upload the file
					$isPrivateFile = in_array($attribute, $this->privateFiles());
					$fileModel = Files::upload($this, $attribute, $isPrivateFile);

					if ($fileModel) {
						$attributeName = $attribute.'_fid';
						$this->$attributeName = $fileModel->id;
					} else {
						$this->addError($attribute, 'Error: '.$this->getAttributeLabel($attribute).' could not be uploaded.');
					}
				}
			}
		}

		// Prevent empty foreign keys being saved as strings.
		if ($schema = $this->getTableSchema())
		{
			foreach ($schema->foreignKeys as $attribute=>$info)
			{
				if (!$this->$attribute)
					$this->$attribute = null;
			}
		}
		
		// Prevent 0000-00-00 values for empty dates
		foreach ($schema->columns as $attribute=>$info)
		{
			if ($info->dbType == 'date' && !$this->$attribute)
				$this->$attribute = null;
		}


		return $valid;
	}

	public function afterFind()
	{
		parent::afterFind();

		$schema = $this->getTableSchema();
		if (isset($schema->columns['status']))
			$this->statusText = ($this->status) ? 'published' : 'unpublished';

		$this->_oldAttributes = $this->attributes;
	}

	protected function privateFiles()
	{
		return array();
	}

	public function columnExists($columnName)
	{
		return array_key_exists($columnName, $this->columns);
	}

	public function getColumns($excludeBaseColumns=false)
	{
		return $this->getTableSchema()->columns;
	}

	public function getColumnNames()
	{
		$columns = array();
		foreach ($this->columns as $column)
			$columns[] = $column->name;
		return $columns;
	}

	public function getBaseColumns()
	{
		return array(
			'id',
			'title',
			'body',
			'status',
			'created',
			'updated',
			'deleted',
		);
	}

	public function isDatetime($columnName)
	{
		return ($this->getTableSchema()->columns[$columnName]->dbType == 'datetime');
	}

	public function addColumn($columnName, $type)
	{
		Yii::app()->db->createCommand()->addColumn($this->tableName(), $columnName, $type);

		// Refresh meta data
		Yii::app()->db->schema->refresh();
		$this->refreshMetaData();

		return $this->columnExists($columnName);
	}

	public function defaultScope()
	{
		$scope = parent::defaultScope();
		$condition = isset($scope['condition']) ? array($scope['condition']) : array();

		$t = $this->getTableAlias(false, false);

		if ($this->columnExists('status') && Yii::app()->request->scriptUrl != '/backend.php' && $this->restrictToActiveRecords)
			$condition['status'] = $t.'.status = 1';

		if (count($condition) > 0)
			$scope['condition'] = implode(' AND ', $condition);

		return $scope;
	}

	public function scopes()
	{
		return array(
			'published'=>array(
				'condition'=>'t.status=1',
			),
		);
	}

	// Parameterized Scopes
	public function byUser($id=null)
	{
		if (!$id)
			$id = Yii::app()->user->id;

		
		$t = $this->getTableAlias(false, false);
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>$t.'.user_id = :user_id',
			'params'=>array(':user_id'=>$id),
		));

		return $this;
	}

	public function limit($limit = 10)
	{
	    $this->getDbCriteria()->mergeWith(array(
			'limit'=>$limit,
	    ));
	    return $this;
	}

	public function byAttribute($attribute, $value)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'t.'.$attribute.' = :'.$attribute,
			'params'=>array(':'.$attribute=>$value),
		));

		return $this;
	}

	public function search($pageSize = 10)
	{
		$pagination = $pageSize ? array('pageSize'=>$pageSize, 'validateCurrentPage'=>false) : false;

		return new CActiveDataProvider($this, array(
			'criteria'=>$this->searchCriteria(),
			'pagination'=>$pagination,
		));
	}

	public function searchCriteria()
	{
		$criteria=new CDbCriteria;

		$t = $this->getTableAlias(false, false);

		if ($this->columnExists('id'))
			$criteria->compare($t.'.id',$this->id,false);

		if ($this->columnExists('user_id'))
			$criteria->compare($t.'.user_id',$this->user_id,false);

		if ($this->columnExists('status'))
			$criteria->compare($t.'.status',$this->status);

		if ($this->columnExists('created'))
			$criteria->compare($t.'.created',$this->created);

		if ($this->columnExists('updated'))
			$criteria->compare($t.'.updated',$this->updated);

		return $criteria;
	}

	public function getPlaceholder($attribute, $showRequired=true)
	{
		return $this->getAttributeLabel($attribute).($showRequired && $this->isAttributeRequired($attribute)?' *':'');
	}

	public static function getRelationFromName($name)
	{
		// remove underscores, capitalize each 'word', remove spaces
		return lcfirst(str_ireplace(' ', '', ucwords(str_ireplace('_', ' ', substr($name, 0, strlen($name)-4))))).'F';
	}

	public function getAttributeLabel($attribute)
	{
		if (substr($attribute, strlen($attribute)-3) == '_id')
			$attribute = substr($attribute, 0, strlen($attribute)-3);
		return parent::getAttributeLabel($attribute);
	}

	public function getClassLabel()
	{
		return $this->getAttributeLabel(get_class($this));
	}

	public function getClassSlug()
	{
		return strtolower(str_replace(' ', '-', $this->classLabel));
	}

	public function getDisplayTitle()
	{
		if ($this->columnExists('title'))
			return $this->title;
		return $this->classLabel.' '.$this->primaryKey;
	}
}