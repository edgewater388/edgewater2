<?php

class ApiController extends Controller
{
	// public function filters()
	// {
	// 	return array(
	// 		'accessControl', // perform access control for CRUD operations
	// 		// 'postOnly + delete', // we only allow deletion via POST request
	// 	);
	// }

	public function missingAction($actionID)
	{
		$this->renderData(array('error'=>Yii::t('yii','The system is unable to find the requested action "{action}".',
			array('{action}'=>$actionID==''?$this->defaultAction:$actionID))));
	}

	public function beforeAction($action)
	{
		$valid = parent::beforeAction($action);

		if (isset($_GET['page']))
			$_GET[$this->getModelClass().'_page'] = $_GET['page'];

		if (Yii::app()->user->isGuest)
		{
			$this->renderData(array('error'=>'You must be logged in to view this page'));
			$valid = false;
		}

		if (!isset(Yii::app()->user->membership->valid_to) || Yii::app()->user->membership->valid_to < date('Y-m-d'))
		{
			$this->renderData(array('error'=>'No active membership found. Unable to complete request.'));
			$valid = false;
		}

		return $valid;
	}

	// public function accessRules()
	// {
	// 	return array(
	// 		array('allow', // authenticated users
	// 			'actions'=>array('index','view','create','update','delete'),
	// 			'users'=>array('*'),
	// 		),
	// 		array('deny',  // deny all users
	// 			'users'=>array('*'),
	// 		),
	// 	);
	// }

	public function render($view,$data=null,$return=false)
	{
		$this->renderData($data);
	}

	public function renderData($data, $terminate=true)
	{
		$data = DataHandler::render($data);

		if (Yii::app()->request->getParam('debug'))
		{
			echo '<pre>';
			print_r($data);
			echo '</pre>';
			exit;
		}
		
		header('Content-Type: application/json');
		echo CJSON::encode($data);

		if ($terminate)
			exit;
	}

	public function loadModel($id)
	{
		$class = $this->getModelClass();
		if (!$model = $class::model()->findByPk($id))
		{
			$this->renderData(array('error'=>'Record not found'));
			exit;
		}
		return $model;
	}

	public function getModelClass()
	{
		return str_ireplace('Controller', '', get_called_class());
	}
}