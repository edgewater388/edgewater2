<?php

// http://www.authorize.net/content/dam/authorize/documents/AIM_guide.pdf

class AuthorizeNet extends CApplicationComponent
{
	public $api_login_id;
	public $transaction_key;

	public $test_api_login_id;
	public $test_transaction_key;

	public $secret_key;
	protected $sandbox = true;

	public $dry_run = false;
	public $dry_run_approved = true;

	public function init()
	{
		parent::init();

		if (Yii::app()->environment == 'production')
			$this->sandbox = false;

		$api_login_id = ($this->sandbox) ? $this->test_api_login_id : $this->api_login_id;
		$transaction_key = ($this->sandbox) ? $this->test_transaction_key : $this->transaction_key;

		define("AUTHORIZENET_API_LOGIN_ID", $api_login_id);
		define("AUTHORIZENET_TRANSACTION_KEY", $transaction_key);
		define("AUTHORIZENET_SANDBOX", $this->sandbox);

		spl_autoload_unregister(array('YiiBase','autoload'));
		require dirname(dirname(__FILE__)).'/vendors/authorize/autoload.php';
		spl_autoload_register(array('YiiBase','autoload'));
	}

	public function getCustomerProfile($customer_profile_id)
	{
		$customerProfile = new AuthorizeNetCustomer;

		if ($customer_profile_id)
		{
			$request = new AuthorizeNetCIM;
			$response = $request->getCustomerProfile($customer_profile_id);

			$customerProfile->customerProfileId = $response->getCustomerProfileId();
			$customerProfile->merchantCustomerId = (string) $response->xml->profile->merchantCustomerId;
			$customerProfile->description = (string) $response->xml->profile->description;
			$customerProfile->email = (string) $response->xml->profile->email;
		}

		return $customerProfile;
	}

	public function updateCustomerProfile($params=array())
	{
		if ($this->dry_run)
		{	
			return array(
				'response'=>new AuthorizeNetCIM_Response(null),
				'success'=>$this->dry_run_approved,
				'customer_profile_id'=>'000000',
				'customer_payment_profile_id'=>'000000',
			);
		}

		$request = new AuthorizeNetCIM;

		$customerProfile = $this->getCustomerProfile($params['customer_profile_id']);
		$customerProfile->merchantCustomerId = $customerProfile->merchantCustomerId ? $customerProfile->merchantCustomerId : $this->generateUniqueCustomerId($params);
		$customerProfile->description = 'Customer: '.$params['name'];
		$customerProfile->email = $params['email'];

		$validationMode = $this->sandbox ? 'testMode' : 'liveMode';

		// Update the account info
		if ($customerProfile->customerProfileId)
		{
			$response = $request->updateCustomerProfile($customerProfile->customerProfileId, $customerProfile);
		}
		else
		{
			$response = $request->createCustomerProfile($customerProfile);
			$customerProfile->customerProfileId = $response->getCustomerProfileId();
		}

		$paymentProfile = new AuthorizeNetPaymentProfile;
		$paymentProfile->customerType = "individual";
		$paymentProfile->billTo->firstName = $params['first_name'];
		$paymentProfile->billTo->lastName = $params['last_name'];
		$paymentProfile->billTo->address = $params['address'];
		// $paymentProfile->billTo->city = '';
		// $paymentProfile->billTo->state = '';
		$paymentProfile->billTo->country = isset($params['country']) ? $params['country'] : null;
		$paymentProfile->billTo->zip = $params['zipcode'];
		$paymentProfile->payment->creditCard->cardNumber = $params['card_number'];
		$paymentProfile->payment->creditCard->expirationDate = $params['card_month'].$params['card_year'];
		$paymentProfile->payment->creditCard->cardCode = $params['card_security'];

		if ($customer_payment_profile_id = $params['customer_payment_profile_id'])
		{
			$response = $request->updateCustomerPaymentProfile($customerProfile->customerProfileId, $customer_payment_profile_id, $paymentProfile, $validationMode);
		}
		else
		{
			$response = $request->createCustomerPaymentProfile($customerProfile->customerProfileId, $paymentProfile, $validationMode);
			$customer_payment_profile_id = isset($response->xml->customerPaymentProfileId) ? $response->xml->customerPaymentProfileId : null;
			$customer_payment_profile_id = (string) $customer_payment_profile_id;
		}

		// echo '<pre>';
		// print_r($response);
		// echo '<pre>';
		// exit;

		return array(
			'response'=>$response,
			'success'=>$response->isOk(),
			'customer_profile_id'=>$customerProfile->customerProfileId,
			'customer_payment_profile_id'=>$customer_payment_profile_id,
		);
	}

	// public function getCustomerPaymentProfile($customer_profile_id, $customer_payment_profile_id)
	// {
	// 	$request = new AuthorizeNetCIM;
	// 	$response = $request->getCustomerPaymentProfile($customer_profile_id, $customer_payment_profile_id);
	// 	$paymentProfile = isset($response->xml->paymentProfile) ? $response->xml->paymentProfile : null;
	// }

	public function charge($params=array())
	{
		if ($this->dry_run)
		{
			return array(
				'approved'=>$this->dry_run_approved,
				'message'=>'Dry run approved: '.($this->dry_run_approved ? 'yes' : 'no'),
				'request'=>new AuthorizeNetTransaction,
				'response'=>$params,
			);
		}

		$request = new AuthorizeNetCIM;

		// Create the transaction
		$transaction = new AuthorizeNetTransaction;
		$transaction->amount = $params['total'];
		$transaction->tax->amount = $params['tax'];
		$transaction->customerProfileId = $params['customer']->customer_profile_id;
		$transaction->customerPaymentProfileId = $params['customer']->customer_payment_profile_id;
		$transaction->order->invoiceNumber = isset($params['transaction_id']) ? $params['transaction_id'] : null;

		$result = $request->createCustomerProfileTransaction('AuthCapture', $transaction);
		$response = array();

		try {
			foreach ($result->getTransactionResponse() as $k=>$v)
				$response[$k] = $v;
		} catch (Exception $e) {
			
		}

		// echo '<pre>';
		// print_r($response);
		// echo '<pre>';
		// exit;

		return array(
			'approved'=>isset($response['approved']) ? $response['approved'] : 0,
			'message'=>isset($response['response_reason_text']) ? $response['response_reason_text'] : null,
			'request'=>$transaction,
			'response'=>$response,
		);
	}

	public function generateUniqueCustomerId($params)
	{
		return 'user_'.$params['user_id'].'_'.CString::randomHash(2);
		// if (isset($params['user_id']) && $params['user_id'])
		// 	return 'user_'.$params['user_id'].'_'.CString::randomHash(2);
	}
}

