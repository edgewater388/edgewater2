<?php 

class BaseRecord extends PActiveRecord
{
	public $created_from;
	public $created_to;
	public $defaultPageSize = 25;
	protected $q; // search query

	public function init()
	{
		parent::init();

		if ($this->isNewRecord && $this->columnExists('weight'))
			$this->weight = 0;
	}

	public function rules()
	{
		return array(
			// standard values
			array('status', 'numerical', 'integerOnly'=>true),
			array('created, updated', 'safe'),
			array('id, status, created, updated', 'safe', 'on'=>'search'),

			// custom values
			array('created_from, created_to', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'status' => 'Status',
			'created' => 'Created',
			'updated' => 'Updated',

			// common attribute labels
			'image_fid' => 'Image',
			'attachment_fid' => 'Attachment',
			'pdf_fid' => 'PDF',
		);
	}

	public function search($pageSize=25)
	{
		return parent::search($pageSize);
	}

	public function searchCriteria()
	{
		$criteria = parent::searchCriteria();

		// add exact column matches
		foreach (array('id', 'user_id', 'status', 'created', 'updated') as $column)
		{
			if ($this->columnExists($column))
				$criteria->compare('t.'.$column, $this->$column, false);
		}

		if ($this->created_from) {
			$created_to = date('Y-m-d', strtotime($this->created_to.' + 1 day'));
			$criteria->addBetweenCondition('t.created',$this->created_from,$created_to);
		}
		elseif ($this->created_to) {
			$created_to = date('Y-m-d', strtotime($this->created_to.' + 1 day'));
			$criteria->addCondition('t.created < :created_to');
			$criteria->params[':created_to'] = $created_to;
		}

		return $criteria;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PressRelease the static model class
	 */
	public static function model($className=null)
	{
		if (!$className)
		{
			// override the class name
			$className = get_called_class();
		}

		return parent::model($className);
	}

	public function getClassLabel()
	{
		return $this->getAttributeLabel(get_class($this));
	}

	public function getClassSlug()
	{
		return strtolower(str_replace(' ', '-', $this->classLabel));
	}

	public function getDisplayTitle()
	{
		if ($this->columnExists('title'))
			return $this->title;
		return $this->classLabel.' '.$this->primaryKey;
	}

	public function getQ()
	{
		return $this->q;
	}

	public function setQ($q=null)
	{
		$this->q = $q;
	}
}