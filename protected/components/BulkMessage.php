<?php

class BulkMessage extends CApplicationComponent 
{
	public $subject;
	protected $message;

	public function __construct($view)
	{
		$this->message = array(
			'html'=>Yii::app()->controller->renderPartial('frontend.views.mail.'.$view, array(), true),
			'from_email'=>Yii::app()->mail->transportOptions['username'],
            'from_name'=>Yii::app()->name,
            'preserve_recipients'=>false,
            'inline_css'=>true,
            'auto_text'=>true,
		);
	}

	public function addTo($email, $name, $vars=array())
	{
		$this->message['to'][] = array(
            'email'=>$email,
            'name'=>$name,
            'type'=>'to',
		);

		if ($vars)
		{
			$mergeVars = array();

			foreach ($vars as $k=>$v)
			{
				$mergeVars[] = array(
					'name'=>$k,
					'content'=>$v,
				);
			}

			$this->message['merge_vars'][] = array(
				'rcpt'=>$email,
				'vars'=>$mergeVars,
			);
		}
	}

	public function addGlobalVars($vars=array())
	{
		foreach ($vars as $k=>$v)
			$this->addGlobalVar($k, $v);
	}

	public function addGlobalVar($name, $content)
	{
		$this->message['global_merge_vars'][] = array(
			'name'=>$name,
			'content'=>$content,
		);
	}

	public function beforeSend()
	{
		$this->message['subject'] = $this->subject;
		$this->message['global_merge_vars'][] = array(
			'name'=>'subject',
			'content'=>$this->subject,
		);
	}

	public function send($async=true)
	{
		$this->beforeSend();

		$mandrill = new Mandrill(Yii::app()->mail->transportOptions['password']);
		$mandrillMessage = new Mandrill_Messages($mandrill);

		return $mandrillMessage->send($this->message, $async);
	}
}