<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	public $layout = '//layouts/main';
	public $ogTitle = '';
	public $ogUrl = '';
	public $ogDescription = '';
	public $ogImage = '';
	public $metaKeywords = '';
	public $metaDescription = '';
	public $pageTitle = '';
	public $bodyClass = '';

	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public $css=array();
	public $js=array();

	public function init()
	{
		parent::init();

		$this->ogTitle = $this->pageTitle;
		$this->ogUrl = $_SERVER['HTTP_HOST'];
		$this->ogDescription = Yii::app()->params['ogDescription'];
		$this->ogImage = 'http://'.$_SERVER['HTTP_HOST'].'/themes/basic/images/og-share.jpg';

		// $hasher = new PasswordHash(8, true);
		// $result = $hasher->CheckPassword('gl0bal', '$P$BgSjThOwzoo1nBbdBZ2VbBQU2ELcYM0');
		// var_dump($result);
		// exit;
	}

	public function setPageTitle($title)
	{
		$title = ($title) ? $title.' - ' : '';
		$this->pageTitle = $title.Yii::app()->name;
	}

	public function beforeAction($action)
	{
		$valid = parent::beforeAction($action);

		if (Yii::app()->user->isGuest == false && !in_array($this->id, array('customer', 'users', 'membership', 'profile', 'newsletter', 'default', 'promoCode')))
		{
			if ($this->id == 'site' && $this->action->id == 'index')
				return $valid;
			
			$pages = array('faq');
			if ($this->id == 'site' && $this->action->id == 'page' && in_array(Yii::app()->request->getParam('view'), $pages))
				return $valid;
			
			// Check if user has a membership
			if (Yii::app()->user->membership && Yii::app()->user->membership->isActive())
				return $valid;

			if (Yii::app()->user->customer)
				$this->redirect(array('membership/change'));

			$this->redirect(array('customer/create'));
		}

		if (!Yii::app()->user->isGuest && Yii::app()->user->identity->is_demo) {
			if (Yii::app()->user->membership->valid_to <= date('Y-m-d')) {
				Yii::app()->user->logout();
			}
		}

		return $valid;
	}
}