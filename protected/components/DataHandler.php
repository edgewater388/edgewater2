<?php

class DataHandler 
// extends CApplicationComponent
{
	public static function render($args)
	{
		// search
		if (array_key_exists('dataProvider', $args))
		{
			$data = $args['dataProvider']->getData();
			return self::renderRows($data, $args['columns']);
		}
		// view
		elseif (array_key_exists('data', $args))
		{
			$data = $args['data'];
			return self::renderRow($data, $args['columns']);
		}
		elseif (array_key_exists('models', $args))
		{
			$data = $args['models'];
			return self::renderRows($data, $args['columns']);
		}
		elseif (is_array($args))
		{
			return $args;
		}

		return $args;
	}

	public static function renderRows($rows, $columns)
	{
		$array = array();
		foreach ($rows as $k=>$row)
			$array[] = self::renderRow($row, $columns);
		return $array;
	}

	public static function renderRow($row, $columns)
	{
		$array = array();

		// Only render the rows columns if the row exists (not true for a record that does not exist)
		if ($row)
		{
			foreach ($columns as $name=>$column)
			{
				if (is_array($column))
				{
					// If name is set, this is a relation
					if (!is_numeric($name))
					{
						$name = self::resolveName($name);

						// MANY records
						if (isset($row->$name))
						{
							if (is_array($row->$name))
								$array[$name] = self::renderRows($row->$name, $column);
							// ONE record
							elseif (is_object($row->$name))
								$array[$name] = self::renderRow($row->$name, $column);
						}
						else
							// retain the key=>null format to return a consistent (albeit empty) result
							$array[$name] = self::renderRow(array(), $column);
					}
					// otherwise, this is a more complex attribute of the current model
					else
					{
						$name = self::resolveName($column['name']);
						$type = isset($column['type']) ? $column['type'] : self::resolveType($column['name']);
						$value = CHtml::value($row, $name);
						$array[$name] = Yii::app()->format->format($value, $type);
					}
				}
				else
				{
					$name = self::resolveName($column);
					$type = self::resolveType($column);
					$value = CHtml::value($row, $name);

					// if (is_bool($value))
					// 	$value = $value ? 1 : 0;

					$key = self::resolveKey($name);
					$array[$key] = Yii::app()->format->format($value, $type);
				}
			}
		}

		return $array;
	}

	protected static function resolveName($column)
	{
		// base case, column is the name
		$name = $column;

		if (is_array($column))
			$name = $column['name'];

		if (strpos($name, ':') > 0)
		  $name = substr($name, 0, strpos($name, ':'));

		return $name;
	}

	protected static function resolveKey($name)
	{
		return str_ireplace('.', '_', $name);
	}

	protected static function resolveType($column)
	{
		$type = 'text';
		$name = $column;

		if (is_array($column))
			$name = $column['name'];

		if (strpos($name, ':') > 0)
			$type = substr($name, strpos($name, ':')+1);
		else if (is_array($column) && isset($column['type']))
			$type = $column['type'];

		return $type;
	}
}