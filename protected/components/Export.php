<?php

class Export
{
	public static function csv($rows, $headers=null, $filename='export', $appendDate=true)
	{		
		if (empty($rows))
			$rows[] = array('No results');

		if (isset($headers[0]) && $headers[0] == 'ID')
			$headers[0] = '"ID"';

		if ($headers)
			array_unshift($rows, $headers);

		if ($appendDate)
			$filename .= '--'.date('Y-m-d');
		
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"".$filename.".csv\"");
		self::outputCSV($rows);
		exit;
	}

	public static function outputCSV($data) {
	    $outstream = fopen("php://output", 'w');
	    function __outputCSV(&$vals, $key, $filehandler) {
	        fputcsv($filehandler, $vals, ',', '"');
	    }
	    array_walk($data, '__outputCSV', $outstream);
	    fclose($outstream);
	}

	public static function getHeaders($model)
	{
		$headers = array();
		foreach ($model->safeAttributeNames as $attribute_name)
			$headers[] = $model->getAttributeLabel($attribute_name);
		return $headers;
	}

	public static function getUrl()
	{
		$url = Yii::app()->request->requestUri;
		$op = (stristr($url, '?')) ? '&' : '?';
		return $url.$op.'export=1';
	}
}