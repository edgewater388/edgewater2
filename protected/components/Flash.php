<?php

class Flash
{	
	public static function setFlash($type, $message) {
		$flashes = array();
		
		foreach (Yii::app()->user->getFlashes(FALSE) as $key => $messages) {
			if ($key == $type) {
				$flashes = $messages;
			}
		}
		
		$flashes[] = $message;
		Yii::app()->user->setFlash($type, $flashes);
	}
	
	public static function getFlashes($type, $default = NULL, $delete = TRUE) {
		$flashes = Yii::app()->user->getFlash($type, $default, $delete);
		return $flashes;
	}
	
	public static function displayFlashes($type = null, $delete = TRUE) {
		$types = ($type) ? array($type) : array('error', 'warning', 'success', 'info');
		
		foreach ($types as $type) {
			$flashes = self::getFlashes($type, NULL, $delete);
			if (is_array($flashes)) {
				echo '<div class="alert alert-message block-message fade in alert-'.$type.'"><a class="close" data-dismiss="alert" href="#">&times;</a><ul>';
				foreach ($flashes as $flash) {
					echo '<li>'.nl2br($flash).'</li>';
				}
				echo '</ul></div>';
			}
		}		
	}
	
/*
	// this method is called by CController::beginWidget()
	public function init() {}
	
	// this method is called by CController::endWidget()
  public function run()
  {
  	$this->renderContent();
  }

	protected function renderContent()
	{
		foreach(Yii::app()->user->getFlashes() as $key => $message) {
		    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
		}
	} 
*/  
}
?>