<?php

Yii::import('zii.widgets.grid.CGridView');

class GridView extends CGridView
{
	// Set a few default values for site-wide consistency
	public $skin = FALSE;
	public $cssFile = FALSE;
	public $itemsCssClass = 'table table-condensed';
	public $template = '{items} {summary} {pager}';
}