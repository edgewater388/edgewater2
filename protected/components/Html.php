<?php

class Html extends CHtml
{
	public static function activeInputTextField($type,$model,$attribute,$htmlOptions=array())
	{
		self::resolveNameID($model,$attribute,$htmlOptions);
		self::clientChange('change',$htmlOptions);
		return self::activeInputField($type,$model,$attribute,$htmlOptions);
	}

	public static function submitButton($label=null, $htmlOptions=array())
	{
		$htmlOptions = CMap::mergeArray(array(
			'class'=>'btn btn-primary',
		), $htmlOptions);
		return parent::submitButton($label, $htmlOptions);
	}
}