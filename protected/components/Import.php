<?php

class Import
{
	public static function importFile($filepath)
	{
		// $handle = fopen($filepath, "r");
		// if ($handle) {
		//     while (($line = fgets($handle)) !== false) {
		//         // process the line read.
		//     }
		// } else {
		//     // error opening the file.
		// }
		return self::importSql(self::readFile($filepath));
	}

	public static function importSql($sql)
	{
		Yii::app()->db->createCommand($sql)->execute();

		return true;
	}

	public static function parseCSV($filepath)
	{
		return self::csv_to_array($filepath);
		// return array_map('str_getcsv', file($filepath));
	}

	public static function readFile($filepath)
	{
		return @file_get_contents($filepath);
	}

	public static function csv_to_array($filename='', $delimiter=',')
	{
	    if(!file_exists($filename) || !is_readable($filename))
	        return FALSE;

	    $header = NULL;
	    $data = array();
	    if (($handle = fopen($filename, 'r')) !== FALSE)
	    {
	        while (($row = fgetcsv($handle, 5000, $delimiter)) !== FALSE)
	        {
	            if(!$header)
	            {
	                $header = array();
	                foreach ($row as $v)
	                	$header[] = CString::machineName($v);

	            } else {
	            	$data[] = array_combine($header, $row);
            		// $data = @array_combine($header, $row);
            		// if (!$data)
            		// {
            		// 	echo '<pre>';
            		// 	print_r($row);
            		// 	echo '</pre>';
            		// 	exit;
            		// }
					// $data[] = $data;
	            }
	        }
	        fclose($handle);
	    }
	    return $data;
	}
}