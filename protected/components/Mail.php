<?php

Yii::import('application.extensions.yii-mail.YiiMail');

class Mail extends YiiMail
{
	public $catchAll = false;
	public $catchAllEmail = 'drt417@gmail.com';
	public $fromEmail = '';
	public $fromName = '';

	public function getCatchAllEmail()
	{
		if (Yii::app()->user->isGuest == false && Yii::app()->user->email)
			return Yii::app()->user->email;
		return $this->catchAllEmail;
	}
}