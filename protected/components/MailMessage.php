<?php

class MailMessage extends YiiMailMessage
{
	public $signature;
	public $tagline;

	public function __construct()
	{
		parent::__construct();

		// set default from email address
		$this->setFrom(Yii::app()->params['fromEmail'], Yii::app()->name);
	}

	public function addTo($email, $name)
	{
		if (Yii::app()->mail->catchAll || Yii::app()->environment == 'development')
			$email = Yii::app()->mail->getCatchAllEmail();

		$name = self::encodeName($name);
		$name .= ' <'.$email.'>';
		return parent::addTo($email, $name);
	}

	public function setBody($body = '', $contentType = 'text/html', $charset = null) {
		return parent::setBody($body, $contentType, $charset);
	}

	public static function encodeName($name)
	{
		if ($name != mb_convert_encoding($name, 'UTF7-IMAP'))
			return '=?utf-8?B?'.base64_encode($name).'?= ';
		return $name;
	}
}