<?php

/**
 * An extension of ActiveRecord class for Pesistent Active Records (PActiveRecord)
 *
 * Deleting a PActiveRecord causes a 'soft' deleted, rather than 'hard' delete
 * to ensure persistent data where necessary.
 *
 * A default scope is automatically applied to every select statement to exclude
 * any record where delete is not null.
 */ 
class PActiveRecord extends ActiveRecord
{
	public function init()
	{
		if (!$this->columnExists('deleted'))
			$this->addColumn('deleted', 'datetime DEFAULT NULL');
	}

	public function delete()
	{
		if($this->beforeDelete())
		{
			if (!$this->columnExists('deleted'))
				$this->addColumn('deleted', 'datetime DEFAULT NULL');

			$this->deleted = date('Y-m-d H:i:s');

			if ($this->columnExists('status'))
				$this->status = 0;

			$result = $this->update();

			$this->afterDelete();
			return $result;
		}
	}

	public function defaultScope()
	{
		$scope = parent::defaultScope();

		$t = $this->getTableAlias(false, false);

		$condition = isset($scope['condition']) ? array($scope['condition']) : array();

		if ($this->columnExists('deleted'))
			$condition[] = $t.'.deleted IS NULL';

		if (count($condition) > 0)
			$scope['condition'] = implode(' AND ', $condition);

		return $scope;
	}
}