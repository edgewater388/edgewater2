<?php

abstract class Searchable
{
	public abstract function isSearchable();
}