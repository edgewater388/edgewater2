<?php

class StripeService extends CApplicationComponent
{
	public $mode = 'test';
	public $keys = array();
	public $dryRun = false;
	public $dryRunApproved = true;

	public function init()
	{
		
	}

	public function getPublicKey()
	{
		return $this->keys[$this->mode]['public'];
	}

	public function getSecretKey()
	{
		return $this->keys[$this->mode]['secret'];
	}

	public function charge($params)
	{
		if ($this->dryRun)
			return self::dryRunResponse($this->dryRunApproved);

		Yii::import('application.vendors.stripe.lib.Stripe', true);
		Stripe::setApiKey($this->secretKey);

		// Try to process payment. If failed, should throw an exception.
		// If no exception is thrown, check the paid status to ensure successful payment
		try
		{
			return Stripe_Charge::create($params);
		}
		catch (Exception $e)
		{
			self::log($e->getMessage(), $params);

			if (stristr($e->getMessage(), 'You cannot use a stripe token more than once'))
			{
				// Flash::setFlash('error', 'Your payment details have expired and must be re-entered.');
			}
			else
				Flash::setFlash('error', 'Your payment could be not processed. Please verify the payment and billing details are correct and re-submit.');
		}

		return false;
	}

	public function createCustomer($params)
	{
		Yii::import('application.vendors.stripe.lib.Stripe', true);
		Stripe::setApiKey($this->secretKey);

		// Try to process payment. If failed, should throw an exception.
		// If no exception is thrown, check the paid status to ensure successful payment
		try
		{
			return Stripe_Customer::create($params);
		}
		catch (Exception $e)
		{
			self::log($e->getMessage(), $params);

			if (stristr($e->getMessage(), 'You cannot use a stripe token more than once'))
			{
				// Flash::setFlash('error', 'Your payment details have expired and must be re-entered.');
			}
			else
				Flash::setFlash('error', 'Your payment could be not processed. Please verify the payment and billing details are correct and re-submit.');
		}

		return false;
	}

	public function updateCustomer($params)
	{
		Yii::import('application.vendors.stripe.lib.Stripe', true);
		Stripe::setApiKey($this->secretKey);

		// Try to process payment. If failed, should throw an exception.
		// If no exception is thrown, check the paid status to ensure successful payment
		try
		{
			$cu = Stripe_Customer::retrieve($params['customer_id']);
			$cu->card = $params['card'];
			$cu->email = $params['email'];
			$cu->description = $params['description'];
			return $cu->save();
		}
		catch (Exception $e)
		{
			self::log($e->getMessage(), $params);

			if (stristr($e->getMessage(), 'You cannot use a stripe token more than once'))
			{
				// Flash::setFlash('error', 'Your payment details have expired and must be re-entered.');
			}
			else
				Flash::setFlash('error', 'Your payment could be not processed. Please verify the payment and billing details are correct and re-submit.');
		}

		return false;
	}

	public static function log($message, $attributes) {
		$msg = 'Failed Transaction'."\n\n".
			   $message."\n\n";

		foreach ($attributes as $k=>$v)
			$msg .= $k.': '.$v."\n";
		
		Yii::log($msg, CLogger::LEVEL_INFO, 'stripe');
		return $msg;
	}

	public static function dryRunResponse($approved=true)
	{
		return array(
			'paid'=>$approved ? 1 : 0,
		);
	}
}