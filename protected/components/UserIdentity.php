<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	protected $_record; // store the user record to be able to access profile, etc
	protected $_id;
	public $email;
	protected $_name;
	
	public function __construct($email, $password)
	{
		$this->email = $email;
		$this->password = $password;
	}

	public function authenticate()
	{
		$record=Users::model()->findByAttributes(array('email'=>$this->email));
		$hasher = new PasswordHash(8, true);

		if($record===null) {
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		}
		else if($hasher->CheckPassword($this->password, $record->password) == false) {
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		}
		else if (!$record->status) {
			$this->errorMessage='Your account is currently inactive or has been blocked. To activate or remove the block, please <a href="/contact" style="font-weight:bold;">contact us</a>.';
		}
		else
		{
		  	$this->_record = $record;
			$this->_id=$record->id;
			$this->email=$record->email;
			$this->errorCode=self::ERROR_NONE;
	      
			// Create a random key to be checked when a user logs in with a cookie
			$record->sessionkey = md5(time() . rand(1000,9999) . Yii::app()->getId());
			$record->lastvisit = time();
			$record->save();
			$this->setState('sessionkey', $record->sessionkey);
			$this->setState('email', $record->email);
			$this->setState('profile', $record->profile);
		}
		return !$this->errorCode;
	}
	
	public function authenticateByEmail($record)
	{
		$this->_record = $record;
		$this->_id = $record->id;
		$this->email = $record->email;
		$this->errorCode = self::ERROR_NONE;

		// Create a random key to be checked when a user logs in with a cookie
		$record->sessionkey = md5(time() . rand(1000,9999) . Yii::app()->getId());
		$record->lastvisit = time();
		$record->save();
		$this->setState('sessionkey', $record->sessionkey);
		$this->setState('email', $record->email);
		$this->setState('profile', $record->profile);
			
		return !$this->errorCode;
	}
	
	public function getId()
	{
	  return $this->_id;
	}
	
	public function getEmail()
	{
		return $this->email;
	}

	public function getName()
	{
		if ($this->_name)
			return $this->_name;
		if (!$this->_record || !$this->_record->profile)
			return '';
		return $this->_record->profile->first_name.' '.$this->_record->profile->last_name;
	}
}		
