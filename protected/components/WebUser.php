<?php

class WebUser extends CWebUser
{
	/**
	 * @var Users
	 */
	protected static $identity;

	/**
	 * Additional security check to ensure users with saved sessions
	 * are not spoofing their identity. 
	 *
	 * The Check: Verify a random key matches their id and random key
	 * stored in the user table.
	 *
	 * Note: This key is updated each time a logs in from the login form
	 */
    
	protected function beforeLogin($id,$states,$fromCookie)
	{
		// If the session key stored in the users cookie matches the key stored 
		// in the db, proceed with logging in. Otherwise, required log in.
		$user = Users::model()->findByPk($id);
		if ($fromCookie)
		{
			$sessionkey = $user->sessionkey;
			if (!isset($states['sessionkey']) || $sessionkey != $states['sessionkey'])
				return false;
		} 
		return true;
	}	
	
	protected function afterLogin($fromCookie) {
		
		// Save email address if rememberMe is selected
		if ($fromCookie || (isset($_POST['LoginForm']['rememberMe']) && $_POST['LoginForm']['rememberMe'] == '1'))
		{
			if (isset(Yii::app()->user->email))
			{
				$cookie = new CHttpCookie('email', Yii::app()->user->email);
				$cookie->expire = time()+60*60*24*30;
				Yii::app()->request->cookies['email'] = $cookie;
			}
		}
			
		else
			unset(Yii::app()->request->cookies['email']);
		
		//for magento login
		$cookieLogin = new CHttpCookie('magentoLogin', Yii::app()->user->email);
		Yii::app()->request->cookies['magentoLogin'] = $cookieLogin;
	}

	protected function beforeLogout()
	{
		if (Yii::app()->user->identity && Yii::app()->user->identity->is_demo) {

			$user_regimens = UserRegimen::model()->findAll('user_id = :user_id',
				array(':user_id' => Yii::app()->user->id));

			if (!empty($user_regimens)) {
				UserRegimen::model()->deleteAll('user_id = :user_id',
					array(':user_id' => Yii::app()->user->id));
			}

			if (!empty(Yii::app()->user->membership) && !Yii::app()->user->membership->delete()) {
				return false;
			}

			if (!Yii::app()->user->identity->delete()) {
				return false;
			}
		}

		return true;
	}
/*
	public function getPhoto()
	{	
		// Store the url to the users photo 
		
		// If url is empty, load from model
		return Profiles::model()->getPhoto(Yii::app()->user->id);
	}
*/

	public function getMembership()
	{
		return Membership::retrieve($this->id);
	}

	public function getCustomer()
	{
		return Customer::retrieve($this->id);
	}

	public function getReferralCode()
	{
		$user_id = $this->id;
		return Yii::app()->db->createCommand('SELECT referral_code FROM users WHERE id = :id')->bindParam(':id',$user_id)->queryScalar();
	}

	public function getIdentity()
	{
		if (!static::$identity) {
			static::$identity = Users::model()->findByPk($this->id);
		}

		return static::$identity;
	}
}