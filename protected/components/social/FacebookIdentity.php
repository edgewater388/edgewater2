<?php

class FacebookIdentity extends SocialIdentity
{
	public function __construct(SocialProfile $profile)
	{
		parent::__construct($profile);

		$this->provider = 'facebook';
		$this->provider_field = 'facebook_id';
		$this->provider_id = $this->profile->id;
	}
}
