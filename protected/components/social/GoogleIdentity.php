<?php

class GoogleIdentity extends SocialIdentity
{
	public function __construct(SocialProfile $profile)
	{
		parent::__construct($profile);

		$this->provider = 'google_plus';
		$this->provider_field = 'google_id';
		$this->provider_id = $this->profile->id;
	}
}