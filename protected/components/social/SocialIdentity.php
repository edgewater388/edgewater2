<?php

class SocialIdentity extends UserIdentity
{
	/**
	 * @var stdClass
	 */
	protected $profile;

	/**
	 * @var string
	 */
	protected $provider_field;

	/**
	 * @var string
	 */
	protected $provider_id;

	/**
	 * @var string
	 */
	protected $provider;

	public function __construct(SocialProfile $profile)
	{
		if (empty($profile) || empty($profile->id)) {
			throw new Exception('Invalid or broken profile data');
		}

		$this->profile = $profile;
	}

	public function authenticate()
	{
		if ($this->provider_id) {
			$record = Users::model()->findByAttributes([$this->provider_field => $this->provider_id]);

			if (is_null($record))
				$record = $this->register();

			if ($record) {
				$this->_record = $record;
				$this->_id = $record->id;
				$this->email = $record->email;
				$this->errorCode = self::ERROR_NONE;

				// Create a random key to be checked when a user logs in with a cookie
				//$record->sessionkey = md5(time() . rand(1000,9999) . Yii::app()->getId());
				$record->save();
				$this->setState('sessionkey', $record->sessionkey);
				$this->setState('email', $record->email);
				$this->setState('profile', $record->profile);
			}

			return !$this->errorCode;
		}

		return false;
	}

	protected function register()
	{
		$model = new Users;
		$model->scenario = $this->provider;
		$model->email = !empty($this->profile->email) ? $this->profile->email : '';
		$model->password = '';
		$model->created = time();
		$model->status = 1;
		$model->reg_vendor = $this->provider;
		$model->{$this->provider_field} = $this->profile->id;

		$profile = new Profile;
		$profile->first_name = isset($this->profile->first_name) ? $this->profile->first_name : '';
		$profile->last_name = isset($this->profile->last_name) ? $this->profile->last_name : '';

		if (!empty($this->profile->birthday)) {
			$birthday = explode('/', $this->profile->birthday);

			if (count($birthday) == 3) {
				$birthday = [
					$birthday[2],
					$birthday[0],
					$birthday[1],
				];
				$profile->birthday = implode('-', $birthday);
			}
		}

		$profile->status = 1;
		$profile->gender = isset($this->profile->gender) ? $this->profile->gender[0] : null;
		$profile->{$this->provider} = isset($this->profile->link) ? $this->profile->link : null;

		if (!$model->validate()) {
			$errors = $model->getErrors();
			if ($errors) {
				foreach ($errors as $key => $message) {
					Flash::setFlash('error', $message[0]);
				}
			}

			return false;
		}

		// use false parameter to disable validation
		$model->save(false);
		$profile->user_id = $model->id;
		$profile->save();

		if ($profile) {
			/**
			 * Functionality to update profile with facebook profile picture
			 */
			$file = Yii::app()->getModule('files')->file->retrieve($this->profile->image_url, false);
			$file = Yii::app()->getModule('files')->file->upload($file->filepath);
			$fileModel = Files::saveFile($file);

			if ($fileModel) {
				$profile->photo_fid = $fileModel->id;
				$profile->update();
			}
		}

		Flash::setFlash('success', '<b>' . $profile->first_name . '</b>, your account has been created.');

		return $model;
	}
}