<?php

class SocialProfile
{
	public $id;
	public $email;
	public $first_name;
	public $last_name;
	public $gender;
	public $link;
	public $image_url;
}