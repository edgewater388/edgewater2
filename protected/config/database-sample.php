<?php
return array(
	'connectionString' => 'mysql:host=127.0.0.1;dbname=DATABASE',
	'emulatePrepare' => true,
	'username' => 'USERNAME',
	'password' => 'PASSWORD',
	'charset' => 'utf8',
);