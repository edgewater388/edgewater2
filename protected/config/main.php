<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
Yii::setPathOfAlias('frontend', dirname(dirname(__FILE__)));

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
$config = [
	'basePath'       => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name'           => 'Physique 57',
	'theme'          => 'basic',
	'environment'    => 'development', // Set default environment to dev. Override this in environment.php

	// Set sourceLanguage and language for message translations (used for default block/page content)
	'sourceLanguage' => 'en',
	'language'       => 'en',

	// preloading 'log' component
	'preload'        => ['log'],

	// autoloading model and component classes
	'import'         => [
		'application.models.*',
		'application.components.*',
		'ext.yii-mail.YiiMailMessage',
		'application.vendors.*',
		// 'application.vendors.facebook.Facebook',
		'application.vendors.emma.Emma',
		'application.helpers.*',
		'application.modules.user.models.*',
		'application.modules.files.models.*',
		'application.modules.files.components.file.File',
		'application.modules.files.components.thumb.Thumb',
		'application.modules.files.components.image.CImageComponent',
		'application.modules.files.behaviors.*',
		'application.extensions.Curl',
		// 'application.extensions.brightcove.Brightcove',
		'application.extensions.jwplatform.JWPlatform',
		'application.components.social.*',
	],

	'modules'    => [
		// uncomment the following to enable the Gii tool

		'gii'   => [
			'class'     => 'system.gii.GiiModule',
			'password'  => 'gl0bal',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters' => ['127.0.0.1', '::1'],
		],

		'user',
		'api' => [
			'access_code' => '34023af3f5ed4df28763dbbcff8da219',
		],
		'files' => [
			'components' => [
				'file'  => [
					'class' => 'File',
					// 'service'=>'s3',
				],
				'thumb' => [
					'class'  => 'Thumb',
					'styles' => [
						'product' => [
							'scaleCrop' => [
								'width'  => '500',
								'height' => '500',
							],
							'quality'   => '75',
						],
						'regimen' => [
							'scaleCrop' => [
								'width'  => '792',
								'height' => '392',
							],
							'quality'   => '90',
						],
						'plan' => [
							'scaleCrop' => [
								'width'  => '1051',
								'height' => '700',
							],
							'quality'   => '90',
						],
					],
				],
				'image' => [
					'class'  => 'CImageComponent',
					// GD or ImageMagick
					'driver' => 'GD',
					// ImageMagick setup path
					'params' => ['directory' => '/opt/local/bin'],
				],
			],
		],
	],

	// application components
	'components' => [
		'user' => [
			// enable cookie-based authentication
			'allowAutoLogin'  => true,
			'loginUrl'        => ['user/users/login'],
			'class'           => 'WebUser',
			'autoRenewCookie' => true,
			'authTimeout'     => 3600 * 24,
			'autoUpdateFlash' => false,
		],

		'authManager' => [
			'class'        => 'CDbAuthManager',
			'connectionID' => 'db',
		],

		// uncomment the following to enable URLs in path-format
		'urlManager'  => [
			'urlFormat'      => 'path',
			'showScriptName' => false,
			// 'caseSensitive'=>false,
			'rules'          => require(dirname(__FILE__) . '/routes.php'),
		],

		'frontUrlManager' => [
			'urlFormat'      => 'path',
			'showScriptName' => false,
			'class'          => 'CUrlManager',
			'rules'          => require(dirname(__FILE__) . '/routes.php'),
		],

		'assetManager' => [
			'forceCopy' => true,
		],

		'errorHandler' => [
			// use 'site/error' action to display errors
			'errorAction' => 'site/error',
		],
		'log'          => [
			'class'  => 'CLogRouter',
			'routes' => [
				[
					'class'  => 'CFileLogRoute',
					'levels' => 'error, warning',
				],
				[
					'class'              => 'CDbLogRoute',
					'connectionID'       => 'db',
					'autoCreateLogTable' => true,
					// 'levels'=>'info',
					'categories'         => 'ext.yii-mail.YiiMail, stripe, cron',
				],
			],
		],
		'mail'         => [
			// 'class' => 'ext.yii-mail.YiiMail',
			'class'            => 'Mail',
			// 'class' => 'application.components.MailMessage',
			'transportType'    => 'smtp',
			'viewPath'         => 'application.views.mail',
			'logging'          => true,
			// 'dryRun' => true,
			'transportOptions' => [
				'host'     => 'smtp.mailgun.org',
				// 'username' => 'postmaster@rsd8fa0c4c23dc46a6896ead691cc73783.mailgun.org',
				'username' => 'ondemand@mg.physique57.com',
				'password' => 'c92720883c985cc32ccda0613632098e',
				'port'     => '587',
				// 'encryption' => 'tls',
			],
		],
		'mailMessage'  => [
			'class' => 'application.components.MailMessage',
		],
		'authorizenet' => [
			'class'                => 'application.components.AuthorizeNet',
			// testing
			'test_api_login_id'    => '6k65ERrz6',
			'test_transaction_key' => '2S53b5g7p69DjDDd', // test magento
//			'test_api_login_id'    => '8fe2SDR22V',
//			// 'transaction_key'=>'5PyA2347fA9MxqWL', // old one
//			'test_transaction_key' => '95Pwr287gFg5aCC7', // new one
			'secret_key'           => 'Simon',

			// production
			'api_login_id'         => '4gPF55eh',
			'transaction_key'      => '6y8SPX6hzntA6538',
		],
		// 'amazon'=>array(
		// 	'class'=>'application.extensions.aws.AmazonAWS',
		// 	'key'=>'',
		// 	'secret'=>'',
		// 	'bucket'=>'',
		// 	'cdn'=>'',
		// 	'cloudfront'=>array(
		// 		'key_pair_id'=>'',
		// 	),
		// ),
		// 'stripe'=>array(
		// 	'class'=>'application.components.StripeService',
		// 	'mode'=>'test',
		// 	'keys'=>array(
		// 		'test'=>array(
		// 			'secret'=>'',
		// 			'public'=>'',
		// 		),
		// 		'live'=>array(
		// 			'secret'=>'',
		// 			'public'=>'',
		// 		),
		// 	),
		// ),
		// 'brightcove'=>array(
		// 	'class'=>'application.extensions.brightcove.Brightcove',
		// 	'token'=>'VfjUshKX1UvV94Mu1MlDFx8OJgbbbkhZQz0kDqOHYas9EUx4BJLrxA..',
		// ),
		'jwplatform'   => [
			'class'  => 'application.extensions.jwplatform.JWPlatform',
			'key'    => 'biGwceRz',
			'secret' => '1hMmBjenrCkFPO2NfIxGLY4L',
		],
		'curl'         => [
			'class' => 'application.extensions.Curl',
		],
		'cache'        => [
			'class'                => 'system.caching.CDbCache',
			'connectionID'         => 'db',
			'autoCreateCacheTable' => true,
			'cacheTableName'       => 'cache',
		],

		'widgetFactory' => [
			'widgets' => [
				'CLinkPager'  => [
					'maxButtonCount'       => 8,
					'cssFile'              => false,
					'header'               => false,
					'nextPageCssClass'     => 'hidden',
					'previousPageCssClass' => 'hidden',
				],
				'CDetailView' => [
					'cssFile'     => false,
					'htmlOptions' => ['class' => 'table table-condensed table-striped'],
				],
			],
		],
	],

	// application-level parameters that can be accessed
	'params'     => require(dirname(__FILE__) . '/params.php'),

];

// Load database variables, if database.php exists
if (file_exists(dirname(__FILE__) . '/database.php')) {
	$config['components']['db'] = require(dirname(__FILE__) . '/database.php');
}

// Load specific environment variables, if environment.php
if (file_exists(dirname(__FILE__) . '/environment.php')) {
	$env_config = require(dirname(__FILE__) . '/environment.php');
	$config = CMap::mergeArray($config, $env_config);
}

return $config;