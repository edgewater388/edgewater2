<?php
// using Yii::app()->params['paramName']
return [
	// this is used in contact page
	'adminEmail'      => 'info@physique57.com',
	'contactEmail'    => 'ondemand@physique57.com',
	'contactEmailAlt' => 'ondemand@physique57.com',
	'fromEmail'       => 'noreply@physique57.com',
	'socialLinks'     => [
		'twitter'    => 'http://twitter.com/',
		// 'facebook'=>'http://facebook.com/',
		'facebook'   => 'http://facebook.com/',
		'googleplus' => 'http://plus.google.com',
		'rss'        => 'http://feeds.feedburner.com/',
		// 'youtube'=>'http://youtube.com/user/',
		'youtube'    => 'http://youtube.com/channel/xxxxxxxxx/feed',
		'linkedin'   => 'http://linkedin.com/company/',
		'pinterest'  => 'http://pinterest.com/',
	],
	// 'mailchimp'=>array(
	// 	'api_key'=>'',
	// 	'list_id'=>'',
	// ),
	'emma'            => [
		'account_id'  => '1358440',
		'public_key'  => '2ce2709f187e49f87fd3',
		'private_key' => 'b78f03ac96975e34b6b9',
		'groups'      => [
			'3125864' => 'Physique 57 Updates',
			'3126888' => 'Physique 57 Offers',
			'3127912' => 'Physique 57 Surveys',
		],
	],
	'share_image_url' => '/themes/basic/images/logo-graph.png',
	'facebook'        => [
		// production
		'client_id'     => '312065842567739',
		'client_secret' => 'cde11e637956b33df6ee1eefe046b1d7',
		// development
//		'client_id'     => '244337366050705',
//		'client_secret' => 'aeb13df50a406f57b4aa950b1c395133',
	],
	'google'          => [
		// production
		'client_id'     => '745305804950-mbgoo1ape1ufejrc8tn57r0q7meqlbmq.apps.googleusercontent.com',
		'client_secret' => 'AhoibhUA2cd_L-C6_JxvHF5u',
		// development
//		'client_id'     => '226836501537-uhi6c71kgp49ujktiaoci4c4534k8c6d.apps.googleusercontent.com',
//		'client_secret' => 'wDKIG-Ha8NaZCVlgtYLYTIdB',
	],
	'twitter'         => '',
	'tax'             => '0',
	'defaultThumbUrl' => '/themes/basic/images/home-logged-in/video-thumb1.jpg',
	'magentoLoginUrl' => 'http://dev.ondemand.physique57.com/shop/yiilogin',
	'magentoAuthCardUrl' => 'http://dev.ondemand.physique57.com/shop/authcard/card/update',
	'securityCode' => '34023af3f5ed4df28763dbbcff8da219'
];