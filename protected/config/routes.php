<?php

// Prevent these urls from being loaded in backend
// Currently CMap::arrayMerge will merge and make
// these routes priority.. not what is wanted

// if($_SERVER['SCRIPT_NAME'] == '/backend.php')
	// return array();

return array(


	''=>'site/index',
	'home'=>'site/index',

	// user module routes
	'login'=>'user/users/login',
	// 'register'=>'user/users/register',
	'logout'=>'user/users/logout',
	'forgot-password'=>'user/users/forgot',
	'account/confirm'=>'user/users/confirm',
	'account/reset'=>'user/users/reset',
	'account/<id:\d+>/editprofile(/<program_id:\d+>)?'=>'user/profile/update',
	'account/<id:\d+>/uploadphoto'=>'user/profile/upload',
	'account/<id:\d+>/billing'=>'stripeCustomer/index',
	'account/<id:\d+>/<type:\w+>'=>'user/users/settings',
	'account/<id:\d+>'=>'user/users/settings',
	'invite/<referral_code:\w+>'=>'user/users/invite',
	'users'=>'user/profile',
	'user/<id:\d+>'=>'user/profile/view',

	// About pages
	//'about'=>'content/about',

	// FAQ
	//'faq'=>'faq/index',

	// special offer / promo
	'promo/<slug>'=>'membership/specialOffer',
	'promochecker/<code:\w+>'=>'promoCode/index',

	'refer-a-friend'=>'user/users/referFriend',
	'account'=>'user/users/account',
	'change-email'=>'user/users/email',
	'change-password'=>'user/users/password',
	'email-preferences'=>'newsletter/manage',
	'manage-profile'=>'user/profile/manage',
	'viewing-activity'=>'user/users/activity',
	'contact-us'=>'site/contact',
	'billing-details'=>'membership/view',
	'cancel-account'=>'membership/cancel',
	'stay-on-account'=>'membership/stayOn',
	'update-payment-info'=>'customer/update',
	'change-plan'=>'membership/change',
	'my-workouts'=>'site/myworkouts',
	'mashup-maker'=>'site/mashupMaker',
	'sign-up'=>'customer/create',
	'sign-up-trial'=>'customer/createTrial',
	'reset-password'=>'user/users/reset',
	'start-demo'=>'customer/startDemo',

	'register-steps' => 'customer/saveRegData',

	'workout-program/<id:\w+>'=>'regimen/view',

	'program/<id:\w+>'=>'regimenProgram/view',
	'program/<id:\d+>/subscribe' => 'regimenProgram/subscribeTheProgram',
	'program/<id:\d+>/unsubscribe' => 'regimenProgram/unsubscribeTheProgram',
	'program/<id:\d+>/user-video-progress' => 'regimenProgram/userVideoProgress',
	
	//workout program page
	//'program/<id:\d+>/show' => 'regimenProgram/show',

	// Site pages
	'contact'=>'site/contact',
	'<view:(account|billing-details|change-email|change-password|email-preferences|change-plan|renew-plan|manage-profile|viewing-activity|cancel-account|update-payment-info|faq|forgot-password|contact-us|misc|sign-up|payment|home-signed-in|my-workouts|mashup-maker|reset-password|success-stories)>'=>'site/page',

	// Video filtering
	'video/by-duration/<duration:\d+>/all' => 'video/allByDuration',
	'video/live-classes/all' => 'video/allLiveClasses',
	'video/by-duration/<duration:\d+>/filter' => 'video/filter',
	'video/live-classes/filter' => 'video/filter',
	
	
	//video page
	'video/<id:\d+>/show'=>'videoPage/show',

	// File paths
	'files/thumb/<style:.+>/<filename:.+\.(jpeg|jpg|png|gif)>'=>'files/default/thumb',

	// Social routes
	'social/auth-facebook' => 'social/authFacebook',
	'social/auth-google' => 'social/authGoogle',

	// Countdown control
	'countdown/start' => 'site/startCountdown',
	'countdown/reset' => 'site/resetCountdown',
		
	//api module routes
	// REST patterns
	['api/default/list', 'pattern' => 'api/<model:\w+>/<code:\w+>/<offset:\d+>/<limit:\d+>', 'verb'=>'GET'],
	['api/default/view', 'pattern' => 'api/<model:\w+>/<id:\d+>/<code:\w+>', 'verb'=>'GET'],
	['api/default/update', 'pattern' => 'api/<model:\w+>/<id:\d+>/<code:\w+>', 'verb'=>'PUT'],
	['api/default/delete', 'pattern' => 'api/<model:\w+>/<id:\d+>/<code:\w+>', 'verb'=>'DELETE'],
	['api/default/create', 'pattern' => 'api/<model:\w+>/<code:\w+>', 'verb'=>'POST'],
	['api/default/updatePayment', 'pattern' => 'api/<model:\w+>/<email:[a-zA-Z0-9_@.\-]+>/<code:\w+>', 'verb'=>'POST'],
	
	// Module routes
	'<module:\w+>/<controller:\w+>/<id:\d+>'=>'<module>/<controller>/view',
	'<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<module>/<controller>/<action>',
	'<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',

	// Generic controller routes
	'<controller:\w+>/<id:\d+>'=>'<controller>/view',
	'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
	'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
);