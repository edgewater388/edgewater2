<?php

// die(var_dump(md5(uniqid(rand(), true))));

class CronController extends Controller
{
	private $_code;
	
	public function init()
	{
		$this->_code = Yii::app()->params['securityCode'];
		parent::init();
	}

	public function beforeAction($action)
	{
		$valid = parent::beforeAction($action);
		Yii::log($action->id. ' cron started ('.date('Y-m-d H:i:s').')', 'info', 'cron');
		return $valid;
	}

	public function afterAction($action)
	{
		parent::afterAction($action);
		Yii::log($action->id. ' cron completed ('.date('Y-m-d H:i:s').')', 'info', 'cron');
	}

	// could be called any time
	public function actionIndex($code=null)
	{
		$this->verifyCode($code);
	}

	// every hour, on the hour.
	public function actionHourly($code=null)
	{
		$this->verifyCode($code);

		$result = Video::retrieveAll();
		echo $result['saved'].'/'.$result['count'].' videos have been retrieved and updated';
	}

	// every day 12am
	public function actionDaily($code=null)
	{
		set_time_limit(24*60*60);
		error_reporting(E_ALL);

		$this->verifyCode($code);

		// process recurring payments (billing date today, auto renew is true)
		Membership::processRecurringPayments();

		// process active promotions where plan is not auto renewing (eg daily plan)
		Membership::processRecurringPromos();

		// Membership::sendTrialEndingReminders();
	}

	public function actionProcessPayments($code=null)
	{
		set_time_limit(24*60*60);
		
		$this->verifyCode($code);
	}

	public function actionTest()
	{
		$message = new MailMessage;
		$message->subject = 'Cron Test Success';
		$message->to = null;
		$message->addTo('davidtiede@gmail.com', 'David Tiede');
		$message->setBody('testing cron');
		Yii::app()->mail->send($message);
	}

	protected function verifyCode($code)
	{
		if ($code === $this->_code)
			return true;
		throw new CHttpException(404,'The requested page does not exist.');
	}
	
	// login user from magento
	public function actionLogin($email=null, $code=null)
	{
		$this->verifyCode($code);
		$user = Users::model()->findByAttributes(['email' => $email]);
		if (empty($user)) {
			throw new CHttpException(404, 'User not found.');
		}
		$identity = new UserIdentity($user->email, $user->password);
		$identity->authenticateByEmail($user);
		
		return Yii::app()->user->login($identity);
	}
}
