<?php

class CustomerController extends Controller
{
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	public function accessRules()
	{
		return [
			['allow',
				'actions' => ['update'],
				'users'   => ['@'],
			],
			['allow',
				'actions' => ['create', 'createTrial', 'saveRegData', 'startDemo'],
				'users'   => ['*'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function actionSaveRegData($trial = null)
	{
		$success = false;
		$errors = 'Method not allowed or the data is broken';
		$fields = '';
		$step_num = 0;

		$session = Yii::app()->session;

		$steps_data = isset($session['steps_data']) ? $session['steps_data'] : null;

		if (!empty($_POST['store_data'])) {
			$store_data = $_POST['store_data'];
			$success = true;
			$errors = '';

			$store_step = $_POST['store_step'];
			$step_num = $store_step;

			foreach ($store_data as $model_name => $attributes) {

				if ($model_name == 'Plan') {
					if (empty(Plan::getPlan($attributes['id']))) {
						$success = false;
						$errors .= 'Oops! Please select a plan and try again.';
						$fields .= ",plan_id";
					}
					continue;
				}

				$model = new $model_name;
				$model->attributes = $attributes;
				$model->scenario = "validate_step_{$store_step}";

				$social = Users::checkIfSocial();

				if ($social) {
					$model->scenario = "social_step_{$store_step}";
				}

				if ($store_step == 2 && $trial) {
					$model->scenario = "validate_step_2_trial";

					if ($social) {
						$model->scenario = "social_trial_step_2";
					}
				}

				if ($success) {
					$success = $model->validate();
				} else {
					$model->validate();
				}

				if (!empty($model->errors)) {
					foreach ($model->errors as $field => $error_msgs) {
						foreach ($error_msgs as $error_msg) {
							$errors .= ' ' . $error_msg;
							if (!(substr($error_msg, -1) == '.')) {
								$errors .= '.';
							}
						}
						$fields .= ",{$field}";
					}
				}
			}

			if ($store_step == 2) {
				unset($store_data['Customer']['new_card_number']);
				unset($store_data['Customer']['card_security']);
			}

			if ($store_step == 1) {
				unset($store_data['Users']['password']);
			}

			$steps_data[$store_step] = $store_data;

			$session['steps_data'] = $steps_data;
		}

		if ($success) {
			$step_num += 1;
		} elseif (!$success && isset($store_step)) {
			$step_num = $store_step;
		}

		if ($trial && isset($store_step) && $store_step == 2) {
			$step_num = 2;
		}

		echo json_encode([
			'success' => $success,
			'errors'  => ltrim($errors),
			'fields'  => ltrim($fields, ','),
			'step'    => $step_num,
		]);
		exit;
	}

	public function actionCreate()
	{
		if (Yii::app()->user->membership && !Yii::app()->user->identity->is_demo) {
			$dest = (Yii::app()->user->membership->isActive()) ? '/account' : ['membership/change'];
			$this->redirect($dest);
		}

		if (!Yii::app()->user->isGuest && !Yii::app()->user->identity->is_demo) {
			$model_user = Users::model()->findByPk(Yii::app()->user->id);
		} elseif (!$model_user = $this->registerUser()) {
			$model_user = new Users;
		}

		if (!$model_customer = $this->loadModel()) {
			$model_customer = new Customer;
			$model_customer->scenario = 'create';
		}

		$plans = Plan::getDefaultPlans();
		$selected_plan_id = !empty($_POST['Plan']) ? (int)$_POST['Plan']['id'] : 8;

		$promoCode = null;

		if (isset($_POST['Customer'])) {
			$profile = Profile::model()->findByAttributes(['user_id' => $model_user->id]);

			$model_customer->attributes = $_POST['Customer'];
			$model_customer->user_id = Yii::app()->user->id;
			$model_customer->status = 1;

			if (!empty($profile)) {
                $social_register = $profile->facebook || $profile->google_plus;
                if ($social_register) {
                    $model_customer->first_name = $profile->first_name;
                    $model_customer->last_name = $profile->last_name;
                }
            }

			$promo_code = null;

			if (!empty($_POST['Customer']['promo_code'])) {
				$promo_code = $_POST['Customer']['promo_code'];
			} elseif (!empty($steps_data = Yii::app()->session->get('steps_data'))) {
				$promo_code = isset($steps_data['1'], $steps_data['1']['Customer']) ? $steps_data['1']['Customer']['promo_code'] : null;
			}

			$model_customer->promo_code = $promo_code;

			// make sure a valid plan is selected
			if (!$plan = Plan::getPlan($selected_plan_id)) {
				$model_customer->addError('plan_id', 'Oops! Please select a plan and try again.');
			}

			$verify_promo_code = false;

			if ($plan && $model_customer->promo_code) {
				if (!$promoCode = PromoCode::verify($model_customer->promo_code, $selected_plan_id)) {
					// see if the promo code is valid for a daily plan, if so change the selected plan
					// on behalf of the user (since the daily plan is not available to select)
					$daily_plan = Plan::getDailyPlan();
					if ($promoCode = PromoCode::verify($model_customer->promo_code, $daily_plan['id'])) {
						$selected_plan_id = $daily_plan['id'];
						$plans[] = $daily_plan;
						$plan = $daily_plan;
					}
				}

				// check if the promo code is valid
				if ($promoCode) {
					$verify_promo_code = true;
					Flash::setFlash('success', 'Promo code ' . $model_customer->promo_code . ' has been successfully applied!');
				} else {
					$model_customer->addError('promo_code', 'Invalid promo code');
				}
			}

			if ($plan && $model_customer->validate(null, false) && $model_customer->save()) {

				if (!$social_register) {
					// usual register
					$profile->first_name = $model_customer->first_name;
					$profile->last_name = $model_customer->last_name;

					if (!$profile->save()) {
						$model_customer->addErrors($profile->errors);
					}
				}

				$membership = Membership::startMembership($model_user->id, $plan['id']);

				if ($model_customer->promo_code) {
					$membership->startPromotion($model_customer->promo_code);
				}

				if (!$membership->trial_end_date) {
					$transaction = Membership::processPayment($membership);
					if (!$transaction || !$transaction->approved) {
						$membership->valid_to = null;
						$membership->addError('payment', 'Unable to process payment');
						$membership->save();
					}
				}

				// Flash::setFlash('success', 'Your payment information has been saved.');
				if (!$membership->hasErrors()) {
					// Send a welcome email
					$message = new MailMessage;
					$message->view = 'welcome';
					$message->addTo($model_user->email, $profile->name);
					$message->subject = 'WELCOME TO PHYSIQUE 57!';
					// $message->tagline = 'WELCOME TO STRONGER. WELCOME TO LEANER. WELCOME TO PHYSIQUE 57!';
					$message->setBody(['user' => $model_user]);
					Yii::app()->mail->send($message);

					// Send Membership Started Email
					$message = new MailMessage;
					$message->view = 'membership_started';
					$message->subject = $membership->trial_end_date ? 'Your trial has started!' : 'Your membership has started!';
					$message->addTo($model_user->email, $profile->name);
					$message->setBody([
						'plan'       => $plan,
						'membership' => $membership,
						'customer'   => $model_customer,
						'user'       => $model_user,
					]);
					Yii::app()->mail->send($message);
				}

				Yii::app()->session->remove('steps_data');

				$this->redirect('/?tutorial=1');
			}
		}

		if (!empty($session = Yii::app()->session['steps_data'])) {
			foreach ($session as $model_attributes) {
				foreach ($model_attributes as $model => $attributes) {
					if ($model == 'Users') {
						$model_user->attributes = $attributes;
					} elseif ($model == 'Customer') {
						$model_customer->attributes = $attributes;
					}
				}
			}
		}

		$auth_links = Users::getSocialAuthLinks();
		Yii::app()->session['social_redirect_url'] = $this->createAbsoluteUrl('customer/create');

		$countdown_timestamp = Settings::getSettingValue(Settings::SETTING_COUNTDOWN_TO);

		return $this->render('//site/pages/sign-up', [
			'model_user'          => $model_user,
			'model_customer'      => $model_customer,
			'plans'               => $plans,
			'selected_plan_id'    => $selected_plan_id,
			'promoCode'           => $promoCode,
			'promo_verified'      => isset($verify_promo_code) ? $verify_promo_code : false,
			'is_guest'            => (Yii::app()->user->isGuest || Yii::app()->user->identity->is_demo),
			'facebook_auth_link'  => $auth_links['facebook'],
			'google_auth_link'    => $auth_links['google'],
			'countdown_timestamp' => $countdown_timestamp,
		]);
	}

	public function actionCreateTrial()
	{
		if (Yii::app()->user->membership && !Yii::app()->user->identity->is_demo) {
			$dest = (Yii::app()->user->membership->isActive()) ? '/account' : ['membership/change'];
			$this->redirect($dest);
		}

		if (!Yii::app()->user->isGuest && !Yii::app()->user->identity->is_demo) {
			$model_user = Users::model()->findByPk(Yii::app()->user->id);
		} elseif (!$model_user = $this->registerUser()) {
			$model_user = new Users;
		}

		if (!$model_customer = $this->loadModel()) {
			$model_customer = new Customer;
			$model_customer->scenario = 'create_trial';
		}

		$promoCode = null;

		$plan = Plan::getDefaultPlan();
		$trial_price = !empty($price = (double)Plan::getTrialPrice()) ? $price : $plan['amount'];

		if (isset($_POST['Customer'])) {
			$profile = Profile::model()->findByAttributes(['user_id' => $model_user->id]);

			$model_customer->attributes = $_POST['Customer'];
			$model_customer->user_id = Yii::app()->user->id;
			$model_customer->status = 1;
			$model_customer->scenario = 'create_trial';

			$social_register = $profile->facebook || $profile->google_plus;

			if ($social_register) {
				$model_customer->first_name = $profile->first_name;
				$model_customer->last_name = $profile->last_name;
			}

			$promo_code = null;

			if (!empty($_POST['Customer']['promo_code'])) {
				$promo_code = $_POST['Customer']['promo_code'];
			} elseif (!empty($steps_data = Yii::app()->session->get('steps_data'))) {
				$promo_code = isset($steps_data['1'], $steps_data['1']['Customer']) ? $steps_data['1']['Customer']['promo_code'] : null;
			}

			$model_customer->promo_code = $promo_code;

			$verify_promo_code = false;

			if ($model_customer->promo_code) {
				if (!$promoCode = PromoCode::verify($model_customer->promo_code)) {
					$model_customer->addError('promo_code', 'Invalid promo code');
				} else {
					$verify_promo_code = true;
					Flash::setFlash('success', 'Promo code ' . $model_customer->promo_code . ' has been successfully applied!');
				}
			}

			if ($model_customer->validate(null, false) && $model_customer->save()) {

				if (!$social_register) {
					// usual register
					$profile->first_name = $model_customer->first_name;
					$profile->last_name = $model_customer->last_name;

					if (!$profile->save()) {
						$model_customer->addErrors($profile->errors);
					}
				}

				$membership = Membership::startMembership($model_user->id, $plan['id'], true);

				if ($model_customer->promo_code) {
					$membership->startPromotion($model_customer->promo_code);
				}

				if (!$membership->trial_end_date) {
					$transaction = Membership::processPayment($membership);
					if (!$transaction || !$transaction->approved) {
						$membership->valid_to = null;
						$membership->addError('payment', 'Unable to process payment');
						$membership->save();
					}
				}

				// Flash::setFlash('success', 'Your payment information has been saved.');
				if (!$membership->hasErrors()) {
					// Send a welcome email
					$message = new MailMessage;
					$message->view = 'welcome';
					$message->addTo($model_user->email, $profile->name);
					$message->subject = 'WELCOME TO PHYSIQUE 57!';
					// $message->tagline = 'WELCOME TO STRONGER. WELCOME TO LEANER. WELCOME TO PHYSIQUE 57!';
					$message->setBody(['user' => $model_user]);
					Yii::app()->mail->send($message);

					// Send Membership Started Email
					$message = new MailMessage;
					$message->view = 'membership_started';
					$message->subject = $membership->trial_end_date ? 'Your trial has started!' : 'Your membership has started!';
					$message->addTo($model_user->email, $profile->name);
					$message->setBody([
						'plan'       => $plan,
						'membership' => $membership,
						'customer'   => $model_customer,
						'user'       => $model_user,
					]);
					Yii::app()->mail->send($message);
				}

				Yii::app()->session->remove('steps_data');

				$this->redirect('/?tutorial=1');
			}
		}

		if (!empty($session = Yii::app()->session['steps_data'])) {
			foreach ($session as $model_attributes) {
				foreach ($model_attributes as $model => $attributes) {
					if ($model == 'Users') {
						$model_user->attributes = $attributes;
					} elseif ($model == 'Customer') {
						$model_customer->attributes = $attributes;
					}
				}
			}
		}

		$auth_links = Users::getSocialAuthLinks();
		Yii::app()->session['social_redirect_url'] = $this->createAbsoluteUrl('customer/createTrial');

		$countdown_timestamp = Settings::getSettingValue(Settings::SETTING_COUNTDOWN_TO);

		return $this->render('//site/pages/sign-up-trial', [
			'model_user'          => $model_user,
			'model_customer'      => $model_customer,
			'promoCode'           => $promoCode,
			'promo_verified'      => isset($verify_promo_code) ? $verify_promo_code : false,
			'is_guest'            => (Yii::app()->user->isGuest || Yii::app()->user->identity->is_demo),
			'facebook_auth_link'  => $auth_links['facebook'],
			'google_auth_link'    => $auth_links['google'],
			'countdown_timestamp' => $countdown_timestamp,
			'trial_price'         => $trial_price,
		]);
	}

	/**
	 * @return bool|Users
	 */
	protected function registerUser($user_data = null, $test_user = false)
	{
		if (!Yii::app()->user->isGuest) {
			if (Yii::app()->user->identity->is_demo) {
				Yii::app()->user->logout();
			}

			return false;
		}

		$model_user = new Users();

		if (!$user_data) {
			if (empty($_POST['Users'])) {
				return false;
			}
			$user_data = $_POST['Users'];
		}

		$model_user->attributes = $user_data;
		$model_user->scenario = 'register';

		if (Users::model()->byAttribute('email', $model_user->email)->exists()) {
			$model_user->addErrors(['email' => 'User with this email already exists']);

			return $model_user;
		}

		$model_user->status = 1;
		$model_user->confirmation_key = $model_user->createConfirmationKey();

		if (!$model_user->save()) {
			return $model_user;
		}
		
		$profile = new Profile();
		$profile->user_id = $model_user->id;
		$profile->status = 1;

		if (!$profile->save()) {
			$model_user->addErrors($profile->errors);
		}

		if ($test_user) {
			$membership = new Membership();
			$membership->user_id = $model_user->id;
			$membership->is_trial_state = 1;
			$membership->trial_from = date('Y-m-d');
			$membership->trial_to = date('Y-m-d', strtotime('now + 1 day'));
			$membership->valid_from = $membership->trial_from;
			$membership->valid_to = $membership->trial_to;
			$membership->status = 1;
			if (!$membership->save()) {
				$model_user->addErrors($membership->errors);
			}
		}

		$loginForm = new UserLoginForm();
		$loginForm->attributes = $user_data;

		// After registering, the user should then be logged in
		if (!$loginForm->validate() || !$loginForm->login()) {
			$model_user->addErrors($loginForm->errors);
		}                                   
		
		return $model_user;
	}

	public function actionStartDemo()
	{
		if (!Yii::app()->user->isGuest || !Settings::getSettingValue(Settings::SETTING_DEMO_ACCESS)) {
			return $this->redirect('/');
		}

		$user_data = [
			'email'    => Customer::getRandomEmail(),
			'password' => Customer::getRandomPassword(),
			'is_demo'  => 1,
		];

		if (!$this->registerUser($user_data, true)) {
			Flash::setFlash('error', 'Cannot start demo view');
		}

		return $this->redirect('/');
	}

	public function actionUpdate()
	{
		if (!$model = $this->loadModel())
			$model = new Customer;

		if ($model->isNewRecord && !Yii::app()->user->membership)
			$this->redirect(['customer/create']);

		if (isset($_POST['Customer'])) {
			$model->attributes = $_POST['Customer'];
			$model->user_id = Yii::app()->user->id;
			$model->status = 1;

			if ($model->save()) {
				$success = true;

				if ($membership = Membership::model()->byAttribute('user_id', $model->user_id)->find()) {
					if ($membership->billing_failed_date || empty($membership->prev_billing_date) && $membership->trial_end_date < date('Y-m-d')) {
						if (!$membership->reactivate()) {
							$success = false;
							$model->addError('card', 'Sorry we were unable to process your payment. Please try again!');
							// Flash::setFlash('error', 'Sorry we were unable to process your payment. Please try again!');
						}
					}
				}

				if ($success) {
					Flash::setFlash('success', 'Your payment information has been updated.');
					$this->redirect('/account');
				}
			}
		}

		$this->render('//site/pages/update-payment-info', [
			'model' => $model,
		]);
	}

	public function loadModel()
	{
		return Customer::retrieve(Yii::app()->user->id);
	}
}