<?php

class MembershipController extends Controller
{
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	public function accessRules()
	{
		return [
			['allow',
				'actions' => ['specialOffer'],
				'users'   => ['*'],
			],
			['allow',
				'actions' => ['view', 'cancel', 'stayOn', 'change'],
				'users'   => ['@'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function actionView()
	{
		$this->render('//site/pages/billing-details', [
			'membership'   => $this->loadModel(),
			'transactions' => Transaction::getUserTransactions(Yii::app()->user->id),
		]);
	}

	public function actionCancel()
	{
		if (!$model = $this->loadModel()) {
			Flash::setFlash('error', 'No membership found');
			$this->redirect('/account');
		}

		// die(var_dump($_POST));
		if (isset($_POST['Membership']['cancellation_date'])) {
			$model->scenario = 'cancel';
			$model->cancel_membership = 1;
			if ($model->save()) {
				Flash::setFlash('success', 'Your membership will be cancelled as of ' . date('M j, Y', strtotime($model->valid_to)));
				$this->redirect('/account?cancelled=1');
			}
		}

		$this->render('//site/pages/cancel-account', [
			'membership' => $model,
		]);
	}

	public function actionStayOn()
	{
		if (!$model = $this->loadModel()) {
			Flash::setFlash('error', 'No membership found');
			$this->redirect('/account');
		}

		if (!$model->checkUserCancelDiscount()) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}

		$model->subtotal = Membership::getPriceWithDiscount();
		$model->discount_offered = 1;

		if ($model->save()) {
			Flash::setFlash('success', 'Your discount has been applied successfully');
		} else {
			Flash::setFlash('error', 'Your discount has not been applied');
		}

		$this->redirect('/account');
	}

	public function actionChange()
	{
		if (!$model = $this->loadModel())
			$model = new Membership;

		if (!Yii::app()->user->customer) {
			if ($model->isNewRecord)
				$this->redirect(['customer/create']);
			else
				$this->redirect(['customer/update']);
		}

		$promoCode = null;
		$model->promo_code = isset($_POST['Membership']['promo_code']) ? $_POST['Membership']['promo_code'] : null;
		$selected_plan_id = isset($_POST['Membership']['plan_id']) ? $_POST['Membership']['plan_id'] : null;

		if ($selected_plan_id) {
			if ($model->promo_code) {
				// check if the promo code is valid
				if ($promoCode = PromoCode::verify($model->promo_code, $selected_plan_id))
					Flash::setFlash('success', 'Promo code ' . $model->promo_code . ' has been successfully applied!');
				else {
					$model->addError('promo_code', 'Invalid promo code');
				}
			}

			if (!$model->hasErrors()) {
				if (Membership::changePlan(Yii::app()->user->id, $selected_plan_id, $model->promo_code)) {
					Flash::setFlash('success', 'Your plan has been updated.');
					$this->redirect('/account?plan_changed=1');
				} else {
					Flash::setFlash('error', 'Unable to change plan, please update your payment info and try again.');
					$model->addError('plan_id', 'Unable to change plan, please update your <a href="/update-payment-info">payment info</a> and try again.');
				}
			}
		}

		$plans = Plan::model()->findAll();

		$this->render('//site/pages/change-plan', [
			'model'     => $model,
			'plans'     => $plans,
			'promoCode' => $promoCode,
		]);
	}

	public function actionSpecialOffer($slug)
	{
		// if (Yii::app()->user->id && Membership::model()->byAttribute('user_id', Yii::app()->user->id)->find())
		// {
		// 	Flash::setFlash('info', 'The promo page is not available for existing members.');
		// 	$this->redirect('/account');
		// }

		$user = new Users('register');
		$profile = new Profile;
		$customer = new Customer('create');
		$plan = Plan::model()->findByAttributes(['slug' => $slug, 'is_special' => true]);

		// disable the page if the promo is not currently active
		if (empty($plan) || date('Y-m-d') < $plan->promo_start_date || date('Y-m-d') > $plan->promo_end_date)
			throw new CHttpException(404, 'Page not found.');

		// if (Yii::app()->request->hostInfo == 'http://physique57.dev')
		// {
		// 	$customer->first_name = 'David';
		// 	$customer->last_name = 'Robert';
		// 	$customer->street = '1234 Summer St';
		// 	$customer->zipcode = '90210';
		// 	$customer->new_card_number = '4007000000027';
		// 	$customer->card_security = '000';
		// 	$customer->card_month = '4';
		// 	$customer->card_year = '2017';
		// 	$customer->over_18 = 1;
		// }

		if (isset($_POST['Users'])) {
			$user->attributes = $_POST['Users'];
			$password = $user->password; // the raw password before it's hashed

			if (!Users::model()->byAttribute('email', $user->email)->exists()) {
				$user->validate();
				$user->status = 1;
				$user->confirmation_key = $user->createConfirmationKey();

				if ($user->save()) {
					$profile->user_id = $user->id;
					$profile->first_name = isset($_POST['Customer']['first_name']) ? $_POST['Customer']['first_name'] : null;
					$profile->last_name = isset($_POST['Customer']['last_name']) ? $_POST['Customer']['last_name'] : null;
					$profile->status = 1;
					$profile->save();
				}
			}

			if (!$user->hasErrors()) {
				$loginForm = new LoginForm;
				$loginForm->email = $user->email;
				$loginForm->password = $password;

				if (!$loginForm->validate() || !$loginForm->login())
					$user->addErrors($loginForm->getErrors());
			}
		}
		
		// make sure the user is logged in before saving the customer
		if (Yii::app()->user->id && isset($_POST['Customer'])) {
//		if (!empty(Yii::app()->user->membership) && !empty(Yii::app()->user->membership->prev_billing_date) && !empty(Yii::app()->user->customer) || $plan->id == Yii::app()->user->membership->plan_id) {
//				Flash::setFlash('error', 'This promotion is only valid for people who do not have an active subscription.');
//				$this->refresh();
//			}
			
			// update the current users customer info instead of creating a new customer
			if (!$customer = Customer::model()->byAttribute('user_id', Yii::app()->user->id)->find()) {
				$customer = new Customer('register');
				$customer->user_id = Yii::app()->user->id;
			}

			$customer->attributes = $_POST['Customer'];
			$customer->email = $user->email;
			$customer->status = 1;

			if ($customer->save()) {
				if ($membership = Membership::startSpecialOffer($plan)) {
					$this->redirect('/?tutorial=1');
				} else {
					// something went wrong with the payment
					// $customer->delete();
					$customer->addError('payment', 'Unable to process payment, please check your payment information and try again.');
				}
			}
		}

		// $this->render('//membership/specialOffer', array(
		$this->render('//site/pages/promo', [
			'user'     => $user,
			'customer' => $customer,
			'plan'     => $plan,
			'image'    => $plan->getThumbUrl()
		]);
	}

	public function loadModel()
	{
		return Membership::getUserMembership();
	}
}