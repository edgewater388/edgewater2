<?php

class NewsletterController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', 
				'actions'=>array('signup'),
				'users'=>array('*'),
			),
			array('allow',
				'actions'=>array('signup','manage'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionSignup()
	{
		$model = new NewsletterForm;
		$dest = '/';

		if (isset($_POST['NewsletterForm']))
		{
			$model->attributes = $_POST['NewsletterForm'];

			if ($model->returnUrl)
				$dest = $model->returnUrl;

			if ($model->validate())
			{
				if ($model->save())
					Flash::setFlash('success', 'You have been added to our newsletter mailing list.');
				else
					Flash::setFlash('error', 'We were unable to subscribe you to our mailing list. Please try again or contact us.');
			}
			else
			{
				foreach ($model->getErrors() as $errors)
				{
					foreach ($errors as $error)
						Flash::setFlash('error', $error);
				}
			}
		}

		$this->redirect($dest);

		// $this->render('signup', array(
		// 	'model'=>$model,
		// ));
	}

	public function actionManage()
	{
		// $response = $emma->myGroups($params);
		$model = new NewsletterSubscriptionForm;

		$account_id = Yii::app()->params['emma']['account_id'];
		$public_key = Yii::app()->params['emma']['public_key'];
		$private_key = Yii::app()->params['emma']['private_key'];

		$emma = new Emma($account_id, $public_key, $private_key);

		$params = array();
		$email = Yii::app()->user->email;
		$member = null;
		$groups = array();

		try {

			$response = $emma->membersListByEmail($email);
			$member = json_decode($response);

			$response = $emma->membersListSingleGroups($member->member_id);
			$member_groups = json_decode($response);

			foreach ($member_groups as $v)
				$groups[$v->member_group_id] = $v->group_name;

		} catch (Exception $e) {
			
		}

		if (isset($_POST['NewsletterSubscriptionForm']))
		{
			$subscribe = array();
			$unsubscribe = array();

			if ($_POST['unsubscribe'] == '1')
			{
				if ($member)
				{
					$params = array(
						'group_ids'=>array(),
					);

					foreach ($_POST['NewsletterSubscriptionForm']['groups'] as $group_id=>$checked)
						$params['group_ids'][] = $group_id;
					
					$response = $emma->membersRemoveSingleFromGroups($member->member_id, $params);
					$response = $emma->membersRemoveSingle($member->member_id);
					$response = json_decode($response);
					$groups = array();
				}
				else
				{
					// nothing to do if the member doesn't exist
				}

				Flash::setFlash('success', 'Your email preferences have been updated.');
				$this->redirect('/account');

			}
			else
			{
				foreach ($_POST['NewsletterSubscriptionForm']['groups'] as $group_id=>$checked)
				{
					if ($checked == '1' && !array_key_exists($group_id, $groups))
						$subscribe[] = $group_id;
					else if ($checked == '0' && array_key_exists($group_id, $groups))
						$unsubscribe[] = $group_id;
				}

				if (!$member && !empty($subscribe))
				{
					$emma = new Emma($account_id, $public_key, $private_key);
					// create the member first
					$params['email'] = $email;
					$params['fields'] = array('first_name'=>Yii::app()->user->profile->first_name, 'last_name'=>Yii::app()->user->profile->last_name);
					$response = $emma->membersAddSingle($params);
					$member = json_decode($response);
				}

				if ($member)
				{
					// subscribe
					if (!empty($subscribe))
					{
						$emma = new Emma($account_id, $public_key, $private_key);
						$params['group_ids'] = $subscribe;
						$response = $emma->membersGroupsAdd($member->member_id, $params);
						$response = json_decode($response);
					}

					// unsubscribe
					if (!empty($unsubscribe))
					{
						$emma = new Emma($account_id, $public_key, $private_key);
						$params['group_ids'] = $unsubscribe;
						$response = $emma->membersRemoveSingleFromGroups($member->member_id, $params);
						$response = json_decode($response);
					}

					$response = $emma->membersListSingleGroups($member->member_id);
					$member_groups = json_decode($response);
					$groups = array();

					foreach ($member_groups as $v)
						$groups[$v->member_group_id] = $v->group_name;
				}

				Flash::setFlash('success', 'Your email preferences have been updated.');
				$this->redirect('/account');
			}
		}

		$this->render('/site/pages/email-preferences', array(
			'model'=>$model,
			'groups'=>$groups,
		));
	}
}