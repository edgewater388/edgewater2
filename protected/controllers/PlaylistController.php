<?php

class PlaylistController extends ApiController
{
	public function actionIndex($public=null, $featured=null)
	{
		$model = new Playlist('search');
		$model->attributes = $_GET;
		$model->isPublic();

		if ($featured == '1')
			$model->isFeatured();

		$this->render('index',array(
			'dataProvider'=>$model->search(10),
			'columns'=>Playlist::getApiColumns(),
		));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);

		// resort the videos for the logged in user
		$model->videos = UserPlaylist::sort(Yii::app()->user->id, $model->id, $model->videos);

		$this->renderData(array(
			'data'=>$model,
			'columns'=>Playlist::getApiColumns(),
		));
	}

	public function actionCreate()
	{
		$model=new Playlist;

		$model->title = Yii::app()->request->getParam('title');
		
		if($model->title)
		{
			$model->user_id = Yii::app()->user->id;
			$model->status = 1;
			if ($model->save())
			{
				UserPlaylist::subscribe($model->user_id, $model->id);
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->title = Yii::app()->request->getParam('title');
		$model->save();

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$success = $model->delete();
		$this->renderData(array('success'=>$success ? 'true' : 'false'));
	}

	public function actionReorder($id, $order)
	{
		$success = Playlist::reorder($id, $order);
		$this->renderData(array('success'=>$success ? 'true' : 'false'));
	}

	public function actionSubscribed()
	{
		$model = new Playlist('search');
		$model->attributes = $_GET;
		$model->isUserSubscribed();

		$this->render('index',array(
			'dataProvider'=>$model->search(10),
			'columns'=>Playlist::getApiColumns(),
		));
	}

	public function actionSubscribe($id)
	{
		$success = UserPlaylist::subscribe(Yii::app()->user->id, $id);
		$this->renderdata(array('success'=>$success ? 'true' : 'false'));
	}

	public function actionUnsubscribe($id)
	{
		$success = UserPlaylist::unsubscribe(Yii::app()->user->id, $id);
		$this->renderdata(array('success'=>$success ? 'true' : 'false'));
	}

	public function actionAdd($playlist_id, $video_id)
	{
		$success = PlaylistVideo::add($playlist_id, $video_id);
		$video = Video::model()->findByPk($video_id);

		$this->renderData(array(
			'success'=>$success ? 1 : 0,
			'video'=>$success ? $video : null,
		));
	}

	public function actionRemove($playlist_id, $video_id)
	{
		$success = PlaylistVideo::remove($playlist_id, $video_id);
		$this->renderData(array('success'=>$success ? 1 : 0));
	}

	public function loadModel($id)
	{
		$private = ($this->action->id == 'view') ? false : true;

		$model = parent::loadModel($id);

		if ($private && $model->user_id != Yii::app()->user->id)
		{
			$this->renderData(array(
				'error'=>'You are not authorized to perform this action',
				'success'=>'false',
			));
			exit;
		}

		return $model;
	}
}
