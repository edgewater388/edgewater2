<?php

class PreviewController extends ApiController
{
	public function beforeAction($action)
	{
		return true;
	}

	public function actionView($id)
	{
		if (!$model = Video::model()->byAttribute('is_public', 1)->findByPk($id))
			$this->renderData(array('error'=>'You must be logged in to view this page'));

		$this->renderData(array(
			'data'=>$model,
			'columns'=>Video::getApiColumns(),
		));
	}

	public function actionPlayed($id)
	{
		$saved = false;
		$model = new VideoPlayLog;
		$model->video_id = $id;

		if ($model->video && $model->video->is_public)
		{
			$model->user_id = Yii::app()->user->id;
			$model->status = 1;
			$saved = $model->save();
		}

		$this->renderData(array(
			'success'=>$saved ? 'true' : 'false',
		));
	}
}