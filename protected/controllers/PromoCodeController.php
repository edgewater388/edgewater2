<?php

class PromoCodeController extends ApiController
{
	public function actionIndex($code=null)
	{
		if ($model = PromoCode::verify($code))
		{
			$this->renderData(array(
				'success'=>1,
				'message'=>$model->description,
			));
		}

		$this->renderData(array(
			'success'=>0,
			'message'=>'Invalid promo code',
		));
	}

	public function beforeAction($action)
	{
		$valid = true;

		return $valid;
	}
}
