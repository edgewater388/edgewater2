<?php

class RegimenController extends Controller
{
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	public function accessRules()
	{
		return [
			['allow',
				'actions' => ['view', 'mashup', 'seeAll'],
				'users'   => ['@'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function actionView($id)
	{
		if (!$model = Regimen::model()->findByPk($id))
			throw new CHttpException('404', 'Regimen not found');

		if (UserRegimen::checkUserSubscribed(Yii::app()->user->id, $id))
			Yii::app()->request->redirect("/program/{$id}");

		$this->render('//site/pages/regimen', [
			'model'  => $model,
			'videos' => $model->getVideos(),
		]);
	}

// /regimen/mashup?regimen_id=REGIMEN_ID&day=DAY_NUMBER
	public function actionMashup($regimen_id, $day = null)
	{
		if (!$model = Regimen::model()->findByPk($regimen_id)) {
			echo CJSON::encode(['error', 'Regimen not found']);
			exit;
		}

		$data = [
			'id'          => null,
			'title'       => 'Day ' . $day,
			'length'      => null,
			'time'        => null,
			'strength'    => null,
			'cardio'      => null,
			'restorative' => null,
			'is_public'   => null,
			'is_featured' => null,
			'weight'      => null,
			'videos'      => [],
		];

		if ($videos = $model->getVideosByDay()) {
			$mashup = array_key_exists($day, $videos) ? $videos[$day] : [];
			$data['videos'] = DataHandler::render([
				'models'  => $mashup,
				'columns' => Video::getApiColumns(),
			]);
		}

		header('Content-Type: application/json');
		echo CJSON::encode($data);
		exit;
	}
}