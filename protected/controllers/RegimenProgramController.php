<?php

class RegimenProgramController extends Controller
{
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	public function accessRules()
	{
		return [
			['allow',
				'actions' => [
					'mashup',
					'view',
					'subscribeTheProgram',
					'unsubscribeTheProgram',
					'userVideoProgress',
				],
				'users'   => ['@'],
			],
			['allow',
				'actions' => [
					'show',
				],
				'users'   => ['*'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function actionUserVideoProgress($id)
	{
		try {
			if (!$regimen = Regimen::model()->findByPk($id))
				throw new CHttpException('404', 'Regimen not found');

			$u2r = UserRegimen::model()->find('user_id = :user_id AND regimen_id = :regimen_id',
				[':user_id' => Yii::app()->user->id, ':regimen_id' => $id]);

			$watched_videos = json_decode($_POST['videos'], true);

			http_response_code(400);
			header('Content-Type : application/json');

			if (empty($watched_videos)) {
				echo json_encode(['error' => 'true', 'error_description' => 'No videos specified', 'response_data' => '']);
				exit;
			}

			$criteria = new CDbCriteria();
			$criteria->addInCondition('video_id', $watched_videos);
			$criteria->addCondition('day=' . (int)$_POST['regimen_day']);
			$criteria->addCondition('u2r_id=' . $u2r->id);

			if (UserRegimenPlan::model()->updateAll(['is_watched' => 1], $criteria)) {

				http_response_code(200);
				$response = [];
				$response['videos'] = $watched_videos;
				$response['progress'] = UserRegimenPlan::getWorkoutProgress($u2r->id);
				$response['completed_workouts_count'] = UserRegimenPlan::getWatchedWorkoutsCount($u2r->id);
				$response['days_left_count'] = UserRegimenPlan::getDaysLeftCount($u2r->id);
				$response['regimen_finished'] = UserRegimenPlan::checkRegimenFinished($u2r->id);

				echo json_encode([
					'error'             => 'false',
					'error_description' => '',
					'response_data'     => json_encode($response),
				]);
				exit;
			}
		} catch (Exception $e) {
			echo json_encode([
				'error'             => 'true',
				'error_description' => $e->getMessage(),
				'response_data'     => '',
			]);
			exit;
		}

		echo json_encode([
			'error'             => 'true',
			'error_description' => 'One or more videos were not found!',
			'response_data'     => '',
		]);
	}

	public function actionSubscribeTheProgram($id)
	{
		if (UserRegimen::checkUserSubscribed(Yii::app()->user->id, $id))
			Yii::app()->request->redirect("/program/{$id}");

		$u2r = new UserRegimen();
		$u2r->user_id = Yii::app()->user->id;
		$u2r->regimen_id = $id;
		$u2r->old_weight = Yii::app()->user->profile->weight;

		if ($u2r->save()) {
			$today_dow = date('N');
			$days_passed = 0;
			$max_regimen_day = 0;
			$regimenPlanByDays = RegimenVideo::model()->findAll('regimen_id=' . (int)$id);
			$days_to_regimen_end = [];
			$after_first_iteration = 0;
			$previousCalendarPlan = false;

			foreach ($regimenPlanByDays as $planDay) {
				if ($planDay->day >= $today_dow) {
					if (!$after_first_iteration && ($today_dow < $planDay->day))
						$days_passed += $planDay->day - $today_dow;

					$calendarPlan = new UserRegimenPlan();
					$calendarPlan->u2r_id = $u2r->id;
					$calendarPlan->video_id = $planDay->video_id;

					if ($previousCalendarPlan && ($planDay->day - $previousCalendarPlan->day) >= 1) {
						for ($i = 1; $i <= ($planDay->day - $previousCalendarPlan->day); $i++) {
							++$days_passed;
						}
					}

					++$after_first_iteration;

					$calendarPlan->day = $planDay->day;
					$regimen_day = strtotime("today + {$days_passed} days");
					$calendarPlan->date = date('Y-m-d', $regimen_day);
					$calendarPlan->save();

					$previousCalendarPlan = $calendarPlan;

					if ($calendarPlan->day > $max_regimen_day)
						$max_regimen_day = $calendarPlan->day;
				} else {
					$days_to_regimen_end[] = $planDay;
				}
			}

			$previousCalendarPlan = false;
			++$days_passed;

			foreach ($days_to_regimen_end as $planDay) {
				$calendarPlan = new UserRegimenPlan();
				$calendarPlan->u2r_id = $u2r->id;
				$calendarPlan->video_id = $planDay->video_id;
				$calendarPlan->day = $planDay->day + $max_regimen_day;

				if ($previousCalendarPlan && ($calendarPlan->day - $previousCalendarPlan->day) >= 1) {
					for ($i = 1; $i <= ($calendarPlan->day - $previousCalendarPlan->day); $i++) {
						++$days_passed;
					}
				}

				$regimen_day = strtotime("today + {$days_passed} days");
				$calendarPlan->date = date('Y-m-d', $regimen_day);
				$calendarPlan->save();
				$previousCalendarPlan = $calendarPlan;
			}

			Yii::app()->request->redirect("/program/{$id}");
		}

		return null;
	}

	public function actionUnsubscribeTheProgram($id)
	{
		UserRegimen::model()->deleteAll('regimen_id = :regimen_id AND user_id = :user_id',
			[':regimen_id' => $id, ':user_id' => Yii::app()->user->id]);

		$program_name = Regimen::model()->find('id = :id', [':id' => $id])->title;

		Flash::setFlash('success', "You have successfully unsubscribed the program \"{$program_name}\"");

		Yii::app()->request->redirect('/workout-program/' . $id);
	}

	public function actionView($id)
	{
		if (!$regimen = Regimen::model()->findByPk($id))
			throw new CHttpException('404', 'Regimen not found');

		$user = Yii::app()->user;


		if (!UserRegimen::checkUserSubscribed($user->id, $id))
			Yii::app()->request->redirect("/workout-program/{$id}");

		$u2r = UserRegimen::model()->find('user_id = :user_id AND regimen_id = :regimen_id',
			[':user_id' => $user->id, ':regimen_id' => $id]);

		//here go the weeks
		$total_weeks_num = UserRegimenPlan::getTotalWeeksCount($u2r->id);

		$criteria = new CDbCriteria();
		$criteria->alias = 'user_regimen_plan';
		$criteria->condition = 'user_regimen_plan.u2r_id = :u2r_id';
		$criteria->condition .= ' AND (video.status = 1)';
		$criteria->order .= 'day';
		$criteria->join = 'JOIN video ON video.id = user_regimen_plan.video_id';
		$criteria->params = [':u2r_id' => $u2r->id];
		$criteria->order = 'user_regimen_plan.day ASC';

		$regimenWorkouts = UserRegimenPlan::model()->findAll($criteria);

		if (empty($regimenWorkouts))
			throw new CHttpException('206', 'Program exists but no videos here yet');

		$start_date = $regimenWorkouts[0]->date;
		$end_date = end($regimenWorkouts)->date;

		$first_workout = reset($regimenWorkouts);

		$start_date = new DateTime($start_date);
		$end_date = new DateTime($end_date);
		$end_date = $end_date->modify('+1 day');

		$interval = new DateInterval('P1D');
		$workoutPeriod = new DatePeriod($start_date, $interval, $end_date);

		foreach ($regimenWorkouts as $key => $workout) {
			for ($i = 1; $i <= count($regimenWorkouts) - 1; ++$i) {

				if (!isset($regimenWorkouts[$key + $i]))
					break;

				if ($regimenWorkouts[$key + $i]->date == $workout->date) {

					if (!is_array($regimenWorkouts[$key])) {
						unset($regimenWorkouts[$key]);
						$regimenWorkouts[$key][] = $workout;
					}

					$regimenWorkouts[$key][] = $regimenWorkouts[$key + $i];
					unset($regimenWorkouts[$key + $i]);
				}
			}
		}

		$dates_array = [];

		foreach ($regimenWorkouts as $key => $workout) {
			if (is_array($workout))
				$gen_workout = $workout[0];
			else
				$gen_workout = $workout;
			$regimenWorkouts[$gen_workout->date] = $workout;
			$dates_array[] = $gen_workout->date;
			unset($regimenWorkouts[$key]);
		}

		foreach ($workoutPeriod as $date) {
			if (!in_array($date->format('Y-m-d'), $dates_array)) {
				$regimenWorkouts[$date->format('Y-m-d')] = false;
			}
		}

		if ($first_workout->day > 1) {
			for ($i = 1; $i < $first_workout->day; $i++) {
				$regimen_date = date('Y-m-d', strtotime($first_workout->date . " -{$i} days"));

				if (date('Y-m-d', strtotime($u2r->created)) <= $regimen_date) {
					$regimenWorkouts[$regimen_date] = false;
				} else {
					$regimenWorkouts[$regimen_date] = [];
				}
			}
		}

		if (count($regimenWorkouts) % 7 != 0) {
			for ($i = 1; $i <= (count($regimenWorkouts) % 7); $i++) {
				$regimenWorkouts[date('Y-m-d', strtotime(max($dates_array) . " +{$i} days"))] = [];
			}
		}

		ksort($regimenWorkouts);

		$skippedWorkouts = [];

		foreach ($regimenWorkouts as $date => $regimenWorkout) {
			if (empty($regimenWorkout))
				continue;

			if (is_array($regimenWorkout))
				$workout = $regimenWorkout[0];
			else
				$workout = $regimenWorkout;

			if (!$workout->is_watched && $workout->date < date('Y-m-d'))
				$skippedWorkouts[$date] = $regimenWorkout;
		}

		$this->render('//site/pages/program', [
			'model'                    => $regimen,
			'workout_progress'         => UserRegimenPlan::getWorkoutProgress($u2r->id),
			'completed_workouts_count' => UserRegimenPlan::getWatchedWorkoutsCount($u2r->id),
			'total_workouts_count'     => UserRegimenPlan::getTotalWorkoutsCount($u2r->id),
			'skipped_count'            => UserRegimenPlan::getSkippedDaysCount($u2r->id),
			'total_weeks_count'        => $total_weeks_num,
			'days_left_count'          => UserRegimenPlan::getDaysLeftCount($u2r->id),
			'regimenWorkouts'          => $regimenWorkouts,
			'first_workout'            => $first_workout,
			'last_workout_day'         => RegimenVideo::getMaxDayByRegimenID($u2r->regimen_id),
			'skippedWorkouts'          => $skippedWorkouts,
			'user'                     => Users::model()->findByPk($user->id),
			'cancel_program_url'       => "/program/{$id}/unsubscribe",
			'next_week_url'            => isset($next_week_num) ? "/program/{$id}/week/{$next_week_num}" : null,
			'prev_week_url'            => isset($prev_week_num) ? "/program/{$id}/week/{$prev_week_num}" : null,
			'current_week_num'         => UserRegimenPlan::getCurrentWeekNum($u2r->id),
			'program_id'               => $id,
		]);
	}
	
	public function actionShow($id) 
	{
		if (($program = UserRegimenPlan::model()->findByPk($id)) == null) {
			return;
		}
		$this->ogTitle = "Hi everyone, I am starting Physique 57's ".$program->getWeekNum()." week(s) workout program. Help me stay motivated during these ".$program->getWeekNum()." week(s) by liking this post!";
		$this->ogUrl = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$this->ogDescription = $program->userRegimen->regimen->description;
		$this->ogImage = $program->video->meta->videoStillURL;
		$this->render('//site/pages/workout-program-page', ['program' => $program]);
	}
}