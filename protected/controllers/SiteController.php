<?php

class SiteController extends Controller
{
	public $displayNewsletterSignup = true;

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return [
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => [
				'class'     => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			],
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'    => [
				'class' => 'CViewAction',
			],
		];
	}

	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			['allow',
				'actions' => ['index', 'contact', 'error', 'page', 'indexNew'],
				'users'   => ['*', '@'],
			],
			['allow',  // allow all users to perform 'index' and 'view' actions
				'actions' => ['login', 'signUpNew', 'startCountdown', 'resetCountdown'],
				'users'   => ['*'],
			],
			['allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => ['logout', 'myworkouts', 'mashupMaker'],
				'users'   => ['@'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex($login = null, $video = null, $mashup = null)
	{
		$this->pageTitle = 'Online workout videos for beginners | Online barre workout dvds';
		$this->ogTitle = 'Online workout videos for beginners | Online barre workout dvds';
		$this->ogDescription = 'Our muscle-toning routines and fitness plans get you fit fast. NEW TO THE BARRE will teach you the techniques and exercises that will transform your body.';

		if ($video && !Yii::app()->user->id) {
			Yii::app()->session['auto_play_video'] = $video;
			$this->redirect('/?login=1');
		}

		if ($mashup && !Yii::app()->user->id) {
			Yii::app()->session['auto_play_mashup'] = $mashup;
			$this->redirect('/?login=1');
		}

		$q = $_SERVER['QUERY_STRING'] ? '?' . $_SERVER['QUERY_STRING'] : '';

		if (Yii::app()->user->id && Yii::app()->request->pathInfo != 'home')
			$this->redirect('/home' . $q);
		else if (!Yii::app()->user->id && Yii::app()->request->pathInfo == 'home')
			$this->redirect('/' . $q);

		if (Yii::app()->user->id && Yii::app()->user->membership) {
			if (Yii::app()->user->membership->isActive()) {
				return $this->render('pages/home-signed-in', [
					'regimens'           => Regimen::getFeaturedRegimens(),
					'playlists'          => Playlist::getFeaturedPlaylists(),
					'new_videos'          => Video::getNewVideos(),
					'live_classes'       => Video::getLiveClasses(3),
					'videos_by_duration' => Video::getAllByDuration(3),
				]);
			}

			return $this->redirect('/account');
		}

		return $this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else {
				if ($error['code'] == '404')
					$error['message'] = 'This page can not be found';
				$this->render('error', $error);
			}
		}
	}

	public function actionContact($video_id = null)
	{
		$model = new ContactForm;

		if (Yii::app()->user->id) {
			$model->first_name = Yii::app()->user->profile->first_name;
			$model->last_name = Yii::app()->user->profile->last_name;
			$model->email = Yii::app()->user->email;
		}

		if ($video_id) {
			$model->body = 'Report a problem for video: ';

			if ($video = Video::model()->findByPk($video_id))
				$model->body .= $video->title . ' (' . $video_id . ')';
			else
				$model->body .= 'Video (' . $video_id . ')';
		}

		if (isset($_POST['ContactForm'])) {
			$model->attributes = $_POST['ContactForm'];
			if ($model->send()) {
				Flash::setFlash('success', 'Your message has been sent');
				$model = new ContactForm;
			}
		}

		$this->render('//site/pages/contact-us', [
			'model' => $model,
		]);
	}

	public function actionMyworkouts()
	{
		$this->render('//site/pages/my-workouts', [
			'playlists' => Playlist::getSubscribedPlaylists(),
			'videos'    => Video::getFavoriteVideos(false),
		]);
	}

	public function actionMashupMaker()
	{
		$this->render('//site/pages/mashup-maker', [
			'videos' => Video::getVideos(9, 'shortest'),
		]);
	}

	public function actionStartCountdown()
	{
		header('Content-Type: application/json');

		$countdown_timestamp = Settings::getSettingValue(Settings::SETTING_COUNTDOWN_TO);

		if (!empty($countdown_timestamp)) {
			echo CJSON::encode(['error' => 'Countdown is already set']);
			exit;
		}

		$countdown_timestamp = strtotime('+4 days');
		$settings = new Settings();

		if (!$settings->saveSetting(Settings::SETTING_COUNTDOWN_TO, $countdown_timestamp)) {
			echo CJSON::encode(['error' => 'Cannot set countdown']);
			exit;
		}

		echo CJSON::encode(['timestamp' => $countdown_timestamp]);
		exit;
	}

	public function actionResetCountdown()
	{
		header('Content-Type: application/json');

		$countdown_timestamp = Settings::getSettingValue(Settings::SETTING_COUNTDOWN_TO);

		if (empty($countdown_timestamp)) {
			echo CJSON::encode(['error' => 'Counter is not set']);
			exit;
		}

		if (time() < $countdown_timestamp) {
			echo CJSON::encode(['error' => 'Countdown is not finished yet']);
			exit;
		}

		$countdown_timestamp = strtotime('+4 days');
		$settings = new Settings();

		if ($settings->saveSetting(Settings::SETTING_COUNTDOWN_TO, $countdown_timestamp)) {
			echo CJSON::encode(['timestamp' => $countdown_timestamp]);
			exit;
		}

		echo CJSON::encode(['error' => 'Countdown hasn\'t been reset']);
		exit;
	}

	/**
	 * Displays the contact page
	 */
	// public function actionContact()
	// {
	// 	$model=new ContactForm;
	// 	if(isset($_POST['ContactForm']))
	// 	{
	// 		$model->attributes=$_POST['ContactForm'];
	// 		if($model->validate())
	// 		{
	// 			$headers="From: {$model->email}\r\nReply-To: {$model->email}";
	// 			mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
	// 			Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
	// 			$this->refresh();
	// 		}
	// 	}
	// 	$this->render('contact',array('model'=>$model));
	// }
}