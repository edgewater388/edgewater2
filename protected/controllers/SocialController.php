<?php

class SocialController extends Controller
{
	const FACEBOOK_API_URL = 'https://graph.facebook.com/v2.9/';
	const FACEBOOK_FIELDS = ['first_name', 'last_name', 'email', 'gender', 'birthday', 'link'];

	public function accessRules()
	{
		return [
			['allow',  // allow all users to perform 'index' and 'view' actions
				'actions' => ['authFacebook', 'authGoogle'],
				'users'   => ['*'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	protected function getRedirectBackUrl()
	{
		return !empty(Yii::app()->session['social_redirect_url']) ?
			Yii::app()->session['social_redirect_url'] :
			$this->createAbsoluteUrl('/');
	}

	public function actionAuthFacebook()
	{
		if (!isset($_GET['code'])) {
			throw new CHttpException('404', 'Page not found.');
		}

		$code = $_GET['code'];

		$client_id = Yii::app()->params->facebook['client_id'];
		$client_secret = Yii::app()->params->facebook['client_secret'];
		$redirect_uri = $this->createAbsoluteUrl('social/authFacebook');

		$url = static::FACEBOOK_API_URL . "oauth/access_token?client_id={$client_id}&redirect_uri={$redirect_uri}&client_secret={$client_secret}&code={$code}";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = json_decode(curl_exec($ch));
		curl_close($ch);

		$url = static::FACEBOOK_API_URL . "me?access_token={$result->access_token}&fields=" . implode(',', static::FACEBOOK_FIELDS);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = json_decode(curl_exec($ch));
		curl_close($ch);

		$profile = new SocialProfile();

		foreach ($result as $item => $value) {
			$profile->{$item} = $value;
		}

		$profile->image_url = "https://graph.facebook.com/{$profile->id}/picture?type=large";

		$identity = new FacebookIdentity($profile);

		if ($identity->authenticate()) {
			$duration = 3600 * 24 * 30;
			if (Yii::app()->user->login($identity, $duration)) {
				Flash::setFlash('success', 'You have successfully logged in with your Facebook account.');
			}
		}

		$this->redirect($this->getRedirectBackUrl());
	}

	public function actionAuthGoogle()
	{
		if (!isset($_GET['code'])) {
			throw new CHttpException('404', 'Page not found.');
		}

		$code = $_GET['code'];

		$client_id = Yii::app()->params->google['client_id'];
		$client_secret = Yii::app()->params->google['client_secret'];
		$redirect_uri = $this->createAbsoluteUrl('social/authGoogle');

		$url = "https://accounts.google.com/o/oauth2/token";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "code={$code}&redirect_uri={$redirect_uri}&client_id={$client_id}&client_secret={$client_secret}&scope=profile%20openid%20email%20&grant_type=authorization_code");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = json_decode(curl_exec($ch));
		curl_close($ch);

		$url = "https://www.googleapis.com/plus/v1/people/me?token={$result->access_token}";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-length: 0',
			'Authorization: Bearer ' . $result->access_token,
		]);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = json_decode(curl_exec($ch));
		curl_close($ch);

		$profile = new SocialProfile();
		$profile->id = $result->id;
		$profile->email = $result->emails[0]->value;

		if (!empty($result->name)) {
			$profile->first_name = $result->name->givenName;
			$profile->last_name = $result->name->familyName;
		}

		$profile->gender = isset($result->gender) ? $result->gender : null;
		$profile->link = isset($result->url) ? $result->url : null;
		$profile->image_url = preg_replace('/\?.*/', '', $result->image->url);

		$identity = new GoogleIdentity($profile);

		if ($identity->authenticate()) {
			$duration = 3600 * 24 * 30;
			if (Yii::app()->user->login($identity, $duration)) {
				Flash::setFlash('success', 'You have successfully logged in with your Google Plus account.');
			}
		}

		$this->redirect($this->getRedirectBackUrl());
	}
}