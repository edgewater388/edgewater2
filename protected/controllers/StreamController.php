<?php

class StreamController extends Controller {
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		return [
			[
				'allow', // allow authenticated user to perform actions
				'actions' => [ 'status' ],
				'users'   => [ '@' ],
			],
		];
	}
	
	public function actionStatus() {
		$stream = Stream::getStream();
		
		$result = [];
		
		if ( $stream ) {
			$result = [
				'stream'       => true,
				'enabled'      => $stream->status,
				'startLiveAt'  => strtotime( $stream->time_start ),
				'endLiveAt'    => strtotime( $stream->time_end ),
				'startVideoIn' => $stream->timer,
			];
		}
		
		echo json_encode( $result );
	}
}