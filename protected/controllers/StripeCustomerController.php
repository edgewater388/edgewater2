<?php

class StripeCustomerController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'pro', 'manage'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Create/Update model.
	 */
	public function actionIndex()
	{
		$this->pageTitle = 'Billing Information';
		$model = $this->loadModel();

		if (!$model)
		{
			$model = new StripeCustomer;
		}
		else
			$model->card_token = 1;
		
		$model->scenario = 'createupdate';

		if(isset($_POST['StripeCustomer']))
		{
			$model->attributes=$_POST['StripeCustomer'];
			if($model->save())
			{
				Flash::setFlash('success', 'Your billing information has been updated');
				$this->redirect(array('index'));
			}
		}

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return StripeCustomer the loaded model
	 * @throws CHttpException
	 */
	public function loadModel()
	{
		$model=StripeCustomer::model()->byUser(Yii::app()->user->id)->find();
		return $model;
	}

	public function loadFeeModel()
	{
		$model=StripeCustomer::model()->byUser(Yii::app()->user->id)->find();
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param StripeCustomer $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='stripe-customer-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
