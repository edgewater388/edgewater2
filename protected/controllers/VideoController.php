<?php

class VideoController extends ApiController
{
	public function actionIndex()
	{
		$model = new Video('search');
		$model->attributes = $_GET;
		$scope = isset($_GET['sort']) ? ($_GET['sort'] == 'longest') ? 'byLongest' : 'byShortest' : null;
		$model->isPublic(false)->isLiveClass(false);

		if (!$scope) {
			if (isset($_SERVER['HTTP_REFERER']) && stristr($_SERVER['HTTP_REFERER'], 'mashup-maker'))
				$scope = 'byShortest';
			else
				$scope = 'byLongest';
		}

		$this->renderData([
			'dataProvider' => $model->$scope()->search(9),
			'columns'      => Video::getApiColumns(),
		]);
	}

	public function actionView($id)
	{
		$model = Video::model()->findByPk($id);

		$this->renderData([
			'data'    => $model,
			'columns' => Video::getApiColumns(),
		]);
	}

	public function actionPlayed($id)
	{
		if (Yii::app()->user->isGuest)
			throw new CHttpException('404', 'Page not found.');

		$saved = false;
		$model = new VideoPlayLog;
		$model->video_id = $id;

		if ($model->video) {
			$model->user_id = Yii::app()->user->id;
			$model->status = 1;
			$saved = $model->save();
		}

		$this->renderData([
			'success' => $saved ? 'true' : 'false',
		]);
	}

	public function actionFavorites()
	{
		$model = new Video('search');
		$model->attributes = $_GET;
		$model->favorites();

		$this->renderData([
			'dataProvider' => $model->search(false),
			'columns'      => Video::getApiColumns(),
		]);
	}

	public function actionFavorite($id)
	{
		$success = UserVideo::add(Yii::app()->user->id, $id);
		$this->renderData([
			'success'  => $success ? 'true' : 'false',
			'video_id' => $id,
		]);
	}

	public function actionUnfavorite($id)
	{
		$success = UserVideo::remove(Yii::app()->user->id, $id);
		$this->renderData([
			'success'  => $success ? 'true' : 'false',
			'video_id' => $id,
		]);
	}

	public function actionAllByDuration($duration)
	{
		if (!in_array($duration, Video::getDurations())) {
			throw new CHttpException('404', 'Page not found.');
		}

		$this->renderData([
			'models'  => Video::getByDuration($duration),
			'columns' => Video::getApiColumns(),
		]);
	}

	public function actionAllLiveClasses()
	{
		$this->renderData([
			'models'  => Video::getLiveClasses(),
			'columns' => Video::getApiColumns(),
		]);
	}

	public function actionFilter($duration = null, $is_live_class = null)
	{
		if (!Yii::app()->request->isPostRequest) {
			throw new CHttpException('404', 'Page not found.');
		}

		if ($duration && !in_array($duration, Video::getDurations())) {
			throw new CHttpException('404', 'Page not found.');
		}

		$model = new Video('search');
		$model->attributes = array_filter($_POST);
		$model->is_public = 0;

		if ($duration) {
			$model->duration = $duration;
		} else {
			//TODO: refactor the code, remove else and duration check - make it based on the route
			$is_live_class = 1;
		}

		$model->isPublic(false)->isLiveClass($is_live_class);

		//TODO: find out why $is_public and $is_live_class fields are being set to null on "search" action
		$model->is_live_class = $is_live_class;

		$this->renderData([
			'dataProvider' => $model->search(),
			'columns'      => Video::getApiColumns(),
		]);
	}
}