<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 06/07/17
 * Time: 18:13
 */

class VideoPageController extends Controller {
	
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			['allow',
				'actions' => ['show'],
				'users'   => ['@', '*'],
			],
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function actionShow($id) 
	{
		if (($video = Video::model()->findByPk($id)) == null) {
			return;
		}
		//$this->ogTitle = $video->id;
		$this->ogTitle = "Hi everyone, I just did Physique 57's {$video->title} video. Help me stay motivated by liking this post!";
		$this->ogUrl = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];;
		$this->ogDescription = $video->description;
		$this->ogImage = $video->meta->thumbnailURL;
		$this->render('//site/pages/video-page', ['video' => $video]);
	}
}

