DROP TABLE IF EXISTS AuthAssignment;
DROP TABLE IF EXISTS AuthItemChild;
DROP TABLE IF EXISTS AuthItem;
DROP TABLE IF EXISTS profile;
DROP TABLE IF EXISTS files;
DROP TABLE IF EXISTS users;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `sessionkey` varchar(128) NOT NULL DEFAULT '',
  `lastvisit` int(10) unsigned NOT NULL DEFAULT '0',
  `confirmation_key` varchar(64) DEFAULT NULL,
  `terms_accepted` tinyint(1) unsigned DEFAULT NULL,
  `settings` blob,
  `status` int(1) unsigned NOT NULL DEFAULT '0',
  `created` int(10) unsigned NOT NULL DEFAULT '0',
  `updated` int(10) unsigned NOT NULL DEFAULT '0',
  `confirmed` tinyint(1) DEFAULT NULL,
  `forgot_key` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO users (password, email, status) VALUES (MD5('gl0bal'), 'davidtiede@gmail.com', 1);

CREATE TABLE `files` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `service` varchar(16) DEFAULT NULL,
  `private` tinyint(1) unsigned DEFAULT '0',
  `filename` varchar(255) DEFAULT NULL,
  `filepath` varchar(255) DEFAULT NULL,
  `filemime` varchar(255) DEFAULT NULL,
  `filesize` int(10) unsigned DEFAULT NULL,
  `filetype` varchar(16) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `created` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `files_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

CREATE TABLE `profile` (
  `user_id` int(11) unsigned NOT NULL,
  `salutation` varchar(8) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL DEFAULT '',
  `last_name` varchar(50) NOT NULL DEFAULT '',
  `company` varchar(50) DEFAULT NULL,
  `street` varchar(64) DEFAULT NULL,
  `street2` varchar(64) DEFAULT NULL,
  `city` varchar(32) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `state` varchar(64) DEFAULT NULL,
  `zip_code` varchar(6) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `phone2` varchar(20) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `description` text,
  `facebook` varchar(64) DEFAULT NULL,
  `twitter` varchar(64) DEFAULT NULL,
  `youtube` varchar(64) DEFAULT NULL,
  `photo_fid` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `completed` float DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `status` (`status`),
  KEY `photo_fid` (`photo_fid`),
  CONSTRAINT `profile_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `profile_ibfk_2` FOREIGN KEY (`photo_fid`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO profile (user_id, first_name, last_name, status) VALUES (1, 'David', 'Tiede', 1);

CREATE TABLE `AuthItem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `AuthItemChild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `AuthAssignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` int(10) unsigned NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  KEY `userid` (`userid`),
  CONSTRAINT `authassignment_ibfk_2` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO AuthItem (name, type, description, bizrule, data) VALUES ('accessBackend', 0, 'access backend', NULL, 'N;');
INSERT INTO AuthItem (name, type, description, bizrule, data) VALUES ('admin', 2, '', NULL, 'N;');
INSERT INTO AuthItem (name, type, description, bizrule, data) VALUES ('super admin', 2, '', NULL, 'N;');
INSERT INTO AuthItemChild (parent, child) VALUES ('super admin', 'admin');
INSERT INTO AuthItemChild (parent, child) VALUES ('admin', 'accessBackend');
INSERT INTO AuthAssignment (itemname, userid, bizrule, data) VALUES ('super admin', 1, NULL, 'N;');
