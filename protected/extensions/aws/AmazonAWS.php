<?php

/**
  * NOTE: This file takes place of the config.inc.php files that comes with the sdk
  * 	  The reason for this is to be able to set the aws credentials in the config
  *		  file, rather than a separate file.
  *
  *		  Simple wrapper to Amazon's AWS SDK.
  *
  *		  Author: David Tiede (davidtiede@gmail.com)
  *		  January 17, 2013
  *
  */

// Include the SDK
require 'sdk/autoload.php';

use Aws\Common\Aws;

class AmazonAWS extends CApplicationComponent
{
	public $key;
	public $secret;
	public $bucket;
	public $buckets;
	public $env;
	public $aws; // instance
	public $cloudfront;
	public $cdn;

	public function init()
	{
		$aws = Aws::factory(array(
			'key'    => $this->key,
			'secret' => $this->secret,
			// 'region' => Region::US_WEST_2
			)
		);

		if (!$this->cdn)
			$this->cdn = 'https://s3.amazonaws.com/'.$this->bucket;

		// Set the default environment to development
		$this->env = Yii::app()->environment;

		// Set the buckets/directory structure
		$this->buckets = new stdClass();
		$this->buckets->uploads = $this->bucket.'/'.$this->env;
		// $this->buckets->static = $this->bucket.'/static/'.$this->env;

		$this->aws = $aws;
	}

	public function getSignedUrl($url)
	{
		$cf = $this->aws->get('cloudfront');
		$cf->setConfig(array(
			'key_pair_id'=>$this->cloudfront['key_pair_id'],
			'private_key'=>dirname(__FILE__).'/keys/pk-'.$this->cloudfront['key_pair_id'].'.pem',
			)
		);
		
		$expire = time()+3*60;
		$options = array(
			'url'=>$url,
			'policy'=>'{"Statement":[{"Resource":"'.$url.'", "Condition":{"DateLessThan":{"AWS:EpochTime":'.$expire.'}}}]}',
			'expire'=>$expire,
		);

		return $cf->getSignedUrl($options);
	}
}