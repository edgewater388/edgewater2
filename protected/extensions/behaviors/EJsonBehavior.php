<?php
class EJsonBehavior extends CBehavior
{
	/**
	 *
	 * @var type 
	 */
	private $owner;
	
	/**
	 *
	 * @var type 
	 */
	private $relations;
	
	/**
	 * 
	 * @return boolean
	 */
	public function toJSON(){
		$this->owner = $this->getOwner();
		
		if (is_subclass_of($this->owner,'CActiveRecord')){
			$attributes = $this->owner->getAttributes($this->owner->fields());
			$this->relations = $this->getRelated();
			$jsonDataSource = ['jsonDataSource' => ['attributes' => $attributes, 'relations' => $this->relations]];
			
			return CJSON::encode($jsonDataSource);
		}
		return false;
	}
	/**
	 * 
	 * @return type
	 */
	private function getRelated()
	{	
		$related = [];
		$obj = null;
		$relations = $this->owner->extraFields();
		
		foreach ($relations as $name){
			$obj = $this->owner->getRelated($name);
			$related[$name] = $obj instanceof CActiveRecord ? $obj->getAttributes($obj->fields()) : $obj;
		}
	    
	    return $related;
	}
}