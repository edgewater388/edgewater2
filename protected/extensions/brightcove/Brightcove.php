<?php

class Brightcove extends CApplicationComponent
{
	public $token;
	private $baseUrl = 'https://api.brightcove.com/services/library';

	// api.brightcove.com/services/library?
	// command=search_videos&
	// page_size=5&
	// video_fields=id%2Cname%2CshortDescription%2ClongDescription%2CcreationDate%2CpublishedDate%2ClastModifiedDate%2Cversion%2ClinkURL%2ClinkText%2Ctags%2CcustomFields%2CcuePoints%2CvideoStillURL%2CvideoStill%2CthumbnailURL%2Cthumbnail%2ClogoOverlay%2CreferenceId%2Clength%2Ceconomics%2CplaysTotal%2CplaysTrailingWeek%2CFLVURL%2Crenditions%2CiOSRenditions%2CHDSRenditions%2ChdsManifestUrl%2CWVMRenditions%2CsmoothRenditions%2CsmoothManifestUrl%2CHLSURL%2CFLVFullLength%2CvideoFullLength%2Ccaptioning%2CadKeys%2CdigitalMaster%2ClogoOverlay%2CaccountId%2CitemState%2CstartDate%2CendDate%2CgeoFiltered%2CgeoFilteredCountries%2CgeoFilterExclude%2CsharedByExternalAcct%2CsharedToExternalAcct&
	// media_delivery=default&
	// sort_by=DISPLAY_NAME%3AASC&
	// page_number=0&
	// get_item_count=true&
	// callback=BCL.onSearchResponse&
	// token=ZY4Ls9Hq6LCBgleGDTaFRDLWWBC8uoXQun0xEEXtlDUHBDPZBCOzbw..

	protected function _call($params)
	{
		$q = array();
		$params['token'] = $this->token;
		
		foreach ($params as $k=>$v)
			$q[] = $k.'='.$v;

		$q = implode('&', $q);

		$response = Yii::app()->curl->get($this->baseUrl.'?'.$q);
		return json_decode($response);
	}

	public function getVideos()
	{
		$videos = array();

		$params['command'] = 'search_videos';
		$params['page_size'] = 50;
		$params['page_number'] = 0;
		$params['get_item_count'] = 'true';
		$params['sort_by'] = 'DISPLAY_NAME:ASC';
		$params['video_fields'] = 'id,name,shortDescription,longDescription,creationDate,publishedDate,lastModifiedDate,version,tags,videoStillURL,thumbnailURL,length,economics,playsTotal,playsTrailingWeek,FLVURL,HLSURL,FLVFullLength,videoFullLength,accountId,itemState';

		$continue = true;

		while ($continue == true)
		{
			$result = $this->_call($params);

			// echo '<pre>';
			// print_r($result);
			// echo '</pre>';
			// exit;

			foreach ($result->items as $item)
				$videos[] = $item;

			$continue = ($result->total_count > $result->page_number * $result->page_size) ? true : false;
			$params['page_number'] += 1;

			sleep(2);
		}

		return $videos;
	}
}