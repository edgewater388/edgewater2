<?php

Yii::import('application.vendors.jwplatform.api', true);

class JWPlatform extends CApplicationComponent
{
	public $key;
	public $secret;

	public function getVideos()
	{
		$api = new BotrAPI($this->key, $this->secret);

		// Here's an example call that lists all videos.
		$params = array(
			'result_limit'=>'1000',
		);

		$result = $api->call('/videos/list', $params);

		return isset($result['videos']) ? $result['videos'] : array();
	}
}