<?php

class CString 
{
	public static function substr($str, $len, $onWordbreak=true)
	{
		if (strlen($str) < $len)
			return $str;

		// Cut the string to the maximum length
		$str = substr($str, 0, $len);

		if ($onWordbreak)
		{
			// Cut the string at the end of the last full word
			$str = substr($str, 0, strrpos($str, ' '));
		}

		// Add an elipsis
		$str .= '...';

		return $str;
	}

	public static function slug($str)
	{
		return strtolower(str_ireplace(' ', '-', $str));
	}

	public static function range($start, $end)
	{
		return array_combine(range($start,$end), range($start,$end));
	}

	public static function nl2br($str)
	{
		return nl2br($str);
	}

	public static function br2nl($str)
	{
		return str_ireplace('<br>', "\r\n", $str);
	}

	public static function randomHash($length=8)
	{
		return substr(md5(uniqid(rand(), true)), 16, $length);
	}

	public static function url($str)
	{
		if (substr($str, 0, 4) == 'http')
			return $str;
		return 'http://'.$str;
	}

	public static function machineName($str)
	{
		$str = preg_replace("/[^ \w]+/", " ", $str);
		return strtolower(str_ireplace(' ', '_', trim($str)));
	}

	public static function splitName($name)
	{
		$name = trim($name);

		if (!$first_space = strpos($name, ' '))
			$first_space = strlen($name);

		$first_name = trim(substr($name, 0, $first_space));
		$last_name = trim(substr($name, $first_space));

		return array($first_name, $last_name);
	}
}