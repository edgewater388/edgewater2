<?php


// Print time ago, instead of actual timestamp
class Time
{ 

  public static function timeAgo($time)
  {
    if (!$time)
      return 'Never';
    
    $now = time();

    if (!is_numeric($time))
      $time = strtotime($time);
    
    if ($now - $time < 0)
      return date('M j', $time);
    else if ($now - $time < 60)
      return (floor($now - $time) <= 1) ? 'a second ago' : $now - $time .' seconds ago';
    else if ($now - $time < 60 + 60)
      return 'about a minute ago';
    else if ($now - $time < 3600)
      return floor(($now - $time) / 60) .' minutes ago';
    else if ($now - $time < 3600 + 3600)
      return 'about an hour ago';
    else if ($now - $time < 86400) // 86400 == 24 hours
      return floor(($now - $time) / 60 / 60) .' hours ago';
    else if ((date('j', $now) - date('j', $time)) === 1) // 1 day ago
        return '1 day ago';
    else if (date('Y', $now) == date('Y', $time))
      return date('M j', $time);
    else
      return date('M j, Y', $time);
  }

  public static function formatRange($from, $to, $style='short')
  {
    // $year_from = date('Y', $from);
    // $year_to = date('Y', $to);

    // if ($year_from == $year_to)
    // $day_from = date('d', $from);
    // $day_to = date('d', $to);
    
    // return dat
  }

  public static function formatTime($from, $to, $includePeriod=true)
  {
    $from = date('g:i', strtotime($from));
    $to = date('g:i', strtotime($to));

    $time = $from;
    if ($from != $to)
      $time .= ' - '.$to;

    if ($includePeriod)
      $time .= date('a', strtotime($to));

    return $time;
  }

  public static function convertToDateTime($time)
  {
    if (!is_numeric($time))
      $time = (int) $time;
    
    return date('Y-m-d H:i:s', $time);
  }

  public static function calculateNextMonth($cur_date=null, $prev_date=null)
  {
    if (!$cur_date)
      $cur_date = date('Y-m-d');
    else if (is_numeric($cur_date))
      $cur_date = date('Y-m-d', $cur_date);

    // Select date + 1 month or last day of next month, which ever date comes first.
    // i.e. next month for 2013-01-31 should be 2013-02-28, not 2013-03-02 (as would be
    // calculate from date + 1 month).
    $next_month = strtotime($cur_date.' + 1 month');
    $last_day_next_month = strtotime($cur_date.' last day of next month');

    if ($last_day_next_month < $next_month)
      $next_month = $last_day_next_month;

    $date = date('Y-m-d',$next_month);

    // For instances where cur_date = something like Feb 28, but prev_date was January 30, the next month
    // should really go back to the 30th.
    // if ($prev_date)
    // {
    //  $day1 = date('d', $next_month);
    //  $day2 = date('d', strtotime($prev_date));

    //  if ($day2 > $day1)
    //    $date = date('Y-m-',$next_month) . $day2;
    // }

    return $date;
  }

  public static function months()
  {
    return array(1=>'01 - Jan',2=>'02 - Feb',3=>'03 - Mar',4=>'04 - Apr',5=>'05 - May',6=>'06 - Jun',7=>'07 - Jul',8=>'08 - Aug',9=>'09 - Sep',10=>'10 - Oct',11=>'11 - Nov',12=>'12 - Dec');
  }
  
  public static function years()
  {
    $years = array();
    $cur_year = date('Y',time());
    for ($i=1; $i<12; $i++) {
      $years[$cur_year] = $cur_year;
      $cur_year++;
    }
    return $years;
  } 
}