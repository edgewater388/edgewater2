<?php

class m130521_142447_base_tables extends CDbMigration
{
	public function up()
	{
		$sql = file_get_contents(dirname(__FILE__).'/../data/schema.mysql.sql');
		$this->execute($sql);
		
		$sql = file_get_contents(dirname(__FILE__).'/../data/schema-custom.mysql.sql');
		$this->execute($sql);

		return true;
	}

	public function down()
	{
		echo 'Cannot delete base tables.';
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}