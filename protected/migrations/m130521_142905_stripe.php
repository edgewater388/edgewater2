<?php

class m130521_142905_stripe extends CDbMigration
{
	public function up()
	{
		$this->execute("
DROP TABLE IF EXISTS `stripe_customer`;
DROP TABLE IF EXISTS `stripe_subscription`;
DROP TABLE IF EXISTS `stripe_transaction`;

CREATE TABLE `stripe_customer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `customer_id` varchar(32) DEFAULT NULL,
  `scope` varchar(16) DEFAULT NULL,
  `live_mode` tinyint(1) unsigned DEFAULT NULL,
  `last4` varchar(4) DEFAULT NULL,
  `card_type` varchar(32) DEFAULT NULL,
  `exp_month` varchar(2) DEFAULT NULL,
  `exp_year` varchar(4) DEFAULT NULL,
  `fingerprint` varchar(32) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `address_line1` varchar(128) DEFAULT NULL,
  `address_line2` varchar(128) DEFAULT NULL,
  `address_city` varchar(128) DEFAULT NULL,
  `address_state` varchar(128) DEFAULT NULL,
  `address_zip` varchar(8) DEFAULT NULL,
  `address_country` varchar(128) DEFAULT NULL,
  `cvc_check` varchar(12) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `delinquent` tinyint(1) unsigned DEFAULT '0',
  `subscription` varchar(128) DEFAULT NULL,
  `discount` varchar(128) DEFAULT NULL,
  `account_balance` int(11) DEFAULT NULL,
  `terms_accepted` tinyint(1) unsigned DEFAULT NULL,
  `created` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `stripe_customer_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `stripe_subscription` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `stripe_transaction` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `confirm_code` varchar(8) DEFAULT NULL,
  `amount` decimal(16,2) DEFAULT NULL,
  `net_amount` decimal(16,2) DEFAULT NULL,
  `card_token` varchar(32) DEFAULT NULL,
  `address_city` varchar(128) DEFAULT NULL,
  `address_country` varchar(128) DEFAULT NULL,
  `address_line1` varchar(128) DEFAULT NULL,
  `address_line2` varchar(128) DEFAULT NULL,
  `address_state` varchar(128) DEFAULT NULL,
  `address_zip` varchar(8) DEFAULT NULL,
  `exp_month` varchar(2) DEFAULT NULL,
  `exp_year` varchar(4) DEFAULT NULL,
  `fingerprint` varchar(32) DEFAULT NULL,
  `last4` varchar(4) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `card_type` varchar(32) DEFAULT NULL,
  `live_mode` tinyint(1) unsigned DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `paid` tinyint(1) unsigned DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `refunded` tinyint(1) unsigned DEFAULT NULL,
  `fee` decimal(16,5) DEFAULT NULL,
  `cvc_check` varchar(12) DEFAULT NULL,
  `captured` tinyint(1) DEFAULT NULL,
  `terms_accepted` tinyint(1) unsigned DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		
		");

		return true;
	}

	public function down()
	{
		$this->execute("
DROP TABLE IF EXISTS `stripe_customer`;
DROP TABLE IF EXISTS `stripe_subscription`;
DROP TABLE IF EXISTS `stripe_transaction`;
			");
		
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}