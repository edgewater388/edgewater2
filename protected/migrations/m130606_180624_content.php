<?php

class m130606_180624_content extends CDbMigration
{
	public function up()
	{
		$this->execute('DROP TABLE IF EXISTS content');
		$this->execute("CREATE TABLE `content` (
  `key` varchar(32) NOT NULL DEFAULT '',
  `value` text,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		return true;
	}

	public function down()
	{
		$this->execute('DROP TABLE content');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}