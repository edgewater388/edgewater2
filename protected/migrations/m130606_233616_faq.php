<?php

class m130606_233616_faq extends CDbMigration
{
	public function up()
	{
		$this->execute('DROP TABLE IF EXISTS faq');
		$this->execute('DROP TABLE IF EXISTS faq_category');
		
		$this->execute("CREATE TABLE `faq_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(32) DEFAULT NULL,
  `weight` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		$this->execute("CREATE TABLE `faq` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) DEFAULT NULL,
  `faq_category_id` int(10) unsigned DEFAULT NULL,
  `answer` text,
  `weight` tinyint(4) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `created` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `faq_category_id` (`faq_category_id`),
  CONSTRAINT `faq_ibfk_1` FOREIGN KEY (`faq_category_id`) REFERENCES `faq_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		return true;
	}

	public function down()
	{
		$this->execute('DROP TABLE faq');
		$this->execute('DROP TABLE faq_category');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}