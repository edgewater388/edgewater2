<?php

class m130609_180210_facebook extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE users ADD COLUMN facebook_id varchar(20) DEFAULT NULL AFTER password');
		$this->execute('ALTER TABLE users ADD COLUMN reg_vendor varchar(32) DEFAULT NULL AFTER facebook_id');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE users DROP COLUMN facebook_id');
		$this->execute('ALTER TABLE users DROP COLUMN reg_vendor');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}