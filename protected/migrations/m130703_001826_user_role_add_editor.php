<?php

class m130703_001826_user_role_add_editor extends CDbMigration
{
	public function up()
	{
		$this->execute("INSERT INTO AuthItem (name, type, description, bizrule, data) VALUES ('editor', 2, '', NULL, 'N;')");
		$this->execute("INSERT INTO AuthItemChild (parent, child) VALUES ('editor', 'accessBackend')");

		$this->execute('UPDATE AuthItemChild SET child="editor" WHERE parent="admin" AND child="accessBackend"');

		return true;
	}

	public function down()
	{
		$this->execute('UPDATE AuthItemChild SET child="accessBackend" WHERE parent="admin" AND child="editor"');

		$this->execute('DELETE FROM AuthItemChild WHERE parent="editor"');
		$this->execute('DELETE FROM AuthItem WHERE name="editor"');

		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}