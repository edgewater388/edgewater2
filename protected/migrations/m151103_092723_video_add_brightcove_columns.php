<?php

class m151103_092723_video_add_brightcove_columns extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE video ADD COLUMN guid VARCHAR(32) DEFAULT NULL AFTER image_fid');
		$this->execute('ALTER TABLE video ADD COLUMN data TEXT DEFAULT NULL AFTER guid');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE video DROP COLUMN data');
		$this->execute('ALTER TABLE video DROP COLUMN guid');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}