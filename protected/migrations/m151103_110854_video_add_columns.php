<?php

class m151103_110854_video_add_columns extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE video ADD COLUMN length VARCHAR(16) DEFAULT NULL AFTER image_fid');
		$this->execute('ALTER TABLE video ADD COLUMN instructor_id INT(10) unsigned DEFAULT NULL AFTER image_fid');
		$this->execute('ALTER TABLE video ADD CONSTRAINT `video_ibfk_2` FOREIGN KEY (`instructor_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE');
		$this->execute('ALTER TABLE video ADD COLUMN type VARCHAR(32) DEFAULT NULL AFTER instructor_id');
		$this->execute('ALTER TABLE video ADD COLUMN level TINYINT(1) unsigned DEFAULT NULL AFTER type');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE video DROP COLUMN level');
		$this->execute('ALTER TABLE video DROP COLUMN type');
		$this->execute('ALTER TABLE video DROP FOREIGN KEY  `video_ibfk_2`');
		$this->execute('ALTER TABLE video DROP COLUMN instructor_id');
		$this->execute('ALTER TABLE video DROP COLUMN length');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}