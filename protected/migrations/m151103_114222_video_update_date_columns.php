<?php

class m151103_114222_video_update_date_columns extends CDbMigration
{
	public function up()
	{
		$this->execute('UPDATE video SET created = NULL, updated = NULL');
		$this->execute('ALTER TABLE video CHANGE created created DATETIME DEFAULT NULL');
		$this->execute('ALTER TABLE video CHANGE updated updated DATETIME DEFAULT NULL');
		return true;
	}

	public function down()
	{
		$this->execute('UPDATE video SET created = NULL, updated = NULL');
		$this->execute('ALTER TABLE video CHANGE updated updated INT(10) unsigned DEFAULT NULL');
		$this->execute('ALTER TABLE video CHANGE created created INT(10) unsigned DEFAULT NULL');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}