<?php

class m151103_120919_users_add_instructor_role extends CDbMigration
{
	public function up()
	{
		$this->execute("INSERT INTO AuthItem (name, type, description, bizrule, data) VALUES ('instructor', 2, '', NULL, 'N;')");
		return true;
	}

	public function down()
	{
		$this->execute('DELETE FROM AuthItem WHERE name = "instructor"');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}