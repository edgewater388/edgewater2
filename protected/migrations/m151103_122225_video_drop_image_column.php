<?php

class m151103_122225_video_drop_image_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE video DROP FOREIGN KEY video_ibfk_1');
		$this->execute('ALTER TABLE video DROP COLUMN image_fid');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE video ADD COLUMN image_fid INT(10) unsigned DEFAULT NULL AFTER description');
		$this->execute('ALTER TABLE video ADD CONSTRAINT `video_ibfk_1` FOREIGN KEY (`image_fid`) REFERENCES `files` (`id`) ON DELETE SET NULL ON UPDATE CASCADE');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}