<?php

class m151103_130038_users_fix_date_columns extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE users CHANGE created created int(10) unsigned DEFAULT NULL');
		$this->execute('ALTER TABLE users CHANGE updated updated int(10) unsigned DEFAULT NULL');
		$this->execute('UPDATE users SET created = NULL, updated = NULL');
		$this->execute('ALTER TABLE users CHANGE created created DATETIME DEFAULT NULL');
		$this->execute('ALTER TABLE users CHANGE updated updated DATETIME DEFAULT NULL');
		return true;
	}

	public function down()
	{
		$this->execute('UPDATE users SET created = NULL, updated = NULL');
		$this->execute('ALTER TABLE users CHANGE updated updated INT(10) unsigned DEFAULT NULL');
		$this->execute('ALTER TABLE users CHANGE created created INT(10) unsigned DEFAULT NULL');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}