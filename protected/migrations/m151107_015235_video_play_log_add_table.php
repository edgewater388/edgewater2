<?php

class m151107_015235_video_play_log_add_table extends CDbMigration
{
	public function up()
	{
		$this->execute("CREATE TABLE `video_play_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `video_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `video_play_log_ibfk_1` (`user_id`),
  KEY `video_play_log_ibfk_2` (`video_id`),
  CONSTRAINT `video_play_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `video_play_log_ibfk_2` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		return true;
	}

	public function down()
	{
		$this->execute('DROP TABLE video_play_log');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}