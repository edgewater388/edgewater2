<?php

class m151108_115933_membership_add_table extends CDbMigration
{
	public function up()
	{
		$this->execute("CREATE TABLE `membership` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `plan_id` int(10) unsigned DEFAULT NULL,
  `subtotal` decimal(16,2) DEFAULT NULL,
  `tax` decimal(16,2) DEFAULT NULL,
  `total` decimal(16,2) DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  `valid_from` date DEFAULT NULL,
  `valid_to` date DEFAULT NULL,
  `prev_billing_date` date DEFAULT NULL,
  `next_billing_date` date DEFAULT NULL,
  `cancelled_date` date DEFAULT NULL,
  `interval` varchar(8) DEFAULT NULL,
  `auto_renew` tinyint(1) unsigned DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `membership_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		$this->execute("CREATE TABLE `membership_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `data` text DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `membership_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		$this->execute("CREATE TABLE `customer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `service` varchar(32) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `customer_profile_id` varchar(255) DEFAULT NULL,
  `customer_payment_profile_id` varchar(255) DEFAULT NULL,
  `card_number` varchar(4) DEFAULT NULL,
  `card_expiry` varchar(4) DEFAULT NULL,
  `card_type` varchar(32) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `street` varchar(30) DEFAULT NULL,
  `street1` varchar(30) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		$this->execute("CREATE TABLE `transaction` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(32) DEFAULT NULL,
  `payment_method` varchar(32) DEFAULT NULL,
  `customer_ref_num` varchar(22) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `street` varchar(30) DEFAULT NULL,
  `street1` varchar(30) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(14) DEFAULT NULL,
  `subtotal` decimal(16,2) DEFAULT NULL,
  `tax` decimal(16,2) DEFAULT NULL,
  `total` decimal(16,2) DEFAULT NULL,
  `currency` varchar(6) DEFAULT NULL,
  `card_number` varchar(4) DEFAULT NULL,
  `card_expiry` varchar(4) DEFAULT NULL,
  `card_type` varchar(32) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `membership_period` varchar(32) DEFAULT NULL,
  `service` varchar(16) DEFAULT NULL,
  `approved` tinyint(1) unsigned DEFAULT NULL,
  `trans_ref` varchar(255) DEFAULT NULL,
  `message` text,
  `host` varchar(255) DEFAULT NULL,
  `data` text,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		$this->execute("CREATE TABLE `transaction_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int(10) unsigned DEFAULT NULL,
  `service` varchar(16) DEFAULT NULL,
  `request` text,
  `response` text,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `transaction_log_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		return true;
	}

	public function down()
	{
		$this->execute('DROP TABLE transaction_log');
		$this->execute('DROP TABLE transaction');
		$this->execute('DROP TABLE customer');
		$this->execute('DROP TABLE membership_log');
		$this->execute('DROP TABLE membership');

		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}