<?php

class m151120_112431_playlist_add_columns extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE playlist ADD COLUMN type VARCHAR(32) DEFAULT NULL AFTER length');
		$this->execute('ALTER TABLE playlist ADD COLUMN level TINYINT(1) unsigned DEFAULT NULL AFTER type');
		$this->execute('ALTER TABLE playlist ADD COLUMN is_public TINYINT(1) unsigned DEFAULT NULL AFTER level');
		$this->execute('ALTER TABLE playlist ADD COLUMN is_featured TINYINT(1) unsigned DEFAULT NULL AFTER is_public');
		$this->execute('ALTER TABLE playlist ADD COLUMN weight TINYINT(3) signed DEFAULT NULL AFTER is_featured');

		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE playlist DROP COLUMN weight');
		$this->execute('ALTER TABLE playlist DROP COLUMN is_featured');
		$this->execute('ALTER TABLE playlist DROP COLUMN is_public');
		$this->execute('ALTER TABLE playlist DROP COLUMN level');
		$this->execute('ALTER TABLE playlist DROP COLUMN type');

		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}