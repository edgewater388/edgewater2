<?php

class m151121_103822_video_add_duration_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE video ADD COLUMN duration TINYINT(3) unsigned DEFAULT NULL AFTER length');
		$this->execute('ALTER TABLE video ADD COLUMN time VARCHAR(10) DEFAULT NULL AFTER length');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE video DROP COLUMN time');
		$this->execute('ALTER TABLE video DROP COLUMN duration');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}