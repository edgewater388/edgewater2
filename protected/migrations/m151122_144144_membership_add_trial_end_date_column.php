<?php

class m151122_144144_membership_add_trial_end_date_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE membership ADD COLUMN trial_end_date DATE DEFAULT NULL AFTER join_date');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE membership DROP COLUMN trial_end_date');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}