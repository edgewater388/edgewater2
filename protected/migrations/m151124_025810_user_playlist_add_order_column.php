<?php

class m151124_025810_user_playlist_add_order_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE user_playlist ADD COLUMN `order` text DEFAULT NULL');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE user_playlist DROP COLUMN `order`');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}