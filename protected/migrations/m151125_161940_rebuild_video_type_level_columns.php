<?php

class m151125_161940_rebuild_video_type_level_columns extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE video DROP COLUMN type');
		$this->execute('ALTER TABLE video DROP COLUMN level');
		$this->execute('ALTER TABLE video ADD COLUMN strength TINYINT(1) unsigned DEFAULT NULL AFTER instructor_id');
		$this->execute('ALTER TABLE video ADD COLUMN cardio TINYINT(1) unsigned DEFAULT NULL AFTER strength');
		$this->execute('ALTER TABLE video ADD COLUMN restorative TINYINT(1) unsigned DEFAULT NULL AFTER cardio');

		return true;
	}

	public function down()
	{		
		$this->execute('ALTER TABLE video DROP COLUMN restorative');
		$this->execute('ALTER TABLE video DROP COLUMN cardio');
		$this->execute('ALTER TABLE video DROP COLUMN strength');
		$this->execute('ALTER TABLE video ADD COLUMN `type` varchar(32) DEFAULT NULL AFTER instructor_id');
		$this->execute('ALTER TABLE video ADD COLUMN `level` tinyint(1) unsigned DEFAULT NULL AFTER type');
		
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}