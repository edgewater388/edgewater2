<?php

class m151125_170447_rebuild_playlist_type_level_columns extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE playlist DROP COLUMN type');
		$this->execute('ALTER TABLE playlist DROP COLUMN level');
		$this->execute('ALTER TABLE playlist ADD COLUMN strength TINYINT(1) unsigned DEFAULT NULL AFTER length');
		$this->execute('ALTER TABLE playlist ADD COLUMN cardio TINYINT(1) unsigned DEFAULT NULL AFTER strength');
		$this->execute('ALTER TABLE playlist ADD COLUMN restorative TINYINT(1) unsigned DEFAULT NULL AFTER cardio');

		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE playlist DROP COLUMN restorative');
		$this->execute('ALTER TABLE playlist DROP COLUMN cardio');
		$this->execute('ALTER TABLE playlist DROP COLUMN strength');
		$this->execute('ALTER TABLE playlist ADD COLUMN `type` varchar(32) DEFAULT NULL AFTER length');
		$this->execute('ALTER TABLE playlist ADD COLUMN `level` tinyint(1) unsigned DEFAULT NULL AFTER type');
		
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}