<?php

class m151126_110351_membership_add_trial_reminder_date_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE membership ADD COLUMN trial_reminder_sent_date DATE DEFAULT NULL AFTER trial_end_date');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE membership DROP COLUMN trial_reminder_sent_date');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}