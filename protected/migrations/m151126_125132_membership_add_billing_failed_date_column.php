<?php

class m151126_125132_membership_add_billing_failed_date_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE membership ADD COLUMN billing_failed_date DATE DEFAULT NULL AFTER next_billing_date');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE membership DROP COLUMN billing_failed_date');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}