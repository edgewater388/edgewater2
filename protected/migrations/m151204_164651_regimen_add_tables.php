<?php

class m151204_164651_regimen_add_tables extends CDbMigration
{
	public function up()
	{
		$this->execute("CREATE TABLE `regimen` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		$this->execute("CREATE TABLE `regimen_video` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `regimen_id` int(10) unsigned DEFAULT NULL,
  `video_id` int(10) unsigned DEFAULT NULL,
  `day` tinyint(3) unsigned DEFAULT NULL,
  `weight` tinyint(3) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `regimen_id` (`regimen_id`),
  KEY `video_id` (`video_id`),
  CONSTRAINT `regimen_video_ibfk_2` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `regimen_video_ibfk_1` FOREIGN KEY (`regimen_id`) REFERENCES `regimen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		return true;
	}

	public function down()
	{
		$this->execute('DROP TABLE regimen_video');
		$this->execute('DROP TABLE regimen');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}