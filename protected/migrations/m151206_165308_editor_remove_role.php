<?php

class m151206_165308_editor_remove_role extends CDbMigration
{
	public function up()
	{
		$this->execute('UPDATE AuthItemChild SET child="accessBackend" WHERE parent="admin" AND child="editor"');

		$this->execute('DELETE FROM AuthItemChild WHERE parent="editor"');
		$this->execute('DELETE FROM AuthItem WHERE name="editor"');

		return true;
	}

	public function down()
	{
		$this->execute("INSERT INTO AuthItem (name, type, description, bizrule, data) VALUES ('editor', 2, '', NULL, 'N;')");
		$this->execute("INSERT INTO AuthItemChild (parent, child) VALUES ('editor', 'accessBackend')");

		$this->execute('UPDATE AuthItemChild SET child="editor" WHERE parent="admin" AND child="accessBackend"');
	
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}