<?php

class m151210_202737_video_update_length_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE video CHANGE length length int(10) unsigned DEFAULT NULL');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE video CHANGE length length VARCHAR(16) DEFAULT NULL');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}