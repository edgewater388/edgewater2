<?php

class m151212_004803_regimen_add_summary_column extends CDbMigration
{
	public function up()
	{
		$this->execute("ALTER TABLE regimen ADD COLUMN summary TEXT DEFAULT NULL AFTER description");
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE regimen DROP COLUMN summary');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}