<?php

class m160117_045632_fix_users_email_unique_index extends CDbMigration
{
	public function up()
	{
		$this->execute('DROP INDEX email ON users');
		$this->execute('CREATE INDEX email ON users (email)');
		
		return true;
	}

	public function down()
	{
		$this->execute('DROP INDEX email ON users');
		// don't add the unique index back.. it could fail if there are duplicate emails
		$this->execute('CREATE INDEX email ON users (email)');

		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}