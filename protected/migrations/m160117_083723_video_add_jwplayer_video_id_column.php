<?php

class m160117_083723_video_add_jwplayer_video_id_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE video ADD COLUMN jwplayer_video_id VARCHAR(64) DEFAULT NULL AFTER guid');

		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE video DROP COLUMN jwplayer_video_id');

		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}