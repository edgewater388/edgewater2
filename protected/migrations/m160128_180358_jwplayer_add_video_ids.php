<?php

class m160128_180358_jwplayer_add_video_ids extends CDbMigration
{
	public function up()
	{
		$videos = array(
			'4636325880001'=>'CBDTnGSL',
			'4605438770001'=>'iqZB78Xi',
			'4605438772001'=>'YeRUG67v',
			'4605438765001'=>'K2OVWYJv',
			'4605438767001'=>'7Pm4nB99',
			'4605438768001'=>'KLCke0tx',
			'4605438766001'=>'jOW7YFpR',
			'4605438769001'=>'Dz6grtcz',
			'4646485621001'=>'NOAYjF4z',
			'4635082807001'=>'PVOzuzke',
			'4647566912001'=>'jusv9Vy4',
			'4635082808001'=>'d0JVsBTX',
			'4646940150001'=>'HmQQ49PR',
			'4646485631001'=>'MReEePFG',
			'4646485622001'=>'cBrCx8n5',
			'4646485629001'=>'CS5YkSf9',
			'4646485625001'=>'NVBDE13i',
			'4646485623001'=>'60EUAmzj',
			'4646940151001'=>'yP12GM4t',
			'4646485624001'=>'fR4B9Nqi',
			'4646940148001'=>'CzeoB31M',
			'4646940155001'=>'5msbxWlG',
			'4646940153001'=>'FmE2IGzq',
			'4646485628001'=>'mJ8vLeuB',
			'4646940156001'=>'xTcI4Gp7',
			'4646485632001'=>'IFydtFb4',
			'4646485630001'=>'HHpBtJnR',
			'4646940149001'=>'muLxn5d8',
			'4646485626001'=>'S7EiWlRv',
			'4646485627001'=>'mjTZIhnW',
			'4647928525001'=>'VcMHJ318',
			'4647566911001'=>'aWE6iewU',
			'4647570247001'=>'rgdA8NcT',
			'4646940154001'=>'FufDZ1f4',
			'4647928526001'=>'ODM7t45p',
			'4647566913001'=>'Vds3uzRj',
			'4646940152001'=>'p4Ob6YSP',
			'4647566910001'=>'zxzWB2b1',
			'4635082801001'=>'BbvU2atD',
			'4635082805001'=>'aBM1hYFj',
		);

		foreach ($videos as $guid=>$jwplayer_video_id)
			$this->execute('UPDATE video SET jwplayer_video_id = "'.$jwplayer_video_id.'" WHERE guid = '.$guid);

		return true;
	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}