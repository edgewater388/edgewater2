<?php

class m160201_185608_regimen_add_weight_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE regimen ADD COLUMN weight TINYINT(3) DEFAULT NULL AFTER description');
		$this->execute('UPDATE regimen SET weight = id');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE regimen DROP COLUMN weight');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}