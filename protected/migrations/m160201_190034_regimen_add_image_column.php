<?php

class m160201_190034_regimen_add_image_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE regimen ADD COLUMN image_fid INT(10) unsigned DEFAULT NULL AFTER description');
		$this->execute('ALTER TABLE regimen ADD CONSTRAINT `regimen_ibfk_1` FOREIGN KEY (`image_fid`) REFERENCES `files` (`id`) ON DELETE SET NULL ON UPDATE CASCADE');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE regimen DROP FOREIGN KEY regimen_ibfk_1');
		$this->execute('ALTER TABLE regimen DROP COLUMN image_fid');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
