<?php

class m160223_004402_video_add_is_public_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE video ADD COLUMN is_public TINYINT(1) UNSIGNED DEFAULT NULL AFTER jwplayer_video_id');
		$this->execute('UPDATE video SET is_public = 0');

		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE video DROP COLUMN is_public');

		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}