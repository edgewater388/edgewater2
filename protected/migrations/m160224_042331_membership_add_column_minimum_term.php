<?php

class m160224_042331_membership_add_column_minimum_term extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE membership ADD COLUMN min_term TINYINT(3) UNSIGNED DEFAULT NULL AFTER `interval`');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE membership DROP COLUMN min_term');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}