<?php

class m160229_182036_membership_add_plan_renewal_count_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE membership ADD COLUMN plan_renewal_count TINYINT(1) UNSIGNED DEFAULT NULL AFTER min_term');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE membership DROP COLUMN plan_renewal_count');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}