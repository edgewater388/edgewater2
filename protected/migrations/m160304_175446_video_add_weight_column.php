<?php

class m160304_175446_video_add_weight_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE video ADD COLUMN weight TINYINT(3) DEFAULT NULL AFTER is_public');
		$this->execute('UPDATE video SET weight = 0');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE video DROP COLUMN weight');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}