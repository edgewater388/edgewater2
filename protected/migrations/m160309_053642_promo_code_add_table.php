<?php

class m160309_053642_promo_code_add_table extends CDbMigration
{
	public function up()
	{
		$this->execute("CREATE TABLE `promo_code` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `description` text,
  `discount_type` varchar(32) DEFAULT NULL,
  `discount_amount` decimal(16,4) DEFAULT NULL,
  `discount_length` int(11) DEFAULT NULL,
  `discount_interval` varchar(8) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `quantity` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		return true;
	}

	public function down()
	{
		$this->execute('DROP TABLE promo_code');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}