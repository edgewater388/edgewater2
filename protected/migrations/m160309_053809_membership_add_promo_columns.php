<?php

class m160309_053809_membership_add_promo_columns extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE membership ADD COLUMN promo_code VARCHAR(32) DEFAULT NULL AFTER total');
		$this->execute('ALTER TABLE membership ADD COLUMN promo_code_id INT(10) UNSIGNED DEFAULT NULL AFTER promo_code');
		$this->execute('ALTER TABLE membership ADD COLUMN promo_amount DECIMAL(16,2) DEFAULT NULL AFTER promo_code_id');
		$this->execute('ALTER TABLE membership ADD COLUMN promo_end_date DATE DEFAULT NULL AFTER promo_amount');
		$this->execute('ALTER TABLE membership ADD CONSTRAINT `membership_ibfk_2` FOREIGN KEY (`promo_code_id`) REFERENCES `promo_code` (`id`) ON DELETE SET NULL ON UPDATE CASCADE');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE membership DROP FOREIGN KEY membership_ibfk_2');
		$this->execute('ALTER TABLE membership DROP COLUMN promo_end_date');
		$this->execute('ALTER TABLE membership DROP COLUMN promo_amount');
		$this->execute('ALTER TABLE membership DROP COLUMN promo_code_id');
		$this->execute('ALTER TABLE membership DROP COLUMN promo_code');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}