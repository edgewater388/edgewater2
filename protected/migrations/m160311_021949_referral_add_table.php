<?php

class m160311_021949_referral_add_table extends CDbMigration
{
	public function up()
	{
		$this->execute("CREATE TABLE `referral_invite` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `referral_invite_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		$this->execute("CREATE TABLE `referral_bank` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `referred_user_id` int(10) unsigned DEFAULT NULL,
  `referral_invite_id` int(10) unsigned DEFAULT NULL,
  `amount` decimal(8,2) DEFAULT NULL,
  `released` date DEFAULT NULL,
  `redeemed` date DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `referred_user_id` (`referred_user_id`),
  KEY `referral_invite_id` (`referral_invite_id`),
  CONSTRAINT `referral_bank_ibfk_3` FOREIGN KEY (`referral_invite_id`) REFERENCES `referral_invite` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `referral_bank_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `referral_bank_ibfk_2` FOREIGN KEY (`referred_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		return true;
	}

	public function down()
	{
		$this->execute('DROP TABLE referral_bank');
		$this->execute('DROP TABLE referral_invite');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}