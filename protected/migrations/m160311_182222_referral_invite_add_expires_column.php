<?php

class m160311_182222_referral_invite_add_expires_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE referral_invite ADD COLUMN expires DATE DEFAULT NULL AFTER email');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE referral_invite DROP COLUMN expires');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}