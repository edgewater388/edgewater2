<?php

class m160318_195223_promo_code_redemption_log_add_table extends CDbMigration
{
	public function up()
	{
		$this->execute("CREATE TABLE `promo_code_redemption_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `promo_code_id` int(10) unsigned DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `data` text,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `promo_code_id` (`promo_code_id`),
  CONSTRAINT `promo_code_redemption_log_ibfk_2` FOREIGN KEY (`promo_code_id`) REFERENCES `promo_code` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `promo_code_redemption_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		return true;
	}

	public function down()
	{
		$this->execute('DROP TABLE promo_code_redemption_log');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}