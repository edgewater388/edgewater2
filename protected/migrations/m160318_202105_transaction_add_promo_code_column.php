<?php

class m160318_202105_transaction_add_promo_code_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE transaction ADD COLUMN promo_code VARCHAR(32) DEFAULT NULL AFTER customer_ref_num');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE transaction DROP COLUMN promo_code');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}