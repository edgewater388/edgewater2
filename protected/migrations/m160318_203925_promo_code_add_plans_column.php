<?php

class m160318_203925_promo_code_add_plans_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE promo_code ADD COLUMN plans TEXT DEFAULT NULL AFTER quantity');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE promo_code DROP COLUMN plans');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}