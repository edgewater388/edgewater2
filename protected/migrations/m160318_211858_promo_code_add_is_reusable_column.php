<?php

class m160318_211858_promo_code_add_is_reusable_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE promo_code ADD COLUMN is_reusable TINYINT(1) UNSIGNED DEFAULT NULL AFTER quantity');
		$this->execute('UPDATE promo_code SET is_reusable = 0');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE promo_code DROP COLUMN is_reusable');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}