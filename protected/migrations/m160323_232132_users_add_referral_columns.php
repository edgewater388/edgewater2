<?php

class m160323_232132_users_add_referral_columns extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE users ADD COLUMN referral_code VARCHAR(16) DEFAULT NULL AFTER reg_vendor');
		$this->execute('ALTER TABLE users ADD COLUMN referred_by VARCHAR(16) DEFAULT NULL AFTER referral_code');

		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE users DROP COLUMN referred_by');
		$this->execute('ALTER TABLE users DROP COLUMN referral_code');
		
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}