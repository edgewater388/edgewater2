<?php

class m160407_230223_transaction_fix_period_date_md_order extends CDbMigration
{
	public function up()
	{
		$dataReader = Yii::app()->db->createCommand('SELECT id, membership_period FROM transaction WHERE membership_period IS NOT NULL')->query();

		while(($row=$dataReader->read()) !== false )
		{
			$period = $row['membership_period'];
			$day = substr($period, 0, 2);
			$month = substr($period, 3, 2);
			$period = $month.'/'.$day.substr($period, 5);
			$this->execute('UPDATE transaction SET membership_period = "'.$period.'" WHERE id = '.$row['id']);
		}

		return true;
	}

	public function down()
	{
		$dataReader = Yii::app()->db->createCommand('SELECT id, membership_period FROM transaction WHERE membership_period IS NOT NULL')->query();

		while(($row=$dataReader->read()) !== false )
		{
			$period = $row['membership_period'];
			$day = substr($period, 0, 2);
			$month = substr($period, 3, 2);
			$period = $month.'/'.$day.substr($period, 5);
			$this->execute('UPDATE transaction SET membership_period = "'.$period.'" WHERE id = '.$row['id']);
		}

		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}