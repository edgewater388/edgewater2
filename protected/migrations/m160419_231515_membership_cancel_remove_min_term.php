<?php

class m160419_231515_membership_cancel_remove_min_term extends CDbMigration
{
	public function up()
	{
		$this->execute('UPDATE membership SET min_term = NULL WHERE cancelled_date IS NOT NULL');
		return true;
	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}