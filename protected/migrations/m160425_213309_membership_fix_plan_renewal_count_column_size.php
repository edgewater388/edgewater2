<?php

class m160425_213309_membership_fix_plan_renewal_count_column_size extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE membership MODIFY plan_renewal_count TINYINT(3) UNSIGNED DEFAULT NULL');
		return true;
	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}