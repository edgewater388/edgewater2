<?php

class m160630_223522_membership_add_billing_failed_count_column extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE membership ADD COLUMN billing_failed_count TINYINT(1) UNSIGNED DEFAULT NULL AFTER billing_failed_date');
		return true;
	}

	public function down()
	{
		$this->execute('ALTER TABLE membership DROP COLUMN billing_failed_count');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}