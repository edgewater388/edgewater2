<?php

class m161121_144234_modify_profile_add_health_fields extends CDbMigration
{
	public function up()
	{
		$this->addColumn('profile', 'weight', 'int(11) UNSIGNED');
		$this->addColumn('profile', 'height', 'int(11) UNSIGNED');
		$this->addColumn('profile', 'fitness_level', 'int(11) UNSIGNED');
	}

	public function down()
	{
		$this->dropColumn('profile', 'weight');
		$this->dropColumn('profile', 'height');
		$this->dropColumn('profile', 'fintess_level');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}