<?php

class m161126_141554_create_regimen_meal_plan_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('regimen_meal_plan', array(
			'id' => 'pk',
			'regimen_id' => 'int(11) NOT NULL',
			'week_num' => 'int(11) NOT NULL',
			'path_to_file' => 'varchar(255) NOT NULL',
			'status' => 'TINYINT DEFAULT 0 NOT NULL',
			'created' => 'DATETIME',
			'updated' => 'DATETIME',
			'deleted' => 'DATETIME',
		));
	}

	public function down()
	{
		$this->dropTable('regimen_meal_plan');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}