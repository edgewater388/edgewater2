<?php

class m161126_170558_create_users_to_regimens extends CDbMigration
{
	public function up()
	{
		$this->createTable('users_to_regimens', array(
			'id' => 'pk',
			'user_id' => 'int(11) NOT NULL',
			'regimen_id' => 'int(11) NOT NULL',
			'created' => 'DATETIME',
			'updated' => 'DATETIME',
			'deleted' => 'DATETIME',
		));
	}

	public function down()
	{
		$this->dropTable('users_to_regimens');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}