<?php

class m161126_170616_create_user_regimen_plan extends CDbMigration
{
	public function up()
	{
		$this->createTable('user_regimen_plan', array(
			'id' => 'pk',
			'u2r_id' => 'int(11) NOT NULL',
			'video_id' => 'int(11) NOT NULL',
			'is_watched' => 'TINYINT(1) DEFAULT 0 NOT NULL',
			'day' => 'int(11) NOT NULL',
			'date' => 'DATE',
			'created' => 'DATETIME',
			'updated' => 'DATETIME',
			'deleted' => 'DATETIME',
		));
	}

	public function down()
	{
		$this->dropTable('user_regimen_plan');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}