<?php

class m161126_173554_update_profile_add_old_weight extends CDbMigration
{
	public function up()
	{
		$this->addColumn('profile', 'old_weight', 'int(11) UNSIGNED');
	}

	public function down()
	{
		$this->dropColumn('profile', 'old_weight');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}