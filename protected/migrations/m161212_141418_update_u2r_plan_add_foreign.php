<?php

class m161212_141418_update_u2r_plan_add_foreign extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey('FK_u2r_plan', 'user_regimen_plan', 'u2r_id', 'users_to_regimens', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		$this->dropForeignKey('FK_u2r_plan', 'user_regimen_plan');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}