<?php

class m161212_163544_update_profile_modify_height_data_type extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('profile', 'height', 'VARCHAR(255)');
	}

	public function down()
	{
		$this->alterColumn('profile', 'height', 'int(11) UNSIGNED');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}