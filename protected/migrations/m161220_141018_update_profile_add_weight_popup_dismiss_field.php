<?php

class m161220_141018_update_profile_add_weight_popup_dismiss_field extends CDbMigration
{
	public function up()
	{
		$this->addColumn('profile', 'do_not_show_weight_popup', 'TINYINT(1) DEFAULT 0 NOT NULL');
	}

	public function down()
	{
		$this->dropColumn('profile', 'do_not_show_weight_popup');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}