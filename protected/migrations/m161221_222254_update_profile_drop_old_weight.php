<?php

class m161221_222254_update_profile_drop_old_weight extends CDbMigration
{
	public function up()
	{
		$this->dropColumn('profile', 'old_weight');
	}

	public function down()
	{
		$this->addColumn('profile', 'old_weight', 'int(11) UNSIGNED');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}