<?php

class m161221_222454_update_users_to_regimens_add_old_weight extends CDbMigration
{
	public function up()
	{
		$this->addColumn('users_to_regimens', 'old_weight', 'int(11) UNSIGNED');
	}

	public function down()
	{
		$this->dropColumn('users_to_regimens', 'old_weight');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}