<?php

class m170324_141608_create_stream_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('stream', [
			'id'                   => 'pk',
			'time_start'           => 'DATETIME',
			'time_end'             => 'DATETIME',
			'embed_js'             => 'TEXT',
			'countdown_message'    => 'TEXT',
			'ready_message'        => 'TEXT',
			'countdown_background' => 'VARCHAR(255)',
			'ready_background'     => 'VARCHAR(255)',
			'is_live'              => 'TINYINT(1) unsigned DEFAULT 0 NOT NULL',
			'status'               => 'TINYINT(1) unsigned DEFAULT 0 NOT NULL',
		]);
	}

	public function down()
	{
		$this->dropTable('stream');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}