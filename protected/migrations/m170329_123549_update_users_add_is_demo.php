<?php

class m170329_123549_update_users_add_is_demo extends CDbMigration
{
	public function up()
	{
		$this->addColumn('users', 'is_demo', 'TINYINT(1) DEFAULT 0 NOT NULL');
	}

	public function down()
	{
		$this->dropColumn('users', 'is_demo');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}