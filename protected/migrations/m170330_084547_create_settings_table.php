<?php

class m170330_084547_create_settings_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('settings', [
			'id'      => 'pk',
			'setting' => 'VARCHAR(255)',
			'value'   => 'VARCHAR(255)',
			'status'  => 'TINYINT(1) DEFAULT 0 NOT NULL',
		]);
	}

	public function down()
	{
		$this->dropTable('settings');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}