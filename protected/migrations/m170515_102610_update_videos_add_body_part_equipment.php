<?php

class m170515_102610_update_videos_add_body_part_equipment extends CDbMigration
{
	public function up()
	{
		$this->addColumn('video', 'body_part', 'VARCHAR(255)');
		$this->addColumn('video', 'equipment', 'VARCHAR(255)');
	}

	public function down()
	{
		$this->dropColumn('video', 'body_part');
		$this->dropColumn('video', 'equipment');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}