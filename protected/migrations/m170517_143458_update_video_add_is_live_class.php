<?php

class m170517_143458_update_video_add_is_live_class extends CDbMigration
{
	public function up()
	{
		$this->addColumn('video', 'is_live_class', 'TINYINT(1) DEFAULT 0 NOT NULL');
	}

	public function down()
	{
		$this->dropColumn('video', 'is_live_class');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}