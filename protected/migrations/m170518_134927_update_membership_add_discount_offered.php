<?php

class m170518_134927_update_membership_add_discount_offered extends CDbMigration
{
	public function up()
	{
		$this->addColumn('membership', 'discount_offered', 'TINYINT(1) DEFAULT 0 NOT NULL');
	}

	public function down()
	{
		$this->dropColumn('membership', 'discount_offered');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}