<?php

class m170524_143754_update_users_add_google_id extends CDbMigration
{
	public function up()
	{
		$this->addColumn('users', 'google_id', 'VARCHAR(255) AFTER `facebook_id`');
	}

	public function down()
	{
		$this->dropColumn('users', 'google_id');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}