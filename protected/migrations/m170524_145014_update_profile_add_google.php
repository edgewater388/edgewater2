<?php

class m170524_145014_update_profile_add_google extends CDbMigration
{
	public function up()
	{
		$this->addColumn('profile', 'google_plus', 'VARCHAR(64) AFTER `facebook`');
	}

	public function down()
	{
		$this->dropColumn('profile', 'google_plus');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}