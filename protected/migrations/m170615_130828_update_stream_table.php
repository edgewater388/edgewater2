<?php

class m170615_130828_update_stream_table extends CDbMigration
{
	public function up()
	{
		$this->addColumn('stream', 'livestream_iframe', 'TEXT');
	}

	public function down()
	{
		$this->dropColumn('stream', 'livestream_iframe');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}