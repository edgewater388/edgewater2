<?php

class m170630_094234_add_columns_price_and_price_color_tbl_product extends CDbMigration
{
	public function up()
	{
		$this->addColumn('product', 'price', 'string');
		$this->addColumn('product', 'price_color', 'string');
	}

	public function down()
	{
		$this->dropColumn('product', 'price');
		$this->dropColumn('product', 'price_color');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}