<?php

class m170720_124009_add_timer_tbl_stream extends CDbMigration
{
	public function up()
	{
		$this->addColumn('stream', 'timer', 'SMALLINT UNSIGNED');
	}

	public function down()
	{
		$this->dropColumn('stream', 'timer');
	}

}