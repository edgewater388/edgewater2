<?php

class m170720_154830_add_boolean_is_new_tbl_video extends CDbMigration
{
	public function up()
	{
		$this->addColumn('video', 'is_new', 'boolean');
	}

	public function down()
	{
		$this->dropColumn('video', 'is_new');
	}

}