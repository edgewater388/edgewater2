<?php

class m170726_103503_alter_user_add_column_is_see_video extends CDbMigration
{
	public function up()
	{
		$this->addColumn('users', 'is_see_video', 'boolean DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('users', 'is_see_video');
	}
}