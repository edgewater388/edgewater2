<?php

class m170911_142318_create_plan_table extends CDbMigration
{
	public function up()
	{
		date_default_timezone_set('America/New_York');
		$description_for_special_plan = <<<HERE
					<ul class="features">
                        <li><span class="icon-checkmark"></span><span class="desc">Unlimited viewing of over 100 videos include Physique LIVE</span></li>
                        <li><span class="icon-checkmark"></span><span class="desc">4 week workout calendar</span></li>
                        <li><span class="icon-checkmark"></span><span class="desc">Exclusive access to co-founder Tanya Becker & instructors</span></li>
                        <li><span class="icon-checkmark"></span><span class="desc">Custom workouts with our mashup creator</span></li>
                    </ul>
                    <p class="bold uppercase">Free Physique 57 Workout Ball</p>
                    <p class="bonus bold uppercase">AVAILABLE ONLY SEPTEMBER 5TH - SEPTEMBER 30TH</p>
HERE;
		$existingPlans = [
			// common plans
			[
				'title'       => 'Monthly',
				'description' => null,
				'interval'    => 'monthly',
				'auto_renew'  => true,
				'amount'      => 57,
				'old_amount'  => 57,
			],
			[
				'title'       => 'Daily Rate',
				'description' => 'Drop in Membership',
				'interval'    => 'daily',
				'auto_renew'  => false,
				'amount'      => 5,
				'old_amount'  => 5,
			],
			[
				'title'       => '$30 Monthly',
				'description' => 'With a 3 month commitment',
				'interval'    => 'monthly',
				'auto_renew'  => true,
				'amount'      => 30,
				'old_amount'  => 30,
				'min_term'    => 3,
			],
			[
				'title'       => '$29.95 Monthly',
				'description' => 'Cancel Anytime',
				'interval'    => 'monthly',
				'auto_renew'  => 1,
				'amount'      => 29.95,
				'old_amount'  => 57,
				'is_default' => true,
				'is_main_default' => true
			],
			[
				'title'       => '$19.95 Monthly',
				'description' => 'With a 3 month commitment',
				'interval'    => 'monthly',
				'auto_renew'  => true,
				'amount'      => 19.95,
				'old_amount'  => 30,
				'min_term'    => 3,
				'is_default' => true
			],
			// special offer plan
			[
				'title'            => 'UNLIMITED ONLINE WORKOUTS FOR $20 ($29.95 value)',
				'description'      => $description_for_special_plan,		
				'interval'         => 'monthly',
				'auto_renew'       => true,
				'amount'           => 20,
				// promo start/end dates are inclusive
				'promo_start_date' => '2017-09-05',
				'promo_end_date'   => '2017-09-30',
				'is_special' => true
			]
		];
		
		$this->createTable('plan', [
			'id' => 'pk',
			'title' => 'string',
			'description' => 'text',
			'interval' => 'string',
			'amount' => 'double',
			'old_amount' => 'double',
			'min_term' => 'int(11)',
			'promo_start_date' => 'date',
			'promo_end_date' => 'date',
			'slug' => 'string',
			'image_fid' => 'int(11) unsigned',
			'is_special' => 'boolean',
			'is_auto_renew' => 'boolean',
			'is_default' => 'boolean',
			'is_main_default' => 'boolean',
			'status' => 'boolean',
			'created' => 'datetime',
			'updated' => 'datetime'
		]);
		$this->addForeignKey('plan_ibfk_1', 'plan', 'image_fid', 'files', 'id', 'SET NULL', 'CASCADE');
		
		foreach ($existingPlans as $key => $plan) {
			$this->insert('plan', [
				'title'            => $plan['title'],
				'description'      => $plan['description'],
				'interval'         => $plan['interval'],
				'is_auto_renew'    => $plan['auto_renew'],
				'amount'           => $plan['amount'],
				'old_amount'       => isset($plan['old_amount']) ? $plan['old_amount'] : null,
				'min_term'         => isset($plan['min_term']) ? $plan['min_term'] : null,
				// promo start/end dates are inclusive
				'promo_start_date' => isset($plan['promo_start_date']) ? $plan['promo_start_date'] : false,
				'promo_end_date'   => isset($plan['promo_end_date']) ? $plan['promo_end_date'] : false,
				'is_special'       => isset($plan['is_special']) ? $plan['is_special'] : false,
				'is_default'       => isset($plan['is_default']) ? $plan['is_default'] : false,
				'is_main_default'  => isset($plan['is_main_default']) ? $plan['is_main_default'] : false,
				'status' => true,
				'slug' => $key,
				'created' => date("Y-m-d H:i:s"),
				'updated' => date("Y-m-d H:i:s")
			]);
		}
	}

	public function down()
	{
		$this->dropTable('plan');
	}
}
