<?php

class CardForm extends CFormModel
{
	public $number;
	public $cvc;
	public $expiry_month;
	public $expiry_year;

	public function rules()
	{
		return array(
			array('number, cvc, expiry_month, expiry_year', 'required'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'number'=>'Credit Card Number',
			'cvc'=>'Security Code',
			'expiry_month'=>'Expiration Date',
		);
	}
}
