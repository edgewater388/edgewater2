<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel
{
	public $first_name;
	public $last_name;
	public $phone;
	public $email;
	public $subject;
	public $body;
	public $verifyCode;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('first_name, last_name, email, phone, body', 'required'),
			// email has to be a valid email address
			array('email', 'email'),
			// verifyCode needs to be entered correctly
			// array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'verifyCode'=>'Verification Code',
		);
	}

	public function beforeValidate()
	{
		$valid = parent::beforeValidate();

		$this->subject = 'Contact: '.CString::substr($this->body, 30);

		return $valid;
	}

	public function send()
	{
		if ($this->validate())
		{
			$message = new MailMessage;
			$message->subject = $this->subject;
			$message->addTo(Yii::app()->params['contactEmail'], Yii::app()->name);

			if (isset(Yii::app()->params['contactEmailAlt']))
				$message->addTo(Yii::app()->params['contactEmailAlt'], Yii::app()->name);

			$message->setFrom($this->email, $this->name);

			$body = '';
			$body.= 'Name: '.$this->name.'<br>';
			$body.= 'Email: '.$this->email.'<br>';
			$body.= 'Phone: '.$this->phone.'<br>';
			$body.= 'Message:<br>';
			$body.= $this->body;
			$message->body = $body;
			
			return Yii::app()->mail->send($message);
		}

		return false;
	}

	public function getName()
	{
		return $this->first_name.' '.$this->last_name;
	}
}