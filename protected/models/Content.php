<?php

/**
 * This is the model class for table "content".
 *
 * The followings are the available columns in table 'content':
 * @property string $key
 * @property string $value
 */
class Content extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Content the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'content';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('key', 'length', 'max'=>32),
			array('value', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('key, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'key' => 'Key',
			'value' => 'Value',
		);
	}

	public function searchCriteria()
	{
		$criteria = parent::searchCriteria();

		$criteria->compare('key',$this->key,false);
		$criteria->compare('value',$this->value,false);

		return $criteria;
	}

	public static function setValue($key, $value)
	{
		if (!$model = self::model()->findByPk($key))
		{
			$model = new Content;
			$model->key = $key;
		}

		$model->value = is_array($value) ? json_encode($value) : $value;
		return $model->save();
	}

	public static function getValue($key)
	{
		if (!$model = self::model()->findByPk($key))
			return false;

		$array = json_decode($model->value);

		// JSON is valid
		if (json_last_error() === 0)
			return $array;

		return $model->value;
	}
}
