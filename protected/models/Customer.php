<?php

/**
 * This is the model class for table "customer".
 * The followings are the available columns in table 'customer':
 *
 * @property string $id
 * @property string $user_id
 * @property string $service
 * @property string $token
 * @property string $card_number
 * @property string $card_expiry
 * @property string $card_type
 * @property string $name
 * @property string $street
 * @property string $street1
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $zipcode
 * @property integer $status
 * @property string $created
 * @property string $updated
 * The followings are the available model relations:
 * @property Users $user
 */
class Customer extends PActiveRecord
{
	public $first_name;
	public $last_name;
	public $card_month;
	public $card_year;
	public $new_card_number;
	public $card_security;
	public $over_18;
	public $email;
	public $promo_code; // for the sign up form

	private $_pk;
	
	public function init()
	{
		parent::init();

		$this->country = 'US';
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			['first_name, last_name', 'required', 'on' => 'complimentary'],
			['first_name, last_name', 'required', 'on' => 'create, create_trial, validate_step_2, validate_step_2_trial', 'except' => 'social_step_2, social_trial_step_2'],
			['new_card_number, card_month, card_year, card_security, street', 'required', 'on' => 'create, validate_step_2, social_step_2'],
			['new_card_number', 'length', 'min' => 13, 'max' => 17],
			['card_security', 'length', 'min' => 2, 'max' => 4],
			['status, over_18', 'numerical', 'integerOnly' => true],
			['over_18', 'required', 'requiredValue' => 1, 'message' => 'You must be at least 18 and agree to Terms of Use and Privacy/Cookies policies to continue.', 'on' => 'create, create_trial complimentary, validate_step_2_trial, validate_step_3, social_step_3'],
			['user_id, zipcode', 'length', 'max' => 10],
			['service, card_type', 'length', 'max' => 32],
			['token', 'length', 'max' => 255],
			['card_number, card_expiry, card_year, card_security', 'length', 'max' => 4],
			['new_card_number', 'length', 'max' => 17],
			['first_name, last_name, name, street, street1', 'length', 'max' => 30],
			['city', 'length', 'max' => 20],
			['state, country, card_month', 'length', 'max' => 2],
			['created, updated', 'safe'],

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			['id, user_id, service, token, card_number, card_expiry, card_type, name, street, street1, city, state, country, zipcode, status, created, updated', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return [
			'user' => [self::BELONGS_TO, 'Users', 'user_id'],
		];
	}

	public function behaviors() {
		return [
			'EJsonBehavior'=> [
				'class'=>'application.extensions.behaviors.EJsonBehavior'
			],
	    ];
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id'              => 'ID',
			'user_id'         => 'User',
			'service'         => 'Service',
			'token'           => 'Token',
			'card_number'     => 'Card Number',
			'new_card_number' => 'Card Number',
			'card_expiry'     => 'Card Expiry',
			'card_type'       => 'Card Type',
			'name'            => 'Name',
			'street'          => 'Street',
			'street1'         => 'Street1',
			'city'            => 'City',
			'state'           => 'State',
			'country'         => 'Country',
			'zipcode'         => 'Zipcode',
			'status'          => 'Status',
			'created'         => 'Created',
			'updated'         => 'Updated',
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchCriteria()
	{
		$criteria = parent::searchCriteria();

		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('service', $this->service, true);
		$criteria->compare('token', $this->token, true);
		$criteria->compare('card_number', $this->card_number, true);
		$criteria->compare('card_expiry', $this->card_expiry, true);
		$criteria->compare('card_type', $this->card_type, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('street', $this->street, true);
		$criteria->compare('street1', $this->street1, true);
		$criteria->compare('city', $this->city, true);
		$criteria->compare('state', $this->state, true);
		$criteria->compare('country', $this->country, true);
		$criteria->compare('zipcode', $this->zipcode, true);

		return $criteria;
	}

	public function beforeValidate()
	{
		$valid = parent::beforeValidate();

		// $this->name = substr($this->first_name.' '.$this->last_name, 30);
		$this->card_month = str_pad($this->card_month, 2, '0', STR_PAD_LEFT);
		$this->card_expiry = $this->card_month . substr($this->card_year, 2);
		$this->street = substr($this->street, 0, 30);
		$this->street1 = substr($this->street1, 0, 30);
		$this->city = substr($this->city, 0, 20);
		$this->state = substr($this->state, 0, 2);
		$this->country = substr($this->country, 0, 2);
		$this->zipcode = substr($this->zipcode, 0, 10);

		$ignore_scenarios = [
			'complimentary',
			'validate_step_1',
			'validate_step_2_trial',
			'validate_step_3',
			'social_step_3',
			'create_trial',
		];

		if (!in_array($this->scenario, $ignore_scenarios)) {
			if ($this->country == 'US' && !$this->zipcode)
				$this->addError('zipcode', 'Zip Code is required');

			if ($this->country == 'CA' && !$this->zipcode)
				$this->addError('zipcode', 'Postal Code is required');
		}

		return $valid;
	}

	public function beforeSave()
	{
		$valid = parent::beforeSave();

		if ($this->scenario == 'import')
			return $valid;

		if ($this->scenario == 'complimentary')
			return $valid;

		if ($this->scenario == 'create_trial')
			return $valid;

		$params = [
			'customer_profile_id'         => $this->customer_profile_id,
			'customer_payment_profile_id' => $this->customer_payment_profile_id,
			// 'user_id'=>Yii::app()->user->id,
			// 'email'=>Yii::app()->user->email,
			'user_id'                     => empty($this->user_id) && !empty(Yii::app()->user->id) ? Yii::app()->user->id : $this->user_id,
			'email'                       => empty($this->email) && !empty(Yii::app()->user->email) ? Yii::app()->user->email : $this->email,
			'name'                        => $this->name,
			'first_name'                  => $this->first_name,
			'last_name'                   => $this->last_name,
			'address'                     => $this->street,
			'zipcode'                     => $this->zipcode,
			'country'                     => $this->country,
			'card_number'                 => $this->new_card_number,
			'card_year'                   => $this->card_year,
			'card_month'                  => $this->card_month,
			'card_security'               => $this->card_security,
		];

		// update the payment information on authorize.net
		$response = Yii::app()->authorizenet->updateCustomerProfile($params);

		// print_r so the xml is a string and can easily be saved to the database. converting xml to array returns false..
		CustomerLog::log($response['response']->getMessageText(), print_r($response, true));

		$this->customer_profile_id = $response['customer_profile_id'];
		$this->customer_payment_profile_id = $response['customer_payment_profile_id'];

		if (!$response['success']) {
			$this->addError('payment', 'Unable to process payment information: ' . $response['response']->getMessageText());
			$valid = false;
		}

		$this->card_number = substr($this->new_card_number, strlen($this->new_card_number) - 4);
		$this->card_type = self::getCardType($this->new_card_number);
		
		$data = array_merge(['email' => empty($this->email) && !empty(Yii::app()->user->email) ? Yii::app()->user->email : $this->email], $this->getAttributes($this->fields()));

		$url = Yii::app()->params['magentoAuthCardUrl'].'?code='.Yii::app()->params['securityCode'];
		$output = Yii::app()->curl->post($url, $data); 

		return $valid;
	}

	public function afterFind()
	{
		parent::afterFind();

		$this->card_year = '20' . substr($this->card_expiry, 2);
		$this->card_month = substr($this->card_expiry, 0, 2);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className active record class name.
	 *
	 * @return Customer the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public static function retrieve($user_id)
	{
		return Customer::model()->byUser($user_id)->find();
	}

	// public static function updateProfile($user_id, $params=array())
	// {

	// }

	public function getName()
	{
		return $this->first_name . ' ' . $this->last_name;
	}

	public static function getCardType($card_number)
	{
		$first_number = substr($card_number, 0, 1);

		if ($first_number == '3')
			return 'Amex';
		if ($first_number == '4')
			return 'Visa';
		if ($first_number == '5')
			return 'MasterCard';
		if (substr($card_number, 0, 4) == '6011')
			return 'Discover Card';

		return null;
	}

	protected static function generateRandomString($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

		return $randomString;
	}

	public static function getRandomEmail()
	{
		return static::generateRandomString() . '@' .
			static::generateRandomString(5) . '.' .
			static::generateRandomString(3);
	}

	public static function getRandomPassword()
	{
		return static::generateRandomString();
	}
	
	//fields for rest api
	public function fields() {
        return [
			'service',
			'token',
			'customer_profile_id',
			'customer_payment_profile_id',
			'card_number',
			'card_expiry',
			'card_type',
			'first_name',
			'last_name',
			'street',
			'street1',
			'city',
			'state',
			'country',
			'zipcode'
		];
	}
	
	//extraFields for rest api
	public function extraFields() {
        return [
			'user'
		];
	}

    public function insertDirectly($attributes=null)
    {
        if(!$this->getIsNewRecord())
            throw new CDbException(Yii::t('yii','The active record cannot be inserted to database because it is not new.'));
        Yii::trace(get_class($this).'.insert()','system.db.ar.CActiveRecord');
        $builder=$this->getCommandBuilder();
        $table=$this->getMetaData()->tableSchema;
        $command=$builder->createInsertCommand($table,$this->getAttributes($attributes));
        if($command->execute())
        {
            $primaryKey=$table->primaryKey;
            if($table->sequenceName!==null)
            {
                if(is_string($primaryKey) && $this->$primaryKey===null)
                    $this->$primaryKey=$builder->getLastInsertID($table);
                elseif(is_array($primaryKey))
                {
                    foreach($primaryKey as $pk)
                    {
                        if($this->$pk===null)
                        {
                            $this->$pk=$builder->getLastInsertID($table);
                            break;
                        }
                    }
                }
            }
            $this->_pk=$this->getPrimaryKey();
            $this->afterSave();
            $this->setIsNewRecord(false);
            $this->setScenario('update');
            return true;
        }
        return false;
    }

    public function updateDrectly($attributes=null)
    {
        if($this->getIsNewRecord())
            throw new CDbException(Yii::t('yii','The active record cannot be updated because it is new.'));
        Yii::trace(get_class($this).'.update()','system.db.ar.CActiveRecord');
        if($this->_pk===null)
            $this->_pk=$this->getPrimaryKey();
        $this->updateByPk($this->getOldPrimaryKey(),$this->getAttributes($attributes));
        $this->_pk=$this->getPrimaryKey();
        $this->afterSave();
        return true;
    }

    public function saveDirectly($runValidation=true,$attributes=null)
    {
        if(!$runValidation || $this->validate($attributes))
            return $this->getIsNewRecord() ? $this->insertDirectly($attributes) : $this->updateDrectly($attributes);
        else
            return false;
    }
}
