<?php

/**
 * This is the model class for table "faq_category".
 *
 * The followings are the available columns in table 'faq_category':
 * @property string $id
 * @property string $category
 * @property integer $weight
 *
 * The followings are the available model relations:
 * @property Faq[] $faqs
 */
class FaqCategory extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FaqCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'faq_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category', 'required'),
			array('weight', 'numerical', 'integerOnly'=>true),
			array('category', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, category, weight', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'faqs' => array(self::HAS_MANY, 'Faq', 'faq_category_id', 'condition'=>'faqs.status=1', 'order'=>'faqs.weight ASC'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category' => 'Category',
			'weight' => 'Weight',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,false);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('weight',$this->weight);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function sorted()
	{
		$this->getDbCriteria()->mergeWith(array(
			'order'=>'t.weight ASC, t.category ASC',
		));

		return $this;
	}

	public static function getList()
	{
		return Html::listData(self::model()->findAll(array('order' => 't.weight ASC')),'id','category');
	}
}