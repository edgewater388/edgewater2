<?php

/**
 * This is the model class for table "membership".
 * The followings are the available columns in table 'membership':
 *
 * @property string $id
 * @property string $user_id
 * @property string $subtotal
 * @property string $tax
 * @property string $total
 * @property string $join_date
 * @property string $prev_billing_date
 * @property string $next_billing_date
 * @property string $interval
 * @property integer $status
 * @property string $created
 * @property string $updated
 * The followings are the available model relations:
 * @property Users $user
 */
class Membership extends BaseRecord
{
	const BASE_TRIAL_DURATION = 15;

	public $cancel_membership;
	public $name, $email; // search params

	// custom
	public $trial_from;
	public $trial_to;
	public $is_trial_state;

	public $new_promo_code;

	// search
	public $period, $period_from, $period_to;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'membership';
	}

	public function init()
	{
		parent::init();
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return CMap::mergeArray(parent::rules(), [
			['user_id', 'required'],
			['auto_renew, status, discount_offered', 'numerical', 'integerOnly' => true],
			['user_id', 'length', 'max' => 10],
			['subtotal, tax, total', 'length', 'max' => 16],
			['interval', 'length', 'max' => 8],
			['valid_from, valid_to, join_date, prev_billing_date, next_billing_date, created, updated', 'safe'],
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			['id, user_id, subtotal, tax, total, join_date, prev_billing_date, next_billing_date, interval, status, created, updated', 'safe', 'on' => 'search'],
			['name, email', 'safe', 'on' => 'search'],

			['cancel_membership', 'safe', 'on' => 'cancel'],
			['plan_id', 'safe', 'on' => 'change, search'],
			['trial_from, trial_to, is_trial_state', 'safe', 'on' => 'search'],
			['period, period_from, period_to', 'safe', 'on' => 'search'],
			['new_promo_code', 'safe', 'on' => 'new_promo_code'],
		]);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return [
			'user'                   => [self::BELONGS_TO, 'Users', 'user_id'],
			'promoCode'              => [self::BELONGS_TO, 'PromoCode', 'promo_code_id'],
			'promoCodeRedemptionLog' => [self::HAS_ONE, 'PromoCodeRedemptionLog', ['promo_code_id' => 'promo_code_id'], 'order' => 'created DESC'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id'                    => 'ID',
			'user_id'               => 'User',
			'subtotal'              => 'Subtotal',
			'tax'                   => 'Tax',
			'total'                 => 'Total',
			'join_date'             => 'Join Date',
			'prev_billing_date'     => 'Prev Billing Date',
			'next_billing_date'     => 'Next Billing Date',
			'valid_from'            => 'Active From',
			'valid_to'              => 'Active To',
			'interval'              => 'Interval',
			'status'                => 'Status',
			'created'               => 'Created',
			'updated'               => 'Updated',

			// custom
			'is_trial_state'        => 'Currently in trial',
			'user.transactionCount' => 'Transactions',
			'user.renewalCount'     => 'Renewals',
		];
	}

	public function searchCriteria()
	{
		$criteria = parent::searchCriteria();

		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('subtotal', $this->subtotal, true);
		$criteria->compare('tax', $this->tax, true);
		$criteria->compare('total', $this->total, true);
		$criteria->compare('join_date', $this->join_date, true);
		$criteria->compare('prev_billing_date', $this->prev_billing_date, true);
		$criteria->compare('next_billing_date', $this->next_billing_date, true);
		$criteria->compare('interval', $this->interval, true);
		$criteria->compare('plan_id', $this->plan_id);
		$criteria->compare('subtotal', $this->subtotal);

		if ($this->name) {
			$nameCriteria = new CDbCriteria;
			$nameCriteria->with = 'user.profile';
			$nameCriteria->compare('CONCAT(profile.first_name," ",profile.last_name)', $this->name, true, 'OR');
			$criteria->mergeWith($nameCriteria);
		}

		if ($this->email) {
			$emailCriteria = new CDbCriteria;
			$emailCriteria->with = 'user';
			$emailCriteria->compare('user.email', $this->email, true, 'OR');
			$criteria->mergeWith($emailCriteria);
		}

		// if ($this->trial_from) {
		// 	$trial_to = date('Y-m-d', strtotime($this->trial_to.' + 1 day'));
		// 	$criteria->addBetweenCondition('t.trial_end_date',$this->trial_from,$trial_to);
		// }
		// elseif ($this->trial_to) {
		// 	$trial_to = date('Y-m-d', strtotime($this->trial_to.' + 1 day'));
		// 	$criteria->addCondition('t.trial_end_date <= :trial_to');
		// 	$criteria->params[':trial_to'] = $trial_to;
		// }

		if ($this->is_trial_state) {
			$criteria->addCondition('t.trial_end_date >= :trial_date');
			$criteria->params[':trial_date'] = date('Y-m-d');
		}

		if ($this->period_from) {
			if (!$this->period_to)
				$this->period_to = date('Y-m-d');

			$period_to = date('Y-m-d', strtotime($this->period_to . ' + 1 day'));
			// $criteria->addBetweenCondition('t.trial_end_date',$this->period_from,$period_to);
			$criteria->addCondition('(t.trial_end_date >= :period_from AND t.trial_end_date < :period_to) OR (t.created >= :period_from AND t.created < :period_to AND t.trial_end_date IS NULL)');
			$criteria->params[':period_from'] = $this->period_from;
			$criteria->params[':period_to'] = $period_to;
		}

		if (!$criteria->order)
			$criteria->order = 't.created DESC';

		if ($query = Yii::app()->request->getParam('query')) {
			$end_date = isset($_GET['Membership']['period_to']) ? $_GET['Membership']['period_to'] : date('Y-m-d');
			// // $end_date = isset($_GET['Membership']['created_to']) ? $_GET['Membership']['created_to'] : date('Y-m-d');
			//       if ($end_date > date('Y-m-d'))
			//       	$end_date = date('Y-m-d');
			$c = new CDbCriteria;

			if ($query == 'active') {
				$c->addCondition('valid_to >= :end_date');
				$c->params[':end_date'] = $end_date;
			} elseif ($query == 'expired') {
				$c->addCondition('valid_to < :end_date AND cancelled_date IS NULL');
				$c->params[':end_date'] = $end_date;
			} elseif ($query == 'cancelled') {
				$c->addCondition('cancelled_date < :end_date');
				$c->params[':end_date'] = $end_date;
			} elseif ($query == 'retained') {
				$c->addCondition('valid_to > trial_end_date'); // membership successfully renewed after the initial trial period
				// $c->addCondition('trial_end_date < :end_date AND valid_to >= :end_date');
			} elseif ($query == 'lost') {
				$c->addCondition('valid_to > cancelled_date'); // the customer cancelled the membership before the next billing cycle
			}

			$criteria->mergeWith($c);
		}

		return $criteria;
	}

	public function scopes()
	{
		return CMap::mergeArray(parent::scopes(), [
			'hasPendingPayment' => [
				'condition' => 'next_billing_date <= curdate()',
			],
			'hasActivePromo'    => [
				'condition' => '(cancelled_date IS NULL AND billing_failed_date IS NULL AND next_billing_date IS NULL AND promo_end_date IS NOT NULL AND promo_end_date > curdate() AND valid_to = curdate())',
			],
			'isNotAutoRenewing' => [
				'condition' => 'auto_renew = 0',
			],
		]);
	}

	public function trialEnding($days = 3)
	{
		$t = $this->getTableAlias(false, false);
		$this->getDbCriteria()->mergeWith([
			'condition' => $t . '.trial_end_date = :date',
			'params'    => [':date' => date('Y-m-d', strtotime('+' . $days . ' days'))],
		]);

		return $this;
	}

	public function trialReminderSent($bool = false)
	{
		$condition = $bool ? 'IS NOT NULL' : 'IS NULL';

		$t = $this->getTableAlias(false, false);
		$this->getDbCriteria()->mergeWith([
			'condition' => $t . '.trial_reminder_sent_date ' . $condition,
		]);

		return $this;
	}

	public function isAutoRenewing($bool = true)
	{
		$t = $this->getTableAlias(false, false);
		$this->getDbCriteria()->mergeWith([
			'condition' => $t . '.auto_renew = :bool',
			'params'    => [':bool' => $bool ? 1 : 0],
		]);

		return $this;
	}

	public function beforeValidate()
	{
		$valid = parent::beforeValidate();

		// check if there is already a membership for this user
		if ($this->isNewRecord && $model = self::model()->byAttribute('user_id', $this->user_id)->find()) {
			$this->addError('user_id', 'Membership already exists for <a href="' . Yii::app()->createUrl('membership/view', ['id' => $model->id]) . '">' . $model->user->profile->name . '</a>.');
		}

		if (!$this->valid_from)
			$this->valid_from = date('Y-m-d');

		if (!$this->join_date)
			$this->join_date = date('Y-m-d');

		if ($this->cancel_membership && !$this->canCancel())
			$this->addError('cancel', 'Oops! Your membership is currently under a minumum term and cannot be cancelled at this time.');

		return true;
	}

	public function beforeSave()
	{
		$valid = parent::beforeSave();

		if ($this->cancel_membership) {
			$this->next_billing_date = null;
			$this->cancelled_date = date('Y-m-d');
			$this->auto_renew = 0;
			$this->min_term = null;

			// reset the promo
			$this->promo_code = null;
			$this->promo_code_id = null;
			$this->promo_amount = null;
			$this->promo_end_date = null;


			ReferralBank::cancelPendingAwards($this->user_id);
		}

		$this->tax = $this->subtotal * Yii::app()->params['tax'];
		$this->total = (double)$this->subtotal + (double)$this->tax;

		return $valid;
	}

	public function afterSave()
	{
		parent::afterSave();

		MembershipLog::log($this);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className active record class name.
	 *
	 * @return Membership the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public static function retrieve($user_id)
	{
		return self::model()->byUser($user_id)->find();
	}

	public function getPeriod()
	{
		if ($this->interval == 'monthly')
			return 'month';
		if ($this->interval == 'yearly')
			return 'year';
		if ($this->interval == 'daily')
			return 'day';

		return '';
	}

	public function getPeriodAbbreviated()
	{
		if ($this->interval == 'monthly')
			return 'MO.';
		if ($this->interval == 'yearly')
			return 'YR.';
		if ($this->interval == 'daily')
			return 'DAY';

		return '';
	}


	public function startPromotion($promo_code, $verify = true, $instantRedemption = false)
	{
		if (!$promoCode = $this->preparePromotion($promo_code, $verify, $instantRedemption))
			return false;

		PromoCodeRedemptionLog::log($this->user_id, $promoCode);

		return $this->save();
	}

	public function preparePromotion($promo_code, $verify = true, $instantRedemption)
	{
		if ($verify && !PromoCode::verify($promo_code, $this->plan_id))
			return false;

		if (!$promoCode = PromoCode::retrieve($promo_code))
			return false;

		$this->promo_code = $promo_code;
		$this->promo_code_id = $promoCode->id;
		$this->promo_amount = $promoCode->calculateAmount($this->subtotal);
		$this->promo_end_date = $promoCode->calculateEndDate($instantRedemption);

		return $promoCode;
	}

	public static function startMembership($user_id, $plan_id = null, $trial_sign_up = false)
	{
		if (!$plan = Plan::getPlan($plan_id)) {
			$plan = Plan::getDefaultPlan();
		}

		if ($trial_sign_up) {
			$trial_price = (double)Plan::getTrialPrice();

			if ($trial_price) {
				$plan['title'] = "\${$trial_price} Monthly";
				$plan['amount'] = $trial_price;
			}
		}

		if (!$model = Membership::model()->byAttribute('user_id', $user_id)->find()) {
			$model = new Membership;
			$model->user_id = $user_id;
			$model->join_date = date('Y-m-d');
			$model->trial_end_date = self::getTrialEndDate();
			$model->valid_from = date('Y-m-d');
			$model->cancelled_date = null;

			if ($plan['interval'] == 'daily')
				$model->trial_end_date = null;
		}

		$model->plan_id = $plan['id'];
		$model->subtotal = $plan['amount'];
		$model->interval = $plan['interval'];
		$model->valid_to = ($model->interval == 'daily') ?
			date('Y-m-d', strtotime('+1 day')) :
			date('Y-m-d', strtotime('+' . static::getTrialDuration($trial_sign_up) . ' days'));
		$model->auto_renew = $plan['is_auto_renew'];
		$model->next_billing_date = $model->auto_renew ? date('Y-m-d', strtotime('+' . static::getTrialDuration($trial_sign_up) . ' days')) : null;
		$model->min_term = isset($plan['min_term']) ? $plan['min_term'] : null;
		$model->plan_renewal_count = 0;
		$model->status = 1;
		$model->save();

		return $model;
	}

	public static function startSpecialOffer($plan)
	{
		$user = Yii::app()->user;

		if (!$model = Membership::model()->byAttribute('user_id', $user->id)->find()) {
			$model = new Membership;
			$model->user_id = $user->id;
			$model->join_date = date('Y-m-d');
			$model->trial_end_date = null;
			$model->valid_from = date('Y-m-d');
			$model->cancelled_date = null;
			$model->prev_billing_date = date('Y-m-d');
		}

		$model->plan_id = $plan['id'];
		$model->subtotal = $plan['amount'];
		$model->valid_to = date('Y-m-d', strtotime('+1 month'));
		$model->next_billing_date = $plan['is_auto_renew'] ? date('Y-m-d', strtotime('+1 month')) : null;
		$model->interval = $plan['interval'];
		$model->auto_renew = $plan['is_auto_renew'];
		$model->min_term = isset($plan['min_term']) ? $plan['min_term'] : null;
		$model->plan_renewal_count = 1;
		$model->status = 1;

		// reset the promo
		$model->promo_code = null;
		$model->promo_code_id = null;
		$model->promo_amount = null;
		$model->promo_end_date = null;

		if ($model->save()) {
			$transaction = self::processPayment($model);

			if ($transaction && $transaction->approved) {
				$message = new MailMessage;
				$message->view = 'recurring_payment_success';
				$message->subject = Yii::app()->name . ' Receipt';
				$message->addTo($model->user->email, $model->user->profile->name);
				$message->setBody([
					'membership'  => $model,
					'user'        => $model->user,
					'transaction' => $transaction,
					'customer'    => $model->user->customer ? $model->user->customer : null,
				]);

				Yii::app()->mail->send($message);

				return $model;
			} else {
				$model->valid_to = null;
				$model->billing_failed_date = date('Y-m-d');
				$model->plan_renewal_count = 0;
				$model->update();
			}
		}

		return false;
	}

	public static function changePlan($user_id, $plan_id, $promo_code = null)
	{
		if (!$model = self::retrieve($user_id)) {
			$model = new Membership;
			$model->user_id = $user_id;
			$model->join_date = date('Y-m-d');
			$model->trial_end_date = self::getTrialEndDate();
			$model->valid_from = date('Y-m-d');
			$model->cancelled_date = null;
		}

		if (!$model->canChange()) {
			Flash::setFlash('error', 'Oops! Your current plan has a minimum commitment and cannot be changed at this time.');

			return false;
		}

		if (!$plan = Plan::getPlan($plan_id)) {

			Flash::setFlash('error', 'Missing or invalid plan. Please try again.');

			return false;
		}

		if ($promo_code && !PromoCode::verify($promo_code, $plan_id)) {
			Flash::setFlash('error', 'Promo code could not be applied to the current plan.');

			return false;
		}

		$model->plan_id = $plan['id'];
		$model->interval = $plan['interval'];
		$model->auto_renew = $plan['is_auto_renew'];
		$model->subtotal = $plan['amount'];
		$model->min_term = isset($plan['min_term']) ? $plan['min_term'] : null;
		$model->plan_renewal_count = 0;
		$model->tax = $model->subtotal * Yii::app()->params['tax'];
		$model->total = $model->subtotal + $model->tax;
		$model->valid_from = ($model->valid_from) ? $model->valid_from : date('Y-m-d');
		$model->cancelled_date = null;
		$model->status = 1;

		// reset the promo when changing plans
		$model->promo_code = null;
		$model->promo_code_id = null;
		$model->promo_amount = null;
		$model->promo_end_date = null;

		$promoCode = null;

		if ($promo_code)
			$promoCode = $model->preparePromotion($promo_code, false, true);

		if ($model->valid_to <= date('Y-m-d')) {
			$model->valid_to = self::calculateValidToDate($model->valid_to, $model->interval);
			$model->next_billing_date = $model->auto_renew ? self::calculateNextDate($model->interval) : null;
			$model->plan_renewal_count = 1; // plan renews at time its being changed
			$transaction = self::processPayment($model);

			if (!$transaction || !$transaction->approved)
				return false;
		} else {
			$model->next_billing_date = $model->auto_renew ? $model->valid_to : null;
		}

		if ($model->save()) {
			if ($promo_code && $promoCode)
				PromoCodeRedemptionLog::log($model->user_id, $promoCode);

			$message = new MailMessage;
			$message->view = 'membership_changed';
			$message->subject = 'You have successfully changed your plan!';
			$message->addTo($model->user->email, $model->user->profile->name);
			$message->setBody([
				'plan'       => $plan,
				'membership' => $model,
				'user'       => $model->user,
			]);
			Yii::app()->mail->send($message);

			return true;
		}

		return false;
	}

	public function isActive()
	{
		return ($this->valid_to >= date('Y-m-d'));
	}

	public function getPromoDiscount()
	{
		return $this->subtotal - $this->promo_amount;
	}

	public static function calculateValidToDate($valid_to, $interval)
	{
		// if the valid to date is in the past, the membership has already expired.
		if (!$valid_to || $valid_to < date('Y-m-d'))
			$valid_to = date('Y-m-d');

		if ($interval == 'monthly')
			return date('Y-m-d', strtotime($valid_to . ' + 1 month'));
		else if ($interval == 'yearly')
			return date('Y-m-d', strtotime($valid_to . ' + 1 year'));
		else if ($interval == 'daily')
			return date('Y-m-d', strtotime($valid_to . ' + 1 day'));

		return $valid_to;
	}

	public static function calculateNextDate($interval)
	{
		if ($interval == 'monthly')
			return date('Y-m-d', strtotime('+1 month'));
		else if ($interval == 'yearly')
			return date('Y-m-d', strtotime('+1 year'));
		else if ($interval == 'daily')
			return date('Y-m-d', strtotime('+1 day'));

		return date('Y-m-d');
	}

	public static function getTrialEndDate()
	{
		return date('Y-m-d', strtotime('+' . static::getTrialDuration() . ' days'));
	}

	public static function sendTrialEndingReminders()
	{
		$models = self::model()
			->trialEnding(3)
			// ->isAutoRenewing(true)
			->trialReminderSent(false)
			->findAll();

		if (!$models)
			return;

		foreach ($models as $model) {
			$message = new MailMessage;
			$message->view = 'trial_ending_reminder';
			$message->subject = 'Your Trial is Ending in 3 days';
			$message->addTo($model->user->email, $model->user->profile->name);
			$message->setBody([
				'membership' => $model,
				'user'       => $model->user,
			]);

			if (Yii::app()->mail->send($message)) {
				$model->trial_reminder_sent_date = date('Y-m-d');
				$model->update();
			}
		}
	}

	public static function processRecurringPayments()
	{
		$models = self::model()
			->hasPendingPayment()
			->isAutoRenewing()
			->findAll();

		if (!$models)
			return;

		// die(var_dump($models));

		foreach ($models as $model) {
			$success = self::processRecurringPayment($model);
		}
	}

	public static function processRecurringPromos()
	{
		$models = self::model()
			->hasActivePromo()
			->isNotAutoRenewing()
			->findAll();

		if (!$models)
			return;

		foreach ($models as $model) {
			$success = self::processRecurringPayment($model);
		}
	}

	public static function processRecurringPayment(Membership $model)
	{
		if (!$model->user) {
			return false;
		}

		// Process the payment
		$transaction = self::processPayment($model);
		$success = ($transaction && $transaction->approved) ? true : false;
		$notify_user = true;

		if ($success) {
			$model->billing_failed_date = null;
			// next_billing_date is null when previous billing date failed, so continue to use the prevoius billing date
			$model->prev_billing_date = date('Y-m-d');//$model->next_billing_date ? $model->next_billing_date : date();
			$model->next_billing_date = $model->auto_renew ? self::calculateNextDate($model->interval) : null;// date('Y-m-d', strtotime('+1 month'));
			$model->valid_to = self::calculateValidToDate($model->valid_to, $model->interval);
			$model->plan_renewal_count += 1;
			$model->billing_failed_count = 0;
			$notify_user = false;
		} else {
			$model->billing_failed_date = date('Y-m-d');//$model->next_billing_date;

			// retry billing once
			if ($model->billing_failed_count < 1) {
				$model->next_billing_date = date('Y-m-d', strtotime('+2 days'));
				// give 2 days extra to be valid until account is suspended
				if ($model->valid_to < date('Y-m-d', strtotime('+2 days'))) {
					$model->valid_to = date('Y-m-d', strtotime('+2 days'));
				}
			} else {
				$model->next_billing_date = null;
			}

			$model->billing_failed_count++;
		}

		if ($notify_user && !empty($model->user->email)) {
			$message = new MailMessage;
			$message->view = $success ? 'recurring_payment_success' : 'recurring_payment_failure';
			$message->subject = $success ? Yii::app()->name . ' Receipt' : 'Please Update Your Payment Method';
			$name = isset($model->user->profile) ? $model->user->profile->name : $model->user->email;
			$message->addTo($model->user->email, $name);
			$message->setBody([
				'membership'  => $model,
				'user'        => $model->user,
				'transaction' => $transaction,
				'customer'    => $model->user->customer ? $model->user->customer : null,
			]);

			Yii::app()->mail->send($message);
		}

		$model->update();

		return $success;
	}

	public static function processPayment(Membership $model)
	{
		if (!$model->user || !$model->user->customer)
			return false;

		$subtotal = $model->getSubtotal();

		// load and apply any pending rewards
		$rewards = [];
		if ($subtotal >= 0) {
			foreach (ReferralBank::getAvailableAwards($model->user_id) as $reward) {
				// stop issuing rewards if it will become a negative amount;
				if (($subtotal - $reward->amount) < 0)
					break;

				$rewards[] = $reward;
				$subtotal -= $reward->amount;
			}
		}

		$transaction = Transaction::charge([
			'user_id'           => $model->user_id,
			// 'description'=>Yii::app()->name.' Receipt',
			'promo_code'        => $model->promo_code,
			'description'       => 'P57 VOD Service',
			'subtotal'          => $subtotal,
			'tax'               => self::calculateTax($subtotal),
			'total'             => self::calculateTotal($subtotal),
			'customer'          => $model->user->customer,
			'membership_period' => $model->calculateNextMembershipPeriod(),
		]);

		if ($transaction && $transaction->approved) {
			// After a successful payment, mark any used rewards as redeemed
			foreach ($rewards as $reward)
				$reward->doRedemption();

			// After a successful payment, release any pending awards
			ReferralBank::releaseRewards($model->user_id);
		}

		return $transaction;
	}

	public function getSubtotal()
	{
		if ($this->promo_amount !== null) {
			if (!$this->promo_end_date || $this->promo_end_date >= date('Y-m-d'))
				return $this->promo_amount;
		}

		return $this->subtotal;
	}

	public static function calculateTax($subtotal)
	{
		return $subtotal * Yii::app()->params['tax'];
	}

	public static function calculateTotal($subtotal)
	{
		return $subtotal + self::calculateTax($subtotal);
	}

	public function calculateNextMembershipPeriod()
	{
		$period = date('m/d/Y') . ' - ';
		if ($this->interval == 'monthly')
			$period .= date('m/d/Y', strtotime('+1 month'));
		else if ($this->interval == 'yearly')
			$period .= date('m/d/Y', strtotime('+1 year'));
		else if ($this->interval == 'daily')
			$period .= date('m/d/Y', strtotime('+1 day'));
		else
			$period .= date('m/d/Y');

		return $period;
	}

	public function getPlan()
	{
		if ($this->plan_id && $plan = Plan::getPlan($this->plan_id))
			return $plan;

		return null;
	}

	public function getDisplayTitle()
	{
		$name = isset($this->user->profile->name) ? $this->user->profile->name : '-';

		return 'Membership: ' . $name;
	}

	public function getCounts()
	{
		$criteria = $this->searchCriteria();

		$columns = [
			'count(*) count'           => 'Count',
			'sum(t.subtotal) subtotal' => 'Subtotal',
			'sum(t.tax) tax'           => 'Tax',
			'sum(t.total) total'       => 'Total',
		];

		// $header = array();
		$select = [];
		foreach ($columns as $column => $label) {
			// $header[$column] = $label;
			$select[] = $column;
		}

		$command = Yii::app()->db->createCommand();
		$command->select(implode(', ', $select));
		$command->from($this->tableName() . ' t');
		$command->where($criteria->condition, $criteria->params);

		return $command->queryRow();
	}

	public function getRetentionCounts()
	{
		$criteria = $this->searchCriteria();

		$columns = [
			'count(*) count'                                                                                  => 'Count',
			// 'sum( case when (trial_end_date < :end_date AND valid_to >= :end_date) then 1 else 0 end) as retained'=>'Retained',
			'sum( case when (valid_to > trial_end_date) then 1 else 0 end) as retained'                       => 'Retained',
			'sum( case when (valid_to > cancelled_date) then 1 else 0 end) as lost'                           => 'Lost',
			'sum( case when valid_to >= :end_date then 1 else 0 end) as active'                               => 'Active',
			'sum( case when (valid_to < :end_date AND cancelled_date IS NULL) then 1 else 0 end) as inactive' => 'Inactive',
			'sum( case when cancelled_date < :end_date then 1 else 0 end) as cancelled'                       => 'Cancelled',
			// 'sum( case when gender = 'male' then 1 else 0 end ) as male'
		];

		// $header = array();
		$select = [];
		foreach ($columns as $column => $label)
			$select[] = $column;

		// {
		//     // $header[$column] = $label;
		// }

		$end_date = isset($_GET['Membership']['period_to']) ? $_GET['Membership']['period_to'] : date('Y-m-d');
		// // $end_date = isset($_GET['Membership']['created_to']) ? $_GET['Membership']['created_to'] : date('Y-m-d');
		// // if ($end_date > date('Y-m-d'))
		// // 	$end_date = date('Y-m-d');
		$criteria->params[':end_date'] = $end_date;

		$command = Yii::app()->db->createCommand();
		$command->select(implode(', ', $select));
		$command->from($this->tableName() . ' t');
		$command->join('profile', 'profile.user_id=t.user_id');
		$command->where($criteria->condition, $criteria->params);

		return $command->queryRow();
	}

	public function exportRetention()
	{
		$criteria = $this->searchCriteria();

		$columns = [
			't.id'               => 'Membership ID',
			't.user_id'          => 'User ID',
			'profile.first_name' => 'First Name',
			'profile.last_name'  => 'Last Name',
			'users.email'        => 'Email',

			't.plan_id'                      => 'Plan ID',
			'count(transaction.id) renewals' => 'Renewals',
			't.subtotal'                     => 'Subtotal',
			't.tax'                          => 'Tax',
			't.total'                        => 'Total',
			't.join_date'                    => 'Join Date',
			't.trial_end_date'               => 'Trial End Date',
			// 't.trial_reminder_sent_date' => 'Trial Reminder Sent Date',
			't.valid_from'                   => 'Active From',
			't.valid_to'                     => 'Active To',
			't.prev_billing_date'            => 'Previous Billing Date',
			't.next_billing_date'            => 'Next Billing Date',
			't.billing_failed_date'          => 'Billing Failed Date',
			't.cancelled_date'               => 'Cancelled Date',
			't.interval'                     => 'Interval',
			't.auto_renew'                   => 'Auto Renew',

			// 't.status'=>'Status',
			't.created'                      => 'Created',
			't.updated'                      => 'Updated',
		];

		$headers = [];
		$select = [];
		foreach ($columns as $column => $label) {
			$headers[] = $label;
			$select[] = $column;
		}

		$rows = Yii::app()->db->createCommand()
			->select(implode(', ', $select))
			->from($this->tableName() . ' t')
			->join('users', 'users.id=t.user_id')
			->join('profile', 'profile.user_id=t.user_id')
			->leftJoin('transaction', 'transaction.user_id=t.user_id AND transaction.approved=1')
			->where($criteria->condition, $criteria->params)
			->group('t.id')
			// ->order('t.created DESC')
			->queryAll();

		foreach ($rows as $k => $row) {
			// $rows[$k]['created'] = date('Y-m-d', $row['created']);
		}

		Export::csv($rows, $headers, 'customer-retention');
	}

	public function canChangeOrCancel()
	{
		// restrict cancellations if the # of renewals is < the minimum term
		// if ($this->min_term && $this->user->renewalCount < $this->min_term)
		if ($this->min_term && $this->plan_renewal_count < $this->min_term) {
			// Exeception: If there is a trial, let the user cancel if the trial end date has not passed
			if ($this->trial_end_date && date('Y-m-d') < $this->trial_end_date)
				return true;

			return false;
		}

		return true;
	}

	public function canCancel()
	{
		return $this->canChangeOrCancel();
	}

	public function canChange()
	{
		return $this->canChangeOrCancel();
	}

	public function reactivate()
	{
		return self::processRecurringPayment($this);
	}

	public static function getTrialDuration($trial_sign_up = false)
	{
		if ($trial_sign_up) {
			$trial_duration = Settings::getSettingValue(Settings::SETTING_TRIAL_SIGN_UP_DURATION);
		} else {
			$trial_duration = Settings::getSettingValue(Settings::SETTING_TRIAL_DURATION);
		}

		if (!$trial_duration)
			return static::BASE_TRIAL_DURATION;

		return $trial_duration;
	}

	/**
	 * @return bool|Membership
	 */
	public static function getUserMembership()
	{
		if (Yii::app()->user->isGuest) {
			return false;
		}

		$model = Membership::retrieve(Yii::app()->user->id);

		if (empty($model)) {
			return false;
		}

		return $model;
	}

	public static function checkUserCancelDiscount()
	{
		$model = static::getUserMembership();

		if (!$model)
			return false;

		return (bool)($model->plan_renewal_count >= 6 && !$model->discount_offered);
	}

	public static function getPriceWithDiscount()
	{
		$model = static::getUserMembership();

		if (!$model)
			return false;

		$discount_percent = (int)Settings::getSettingValue(Settings::SETTING_CANCEL_DISCOUNT);
		$discount_coefficient = round((100 - $discount_percent) / 100, 2);

		return round($model->subtotal *= $discount_coefficient, 2);
	}
}
