<?php

/**
 * This is the model class for table "membership_log".
 *
 * The followings are the available columns in table 'membership_log':
 * @property string $id
 * @property string $user_id
 * @property string $data
 * @property integer $status
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class MembershipLog extends PActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'membership_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status', 'numerical', 'integerOnly'=>true),
			array('user_id', 'length', 'max'=>10),
			array('data, created, updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, data, status, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'data' => 'Data',
			'status' => 'Status',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	public function searchCriteria()
	{
		$criteria = parent::searchCriteria();

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('data',$this->data,true);

		return $criteria;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MembershipLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function log($membership)
	{
		$model = new MembershipLog;
		$model->user_id = $membership->user_id;
		$model->data = serialize($membership->attributes);
		$model->status = 1;
		return $model->save();
	}
}
