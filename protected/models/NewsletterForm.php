<?php

class NewsletterForm extends CFormModel
{
	public $first_name;
	public $last_name;
	public $email;
	public $returnUrl;

	public function rules()
	{
		return array(
			// array('first_name, last_name', 'required'),
			array('email', 'required'),
			array('email', 'email'),
			array('returnUrl', 'safe'),
		);
	}

	public function init()
	{
		$this->returnUrl = $_SERVER['REQUEST_URI'];
	}

	public function save()
	{
		$account_id = Yii::app()->params['emma']['account_id'];
		$public_key = Yii::app()->params['emma']['public_key'];
		$private_key = Yii::app()->params['emma']['private_key'];

		$emma = new Emma($account_id, $public_key, $private_key);

		$member = array();
		$member['email'] = $this->email;
		$member['fields'] = array('first_name'=>$this->first_name, 'last_name'=>$this->last_name);
		$response = $emma->membersAddSingle($member);
		$response = json_decode($response);
					
		// $response = $mailchimp->listSubscribe($list_id, $this->email, array(
		// 	'FNAME'=>$this->first_name,
		// 	'LNAME'=>$this->last_name,
		// ));//, null, null, true);

		// if (!$response)
		// 	$this->addError('form', $mailchimp->errorMessage);

		return ($response->status == 'a');
	}
} 
