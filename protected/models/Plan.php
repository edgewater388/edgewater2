<?php

/**
 * This is the model class for table "plan".
 *
 * The followings are the available columns in table 'plan':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $interval
 * @property double $amount
 * @property double $old_amount
 * @property integer $min_term
 * @property string $promo_start_date
 * @property string $promo_end_date
 * @property string $slug
 * @property string $image_fid
 * @property integer $is_special
 * @property integer $is_auto_renew
 *
 * The followings are the available model relations:
 * @property Files $imageF
 */
class Plan extends BaseRecord
{
	const MONTHLY_INTERVAL = 'monthly';
	const DAILY_INTERVAL = 'daily';
	
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'plan';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
			['slug, title, interval, is_auto_renew, amount', 'required'],
            ['min_term', 'numerical', 'integerOnly'=> true],
			['is_special, is_auto_renew, is_default, is_main_default, status', 'boolean'],
			['is_special, is_auto_renew, is_default, is_main_default', 'default', 'value' => false],
            ['amount, old_amount', 'numerical'],
            ['title, interval, slug', 'length', 'max'=>255],
			['slug', 'unique'],
            ['image_fid', 'length', 'max' => 11],
            ['description, promo_start_date, promo_end_date, created, updated', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, title, description, interval, amount, old_amount, min_term, promo_start_date, promo_end_date, slug, image_fid, is_special, is_default, is_main_default, is_auto_renew, created, updated', 'safe', 'on'=>'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'imageF' => [self::BELONGS_TO, 'Files', 'image_fid'],
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'interval' => 'Interval',
            'amount' => 'Amount',
            'old_amount' => 'Old Amount',
            'min_term' => 'Min Term',
            'promo_start_date' => 'Promo Start Date',
            'promo_end_date' => 'Promo End Date',
            'slug' => 'Url',
            'image_fid' => 'Image Fid',
            'is_special' => 'Is Special',
            'is_auto_renew' => 'Is Auto Renew',
			'is_default' => 'Is Default Plan',
            'is_main_default' => 'Is Main Default Plan',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($pageSize=25)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('interval',$this->interval,true);
        $criteria->compare('amount',$this->amount);
        $criteria->compare('old_amount',$this->old_amount);
        $criteria->compare('min_term',$this->min_term);
        $criteria->compare('promo_start_date',$this->promo_start_date,true);
        $criteria->compare('promo_end_date',$this->promo_end_date,true);
        $criteria->compare('slug',$this->slug,true);
        $criteria->compare('image_fid',$this->image_fid,true);
        $criteria->compare('is_special',$this->is_special);
        $criteria->compare('is_auto_renew',$this->is_auto_renew);
		$criteria->compare('is_default',$this->is_special);
        $criteria->compare('is_main_default',$this->is_auto_renew);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Plan the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
	
	public static function getPlan($id)
	{
		return static::model()->findByPk($id);
	}

	public static function getDefaultPlan()
	{
		return static::model()->findByAttributes(['is_main_default' => true]);
	}

	public static function getTrialPrice()
	{
		return Settings::getSettingValue(Settings::SETTING_TRIAL_SIGN_UP_PRICE);
	}

	public static function getDefaultPlans()
	{
		return static::model()->findAllByAttributes(['is_default' => true]);
	}

	public static function getSpecialOfferPlan()
	{
		return static::model()->findAllByAttributes(['is_special' => true]);
	}

	public static function getDailyPlan()
	{
		return static::model()->findByAttributes(['interval' => static::DAILY_INTERVAL]);
	}
	
	public static function getIntervals()
	{
		return [
			static::DAILY_INTERVAL => 'Daily',
			static::MONTHLY_INTERVAL => 'Monthly'
		];
	}

	public static function getAllPlanOptions()
	{
		$plans = static::model()->findAll();
		$list = [];
		foreach ($plans as $plan) {
			if ($plan->is_special) {
				$list[$plan['id']] = $plan['title'] . ' $' . $plan['amount'] . ' (Promo)';
			} else {
				$list[$plan['id']] = $plan['title'] . ' $' . $plan['amount'];
			}
		}	

		return $list;
	}

	public static function getList()
	{
		$plans = static::model()->findAll();
		$list = [];
		foreach ($plans as $plan)
			$list[$plan['id']] = $plan['title'];

		return $list;
	}
	
	public function getThumbUrl()
	{
		return ($this->imageF) ? $this->imageF->thumb('plan') : null;
	}
}
