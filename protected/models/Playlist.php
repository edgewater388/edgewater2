<?php

/**
 * This is the model class for table "playlist".
 *
 * The followings are the available columns in table 'playlist':
 * @property string $id
 * @property string $user_id
 * @property string $title
 * @property string $length
 * @property integer $status
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Video[] $videos
 * @property Users[] $users
 */
class Playlist extends BaseRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'playlist';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return CMap::mergeArray(parent::rules(), array(
			array('status, strength, cardio, restorative, is_public, is_featured, weight', 'numerical', 'integerOnly'=>true),
			array('user_id, length', 'length', 'max'=>10),
			array('title', 'length', 'max'=>255),
			array('created, updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, title, strength, cardio, restorative, length, is_public, is_featured, weight, status, created, updated', 'safe', 'on'=>'search'),
		));
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'videos' => array(self::MANY_MANY, 'Video', 'playlist_video(playlist_id, video_id)', 'scopes'=>array('playlistOrder')),
			'playlist_videos' => array(self::HAS_MANY, 'PlaylistVideo', 'playlist_id', 'order'=>'playlist_videos.weight ASC'),
			'users' => array(self::MANY_MANY, 'Users', 'user_playlist(playlist_id, user_id)'),

			'user_ordered_videos' => array(self::MANY_MANY, 'Video', 'playlist_video(playlist_id, video_id)', 'order'=>'videos_videos.weight ASC'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'title' => 'Title',
			'length' => 'Length',
			'status' => 'Status',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	public function searchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = parent::searchCriteria();

		$criteria->compare('user_id',$this->user_id,false);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('length',$this->length,false);
		$criteria->compare('strength',$this->strength);
		$criteria->compare('cardio',$this->cardio);
		$criteria->compare('restorative',$this->restorative);
		$criteria->compare('is_public',$this->is_public,false);
		$criteria->compare('is_featured',$this->is_featured,false);

		return $criteria;
	}

	public function beforeSave()
	{
		$valid = parent::beforeSave();

		$this->length = 0;

		foreach ($this->videos as $video)
		{
			$this->length += $video->length;
		}

		if ($this->is_public == null)
			$this->is_public = 0;

		if ($this->is_featured == null)
			$this->is_featured = 0;

		return $valid;
	}

	public function defaultScope()
	{
		$scope = parent::defaultScope();

		if ($this->scenario == 'search')
		{
			$t = $this->getTableAlias(false, false);
			$scope['order'] = $t.'.weight ASC, '.$t.'.title ASC';
		}

		return $scope;
	}

	public function scopes()
	{
		return CMap::mergeArray(parent::scopes(), array(
			'isPublic'=>array(
				'condition'=>'t.is_public = 1',
			),
			'isFeatured'=>array(
				'condition'=>'t.is_featured = 1',
			),
			'ordered'=>array(
				'order'=>'t.weight ASC, t.title ASC',
			),
			'isUserSubscribed'=>array(
				'join'=>'INNER JOIN user_playlist ON user_playlist.playlist_id = t.id',
				'condition'=>'user_playlist.user_id = :user_id',
				'params'=>array(
					':user_id'=>Yii::app()->user->id,
				),
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Playlist the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getTime()
	{
		if (!$this->length)
			return '0:00';

		$minutes = $this->getMinutes();
		$seconds = $this->getSeconds();

		return $minutes.':'.$seconds;
	}

	public function getMinutes()
	{
		return floor($this->length / 1000 / 60);
	}

	public function getSeconds()
	{
		$seconds = floor(($this->length / 1000) - ($this->getMinutes() * 60));
		return str_pad($seconds, 2, '0', STR_PAD_LEFT);
	}

	public static function getApiColumns()
	{
		return array(
			'id',
			'title',
			'length',
			'time',
			'strength',
			'cardio',
			'restorative',
			// 'type',
			// 'typeName',
			// 'level',
			// 'levelName',
			'is_public',
			'is_featured',
			'weight',
			'videos'=>Video::getApiColumns()
		);
	}

	public function getThumbUrl()
	{
		$url = isset($this->videos[0]->meta->videoStillURL) ? $this->videos[0]->meta->videoStillURL : self::getDefaultThumbUrl();
		return str_ireplace('http://', '//', $url);
	}

	public function getStillUrl()
	{
		$url = isset($this->videos[0]->meta->videoStillURL) ? $this->videos[0]->meta->videoStillURL : self::getDefaultThumbUrl();
		return str_ireplace('http://', '//', $url);
	}
	
	public function getIsNew()
	{
		return isset($this->videos[0]) && ($this->videos[0]->is_new) ? $this->videos[0]->is_new : false;
	}

	public static function getDefaultThumbUrl()
	{
		return Yii::app()->params['defaultThumbUrl'];
	}

	// public function getTypeName()
	// {
	// 	$types = Video::getTypes();
	// 	return isset($types[$this->type]) ? $types[$this->type] : null;
	// }

	// public function getLevelName()
	// {
	// 	$levels = Video::getLevels();
	// 	return isset($levels[$this->level]) ? $levels[$this->level] : null;
	// }

	public static function getFeaturedPlaylists()
	{
		return self::model()->isPublic()->isFeatured()->ordered()->findAll();
	}

	public static function getMyPlaylists()
	{
		return self::model()->byUser()->findAll();
	}

	public static function getSubscribedPlaylists()
	{
		return self::model()->isUserSubscribed()->findAll();
	}

	// public function getLevelWord()
	// {
	// 	if ($this->level === '1')
	// 		return 'one';
	// 	if ($this->level === '2')
	// 		return 'two';
	// 	if ($this->level === '3')
	// 		return 'three';
	// 	return null;
	// }

	public static function reorder($id, $order=array())
	{
		if (!$playlist = self::model()->findByPk($id))
			return false;

		$video_ids = array();
		foreach ($playlist->videos as $video)
			$video_ids[] = $video->id;

		$ordered_ids = array_unique(explode(',', $order));
		$unordered_ids = array_diff($video_ids, $ordered_ids);
		$new_order = array_merge($ordered_ids, $unordered_ids);
		$new_order = array_intersect($new_order, $video_ids);
		
		// update the users ordering of the playlist
		$success = UserPlaylist::reorder(Yii::app()->user->id, $id, $new_order);

		if (Yii::app()->user->id == $playlist->user_id)
		{
			// update the initial playlist order if this is the users playlist
			foreach ($playlist->playlist_videos as $playlist_video)
			{
				$k = array_search($playlist_video->video_id, $new_order);
				$playlist_video->weight = $k;
				$playlist_video->save();
			}
		}

		return $success;
	}
}
