<?php

/**
 * This is the model class for table "playlist_video".
 *
 * The followings are the available columns in table 'playlist_video':
 * @property string $playlist_id
 * @property string $video_id
 * @property integer $weight
 */
class PlaylistVideo extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'playlist_video';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('playlist_id, video_id', 'required'),
			array('weight', 'numerical', 'integerOnly'=>true),
			array('playlist_id, video_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('playlist_id, video_id, weight', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'playlist' => array(self::BELONGS_TO, 'Playlist', 'playlist_id'),
			'video' => array(self::BELONGS_TO, 'Video', 'video_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'playlist_id' => 'Playlist',
			'video_id' => 'Video',
			'weight' => 'Weight',
		);
	}

	public function beforeSave()
	{
		$valid = parent::beforeSave();

		$valid &= $this->checkAccess($this->playlist_id, $this->video_id);

		if ($this->weight === null)
		{
			$playlist_id = $this->playlist_id;

			$count = Yii::app()->db->createCommand('SELECT count(*) FROM playlist_video WHERE playlist_id = :playlist_id')
				->bindParam(':playlist_id', $playlist_id)
				->queryScalar();

			$this->weight = $count;
		}

		return $valid;
	}

	public function beforeDelete()
	{
		$valid = parent::beforeDelete();

		if (!$this->checkAccess($this->playlist_id, $this->video_id))
		{
			$this->addError('access', 'Access denied');
			$valid = false;
		}

		return $valid;
	}

	public function afterSave()
	{
		parent::afterSave();

		$this->playlist->save();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PlaylistVideo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function add($playlist_id, $video_id)
	{
		if (!$model = self::model()->findByPk(array('playlist_id'=>$playlist_id, 'video_id'=>$video_id)))
		{
			$model = new PlaylistVideo;
			$model->playlist_id = $playlist_id;
			$model->video_id = $video_id;
		}

		return $model->save();
	}

	public static function remove($playlist_id, $video_id)
	{
		if ($model = self::model()->findByPk(array('playlist_id'=>$playlist_id, 'video_id'=>$video_id)))
			return $model->delete();
		return true;
	}

	public function checkAccess($playlist_id, $video_id)
	{
		$playlist = Playlist::model()->findByPk($playlist_id);
		$video = Video::model()->findByPk($video_id);

		if (Yii::app()->user->isGuest)
			return false;

		if (!$playlist || !$video)
			return false;

		if ($playlist->user_id == Yii::app()->user->id || Yii::app()->user->checkAccess('accessBackend'))
			return true;

		return false;
	}
}
