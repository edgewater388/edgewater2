<?php

/**
 * This is the model class for table "promo_code".
 *
 * The followings are the available columns in table 'promo_code':
 * @property string $id
 * @property string $code
 * @property string $description
 * @property string $discount_type
 * @property string $discount_amount
 * @property integer $discount_length
 * @property string $discount_interval
 * @property string $start_date
 * @property string $end_date
 * @property string $quantity
 * @property string $is_reusable
 * @property string $plans
 * @property integer $status
 * @property string $created
 * @property string $updated
 */
class PromoCode extends BaseRecord
{
	public $plans_list = array();

	public function init()
	{
		parent::init();

		$this->discount_interval = 'month';
		$this->is_reusable = 0;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'promo_code';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('discount_length, status', 'numerical', 'integerOnly'=>true),
			array('code, discount_type', 'length', 'max'=>32),
			array('discount_amount', 'length', 'max'=>16),
			array('discount_interval', 'length', 'max'=>8),
			array('quantity', 'length', 'max'=>10),
			array('description, start_date, end_date, plans, plans_list, created, updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, code, description, discount_type, discount_amount, discount_length, discount_interval, start_date, end_date, quantity, plans, plans_list, status, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'redemptionCount' => array(self::STAT, 'PromoCodeRedemptionLog', 'promo_code_id', 'select'=>'count(*)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code' => 'Promo Code',
			'description' => 'Description',
			'discount_type' => 'Discount Type',
			'discount_amount' => 'Discount Amount',
			'discount_length' => 'Discount Length',
			'discount_interval' => 'Discount Interval',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'quantity' => 'Quantity',
			'plans' => 'Plans',
			'status' => 'Status',
			'created' => 'Created',
			'updated' => 'Updated',

			'plans_list'=>'Plans',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($pageSize=25)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = $this->searchCriteria();

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>$pageSize,
				'validateCurrentPage'=>false,
			),
		));
	}

	public function searchCriteria()
	{
		$criteria = parent::searchCriteria();

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.code',$this->code,true);
		$criteria->compare('t.description',$this->description,true);
		$criteria->compare('t.discount_type',$this->discount_type,true);
		$criteria->compare('t.discount_amount',$this->discount_amount,true);
		$criteria->compare('t.discount_length',$this->discount_length);
		$criteria->compare('t.discount_interval',$this->discount_interval,true);
		$criteria->compare('t.start_date',$this->start_date,true);
		$criteria->compare('t.end_date',$this->end_date,true);
		$criteria->compare('t.quantity',$this->quantity);
		$criteria->compare('t.status',$this->status);
		$criteria->compare('t.created',$this->created,true);
		$criteria->compare('t.updated',$this->updated,true);

		return $criteria;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PromoCode the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeValidate()
	{
		$valid = parent::beforeValidate();

		if (!$this->code)
			$this->code = CString::randomHash();

		// code should be unique
		if (!$this->isUniqueCode())
			$this->addError('code', 'Promo code must be unique');

		// field cannot be saved as an empty string
		if (!is_numeric($this->discount_length))
			$this->discount_length = null;

		// field cannot be saved as an empty string
		if (!is_numeric($this->quantity))
			$this->quantity = null;

		return $valid;
	}

	public function beforeSave()
	{
		$valid = parent::beforeSave();

		if ($this->plans_list)
			$this->plans = json_encode($this->plans_list);

		return $valid;
	}

	public function afterFind()
	{
		parent::afterFind();

		$this->plans_list = json_decode($this->plans);
	}

	public static function getDiscountTypes()
	{
		return array(
			'percent'=>'Percent',
			'fixed_amount'=>'Dollars (Fixed Amount)',
		);
	}

	public static function getDiscountIntervals()
	{
		return array(
			'day'=>'Day(s)',
			'month'=>'Month(s)',
		);
	}

	public static function getList()
	{
		return CHtml::listData(PromoCode::model()->findAll(), 'id', 'code');
	}

	public static function retrieveById($id)
	{
		return PromoCode::model()->findByPk($id);
	}

	public static function retrieve($code)
	{
		return PromoCode::model()->byAttribute('code',$code)->find();
	}

	public static function verify($code, $plan_id=null)
	{
		// The code must at least exist to be valid
		if (!$model = self::retrieve($code))
			return false;

		// Today's date cannot be before the promo start date
		if ($model->start_date && $model->start_date > date('Y-m-d'))
			return false;

		// Today's date cannot be past the promo end date
		if ($model->end_date && $model->end_date < date('Y-m-d'))
			return false;

		// Promo codes cannot be used more than once
		if (!$model->is_reusable && PromoCodeRedemptionLog::hasBeenRedeemed(Yii::app()->user->id, $model->id))
			return false;

		if ($plan_id && !empty($model->plans_list) && !in_array($plan_id, $model->plans_list))
			return false;

		// If there is a max quantity, check if the limit has been reached
		if (!$model->quantityAvailable)
			return false;

		return $model;
	}

	public function calculateAmount($subtotal)
	{
		$amount = $subtotal;

		// percent
		if ($this->discount_type == 'percent')
			$amount = $subtotal - ($subtotal * $this->discount_amount / 100);
		// fixed amount
		else if ($this->discount_type == 'fixed_amount')
			$amount = $subtotal - $this->discount_amount;

		return max(0, $amount);
	}

	public function calculateEndDate($instantRedemption=false)
	{
		if ($this->discount_length)
		{
			// if being redeemed instantly, subtract 1 from the discount length (ie. if it's a 
			// 1 month free promotion and redeemed instantly, it should expire right away).
			if ($instantRedemption)
				$this->discount_length--;
			
			return date('Y-m-d', strtotime('+'.$this->discount_length.' '.$this->discount_interval.' +1 day'));
		}
		return null;
	}

	public function getDisplayTitle()
	{
		return $this->code;
	}

	public function getQuantityAvailable()
	{
		if ($this->quantity === null)
			return true;

		$redeemed = PromoCodeRedemptionLog::getQuantityRedeemed($this->id);
		return max(0, $this->quantity - $redeemed);
	}

	public function isUniqueCode()
	{
		if ($this->id)
			return !(self::model()->byAttribute('code', $this->code)->exists(array('condition'=>'id != :id', 'params'=>array(':id'=>$this->id))));
		return !(self::model()->byAttribute('code', $this->code)->exists());
	}

	public function getTransactionCount()
	{
		return Transaction::model()->byAttribute('promo_code', $this->code)->count();
	}

	public function exportReport()
	{
		$criteria = $this->searchCriteria();

		$columns = array(
			't.id' => 'ID',
			't.code' => 'User',
			't.description' => 'Description',
			't.discount_type' => 'Discount Type',
			't.discount_amount' => 'Discount Amount',
			't.discount_length' => 'Discount Length',
			't.discount_interval' => 'Discount Interval',
			't.start_date' => 'Start Date',
			't.end_date' => 'End Date',
			't.quantity' => 'Quantity',
			't.is_reusable' => 'Is Reusable',
			't.plans' => 'Plan IDs',
			'count(log.id)'=>'Redemption Count',
			'count(transaction.id)'=>'Transaction Count',
			't.status' => 'Status',
			't.created' => 'Created',
			't.updated' => 'Updated',
		);

		$headers = array();
		$select = array();
		foreach ($columns as $column=>$label)
		{
			$headers[] = $label;
			$select[] = $column;
		}

		$rows = Yii::app()->db->createCommand()
			->select(implode(', ', $select))
			->from($this->tableName().' t')
			->leftJoin('promo_code_redemption_log log', 't.id = log.promo_code_id')
			->leftJoin('transaction', 't.code = transaction.promo_code')
			->where($criteria->condition, $criteria->params)
			->order('t.created ASC')
			->group('t.id')
			->queryAll();

		foreach($rows as $k=>$row)
		{
			// $rows[$k]['created'] = date('Y-m-d', $row['created']);
		}

		Export::csv($rows, $headers, 'promo-codes-report');
	}
}
