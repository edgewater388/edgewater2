<?php

/**
 * This is the model class for table "promo_code_redemption_log".
 *
 * The followings are the available columns in table 'promo_code_redemption_log':
 * @property string $id
 * @property string $user_id
 * @property string $promo_code_id
 * @property string $code
 * @property string $data
 * @property integer $status
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property PromoCode $promoCode
 * @property Users $user
 */
class PromoCodeRedemptionLog extends BaseRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'promo_code_redemption_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status', 'numerical', 'integerOnly'=>true),
			array('user_id, promo_code_id', 'length', 'max'=>10),
			array('code', 'length', 'max'=>32),
			array('data, created, updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, promo_code_id, code, data, status, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'promoCode' => array(self::BELONGS_TO, 'PromoCode', 'promo_code_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'promo_code_id' => 'Promo Code',
			'code' => 'Code',
			'data' => 'Data',
			'status' => 'Status',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($pageSize = 25)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('promo_code_id',$this->promo_code_id,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PromoCodeRedemptionLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function log($user_id, $promo_code)
	{
		$model = new PromoCodeRedemptionLog;
		$model->user_id = $user_id;
		$model->promo_code_id = isset($promo_code->id) ? $promo_code->id : null;
		$model->code = isset($promo_code->code) ? $promo_code->code : null;
		$model->data = isset($promo_code->attributes) ? json_encode($promo_code->attributes) : null;
		$model->status = 1;
		return $model->save();
	}

	public static function hasBeenRedeemed($user_id, $promo_code_id)
	{
		return self::model()->byAttribute('user_id', $user_id)->byAttribute('promo_code_id', $promo_code_id)->exists();
	}

	public static function getQuantityRedeemed($promo_code_id)
	{
		return Yii::app()->db->createCommand('SELECT count(*) FROM promo_code_redemption_log WHERE promo_code_id = :id')->bindParam(':id', $promo_code_id)->queryScalar();
	}

	// public function getRedemptionCounts()
	// {
	// 	$dataReader = Yii::app()->db->createCommand('SELECT pc.id, pc.code, count(log.id) FROM promo_code pc LEFT JOIN promo_code_redemption_log log ON pc.id = log.code GROUP BY pc.id')->query();

	// 	$counts = array();
	// 	while(($record=$dataReader->read()) !== false)
	// 	{
	// 		$counts[$record['id']] = $record
	// 	}

	// 	return $counts;
	// }
}
