<?php

/**
 * This is the model class for table "referral_bank".
 *
 * The followings are the available columns in table 'referral_bank':
 * @property string $id
 * @property string $user_id
 * @property string $referred_user_id
 * @property string $referral_invite_id
 * @property string $amount
 * @property string $released
 * @property string $redeemed
 * @property integer $status
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property ReferralInvite $referralInvite
 * @property Users $user
 * @property Users $referredUser
 */
class ReferralBank extends BaseRecord
{
	public $invite_type;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'referral_bank';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('user_id, referred_user_id, referral_invite_id', 'length', 'max'=>10),
			array('amount', 'length', 'max'=>8),
			array('released, redeemed, created, updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, referred_user_id, referral_invite_id, amount, released, redeemed, status, created, updated', 'safe', 'on'=>'search'),

			// custom search
			array('invite_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'referralInvite' => array(self::BELONGS_TO, 'ReferralInvite', 'referral_invite_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'referredUser' => array(self::BELONGS_TO, 'Users', 'referred_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'referred_user_id' => 'Referred User',
			'referral_invite_id' => 'Referral Invite',
			'amount' => 'Reward Amount',
			'released' => 'Date Released',
			'redeemed' => 'Date Redeemed',
			'status' => 'Status',
			'created' => 'Created',
			'updated' => 'Updated',

			'user.profile.name' => 'Referred By',
			'referredUser.profile.name' => 'New Account',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($pageSize = 25)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = $this->searchCriteria();

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchCriteria()
	{
		$criteria = parent::searchCriteria();

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.user_id',$this->user_id);
		$criteria->compare('t.referred_user_id',$this->referred_user_id);
		$criteria->compare('t.referral_invite_id',$this->referral_invite_id);
		$criteria->compare('t.amount',$this->amount,true);
		$criteria->compare('t.released',$this->released,true);
		$criteria->compare('t.redeemed',$this->redeemed,true);
		$criteria->compare('t.status',$this->status);
		$criteria->compare('t.created',$this->created,true);
		$criteria->compare('t.updated',$this->updated,true);

		if ($this->invite_type == 'email')
			$criteria->addCondition('t.referral_invite_id IS NOT NULL');
		else if ($this->invite_type == 'link')
			$criteria->addCondition('t.referral_invite_id IS NULL');

		return $criteria;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReferralBank the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function add($referral_invite_id, $user_id, $referred_user_id)
	{
		$model = new ReferralBank('create');
		$model->user_id = $user_id;
		$model->referred_user_id = $referred_user_id;
		$model->referral_invite_id = $referral_invite_id;
		$model->amount = self::getRewardAmount();
		$model->status = 1;
		return $model->save();
	}

	public static function getRewardAmount()
	{
		if ($value = Content::getValue('refer_friend_reward_amount'))
			return $value;
		return self::getDefaultRewardAmount();
	}

	public static function getDefaultRewardAmount()
	{
		return 10;
	}

	public static function releaseRewards($user_id)
	{
		$models = ReferralBank::model()->byAttribute('referred_user_id', $user_id)->findAll(array('condition'=>'released IS NULL'));
		foreach ($models as $model)
		{
			$model->released = date('Y-m-d', strtotime('+2 weeks'));
			$model->save();

			// make sure the user still exists
			if ($model->user)
			{
				$message = new MailMessage;
				$message->view = 'referral_reward';
				$message->subject = 'Congratulations! You have earned $'.$model->amount.'.';
				$message->tagline = 'Congratulations!';
				$message->addTo($model->user->email, $model->user->profile->name);
				$message->setBody(array(
					'model'=>$model,
				));

				Yii::app()->mail->send($message);
			}
		}
	}

	public static function getAvailableAwards($user_id)
	{
		return ReferralBank::model()->byAttribute('user_id', $user_id)->findAll(array('condition'=>'released IS NOT NULL AND released <= "'.date('Y-m-d').'" AND redeemed IS NULL'));
	}

	public static function cancelPendingAwards($referred_user_id)
	{
		$models = ReferralBank::model()->byAttribute('referred_user_id', $referred_user_id)->findAll(array('condition'=>'redeemed IS NULL'));

		foreach ($models as $model)
		{
			$model->released = null;
			$model->save();
		}
	}

	public function doRedemption()
	{
		$this->redeemed = date('Y-m-d');
		$this->save();
	}

	public function export()
	{
		$criteria = $this->searchCriteria();

		$columns = array(
			't.id' => 'ID',
			'CONCAT(new_account.first_name," ",new_account.last_name)' => 'New Account',
			'CONCAT(referred_by.first_name," ",referred_by.last_name)' => 'Referred By',
			'amount' => 'Reward Amount',
			'released' => 'Date Released',
			'redeemed' => 'Date Redeemed',
			't.status' => 'Status',
			't.created' => 'Created',
			't.updated' => 'Updated',
		);

		$headers = array();
		$select = array();
		foreach ($columns as $column=>$label)
		{
			$headers[] = $label;
			$select[] = $column;
		}

		$rows = Yii::app()->db->createCommand()
			->select(implode(', ', $select))
			->from($this->tableName().' t')
			->join('profile referred_by', 'referred_by.user_id=t.user_id')
			->join('profile new_account', 'new_account.user_id=t.referred_user_id')
			->where($criteria->condition, $criteria->params)
			->order('t.created DESC')
			->queryAll();

		foreach($rows as $k=>$row)
		{
			// $rows[$k]['created'] = date('Y-m-d', $row['created']);
		}

		Export::csv($rows, $headers, 'referral-rewards');
	}

	public function getInviteType()
	{
		$types = self::getInviteTypes();
		$type = $this->referral_invite_id ? 'email' : 'link';
		return $types[$type];
	}

	public static function getInviteTypes()
	{
		return array(
			'email'=>'Email',
			'link'=>'Invite Link',
		);
	}
}
