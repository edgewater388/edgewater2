<?php

/**
 * This is the model class for table "referral_invite".
 *
 * The followings are the available columns in table 'referral_invite':
 * @property string $id
 * @property string $user_id
 * @property string $email
 * @property string $expires
 * @property integer $status
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property ReferralBank[] $referralBanks
 * @property Users $user
 */
class ReferralInvite extends BaseRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'referral_invite';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email', 'email'),
			array('email', 'required', 'on'=>'create'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('user_id', 'length', 'max'=>10),
			array('email', 'length', 'max'=>255),
			array('expires, created, updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, email, expires, status, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'referralBanks' => array(self::HAS_MANY, 'ReferralBank', 'referral_invite_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'email' => 'Email',
			'expires' => 'Expires',
			'status' => 'Status',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($pageSize = 25)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = $this->searchCriteria();

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchCriteria()
	{
		$criteria = parent::searchCriteria();

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.user_id',$this->user_id);
		$criteria->compare('t.email',$this->email,true);
		$criteria->compare('t.expires',$this->expires);
		$criteria->compare('t.status',$this->status);
		$criteria->compare('t.created',$this->created,true);
		$criteria->compare('t.updated',$this->updated,true);

		return $criteria;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReferralInvite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function scopes()
	{
		return CMap::mergeArray(parent::scopes(), array(
			'notExpired'=>array(
				'condition'=>'(t.expires IS NULL OR t.expires >= "'.date('Y-m-d').'")',
			),
			'fifo'=>array(
				'order'=>'t.created ASC',
			),
		));
	}

	public static function invite($email)
	{
		$model = new ReferralInvite('create');
		$model->user_id = Yii::app()->user->id;
		$model->email = $email;
		$model->expires = date('Y-m-d', strtotime('+30 days'));
		$model->status = 1;
		
		if ($model->save())
		{
			$name = isset(Yii::app()->user->profile->first_name) ? Yii::app()->user->profile->first_name : 'Your friend';
			$message = new MailMessage;
			$message->view = 'referral_invite';
			// $message->subject = $name.' has invited you to check out '.Yii::app()->name.'!';
			$message->subject = 'You\'ve been invited to check out '.Yii::app()->name.'!';
			$message->addTo($email, $email);
			$message->setBody(array(
				'referralInvite'=>$model,
			));

			Yii::app()->mail->send($message);
		}

		return $model;
	}

	public static function awardReferral($email, $referred_user_id=null)
	{
		if ($model = self::model()->byAttribute('email', $email)->notExpired()->fifo()->find())
			ReferralBank::add($model->id, $model->user_id, $referred_user_id);
	}

	public function export()
	{
		$criteria = $this->searchCriteria();

		$columns = array(
			't.id' => 'ID',
			'CONCAT(referred_by.first_name," ",referred_by.last_name)' => 'Referred By',
			'email' => 'Email',
			'expires' => 'Expires',
			't.created' => 'Created',
			't.updated' => 'Updated',
		);

		$headers = array();
		$select = array();
		foreach ($columns as $column=>$label)
		{
			$headers[] = $label;
			$select[] = $column;
		}

		$rows = Yii::app()->db->createCommand()
			->select(implode(', ', $select))
			->from($this->tableName().' t')
			->join('profile referred_by', 'referred_by.user_id=t.user_id')
			->where($criteria->condition, $criteria->params)
			->order('t.created DESC')
			->queryAll();

		foreach($rows as $k=>$row)
		{
			// $rows[$k]['created'] = date('Y-m-d', $row['created']);
		}

		Export::csv($rows, $headers, 'referral-invites');
	}
}
