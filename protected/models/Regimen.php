<?php

/**
 * This is the model class for table "regimen".
 * The followings are the available columns in table 'regimen':
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property integer $status
 * @property string $created
 * @property string $updated
 * The followings are the available model relations:
 * @property RegimenVideo[] $regimenVideos
 */
class Regimen extends BaseRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'regimen';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			['weight, status', 'numerical', 'integerOnly' => true],
			['title', 'length', 'max' => 255],
			['description, summary, created, updated', 'safe'],
			['image_fid', 'length', 'max' => 10],
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			['id, title, description, status, created, updated', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return [
			'regimenVideos' => [self::HAS_MANY, 'RegimenVideo', 'regimen_id', 'order' => 'regimenVideos.day ASC, regimenVideos.weight ASC'],
			'imageF'        => [self::BELONGS_TO, 'Files', 'image_fid'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id'          => 'ID',
			'title'       => 'Title',
			'description' => 'Description',
			'status'      => 'Status',
			'created'     => 'Created',
			'updated'     => 'Updated',
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchCriteria()
	{
		$criteria = parent::searchCriteria();

		$criteria->compare('title', $this->title, true);
		$criteria->compare('description', $this->description, true);

		if ($this->q)
			$criteria->compare('title', $this->q, true);

		return $criteria;
	}

	public function defaultScope()
	{
		$scope = parent::defaultScope();

		$t = $this->getTableAlias(false, false);
		$scope['order'] = $t . '.weight ASC';

		return $scope;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className active record class name.
	 *
	 * @return Regimen the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getVideos()
	{
		return $this->getVideosByWeek();
	}

	public function getVideosByDay()
	{
		$videos = [];
		$max_day = 0;

		foreach ($this->regimenVideos as $regimenVideo) {
			// if a video is not published, it should not added to the regimen
			if ($regimenVideo->video)
				$videos[$regimenVideo->day][] = $regimenVideo->video;

			$max_day = max($max_day, $regimenVideo->day);
		}

		ksort($videos);

		return $videos;
	}

	public function getVideosByWeek()
	{
		$videos = $this->getVideosByDay();

		end($videos);
		$max_day = key($videos);
		reset($videos);

		$weeks = ceil($max_day / 7);

		for ($i = 1; $i < ($weeks * 7) + 1; $i++) {
			if (!array_key_exists($i, $videos))
				$videos[$i][] = null;
		}

		ksort($videos);

		$byWeek = [];
		foreach ($videos as $day => $video)
			$byWeek[ceil($day / 7)][$day] = $video;

		return $byWeek;
	}

	public function hasCompleted()
	{
		$userPlayedVideos = VideoPlayLog::userPlayedVideos(Yii::app()->user->id);

		foreach ($this->regimenVideos as $regimenVideo)
			if (!in_array($regimenVideo->video_id, $userPlayedVideos))
				return false;

		return true;
	}

	public function getLength()
	{
		return Yii::t('test', '{n} week|{n} weeks', count($this->videos));
	}

	public static function getFeaturedRegimens($limit = null)
	{
		$criteria = new CDbCriteria([
			'limit' => $limit,
		]);

		return static::model()->findAll($criteria);
	}

	public static function getRegimenCount()
	{
		$sql = "SELECT COUNT(*) FROM regimen WHERE status = 1";

		return Yii::app()->db->createCommand($sql)->queryScalar();
	}
}
