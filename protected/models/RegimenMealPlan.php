<?php

/**
 * Class RegimenMealPlan
 *
 * This is the model class for table "regimen_meal_plan".
 *
 * @property int $id
 * @property int $regimen_id
 * @property int $week_num
 * @property string $path_to_file
 *
 * The followings are the available model relations:
 * @property Regimen $regimen
 */

class RegimenMealPlan extends BaseRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'regimen_meal_plan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('week_num', 'numerical', 'integerOnly'=>true),
			array('regimen_id', 'length', 'max'=>10),
			array('path_to_file, created, updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, regimen_id, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'regimen' => array(self::BELONGS_TO, 'Regimen', 'regimen_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'regimen_id' => 'Regimen',
			'week_num' => 'Week Number',
			'path_to_file' => 'Meal Plan PDF',
		);
	}

	public function save($runValidation = true, $attributes = NULL)
	{
		if (empty($this->regimen_id) || $this->regimen_id <= 0)
			return false;

		if (empty($_FILES['RegimenMealPlan']['name']['path_to_file']))
			return false;

		if ($_FILES['RegimenMealPlan']['error']['path_to_file'])
			return $_FILES['error']['path_to_file'];

		$filename = $_FILES['RegimenMealPlan']['name']['path_to_file'];
		$file = $_FILES['RegimenMealPlan']['tmp_name']['path_to_file'];
		$base_path = 'uploads/'.$filename;

		move_uploaded_file($file, __DIR__.'/../../'.$base_path);
		$this->path_to_file = $base_path;

		return parent::save($runValidation = true, $attributes = NULL);
	}

	public function searchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria = parent::searchCriteria();

		if (!empty($this->regimen_id)) {
			$criteria->condition = 'regimen_id = :regimen_id';
			$criteria->params = array(':regimen_id' => $this->regimen_id);
		}

		return $criteria;
	}

	/**
	 * @param int $regimen_id
	 * @param int $week_num
	 * @return bool|string
	 */
	public static function getFilePath($regimen_id, $week_num)
	{
		$meal_plan = self::model()->find('regimen_id = :regimen_id AND week_num = :week_num AND status = 1',
			array(':regimen_id' => $regimen_id, ':week_num' => $week_num));

		if (!is_null($meal_plan)) {
			return $meal_plan->path_to_file;
		}

		return false;
	}

	public function getDisplayTitle()
	{
		return "Regimen ID: {$this->regimen_id} / Week Number: {$this->week_num}";
	}

	public function defaultScope()
	{
		$scope = parent::defaultScope();

		$t = $this->getTableAlias(false, false);
		$scope['order'] = $t.'.regimen_id DESC, '.$t.'.week_num ASC';

		return $scope;
	}
}