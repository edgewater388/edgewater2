<?php

/**
 * This is the model class for table "regimen_video".
 *
 * The followings are the available columns in table 'regimen_video':
 * @property string $id
 * @property string $regimen_id
 * @property string $video_id
 * @property integer $day
 * @property integer $weight
 * @property integer $status
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property Video $video
 * @property Regimen $regimen
 */
class RegimenVideo extends BaseRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'regimen_video';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('day, weight, status', 'numerical', 'integerOnly'=>true),
			array('regimen_id, video_id', 'length', 'max'=>10),
			array('created, updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, regimen_id, video_id, day, weight, status, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'video' => array(self::BELONGS_TO, 'Video', 'video_id'),
			'regimen' => array(self::BELONGS_TO, 'Regimen', 'regimen_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'regimen_id' => 'Regimen',
			'video_id' => 'Video',
			'day' => 'Day',
			'weight' => 'Weight',
			'status' => 'Status',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchCriteria()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = parent::searchCriteria();

		$criteria->compare('regimen_id',$this->regimen_id,true);
		$criteria->compare('video_id',$this->video_id,true);
		$criteria->compare('day',$this->day);
		$criteria->compare('weight',$this->weight);

		return $criteria;
	}

	public function defaultScope()
	{
		$scope = parent::defaultScope();

		$t = $this->getTableAlias(false, false);
		$scope['order'] = $t.'.day ASC, '.$t.'.weight ASC';

		return $scope;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RegimenVideo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDisplayTitle()
	{
		return ($this->video) ? $this->video->title : 'Regimen Video';
	}

	public function getDayTitle()
	{
		return 'Day '.$this->day;
	}

	public static function getMaxDayByRegimenID($regimen_id)
	{
		$criteria = new CDbCriteria;
		$criteria->select = "MAX(day) as day";
		$criteria->condition = 'regimen_id = :regimen_id';
		$criteria->params = array(':regimen_id' => $regimen_id);

		return self::model()->find($criteria)->day;
	}
}
