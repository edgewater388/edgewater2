<?php

/**
 * Class Settings
 *
 * @property $setting
 * @property $value
 * @property $status
 */
class Settings extends BaseRecord
{
	const SETTING_DEMO_ACCESS = 'demo_access';
	const SETTING_STREAM_POPUP_TITLE = 'stream_popup_title';
	const SETTING_STREAM_POPUP_TEXT = 'stream_popup_text';
	const SETTING_TRIAL_DURATION = 'trial_duration';
	const SETTING_TRIAL_SIGN_UP_DURATION = 'trial_sign_up_duration';
	const SETTING_TRIAL_SIGN_UP_PRICE = 'trial_sign_up_price';
	const SETTING_COUNTDOWN_TO = 'countdown_to';
	const SETTING_CANCEL_DISCOUNT = 'cancel_discount';

	public $demo_access;
	public $stream_popup_title;
	public $stream_popup_text;
	public $trial_duration;
	public $trial_sign_up_duration;
	public $trial_sign_up_price;
	public $countdown_to;
	public $cancel_discount;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'settings';
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'demo_access'            => 'Demo Access',
			'stream_popup_title'     => 'Stream Popup Title',
			'stream_popup_text'      => 'Stream Popup Text',
			'trial_duration'         => 'Trial Duration (days)',
			'trial_sign_up_price'    => 'Trial Sign Up Price',
			'trial_sign_up_duration' => 'Trial Sign Up Duration (days)',
			'countdown_to'           => 'Time when countdown ends',
			'cancel_discount'        => 'Discount on subscription cancel (percent)',
		];
	}

	public static function listSettings()
	{
		return [
			static::SETTING_DEMO_ACCESS,
			static::SETTING_STREAM_POPUP_TITLE,
			static::SETTING_STREAM_POPUP_TEXT,
			static::SETTING_TRIAL_DURATION,
			static::SETTING_TRIAL_SIGN_UP_DURATION,
			static::SETTING_TRIAL_SIGN_UP_PRICE,
			static::SETTING_COUNTDOWN_TO,
			static::SETTING_CANCEL_DISCOUNT,
		];
	}

	public static function getSwitchOptions()
	{
		return [
			0 => 'Off',
			1 => 'On',
		];
	}

	public function saveSetting($name, $value)
	{
		if (!in_array($name, static::listSettings())) {
			return false;
		}

		$setting = static::getSetting($name);

		if (empty($setting)) {
			$setting = new static();
			$setting->setting = $name;
			$setting->value = $value;
			$setting->status = 1;
			$setting->save();

			return true;
		}

		$setting->value = $value;
		$setting->save();

		return true;
	}

	public function saveSettings($data)
	{
		if (empty($data) || !is_array($data)) return false;

		foreach ($data as $name => $value) {
			$this->saveSetting($name, $value);
		}

		return true;
	}

	/**
	 * @param string $setting
	 *
	 * @return CActiveRecord
	 */
	public static function getSetting($setting)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'setting = :setting';
		$criteria->params = [':setting' => $setting];

		$result = self::model()->find($criteria);

		return $result;
	}

	/**
	 * @param string $setting
	 *
	 * @return string|null
	 */
	public static function getSettingValue($setting)
	{
		return !empty($result = static::getSetting($setting)) ? $result->value : null;
	}
}