<?php

/**
 * Class Stream
 *
 * This is the model class for table "stream".
 *
 * @property string $id
 * @property datetime $time_start
 * @property datetime $time_end
 * @property smallint unsigned $timer
 * @property string $embed_js
 * @property string $livestream_iframe
 * @property string $countdown_message
 * @property string $ready_message
 * @property string $countdown_background
 * @property string $ready_background
 * @property bool $status
 */
class Stream extends BaseRecord {
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'stream';
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			[ 'status', 'numerical', 'integerOnly' => true ],
			[ 'time_start, time_end, countdown_message, ready_message', 'required' ],
			[ 'countdown_background, ready_background, livestream_iframe, embed_js, timer, youtube_embed_code', 'safe' ],
			[
				'time_start',
				'compare',
				'compareAttribute' => 'time_end',
				'operator'         => '<',
				'message'          => 'Start Time must be less than End Time'
			],
			[
				'time_end',
				'compare',
				'compareAttribute' => 'time_start',
				'operator'         => '>',
				'message'          => 'End Time must be greater than Start Time'
			],
		];
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return [
			'time_start'           => 'Start Time',
			'time_end'             => 'End Time',
			'timer'                => 'Before Stream (minutes)',
			'embed_js'             => 'JW Player Embed js',
			'livestream_iframe'    => 'Iframe For Livestream',
			'countdown_message'    => 'Countdown Message',
			'ready_message'        => 'Ready to start message',
			'countdown_background' => 'Countdown background url',
			'ready_background'     => 'Ready to start background url',
			'youtube_embed_code'   => 'YouTube Embed Code'
		];
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className active record class name.
	 *
	 * @return Regimen the static model class
	 */
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	
	/**
	 * @return string
	 */
	public function getDisplayTitle() {
		$time_start = date( 'M d Y, H:i', strtotime( $this->time_start ) );
		$time_end   = date( 'M d Y, H:i', strtotime( $this->time_end ) );
		
		return "{$this->classLabel}: {$time_start} - {$time_end}";
	}
	
	/**
	 * @return array|mixed|null
	 */
	public static function getStream() {
		$criteria            = new CDbCriteria();
		$criteria->condition = 'time_end > :now AND deleted IS NULL';
		$criteria->params    = [ ':now' => date( 'Y-m-d H:i:s' ) ];
		$criteria->order     = 'time_end ASC, id DESC';
		$criteria->limit     = 1;
		
		return static::model()->find( $criteria );
	}
	
	public function getIsReadyMessageVisible() {
		return $this->status
		       && ( strtotime( 'now' ) < strtotime( $this->time_end ) )
		       && ( strtotime( $this->time_start ) >= strtotime( 'now' ) );
	}
	
	public function getIsCountdownMessageVisible() {
		return $this->status
		       && ( strtotime( 'now' ) < strtotime( $this->time_end ) )
		       && ( strtotime( 'now' ) < strtotime( $this->time_start ) );
	}
	
	public function getIsMessageVisible() {
		return $this->isReadyMessageVisible && $this->isCountdownMessageVisible;
	}
	
	public function getIsVideoVisible() {
		return $this->status && ( strtotime( 'now' ) < strtotime( $this->time_end ) );
	}
	
	public function getIsPlayerVisible() {
		return $this->isMessageVisible || $this->isVideoVisible;
	}
}