<?php

/**
 * This is the model class for table "stripe_customer".
 *
 * The followings are the available columns in table 'stripe_customer':
 * @property string $id
 * @property string $user_id
 * @property string $customer_id
 * @property integer $live_mode
 * @property string $last4
 * @property string $card_type
 * @property string $exp_month
 * @property string $exp_year
 * @property string $fingerprint
 * @property string $name
 * @property string $address_line1
 * @property string $address_line2
 * @property string $address_city
 * @property string $address_state
 * @property string $address_zip
 * @property string $address_country
 * @property string $cvc_check
 * @property string $phone
 * @property string $email
 * @property integer $delinquent
 * @property string $subscription
 * @property string $discount
 * @property integer $account_balance
 * @property integer $terms_accepted
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class StripeCustomer extends ActiveRecord
{
	public $amount;
	public $card_token;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StripeCustomer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stripe_customer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('terms_accepted', 'required', 'requiredValue'=>1, 'message'=>'Terms of Use Agreement must be accepted.', 'on'=>'createupdate'),

			array('name, email, address_line1, address_city, address_country', 'required', 'on'=>'createupdate'),
			array('address_state, address_zip', 'validateFromCountry', 'on'=>'createupdate'),
			array('live_mode, delinquent, account_balance, terms_accepted', 'numerical', 'integerOnly'=>true),
			array('user_id, created, updated', 'length', 'max'=>10),
			array('customer_id, card_type, fingerprint', 'length', 'max'=>32),
			array('last4, exp_year', 'length', 'max'=>4),
			array('exp_month', 'length', 'max'=>2),
			array('name, address_line1, address_line2, address_city, address_state, address_country, email, subscription, discount', 'length', 'max'=>128),
			array('address_zip', 'length', 'max'=>8),
			array('cvc_check', 'length', 'max'=>12),
			array('phone', 'length', 'max'=>16),
			array('amount, card_token', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, customer_id, live_mode, last4, card_type, exp_month, exp_year, fingerprint, name, address_line1, address_line2, address_city, address_state, address_zip, address_country, cvc_check, phone, email, delinquent, subscription, discount, account_balance, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'customer_id' => 'Customer',
			'live_mode' => 'Live Mode',
			'last4' => 'Last4',
			'card_type' => 'Card Type',
			'exp_month' => 'Exp Month',
			'exp_year' => 'Exp Year',
			'fingerprint' => 'Fingerprint',
			'name' => 'Name',
			'address_city' => 'City',
			'address_country' => 'Country',
			'address_line1' => 'Street',
			'address_line2' => 'Address Line2',
			'address_state' => 'State',
			'address_zip' => 'Zip Code',
			'cvc_check' => 'Cvc Check',
			'phone' => 'Phone',
			'email' => 'Email',
			'delinquent' => 'Delinquent',
			'subscription' => 'Subscription',
			'discount' => 'Discount',
			'account_balance' => 'Account Balance',
			'terms_accepted' => 'Terms Accepted',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,false);
		$criteria->compare('user_id',$this->user_id,false);
		$criteria->compare('customer_id',$this->customer_id,false);
		$criteria->compare('live_mode',$this->live_mode);
		$criteria->compare('last4',$this->last4,true);
		$criteria->compare('card_type',$this->card_type,true);
		$criteria->compare('exp_month',$this->exp_month,true);
		$criteria->compare('exp_year',$this->exp_year,true);
		$criteria->compare('fingerprint',$this->fingerprint,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address_line1',$this->address_line1,true);
		$criteria->compare('address_line2',$this->address_line2,true);
		$criteria->compare('address_city',$this->address_city,true);
		$criteria->compare('address_state',$this->address_state,true);
		$criteria->compare('address_zip',$this->address_zip,true);
		$criteria->compare('address_country',$this->address_country,true);
		$criteria->compare('cvc_check',$this->cvc_check,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('delinquent',$this->delinquent);
		$criteria->compare('subscription',$this->subscription,true);
		$criteria->compare('discount',$this->discount,true);
		$criteria->compare('account_balance',$this->account_balance);
		$criteria->compare('terms_accepted',$this->terms_accepted);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function validateFromCountry($attribute, $value)
	{
		if ($this->address_country == 'US' || $this->address_country == 'CA')
		{
			if (!$this->$attribute)
				$this->addError($attribute, $this->getAttributeLabel($attribute).' cannot be blank.');
		}
	}

	public function beforeSave()
	{
		$valid = parent::beforeSave();

		$action = ($this->isNewRecord || !$this->customer_id) ? 'createCustomer' : 'updateCustomer';
		
		// Create / update new customer via Stripe API
		$response = Yii::app()->stripe->$action(array(
			'card' => ($this->card_token == '1') ? null : $this->card_token, // obtained with Stripe.js
			'customer_id' => $this->customer_id,
			'email' => $this->email,
			'description' => $this->name.' ('.$this->email.')',
		));
		
		if (isset($response['id']))
		{
			$this->customer_id = isset($response['id']) ? $response['id'] : null;
			$this->cvc_check = isset($response['card']->cvc_check) ? $response['card']->cvc_check : null;
			$this->live_mode = isset($response['livemode']) ? $response['livemode'] : null;
			$valid &= true;
		}

		// if ( isset($response['id']) )
		// {
		// 	$stripeCustomer = new StripeCustomer('create');
		// 	$stripeCustomer->attributes = $response;
		// 	$stripeCustomer->attributes = isset($response['active_card']) ? $response['active_card'] : null;
		// 	$stripeCustomer->card_type = isset($response['type']) ? $response['type'] : null;
		// 	$stripeCustomer->save();
		// 	$this->stripe_id = $stripeCustomer->id; 

		// 	Yii::app()->stripe->subscribeCustomer()

		// 	var_dump($stripeCustomer);

		// 	// $this->stripe_id = isset($response['id']) ? $response['id'] : null;
		// 	// $this->fee = isset($response['fee']) ? $response['fee'] / 100 : null;
		// 	// $this->paid = isset($response['paid']) ? $response['paid'] : null;
		// 	// $this->currency = isset($response['currency']) ? $response['currency'] : null;
		// 	// $this->cvc_check = isset($response['card']->cvc_check) ? $response['card']->cvc_check : null;
		// 	// $this->refunded = isset($response['refunded']) ? $response['refunded'] : null;
		// 	// $this->captured = isset($response['captured']) ? $response['captured'] : null;
		// 	// $this->live_mode = isset($response['livemode']) ? $response['livemode'] : null;

		// 	$valid &= true;
		// }
		// else 
		// {
		// 	$this->card_token = '';
		// 	$valid = false;
		// }

		return $valid;
	}
}