<?php

/**
 * This is the model class for table "transaction".
 *
 * The followings are the available columns in table 'transaction':
 * @property string $id
 * @property string $user_id
 * @property string $type
 * @property string $payment_method
 * @property string $customer_ref_num
 * @property string $name
 * @property string $street
 * @property string $street1
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $zipcode
 * @property string $email
 * @property string $phone
 * @property string $subtotal
 * @property string $tax
 * @property string $total
 * @property string $currency
 * @property string $card_number
 * @property string $card_expiry
 * @property string $card_type
 * @property string $service
 * @property integer $approved
 * @property string $trans_ref
 * @property string $message
 * @property string $host
 * @property string $data
 * @property integer $status
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property TransactionLog[] $transactionLogs
 */
class Transaction extends BaseRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'transaction';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return CMap::mergeArray(parent::rules(), array(
			array('approved, status', 'numerical', 'integerOnly'=>true),
			array('user_id, zipcode', 'length', 'max'=>10),
			array('type, payment_method, card_type, promo_code', 'length', 'max'=>32),
			array('customer_ref_num', 'length', 'max'=>22),
			array('name, street, street1', 'length', 'max'=>30),
			array('city', 'length', 'max'=>20),
			array('state, country', 'length', 'max'=>2),
			array('email', 'length', 'max'=>50),
			array('phone', 'length', 'max'=>14),
			array('subtotal, tax, total, service', 'length', 'max'=>16),
			array('currency', 'length', 'max'=>6),
			array('card_number, card_expiry', 'length', 'max'=>4),
			array('trans_ref, host', 'length', 'max'=>255),
			array('message, data, created, updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, type, payment_method, customer_ref_num, promo_code, name, street, street1, city, state, country, zipcode, email, phone, subtotal, tax, total, currency, card_number, card_expiry, card_type, service, approved, trans_ref, message, host, data, status, created, updated', 'safe', 'on'=>'search'),
		));
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'transactionLogs' => array(self::HAS_MANY, 'TransactionLog', 'transaction_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'type' => 'Type',
			'payment_method' => 'Payment Method',
			'customer_ref_num' => 'Customer Ref Num',
			'promo_code' => 'Promo Code',
			'name' => 'Name',
			'street' => 'Street',
			'street1' => 'Street1',
			'city' => 'City',
			'state' => 'State',
			'country' => 'Country',
			'zipcode' => 'Zipcode',
			'email' => 'Email',
			'phone' => 'Phone',
			'subtotal' => 'Subtotal',
			'tax' => 'Tax',
			'total' => 'Total',
			'currency' => 'Currency',
			'card_number' => 'Card Number',
			'card_expiry' => 'Card Expiry',
			'card_type' => 'Card Type',
			'service' => 'Service',
			'approved' => 'Approved',
			'trans_ref' => 'Trans Ref',
			'message' => 'Message',
			'host' => 'Host',
			'data' => 'Data',
			'status' => 'Status',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchCriteria()
	{
		$criteria = parent::searchCriteria();

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('payment_method',$this->payment_method,true);
		$criteria->compare('customer_ref_num',$this->customer_ref_num,true);
		$criteria->compare('promo_code',$this->promo_code,false);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('street',$this->street,true);
		$criteria->compare('street1',$this->street1,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('zipcode',$this->zipcode,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('subtotal',$this->subtotal,true);
		$criteria->compare('tax',$this->tax,true);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('currency',$this->currency,true);
		$criteria->compare('card_number',$this->card_number,true);
		$criteria->compare('card_expiry',$this->card_expiry,true);
		$criteria->compare('card_type',$this->card_type,true);
		$criteria->compare('service',$this->service,true);
		$criteria->compare('approved',$this->approved);
		$criteria->compare('trans_ref',$this->trans_ref,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('host',$this->host,true);
		$criteria->compare('data',$this->data,true);

		return $criteria;
	}

	public function defaultScope()
	{
		$scope = parent::defaultScope();
		$condition = isset($scope['condition']) ? array($scope['condition']) : array();

		$t = $this->getTableAlias(false, false);

		$condition[] = $t.'.approved = 1';

		if (Yii::app()->request->scriptUrl != '/backend.php')
			$condition[] = $t.'.created >= "'.date('Y-m-d', strtotime('1 year ago')).'"';

		if (count($condition) > 0)
			$scope['condition'] = implode(' AND ', $condition);

		$scope['order'] = $t.'.created DESC';

		return $scope;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Transaction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function export()
	{
		$criteria = $this->searchCriteria();

		$columns = array(
			't.id' => 'ID',
			't.user_id' => 'User',
			'profile.first_name' => 'First Name',
			'profile.last_name' => 'Last Name',
			't.type' => 'Type',
			't.payment_method' => 'Payment Method',
			't.customer_ref_num' => 'Customer Ref Num',
			't.promo_code' => 'Promo Code',
			't.name' => 'Name',
			't.street' => 'Street',
			// 't.street1' => 'Street1',
			// 't.city' => 'City',
			// 't.state' => 'State',
			// 't.country' => 'Country',
			't.zipcode' => 'Zipcode',
			't.email' => 'Email',
			't.phone' => 'Phone',
			't.subtotal' => 'Subtotal',
			't.tax' => 'Tax',
			't.total' => 'Total',
			't.currency' => 'Currency',
			't.card_number' => 'Card Number',
			't.card_expiry' => 'Card Expiry',
			't.card_type' => 'Card Type',
			't.service' => 'Service',
			't.approved' => 'Approved',
			't.trans_ref' => 'Trans Ref',
			't.message' => 'Message',
			't.host' => 'Host',
			// 't.data' => 'Data',
			't.status' => 'Status',
			't.created' => 'Created',
			't.updated' => 'Updated',
		);

		$headers = array();
		$select = array();
		foreach ($columns as $column=>$label)
		{
			$headers[] = $label;
			$select[] = $column;
		}

		$rows = Yii::app()->db->createCommand()
			->select(implode(', ', $select))
			->from($this->tableName().' t')
			// left joins in case event / company / sales user is null or does not exist
			->join('profile', 'profile.user_id=t.user_id')
			->where($criteria->condition, $criteria->params)
			->order('t.created ASC')
			->queryAll();

		foreach($rows as $k=>$row)
		{
			// $rows[$k]['created'] = date('Y-m-d', $row['created']);
		}

		Export::csv($rows, $headers, 'transactions');
	}

	public function getCounts()
    {
    	$criteria = $this->searchCriteria();

        $columns = array(
            'count(*) count'=>'Count',
            'sum(t.subtotal) subtotal'=>'Subtotal',
			'sum(t.tax) tax'=>'Tax',
            'sum(t.total) total'=>'Total',
        );

        // $header = array();
        $select = array();
        foreach ($columns as $column=>$label)
        {
            // $header[$column] = $label;
            $select[] = $column;
        }

        $command = Yii::app()->db->createCommand();
        $command->select(implode(', ', $select));
        $command->from($this->tableName().' t');
        $command->where($criteria->condition, $criteria->params);
        return $command->queryRow();
    }

	public static function getUserTransactions($user_id)
	{
		return Transaction::model()->byUser($user_id)->findAll();
	}

	public function getCardType()
	{
		return $this->card_type;
	}

	public static function charge($params)
	{
		$model = new Transaction;
		$model->status = 1;
		$model->user_id = $params['user_id'];
		$model->service = 'authorize_net';
		$model->host = $_SERVER['REMOTE_ADDR'];
		$model->promo_code = isset($params['promo_code']) ? $params['promo_code'] : null;
		$model->subtotal = $params['subtotal'];
		$model->tax = $params['tax'];
		$model->total = $params['total'];
		$model->description = $params['description'];
		$model->membership_period = $params['membership_period'];
		$model->approved = 0;
		$model->save();

		if ($model->subtotal <= 0)
		{
			$model->approved = 1;
			$model->message = '$0 transaction, automatically approved';
			$model->save();
			return $model;
		}

		$params['transaction_id'] = $model->id;
		$result = Yii::app()->authorizenet->charge($params);
		$response = $result['response'];

		$model->approved = ($result['approved']) ? 1 : 0;
		$model->message = $result['message'];
		$model->customer_ref_num = isset($response['customer_id']) ? $response['customer_id'] : null;
		$model->type = isset($response['transaction_type']) ? $response['transaction_type'] : null;
		$model->trans_ref = isset($response['transaction_id']) ? $response['transaction_id'] : null;
		$model->name = (isset($response['first_name']) && isset($response['last_name'])) ? $response['first_name'].' '.$response['last_name'] : null;
		$model->email = isset($response['email_address']) ? $response['email_address'] : null;
		$model->street = isset($response['address']) ? $response['address'] : null;
		$model->zipcode = isset($response['zip_code']) ? $response['zip_code'] : null;
		$model->country = isset($response['country']) ? substr($response['country'], 0, 2) : null;
		$model->payment_method = isset($response['method']) ? $response['method'] : null;
		$model->card_number = isset($response['account_number']) ? substr($response['account_number'], 4, 4) : null;
		$model->card_type = isset($response['card_type']) ? $response['card_type'] : null;
		$model->data = serialize($response);
		$model->save();

		TransactionLog::log($model->id, 'authorize_net', $result['approved'], $result['message'], $result['request'], $response);

		return $model;
	}

	public function getDisplayTitle()
	{
		return 'Transaction #'.$this->id.' - '.$this->name;
	}
}
