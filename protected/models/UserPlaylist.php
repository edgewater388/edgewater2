<?php

/**
 * This is the model class for table "user_playlist".
 *
 * The followings are the available columns in table 'user_playlist':
 * @property string $user_id
 * @property string $playlist_id
 */
class UserPlaylist extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_playlist';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, playlist_id', 'required'),
			array('user_id, playlist_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, playlist_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'playlist_id' => 'Playlist',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

	}
	
	public function searchCriteria()
	{
		$criteria = parent::searchCriteria();

		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('playlist_id',$this->playlist_id,true);

		return $criteria;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserPlaylist the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function reorder($user_id, $playlist_id, $order=array())
	{
		if (!$model = self::model()->findByPk(array('user_id'=>$user_id, 'playlist_id'=>$playlist_id)))
		{
			$model = new UserPlaylist;
			$model->user_id = $user_id;
			$model->playlist_id = $playlist_id;
		}

		$model->order = implode(',', $order);
		return $model->save();
	}

	public static function subscribe($user_id, $playlist_id)
	{
		if (!$model = self::model()->findByPk(array('user_id'=>$user_id, 'playlist_id'=>$playlist_id)))
		{
			$model = new UserPlaylist;
			$model->user_id = $user_id;
			$model->playlist_id = $playlist_id;
		}

		return $model->save();
	}

	public static function unsubscribe($user_id, $playlist_id)
	{
		if ($model = self::model()->findByPk(array('user_id'=>$user_id, 'playlist_id'=>$playlist_id)))
			return $model->delete();
		return true;
	}

	public static function sort($user_id, $playlist_id, $videos=array())
	{
		if (!$model = self::model()->findByPk(array('user_id'=>$user_id, 'playlist_id'=>$playlist_id)))
			return $videos;

		if (!$model->order)
			return $videos;

		$order = explode(',', $model->order);
		$order = array_flip($order);
		$sorted_videos = array();

		foreach ($videos as $k=>$video)
		{
			if (array_key_exists($video->id, $order))
			{
				$key = $order[$video->id];
				$sorted_videos[$key] = $video;
				unset($videos[$k]);
			}
		}

		ksort($sorted_videos);

		$sorted_videos = array_merge($sorted_videos, $videos);

		return $sorted_videos;
	}
}
