<?php

class UserRegimen extends ActiveRecord
{
	public function tableName()
	{
		return 'users_to_regimens';
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return [
			'userRegimenPlan' => [self::HAS_MANY, 'UserRegimenPlan', 'u2r_id'],
			'regimen' => [self::BELONGS_TO, 'Regimen', 'regimen_id'],
		];
	}

	public function afterDelete()
	{
		parent::afterDelete();
		UserRegimenPlan::model()->deleteAll('u2r_id = :u2r_id', [':u2r_id' => $this->id]);
	}

	/**
	 * @param $user_id
	 * @param $regimen_id
	 *
	 * @return bool
	 */
	public static function checkUserSubscribed($user_id, $regimen_id)
	{
		return (bool)self::model()->exists('user_id = :user_id AND regimen_id = :regimen_id',
			[":user_id" => $user_id, ':regimen_id' => $regimen_id]
		);
	}

	/**
	 * @param $user_id
	 * @param $regimen_id
	 *
	 * @return bool
	 */
	public static function checkProgramExpired($user_id, $regimen_id)
	{
		$u2r = self::model()->find('user_id = :user_id AND regimen_id = :regimen_id',
			[":user_id" => $user_id, ':regimen_id' => $regimen_id]
		);

		if (empty($u2r))
			return false;

		$max_plan = UserRegimenPlan::model()->find('day = (SELECT MAX(day) FROM user_regimen_plan WHERE u2r_id = :u2r_id) AND u2r_id = :u2r_id', [':u2r_id' => $u2r->id]);

		if (empty($max_plan))
			return false;

		return date('Y-m-d') > date('Y-m-d', strtotime($max_plan->date));
	}

	public static function getU2R_ID($user_id, $regimen_id)
	{
		return self::model()->find('user_id = :user_id AND regimen_id = :regimen_id',
			[":user_id" => $user_id, ':regimen_id' => $regimen_id]
		)->id;
	}
}