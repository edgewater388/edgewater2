<?php

class UserRegimenPlan extends ActiveRecord
{
	public function tableName()
	{
		return 'user_regimen_plan';
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return [
			'userRegimen' => [self::BELONGS_TO, 'UserRegimen', 'u2r_id'],
			'video'       => [self::BELONGS_TO, 'Video', 'video_id'],
		];
	}

	public function getWeekNum()
	{
		return ceil($this->day / 7);
	}

	public static function getApproxDaysPassed($u2r_id, $date = null, $i = 1)
	{
		if (is_null($date))
			$date = date('Y-m-d');

		$criteria = new CDbCriteria();
		$criteria->select = 'day';
		$criteria->condition = 'u2r_id = :u2r_id AND date = :date';
		$criteria->params = [':date' => $date, ':u2r_id' => $u2r_id];
		$days_passed = self::model()->find($criteria);
		$last_workout = self::model()->find('day = (SELECT MAX(day) FROM user_regimen_plan WHERE u2r_id = :u2r_id) AND u2r_id = :u2r_id', [':u2r_id' => $u2r_id]);

		while (empty($days_passed->day)) {
			if (date('Y-m-d') >= $last_workout->date) {
				$date = date('Y-m-d', strtotime("-{$i} days"));
			} else {
				$date = date('Y-m-d', strtotime("+{$i} days"));
			}

			++$i;
			$days_passed = self::getApproxDaysPassed($u2r_id, $date, $i);
		}

		return $days_passed;
	}

	public static function getCurrentWeekNum($u2r_id)
	{
		$days_passed = self::getApproxDaysPassed($u2r_id);

		return ceil($days_passed->day / 7);
	}

	public static function checkRegimenFinished($u2r_id)
	{
		$progress = self::getWorkoutProgress($u2r_id);

		if ($progress == 100)
			return true;
		else
			return false;
	}

	public static function getTotalWorkoutsCount($u2r_id)
	{
		$criteria = new CDbCriteria;
		$criteria->select = "COUNT(id) as id";
		$criteria->condition = 'u2r_id = :u2r_id';
		$criteria->params = [':u2r_id' => $u2r_id];

		return self::model()->find($criteria)->id;
	}

	public static function getWatchedWorkoutsCount($u2r_id)
	{
		$criteria = new CDbCriteria;
		$criteria->select = "COUNT(id) as id";
		$criteria->condition = 'u2r_id = :u2r_id AND is_watched = 1';
		$criteria->params = [':u2r_id' => $u2r_id];

		return self::model()->find($criteria)->id;
	}

	public static function getSkippedDaysCount($u2r_id)
	{
		$criteria = new CDbCriteria;
		$criteria->select = "COUNT(DISTINCT day) as id";
		$criteria->condition = 'u2r_id = :u2r_id AND is_watched = 0 AND date < :current_date';
		$criteria->params = [':u2r_id' => $u2r_id, ':current_date' => date('Y-m-d')];

		return self::model()->find($criteria)->id;
	}

	public static function getDaysLeftCount($u2r_id)
	{
		$criteria = new CDbCriteria;
		$criteria->alias = 'user_regimen_plan';
		$criteria->select = "COUNT(DISTINCT user_regimen_plan.day) as id";
		$criteria->condition = 'user_regimen_plan.u2r_id = :u2r_id AND user_regimen_plan.is_watched = 0';
		$criteria->condition .= ' AND user_regimen_plan.date >= :current_date AND video.status = 1';
		$criteria->join = 'JOIN video ON video.id = user_regimen_plan.video_id';
		$criteria->params = [':u2r_id' => $u2r_id, ':current_date' => date('Y-m-d')];

		return self::model()->find($criteria)->id;
	}

	public static function getWorkoutProgress($u2r_id)
	{
		$total_workouts_count = self::getTotalWorkoutsCount($u2r_id);
		$watched_workouts_count = self::getWatchedWorkoutsCount($u2r_id);

		return round($watched_workouts_count / $total_workouts_count * 100, 1);
	}

	public static function getTotalWeeksCount($u2r_id)
	{
		$criteria = new CDbCriteria();
		$criteria->select = 'MAX(day) as day';
		$criteria->condition = "u2r_id = {$u2r_id}";
		$max_day = self::model()->find($criteria)->day;

		return ceil($max_day / 7);
	}

	public static function weekOrdinal($number)
	{
		$ends = ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th'];
		if ((($number % 100) >= 11) && (($number % 100) <= 13))
			return $number . 'th';
		else
			return $number . $ends[$number % 10];
	}

	public static function getCurrentWorkoutDay($u2r_id)
	{
		$current_workout = self::model()->find('date = :date AND u2r_id = :u2r_id', [
			':date'   => date('Y-m-d'),
			':u2r_id' => $u2r_id,
		]);
		$first_workout = self::model()->find('day = (SELECT MIN(day) FROM user_regimen_plan WHERE u2r_id = :u2r_id) AND u2r_id = :u2r_id', [':u2r_id' => $u2r_id]);
		$last_workout = self::model()->find('day = (SELECT MAX(day) FROM user_regimen_plan WHERE u2r_id = :u2r_id) AND u2r_id = :u2r_id', [':u2r_id' => $u2r_id]);

		if (empty($first_workout) || empty($last_workout))
			return false;

		$i = 0;
		while (empty($current_workout->day)) {

			if (date('Y-m-d') >= $last_workout->date) {
				$date_to_find = date('Y-m-d', strtotime("today -{$i} days"));
			} else {
				$date_to_find = date('Y-m-d', strtotime("today +{$i} days"));
			}

			$current_workout = self::model()->find('date = :date AND u2r_id = :u2r_id',
				[':date' => $date_to_find, ':u2r_id' => $u2r_id]);
			++$i;
		}

		if ($first_workout->day > 1) {
			$current_workout->day -= $first_workout->day - 1;
		}

		if ($current_workout->day - $i < 1)
			return $current_workout->day - $i + 1;

		return $current_workout->day - $i;
	}
}