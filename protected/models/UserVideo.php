<?php

class UserVideo extends ActiveRecord
{
	public function tableName()
	{
		return 'user_video';
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, video_id', 'required'),
			array('user_id, video_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, video_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'video' => array(self::BELONGS_TO, 'Video', 'video_id'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function add($user_id, $video_id)
	{
		if (!$model = self::model()->findByPk(array('user_id'=>$user_id, 'video_id'=>$video_id)))
		{
			$model = new UserVideo;
			$model->user_id = $user_id;
			$model->video_id = $video_id;

			if (!$model->user || !$model->video)
				return false;
		}

		$model->status = 1;
		return $model->save();
	}

	public static function remove($user_id, $video_id)
	{
		if ($model = self::model()->findByPk(array('user_id'=>$user_id, 'video_id'=>$video_id)))
			return $model->delete();
		return true;
	}
}