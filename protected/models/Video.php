<?php

/**
 * This is the model class for table "video".
 * The followings are the available columns in table 'video':
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property integer $status
 * @property string $created
 * @property string $updated
 * @property boolean $is_new
 * The followings are the available model relations:
 * @property Tag[] $tags
 */
class Video extends BaseRecord
{
	public $product_ids;
	public $type;
	public $level;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'video';
	}

	public function init()
	{
		parent::init();

		$this->is_public = 0;
		$this->weight = 0;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return CMap::mergeArray(parent::rules(), [
			['status, strength, cardio, restorative, is_public, weight, is_live_class', 'numerical', 'integerOnly' => true],
			['title, body_part, equipment', 'length', 'max' => 255],
			['instructor_id', 'length', 'max' => 10],
			['description, created, updated, body_part, equipment, is_live_class, is_new, duration', 'safe'],
			['length', 'length', 'max' => 16],
			['jwplayer_video_id', 'length', 'max' => 64],
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			['id, title, description, type, level, strength, cardio, restorative, length, duration, instructor_id, created_from, created_to, status, created, updated, is_public, weight', 'safe', 'on' => 'search'],
		]);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return [
			'playlists'     => [self::MANY_MANY, 'Playlist', 'playlist_video(video_id, playlist_id)'],
			'users'         => [self::MANY_MANY, 'Users', 'user_video(video_id, user_id)'],
			'instructor'    => [self::BELONGS_TO, 'Users', 'instructor_id'],
			'videoPlayLogs' => [self::HAS_MANY, 'VideoPlayLog', 'video_id'],
			'products'      => [self::MANY_MANY, 'Product', 'video_product(video_id, product_id)'],
			'tags'          => [self::MANY_MANY, 'Tag', 'video_tag(video_id, tag_id)'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return CMap::mergeArray(parent::attributeLabels(), [
			'id'             => 'ID',
			'title'          => 'Title',
			'description'    => 'Description',
			'jwplayer_video' => 'JW Player Video ID',
			'is_public'      => 'Preview Enabled',
			'status'         => 'Status',
			'created'        => 'Created',
			'updated'        => 'Updated',
		]);
	}

	public function search($pageSize = 12)
	{
		return parent::search($pageSize);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchCriteria()
	{
		$criteria = parent::searchCriteria();

		$partial_matches = ['title', 'description', 'body_part', 'equipment'];
		$types = ['strength', 'cardio', 'restorative'];

		foreach (array_keys($this->attributes) as $attribute) {
			if (in_array($attribute, $partial_matches)) {
				$criteria->compare($attribute, $this->{$attribute}, true);
			} else {
				$criteria->compare($attribute, $this->{$attribute});
			}
		}

		if ($this->type) {
			$index = array_search($this->type, $types);

			if ($index !== false) {
				$typeCriteria = new CDbCriteria;

				if ($this->level)
					$typeCriteria->compare($types[$index], $this->level);
				else
					$typeCriteria->addCondition($types[$index] . ' IS NOT NULL');

				$criteria->mergeWith($typeCriteria);
			}
		} elseif ($this->level) {
			$levelCriteria = new CDbCriteria;
			foreach ($types as $type)
				$levelCriteria->compare($type, $this->level, false, 'OR');
			$criteria->mergeWith($levelCriteria);
		}

		if ($this->created_from) {
			$created_to = date('Y-m-d', strtotime($this->created_to . ' + 1 day'));
			$criteria->addBetweenCondition('t.created', $this->created_from, $created_to);
		}

		if ($this->q)
			$criteria->compare('title', $this->q, true);

		return $criteria;
	}

	public function defaultScope()
	{
		if (substr($_SERVER['REQUEST_URI'], 0, 5) == '/cron')
			$this->restrictToActiveRecords = false;

		$scope = parent::defaultScope();

		$t = $this->getTableAlias(false, false);
		$scope['order'] = $t . '.length ASC, ' . $t . '.title ASC';

		return $scope;
	}

	public function scopes()
	{
		return CMap::mergeArray(parent::scopes(), [
			'favorites'     => [
				'join'   => 'INNER JOIN user_video ON user_video.video_id = t.id AND user_video.user_id = :user_id',
				'params' => [
					':user_id' => Yii::app()->user->Id,
				],
			],
			'byLongest'     => [
				'order' => 't.length DESC, t.title ASC',
			],
			'byShortest'    => [
				'order' => 't.length ASC, t.title ASC',
			],
			'playlistOrder' => [
				'order' => 'videos_videos.weight ASC',
			],
			'byWeight'      => [
				'order' => 't.weight ASC, t.title ASC',
			],
			'isNew'=> [
				'condition' => 'is_new = 1 AND is_live_class = 0',
			],
		]);
	}

	public function isPublic($is_public)
	{
		$t = $this->getTableAlias(false, false);

		$this->getDbCriteria()->mergeWith([
			'condition' => $t . '.is_public = :is_public',
			'params'    => [':is_public' => (int)(bool)$is_public],
		]);

		return $this;
	}

	public function isLiveClass($is_live_class)
	{
		$t = $this->getTableAlias(false, false);

		$this->getDbCriteria()->mergeWith([
			'condition' => $t . '.is_live_class = :is_live_class',
			'params'    => [':is_live_class' => (int)(bool)$is_live_class],
		]);

		return $this;
	}

	public function beforeSave()
	{
		$valid = parent::beforeSave();

		$this->duration = $this->getDuration();
		$this->length = (int)$this->length;
		$this->time = $this->getTime();

		if (!$this->is_public)
			$this->is_public = 0;

		return $valid;
	}

	public function afterFind()
	{
		$this->duration = $this->getDuration();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className active record class name.
	 *
	 * @return Video the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public static function addOrUpdate($attributes)
	{
		// echo '<pre>';
		// print_r($attributes);
		// echo '</pre>';
		// exit;

		// Create a new record if the model does not exist	
		if (!$model = Video::model()->byAttribute('guid', $attributes->id)->find()) {
			$model = new Video;
			$model->guid = $attributes->id;
			$model->status = 0;
		}

		$model->title = $attributes->name;
		$model->description = $attributes->shortDescription;
		$model->data = serialize($attributes);
		$model->length = $attributes->length;

		return $model->save();
	}

	public static function addOrUpdateJWVideo($attributes)
	{
		// k($attributes);

		// Create a new record if the model does not exist	
		if (!$model = Video::model()->byAttribute('jwplayer_video_id', $attributes['key'])->find()) {
			// k($attributes);
			$model = new Video;
			$model->jwplayer_video_id = $attributes['key'];
			$model->status = 0;
		}

		$model->title = $attributes['title'];
		$model->description = $attributes['description'];
		$model->data = serialize($attributes);
		$model->length = $attributes['duration'] * 1000;
		$model->created = date('Y-m-d h:i:s', $attributes['date']);

		return $model->save();
	}

	public static function getTypes()
	{
		return [
			'strength'    => 'Strength',
			'cardio'      => 'Cardio',
			'restorative' => 'Restorative',
			// 'combo'=>'Combo',
		];
	}

	public static function getLevels()
	{
		return [
			'1' => 'Level 1',
			'2' => 'Level 2',
			'3' => 'Level 3',
		];
	}

	// public function getTypeName()
	// {
	// 	$types = self::getTypes();
	// 	return isset($types[$this->type]) ? $types[$this->type] : null;
	// }

	// public function getLevelName()
	// {
	// 	$levels = self::getLevels();
	// 	return isset($levels[$this->level]) ? $levels[$this->level] : null;
	// }

	public function getMeta()
	{
		// die(var_dump(unserialize($this->data)));
		if ($this->isJWPlayerVideo) {
			return (object)[
				'id'            => $this->jwplayer_video_id,
				'videoStillURL' => $this->stillUrl,
				'thumbnailURL'  => $this->thumbUrl,
				'FLVURL'        => '//content.jwplatform.com/manifests/' . $this->jwplayer_video_id . '.m3u8',
				'HLSURL'        => '//content.jwplatform.com/manifests/' . $this->jwplayer_video_id . '.m3u8',
				'rmtp'          => '//content.jwplatform.com/manifests/' . $this->jwplayer_video_id . '.m3u8',
				'source_320'    => '//content.jwplatform.com/videos/' . $this->jwplayer_video_id . '-S1TfV4NJ.mp4',
				'source_480'    => '//content.jwplatform.com/videos/' . $this->jwplayer_video_id . '-e9wTPhKK.mp4',
				'source_720'    => '//content.jwplatform.com/videos/' . $this->jwplayer_video_id . '-YaD509Lu.mp4',
				'source_1280'   => '//content.jwplatform.com/videos/' . $this->jwplayer_video_id . '-mvAybheq.mp4',
				'source_1920'   => '//content.jwplatform.com/videos/' . $this->jwplayer_video_id . '-O3HlRFF6.mp4',
			];
		}

		return unserialize($this->data);
	}

	public function getTime()
	{
		if (!$this->length)
			return '0:00';

		$minutes = $this->getMinutes();
		$seconds = $this->getSeconds();

		return $minutes . ':' . $seconds;
	}

	public function getMinutes()
	{
		return floor($this->length / 1000 / 60);
	}

	public function getSeconds()
	{
		$seconds = floor(($this->length / 1000) - ($this->getMinutes() * 60));

		return str_pad($seconds, 2, '0', STR_PAD_LEFT);
	}

	public function getRoundedMinutes()
	{
		return ceil($this->length / 1000 / 60);
	}

	public static function getApiColumns()
	{
		return [
			'id',
			'title',
			'description',
			'instructor_id',
			'instructor.profile.first_name',
			'instructor.profile.name',
			'strength',
			'cardio',
			'restorative',
			'length',
			'time',
			'duration',
			'body_part',
			'equipment',
			'is_new',
			'meta'     => [
				'id',
				// 'name',
				// 'shortDescription',
				// 'longDescription',
				// 'creationDate',
				// 'publishedDate',
				// 'lastModifiedDate',
				// 'tags',
				'videoStillURL',
				'thumbnailURL',
				// 'length',
				// 'economics',
				// 'itemState',
				// 'playsTotal',
				// 'playsTrailingWeek',
				// 'version',
				'rmtp',
				'FLVURL',
				'HLSURL',
				'source_320',
				'source_480',
				'source_720',
				'source_1280',
				'source_1920',
				// 'accountId',
				// 'FLVFullLength',
				// 'videoFullLength',
			],
			'products' => [
				'id',
				'title',
				'thumbUrl',
				'url',
				'price',
				'price_color',
			],
		];
	}
	
	public function getDuration()
	{
		$minutes = $this->minutes;

		switch (true) {
			case ($minutes <= 10):
				$duration = 10;
				break;
			case ($minutes >= 11 && $minutes <= 17):
				$duration = 15;
				break;
			case ($minutes >= 18 && $minutes <= 36):
				$duration = 30;
				break;
			default:
				$duration = 60;
				break;
		}

		return $this->duration = $duration;
	}

	public static function getDurations()
	{
		return [
			10,
			15,
			30,
			60,
		];
	}

	public static function getBodyParts()
	{
		return [
			'Arms'         => 'Arms',
			'Abs'          => 'Abs',
			'Seat'         => 'Seat',
			'Glutes'       => 'Glutes',
			'Thighs'       => 'Thighs',
			'Inner Thighs' => 'Inner Thighs',
			'Upper Body'   => 'Upper Body',
			'Lower Body'   => 'Lower Body',
			'Full Body'    => 'Full Body',
		];
	}

	public static function getEquipment()
	{
		return [
			'Weights'          => 'Weights',
			'Ball'             => 'Ball',
			'Sturdy Furniture' => 'Sturdy Furniture',
			'Bands'            => 'Bands',
			'No Equipment'     => 'No Equipment',
		];
	}

	public function getIsJWPlayerVideo()
	{
		return ($this->jwplayer_video_id);
	}

	public function getThumbUrl()
	{
		if ($this->isJWPlayerVideo)
			return '//content.jwplatform.com/thumbs/' . $this->jwplayer_video_id . '-480.jpg';

		return isset($this->meta->videoStillURL) ? $this->meta->videoStillURL : self::getDefaultThumbUrl();
	}

	public function getStillUrl()
	{
		if ($this->isJWPlayerVideo)
			return '//content.jwplatform.com/thumbs/' . $this->jwplayer_video_id . '-1280.jpg';

		return isset($this->meta->videoStillURL) ? $this->meta->videoStillURL : self::getDefaultThumbUrl();
	}

	public static function getDefaultThumbUrl()
	{
		return Yii::app()->params['defaultThumbUrl'];
	}

	public function getFlvUrl()
	{
		return 'rtmp://jwplayer.com/';
	}

	public function getHlsUrl()
	{
		return '//jwplayer.com/';
	}

	public static function getVideos($limit = 9, $sort = 'longest', $preview = false, $scope = null)
	{
		if (!$scope)
			$scope = $sort == 'shortest' ? 'byShortest' : 'byLongest';

		$is_public = $preview ? 1 : 0;

		return Video::model()->limit($limit)->isPublic($is_public)->$scope()->findAll();
	}

	public static function getPreviewVideos($limit = 5)
	{
		return self::getVideos($limit, null, true, 'byWeight');
		// return Video::model()->limit($limit)->byAttribute('is_public', 1)->findAll();
	}

	public static function getFavoriteVideos($limit = 6)
	{
		return Video::model()->limit($limit)->favorites()->findAll();
	}

	public static function getNewVideos()
	{
		return Video::model()->isNew()->findAll();
	}

	public static function getAllByDuration($limit = null, $preview = false)
	{
		$durations = static::getDurations();
		$videos = [];

		foreach ($durations as $duration) {
			$videos[$duration] = static::getByDuration($duration, $limit, $preview);
		}

		return $videos;
	}

	public static function getByDuration($duration, $limit = null, $preview = false)
	{
		$model = static::model();

		$t = $model->getTableAlias(false, false);

		$model->getDbCriteria()->mergeWith([
			'condition' => $t . '.duration = :duration',
			'limit'     => $limit,
			'params'    => [':duration' => (int)$duration],
		]);

		$is_public = $preview ? 1 : 0;

		$model
			->isPublic($is_public)
			->isLiveClass(false);

		return $model
			->isPublic($is_public)
			->isLiveClass(false)
			->findAll();
	}

	public static function getLiveClasses($limit = null, $preview = false)
	{
		$model = static::model();

		$model->getDbCriteria()->mergeWith([
			'limit' => $limit,
			'order' => 'created DESC',
		]);

		$is_public = $preview ? 1 : 0;

		return $model->isPublic($is_public)->isLiveClass(true)->findAll();
	}

	public static function getLevelWord($n)
	{
		if ($n === '1')
			return 'one';
		if ($n === '2')
			return 'two';
		if ($n === '3')
			return 'three';

		return null;
	}

	public static function retrieveAll()
	{
		set_time_limit(0);

		// $videos = Yii::app()->brightcove->getVideos();
		$videos = Yii::app()->jwplatform->getVideos();

		$count = count($videos);
		$saved = 0;
		foreach ($videos as $video) {
			if (Video::addOrUpdateJWVideo($video))
				$saved++;
		}

		return [
			'saved' => $saved,
			'count' => $count,
		];

		// echo '<pre>';
		// echo 'Count: '.$count.'<br>';
		// echo 'Saved: '.$saved.'<br>';
		// // print_r($videos);
		// echo '</pre>';
		// exit;
	}

	public static function getWorkoutStrength($strength)
	{
		$array_strengths = [
			1 => 'one',
			2 => 'two',
			3 => 'three',
		];

		return $array_strengths[$strength];
	}
}
