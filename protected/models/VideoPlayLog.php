<?php

/**
 * This is the model class for table "video_play_log".
 *
 * The followings are the available columns in table 'video_play_log':
 * @property string $id
 * @property string $user_id
 * @property string $video_id
 * @property integer $status
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Video $video
 */
class VideoPlayLog extends PActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'video_play_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status', 'numerical', 'integerOnly'=>true),
			array('user_id, video_id', 'length', 'max'=>10),
			array('created, updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, video_id, status, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'video' => array(self::BELONGS_TO, 'Video', 'video_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'video_id' => 'Video',
			'status' => 'Status',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchCriteria()
	{
		$criteria = parent::searchCriteria();

		$criteria->compare('video_id',$this->video_id,true);

		return $criteria;
	}

	public function defaultScope()
	{
		$scope = parent::defaultScope();
		$t = $this->getTableAlias(false, false);

		$condition = isset($scope['condition']) ? array($scope['condition']) : array();
		$condition[] = $t.'.created >= "'.date('Y-m-d', strtotime('3 months ago')).'"';

		if (count($condition) > 0)
			$scope['condition'] = implode(' AND ', $condition);

		$scope['order'] = $t.'.created DESC';

		return $scope;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VideoPlayLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function userPlayedVideos($user_id)
	{
		$sql = 'SELECT id, video_id FROM video_play_log WHERE user_id = :user_id GROUP BY video_id';

		$rows = Yii::app()->db->createCommand($sql)->bindParam(':user_id', $user_id)->queryAll();
		return Html::listData($rows, 'id', 'video_id');
	}
}
