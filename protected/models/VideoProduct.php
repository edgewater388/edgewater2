<?php

/**
 * This is the model class for table "video_product".
 *
 * The followings are the available columns in table 'video_product':
 * @property string $product_id
 * @property string $video_id
 */
class VideoProduct extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'video_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, video_id', 'required'),
			array('product_id, video_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('product_id, video_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
			'video' => array(self::BELONGS_TO, 'Video', 'video_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'product_id' => 'Product',
			'video_id' => 'Video',
		);
	}

	public function beforeSave()
	{
		$valid = parent::beforeSave();

		return $valid;
	}

	public function beforeDelete()
	{
		$valid = parent::beforeDelete();

		return $valid;
	}

	public function afterSave()
	{
		parent::afterSave();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PlaylistVideo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function add($video_id, $product_id)
	{
		if (!$model = self::model()->findByPk(array('product_id'=>$product_id, 'video_id'=>$video_id)))
		{
			$model = new VideoProduct;
			$model->product_id = $product_id;
			$model->video_id = $video_id;
		}

		$model->status = 1;

		return $model->save();
	}

	public static function remove($video_id, $product_id)
	{
		if ($model = self::model()->findByPk(array('product_id'=>$product_id, 'video_id'=>$video_id)))
			return $model->delete();
		return true;
	}
}
