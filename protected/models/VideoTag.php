<?php

/**
 * This is the model class for table "video_tag".
 *
 * The followings are the available columns in table 'video_tag':
 * @property string $video_id
 * @property string $tag_id
 */
class VideoTag extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return VideoTag the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'video_tag';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('video_id, tag_id', 'required'),
			array('video_id, tag_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('video_id, tag_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'video_id' => 'Video',
			'tag_id' => 'Tag',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('video_id',$this->video_id,false);
		$criteria->compare('tag_id',$this->tag_id,false);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function add($video_id, $tags)
	{
		$tags = explode(',', $tags);

		foreach ($tags as $tag)
		{
			$tag = trim($tag);

			if ($tag)
			{
				$tag_id = Tag::getTagId($tag);
				$model = new VideoTag('create');
				$model->video_id = $video_id;
				$model->tag_id = $tag_id;
				$model->save();
			}
		}
	}
}