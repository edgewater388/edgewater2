 <?php

class ApiModule extends CWebModule
{
	public $access_code;
	
    public function init()
    {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        // import the module-level models and components
        $this->setImport(array(
            'api.models.*',
            'api.components.*',
        ));
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {
			if (isset($_GET['code']) && $this->verifyCode($_GET['code']))
				return true;
			return false;
        }
        else
            return false;
    }
	
	/**
	 * 
	 */
	protected function verifyCode($code)
	{
		if ($code === $this->access_code)
			return true;
		throw new CHttpException(404,'The requested page does not exist.');
	}
}