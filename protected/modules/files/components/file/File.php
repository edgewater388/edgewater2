<?php

Yii::import('application.modules.files.components.file.cfile.CFile');

class File extends CFile
{
	public $service;
	public $new_filename;
	public $url;
	protected $_filetype;
	protected $_isPrivate;

	public function set($filePath, $greedy=false)
	{
		if (substr($filePath, 0, 1) == '/')
			$filePath = substr($filePath, 1);
		
		$instance = parent::set($filePath, $greedy);

		// Set the variables of $this
		foreach ($instance as $k=>$v)
			$this->$k = $v;

		$this->pathInfo();

		return $this;
	}

	public function upload($attribute, $private=false)
	{
		// Set the File instance (tmp file)
		$this->set($attribute);
		$this->_isPrivate = $private;

		if (!$this->exists)
			return false;

		// Generate a new filename
		$filename = $this->generateFilename();
		$basename = $filename.'.'.strtolower($this->getExtension());

		// Set the new destination path w/ new filename
		$fileDest = $this->getFileDestination($this->service) . '/'. $this->getType() .'/' . $basename;

		// Move the file to the directory
		if ($this->service == 's3')
			$this->s3upload($fileDest);
		else
			$this->move($fileDest);

		$this->_basename = $basename;

		return $this;
	}

	public function retrieve($filePath, $s3=true)
	{
		$filePath = substr($filePath, strpos($filePath, 'uploads/'));
		
		$tmp_dir = $this->getTmpFileDestination();
		$tmp_filepath = $tmp_dir.'/'.basename($filePath);

		if (!is_dir($tmp_dir))
			$this->createDir(0755, $tmp_dir);

		// Breakup url
		$pathinfo = parse_url($filePath);

		// Remove preceding "/"
		$key = substr($pathinfo['path'], 1); 

		if ($s3)
		{
			$s3 = Yii::app()->amazon->aws->get('s3');
			$success = $s3->getObject(array(
		        'Bucket' => Yii::app()->amazon->bucket,
		        'Key'    => $key,
		        'SaveAs' => $tmp_filepath,
		    ));
		}
		else
		{
			$tmp_filepath = $tmp_dir.'/'.$this->generateFilename().'.jpg';
			$image = file_get_contents($filePath);

			if (!is_dir(dirname($tmp_filepath)))
				$this->createDir(0755, dirname($tmp_filepath));

			file_put_contents($tmp_filepath, $image);
		}

		$file = new File;
		return $file->set($tmp_filepath);
	}

	public function s3upload($fileDest)
	{
		$s3 = Yii::app()->amazon->aws->get('s3');
		$result = $s3->putObject(array(
	        'Bucket' => Yii::app()->amazon->bucket,
	        'Key'    => $fileDest,
	        'Body'   => fopen($this->realpath, 'r'),
	        'ContentType'=>$this->mimeType,
	        // 'ACL'    => Aws\S3\Enum\CannedAcl::PUBLIC_READ
	    ));

	    @unlink($this->_realpath);

	    if ($result)
	    {
	    	$this->_filepath = $fileDest;
	    	$this->_realpath = Yii::app()->amazon->bucket.'/'.$fileDest;
	    	$this->_basename = basename($this->_filepath);
	    	$this->_filename = basename($this->_basename, '.'.$this->_extension);
	    	$this->url = File::getUrl($this->_filepath, $this->service);

	    	return $this;
	    }

	    $this->addLog('Error uploading file to s3');
    	return false;
	}

	public function move($fileDest)
    {
		// Create directory if dest directory does not exist
		if (!is_dir(dirname($fileDest)))
			$this->createDir(0755, dirname($fileDest));

		// Move the file
		return parent::move($fileDest);
    }

    public function copy($fileDest)
    {
		// Create directory if dest directory does not exist
		if (!is_dir(dirname($fileDest)))
			$this->createDir(0755, dirname($fileDest));

		// Move the file
		return parent::copy($fileDest);
    }

    protected function pathInfo()
    {
		parent::pathInfo();
		$this->_size = $this->getSize(false);
		$this->url = File::getUrl($this->_filepath, $this->service);
		$this->_filetype = $this->getType();
    }

    protected function exists()
    {
    	if ($this->service == 's3')
			return $this->remoteExists();

		return parent::exists();
    }

    protected function remoteExists()
    {
    	// $s3 = Yii::app()->amazon->aws->get('s3');
		// $this->_exists = $s3->doesObjectExist(Yii::app()->amazon->bucket, $this->filepath);
		// return $this->_exists;
		$header_response = get_headers($this->url, 1);

		if ( strpos( $header_response[0], "404" ) !== false )
			return false;
		return true;
    }

    public static function getUrl($filepath, $useCDN=false)
    {
    	return ($useCDN) ? Yii::app()->amazon->cdn.'/'.$filepath : '/'.$filepath;
    }

    public function generateFilename()
	{
		return uniqid().'_'.rand(10000,99999);
	}

	public function getTmpFileDestination()
	{
		return 'uploads/tmp';
	}

	public function getFileDestination($service=null)
	{
		$prefix = ($service == 's3') ? '' : 'uploads/';
		return $prefix.File::getShortEnv().'/'.($this->isPrivate()?'private':'public');
	}

	public function isPrivate()
	{
		return ($this->_isPrivate);
	}

	public static function getShortEnv()
	{
		if (Yii::app()->environment == 'production') return 'prod';
		if (Yii::app()->environment == 'staging') return 'stage';
		return 'dev';
	}

	public function getType()
	{
		// i.e. application/pdf, return pdf
		if (substr($this->mimetype, 0, 11) == 'application')
			return substr($this->mimetype, strpos($this->mimetype, '/')+1);

		// i.e. audio/mp3, return audio
		return substr($this->mimetype, 0, strpos($this->mimetype, '/'));
	}

	public function getFilepath()
	{
		return $this->_filepath;
	}

	public function getFiletype()
	{
		return 'image';
		return $this->_filetype;
	}
}