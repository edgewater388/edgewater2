<?php

/*
 * Requires File and CImageComponent classes
 */
class Thumb extends CApplicationComponent
{
	public $styles;
	public $flush = false;

	public function init()
	{
		$this->styles = CMap::mergeArray(
			array(
				'small'=>array(
					'scaleCrop'=>array(
						'width'=>'55',
						'height'=>'55',
					),
					'quality'=>'75',
				),
				'medium'=>array(
					'scaleCrop'=>array(
						'width'=>'90',
						'height'=>'90',
					),
					'quality'=>'75',
				),
				'large'=>array(
					'scaleCrop'=>array(
						'width'=>'300',
						'height'=>'300',
					),
					'quality'=>'75',
				),
			),
			$this->styles
		);
	}

	public function generate($filepath, $style)
    {
        $file = new File;

    	$filename = basename($filepath);
    	$dir = $file->getFileDestination().'/thumb/'.$style;
    	$filedest = $dir.'/'.$filename;

    	if ($this->thumbExists($filedest))
    		return $filedest;

    	if (!$this->isValidStyle($style))
    		return false;

    	// Create directory if not already created
    	if (!is_dir($dir) && !$file->createDir(0755, $dir))
    		return false;

        // Retrieve file
        $file = $this->getFile($filepath);

        // Could not load file, return false
        if (!$file || !$file->exists)
            return false;

		$image = Yii::app()->getModule('files')->image->load($file->filepath);

		foreach ($this->styles[$style] as $action=>$args)
		{
			$args = is_array($args) ? $args : (array) $args;
			call_user_func_array(array($image, $action), $args);
		}

		$saved = $image->save($filedest);

		// The file is just a tmp file, delete it
		@unlink($file->filepath);

		if ($saved)
			return $filedest;
    	return false;
    }

    protected function getFile($filepath)
    {
    	$file = new File;
    	$file->service = false;

    	if (substr($filepath, 0, 4) == 'http')
    		$file = $file->retrieve($filepath);
    	else
    		$file = $file->set($filepath)
                         ->copy($file->getTmpFileDestination().'/'.basename($filepath))
                         ->set($file->getTmpFileDestination().'/'.basename($filepath));

    	return $file;
    }

    public function isValidStyle($style)
    {
    	return array_key_exists($style, $this->styles);
    }

    public function thumbExists($filepath)
    {
    	// Return true only if flush is false and file exists;
    	return !$this->flush && file_exists($filepath);
    }
}