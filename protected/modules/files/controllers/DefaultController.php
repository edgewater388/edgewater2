<?php

class DefaultController extends Controller
{
	public function invalidActionParams($action)
	{
		if ($action->id == 'thumb')
			throw new CHttpException(404,'The requested page does not exist.');
		else
			parent::invalidActionParams($action);
	}

	public function actionView($id)
	{
		// $file = Yii::app()->file->set($filepath);
		$file = $this->loadModel($id);
		$f = $file->thumb(50, 50);
		die(var_dump($f));
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionThumb($filename, $style)
	{
		// Load the file model to find out if file is valid, and where it is located
		$model = Files::model()->published()->findByAttributes(
			array('filetype'=>'image'),
			'filename=:filename',
			array(':filename'=>$filename)
		);

		// Return 404 if file does not exists or is invalid (unpublished, not an image, etc)
		if (!$model) throw new CHttpException(404,'The requested page does not exist.');
		
		$filepath = Yii::app()->getModule('files')->thumb->generate(File::getUrl($model->filepath, $model->service), $style);
		
		if ($filepath)
		{
			$img = getimagesize($filepath);
            header('Content-Type: '.$img['mime']);
            readfile($filepath);
            exit;
		}

		throw new CHttpException(404,'The requested page does not exist.');
	}

	public function loadModel($id)
	{
		$model=Files::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionIndex()
	{
		$this->render('index');
	}
}