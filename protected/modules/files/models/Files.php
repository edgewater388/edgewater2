<?php

/**
 * This is the model class for table "file".
 *
 * The followings are the available columns in table 'file':
 * @property string $id
 * @property string $user_id
 * @property string $service
 * @property string $filename
 * @property string $filepath
 * @property string $filemime
 * @property string $filesize
 * @property string $filetype
 * @property integer $status
 * @property string $created
 * @property string $updated
 */
class Files extends ActiveRecord
{
	public $absoluteUrl;
	// public $url;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return File the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'files';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status', 'numerical', 'integerOnly'=>true),
			array('user_id, filesize, created, updated', 'length', 'max'=>10),
			array('service, filetype', 'length', 'max'=>16),
			array('filename, filepath, filemime', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, service, filename, filepath, filemime, filesize, filetype, status, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'service' => 'Service',
			'filename' => 'Filename',
			'filepath' => 'Filepath',
			'filemime' => 'Filemime',
			'filesize' => 'Filesize',
			'filetype' => 'Filetype',
			'status' => 'Status',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	// public function search()
	// {
	// 	// Warning: Please modify the following code to remove attributes that
	// 	// should not be searched.

	// 	$criteria=new CDbCriteria;

	// 	$criteria->compare('id',$this->id,true);
	// 	$criteria->compare('user_id',$this->user_id,true);
	// 	$criteria->compare('service',$this->service,true);
	// 	$criteria->compare('filename',$this->filename,true);
	// 	$criteria->compare('filepath',$this->filepath,true);
	// 	$criteria->compare('filemime',$this->filemime,true);
	// 	$criteria->compare('filesize',$this->filesize,true);
	// 	$criteria->compare('filetype',$this->filetype,true);
	// 	$criteria->compare('status',$this->status);
	// 	$criteria->compare('created',$this->created,true);
	// 	$criteria->compare('updated',$this->updated,true);

	// 	return new CActiveDataProvider($this, array(
	// 		'criteria'=>$criteria,
	// 	));
	// }

	public function afterFind()
	{
		// $this->url = $this->getUrl($this->filepath, $this->service, $this->private);
		$this->absoluteUrl = Yii::app()->createAbsoluteUrl($this->getUrl($this->filepath, $this->service, $this->private));
	}

	public static function upload($model, $attribute, $private=false) {
		$file = Yii::app()->getModule('files')->file->upload(CHtml::resolveName($model, $attribute), $private);

		$fileModel = self::saveFile($file, $private);

		if ($fileModel)
			return $fileModel;

		Flash::setFlash('error', 'Error uploading file. Please try again or contact us.');
		return false;
	}

	public static function saveFile($file, $private=false)
	{
		if ($file && $file->exists)
		{
			$fileModel = new Files;
			$fileModel->service = $file->service;
			$fileModel->filename = $file->basename;
			$fileModel->filepath = $file->filepath;
			$fileModel->filesize = $file->getSize(false);
			$fileModel->filemime = $file->mimeType;
			$fileModel->filetype = $file->filetype;
			$fileModel->private = ($private) ? 1 : 0;
			$fileModel->status = 1;
			
			if ($fileModel->save())
				return $fileModel;
		}

		return false;
	}

	public function thumb($style)
	{
		if ($this->filetype != 'image')
			return false;
	
		// $this->service = (Yii::app()->environment == 'production');
		$this->service = false;
		return $this->createUrl('files/thumb/'.$style.'/'.$this->filename);
	}
	
	public function getUrl()
	{
		return $this->createUrl($this->filepath);
	}

	public function createUrl($filepath)
	{
		// Prefix with CDN if using s3
		$url = ($this->service == 's3') ? Yii::app()->amazon->cdn : '';

		// Append filepath (same directory structure for s3 and local storage)
		$url .= '/'.$filepath;

		// For private files, get the signed url
		if ($this->service == 's3' && $this->private)
			$url = Yii::app()->amazon->getSignedUrl($url);

		return $url;
	}
}