<?php

class ProfileController extends Controller
{
	public $name;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			// array('allow',  // allow all users to perform 'index' and 'view' actions
			// 	'actions'=>array('view','stateoptions'),
			// 	'users'=>array('*'),
			// ),
			['allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => ['manage', 'update'],
				'users'   => ['@'],
			],
			// array('allow',
			// 	'actions'=>array('update','upload'),
			// 	'users'=>array('@'),
			// 	'expression'=>'$user->id===(isset($_GET[\'id\']) ? $_GET[\'id\'] : NULL)',
			// ),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('update','admin','delete', 'index'),
			// 	'roles'=>array('admin'),
			// ),
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Displays a particular model.
	 *
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{

		$model = $this->loadModel($id);
		if (!$model->status) {
			if ($model->user_id === Yii::app()->user->id)
				Flash::setFlash('error', 'Your profile is unpublished and cannot be viewed by others.');
			else if (Yii::app()->authManager->checkAccess('accessBackend', Yii::app()->user->id))
				Flash::setFlash('warning', 'This profile is unpublished and cannot be viewed with out admin priviledges.');
			else
				throw new CHttpException(404, 'The requested page does not exist.');
		}

		$this->pageTitle = $model->first_name . ' ' . $model->last_name . ' - ' . Yii::app()->name;

		$this->render('view', [
			'model' => $model,
		]);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id, $program_id = null)
	{
		// Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->getBaseUrl() . '/js/edit-profile.js');

		// Display the navigation on all settings pages
		$model = $this->loadModel($id);
		$model->scenario = 'update';

		if ($model->user->is_demo)
			$model->scenario = 'updateDemo';

		$this->pageTitle = $model->name;

		if ($model->completed <= 0.40) {
			// Yii::app()->clientScript->registerCSSFile(Yii::app()->theme->getBaseUrl() . '/css/alt-page.css', 'all');
		}

		if (!$model->country)
			$model->country = 'CA';

		if (isset($_POST['Profile'])) {
			foreach ($_POST['Profile'] as $field => $value)
				$model->{$field} = $value;

			$model->birthday = date('Y-m-d', strtotime($model->birthday));

			if (empty($model->weight))
				$model->weight = null;

			if (empty($model->height))
				$model->height = null;
			elseif (is_array($model->height))
				$model->height = implode('.', $model->height);

			if (!empty($program_id))
				$model->current_program_id = $program_id;

			if ($model->save()) {
				if (Yii::app()->request->isAjaxRequest) {

					echo json_encode([
						'error'             => 'false',
						'error_description' => '',
						'response_data'     => 'profile updated',
					]);

					Yii::app()->end();
				}

				Flash::SetFlash('success', 'Profile updated');
				//$this->redirect(array('view','id'=>$model->user_id));

				$this->redirect(Yii::app()->createUrl('/user/profile/view', ['id' => $model->user_id]));
			}
		}

		if (!empty($model->height))
			$model->height = explode('.', $model->height);
		else
			$model->height = explode('.', '4.0');

		$this->render('update', [
			'model' => $model,
		]);
	}

	public function actionManage()
	{
		$model = Profile::model()->findByPk(Yii::app()->user->id);
		$model->new_name = implode(' ', [$model->first_name, $model->last_name]);

		if (isset($_POST['Profile']['new_name'])) {
			$model->scenario = 'updateName';

			foreach ($_POST['Profile'] as $field => $value)
				$model->{$field} = $value;

			$model->birthday = date('Y-m-d', strtotime($model->birthday));

			if (empty($model->weight))
				$model->weight = null;

			if (empty($model->height))
				$model->height = null;
			else
				$model->height = implode('.', $model->height);

			if ($model->save()) {
				$model->name = $model->new_name;
				Flash::setFlash('success', 'Your name has been updated');
				$this->redirect('/account');
			}
		}

		if (!empty($model->height))
			$model->height = explode('.', $model->height);
		else
			$model->height = explode('.', '4.0');

		if (isset($_FILES['Profile'])) {
			// the photo will automatically be uploaded
			if ($model->save()) {
				Flash::setFlash('success', 'Your photo has been updated');
				$this->redirect('/account');
			}
		}

		$this->render('//site/pages/manage-profile', [
			'model'         => $model,
			'fitnessLevels' => Profile::getFitnessLevels(),
			'genders'       => Profile::getGenders(),
		]);
	}

	public function actionUpload($id, $ajax = false)
	{
		$model = $this->loadModel($id);
		$model->scenario = 'upload';

		$response = [
			'success' => false,
			'url'     => null,
			'message' => 'Error uploading photo. Please try again.',
		];

		if (isset($_FILES['Profile'])) {
			if ($model->save() && $model->photo_fid) {
				$response['success'] = true;
				$response['url'] = $model->photoF->thumb('profile_large');
			}
		}

		if ($ajax) {
			echo json_encode($response);
			exit;
		}

		$this->redirect(Yii::app()->createUrl('/user/users/settings', ['id' => $model->user_id]));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['admin']);
		} else
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{

		$dataProvider = new CActiveDataProvider('Profile');
		$this->render('index', [
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new Profile('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Profile']))
			$model->attributes = $_GET['Profile'];

		$this->render('admin', [
			'model' => $model,
		]);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model = Profile::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');

		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 *
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'profile-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionStateoptions($country_code = null)
	{
		$json = [
			'options' => Location::stateOptions($country_code),
		];
		echo json_encode($json);
		exit;
	}
}
