<?php

class UsersController extends Controller
{
	public $name;

	public $isShowLoginIframe = false;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl', // perform access control for CRUD operations
		];
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			['allow',
				'actions' => ['account', 'email', 'password', 'activity', 'register', 'videoSee'],
				'users'   => ['@'],
			],
			['deny',  // deny authenticated users to register
				'actions' => ['register', 'login', 'forgot', 'reset'],
				'users'   => ['@'],
			],
			['allow', // allow anonymous users to register
				'actions' => ['login', 'register', 'confirm', 'forgot', 'reset', 'invite', 'referFriend'],
				'users'   => ['*'],
			],
			['allow',
				'actions' => ['logout'],
				'users'   => ['@'],
			],
			// array('allow', // 'update' actions
			// 	'actions'=>array('settings'),
			// 	'users'=>array('@'),
			// 	'expression'=>'$user->id===(isset($_GET[\'id\']) ? $_GET[\'id\'] : NULL)', // only allow users to update their own
			// ),
			// array('allow', // allow admin user to perform 'admin' and 'delete' actions
			// 	'actions'=>array('settings', 'account', 'create', 'admin','delete'),
			// 	'roles'=>array('admin'),
			// ),
			['deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	public function actionInvite($referral_code)
	{
		Yii::app()->session['referral_code'] = $referral_code;

		if (Yii::app()->user->id)
			$this->redirect('/account');
		$this->redirect('/sign-up');
	}

	public function actionReferFriend()
	{
		if (Yii::app()->user->id)
			$this->redirect('/account#referafriend');

		Yii::app()->user->returnUrl = Yii::app()->request->requestUri;
		$this->redirect('/?login=1&dest=/refer-a-friend');
	}

	public function actionAccount($getinvitelink = null)
	{
		$model = $this->loadModel();

		if ($model->is_demo) {
			return $this->redirect('/sign-up');
		}

		$this->pageTitle = Yii::app()->name . ' - My Account';

		$membership = Yii::app()->user->membership;
		$promo_code = null;

		if ($membership && isset($_POST['Membership']['new_promo_code']) && $promo_code = $_POST['Membership']['new_promo_code']) {
			if ($membership->startPromotion($promo_code)) {
				Flash::setFlash('success', "Promo {$promo_code} has been successfully activated.");
			} else {
				$membership->scenario = 'new_promo_code';
				$membership->new_promo_code = $promo_code;
				$membership->addError('new_promo_code', 'Invalid or expired promo code');
				Flash::setFlash('error', 'Invald or expired promo code');
			}
		}

		$referral_invite = new ReferralInvite('create');

		if (isset($_POST['ReferralInvite']['email'])) {
			$emails = explode(',', $_POST['ReferralInvite']['email']);
			$emails = array_slice($emails, 0, 10); // first 10
			$sent = [];
			$notsent = [];

			foreach ($emails as $k => $email) {
				$email = trim($email);
				$referral_invite = ReferralInvite::invite($email);

				if ($referral_invite->id)
					$sent[] = $email;
				else
					$notsent[] = $email;
			}

			if ($sent) {
				Flash::setFlash('success', 'You have successfully sent an invite to ' . implode(', ', $sent) . '.');
				$referral_invite = new ReferralInvite('create');
			}

			if ($notsent) {
				Flash::setFlash('success', 'We were unable to invite ' . implode(', ', $notsent) . '.');
				$referral_invite = new ReferralInvite('create');
			}
		}

		if ($getinvitelink) {
			$user = Users::model()->findByPk(Yii::app()->user->id);
			$user->getReferralCode(true);
			$this->redirect('/account#referafriend');
		}

		$this->render('//site/pages/account', [
			'model'           => $model,
			'membership'      => $membership,
			'customer'        => Yii::app()->user->customer,
			'referral_invite' => $referral_invite,
		]);
	}

	public function actionEmail()
	{
		$model = $this->loadModel();
		$model->scenario = 'updateEmail';

		if (Users::checkIfSocial()) {
			$model->scenario = 'updateSocialEmail';
		}

		$model->email = '';

		if (isset($_POST['Users'])) {
			$model->email = isset($_POST['Users']['email']) ? $_POST['Users']['email'] : null;
			$model->current_password = isset($_POST['Users']['current_password']) ? $_POST['Users']['current_password'] : null;

			if ($model->save()) {
				Flash::setFlash('success', 'Your email has been updated');
				$this->redirect('/account');
			}
		}

		$this->render('//site/pages/change-email', [
			'model' => $model,
		]);
	}

	public function actionPassword()
	{
		if (Users::checkIfSocial()) {
			throw new CHttpException('404', 'Page not found.');
		}

		$model = $this->loadModel();
		$model->scenario = 'updatePassword';

		if (isset($_POST['Users'])) {
			$model->current_password = isset($_POST['Users']['current_password']) ? $_POST['Users']['current_password'] : null;
			$model->new_password = isset($_POST['Users']['new_password']) ? $_POST['Users']['new_password'] : null;
			$model->password2 = isset($_POST['Users']['password2']) ? $_POST['Users']['password2'] : null;

			if ($model->save()) {
				Flash::setFlash('success', 'Your password has been updated');
				$this->redirect('/account');
			}
		}

		$this->render('//site/pages/change-password', [
			'model' => $model,
		]);
	}

	/**
	 * Settings Dashboard / forms
	 */
	// public function actionSettings($id, $type = null)
	// {
	// 	$this->pageTitle = 'My Account';

	// 	$model = $this->loadModel($id)->with('profile');
	// 	$model->scenario = 'update';		

	// 	if ($type)
	// 		$type = strtolower($type);

	// 	if ($type == 'password')
	// 		$model->scenario = 'updatePassword';

	// 	if (isset($_POST['Users']))
	// 	{
	// 		$model->attributes=$_POST['Users'];
	// 		if($model->save()) {
	// 			Flash::setFlash('success', 'Changes Saved');
	// 			$this->redirect(Yii::app()->createUrl('user/users/settings', array('id'=>$model->id)));
	// 		}
	// 	}
	// 	if (isset($_POST['Profile']))
	// 	{
	// 		$model->profile->attributes=$_POST['Profile'];
	// 		if($model->profile->save()) {
	// 			Flash::setFlash('success', 'Changes Saved');
	// 			$this->redirect(Yii::app()->createUrl('user/users/settings', array('id'=>$model->id)));
	// 		}
	// 	}

	// 	// Display the navigation on all settings pages
	// 	$this->name=$model->profile->first_name.' '.$model->profile->last_name;

	// 	$dashboardItems = array(
	// 		'name'=>array(
	// 			'label'=>'Name',
	// 			'desc'=>$model->profile->first_name.' '.$model->profile->last_name,
	// 			'edit_url'=>Yii::app()->createUrl('user/users/settings',array('id'=>$model->id,'type'=>'name')),
	// 		),
	// 		'email'=>array(
	// 			'label'=>'Email',
	// 			'desc'=>'<span class="email">'.$model->email.'</span>',
	// 			'edit_url'=>Yii::app()->createUrl('user/users/settings',array('id'=>$model->id,'type'=>'email')),
	// 		),
	// 		'password'=>array(
	// 			'label'=>'Password',
	// 			'desc'=>'********',
	// 			'edit_url'=>Yii::app()->createUrl('user/users/settings',array('id'=>$model->id,'type'=>'password')),
	// 		),
	// 		// 'editprofile'=>array(
	// 		// 	'label'=>'Profile',
	// 		// 	'desc'=>'Customize your profile',
	// 		// 	'edit_url'=>Yii::app()->createUrl('user/profile/update',array('id'=>$model->id)),
	// 		// ),
	// 	);

	// 	/**
	// 	 * Facebook Connect users cannot modify their email address from within JazzCuts
	// 	 * @todo, syncronize their facebook email with the JazzCuts email.. 
	// 	 * Users do not require a password, so do not allow users to edit password.
	// 	 */
	// 	$settings = array();
	// 	$settings[] = 'name';
	// 	$settings[] = 'email';
	// 	$settings[] = 'password';

	// 	if ($model->reg_vendor == 'facebook')
	// 	{
	// 		$dashboardItems['email']['edit_url'] = '';
	// 		$dashboardItems['password']['label'] = 'Sign In';
	// 		$dashboardItems['password']['desc'] = 'Facebook Connect';
	// 		$dashboardItems['password']['edit_url'] = '';

	// 		$settings = array();
	// 		$settings[] = 'name';
	// 	}

	// 	if (in_array($type, $settings)) {
	// 		$dashboardItems[$type]['render'] = 'settings/'.$type;
	// 		$dashboardItems[$type]['cancel_url'] = Yii::app()->createUrl('user/users/settings',array('id'=>$id));
	// 	}

	// 	if(!isset($_POST['Users']) && $type == 'password')
	// 		$model->password = '';

	// 	$this->render('settings/dashboard',array(
	// 		'model'=>$model,
	// 		'dashboardItems'=>$dashboardItems,
	// 	));
	// }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionRegister()
	{

		if (!Yii::app()->user->isGuest) {
			if (Yii::app()->user->identity->is_demo) {
				Yii::app()->user->logout();
			} else {
				return $this->redirect(['/']);
			}
		}

		$model = new Users;
		$model->scenario = 'register';

		if (Yii::app()->session['referral_code'])
			$model->referred_by = Yii::app()->session['referral_code'];

		if (isset($_POST['Users'])) {
			$model->attributes = $_POST['Users'];

			if (!Users::model()->byAttribute('email', $model->email)->exists()) {
				$model->status = 1;
				$model->confirmation_key = $model->createConfirmationKey();

				if ($model->save()) {
					$profile = new Profile;
					$profile->user_id = $model->id;
					$profile->status = 1;
					$profile->save();

					// Set password confirmation message, also send password confirmation email.
					// Flash::setFlash('success', '<b>'.$profile->first_name.'</b>, confirm your email address ('.$model->email.') by clicking on the confirmation link in the email we\'ve just sent.');

					// Send a confirm email
					// $message = new MailMessage;
					// $message->view = 'registrationConfirm';
					// $message->addTo($model->email, $model->email);
					// $message->subject = 'Confirm your '.Yii::app()->name.' account';
					// $message->tagline = 'Please confirm your '.Yii::app()->name.' account';
					// $message->setBody(array('user'=>$model));
					// $sent = Yii::app()->mail->send($message);
				}
			}

			$loginForm = new LoginForm;
			$loginForm->attributes = $_POST['Users'];

			// After registering, the user should then be logged in
			if ($loginForm->validate() && $loginForm->login())
				$this->redirect(['/customer/create']);
			else
				$model->addErrors($loginForm->getErrors());
		}

		$this->render('//site/pages/sign-up', [
			'model' => $model,
		]);
	}

	public function actionConfirm($email, $code = null)
	{
		$valid = false;

		if ($code) {
			$model = Users::model()->find(
				'email=:email AND confirmation_key=:code',
				[
					':email' => $email,
					':code'  => $code,
				]);

			if ($model) {
				$model->confirmation_key = null;
				$model->confirmed = 1;
				$valid = $model->save(false);
			}
		}

		if ($valid)
			Flash::setFlash('success', 'Your email address has been confirmed.');
		else
			Flash::setFlash('error', 'Missing or invalid code');

		if (Yii::app()->user->isGuest)
			$this->redirect('/login');
		else
			$this->redirect('/account/' . Yii::app()->user->id);
	}

	public function actionReset($email, $code = null)
	{
		$valid = false;

		// if (!$code)
		// {
		// 	Flash::setFlash('error', 'Missing password reset code.');
		// 	$this->redirect(array('users/forgot','email'=>$email));
		// }

		if ($code) {
			$model = Users::model()->find(
				'email=:email AND forgot_key=:code',
				[
					':email' => $email,
					':code'  => $code,
				]);

			if ($model)
				$valid = true;
		}

		if (!$valid) {
			Flash::setFlash('error', 'Missing or invalid code.');
			$this->redirect(['/user/users/login']);
		}

		if (isset($_POST['Users']['new_password']) && isset($_POST['Users']['password2'])) {
			$model->scenario = 'resetPassword';
			$model->new_password = $_POST['Users']['new_password'];
			$model->password2 = $_POST['Users']['password2'];
			$model->forgot_key = null;

			if ($model->save()) {
				Flash::setFlash('success', 'Your password has been updated.');
				// $this->redirect('/login');

				$loginForm = new LoginForm;
				$loginForm->email = $model->email;
				$loginForm->password = $model->new_password;
				if ($loginForm->validate())
					$loginForm->login();

				$this->redirect(['/user/users/account']);

				{

				}
			} else
				Flash::setFlash('error', 'We were unable to reset your password.');
		}

		$this->render('//site/pages/reset-password', [
			'model' => $model,
		]);
	}

	public function actionForgot($email = null)
	{
		$model = new Users;

		if ($email)
			$model->email = $email;

		// die(var_dump($_POST));
		if (isset($_POST['Users']['email'])) {
			$model->email = $_POST['Users']['email'];
			$userModel = Users::model()->find(
				'email=:email',
				[
					':email' => $model->email,
				]);

			if ($userModel) {
				$userModel->forgot_key = $userModel->createPasswordResetKey();

				if ($userModel->save()) {
					$message = new MailMessage;
					$message->view = 'forgotPassword';
					$message->subject = 'Request to reset your ' . Yii::app()->name . ' password';
					$message->addTo($userModel->email, $userModel->profile->name);
					$message->setBody([
						'user' => $userModel,
					]);
					$sent = Yii::app()->mail->send($message);

					if ($sent)
						Flash::setFlash('success', 'An email has been sent to <strong>' . $userModel->email . '</strong> with instructions to reset your password.');
					else
						Flash::setFlash('error', 'We were unable to send the email. Please try again or contact us.');
				} else {
					Flash::setFlash('error', 'We were unable to process your request. Please try again or contact us.');
				}
			} else {
				Flash::setFlash('error', 'The email address <strong>' . $model->email . '</strong> is not registered.');
			}
		}

		$this->render('//site/pages/forgot-password', ['model' => $model]);
	}

	public function actionActivity()
	{
		$model = new VideoPlayLog('search');
		$model->attributes = $_GET;
		$model->user_id = Yii::app()->user->id;

		if (isset($_GET['page']))
			$_GET['VideoPlayLog_page'] = $_GET['page'];

		$dataProvider = $model->search(10);

		$this->render('//site/pages/viewing-activity', [
			'models'      => $dataProvider->getData(),
			'total_count' => $dataProvider->totalItemCount,
		]);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel()
	{
		$model = Users::model()->findByPk(Yii::app()->user->id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');

		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 *
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		// $facebookIdentity = new FacebookIdentity();
		// if (isset($_REQUEST['code'])) {
		// 	// attempt to authenticate user via facebook
		// 	$facebookIdentity->authenticate();
		// 	$duration= 3600*24*30;
		// 	$success = Yii::app()->user->login($facebookIdentity, $duration);

		// 	if ($success)
		// 		Flash::setFlash('success', 'You have successfully logged in with your Facebook account.');
		// 	else
		// 		$this->redirect(array('/user/users/register'));

		// 	$dest = (isset($_GET['dest'])) ? $_GET['dest'] : Yii::app()->user->returnUrl; // dest is sent as a parameter for current page url to facebook

		// 	$url = parse_url(urldecode($_GET['dest']));

		// 	if (isset($url['query']))
		// 	{
		// 		if (substr($url['query'],0,4) == 'dest')
		// 			$dest = substr($url['query'],5);
		// 	}

		// 	if ($dest == '/register')
		// 		$dest = '/subscription/create';
		// 	else if ($dest == '/login' || $dest == '' || $dest == '/')
		// 	{
		// 		$dest = Yii::app()->createUrl('/');
		// 	}

		// 	$this->redirect($dest);
		// }

		$model = new LoginForm;

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()) {
				$user = Yii::app()->user;
				if ($auto_play_video = Yii::app()->session['auto_play_video'])
					$this->redirect('/?video=' . $auto_play_video);

				if ($auto_play_mashup = Yii::app()->session['auto_play_mashup'])
					$this->redirect('/?mashup=' . $auto_play_mashup);

				//Flash::setFlash('success', 'You have successfully logged in.');

				// $dest = (isset($_GET['dest'])) ? $_GET['dest'] : Yii::app()->user->returnUrl; // dest is sent as a parameter for current page url to facebook
				if (!$dest = Yii::app()->request->getParam('dest'))
					$dest = '/';

				if ($dest == '/login')
					$dest = '/';

				if (empty(Yii::app()->user->customer) && !empty($user->membership) && empty($user->membership->prev_billing_date) && ($userMemberShipTrialEndDate = $user->membership->trial_end_date) != null && $userMemberShipTrialEndDate < date('Y-m-d')) {
					$this->redirect('/update-payment-info');
				}
				
				$this->redirect($dest);
			} else {
				$this->render('//site/index', [
					'model' => $model,
				]);
				exit;
			}
		}

		if (!$dest = Yii::app()->request->getParam('dest'))
			$dest = '/';
		
		$this->redirect($dest . '?login=1');

		$this->render('login', [
			'model' => $model,
		]);
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	/**
	 * Ajax action to set up that user already see video.
	 */
	public function actionVideoSee()
	{
		if (Yii::app()->request->isAjaxRequest) {
			if (($user = Users::model()->findByPk(Yii::app()->user->identity->id)) != null) {
				$user->is_see_video = true;
				$user->save();
			} 
			return true;
		} 
		throw new CHttpException(404,'The requested page does not exist.');
	}
}
