<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
	public $email;
	public $password;
	public $rememberMe;

	private $_identity;

	public function init()
	{
		$cookie = Yii::app()->request->cookies['email'];

		$class = get_class(Yii::app()->user);
		if ($class != 'AdminUser' && isset($cookie->value)) {
			$this->email = $cookie->value;

			if (Yii::app()->user->allowAutoLogin)
				$this->rememberMe = 1;
		}
	}

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return [
			// username and password are required
			['email, password', 'required'],
			// rememberMe needs to be a boolean
			['rememberMe', 'boolean'],
			// password needs to be authenticated
			['password', 'authenticate'],
		];
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return [
			'rememberMe' => 'Remember Me?',
		];
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute, $params)
	{
		unset(Yii::app()->request->cookies['email']);

		if (!$this->hasErrors()) {
			$this->_identity = new UserIdentity($this->email, $this->password);
			if (!$this->_identity->authenticate()) {
				if (isset($this->_identity->errorMessage) && $this->_identity->errorMessage !== '')
					$this->addError('password', $this->_identity->errorMessage);
				else {
					if (substr(Yii::app()->request->requestUri, 1, 8) == 'sign-up')
						$this->addError('email', 'An account already exists for ' . $this->email . '. <a href="' . Yii::app()->createUrl('user/users/forgot', ['email' => $this->email]) . '">Forgot your password?</a>');
					else
						$this->addError('password', 'Incorrect password for ' . $this->email . '. <a href="' . Yii::app()->createUrl('user/users/forgot', ['email' => $this->email]) . '">Forgot your password?</a>');
				}
			}
		}
	}

	/**
	 * Logs in the user using the given email and password in the model.
	 *
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if ($this->_identity === null) {
			$this->_identity = new UserIdentity($this->email, $this->password);
			$this->_identity->authenticate();
		}
		if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
			$duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
			Yii::app()->user->login($this->_identity, $duration);

			return true;
		} else
			return false;
	}
}
