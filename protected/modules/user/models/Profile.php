<?php

/**
 * This is the model class for table "profile".
 * The followings are the available columns in table 'profile':
 *
 * @property string $user_id
 * @property string $first_name
 * @property string $last_name
 * @property integer $status
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class Profile extends ActiveRecord
{
	public $dob = ['m', 'd', 'y'];
	public $name = '';
	public $location;
	public $cur_photo_fid;
	public $photo;
	public $new_name;
	public $current_program_id;

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @return Profile the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'profile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			['first_name, last_name', 'required', 'on' => 'register, update, updateName', 'except' => 'updateDemo'],
			// array('user_id, country, state, city, description, phone', 'required','on'=>'update'),
			// array('photo_fid', (Yii::app()->user->isGuest == false && Yii::app()->user->profile->photo_fid) ? 'safe' : 'required', 'on'=>'update', 'message'=>'Please upload a photo'),
			// array('photo_fid', 'required', 'on'=>'userCreate'),
			['photo', 'file', 'types' => 'jpg,jpeg,png', 'allowEmpty' => true],
			['status, do_not_show_weight_popup', 'numerical', 'integerOnly' => true],
			['user_id, photo_fid', 'length', 'max' => 11],
			['salutation', 'length', 'max' => 8],
			['first_name, last_name, company', 'length', 'max' => 50],
			['street, street2, state, facebook, google_plus, twitter, youtube', 'length', 'max' => 64],
			['city', 'length', 'max' => 32],
			['country', 'length', 'max' => 2],
			['zip_code', 'length', 'max' => 6],
			['gender, do_not_show_weight_popup', 'length', 'max' => 1],
			['phone, phone2', 'length', 'max' => 20],
			['created, updated', 'length', 'max' => 10],
			['birthday, description', 'safe'],
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			['user_id, salutation, first_name, last_name, company, street, street2, city, country, state, zip_code, phone, phone2, birthday, status, created, updated', 'safe', 'on' => 'search'],
			['new_name', 'required', 'on' => 'updateName'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return [
			'user'   => [self::BELONGS_TO, 'Users', 'user_id'],
			'photoF' => [self::BELONGS_TO, 'Files', 'photo_fid'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'user_id'     => 'User',
			'salutation'  => 'Salutation',
			'first_name'  => 'First Name',
			'last_name'   => 'Last Name',
			'company'     => 'Company',
			'street'      => 'Street',
			'street2'     => 'Street2',
			'city'        => 'City',
			'country'     => 'Country',
			'state'       => 'Province/State',
			'zip_code'    => 'Postal/Zip Code',
			'phone'       => 'Phone Number',
			'phone2'      => 'Phone (Secondary)',
			'birthday'    => 'Birthday',
			'gender'      => 'Gender',
			'description' => 'Bio',
			'facebook'    => 'Facebook',
			'twitter'     => 'Twitter',
			'youtube'     => 'YouTube',
			'photo_fid'   => 'Photo',
			'status'      => 'Status',
			'created'     => 'Created',
			'updated'     => 'Updated',
		];
	}

	/**
	 * @return array
	 */
	public static function getFitnessLevels()
	{
		return [
			1 => 'New',
			2 => 'Beginner',
			3 => 'Intermediate',
			4 => 'Advanced',
		];
	}

	/**
	 * @param $level_id
	 *
	 * @return string|null
	 */
	public function getFitnessLevel()
	{
		$levels = self::getFitnessLevels();

		return isset($levels[$this->fitness_level]) ? $levels[$this->fitness_level] : null;
	}

	public static function getGenders()
	{
		return [
			'm' => 'Male',
			'f' => 'Female',
		];
	}

	public function getGender()
	{
		$levels = self::getGenders();

		return isset($levels[$this->gender]) ? $levels[$this->gender] : null;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchCriteria()
	{
		$criteria = parent::searchCriteria();

		$criteria->compare('salutation', $this->salutation, true);
		$criteria->compare('first_name', $this->first_name, true);
		$criteria->compare('last_name', $this->last_name, true);
		$criteria->compare('company', $this->company, true);
		$criteria->compare('street', $this->street, true);
		$criteria->compare('street2', $this->street2, true);
		$criteria->compare('city', $this->city, true);
		$criteria->compare('country', $this->country, true);
		$criteria->compare('state', $this->state, true);
		$criteria->compare('zip_code', $this->zip_code, true);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('phone2', $this->phone2, true);
		$criteria->compare('birthday', $this->birthday, true);
		$criteria->compare('photo_fid', $this->photo_fid, true);

		return $criteria;
	}

	public function beforeValidate()
	{
		$valid = parent::beforeValidate();

		if ($this->scenario == 'updateName') {
			if ($this->new_name) {
				$name = CString::splitName($this->new_name);
				$this->first_name = $name[0];
				$this->last_name = $name[1];
			}
		}

		// $file_fields = array(
		// 	'photo_fid',
		// );

		// foreach ($file_fields as $field)
		// {
		// 	$current_field = 'cur_'.$field;

		// 	// Only upload / create files if file there is a file
		// 	if (isset($_FILES['Profile']['tmp_name'][$field]) && $_FILES['Profile']['tmp_name'][$field])
		// 	{
		// 		$file = new Files;
		// 		$saved = $file->saveFile($this, $field);

		// 		$success &= $saved;

		// 		if ($saved)
		// 			$this->$field = $file->id;
		// 		else
		// 			$this->$field = $this->$current_field;
		// 		//$errors = $file->getErrors();
		// 	}
		// 	else
		// 		$this->$field = $this->$current_field;
		// }

		return $valid;
	}

	public function beforeSave()
	{
		$saved = parent::beforeSave();
		// if ($this->isNewRecord) {
		// 	$this->created = time();
		// }

		$this->completed = $this->_calculateComplete();

		if (!empty($this->current_program_id)) {
			$user = $this->findByPk($this->user_id);

			$u2r = UserRegimen::model()->find('user_id = :user_id AND regimen_id = :regimen_id', [
				':user_id'    => $this->user_id,
				':regimen_id' => $this->current_program_id,
			]);

			if ($user) {
				$old_weight = $user->weight;
			}

			if (!empty($old_weight)) {
				$u2r->old_weight = $old_weight;
			} else {
				$u2r->old_weight = $this->weight;
			}

			$u2r->save();
		}

		if (is_array($this->height)) {
			$this->height = implode('.', $this->height);
		}


		// $this->updated = time();

		return $saved;
	}

	public function afterSave()
	{
		$saved = parent::afterSave();

		// Update the users session after they update their info
		if (Yii::app()->user->id == $this->user_id) {
			Yii::app()->user->setState('name', $this->first_name . ' ' . $this->last_name);
			Yii::app()->user->setState('profile', $this);
		}

		return $saved;
	}

	public function afterFind()
	{
		parent::afterFind();

		$this->name = $this->first_name . ' ' . $this->last_name;
		// $this->cur_photo_fid = $this->photo_fid;

		// Format the users' location
		$this->location = $this->city;
		if ($this->location && $this->state)
			$this->location .= ', ';
		$this->location .= $this->state;

		if (Yii::app()->user->id == $this->user_id)
			Yii::app()->user->setState('profile', $this);

		return true;
	}

	public function scopes()
	{
		return [
			'published'   => [
				'condition' => 't.status=1',
			],
			'unpublished' => [
				'condition' => 't.status=0',
			],
			'recently'    => [
				'order' => 't.updated DESC',
				'limit' => 5,
			],
		];
	}

	// Parameterized Scope
	public function limit($limit = 10)
	{
		$this->getDbCriteria()->mergeWith([
			'limit' => $limit,
		]);

		return $this;
	}

	public function getMonthOptions($size = null)
	{
		return ['' => 'Month'] + Yii::app()->locale->getMonthNames($size);
	}

	public function getDayOptions()
	{
		$days = ['' => 'Day'];
		for ($i = 1; $i <= 31; $i++)
			$days[$i] = $i;

		return $days;
	}

	public function getYearOptions($minAge = null)
	{
		$cur_year = date('Y', time());

		$years = ['' => 'Year'];
		for ($i = $cur_year - $minAge; $i >= $cur_year - 105; $i--) {
			$years[$i] = $i;
		}

		return $years;
	}

	public function getSalutations()
	{
		return [
			''     => '',
			'mr'   => 'Mr.',
			'mrs'  => 'Mrs.',
			'miss' => 'Miss',
			'ms'   => 'Ms.',
			'dr'   => 'Dr.',
			'prof' => 'Prof.',
		];
	}

	private function _calculateComplete()
	{
		$fields = [
			'first_name'  => 20,
			'last_name'   => 20,
			'description' => 20,
			'country'     => 10,
			'city'        => 10,
			'street'      => 5,
			'gender'      => 5,
			'birthday'    => 5,
			'twitter'     => 5,
		];

		$score = 0;
		$max_score = 0;

		foreach ($fields as $field => $field_score) {
			if ($this->$field != null)
				$score += $field_score;
			$max_score += $field_score;
		}

		return round($score / $max_score, 2);
	}

	public function getThumbnailUrl($style = 'small')
	{
		if ($this->photoF)
			return $this->photoF->thumb($style);

		return '/themes/basic/images/global/profile-pic-generic.png';
	}

	public function getWeightChange($program_id)
	{
		$u2r = UserRegimen::model()->find('user_id = :user_id AND regimen_id = :regimen_id', [
			':user_id'    => $this->user_id,
			':regimen_id' => $program_id,
		]);

		if (!empty($u2r->old_weight)) {
			return $u2r->old_weight - $this->weight;
		}

		return 0;
	}

	public function getProfileIsEmpty()
	{
		$filled = [$this->birthday, $this->height, $this->weight, $this->gender, $this->fitness_level];

		return count(array_filter($filled)) !== count($filled);
	}
	
	//fields for rest api
	public function fields() {
        return [
			'first_name',
			'last_name',
			'company',
			'street',
			'street2',
			'city',
			'state',
			'country',
			'zipcode',
			'phone',
			'phone2',
			'birthday',
			'gender'
		];
	}
	
}
