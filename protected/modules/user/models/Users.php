<?php

/**
 * This is the model class for table "users".
 * The followings are the available columns in table 'users':
 *
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $provider
 * @property string $facebook_id
 * @property string $sessionkey
 * @property string $lastvisit
 * @property string $confirmation_key
 * @property integer $terms_accepted
 * @property string $settings
 * @property integer $confirmed
 * @property string $forgot_key
 * @property string $deleted
 * @property string $status
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 */
class Users extends BaseRecord
{
	public $name;
	public $name_or_email;
	public $email2;
	public $new_password;
	public $current_password;
	public $password2;
	public $created_from;
	public $created_to;
	public $is_speaker;
	public $company_id;
	public $with_company;
	public $company;
	public $is_demo;

	protected $_email;

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @return Users the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			// array('email, email2, password, password2', 'required', 'on'=>'register'),
			['email', 'required', 'except' => 'facebook'],
			['password', 'required', 'on' => 'register, create, import, validate_step_1'],
			['current_password, new_password, password2', 'required', 'on' => 'updatePassword'],
			['current_password', 'required', 'on' => 'updateEmail'],

			['referred_by', 'length', 'max' => 16],

			// array('terms_accepted', 'required', 'requiredValue'=>1, 'message'=>'Terms & Conditions must be accepted.', 'on'=>'register'),
			['terms_accepted, confirmed, lastvisit, status', 'numerical', 'integerOnly' => true],
			['email, password, sessionkey', 'length', 'max' => 128],
			['password, new_password', 'length', 'min' => 6],
			['email', 'unique', 'message' => '{value} is already registered. <a href="' . Yii::app()->createUrl('user/users/forgot') . '">Forgot your password?</a>', 'on' => 'register, validate_step_1'],
			['email', 'unique', 'message' => '{value} is already registered. Please enter a different email address.', 'on' => 'create, update, updateEmail, updateSocialEmail, adminUpdate, import'],
			//array('email', 'unique', 'message'=>'{value} is already registered. If you would like to login with your Facebook account, you may link your account under <a href="/myaccount#facebook">Account Settings</a>', 'on'=>'facebook'),
			['email', 'unique', 'message' => '{value} is already registered. <a href="' . Yii::app()->createUrl('user/users/forgot') . '">Forgot your password?</a>', 'on' => 'facebook, google_plus'],
			['facebook_id', 'unique', 'message' => 'Your Facebook ID has already been connected.', 'on' => 'facebook'],
			['google_id', 'unique', 'message' => 'Your Google Plus ID has already been connected.', 'on' => 'google_plus'],
			// array('email2', 'compare', 'compareAttribute' => 'email', 'message'=>'Emails do not match.', 'on'=>'register'),
			// array('password2', 'compare', 'compareAttribute' => 'password', 'message'=>'Passwords do not match.', 'on'=>'register'),
			['password2', 'compare', 'compareAttribute' => 'new_password', 'message' => 'Passwords do not match.', 'on' => 'updatePassword, resetPassword'],
			['name, created, updated, deleted, is_demo, is_see_video', 'safe'],
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			['id, name, email, name_or_email, created, created_from, created_to, lastvisit, status, is_speaker, company_id, company, with_company', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return [
			//'authitems'        => [self::MANY_MANY, 'Authitem', 'AuthAssignment(userid, itemname)'],
			'files'            => [self::HAS_MANY, 'Files', 'user_id'],
			'profile'          => [self::HAS_ONE, 'Profile', 'user_id'],
			'customer'         => [self::HAS_ONE, 'Customer', 'user_id'],
			'transactionCount' => [self::STAT, 'Transaction', 'user_id', 'select' => 'count(*)'],
			'renewalCount'     => [self::STAT, 'Transaction', 'user_id', 'select' => 'count(*)', 'condition' => 'approved=1'],
		];
	}

	/**
	 * @return float
	 */
	public function getAge()
	{
		$diff = abs(strtotime('today') - strtotime($this->profile->birthday));

		return floor($diff / (365 * 60 * 60 * 24));
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id'               => 'ID',
			'email'            => 'Email',
			'email2'           => 'Re-enter Email',
			'password'         => 'Password',
			'new_password'     => 'New Password',
			'password2'        => 'Re-enter Password',
			'sessionkey'       => 'Sessionkey',
			'confirmation_key' => 'Confirmation Key',
			'forgot_key'       => 'Forgot Password Key',
			'confirmed'        => 'Confirmed',
			'terms_accepted'   => 'Terms Accepted',
			'lastvisit'        => 'Last Visit',
			'status'           => 'Status',
			'created'          => 'Created',
			'updated'          => 'Updated',

			// custom
			'payoutsSum'       => 'Total Paid Out',
			'payoutsCount'     => 'Number of Payouts',
			'royaltiesSum'     => 'Total Royalties',
			'royaltiesCount'   => 'Number of Royalties',
		];
	}

	public function behaviors() {
		return [
			'EJsonBehavior'=> [
				'class'=>'application.extensions.behaviors.EJsonBehavior'
			],
	    ];
	}
	
	public function getClassLabel()
	{
		return 'User';
	}

	public function getDisplayTitle()
	{
		return isset($this->profile) ? $this->profile->name : parent::getDisplayTitle();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($pageSize = 25)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		return new CActiveDataProvider($this, [
			'criteria'   => $this->searchCriteria(),
			'pagination' => [
				'pageSize'            => $pageSize,
				'validateCurrentPage' => false,
			],
		]);
	}

	public function searchCriteria()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('t.id', $this->id, false);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('t.created', $this->created);
		// $criteria->compare('lastvisit',$this->lastvisit);
		// $criteria->compare('t.status',$this->status);

		if ($this->name) {
			$nameCriteria = new CDbCriteria;
			$nameCriteria->with = 'profile';
			$nameCriteria->compare('CONCAT(profile.first_name," ",profile.last_name)', $this->name, true, 'OR');
			$criteria->mergeWith($nameCriteria);
		}

		if ($this->created_from) {
			$created_to = date('Y-m-d', strtotime($this->created_to . ' + 1 day'));
			$criteria->addBetweenCondition('t.created', $this->created_from, $created_to);
		}

		if ($this->name_or_email) {
			$nameCriteria = new CDbCriteria;
			$nameCriteria->with = 'profile';
			$nameCriteria->compare('CONCAT(profile.first_name," ",profile.last_name)', $this->name_or_email, true, 'OR');
			$nameCriteria->compare('email', $this->name_or_email, true, 'OR');
			$criteria->mergeWith($nameCriteria);
		}

		if ($this->q) {
			$nameCriteria = new CDbCriteria;
			$nameCriteria->with = 'profile';
			$nameCriteria->compare('CONCAT(profile.first_name," ",profile.last_name)', $this->q, true, 'OR');
			$nameCriteria->compare('email', $this->q, true, 'OR');
			$criteria->mergeWith($nameCriteria);
		}

		$nameCriteria = new CDbCriteria;
		$nameCriteria->condition = 'is_demo = 0';
		$criteria->mergeWith($nameCriteria);

		// if ($this->q)
		// {
		// 	$nameCriteria=new CDbCriteria;
		// 	$nameCriteria->with = 'profile';
		// 	$nameCriteria->compare('CONCAT(profile.first_name," ",profile.last_name)',$this->q,true,'OR');
		// 	$nameCriteria->compare('email', $this->q, true, 'OR');
		// 	$criteria->mergeWith($nameCriteria);
		// }

		// $criteria->order = 't.created ASC';
		$criteria->order = 't.id ASC';

		return $criteria;
	}

	public function scopes()
	{
		return CMap::mergeArray(parent::scopes(), [
			'sortByName' => [
				'order' => 'profile.first_name ASC, profile.last_name ASC',
			],
			'is_artist'  => [
				'with'      => 'profile',
				'condition' => 'profile.type != "user"',
			],
		]);
	}

	public function beforeValidate()
	{
		$valid = parent::beforeValidate();

		// if ($this->provider == 'facebook' && isset($this->_oldAttributes['email']) && $this->_oldAttributes['email'] != $this->email)
		// {
		// 	$this->addError('email', 'Email address cannot be changed for accounts created via Facebook Connect');
		// 	$valid = false;
		// }

		if ($this->scenario == 'updateEmail' || $this->scenario == 'updatePassword') {
			if ($this->current_password && !$this->verifyCurrentPassword($this->id, $this->current_password))
				$this->addError('current_password', 'The password entered is incorrect. Please enter your current password.');
		}

		return $valid;
	}

	public function beforeSave()
	{
		$valid = parent::beforeSave();

		$valid &= $this->validate();
		if (!$valid)
			return;

		// Encrypt the password on user registration / account creation
		if ($this->scenario == 'register' || $this->scenario == 'create') {
			$this->password = $this->encryptPassword($this->password);
		} // Encrypt the new password if the user is updating their password
		else if ($this->new_password) {
			$this->password = $this->encryptPassword($this->new_password);
		}

		return $valid;
	}

	public function afterSave()
	{
		parent::afterSave();

		if (Yii::app()->user->id == $this->id) {
			Yii::app()->user->setState('email', $this->email);
			Yii::app()->user->setState('profile', $this->profile);
		}

		// referrals come from two ways, 1) special referral link 2) email invitiation
		// referral link takes priority
		if ($this->scenario == 'register') {
			if ($this->referred_by) {
				$referral_code = $this->referred_by;
				$user_id = Yii::app()->db->createCommand('SELECT id FROM users WHERE referral_code = :referral_code')->bindParam(':referral_code', $referral_code)->queryScalar();

				if ($user_id)
					ReferralBank::add(null, $user_id, $this->id);
				else
					ReferralInvite::awardReferral($this->email, $this->id);
			} else {
				// reward the first valid invite of this email, if any
				ReferralInvite::awardReferral($this->email, $this->id);
			}
		}
	}

	// public function afterFind()
	// {
	// 	parent::afterFind();
	// }

	public function beforeDelete()
	{
		$valid = parent::beforeDelete();

		$this->profile->delete();

		return $valid;
	}

	public function getUsersByRole($role_name)
	{
		$users = $this->model()->with('profile', 'authAssignment')->findAll('authAssignment.itemname=:role_name', [':role_name' => $role_name]);

		$rows = [];
		foreach ($users as $user) {
			if (isset($user->profile->firstname))
				$rows[$user->id] = $user->profile->firstname . ' ' . (isset($user->profile->lastname) ? $user->profile->lastname : '');
		}

		return $rows;
	}

	public static function getUsers($limit = 10)
	{
		return CHtml::listData(Users::model()->published()->with('profile')->limit($limit)->sortByName()->findAll(), 'id', function ($data) {
			return $data->profile->name . ' (' . $data->email . ')';
		});
	}

	// autoload "protected/lib/PasswordHash.php"
	// Yii::import('application.lib.PasswordHash');
	// See: http://www.yiiframework.com/wiki/275/how-to-write-secure-yii-applications/#hh23

	public function encryptPassword($str)
	{
		$hasher = new PasswordHash(8, true);

		return $hasher->HashPassword($str);
		// return md5($str);
	}

	public function verifyCurrentPassword($user_id, $password)
	{
		$hasher = new PasswordHash(8, true);
		if (!$record = Users::model()->findByPk($user_id))
			return false;

		return $hasher->CheckPassword($password, $record->password);

		// return ($record && $record->password === $this->encryptPassword($password));
	}

	public function createConfirmationKey()
	{
		return substr(md5(rand(1000, 10000) . time() . 'akej4#&2k3*S(DF*Z()1kj2h3!#$*dfvnJFKDa324wd54'), 0, 64);
	}

	public function createPasswordResetKey()
	{
		return substr(md5(rand(1000, 10000) . time() . 'akej4#&2k3*S(DF*Z()1kj2h3!#$*dfvnJFKDa324wd54'), 0, 64);
	}


	/****************************************
	 * Admin Functions
	 ***************************************/
	public function getTotalUsers()
	{
		return $this->model()->count();
	}

	public static function getUserCounts()
	{

	}

	public function getConfirmLink()
	{
		return Yii::app()->createAbsoluteUrl('user/users/confirm', [
			'email' => $this->email,
			'code'  => $this->confirmation_key,
		]);
	}

	public function getPasswordResetLink()
	{
		return Yii::app()->createAbsoluteUrl('user/users/reset', [
			'email' => $this->email,
			'code'  => $this->forgot_key,
		]);
	}

	/*
		public function facebookLogin() {
			k($this->_identity);
			if($this->_identity===null)
			{
				$this->_identity=new FacebookIdentity();
				$this->_identity->authenticate();
			}
		}
	*/

	public static function getAdminList()
	{
		$rows = Yii::app()->db->createCommand('SELECT users.id, CONCAT(profile.first_name," ",profile.last_name) as name
			FROM users
			INNER JOIN AuthAssignment ON AuthAssignment.userid = users.id
			INNER JOIN profile ON profile.user_id = users.id
			WHERE AuthAssignment.itemname = "admin"')->queryAll();

		return CHtml::listData($rows, 'id', 'name');
	}

	public function export()
	{
		$criteria = $this->searchCriteria();

		$columns = [
			't.id'               => 'User ID',
			't.email'            => 'Email',
			'profile.first_name' => 'First Name',
			'profile.last_name'  => 'Last Name',
			't.status'           => 'Status',
			't.created'          => 'Created',
			't.updated'          => 'Updated',
		];

		$headers = [];
		$select = [];
		foreach ($columns as $column => $label) {
			$headers[] = $label;
			$select[] = $column;
		}

		$rows = Yii::app()->db->createCommand()
			->select(implode(', ', $select))
			->from($this->tableName() . ' t')
			// left joins in case event / company / sales user is null or does not exist
			->join('profile', 'profile.user_id=t.id')
			->where($criteria->condition, $criteria->params)
			->order('t.created ASC')
			->queryAll();

		foreach ($rows as $k => $row) {
			// $rows[$k]['created'] = date('Y-m-d', $row['created']);
		}

		Export::csv($rows, $headers, 'users');
	}

	public static function getListByRole($role)
	{
		// Select all users with the SALES role
		$rows = Yii::app()->db->createCommand('SELECT users.id, CONCAT(profile.first_name," ",profile.last_name) as name
			FROM users
			INNER JOIN AuthAssignment ON AuthAssignment.userid = users.id
			INNER JOIN profile ON profile.user_id = users.id
			WHERE AuthAssignment.itemname = :role
			ORDER BY profile.first_name ASC')
			->bindParam(':role', $role)
			->queryAll();

		return CHtml::listData($rows, 'id', 'name');
	}

	public static function getInstructors()
	{
		return self::getListByRole('instructor');
	}

	public static function generateReferralCode()
	{
		$code = CString::randomHash();

		return $code;
	}

	public function getReferralCode($autogenerate = false)
	{
		if (!$this->referral_code && $autogenerate) {
			$this->referral_code = self::generateReferralCode();
			$this->update();
		}

		return $this->referral_code;
	}

	public static function getSocialAuthLinks($social = null)
	{
		$facebook_client_id = Yii::app()->params->facebook['client_id'];
		$facebook_redirect_uri = Yii::app()->createAbsoluteUrl('social/authFacebook');

		$google_client_id = Yii::app()->params->google['client_id'];
		$google_redirect_uri = Yii::app()->createAbsoluteUrl('social/authGoogle');

		$facebook = "https://www.facebook.com/v2.9/dialog/oauth?client_id={$facebook_client_id}&redirect_uri={$facebook_redirect_uri}&scope=email,user_birthday";
		$google = "https://accounts.google.com/o/oauth2/auth?client_id={$google_client_id}&redirect_uri={$google_redirect_uri}&auth_type=rerequest&response_type=code&scope=profile openid email";

		return [
			'facebook' => $facebook,
			'google'   => $google,
		];
	}

	public static function checkIfSocial()
	{
		if (Yii::app()->user->isGuest) {
			return false;
		}

		return Yii::app()->user->identity->facebook_id || Yii::app()->user->identity->google_id;
	}
	
	//fields for rest api
	public function fields() {
        return [
			'id',
			'email',
			'password'
		];
	}
	
	//extraFields for rest api
	public function extraFields() {
        return [
			'customer',
			'profile'
		];
	}

    public function insertDirectly($attributes=null)
    {
        if(!$this->getIsNewRecord())
            throw new CDbException(Yii::t('yii','The active record cannot be inserted to database because it is not new.'));
        Yii::trace(get_class($this).'.insert()','system.db.ar.CActiveRecord');
        $builder=$this->getCommandBuilder();
        $table=$this->getMetaData()->tableSchema;
        $command=$builder->createInsertCommand($table,$this->getAttributes($attributes));
        if($command->execute())
        {
            $primaryKey=$table->primaryKey;
            if($table->sequenceName!==null)
            {
                if(is_string($primaryKey) && $this->$primaryKey===null)
                    $this->$primaryKey=$builder->getLastInsertID($table);
                elseif(is_array($primaryKey))
                {
                    foreach($primaryKey as $pk)
                    {
                        if($this->$pk===null)
                        {
                            $this->$pk=$builder->getLastInsertID($table);
                            break;
                        }
                    }
                }
            }
            $this->_pk=$this->getPrimaryKey();
            $this->afterSave();
            $this->setIsNewRecord(false);
            $this->setScenario('update');
            return true;
        }
        return false;
    }

    public function updateDrectly($attributes=null)
    {
        if($this->getIsNewRecord())
            throw new CDbException(Yii::t('yii','The active record cannot be updated because it is new.'));
        Yii::trace(get_class($this).'.update()','system.db.ar.CActiveRecord');
        if($this->_pk===null)
            $this->_pk=$this->getPrimaryKey();
        $this->updateByPk($this->getOldPrimaryKey(),$this->getAttributes($attributes));
        $this->_pk=$this->getPrimaryKey();
        $this->afterSave();
        return true;
    }

    public function saveDirectly($runValidation=true,$attributes=null)
    {
        if(!$runValidation || $this->validate($attributes))
            return $this->getIsNewRecord() ? $this->insertDirectly($attributes) : $this->updateDrectly($attributes);
        else
            return false;
    }
}

