<?php Yii::app()->clientScript->registerCSSFile(Yii::app()->theme->getBaseUrl() . '/css/alt-page.css', 'all'); ?>

<?php //Yii::app()->clientScript->registerCSSFile(Yii::app()->theme->getBaseUrl() . '/css/account.css', 'all'); ?>

<?php $this->beginContent('//layouts/main'); ?>

<div class="">
	<div class="row-fluid">
		<div class="span3">
			<div id="sidebar">
				<?php echo $this->clips['sidebar']; ?>
				<?php $this->renderPartial('//profile/_sidebar'); ?>

				<?php $this->widget('zii.widgets.CMenu',array(
					'items'=>array(
						array(
							'label'=>'My Account', 
							'url'=>array('users/settings','id'=>Yii::app()->user->id), 
						),
						array(
							'label'=>'Edit Profile', 
							'url'=>array('profile/update','id'=>Yii::app()->user->id), 
						),
						array(
							'label'=>'Campaigns', 
							'url'=>array('campaign/index','id'=>Yii::app()->user->id), 
						),
						// array(
						// 	'label'=>'Contributions', 
						// 	'url'=>array('donation/index','id'=>Yii::app()->user->id), 
						// ),
					),
					'htmlOptions'=>array('class'=>'nav nav-account'),
					'encodeLabel'=>false,
				)); ?>
			</div><!-- sidebar -->
		</div>
		<div class="span9">
			<div class="<?php echo $this->id.'-'.$this->action->id; ?>" style="margin-left: 26px;">
				<!-- <div id="account-header" class="">
					<h1><?php //echo $this->pageTitle; ?></h1>
				</div> -->
				<div class="lines light"></div>
				<div id="content" class="account-content stripes">
					<div class="block">
						<?php echo $content; ?>
					</div>
				</div><!-- content -->
			</div>
		</div>
	</div>
</div>
<?php $this->endContent(); ?>