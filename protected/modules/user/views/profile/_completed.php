<div class="info-box">
	<div class="raised-meter">
		<div class="meter">
			<div class="meter-left"></div>
			<div class="meter-right"></div>
			<div class="meter-middle"></div>
		</div>
		<div class="bar" style="width: <?php echo $score; ?>%;">
			<div class="bar-left"></div>
			<div class="bar-right"></div>
			<div class="bar-middle"></div>
		</div>
	</div>
	<div class="goal-text">
		<div class="pull-left">
			Profile
		</div>
		<?php echo round($score*100).'% Complete'; ?>
		<div class="clearfix"></div>
	</div>
</div>