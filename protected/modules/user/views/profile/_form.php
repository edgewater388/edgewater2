<div class="form aform-horizontal">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'profiles-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'enctype' => 'multipart/form-data'
	),
)); ?>

	<?php echo $form->errorSummary($model,NULL,NULL,array('class'=>'alert alert-message block-message alert-error')); ?>

	<div class="control-group">
		<?php echo $form->labelEx($model, 'description',array('class'=>'control-label')); ?>
		<div class="well controls">
			<div class="row-fluid">
				<div class="span12">
					<?php echo $form->textarea($model,'description',array('class'=>'span12','rows'=>4)); ?>
				</div>
				<?php echo $form->error($model,'description'); ?>
				<p class="note" style="margin-bottom: 0;"></p>
			</div>
		</div>
	</div>

	<div class="divider"></div>

	<label>About Yourself</label>
	<div class="well">
		<div class="row-fluid">
			<div class="span6">
				<div class="control-group">
					<?php echo $form->labelEx($model,'street',array('class'=>'control-label')); ?>
					<div class="controls">
						<?php echo $form->textField($model,'street',array('size'=>50,'maxlength'=>128)); ?><br>
						<?php echo $form->textField($model,'street2',array('size'=>50,'maxlength'=>128)); ?>
						<?php echo $form->error($model,'street'); ?>
						<?php echo $form->error($model,'street2'); ?>
					</div>
				</div>

				<div class="control-group">
					<?php echo $form->labelEx($model,'city',array('class'=>'control-label')); ?>
					<div class="controls">
						<?php echo $form->textField($model,'city',array('size'=>50,'maxlength'=>128)); ?>
						<?php echo $form->error($model,'city'); ?>
					</div>
				</div>

				<div class="control-group">
					<?php echo $form->labelEx($model,'state',array('class'=>'control-label')); ?>
					<div class="controls">
						<?php if ($stateOptions = Location::stateOptions($model->country)): ?>
							<?php echo $form->dropDownList($model,'state',$stateOptions); ?>
						<?php else: ?>
							<?php echo $form->textField($model,'state',array('size'=>50,'maxlength'=>128)); ?>
						<?php endif; ?>
						<?php echo $form->error($model,'state'); ?>
					</div>
				</div>

				<div class="control-group">
					<?php echo $form->labelEx($model,'country',array('class'=>'control-label')); ?>
					<div class="controls">
						<?php //echo $model->country; ?>
						<?php echo $form->dropDownList($model,'country',Location::countries()); ?>
						<?php echo $form->error($model,'country'); ?>
					</div>
				</div>

				<div class="control-group">
					<?php echo $form->labelEx($model,'phone',array('class'=>'control-label')); ?>
					<div class="controls">
						<?php echo $form->textField($model,'phone',array('size'=>50,'maxlength'=>20)); ?>
						<?php echo $form->error($model,'phone'); ?>
					</div>
				</div>
			</div>
			<div class="span6">
				<div class="control-group">
					<?php echo $form->labelEx($model, 'photo',array('class'=>'control-label','label'=>'Upload a Photo')); ?>
					<div class="controls">
						<?php if ($model->photo_fid): ?>
						<div class="banner"><img src="<?php echo $model->photoF->thumb('profile_large'); ?>"></div>
						<?php else: ?>
						<div class="banner placeholder"></div>
						<?php endif; ?>
						<div class="input">
							<?php echo $form->fileField($model,'photo'); ?>
						</div>
					</div>
				</div>
				<div class="control-group">
					<?php echo $form->labelEx($model, 'gender',array('class'=>'control-label')); ?>
					<div class="controls">
						<?php $gender_options = (($model->gender) ? array() : array(''=>'Gender')) + array('f'=>'Female','m'=>'Male'); ?>
						<?php echo $form->dropDownList($model,'gender',$gender_options,array('class'=>'span4')); ?>
					</div>
				</div>

				<div class="control-group">
					<div class="inline-select">
						<?php echo $form->labelEx($model, 'birthday',array('class'=>'control-label')); ?>
						<div class="controls">
							<?php echo $form->dropDownList($model,'dob[m]',$model->getMonthOptions('abbreviated'),array()); ?>
							<?php echo $form->dropDownList($model,'dob[d]',$model->getDayOptions(),array()); ?>
							<?php echo $form->dropDownList($model,'dob[y]',$model->getYearOptions(),array()); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="divider"></div>

	<label>Share & Connect through Facebook, Twitter, and YouTube</label>
	<div class="well">
		<div class="social-input">
			<div class="control-group">
				<?php //echo $form->labelEx($model,'facebook',array('class'=>'control-label')); ?>
				<div class="controls">
					<div class="input-prepend">
						<span class="add-on">
							<span style="position:relative; top: -2px;">
								<img src="/themes/basic/images/social/facebook.png" alt="" width="16" height="16">
							</span>
						</span> 
						<?php echo $form->textField($model,'facebook',array('size'=>50,'maxlength'=>128,'class'=>'span5', 'placeholder'=>'e.g. http://facebook.com/govoluntouring')); ?>
					</div>
					<?php echo $form->error($model,'facebook'); ?>
				</div>
			</div>
		
			<div class="control-group">
				<?php //echo $form->labelEx($model,'twitter',array('class'=>'control-label')); ?>
				<div class="controls">
					<div class="input-prepend">
						<span class="add-on">
							<span style="position:relative; top: -2px;">
								<img src="/themes/basic/images/social/twitter.png" alt="" width="16" height="16">
							</span> 
						</span> 
						<?php echo $form->textField($model,'twitter',array('size'=>50,'maxlength'=>128,'class'=>'span5', 'placeholder'=>'e.g. http://twitter.com/govoluntouring')); ?>
					</div>
					<?php echo $form->error($model,'twitter'); ?>
				</div>
			</div>
		
			<div class="control-group" style="margin-bottom: 0;">
				<?php //echo $form->labelEx($model,'youtube',array('class'=>'control-label')); ?>
				<div class="controls">
					<div class="input-prepend">
						<span class="add-on">
							<span style="position:relative; top: -2px;">
								<img src="/themes/basic/images/social/youtube.png" alt="" width="16" height="16">
							</span> 
						</span> 
						<?php echo $form->textField($model,'youtube',array('size'=>50,'maxlength'=>128,'class'=>'span5', 'placeholder'=>'e.g. http://youtube.com/user/govoluntouring')); ?>
					</div>
					<?php echo $form->error($model,'youtube'); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="line divider"></div>
	
	<div class="aform-actions buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save Changes', array('class'=>'btn btn-primary btn-soft pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->