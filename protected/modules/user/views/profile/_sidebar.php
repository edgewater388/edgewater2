<div class="account-image-block">
<?php if (isset(Yii::app()->user->profile->photo)): ?>
	<img src="<?php echo Yii::app()->user->profile->photoF->thumb('profile_large'); ?>">
<?php else: ?>
	<div style="border: 1px solid #ddd; border-radius: 8px; line-height: 216px; height:214px; text-align: center; font-weight:bold;">Your Photo</div>
<?php endif; ?>

	<div class="profile-name"><?php echo Yii::app()->user->name; ?></div>
</div>

<?php //$this->renderPartial('//profile/_completed',array('score'=>Yii::app()->user->profile->completed)); ?>