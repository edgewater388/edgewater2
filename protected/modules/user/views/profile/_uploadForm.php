<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->getBaseUrl() . '/js/dropzone.js', CClientScript::POS_HEAD); ?>

<script>
	jQuery(document).ready(function($) {
		var previewTemplate = '<div class="file-preview"><div class="details"></div><div class="filename"><span></span></div><div class="progress"><span class="upload"></span></div><div class="success-mark"><span>Success</span></div><div class="error-mark"><span>Error</span></div><div class="error-message"><span></span></div></div>';
		
		// Dropzone.autoDiscover = false;
		Dropzone.options.profileForm = {
			// forceFallback: true,
			url: "<?php echo Yii::app()->createUrl('/user/profile/upload',array('id'=>$model->user_id, 'ajax'=>true)); ?>", 
			paramName: "Profile[photo]",
			previewsContainer: '.dropzone-previews',
			dictDefaultMessage: '<a class="upload-profile-photo text-orange"><i class="icon-pencil" style="opacity: .3;"></i> Upload New Photo</a>',
			dictFallbackMessage: '',
			clickable: true,
			// previewTemplate: previewTemplate,
			parallelUploads: 1,
			thumbnailWidth: 250,
			thumbnailHeight: 250,
			init: function() {
				this.on("addedfile", function(file) { 
					$('.profile-photo').prepend('<div class="loading loading-overlay"></div>');
				});
			},
			error: function(file, errorMessage) {
				// console.log(file);
			},
			success: function(file, response) {
				// console.log(response);
				response = JSON.parse(response);
				if (response.success != true) {
					$(file.previewTemplate).removeClass('success').addClass('error');
					$('.profile-photo .loading').remove();
					$('.alert-message').remove();
					$('.page-inner').prepend('<div class="alert alert-message block-message fade in alert-error"><a class="close" data-dismiss="alert" href="#">×</a><ul><li>'+response.message+'</li></ul></div>');
				} else {
					var image = new Image();
					image.onload = function() {
						$('.profile-photo').html('<img src="'+response.url+'" alt="">');
					}
					image.src = response.url;
					
					$(file.previewTemplate).addClass('');
				}
			},
		  	complete: function(file) {
				$(file.previewTemplate).find('.loading').remove();
				$(file.previewTemplate).addClass('complete');
			}
		};

		// var dropzone = new Dropzone('div[data-target="dropzone"]', { 
		// 	url: "<?php echo Yii::app()->createUrl('/user/profile/upload',array('id'=>$model->user_id, 'ajax'=>true)); ?>", 
		// 	paramName: 'Profile[photo]',
		// 	previewsContainer: '.dropzone-previews',
		// 	clickable: true,
		// 	previewTemplate: previewTemplate,
		// 	// enqueueForUpload: false,
		// 	parallelUploads: 1,
		// 	thumbnailWidth: 250,
		// 	thumbnailHeight: 250
		// });

		// dropzone.on("addedfile", function(file) {
		// 	$('.profile-photo').prepend('<div class="loading loading-overlay"></div>');
		// 	// $(file.previewTemplate).prepend('<div class="loading overlay"></div>');
		// });

		// dropzone.on("error", function(file, errorMessage) {
		// 	// console.log(file);
		// });

		// dropzone.on("success", function(file, response) {
		// 	console.log(response);
		// 	response = JSON.parse(response);
		// 	if (response.success != true) {
		// 		$(file.previewTemplate).removeClass('success').addClass('error');
		// 		$('.profile-photo .loading').remove();
		// 		$('.alert-message').remove();
		// 		$('.page-inner').prepend('<div class="alert alert-message block-message fade in alert-error"><a class="close" data-dismiss="alert" href="#">×</a><ul><li>'+response.message+'</li></ul></div>');
		// 	} else {
		// 		var image = new Image();
		// 		image.onload = function() {
		// 			$('.profile-photo').html('<img src="'+response.url+'" alt="">');
		// 		}
		// 		image.src = response.url;
				
		// 		$(file.previewTemplate).addClass('');
		// 	}
		// });

		// dropzone.on("complete", function(file) {
		// 	$(file.previewTemplate).find('.loading').remove();
		// 	$(file.previewTemplate).addClass('complete');
		// });

		$('.upload-profile-photo').live('click', function(e) {
			// $('.profile-photo').click();
			e.preventDefault();
		});

		$('.dropzone .profile-photo').click( function() {
			$('.dropzone').click();
		});
	});
</script>

<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'id'=>'profileForm',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'enctype' => 'multipart/form-data',
		'class'=>'dropzone',
	),
	'action'=>Yii::app()->createUrl('/user/profile/upload',array('id'=>$model->user_id)),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<div class="profile-photo" data-target="dropzone" style="margin-bottom: 1em;">
			<?php if ($model->photo_fid): ?>
				<img src="<?php echo $model->photoF->thumb('profile_large'); ?>" alt="">
			<?php else: ?>
				<div style="background-color: #e8e8e8; width: 100%; height: 250px;"></div>
			<?php endif; ?>
		</div>

		<div class="dropzone-previews"></div>
	</div>

	<div class="fallback">
		<div class="control-group">
			<a href="#" class="upload-profile-photo"><i class="icon-pencil" style="opacity: .3;"></i> <?php echo $model->photo_fid ? 'Upload New Photo' : 'Upload Photo'; ?></a>

			<div class="controls">
				<?php echo $form->fileField($model,'photo'); ?>
				<?php echo $form->error($model,'photo'); ?>
			</div>
		</div>
		
		<div class="buttons">
			<?php echo Html::submitButton('Upload', array('class'=>'btn btn-alt btn-small')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<div class="clearfix"></div>
