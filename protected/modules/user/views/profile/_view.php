<div class="span3">
	<div class="photo">
		<a href="<?php echo Yii::app()->createUrl('/user/profile/view',array('id'=>$data->user_id)); ?>">
			<img src="<?php echo ($data->photo_fid) ? $data->photo_fid : ''; ?>">
		</a>
	</div>

	<div class="name">
		<a href="<?php echo Yii::app()->createUrl('/user/profile/view',array('id'=>$data->user_id)); ?>">
			<?php echo $data->name; ?>
		</a>
	</div>
</div>