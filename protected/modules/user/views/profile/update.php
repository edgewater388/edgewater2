<?php
/*
$this->breadcrumbs=array(
	'My Account' => array('users/settings', $model->user_id),
);
*/
?>

<div class="page-header">
	<h1><?php echo $this->pageTitle; ?></h1>
</div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'sidebar')); ?>

<?php $this->endWidget();?>