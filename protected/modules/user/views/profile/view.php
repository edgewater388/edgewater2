<?php Yii::app()->clientScript->registerCSSFile(Yii::app()->getBaseUrl() . '/themes/jazzcuts/css/profile.css', 'all'); ?>

<?php
/*
$this->breadcrumbs=array(
	'Profiles'=>array('index'),
	$model->first_name.' '.$model->last_name,
);
*/
?>

<div class="page-header">
	<h1><?php echo $model->first_name.' '.$model->last_name; ?></h1>
	<div class="sub-heading small links ">
		<?php if ($model->facebook): ?><a href="http://facebook.com/<?php echo $model->facebook; ?>" class="social-link facebook">Facebook</a><?php endif; ?>
		<?php if ($model->twitter): ?><a href="http://twitter.com/<?php echo $model->twitter; ?>" class="social-link facebook">Twitter</a><?php endif; ?>
		<?php if ($model->youtube): ?><a href="http://plus.google.com/<?php echo $model->youtube; ?>" class="social-link facebook">YouTube</a><?php endif; ?>
	</div>
</div>

<!-- <h3>Artist description</h3> -->
<?php if ($model->description): ?>
<div class="description medium short-description">
	<div class="description-inner">
	<?php echo nl2br($model->description); ?>
	</div>
	
	<?php // only display the more if there is at least *some* text ?>
	<?php if (strlen($model->description) > 250): ?>
	<div class="overflow"></div>
	<div class="overflow-controls">
		<a href="#more" class="more-btn" onclick="$(this).parents('.short-description').removeClass('short-description'); $(this).hide(); return false;">More</a>
		<div class="clearfix"></div>
	</div>
	<?php endif; ?>
	
	<div class="divider line"></div>
</div>
<?php endif; ?>
	

<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'sidebar_left')); ?>

<?php $this->endWidget();?>
