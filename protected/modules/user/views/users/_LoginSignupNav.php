<?php $this->widget('zii.widgets.CMenu',array(
	'items'=>array(
		array(
			'label'=>'Login', 
			'url'=>array('site/login'), 
			'linkOptions'=>array('class'=>''),
		),
		array(
			'label'=>'Sign Up', 
			'url'=>array('users/register'), 
			'linkOptions'=>array('class'=>''),
		),
	),
	'htmlOptions'=>array('class'=>'nav nav-tabs'),
	'encodeLabel'=>false,
)); ?>