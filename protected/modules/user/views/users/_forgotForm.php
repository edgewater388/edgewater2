<div class="form form-stacked">
	<!-- <h3>Create an account</h3> -->

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
	'action'=>array('users/forgot'),
)); ?>

	<?php //echo $form->errorSummary($model,NULL,NULL,array('class'=>'alert alert-message block-message alert-error')); ?>
	
	<div class="row-fluid">
		<div class="control-group">
			<?php echo $form->labelEx($model,'email'); ?>
			<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128,'class'=>'span12')); ?>
			<?php echo $form->error($model,'email'); ?>
		</div>
	</div>

	<div class="control-group buttons text-right">
		<?php echo CHtml::submitButton('Reset Password', array('class'=>'btn btn-alt btn-block btn-small')); ?>
		<div class="clearfix"></div>
	</div>
	
<?php $this->endWidget(); ?>

</div><!-- form -->