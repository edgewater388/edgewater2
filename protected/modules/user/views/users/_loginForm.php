<div class="form">
<!-- 	<h3>Sign in</h3> -->
<?php $form=$this->beginWidget('ActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="control-group">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('class'=>'span12')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('class'=>'span12')); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="control-group rememberMe hidden-phone">
		<div class="form-inline aoptions acheckbox">
			<?php echo $form->checkBox($model,'rememberMe',array('style'=>'margin-top:-2px;')); ?>
			<?php echo $form->label($model,'rememberMe', array('class'=>'small medium')); ?>
		</div>
		<?php echo $form->error($model,'rememberMe'); ?>
		<div class="clearfix"></div>
	</div>

	<div class="buttons text-right">
		<?php echo Html::submitButton('Login', array('class'=>'btn btn-alt btn-large')); ?>
	</div>
	<div class="text-right forgot-password-wrapper">
		<a href="<?php echo Yii::app()->createUrl('/user/users/forgot'); ?>" class="dark bold" style="font-size: 10px;">Forgot Password?</a>
	</div>

	<div class="divider divider-large hidden-phone"></div>
	<div class="divider divider-large hidden-phone"></div>

<?php $this->endWidget(); ?>
</div><!-- form -->