<div class="form form-horizontal">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php //echo $form->errorSummary($model); ?>

	<div class="control-group">
		<?php echo $form->labelEx($model,'new_password',array('style'=>'width: 132px;')); ?>
		<div class="form-inline">
			<?php echo $form->passwordField($model,'new_password',array('size'=>60,'maxlength'=>128)); ?>
			<?php echo $form->error($model,'new_password'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model, 'password2',array('style'=>'width: 132px;')); ?>
		<div class="form-inline">
			<?php echo $form->passwordField($model, 'password2',array('size'=>60,'maxlength'=>128)); ?>
			<?php echo $form->error($model, 'password2'); ?>
		</div>
	</div>
	
	<div class="control-group buttons">
		<?php echo CHtml::submitButton('Reset Password', array('class'=>'btn btn-primary btn-small')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->