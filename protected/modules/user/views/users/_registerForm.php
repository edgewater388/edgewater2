<div class="form">
	<!-- <h3>Create an account</h3> -->

<?php $form=$this->beginWidget('ActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
	'action'=>array('users/register'),
)); ?>

	<?php echo $form->errorSummary($model); ?>
	
	<div class="row-fluid">
		<div class="span6">
			<div class="control-group">
				<?php echo $form->labelEx($profile,'first_name'); ?>
				<div class="controls">
					<?php echo $form->textField($profile,'first_name',array('class'=>'span12','maxlength'=>50)); ?>
					<?php echo $form->error($profile,'first_name'); ?>
				</div>
			</div>
		</div>
		<div class="span6">
			<div class="control-group">
				<?php echo $form->labelEx($profile,'last_name'); ?>
				<div class="controls">
					<?php echo $form->textField($profile,'last_name',array('class'=>'span12','maxlength'=>50)); ?>
					<?php echo $form->error($profile,'last_name'); ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="control-group">
		<?php echo $form->labelEx($model,'email'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128,'class'=>'span12')); ?>
			<?php echo $form->error($model,'email'); ?>
		</div>
	</div>
	<div class="control-group">
		<?php echo $form->labelEx($model,'email2'); ?>
		<div class="controls">
			<?php echo $form->textField($model,'email2',array('size'=>60,'maxlength'=>128,'class'=>'span12')); ?>
			<?php echo $form->error($model,'email2'); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'password'); ?>
		<div class="controls">
			<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128,'class'=>'span12')); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>
	</div>
	<div class="control-group">
		<?php echo $form->labelEx($model, 'password2'); ?>
		<div class="controls">
			<?php echo $form->passwordField($model, 'password2',array('size'=>60,'maxlength'=>128,'class'=>'span12')); ?>
			<?php echo $form->error($model, 'password2'); ?>
		</div>
	</div>

	<?php /*
	<div class="control-group">
		<label for="">&nbsp;</label>
		<div class="controls checkbox">
			<?php echo $form->checkBox($model,'terms_accepted'); ?> 
			<?php echo CHtml::label('I accept the <a href="/terms">terms and conditions</a>', 'Users_terms_accepted', array('uncheckValue' => '')); ?>
			<div class="clearfix"></div>
			<?php echo $form->error($model,'terms_accepted'); ?>
		</div>
		<div class="clearfix"></div>
	</div>
	*/ ?>

	<div class="control-group buttons text-right">
		<?php echo Html::submitButton('Sign Up', array('class'=>'btn btn-alt btn-large')); ?>
		<div class="clearfix"></div>
	</div>
	
	<div class="divider divider-large hidden-phone"></div>
<?php $this->endWidget(); ?>

</div><!-- form -->