<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php //echo $form->errorSummary($model); ?>

	<div class="control-group row-fluid">
		<div class="span12">
			<?php echo $form->textField($model,'email',array('class'=>'span12','maxlength'=>128)); ?>	
		</div>
		<?php echo $form->error($model,'email'); ?>
	</div>
		
	<div class="text-right buttons">
		<a href="<?php echo Yii::app()->createUrl('/user/users/settings', array('id'=>Yii::app()->user->id)); ?>" class="btn btn-default btn-small">Cancel</a>
		<?php echo Html::submitButton('Save Changes', array('class'=>'btn btn-alt btn-small')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->