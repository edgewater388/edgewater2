<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php //echo $form->errorSummary($model); ?>
	
	<div class="control-group row-fluid">
		<div class="span6">
			<?php echo $form->textField($model->profile,'first_name',array('class'=>'span12','maxlength'=>50,'placeholder'=>'First Name')); ?>	
		</div>
		<div class="span6">
			<?php echo $form->textField($model->profile,'last_name',array('class'=>'span12','maxlength'=>50,'placeholder'=>'Last Name')); ?>
		</div>
		<?php echo $form->error($model->profile,'first_name'); ?>
		<?php echo $form->error($model->profile,'last_name'); ?>
	</div>
	
	<div class="buttons text-right">
		<a href="<?php echo Yii::app()->createUrl('/user/users/settings', array('id'=>Yii::app()->user->id)); ?>" class="btn btn-default btn-small">Cancel</a>
		<?php echo Html::submitButton('Save Changes', array('class'=>'btn btn-alt btn-small')); ?>
	</div>
	
<?php $this->endWidget(); ?>

</div><!-- form -->