<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php //echo $form->errorSummary($model); ?>

	<div class="control-group">
		<?php echo $form->labelEx($model,'current_password',array('style'=>'width: 132px;')); ?>
		<div class="form-inline">
			<?php echo $form->passwordField($model,'current_password',array('class'=>'span12','maxlength'=>128)); ?>
			<?php echo $form->error($model,'current_password'); ?>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="control-group">
				<?php echo $form->labelEx($model,'new_password',array('style'=>'width: 132px;')); ?>
				<div class="form-inline">
					<?php echo $form->passwordField($model,'new_password',array('class'=>'span12','maxlength'=>128)); ?>
					<?php echo $form->error($model,'new_password'); ?>
				</div>
			</div>
		</div>
		<div class="span6">
			<div class="control-group">
				<?php echo $form->labelEx($model, 'password2',array('style'=>'width: 132px;')); ?>
				<div class="form-inline">
					<?php echo $form->passwordField($model, 'password2',array('class'=>'span12','maxlength'=>128)); ?>
					<?php echo $form->error($model, 'password2'); ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="text-right buttons">
		<a href="<?php echo Yii::app()->createUrl('/user/users/settings', array('id'=>Yii::app()->user->id)); ?>" class="btn btn-default btn-small">Cancel</a>
		<?php echo Html::submitButton('Save Changes', array('class'=>'btn btn-alt btn-small')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->