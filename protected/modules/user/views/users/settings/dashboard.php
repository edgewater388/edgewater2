<?php $this->renderPartial('//menu/account'); ?>

<div class="row-fluid">
	<div class="span4">
		<?php $this->renderPartial('application.modules.user.views.profile._uploadForm',array('model'=>$model->profile)); ?>		
	</div>
	<div class="span8">
		<?php $key = key($dashboardItems); ?>
		<?php foreach ($dashboardItems as $k => $item): ?>
		<div class="control-group">
			<div class="control-label">
				<div class="col-label"><?php echo $item['label']; ?></div>
			</div>
			<div class="controls">
				<?php if (isset($item['render'])): ?>
					<?php $this->renderPartial($item['render'],array('model'=>$model)); ?>
				<?php else: ?>
					<div class="row-fluid">
						<div class="span4 col-desc"><?php echo $item['desc']; ?></div>
						<div class="span2"><?php echo ($item['edit_url']) ? '<a href="'.$item['edit_url'].'"><i class="icon-pencil" style="opacity: .3;"></i> Edit</a>' : ''; ?></div>
					</div>
				<?php endif; ?>
			</div>

			<div class="divider divider-small"></div>
		</div>
		
		<?php /*
		<div class="control-group row-fluid <?php echo strtolower($item['label']); ?> <?php if ($key!=$k) echo ''; ?>">
			<?php if (isset($item['render'])): ?>
				<div class="span8"><?php $this->renderPartial($item['render'],array('model'=>$model)); ?></div>
				<div class="span2 right col-link"><?php echo ($item['cancel_url']) ? '<a href="'.$item['cancel_url'].'">Cancel</a>' : ''; ?></div>
			<?php else: ?>
				<div class="span8 col-desc"><?php echo $item['desc']; ?></div>
				<div class="span2 right col-link"><?php echo ($item['edit_url']) ? '<a href="'.$item['edit_url'].'">Edit</a>' : ''; ?></div>
			<?php endif; ?>
			<div class="clearfix"></div>
		</div>
		*/ ?>
		<?php endforeach; ?>
	</div>
</div>	
