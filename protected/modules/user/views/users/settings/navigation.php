<?php $user_id = (isset($_GET['id'])) ? $_GET['id'] : Yii::app()->user->id; ?>
<ul>
	<li class="<?php echo ($this->id == 'users' && $this->action->id=='settings')?'active':''; ?>"><a href="/u/<?php echo $user_id; ?>/settings">Account Settings</a></li>
	<li class="<?php echo ($this->id == 'profile' && $this->action->id=='update')?'active':''; ?>"><a href="/u/<?php echo $user_id; ?>/profile">Profile</a></li>
	<li><a href="">My Subscription</a></li>
	<li><a href="">Email Settings</a></li>
</ul>

<?php
/*
	$items = array(
		array('url'=>array('/users/settings/'.$user_id), 'label'=>'Account Settings'),
	);
	
	$this->widget('zii.widgets.CMenu',array(
		'activateParents' => true,
		'items'=>$items,
	));
*/
?>