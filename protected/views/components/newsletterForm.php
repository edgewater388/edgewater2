<div class="form">
<?php $form=$this->beginWidget('ActiveForm', array(
	'id'=>'newsletter-form',
	'action'=>Yii::app()->createUrl('newsletter/signup'),
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
)); ?>

	<?php echo $form->errorSummary($model); ?>

<div class="newsletter-signup-form">
	<div class="row-fluid">
		<div class="span3">
			<?php echo $form->textField($model, 'first_name', array('class'=>'span12', 'placeholder'=>'First Name')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textField($model, 'last_name', array('class'=>'span12', 'placeholder'=>'Last Name')); ?>
		</div>
		<div class="span4">
			<?php echo $form->textField($model, 'email', array('class'=>'span12', 'placeholder'=>'Your Email Address')); ?>
		</div>
		<div class="span2">
			<?php echo Html::submitButton('Submit', array('class'=>'btn btn-alt span12')); ?>
		</div>
		<?php echo $form->hiddenField($model, 'returnUrl'); ?>
	</div>
<?php $this->endWidget(); ?>
</div><!-- form -->