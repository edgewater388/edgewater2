<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<?php $form = new ActiveForm; ?>

<div class="card-form errors">

	<div class="row-fluid">
		<div class="span6">

			<div class="form-errors"></div>

			<div class="row-fluid">
				<div class="span8 control-group">
					<?php echo $form->label($model, 'number'); ?>
					<div class="controls">
						<?php echo $form->textField($model, 'number', array('class'=>'span12 card-number', 'placeholder'=>'xxxx - xxxx - xxxx - xxxx')); ?>	
						<?php echo $form->error($model, 'number'); ?>
					</div>
				</div>
				<div class="span4 control-group security-code-wrapper">
					<?php echo $form->label($model, 'cvc', array('style'=>'display: inline-block; vertical-align: middle;')); ?> <i class="icon-info-sign" data-toggle="tooltip" title="Security Code is 3 or 4 numbers usuallly found on the back of your card"></i>
					<div class="controls">
						<?php echo $form->textField($model, 'cvc', array('class'=>'span12 card-cvc', 'placeholder'=>'xxx')); ?>	
						<?php echo $form->error($model, 'cvc'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="control-group expiry-wrapper">
		<?php echo $form->label($model, 'expiry_month'); ?>
		<div class="controls">
			<?php echo $form->textField($model, 'expiry_month', array('class'=>'span1 text-center card-expiry-month', 'placeholder'=>'mm', 'maxlength'=>'2')); ?>	
			<?php echo $form->textField($model, 'expiry_year', array('class'=>'span1 text-center card-expiry-year', 'placeholder'=>'yy', 'maxlength'=>'4')); ?>	
			<?php echo $form->error($model, 'expiry_month'); ?>
			<?php echo $form->error($model, 'expiry_year'); ?>
		</div>
	</div>
</div>

<?php /*
<div class="note">
	*All donations are subject to a 2.9% + 30¢ transaction fee
</div>
*/ ?>

<script>
	Stripe.setPublishableKey("<?php echo Yii::app()->stripe->publicKey; ?>");
	var form;

	jQuery(document).ready(function($) {

		if ( $('#donation-donation-form').length > 0 )
			form = $('#donation-donation-form');
		else if ( $('#stripe-customer-form').length > 0 )
			form = $('#stripe-customer-form');
		
		if ( $('input[name$="[card_token]"]').val() != '' )
			populatePaymentDetails();

		$('.card-form.verified input').click( function() {
			clearPaymentValues();
		});

		if (form) 
		{
			form.submit( function(e) {
				if ($('input[name$="[card_token]"]').val() == '')
				{
					stripeCreateToken();
					e.preventDefault();
				}
			});
		}
	});

	function stripeCreateToken()
	{
		Stripe.card.createToken({
			number: $('.card-number').val(),
			cvc: $('.card-cvc').val(),
			exp_month: $('.card-expiry-month').val(),
			exp_year: $('.card-expiry-year').val()
		}, stripeResponseHandler);
	}
	
	function stripeResponseHandler(status, response) {
	    if (response.error) {
	        //
	        // show the errors on the form
	        // $(".payment-errors").text(response.error.message);
	        $('.card-form .form-errors').html('<div class="alert alert-message form-message block-message fade in alert-error"><p>Please fix the following input errors:</p><ul><li>'+response.error.message+'</li></ul></div>');

	    } else {
	        // token contains id, last4, and card type
	        var token = response['id'];
	        // insert the token into the form so it gets submitted to the server
	        form.find('input[name$="[card_token]"]').val(token);
	        form.find('input[name$="[fingerprint]"]').val(response.card.fingerprint);
	        form.find('input[name$="[card_type]"]').val(response.card.type);
	        form.find('input[name$="[last4]"]').val(response.card.last4);
	        form.find('input[name$="[exp_month]"]').val(response.card.exp_month);
	        form.find('input[name$="[exp_year]"]').val(response.card.exp_year);
	        // and submit

	        form.get(0).submit();
	    }
	}

	function populatePaymentDetails()
	{
		$('.card-form').addClass('verified');

		$('.card-number').val( '**** **** **** ' + $('input[name$="[last4]"]').val() );
		$('.card-cvc').val( '***' );
		$('.card-expiry-month').val( $('input[name$="[exp_month]"]').val() );
		$('.card-expiry-year').val( $('input[name$="[exp_year]"]').val() );
	}

	function clearPaymentValues()
	{
		$('.card-form.verified input').unbind('click');

		$('.card-form').removeClass('verified');
		$('.card-number').val('');
		$('.card-cvc').val('');

		form.find('input[name$="[card_token]"]').val('');
		form.find('input[name$="[fingerprint]"]').val('');
		form.find('input[name$="[card_type]"]').val('');
		form.find('input[name$="[last4]"]').val('');
        form.find('input[name$="[exp_month]"]').val('');
        form.find('input[name$="[exp_year]"]').val('');
	}

</script>