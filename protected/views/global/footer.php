<footer id="footer">
        <ul class="nav">
            <li><a href="http://physique57.com/book-a-class/">Book&nbsp;a&nbsp;Class</a></li>
            <li><a href="http://physique57.com/our-story/">Our&nbsp;Story</a></li>
            <li><a href="http://physique57.com/shop/all-products/">Shop</a></li>
            <li><a href="http://physique57.com/blog/">Blog</a></li>
            <li><a href="http://physique57.com/buzz">Buzz</a></li>
            <li><a href="http://physique57.com/international-opportunities/">International Licensing</a></li>
        </ul>
        <ul class="nav">
            <li><a href="http://physique57.com/work-with-us/">Work&nbsp;With&nbsp;Us</a></li>
            <li><a href="http://physique57.com/contact/">Contact</a></li>
            <li><a href="http://physique57.com/privacy-legal/">Privacy/Legal</a></li>
            <li><a href="http://physique57.com/terms-and-conditions/">Terms&nbsp;&&nbsp;Conditions</a></li>
            <li><a href="http://physique57.com/return-policy/">Return Policy</a></li>
        </ul>
        <div class="connect-wrap footer-block">
            <h3>CONNECT</h3>
            <p>Let’s get social. Tap, like, and join the Physique 57 community for event news, special
                offers, and exclusive fitness tips to help keep you motivated. We can’t wait to connect with you.</p>
            <ul>
                <li class="soc-link" id="facebook"><a href="https://www.facebook.com/Physique57" target="_blank"></a></li>
                <li class="soc-link" id="twitter"><a href="https://twitter.com/Physique57" target="_blank"></a></li>
                <li class="soc-link" id="pinterest"><a href="https://www.pinterest.com/physique57/" target="_blank"></a></li>
                <li class="soc-link" id="instagram"><a href=" https://www.instagram.com/Physique57/" target="_blank"></a></li>
                <li class="soc-link" id="youtube"><a href="https://www.youtube.com/user/Physique57" target="_blank"></a></li>
            </ul>
        </div>
        <div class="newsletter-wrap footer-block">
            <h3>NEWSLETTER</h3>
            <p>Sign up and receive exclusive offers, behind-the-scenes sneak peeks, wellness expert tips, and more.</p>

            <a href="https://app.e2ma.net/app2/audience/signup/1780047/1358440/?v=a" target="_blank" class="footer_btn btn blue">ENTER YOUR EMAIL</a>

        </div>
        <h6>Copyright <?= date('Y'); ?> Physique 57. "Physique 57, Sculpting Bodies.Changing Lives." And The
            Physique 57 Logo are trademarks of Physique 57.</h6>

        <?php $this->renderPartial('//partials/adroll-tracking'); ?>

</footer>