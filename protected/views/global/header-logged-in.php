<header id="header" class="logged-in">
    <div class="lockout">
        <aside id="main-menu">
            <div id="nav-wrap">
                <?php include 'nav-menu.php' ?>
            </div>
            <div id="background-wrap">
                <div class="background closed bg-column-1">
                </div>
                <div class="background closed bg-column-2">
                </div>
                <div class="background closed bg-column-3">
                </div>
                <div id="img-wrap" class="closed"><a href="http://physique57.com/"></a>
                </div>
            </div>
        </aside>
        <div class="left">
            <div class="inner">
                <div class="pic">
                    <div class="image">
                        <a href="<?php if (Yii::app()->user->identity->is_demo): ?>/sign-up<?php else: ?>/account<?php endif; ?>">
                            <img src="<?php echo Yii::app()->user->profile->getThumbnailUrl(); ?>">
                        </a>
                    </div>
                </div>
                <div class="user">
                    <a href="<?php if (Yii::app()->user->identity->is_demo): ?>/sign-up<?php else: ?>/account<?php endif; ?>">
                        <?php if (Yii::app()->user->profile->first_name): ?>
                            <h5>Welcome, <?php echo Yii::app()->user->profile->first_name; ?>!</h5>
                        <?php else: ?>
                            <h5>Welcome!</h5>
                        <?php endif; ?>
                    </a>
                    <div>
                        <a href="/logout" class="logout"><span class="desktop">Logout</span><div class="logout-icon"></div></a>
                        <a href="/faq" class="logout help">Help</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="center">
            <?php if (Yii::app()->user->identity->is_demo): ?>
                <a class="btn white" href="/sign-up">sign up</a>
            <?php else: ?>
                <a class="btn white" href="/account">account</a>
            <?php endif; ?>
            <div class="logo">
                <a href="/">
                    <div class="inner"></div>
                </a>
            </div>
            <?php if (Yii::app()->user->membership): ?>
                <?php if (Yii::app()->user->membership->isActive()): ?>
                    <a class="btn white" href="/my-workouts">my workouts</a>
                <?php else: ?>
                    <a class="btn white" href="/change-plan">Add a Plan</a>
                <?php endif; ?>
            <?php else: ?>
                <?php if ($this->id == 'customer' && $this->action->id == 'create'): ?>
                <span class="btn white" style="visibility: hidden;"></span>
                <?php else: ?>
                <a class="btn white" href="/sign-up">Sign Up</a>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="right">
            <div class="wrap">
                <div class="hamburger">
                    <div class="bar"></div>
                    <div class="bar"></div>
                </div>
                <h5>MENU</h5>
            </div>
        </div>
    </div>
    
</header>
