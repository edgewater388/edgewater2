<header id="header" class="logged-out">
    <div class="lockout">
        <aside id="main-menu">
            <div id="nav-wrap">
				<?php include 'nav-menu.php' ?>
            </div>
            <div id="background-wrap">
                <div class="background closed bg-column-1">
                </div>
                <div class="background closed bg-column-2">
                </div>
                <div class="background closed bg-column-3">
                </div>
                <div id="img-wrap" class="closed"><a href="http://physique57.com/"></a>
                </div>
            </div>
        </aside>
        <div class="left">
            <ul>
                <li class="sign-in" data-modal="#sign-in"><a href="#">sign in</a></li>
                <li class="register"><a href="/sign-up">register</a></li>
                <li class="help"><a href="/faq">help</a></li>
            </ul>

        </div>
        <div class="center">
            <div class="logo">
                <a href="/">
                    <div class="inner"></div>
                </a>
            </div>
        </div>
        <div class="right">
            <div class="wrap">
                <div class="hamburger">
                    <div class="bar"></div>
                    <div class="bar"></div>
                </div>
                <h5>MENU</h5>
            </div>
        </div>

    </div>

</header>
