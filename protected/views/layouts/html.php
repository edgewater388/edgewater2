<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="en"/>

    <title><?php echo $this->pageTitle; ?></title>
    <meta name="description" content="<?php echo htmlentities($this->ogDescription); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary"/>
    <meta property="twitter:site" content="@<?php echo Yii::app()->params['twitter']; ?>"/>
    <meta property="twitter:title" content=" <?php echo $this->ogTitle; ?>"/>
    <meta property="twitter:description" content="<?php echo htmlentities($this->ogDescription); ?>"/>
    <meta property="twitter:image" content="<?php echo $this->ogImage; ?>"/>
    <meta property="twitter:url" content="http://<?php echo $this->ogUrl; ?>"/>

    <!-- Facebook -->
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="http://<?php echo $this->ogUrl; ?>"/>
    <meta property="og:title" content="<?php echo $this->ogTitle; ?>"/>
    <meta property="og:description" content="<?php echo htmlentities($this->ogDescription); ?>"/>
    <meta property="og:image" content="<?php echo $this->ogImage; ?>"/>

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,400%7COpen+Sans:300,300i,400,600,700%7CRoboto:100,300,400,500,700,900%7CDroid+Serif" rel="stylesheet">

    <link rel="shortcut icon" href="/themes/basic/images/favicon.ico">
    <link rel="icon" type="image/png" href="/themes/basic/images/favicon-32x32.png" sizes="32x32"/>
    <link rel="icon" type="image/png" href="/themes/basic/images/favicon-16x16.png" sizes="16x16"/>

	<?php $this->css[] = Yii::app()->theme->getBaseUrl() . '/css/main.css'; ?>
    <link href='https://fonts.googleapis.com/css?family=Playfair+Display:700%7CLato:100,100i,300,400,600,700,900%7CLibre+Baskerville:400,400italic'
          rel='stylesheet' type='text/css'>

    <!-- jquery ui styles -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<?php

	$themeUrl = Yii::app()->theme->getBaseUrl();
	$cs = Yii::app()->getClientScript();
	// $cs->registerCssFile($themeUrl . '/css/styles.css');

	// Allow views/controllers to add css styles with $this->css[] = [filepath];
	foreach ($this->css as $filepath)
		$cs->registerCSSFile($filepath);

	$cs->registerCoreScript('jquery');
	// $cs->registerScriptFile($themeUrl . '/js/scripts.js');
	?>

    <!--Kissmetrics-->
    <script type="text/javascript">var _kmq = _kmq || [];
      var _kmk = _kmk || '36dbd4e37a7587e3364e54bf8ffe9f1c5b444e85';
      function _kms(u) {
        setTimeout(function () {
          var d = document, f = d.getElementsByTagName('script')[0],
            s = d.createElement('script');
          s.type = 'text/javascript';
          s.async = true;
          s.src = u;
          f.parentNode.insertBefore(s, f);
        }, 1);
      }
      _kms('//i.kissmetrics.com/i.js');
      _kms('//scripts.kissmetrics.com/' + _kmk + '.2.js');
    </script>

    <!-- start Mixpanel -->
    <script type="text/javascript">(function (e, a) {
        if (!a.__SV) {
          var b = window;
          try {
            var c, l, i, j = b.location, g = j.hash;
            c = function (a, b) {
              return (l = a.match(RegExp(b + "=([^&]*)"))) ? l[1] : null
            };
            g && c(g, "state") && (i = JSON.parse(decodeURIComponent(c(g, "state"))), "mpeditor" === i.action && (b.sessionStorage.setItem("_mpcehash", g), history.replaceState(i.desiredHash || "", e.title, j.pathname + j.search)))
          } catch (m) {
          }
          var k, h;
          window.mixpanel = a;
          a._i = [];
          a.init = function (b, c, f) {
            function e(b, a) {
              var c = a.split(".");
              2 == c.length && (b = b[c[0]], a = c[1]);
              b[a] = function () {
                b.push([a].concat(Array.prototype.slice.call(arguments,
                  0)))
              }
            }

            var d = a;
            "undefined" !== typeof f ? d = a[f] = [] : f = "mixpanel";
            d.people = d.people || [];
            d.toString = function (b) {
              var a = "mixpanel";
              "mixpanel" !== f && (a += "." + f);
              b || (a += " (stub)");
              return a
            };
            d.people.toString = function () {
              return d.toString(1) + ".people (stub)"
            };
            k = "disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
            for (h = 0; h < k.length; h++)e(d, k[h]);
            a._i.push([b, c, f])
          };
          a.__SV = 1.2;
          b = e.createElement("script");
          b.type = "text/javascript";
          b.async = !0;
          b.src = "undefined" !== typeof MIXPANEL_CUSTOM_LIB_URL ? MIXPANEL_CUSTOM_LIB_URL : "file:" === e.location.protocol && "//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//) ? "https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js" : "//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";
          c = e.getElementsByTagName("script")[0];
          c.parentNode.insertBefore(b, c)
        }
      })(document, window.mixpanel || []);
      mixpanel.init("ea13324ba94e39bcfaf2bb02190ecf5c");</script>
    <!-- end Mixpanel -->

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-W2CK88');</script>
    <!-- End Google Tag Manager -->

	<?php $this->renderPartial('//partials/facebook-pixel'); ?>
	<?php $this->renderPartial('//partials/woopra'); ?>

</head>

<body class="<?php echo(Yii::app()->user->isGuest ? 'logged-out' : 'logged-in');?> section-<?php echo $this->id; ?> page-<?php echo $this->action->id; ?> <?php echo $this->bodyClass; ?>">
<?php echo $content; ?>
<!--[if IE 9]>
<script src="/themes/basic/js/vendor/matchMedia.min.js"></script>
<![endif]-->
<script language="JavaScript" type="text/javascript" src="<?php echo ($_SERVER['SERVER_PORT'] == 443) ? 'https://sadmin.brightcove.com' : 'http://admin.brightcove.com'; ?>/js/BrightcoveExperiences.js"></script>

<script type="text/javascript" src="https://content.jwplatform.com/libraries/DQsfQt1h.js"></script>
<script type="text/javascript" src="/themes/basic/js/vendor/vendor.js"></script>
<script type="text/javascript" src="/themes/basic/js/global.js"></script>
<script type="text/javascript" src="/themes/basic/js/pages/home-logged-out/slideshow.js" defer></script>
<script type="text/javascript" src="/themes/basic/js/pages/home-logged-in/jcf.js" defer></script>
<script type="text/javascript" src="/themes/basic/js/pages/home-logged-in/jcf.select.js" defer></script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W2CK88"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Login synchronization with magento -->
<?php if (isset(Yii::app()->request->cookies['magentoLogin']) && !Yii::app()->user->isGuest): unset(Yii::app()->request->cookies['magentoLogin']); $email = Yii::app()->user->email; ?>
	<iframe width=1 height=1 src=<?php echo Yii::app()->params['magentoLoginUrl'].'?code='.Yii::app()->params['securityCode'].'&email='.urlencode($email)?> scrolling=no frameborder=0></iframe>
<?php endif;?>	
</body>

<script>
  (function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
      }, i[r].l = 1 * new Date();
    a = s.createElement(o),
      m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
  })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
  ga('create', 'UA-1552479-7', 'auto');
  ga('create', 'UA-72312734-2', {'name': 'RPTracker'});
  ga('send', 'pageview');
  ga('RPTracker.send', 'pageview');
</script>
<?php if (!Yii::app()->user->isGuest): ?>
<script>
	ga("set", "userId", '<?=Yii::app()->user->email?>');
</script>
<?php endif; ?>
<script type="text/javascript">
  $('.sign-in-form').submit(function () {
    _kmq.push(['identify', $('#LoginForm_email').val()]);
    _kmq.push(['record', 'Signs into site', {'email': $('#LoginForm_email').val()}]);
  });
</script>

<!-- Pixel Tracking -->
<?php $this->renderPartial('//partials/pixel-tracking'); ?>

<?php if (Yii::app()->user->id && Yii::app()->user->getState('woopra_login') == null): ?>
	<?php Yii::app()->user->setState('woopra_login', 1); ?>
    <script>
      woopra.track("user_logged_in", {});
    </script>
<?php endif; ?>
<script src="//cdn.leadmanagerfx.com/js/mcfx/1108" type="text/javascript"></script>

</html>