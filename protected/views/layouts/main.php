<?php $this->beginContent('//layouts/html'); ?>

<?php
    if (Yii::app()->user->isGuest)
        $this->renderPartial('//global/header-logged-out');
    else
        $this->renderPartial('//global/header-logged-in');
?>

<?php if ($this->clips['jumbotron']): ?>
    <section class="jumbotron">
        <div class="jumbotron-inner">
            <?php echo $this->clips['jumbotron']; ?>
        </div>
    </section>
<?php endif; ?>

    <div class="alerts">
        <div class="lockout">
            <div class="inner">
                <?php Flash::displayFlashes(); ?>
            </div>
        </div>
    </div>

    <div id="page-wrap">
        <?php echo $content; ?>
    </div><!-- page -->
<?php $this->renderPartial('//global/footer'); ?>
<?php $this->renderPartial('//modals/sign-in'); ?>
<?php $this->renderPartial('//templates/brightcove-embed'); ?>
<?php $this->renderPartial('//templates/mashup-thumb'); ?>
<?php $this->renderPartial('//templates/video-li'); ?>
<?php $this->renderPartial('//templates/equipment-li'); ?>
<?php $this->renderPartial('//modals/mashup-name'); ?>
<?php $this->endContent(); ?>
