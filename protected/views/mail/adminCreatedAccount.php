<?php include(dirname(__FILE__).'/parts/_header.php'); ?>

<p>
	An administrator at <?php echo Yii::app()->name; ?> has created an account for you. You can <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/login">log in</a> with the following email address and password:
</p>
<p>
	<strong>Email</strong><br>
	<?php echo $user->email; ?><br><br>

	<strong>Password</strong><br>
	<?php echo $password; ?><br><br>
</p>
<p>
	If you have any questions, please email us at <a style="<?php echo $css['a']; ?>" href="mailto:<?php echo Yii::app()->params['adminEmail']; ?>"><?php echo Yii::app()->params['adminEmail']; ?></a>.
</p>

<?php include(dirname(__FILE__).'/parts/_footer.php'); ?>