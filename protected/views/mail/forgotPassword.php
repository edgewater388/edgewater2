<?php include(dirname(__FILE__).'/parts/_header.php'); ?>

<p>Someone requested that the password be reset for the following account: <br><?php echo $user->email; ?></p>

<p>If this was a mistake, just ignore this email and nothing will happen.</p>

<p>
	To reset your password, visit the following address: <br>
	<a href="<?php echo $user->getPasswordResetLink(); ?>" style="<?php echo $css['btn']; ?>">Reset Password</a>
</p>

<?php include(dirname(__FILE__).'/parts/_footer.php'); ?>