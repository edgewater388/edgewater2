<?php include(dirname(__FILE__).'/_styles.php'); ?>

<div style="background-color: #f3f3f3; margin:0!important; padding:0!important;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td style="background-color: #fff;">
					<table border="0" cellspacing="0" cellpadding="0" style="<?php echo $css['table']; ?> padding: 0; background-color: #f3f3f3;">
						<tbody>
							<tr>
								<td>
									<table border="0" cellspacing="0" cellpadding="0" style="<?php echo $css['table']; ?> padding: 20px 0; background-color: #fff; color: #fff;">
										<tbody>
											<tr>
												<td>&nbsp;</td>
												<td style="text-align: center; padding-bottom: 32px;" width="550">
													<a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>"><img src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/themes/basic/images/global/header/logo-dark.png" alt="<?php echo Yii::app()->name; ?>" width="133" height="105" style="background-color: transparent;"></a>
												</td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td width="550">
													<h1 style="<?php echo $css['h1']; ?>"><?php echo $mail->tagline ? $mail->tagline : $mail->getSubject(); ?></h1>
												</td>
												<td>&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table border="0" cellspacing="0" cellpadding="0" style="<?php echo $css['table']; ?>">
										<tbody>
											<tr>
												<td>&nbsp;</td>
												<td style="padding: 30px 0px;" width="550">
