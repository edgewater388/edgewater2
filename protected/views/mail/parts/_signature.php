<?php if (isset($signature)): ?>
<tr>
	<td>
		<table border="0" cellspacing="0" cellpadding="0" style="<?php echo $css['table']; ?> padding-top: 0;">
			<tbody>
				<tr>
					<td>
						<p>
							<?php echo $signature; ?>
						</p>
					</td>
				</tr>
			</tbody>
		</table>
	</td>
</tr>
<?php endif; ?>
