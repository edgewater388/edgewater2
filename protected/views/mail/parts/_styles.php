<?php /*
<style type="text/css">
	<?php //include_once(Yii::app()->getBasePath().'/../themes/mail/css/bootstrap.min.css'); ?>
</style>
*/ ?>

<?php 
$css = array(
	'table'=>'font-family:Arial,Helvetica,Arial,sans-serif; font-size: 14px; line-height: 22px; color: #5c636d; width: 100%; padding: 20px;',
	'p'=>'font-family:Arial,Helvetica,Arial,sans-serif; font-size: 14px; line-height: 22px; color: #5c636d;',
	'btn'=>'color: white; font-weight: bold; background-color: #6ecff6; display: inline-block; padding: 0 16px; border-width: 1px; line-height: 26px; text-decoration: none; border-radius: 3px; border: 1px solid #57c9f5;',//border: 1px solid rgba(0,0,0,0.1); background-image: -moz-linear-gradient(top, #F79321, #DF831A); background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#F79321), to(#DF831A)); background-image: -webkit-linear-gradient(top, #F79321, #DF831A); background-image: -o-linear-gradient(top, #F79321, #DF831A); background-image: linear-gradient(to bottom, #F79321, #DF831A); background-repeat: repeat-x;',
	'a'=>'color: #6ecff6;',
	'quote'=>'font-weight: bold; font-family: Georgia, Times, serif; font-style: italic;',
	'h1'=>'color: #5c636d; text-align:center; text-transform: uppercase; font-family: Times New Roman, Georgia, Serif; font-size: 22px; line-height: 34px; letter-spacing: 3pt;',
	'h2'=>'color: #5c636d; text-transform: uppercase; font-family: Times New Roman, Georgia, Serif; font-size: 22px; line-height: 28px;',
	//'quote-name'=>'font-weight: bold; font-family: Georgia, Times, serif; text-align: right; color: #999;',
);
?>

<style>
	a { <?php echo $css['a']; ?> }
	<?php /*foreach ($css as $k=>$v): ?>
	<?php echo $k; ?> {
		<?php echo $v; ?>
	}
	<?php endforeach;*/ ?>

	html,
	body {
		background-color: #f5f5f5;
		width: 100%;
		margin: 0;
		padding: 0;
	}
</style>