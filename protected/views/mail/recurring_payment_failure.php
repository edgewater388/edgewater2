<?php include(dirname(__FILE__).'/parts/_header.php'); ?>

<p>Sorry for the interruption, but we are having trouble authorising your 
	<?php if ($customer): ?>
	<?php echo $customer->card_type; ?> ending 
	in <?php echo $customer->card_number; ?>. 
	<?php else: ?>
	card.
	<?php endif; ?>
	Please visit the account payment page at 
	<a href="<?php echo Yii::app()->createAbsoluteUrl('/user/users/account'); ?>" style="<?php echo $css['a']; ?> text-decoration:none;"><?php echo Yii::app()->createAbsoluteUrl('/user/users/account'); ?></a> 
	and update your payment information.</p>

<?php include(dirname(__FILE__).'/parts/_footer.php'); ?>