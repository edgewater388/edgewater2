<?php include(dirname(__FILE__).'/parts/_header.php'); ?>

<h2 style="<?php echo $css['h2']; ?>">Order <?php echo $transaction->id; ?></h2>
<table style="<?php echo $css['table']; ?> padding: 0;">
	<thead>
		<tr>
			<th style="text-align: left; font-weight: bold;">Product</th>
			<th style="text-align: left; font-weight: bold;">Quantity</th>
			<th style="text-align: right; font-weight: bold;">Price</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo $transaction->description; ?></td>
			<td>1</td>
			<td style="text-align: right;">$<?php echo $membership->subtotal; ?></td>
		</tr>
		<?php if ($membership->total - $membership->getSubtotal() > 0): ?>
		<tr>
			<td colspan="2">
				Discount
			</td>
			<td style="text-align: right;">-$<?php echo ($membership->total - $membership->getSubtotal()); ?></td>
		</tr>
		<?php endif; ?>
		<tr>
			<td colspan="2" style="font-weight: bold;">Subtotal:</td>
			<td style="text-align: right;">$<?php echo $transaction->subtotal; ?></td>
		</tr>
		<tr>
			<td colspan="2" style="font-weight: bold;">Tax:</td>
			<td style="text-align: right;">$<?php echo $transaction->tax; ?></td>
		</tr>
		<tr>
			<td colspan="2" style="font-weight: bold;">Total:</td>
			<td style="text-align: right;">$<?php echo $transaction->total; ?></td>
		</tr>
	</tbody>
</table>

<br>

<h2 style="<?php echo $css['h2']; ?>">Subscription Information</h2>
<table style="<?php echo $css['table']; ?> padding: 0;">
	<thead>
		<tr>
			<th style="text-align: left; font-weight: bold;">Start Date</th>
			<th style="text-align: left; font-weight: bold;">End Date</th>
			<th style="text-align: left; font-weight: bold;">Auto Renew</th>
			<th style="text-align: right; font-weight: bold;">Price</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo date('Y-m-d'); ?></td>
			<td><?php echo $membership->valid_to; ?></td>
			<td><?php echo $membership->auto_renew ? 'Yes' : 'No'; ?></td>
			<?php /*
			<td style="text-align: right;">$<?php echo $transaction->subtotal; ?></td>
			*/ ?>
			<td style="text-align: right;">
				<?php if ($membership->total != $membership->getSubtotal()): ?>
					<span style="text-decoration: line-through">$<?php echo $membership->total; ?></span>
					$<?php echo ($membership->getSubtotal()); ?><br>
					<?php if ($membership->promo_end_date): ?>
						<span style="font-size: 10px;">(expires <?php echo $membership->promo_end_date; ?>)</span>
					<?php endif; ?>
				<?php else: ?>
					$<?php echo $membership->total; ?>
				<?php endif; ?>
			</td>
		</tr>
	</tbody>
</table>

<br>

<h2 style="<?php echo $css['h2']; ?>">Your Details</h2>
<table style="<?php echo $css['table']; ?> padding: 0;">
	<tbody>
		<tr>
			<td style="width: 90px; font-weight: bold;">Email:</td>
			<td><?php echo $user->email; ?></td>
		</tr>
	</tbody>
</table>

<h2 style="<?php echo $css['h2']; ?>">Billing Address</h2>
<table style="<?php echo $css['table']; ?> padding: 0;">
	<tbody>
		<tr>
			<td>
				<?php echo $user->profile->name; ?><br>
				<?php echo $customer ? $customer->zipcode : null; ?>
			</td>
		</tr>
	</tbody>
</table>

<?php include(dirname(__FILE__).'/parts/_footer.php'); ?>