<?php include(dirname(__FILE__).'/parts/_header.php'); ?>

<h2 style="<?php echo $css['h2']; ?> text-align: center;">Workout anytime, anywhere.</h2>
<p style="<?php echo $css['p']; ?> text-align: center;">
	Your friend <strong><?php echo $referralInvite->user->profile->first_name; ?></strong> has invited you to check out <?php echo Yii::app()->name; ?> On Demand.
</p>

<?php $promo_code_id = Content::getValue('refer_friend_promo_code'); ?>
<?php if ($promo_code_id && $promoCode = PromoCode::retrieveById($promo_code_id)): ?>
<p style="<?php echo $css['p']; ?> text-align: center;">
	Bonus! <br>
	<?php echo $promoCode->description; ?><br>
	with promo code: <strong><?php echo $promoCode->code; ?></strong>
</p>
<?php endif; ?>

<p style="<?php echo $css['p']; ?> text-align: center;">
	<a href="http://<?php echo $_SERVER['HTTP_HOST']; ?><?php echo Yii::app()->createUrl('/user/users/register'); ?>" style="<?php echo $css['btn']; ?>">Join <?php echo Yii::app()->name; ?></a>
</p>

<?php include(dirname(__FILE__).'/parts/_footer.php'); ?>