<?php include(dirname(__FILE__).'/parts/_header.php'); ?>

<h2 style="<?php echo $css['h2']; ?> text-align: center;">You've earned $<?php echo $model->amount; ?>!</h2>
<p style="<?php echo $css['p']; ?> text-align: center;">
	Your friend <strong><?php echo $model->referredUser->profile->first_name; ?></strong> has joined <?php echo Yii::app()->name; ?> because of you. To say thank you, you'll receive $<?php echo $model->amount; ?> off your next month!
</p>

<?php include(dirname(__FILE__).'/parts/_footer.php'); ?>