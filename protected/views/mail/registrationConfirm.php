<?php include(dirname(__FILE__).'/parts/_header.php'); ?>

<p>
	Thank you for signing up at <?php echo Yii::app()->name; ?>. Please confirm your email address.
</p>
<p>
	<a style="<?php echo $css['a']; ?>" href="<?php echo $user->getConfirmLink(); ?>" style="<?php echo $css['btn']; ?>">Confirm your account now</a>
</p>
<p>
	Or, click the following link:
	<a style="<?php echo $css['a']; ?>" href="<?php echo $user->getConfirmLink(); ?>" style="<?php echo $css['a']; ?>"><?php echo $user->getConfirmLink(); ?></a>
</p>

<?php include(dirname(__FILE__).'/parts/_footer.php'); ?>