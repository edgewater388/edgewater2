<?php include(dirname(__FILE__).'/parts/_header.php'); ?>

<p style="<?php echo $css['p']; ?>">
	Dear <?php echo $user->profile->first_name; ?>,
</p>

<h2 style="<?php echo $css['h2']; ?>">WELCOME TO STRONGER. WELCOME TO LEANER. WELCOME TO PHYSIQUE 57!</h2>

<p style="<?php echo $css['p']; ?> margin: 8px 0;">
	It's official. You're now officially part of the Physique 57 fitness community!
</p>
<p style="<?php echo $css['p']; ?> margin: 8px 0;">
	Results are proven. Our instructors are the best in the industry. And our team's commitment to you and your goals is the real deal. Take this moment and commit to giving these workouts (and yourself) your all.
</p>
<p style="<?php echo $css['p']; ?> margin-top: 8px;">
	We encourage you to engage with our global fitness community online where you'll find tips, motivation and camaraderie by connecting with other Physique 57 enthusiasts through Facebook and twitter to keep you energized.
</p>

<h2 style="<?php echo $css['h2']; ?>">STAY UP TO DATE WITH PHYSIQUE </h2>
<p style="<?php echo $css['p']; ?>">
	Bookmark our 
	<a style="<?php echo $css['a']; ?>" href="http://www.ondemand.physique57.com">website</a> 
	for news and updates, new client specials and more. Follow us on 
	<a style="<?php echo $css['a']; ?>" href="http://email.mindbodyonline.com/wf/click?upn=x7yemOdK3RYrexfkPbU-2FcGJTUSljb7L1JaSffCTxTv2UTqJtJ0naQ4w-2FqdUknQZE_OLfa7I4aweHaEAGsEOGt6pA4cR92NNv8WczwFcmLoU4LxR0SkGpX4v6VOe-2FM2ghPuZCUgmJSLfXyonVBVq9DkWw4kSbR6EjfpfAIHFUg9VBZcPWGUSmajYpsu308Lu0-2FGRokaPysrlA3eVWeEBGI0Jv5MBq5Lv-2B68TkXtlHyyBkHhI-2F6LRrtPiwrQ9uGNhfn2QhW9HfbSrxUyMI8Idm1wS20rd0zIHaIGl7kJ0FJotMlsIW7nouhdVDgz49cpv7AiYkU7iK9EGRrVUjeGQiHta9VSTYLxO0jVCJTdSnaCtc-3D">Facebook</a>, 
	<a style="<?php echo $css['a']; ?>" href="http://email.mindbodyonline.com/wf/click?upn=FFryBNCoBbZa6SG5AGS40rZTrf3VMuO96B4e-2FAJ3xrGiwc5AH3bOfBAb6oITWLsh_OLfa7I4aweHaEAGsEOGt6pA4cR92NNv8WczwFcmLoU4LxR0SkGpX4v6VOe-2FM2ghPuZCUgmJSLfXyonVBVq9DkWw4kSbR6EjfpfAIHFUg9VBZcPWGUSmajYpsu308Lu0-2FGRokaPysrlA3eVWeEBGI0OkVpUTGrcnqFRr8t-2FszPWz-2FfjwIg57rdAny3rzND6lQrBdsW0atlsseflgCSdJa-2F-2F-2FETuqbdMKIFhXMa2iL0WhGJePzKZGMcZ2n5yfVn-2BT-2BVMWLDWcQ5GL8M4o6aC1Yg-2BcRTxMarPlIB6PhcGSq0B8-3D">Twitter</a>, and 
	<a style="<?php echo $css['a']; ?>" href="http://www.instagram.com/physique57">Instagram</a> 
	for exclusive promotions and to join in on the <strong>PHYSIQUE 57®</strong> conversation
	 -- from fitness and guest expert nutrition tips to fun giveaways and healthy living #inspo. 
	 Together we will celebrate your goals from start to finish, cheering you on every pulse, 
	 lift, and rep of the way.
</p>

<p style="<?php echo $css['p']; ?>">
	We know that finding the time to workout and stay dedicated isn't easy. 
	By adapting our studio classes to on-the-go devices, we hope we're making it a little easier for you 
	to devote time to bettering yourself. That's our ultimate mission.
</p>

<p style="<?php echo $css['p']; ?>">
You've got this. Now let's get started!
</p>

<p style="<?php echo $css['p']; ?>">
	Jennifer Maanavi & Tanya Becker
</p>

<p style="<?php echo $css['p']; ?>">
	Co-founders, Physique 57
</p>

<?php include(dirname(__FILE__).'/parts/_footer.php'); ?>