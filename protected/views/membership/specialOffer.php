<?php $this->bodyClass = 'faq short-nav short-nav-perm'; ?>
<?php //$this->bodyClass = 'sign-up no-top-padding'; ?>
<?php $this->bodyClass .= ' payment'; ?>
<?php //$this->bodyClass = 'payment dark-header no-top-padding'; ?>

<section class="header">
    <header>
        <div class="lockout">
            <div class="side-lines">
                <h1>Sign Up</h1>
            </div>
            <h4 class="sub-header">Special Offer</h4>
        </div>
    </header>
</section>    

<section id="payment" style="padding-top: 0;">
    <?php $form=$this->beginWidget('ActiveForm', array(
        'action'=>Yii::app()->createUrl('/membership/specialOffer'),
        'htmlOptions'=>array(
            'data-validate'=>''
        ),
    )); ?>

    <?php echo $form->errorSummary($user); ?>
    <?php echo $form->errorSummary($customer); ?>

    <div id="hero">
        <div class="right" data-mobile="/themes/basic/images/payment/hero-girl-mobile.jpg"
             data-desktop="/themes/basic/images/payment/hero-girl-desktop.jpg">
        </div>
        <div class="left">
            <div class="inner">
                <div class="title">
                    <h3><span class="dollar">$</span><?php echo $plan['amount']; ?> <span class="term">MONTHLY</span></h3>

                    <div class="free">First week free</div>
                </div>
                <div class="side-lines">&nbsp;</div>
                <div class="body-copy">
                    <p>Work out using the device of your choice.</p>
                    <p> Stream content via your laptop, TV, phone or tablet.</p>
                    <p>Unlimited viewing access.</p>
                    <p>Cancel anytime.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="top">
        <div class="lockout">
            <div class="side-lines">
                <h1 style="font-size: 32px;">start your free trial</h1>
            </div>

            <div class="inputs">
                <?php echo $form->textField($user,'email',array('maxlength'=>128,'placeholder'=>'Email Address','data-validate'=>'min:5|email', 'class' => 'email')); ?>
                <?php echo $form->passwordField($user,'password',array('maxlength'=>128,'placeholder'=>'Password','data-validate'=>'min:6', 'class' => 'password')); ?>
            </div>
            <ul class="error-messages">
                <li class="email">Please enter a valid email!</li>
                <li class="password">Your password must be at least six characters!</li>
            </ul>
        </div>
    </div>

    <div class="lockout">
        <div class="side-lines">
            <h1>sign up</h1>
        </div>
    </div>

    <div id="payment-details">
        <div class="lockout">
            <h4>ENTER YOUR PAYMENT DETAILS</h4>
            <hr>
            <div class="top-text">
                <ol>
                    <li>You will not be charged until after your free trial ends on <?php echo date('m/d/y', strtotime('+1 week')); ?>.</li>
                    <li>No commitments, cancel online anytime.</li>
                    <li>Unlimited streaming for 1 user.</li>
                </ol>
            </div>


            <div class="credit-card">
                <p>CREDIT CARD</p>
                <div class="cards"></div>
            </div>

                <div class="inputs">
                    <div class="row">
                        <div class="input-item first">
                            <label>FIRST NAME <span class="must-match"><span class="darker-blue">*</span>Must match card</span></label>
                            <?php echo $form->textField($customer, 'first_name', array('data-validate'=>'min:2')); ?>
                        </div>
                        <div class="input-item last">
                            <label>LAST NAME <span class="must-match"><span class="darker-blue">*</span>Must match card</span></label>
                            <?php echo $form->textField($customer, 'last_name', array('data-validate'=>'min:2')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-item street first">
                            <label>STREET ADDRESS</label>
                            <?php echo $form->textField($customer, 'street', array('data-validate'=>'min:2')); ?>
                        </div>
                        <div class="input-item zip">
                            <label>ZIP CODE</label>
                            <?php echo $form->textField($customer, 'zipcode', array('maxlength'=>'12')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-item card lock">
                            <label>CARD NUMBER</label>
                            <?php echo $form->textField($customer, 'new_card_number', array('data-validate'=>'min:13')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-item expiration-month">
                            <label>EXPIRATION DATE</label>
                            <?php echo $form->dropDownList($customer, 'card_month', Time::months(), array('id'=>'expiry-month', 'data-validate'=>'min:1')); ?>
                            <div class="down-arrow"><img src="/themes/basic/images/global/down-arrow.svg"></div>
                        </div>
                        <div class="input-item expiration-year">
                            <label>&nbsp;</label>
                            <?php echo $form->dropDownList($customer, 'card_year', Time::years(), array('id'=>'expiry-year', 'data-validate'=>'min:1')); ?>
                            <div class="down-arrow"><img src="/themes/basic/images/global/down-arrow.svg"></div>
                        </div>
                        <div class="input-item security-code lock">
                            <label>SECURITY CODE</label>
                            <?php echo $form->textField($customer, 'card_security', array('data-validate'=>'min:2', 'maxlength'=>4)); ?>
                        </div>
                        <div class="input-item qmark">
                            <span class="rollover">
                                <img src="/themes/basic/images/account/credit-cards-hover@2x.png" width="276">
                            </span>
                        </div>
                        <?php /*
                        <div class="input-item coupon">
                            <input type="text" maxlength="30" placeholder="Coupon Code"/>
                        </div>
                        <div class="input-item apply-coupon">
                            <a class="btn" href="#">apply coupon</a>
                        </div>
                        */ ?>
                    </div>
                </div>

                <div class="legal">
                    <p>Your Physique 57 VOD membership, which starts with a 7 day free trial offer, will begin when you
                        click Start Membership. Simply cancel anytime within your trial period and you will not be
                        charged. To cancel, go to "Your Account" and click on "Cancel Membership." No refunds or credits
                        for partial months. By clicking Start Membership, you authorize us to continue your
                        month-to-month Physique 57 VOD membership (currently $<?php echo $plan['amount']; ?> per month)
                        automatically charged monthly to the payment method provided, until you cancel. Only one free
                        trial per member. See Terms of Use for more details.
                    </p>
                </div>
                <div class="checkbox-wrap">
                    <div class="left">
                        <!-- <input type="checkbox" name="over_18" id="over_18" class="optional" value="1"/> -->
                        <?php echo $form->checkbox($customer, 'over_18', array('id' => 'over_18')); ?>
                    </div>
                    <div class="right">
                        <label class="checkbox" for="over_18">
                            <div
                                class="main-checkbox">I am over 18, and I agree to the above conditions and the <a
                                    href="http://physique57.com/terms-and-conditions/" target="_blank">Terms of
                                    Use</a> and <a href="http://physique57.com/privacy-legal/" target="_blank">Privacy / Cookies</a>.
                            </div>
                        </label>
                    </div>
                </div>
                <div class="buttons">
                    <input type="submit" class="btn blue big" value="start membership" id="start-membership-btn"/>
                </div>
                <div class="secure">
                    <img src="/themes/basic/images/global/icon-lock@2x.png" width="26" alt="Secure">
                    <p>Secure Server</p>
                    <p class="tell">Tell me more<span class="rollover">
                                We take your privacy seriously We safeguard your payment information (such as credit card number)
                                with SSL (Secure Sockets Layer) when sent over the Internet. SSL encrypts personal information,
                                including your name and payment information, so that it cannot be read in transit by a third party.
                                We also store your payment information in an encrypted format.</span>
                    </p>
                </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</section>

<script type="text/javascript">
	    $('#yw0').submit(function () {
	        _kmq.push(['identify', $('#Users_email').val()]);
	        _kmq.push(['record', 'Starts free trial', {'email': $('#Usersemail').val()}]);
	    });
	</script>
