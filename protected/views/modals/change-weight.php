<div class="modal" id="change-weight">
	<div class="lockout">
		<div class="close-btn close-modal"></div>
		<div class="inner sign-in">
			<h1>Personal Info</h1>

			<?php $form = $this->beginWidget('CActiveForm', array(
				'id'=>'start-program-form',
				'enableAjaxValidation'=>true,
				'action' => '/account/'.Yii::app()->user->id.'/editprofile/'.$program_id,
				'htmlOptions'=>array(
					'class' => 'start-program-form',
				),
			)); ?>

			<div class="inputs">

				<?= $form->textField( Yii::app()->user->profile, 'weight', array(
					'class' => 'form-control', 'placeholder' => 'Weight (lb.)',
				));?>

				<span class="select-wrap">
					<?= $form->dropDownList( Yii::app()->user->profile, 'fitness_level', Profile::getFitnessLevels()); ?>
				</span>

                <label>
                    <?= $form->checkBox( Yii::app()->user->profile, 'do_not_show_weight_popup', array(
                            'class' => 'form-control',
                    ));?> Do not show me this popup anymore
                </label>

				<div class="blue-dot-btn sign-in-btn">
					<a href="#">
						Update weight
						<div class="border tl"></div>
						<div class="border tr"></div>
						<div class="border br"></div>
						<div class="border bl"></div>
					</a>
				</div>
			</div>

			<?php $this->endWidget(); ?>
		</div>
	</div>
</div>