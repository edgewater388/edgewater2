<div class="modal" id="confirm-mashup-delete">
    <div class="lockout">
        <div class="close-btn close-modal"></div>
        <div class="inner confirm-mashup-delete">
            <h1>DELETE THIS MASHUP?</h1>
            <div class="buttons">
                <div class="btn blue close-modal">cancel</div>
                <div class="btn blue close-modal confirm">delete</div>
            </div>
        </div>
    </div>
</div>