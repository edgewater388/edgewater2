<div class="modal" id="confirm-plan">
    <div class="lockout">
        <div class="close-btn close-modal"></div>
        <div class="inner">
            <h1>change plan</h1>
            <ul>
                <li>
                    <h5 class="type">current plan</h5>
                    <?php if ($model->isActive()): ?>
                    <div class="thumb">
                        <img src="/themes/basic/images/account/renew-plan-thumb<?php echo $model->plan_id; ?>.jpg">
                        <div class="text-overlay">
                            <h3><span class="dollar">$</span><?php echo number_format($model->subtotal, 2); ?></h3>
                            <h5><?php echo $model->interval; ?></h5>
                        </div>
                    </div>
                    <?php else: ?>
                    None
                    <?php endif; ?>
                </li>
                <li>
                    <?php 
                        $next_billing_date = null;
                        if ($membership = Yii::app()->user->membership)
                        {
                            // membership is still active
                            if ($membership->valid_to && $membership->valid_to >= date('Y-m-d'))
                                $next_billing_date = date('n/j/Y', strtotime($membership->valid_to));
                            // membership will be billed right away
                            else
                                $next_billing_date = date('n/j/Y');
                        }

                        // new membership
                        if (!$next_billing_date)
                            $next_billing_date = date('n/j/Y', strtotime(Membership::getTrialEndDate()));
                    ?>
                    <h5 class="type">NEW PLAN (STARTING <?php echo $next_billing_date; ?>)</h5>
                    <div class="thumb active">
                        <img class="confirm-image" src="/themes/basic/images/account/renew-plan-thumb2.jpg">
                        <div class="text-overlay confirm-text-overlay"></div>
                    </div>
                </li>
            </ul>
            <div class="buttons">
                <input type="submit" class="btn blue" value="confirm change" />
                <br>
                <div class="btn white close-modal">cancel</div>
            </div>
        </div>
    </div>
</div>