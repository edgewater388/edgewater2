<div class="modal" id="finish-program">
    <div class="lockout">
        <div class="close-btn close-modal"></div>
        <div class="inner finish-program">
            <h1>Congratulations!</h1>

            <p>You have successfully completed the program!</p>

            <?php if ($skipped_count): ?>
                <p class="has-skipped-workouts">
                    You have <?= $skipped_count; ?> skipped days, would you like to complete them or finish now?
                </p>
            <?php endif; ?>

            <div class="blue-dot-btn sign-in-btn">
                <a href="<?= $cancel_program_url; ?>">
                    Finish
                    <div class="border tl"></div>
                    <div class="border tr"></div>
                    <div class="border br"></div>
                    <div class="border bl"></div>
                </a>
            </div>
        </div>
    </div>
</div>