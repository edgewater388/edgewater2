<div class="modal" id="lowerPriceOffering">
    <div class="lockout">
        <div class="close-btn close-modal"></div>
        <div class="inner">
            <h1>$<?= $price_with_discount = Membership::getPriceWithDiscount(); ?> Monthly if You Stay.</h1>
            <div class="content content--modal">
                <p>Thanks for being a long time customer and making your health and fitness a priority in your life.
                    We don't want you to lose the great momentum you have built over the past months, so to help you
                    stay we would like to offer you a $<?= $price_with_discount; ?> monthly plan.</p>
            </div>
            <div class="buttons-inline">
                <div class="blue-dot-btn">
                    <a href="/stay-on-account">
                        Get the discount
                        <div class="border tl"></div>
                        <div class="border tr"></div>
                        <div class="border br"></div>
                        <div class="border bl"></div>
                    </a>
                </div>
                <div class="blue-dot-btn">
                    <a href="/cancel-account">
                        Cancel membership
                        <div class="border tl"></div>
                        <div class="border tr"></div>
                        <div class="border br"></div>
                        <div class="border bl"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>