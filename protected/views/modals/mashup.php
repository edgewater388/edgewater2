<div class="modal" id="mashup">
    <div class="lockout">
        <div class="close-btn close-modal"></div>
        <div class="inner mashup">
            <h1 class="video-title move-up"></h1>
            <div class="player bc-player"></div>
            <div class="bottom">
                <div class="slider">
                    <h5>IN THIS MASHUP</h5>
                    <ul class="thumb-slider video-thumbs">
                    </ul>
                </div>
                <div class="description">
                    <p></p>
                </div>
                <div class="equipment-required">
                    <h4>Get Optional Equipment</h4>
                    <ul class="equipment-thumbs"></ul>
                </div>
            </div>
        </div>
    </div>
</div>
