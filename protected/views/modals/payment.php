<div class="modal" id="payment">
    <div class="lockout">
        <div class="close-btn close-modal"></div>
        <div class="inner pgrogram-modal">
            <p>We ask for your payment information now so you can enjoy Physique 57 uninterrupted after your 30-day free trial period ends. If you cancel your subscription before the end of the 30-day free trial, your card won’t be charged.</p>
            <p>You can cancel anytime within the first 30 days by going to the “Account" section on our website</p>
        </div>
    </div>
</div>