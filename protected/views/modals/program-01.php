<div class="modal" id="program-01">
    <div class="lockout">
        <div class="close-btn close-modal"></div>
        <div class="inner pgrogram-modal">
            <h1>Lose 10 in 10</h1>
            <p>Ten weeks. That’s all it takes to lose those nagging 10 pounds that won’t seem to budge. Our barre online lessons are specifically designed to help you tackle your toughest trouble spots and melt away the fat. Coupled with our Nutrition Plan, this program will help boost your metabolic rate, and get your heart pumping. You’ll have fun and see results with these challenging workouts designed just for women!</p>
        </div>
    </div>
</div>