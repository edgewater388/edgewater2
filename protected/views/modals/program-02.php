<div class="modal" id="program-02">
    <div class="lockout">
        <div class="close-btn close-modal"></div>
        <div class="inner pgrogram-modal">
            <h1>Best Beach Body</h1>
            <p>This 4 week program is specifically designed to help you feel confident, sexy, and strong no matter what time of the year they may be visiting a beach, poolside or on vacation. You will not only look amazing, but feel toned and trim from head to toe. Exercises focus on areas specific to wearing a bathing suit: e.g lifting and sculpting glutes, whittling down the waistline, and inner and outer thighs and arm chiseling moves.</p>
        </div>
    </div>
</div>