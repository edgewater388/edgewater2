<div class="modal" id="program-04">
    <div class="lockout">
        <div class="close-btn close-modal"></div>
        <div class="inner pgrogram-modal">
            <h1>Best. Body. Ever</h1>
            <p>Twelve weeks to redefine your body, for this season and forever. Ideal for resetting your fitness routine, metabolism, and physique from the ground up. You'll build stamina, endurance and your dream body with commitment and consistency. Get ready for the most rewarding and best fitness journey of your life.</p>
        </div>
    </div>
</div>