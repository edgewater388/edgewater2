<div class="modal" id="program-05">
    <div class="lockout">
        <div class="close-btn close-modal"></div>
        <div class="inner pgrogram-modal">
            <h1>Two Weeks to Hard Core</h1>
            <p>This Program will not only cinch every inch of your core, but also leave you feeling stronger, taller, and more confident. Fast paced, high intensity, core shredding sequences will have you pulsing, tucking, and twisting your way to your best body yet. Are you ready to be hard core?</p>
        </div>
    </div>
</div>