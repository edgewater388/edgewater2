<div class="modal" id="program-06">
    <div class="lockout">
        <div class="close-btn close-modal"></div>
        <div class="inner pgrogram-modal">
            <h1>Total Body Blast</h1>
            <p>Four weeks of high-intensity cardio and strength training combos that will result in a total body transformation. Blast calories, burn fat, and rev your metabolism. The results are serious -- and will set you on a path for your best body for life.</p>
        </div>
    </div>
</div>