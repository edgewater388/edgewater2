<?php
$model = new LoginForm;
// $model->email = 'foo6@bar.com';
// $model->password = 'gl0bal';

if (isset($_POST['LoginForm'])) {
	$model->attributes = $_POST['LoginForm'];
	$model->validate();
}
?>

<div class="modal" id="sign-in">
    <div class="lockout">
        <div class="close-btn close-modal"></div>
        <div class="inner sign-in">
            <h1>sign in</h1>
			<?php $form = $this->beginWidget('ActiveForm', [
				'action'      => Yii::app()->createUrl('/user/users/login', ['login' => 1]),
				'htmlOptions' => [
					'data-validate'   => '',
					'data-submit-btn' => '.blue-dot-btn',
					'class'           => 'sign-in-form',
				],
			]); ?>

			<?= $form->errorSummary($model); ?>
            <div class="inputs">
				<?= $form->textField($model, 'email', ['placeholder' => 'Email Address', 'data-validate' => 'min:5|email']); ?>
				<?= $form->passwordField($model, 'password', ['placeholder' => 'Password', 'data-validate' => 'min:6']); ?>
				<?php
                    if (!$dest = Yii::app()->request->getParam('dest')) {
                        $url = parse_url($_SERVER['REQUEST_URI']);
                        $dest = $url['path'];
                    }
				?>
                <input type="hidden" name="dest" value="<?= $dest; ?>">
            </div>
            <div class="blue-dot-btn sign-in-btn">
                <a href="#">
                    SIGN IN
                    <div class="border tl"></div>
                    <div class="border tr"></div>
                    <div class="border br"></div>
                    <div class="border bl"></div>
                </a>
            </div>
            <input type="submit" class="hide"/>
			<?php $this->endWidget(); ?>
            <p class="forgot"><a href="/forgot-password">Forgot Your Password?</a></p>
            <p class="new">NEW TO PHYSIQUE 57?<a href="/sign-up">SIGN UP</a></p>

            <?php $auth_links = Users::getSocialAuthLinks(); ?>
            <div class="log-in-socials-wr">
                <div class="log-in-top"><span>OR</span></div>
                <a class="social-btn social-btn--facebook" href="<?= $auth_links['facebook']; ?>">
                    <span class="icon-facebook-logo"></span>SIGN IN WITH FACEBOOK
                </a>
                <a class="social-btn social-btn--gplus" href="<?= $auth_links['google']; ?>">
                    <span class="icon-google"></span>SIGN IN WITH GOOGLE+
                </a>
            </div>
        </div>
    </div>
</div>