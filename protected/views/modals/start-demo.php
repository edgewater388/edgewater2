<div class="modal" id="start-demo" data-auto-open-modal="true">
	<div class="lockout">
		<div class="close-btn close-modal"></div>
		<div class="inner start-demo">
			<h1><?= Settings::getSettingValue(Settings::SETTING_STREAM_POPUP_TITLE); ?></h1>
			<div class="content">
				<?= Settings::getSettingValue(Settings::SETTING_STREAM_POPUP_TEXT); ?>
			</div>
			<div class="blue-dot-btn sign-in-btn">
				<a href="<?= $this->createUrl('/start-demo'); ?>">
					Watch Live Stream
					<div class="border tl"></div>
					<div class="border tr"></div>
					<div class="border br"></div>
					<div class="border bl"></div>
				</a>
			</div>
		</div>
	</div>
</div>