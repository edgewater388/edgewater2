<div class="modal" id="start-program">
    <div class="lockout">
        <div class="close-btn close-modal"></div>
        <div class="inner sign-in">
            <h1>Personal Info</h1>

            <?php $form = $this->beginWidget('CActiveForm', array(
                'id'=>'start-program-form',
                'enableAjaxValidation'=>true,
                'action' => '/account/'.Yii::app()->user->id.'/editprofile',
                'htmlOptions'=>array(
                    'class' => 'start-program-form',
                ),
            )); ?>

                <div class="inputs">
                    
                    <?= $form->textField( Yii::app()->user->profile, 'birthday', array(
                        'class' => 'form-control datepicker',
                        'placeholder' => 'Date of birth',
                    ));?>

                    <?= $form->textField( Yii::app()->user->profile, 'weight', array(
                        'class' => 'form-control', 'placeholder' => 'Weight (lb.)',
                    ));?>

                    <div class="user-height form-group">
                        <label for="height">Height (feet, e.g. "5.10"): </label>
                        <div class="select-group">
                            <?php
                                if (!is_array(Yii::app()->user->profile->height)) {
                                    if (!empty(Yii::app()->user->profile->height))
                                        $user_height = explode('.', Yii::app()->user->profile->height);
                                    else
                                        $user_height = explode('.', '4.0');
                                } else {
									$user_height = Yii::app()->user->profile->height;
                                }
                            ?>
                            <span class="select-wrap">
                                <?php
                                    echo $form->dropDownList(Yii::app()->user->profile, 'height[0]',
                                        array_combine(range(4,6), range(4,6)),
                                        array('options' => array($user_height[0] => array('selected'=>true))));
                                ?>
                            </span>
                            <span class="select-wrap">
                                <?php
                                    echo $form->dropDownList(Yii::app()->user->profile, 'height[1]', array_combine(range(0,11), range(0,11)),
                                        array('options' => array($user_height[1] => array('selected'=>true))));
                                ?>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fitness_level">Fitness level: </label>
                        <span class="select-wrap">
                             <?= $form->dropDownList( Yii::app()->user->profile, 'fitness_level', Profile::getFitnessLevels()); ?>
                        </span>
                    </div>

                    <span class="select-wrap">
                         <?= $form->dropDownList( Yii::app()->user->profile, 'gender', Profile::getGenders()); ?>
                    </span>

                    <div class="blue-dot-btn sign-in-btn">
                        <a href="#">
                            Start The Program
                            <div class="border tl"></div>
                            <div class="border tr"></div>
                            <div class="border br"></div>
                            <div class="border bl"></div>
                        </a>
                    </div>
                </div>

            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>