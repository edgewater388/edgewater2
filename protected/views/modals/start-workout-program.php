<div class="modal" id="start-workout-program" data-auto-open-modal="true">
	<div class="lockout congrats-modal">
		<div class="close-btn close-modal"></div>
		<div class="inner">
			<div class="content">
				<div class="side-lines"><h4>WORKOUT PROGRAMS</h4></div>
				<h2>Congratulations on starting <span>the workout program.</span></h2>
				<em>We know staying committed to the program may be hard so why no get help from your friends.</em>
				<p>Click the link below and we will share with your friends both when you do, and when do not do the scheduled workout.</p>
				<div class="separate"></div>
				<span class="subtitle">And to do our part in helping you stay motivated, by clicking the link below, <strong>we will give you $10 off your next month.</strong></span>
				<a href="#" class="btn blue">click here</a>
			</div>
		</div>
	</div>
</div>