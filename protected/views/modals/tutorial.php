<div class="modal" id="tutorial">
    <div class="lockout">
        <div class="close-btn close-modal"></div>
        <div class="inner tutorial">
            <div class="tutorial-header">
            <h1>get started with mashups!</h1>
            <p>Congratulations on your decision to strengthen and better your body.
                You’re on your way to a leaner, more sculpted figure.
                We’ll now take you through the three easy steps to creating mashups
                that will give you the results you want.</p>
            </div>
            <ul class="steps">
                <li class="step1">
                    <h4>Select your favorite exercises</h4>
                    <p>Browse workouts and choose targeted exercises based on your goals.</p>
                    <img data-mobile="/themes/basic/images/tutorial/step1-mobile.jpg" data-desktop="/themes/basic/images/tutorial/step1.jpg"/>
                </li>
                <li class="step2">
                    <h4>Organize workouts into playlists</h4>
                    <p>Structure your video playlist based on your personal style and workout approach.</p>
                    <img src="/themes/basic/images/tutorial/step2.png"/>
                </li>
                <li class="step3">
                    <h4>Combine short workouts into a mashup</h4>
                    <p>Link together your favorite exercises to create a workout that makes the most of your time.</p>
                    <img src="/themes/basic/images/tutorial/step3.jpg"/>
                </li>
            </ul>
            <ul class="nav">
                <li class="dot active"></li>
                <li class="dot"></li>
                <li class="dot"></li>
            </ul>
            <div class="buttons">
                <div class="btn blue next-step-btn">
                    <div class="next-text">next step</div>
                    <div class="finish-text">finish</div>
                </div>
                <div class="skip-for-now close-modal">Skip for now</div>
            </div>


        </div>
    </div>
</div>