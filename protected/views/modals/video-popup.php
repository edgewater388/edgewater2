<div class="modal modal-video <?php echo !Yii::app()->user->identity->is_see_video ? 'first-video-popup' : '' ?>" id="video">
    <div class="lockout">
		<?php if (!Yii::app()->user->isGuest && !Yii::app()->user->identity->is_see_video): ?>
			<div class="overlay" style="opacity: 1; visibility: visible;">
                <div class="social-text">
                    <p>We know staying focused can be hard, so why not get a little help from your friends. After each video let your friends know what you did so they can keep you motivate.</p>
                    <div class="arrow right"></div>
                </div>
                <div class="product-text">
                    <div class="arrow left"></div>
                    <p>Like what you see in the videos, you can buy it here at a discount for being a member!</p>
                    <div class="arrow right"></div>
                </div>
            </div>
		<?php endif; ?>
        <div class="close-btn close-modal"></div>
        <h1 class="video-title">TOTAL BODY - FOCUS: ARMS AND ABS</h1>
        <div class="modal-row modal-row--video">
            <div class="inner video">
                <div class="player bc-player"></div>
            </div>
            <div class="modal-video__description">
                <div class="video-instructor description-item"></div>
                <div class="description-item">
                    <h3>ABOUT THE WORKOUT</h3>
                    <dl class="definition-list">
                        <dt>Focus:</dt>
                        <dd class="video-body-part">Total Body</dd>
                        <dt>Time:</dt>
                        <dd><span class="video-duration">40
							<!--</span> Minutes</dd>-->
                        <dt>Props:</dt>
                        <dd class="video-equipment">Chair, Weights, Resistance Band</dd>
                    </dl>
                </div>
<!--				                <div class="description-item">
                    <h3>SHARE THIS WORKOUT</h3>
                    <ul class="socials">
                        <li><a href="#" target="_blank" rel="noopener noreferrer"><img src="../../../themes/basic/images/modal-video/ico-facebook.png" alt="facebook" /></a></li>
                        <li><a href="#" target="_blank" rel="noopener noreferrer"><img src="../../../themes/basic/images/modal-video/ico-twitter.png" alt="facebook" /></a></li>
                        <li><a href="#" target="_blank" rel="noopener noreferrer"><img src="../../../themes/basic/images/modal-video/ico-instagram.png" alt="facebook" /></a></li>
                    </ul>
                </div>-->
                <div class="description-item">
                    <h3>SHARE THIS WORKOUT</h3>
                    <?php $this->widget('application.widgets.socials.Socials', ['disabled' => true]); ?>
                </div>
            </div>
        </div>
        <div class="modal-row recommended-items-title">
           
        </div>
        <div class="modal-row">
            <ul class="recommended-items"></ul>
        </div>
        <div class="modal-row">
            <a href="https://physique57.com/shop/all-products/" class="recom-link" target="_blank" rel="noopener noreferrer">
                See all items
            </a>
        </div>
    </div>
</div>
