<div class="modal" id="video">
    <div class="lockout">
        <div class="close-btn close-modal"></div>
        <div class="inner video">
            <h1 class="video-title"></h1>
            <div class="player bc-player"></div>
            <div class="bottom">
                <div class="description">
                    <p></p>
                </div>
                <div class="equipment-required">
                    <h4>Get Optional Equipment</h4>
                    <ul class="equipment-thumbs"></ul>
                </div>
            </div>
        </div>
    </div>
</div>