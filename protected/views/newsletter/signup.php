<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'page_header')); ?>
	<div class="page-header page-header-about">
		<div class="container text-center">
			<h1>Newsletter</h1>
			<h2>Signup</h2>
		</div>
	</div>
<?php $this->endWidget();?>

<?php $this->renderPartial('//blocks/newsletter', array('model'=>$model)); ?>

<div class="divider divider-large"></div>