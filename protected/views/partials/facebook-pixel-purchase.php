<!-- Start Facebook pixel tracking - purchase -->
<?php $total = (Yii::app()->user->membership) ? Yii::app()->user->membership->total : 0; ?>
<script>
fbq('track', 'Purchase', {value: '<?php echo $total; ?>', currency:'USD'});
</script>
<!-- End Facebook pixel tracking - purchase -->
