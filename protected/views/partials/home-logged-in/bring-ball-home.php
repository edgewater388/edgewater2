<section id="bring-ball-home">
    <div class="lockout">

        <header>
            <div class="lockout">
                <div class="side-lines white">
                    <h4>BRING</h4>
                </div>
                <h1>PHYSIQUE 57 HOME</h1>
            </div>
        </header>
        <div class="ball">
            <img src="/themes/basic/images/home-logged-in/ball.jpg" alt="">
        </div>
        <div class="blue-dot-btn">
            <a href="http://physique57.com/shop/all-products/apparel/">
                SHOP NOW
                <div class="border tl"></div>
                <div class="border tr"></div>
                <div class="border br"></div>
                <div class="border bl"></div>
            </a>
        </div>
    </div>

</section>