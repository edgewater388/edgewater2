<section id="hero" class="hero-with-player">
    <div class="centered-text"
         style="<?php if ( $stream && $stream->isPlayerVisible ): ?>display: none; <?php endif ?>">
        <div class="inner">
            <h4 class="top-text">YOUR PHYSIQUE 57 ON DEMAND</h4>
            <h1 class="title">MEET THE EPIC YOU</h1>
            <h5 class="subtitle">It never gets easier, you just get better</h5>
            <div class="blue-dot-btn">
                <a href="/my-workouts">
                    MY WORKOUTS
                    <div class="border tl"></div>
                    <div class="border tr"></div>
                    <div class="border br"></div>
                    <div class="border bl"></div>
                </a>
            </div>
            <div class="scroll-cta">
                <div class="dot one animated"></div>
                <div class="dot two animated"></div>
                <div class="dot three animated"></div>
                <div class="arrow four animated"></div>
            </div>
        </div>
    </div>
	<?php if ($stream): $hidden = ''; ?>
		<?php if (($data = $stream->embed_js) != null) {
			$plaeyerId = "stream-player";
			$hidden = 'hidden';
		} elseif (($data = $stream->livestream_iframe) != null) {
			$plaeyerId = "iframe-stream-player";
			Yii::app()->getClientScript()->registerScript($this->getId(), "
				(function ($) {
					var fullScreenBtn = $('[data-role=\"player-fullscreen\"]');
					fullScreenBtn.click(function () {
						var iframe = document.querySelector('".$plaeyerId." iframe');
						iframe.src = iframe.src.replace(/mute=true/, 'mute=false');
						fullScreenBtn.remove();
					});
				})(jQuery);
			", CClientScript::POS_END);
		}
		?>
		<?php if (!empty($plaeyerId)) : ?>
			<div id="<?=$plaeyerId?>" class="stream-player"
				 data-enabled="<?php echo $stream->status; ?>"
				 data-countdown-message="<?php echo htmlentities( $stream->countdown_message ); ?>"
				 data-ready-to-start-message="<?php echo htmlentities( $stream->ready_message ); ?>"
				 data-start-live-at="<?php echo strtotime( $stream->time_start ); ?>"
				 data-start-video-in="<?php echo $stream->timer; ?>"
				 data-end-live-at="<?php echo strtotime( $stream->time_end ); ?>"
				 data-update-url="<?php echo htmlentities( $stream_update_url ); ?>"
				 data-hide-content="#hero > .centered-text"
				 data-countdown-background="<?php echo $stream->countdown_background ?>"
				 data-ready-to-start-background="<?php echo $stream->ready_background ?>"
			>
				<div class="player-content player-holder <?=$hidden?>">
					<?php echo $data; ?>
					<div class="player-fullscreen-btn" data-role="player-fullscreen">
						<span>
							Watch Live
						</span>
					</div>
				</div>
				<div class="player-content message-holder hidden"
					 style="
							 background-image: url('<?php echo $stream->isReadyMessageVisible ? $stream->ready_background : $stream->countdown_background; ?>')
							 ">
					<div class="message-wrapper"></div>
				</div>
			</div>
		<?php endif; ?>
	<?php endif; ?>
</section>