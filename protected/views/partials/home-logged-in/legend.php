<section id="legend">
    <div class="full-width full-width-1185">
        <h5>LEGEND:</h5>
        <ul>
            <li class="strength">
                <div class="icon strength one"></div>
                <div class="type">strength</div>
            </li>
            <li>
                <div class="icon cardio one"></div>
                <div class="type">cardio</div>
            </li>
            <li class="restorative">
                <div class="icon restorative one"></div>
                <div class="type">restorative</div>
            </li>
        </ul>
    </div>
</section>