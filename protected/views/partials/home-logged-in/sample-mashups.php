<section id="sample-mashups">
    <div class="full-width full-width-1185">
        <header class="mashups-header">
            <div class="side-lines right-line-only">
                <h4>New Videos / Mashups <a href="/mashup-maker" class="btn blue">create a mashup</a></h4>
            </div>
        </header>

        <ul class="video-thumbs" data-match-tallest="li">
			<?php foreach ($playlists as $playlist): ?>
                <li class="initial-video-item">
                    <div class="thumb" data-modal="#mashup" data-playlist="<?php echo $playlist->id; ?>">
                        <img src="<?php echo $playlist->thumbUrl; ?>">
						<?php if($playlist->getIsNew()) : ?>
							<div class="new-video"><span>New</span></div>
						<?php endif; ?>
                        <div class="title-icon">
                            <div class="title"><?php echo $playlist->title; ?></div>
                        </div>
                        <div class="favorite-playlist" title="Save to My Workouts">
                            <div class="icon"></div>
                            <div class="checkmark">
                                <img src="/themes/basic/images/global/icon-checkmark.png">
                            </div>
                        </div>
                    </div>
                </li>
			<?php endforeach; ?>
			<?php foreach ($new_videos as $video): ?>
				<li class="initial-video-item">
					<div class="thumb" data-modal="#video" data-video="<?= $video->id; ?>">
						<img src="<?= $video->thumbUrl; ?>">
						<?php if($video->is_new) : ?>
							<div class="new-video"><span>New</span></div>
						<?php endif; ?>
						<div class="icon time"><?= $video->minutes; ?> min</div>
						<div class="title-icon">
							<div class="title"><?= $video->title; ?></div>
							<?php if ($video->strength): ?>
								<div class="icon strength white <?= Video::getLevelWord($video->strength); ?>"
									 title="strength"></div>
							<?php endif; ?>
							<?php if ($video->cardio): ?>
								<div class="icon cardio white <?= Video::getLevelWord($video->cardio); ?>"
									 title="cardio"></div>
							<?php endif; ?>
							<?php if ($video->restorative): ?>
								<div class="icon restorative white <?= Video::getLevelWord($video->restorative); ?>"
									 title="restorative"></div>
							<?php endif; ?>
						</div>
						<div class="add-favorite" title="Add to favorites">
							<div class="icon"></div>
							<div class="checkmark">
								<img src="/themes/basic/images/global/icon-checkmark.png">
							</div>
						</div>
					</div>
				</li>
			<?php endforeach; ?>
        </ul>

        <a href="/mashup-maker" class="btn blue mobile">create a mashup</a>
    </div>
</section>
