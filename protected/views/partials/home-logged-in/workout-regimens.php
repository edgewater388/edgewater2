<section id="workout-regimens">
    <div class="full-width full-width-1185">
        <header class="workouts-header">
            <div class="side-lines right-line-only">
                <h4>workout programs</h4>
            </div>
        </header>
        <ul class="columns-3">
			<?php foreach ($regimens as $k => $regimen): ?>
                <li>
					<?php if ($regimen->imageF): ?>
                        <img class="bg-image" src="<?= $regimen->imageF->thumb('regimen'); ?>">
					<?php endif; ?>
                    <div class="text-overlay">
                        <div class="centered centered--top-left">
                            <div class="inner">
								<?php if (UserRegimen::checkProgramExpired(Yii::app()->user->id, $regimen->id)): ?>
                                    <h5>Expired</h5>
								<?php else: ?>
									<?php if (UserRegimen::checkUserSubscribed(Yii::app()->user->id, $regimen->id)): ?>
                                        <h5>In progress,
                                            day: <?= UserRegimenPlan::getCurrentWorkoutDay(UserRegimen::getU2R_ID(Yii::app()->user->id, $regimen->id)); ?></h5>
									<?php else: ?>
                                        <h5><?= $regimen->length; ?></h5>
									<?php endif; ?>
								<?php endif; ?>
                                <h1><?= $regimen->title; ?></h1>
                            </div>
                        </div>
                    </div>
                    <a href="/workout-program/<?= $regimen->id; ?>">
                        <div class="hover-overlay">
                            <div class="centered">
                                <div class="inner">
                                    <p><?= $regimen->summary; ?></p>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
			<?php endforeach; ?>
        </ul>

		<?php if (Regimen::getRegimenCount() > 6): ?>
            <div id="see-more-regimens">
                <div class="blue-dot-btn gray large">
                    <a href="#">
                        SEE MORE
                        <div class="border tl"></div>
                        <div class="border tr"></div>
                        <div class="border br"></div>
                        <div class="border bl"></div>
                    </a>
                </div>
            </div>
		<?php endif; ?>
    </div>
</section>
