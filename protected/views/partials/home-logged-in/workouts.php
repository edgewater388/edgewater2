<section id="workouts">
    <div class="full-width full-width-1185 video-pagination">
        <header class="workouts-header">
            <div class="side-lines right-line-only">
                <h4>workouts</h4>
            </div>
        </header>
        <section class="workouts-section">
            <header class="workouts-section__header">
                <h5>Live Classes</h5>
                <div class="filter-wr">
                    <span class="see-more-regimens">
                        <span class="blue-dot-btn gray large">
                            <a href="#">
                                SHOW ALL
                                <div class="border tl"></div>
                                <div class="border tr"></div>
                                <div class="border br"></div>
                                <div class="border bl"></div>
                            </a>
                        </span>
                    </span>
                    <span class="select-holder">
                        <?= CHtml::dropDownList('instructor_id', null, ['' => 'Trainer'] + Users::getInstructors()); ?>
                    </span>
                    <span class="select-holder">
                        <?= CHtml::dropDownList('type', null, ['' => 'Type'] + Video::getTypes()); ?>
                    </span>
                    <span class="select-holder">
                        <?= CHtml::dropDownList('level', null, ['' => 'Level'] + Video::getLevels()); ?>
                    </span>
                    <span class="select-holder">
                        <?= CHtml::dropDownList('body_part', null, ['' => 'Body Part'] + Video::getBodyParts()); ?>
                    </span>
                    <span class="select-holder">
                        <?= CHtml::dropDownList('equipment', null, ['' => 'Equipment'] + Video::getEquipment()); ?>
                    </span>
                </div>
            </header>
            <ul class="video-thumbs" data-match-tallest="li">
				<?php foreach ($live_classes as $video): ?>
                    <li class="initial-video-item">

                        <div class="thumb" data-modal="#video" data-video="<?= $video->id; ?>">
                            <img src="<?= $video->thumbUrl; ?>">
							<?php if($video->is_new) : ?>
								<div class="new-video"><span>New</span></div>
							<?php endif; ?>
                            <div class="icon time">
                                <?= $video->minutes; ?> min
                            </div>
                            
                            <div class="title-icon">
                                <div class="title"><?= $video->title; ?></div>
								<?php if ($video->strength): ?>
                                    <div class="icon strength white <?= Video::getLevelWord($video->strength); ?>"
                                         title="strength"></div>
								<?php endif; ?>
								<?php if ($video->cardio): ?>
                                    <div class="icon cardio white <?= Video::getLevelWord($video->cardio); ?>"
                                         title="cardio"></div>
								<?php endif; ?>
								<?php if ($video->restorative): ?>
                                    <div class="icon restorative white <?= Video::getLevelWord($video->restorative); ?>"
                                         title="restorative"></div>
								<?php endif; ?>
                            </div>
                            <div class="add-favorite" title="Add to favorites">
                                <div class="icon"></div>
                                <div class="checkmark">
                                    <img src="/themes/basic/images/global/icon-checkmark.png">
                                </div>
                            </div>
                        </div>
                    </li>
				<?php endforeach; ?>
            </ul>
        </section>
		<?php foreach ($videos_by_duration as $duration => $videos): ?>
            <section class="workouts-section" data-duration="<?= $duration; ?>">
                <header class="workouts-section__header">
                    <h5 class="ico-clock"><?= $duration; ?> <span>min</span></h5>
                    <div class="filter-wr">
                        <span class="see-more-regimens">
                            <span class="blue-dot-btn gray large">
                                <a href="#">
                                    SHOW ALL
                                    <div class="border tl"></div>
                                    <div class="border tr"></div>
                                    <div class="border br"></div>
                                    <div class="border bl"></div>
                                </a>
                            </span>
                        </span>
                        <span class="select-holder">
                            <?= CHtml::dropDownList('instructor_id', null, ['' => 'Trainer'] + Users::getInstructors()); ?>
                        </span>
                        <span class="select-holder">
                            <?= CHtml::dropDownList('type', null, ['' => 'Type'] + Video::getTypes()); ?>
                        </span>
                        <span class="select-holder">
                            <?= CHtml::dropDownList('level', null, ['' => 'Level'] + Video::getLevels()); ?>
                        </span>
                        <span class="select-holder">
                            <?= CHtml::dropDownList('body_part', null, ['' => 'Body Part'] + Video::getBodyParts()); ?>
                        </span>
                        <span class="select-holder">
                            <?= CHtml::dropDownList('equipment', null, ['' => 'Equipment'] + Video::getEquipment()); ?>
                        </span>
                    </div>
                </header>
                <script type="text/template" id="slider-video-item-template">
                    <li style="height: 201px;">
                        <div class="thumb" data-modal="#video" data-video="{{id}}">
                            <img src="{{imgUrl}}">
                            <div class="icon time">{{duration}}</div>
                            <div class="title-icon">
                                <div class="title">{{title}}</div>
                            </div>
                            <div class="add-favorite" title="Add to favorites">
                                <div class="icon"></div>
                                <div class="checkmark">
                                    <img src="/themes/basic/images/global/icon-checkmark.png">
                                </div>
                            </div>
                        </div>
                    </li>
                </script>
                <script type="text/template" id="video-item-empty">
                    <div class="stub-video" style="height: 50px; width: 100%;">
                        We couldn't find any videos matching your selection, but we're always open to suggestions.
                        <a href="/contact-us">Please Contact Us.</a>
                    </div>
                </script>
                <script type="text/template" id="loading-video">
                    <div class="loading-splash">Loading ...</div>
                </script>
                <ul class="video-thumbs" data-match-tallest="li">
					<?php foreach ($videos as $video): ?>
                        <li class="initial-video-item">
                            <div class="thumb" data-modal="#video" data-video="<?= $video->id; ?>">
                                <img src="<?= $video->thumbUrl; ?>">
                                <div class="icon time"><?= $video->minutes; ?> min</div>
                                <div class="title-icon">
                                    <div class="title"><?= $video->title; ?></div>
									<?php if ($video->strength): ?>
                                        <div class="icon strength white <?= Video::getLevelWord($video->strength); ?>"
                                             title="strength"></div>
									<?php endif; ?>
									<?php if ($video->cardio): ?>
                                        <div class="icon cardio white <?= Video::getLevelWord($video->cardio); ?>"
                                             title="cardio"></div>
									<?php endif; ?>
									<?php if ($video->restorative): ?>
                                        <div class="icon restorative white <?= Video::getLevelWord($video->restorative); ?>"
                                             title="restorative"></div>
									<?php endif; ?>
                                </div>
                                <div class="add-favorite" title="Add to favorites">
                                    <div class="icon"></div>
                                    <div class="checkmark">
                                        <img src="/themes/basic/images/global/icon-checkmark.png">
                                    </div>
                                </div>
                            </div>
                        </li>

					<?php endforeach; ?>
                </ul>
            </section>
		<?php endforeach; ?>
    </div>
</section>
