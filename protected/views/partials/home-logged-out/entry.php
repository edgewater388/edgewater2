<section class="entry-section">
	<div class="img-holder">
		<div class="img-frame">
			<img src="/themes/basic/images/home-logged-out/entry-bg.jpg" alt="image description" width="1440" height="400">
		</div>
		<div class="triangle">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" preserveAspectRatio="none">
				<polygon fill='#ffffff' points="0,100 100,0 100,100"/>
			</svg>
		</div>
	</div>
	<div class="container">
		<div class="text-holder">
			<h1>STRENGTHEN, LENGTHEN & TONE</h1>
			<div class="divider-color"></div>
			<p class="subtitle">See visible results in 8 workouts with:</p>
			<p><span class="color">No equipment required workouts</span> 80+ Videos & 6 Workout Programs <br>10, 15, 30, 45 and 57 – minute videos <br>Strength Training, Cardio, & Restorative Workouts</p>
		</div>
	</div>
</section>