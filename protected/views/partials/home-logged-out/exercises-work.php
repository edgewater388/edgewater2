<section class="exercises-work">
	<div class="img-holder">
		<div class="img-frame">
			<img src="themes/basic/images/home-logged-out/work-01.jpg" alt="image description" width="1440" height="701">
		</div>
	</div>
	<div class="text-holder">
		<div class="headline">
			<div class="container">
				<h1>Why Our <br>Exercises work.</h1>
			</div>
		</div>
		<div class="visual">
			<div class="container">
				<div class="img-circle">
					<img src="themes/basic/images/home-logged-out/work-02.jpg" alt="image description" width="352" height="352">
				</div>
				<div class="img-circle">
					<img src="themes/basic/images/home-logged-out/work-04.jpg" alt="image description" width="352" height="352">
				</div>
			</div>
		</div>
		<div class="text">
			<div class="container">
				<div class="text-frame">
					<p>Our specially designed program is 57 minutes of pure efficiency, where we fuse high intensity cardio blasts with head-to-toe body sculpting, muscle toning and core building that’s always challenging and never boring. We designed a method specifically for women that activates even the tiniest of muscles, delivering a stronger, leaner, chiseled-out physique and empowered confidence that lasts all day. Trust us, it’s addictive.</p>
					<a href="#" class="btn blue"><i class="icon-arrow-dotted"></i><span>SIGN UP FOR FREE TRIAL</span></a>
					<div class="triangle">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" preserveAspectRatio="none">
							<polygon fill='#000000' fill-opacity="0.4" points="0,0 100,0 0,100"/>
						</svg>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>