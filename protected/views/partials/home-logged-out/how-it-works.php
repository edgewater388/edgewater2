<section class="how-it-works">
	<div class="img-holder">
		<div class="img-frame">
			<img src="/themes/basic/images/home-logged-out/how-it-works-01.jpg" alt="image description" width="1007" height="623">
		</div>
	</div>
	<div class="text-holder">
		<div class="container">
			<div class="box">
				<div class="triangle">
					<svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" preserveAspectRatio="none">
						<polygon fill='#ffffff' points="0,0 100,0 0,100"/>
					</svg>
				</div>
				<div class="headline">
					<h1>HOW IT WORKS</h1>
					<div class="triangle right">
						<svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" preserveAspectRatio="none">
							<polygon fill='#535658' points="0,0 100,0 0,100"/>
						</svg>
					</div>
					<div class="triangle bottom">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" preserveAspectRatio="none">
							<polygon fill='#535658' points="0,0 100,0 100,100"/>
						</svg>
					</div>
				</div>
				<ol>
					<li>We combine strength training and cardio to give you rapid results</li>
					<li>Chisel your muscles and transform your entire body with our proven interval overload method</li>
					<li>Innovative and exciting choreography keeps you challenged and engaged at every level</li>
				</ol>
			</div>
			<div class="btn-holder">
				<a href="<?= $this->createUrl('customer/create'); ?>" class="btn blue">SIGN UP FOR FREE TRIAL</a>
			</div>
		</div>
	</div>
</section>