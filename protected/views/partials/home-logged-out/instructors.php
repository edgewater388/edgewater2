<div class="instructors-section">
	<div class="headline">
		<div class="container">
			<div class="frame">
				<h1>OUR instructors</h1>
				<p>Our vetting and development program is the most rigorous in the industry, and it shows.</p>
				<div class="divider"></div>
				<div class="triangle right">
					<svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" preserveAspectRatio="none">
						<polygon fill='#535658' points="0,0 100,0 1,100"/>
					</svg>
				</div>
				<div class="triangle bottom">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" preserveAspectRatio="none">
						<polygon fill='#2d3133' points="0,-1 100,-1 100,100"/>
					</svg>
				</div>
			</div>
		</div>
	</div>
	<div class="slideshow">
		<div class="slideset">
			<div class="slide">
				<div class="slide-frame">
					<div class="text-holder">
						<div class="text-frame">
							<div class="container">
								<h2 class="name"><span>Shannon</span> Smith</h2>
								<div class="box">
									<strong>“If you change nothing, nothing will change. Make one simple change”</strong>
									<ul>
										<li>Certifications: NASM Personal Trainer, BFA in Dance</li>
										<li>Dance Background:  Mamma Mia, Cats, Legally Blond….</li>
										<li>Style: Shannon brings good old Midwestern positivity to the barre yet gives you a challenging workout where you will find yourself quivering at your edge.</li>
									</ul>
									<div class="btn-holder">
										<a href="#" class="btn blue">READ MORE</a>
									</div>
									<div class="triangle">
										<svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" preserveAspectRatio="none">
											<polygon fill='#ffffff' points="0,0 100,0 0,100"/>
										</svg>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="img-holder">
					<div class="img-frame">
						<img src="themes/basic/images/home-logged-out/instructor-01.jpg" alt="image description" width="1115" height="840">
					</div>
				</div>
			</div>
			<div class="slide">
				<div class="slide-frame">
					<div class="text-holder">
						<div class="text-frame">
							<div class="container">
								<h2 class="name"><span>Julie</span> Cobble</h2>
								<div class="box">
									<strong>“Strive for progress, not perfection.”</strong>
									<ul>
										<li>Certifications: NASM Personal Trainer, 200-hour Yoga Alliance Certification</li>
										<li>Dance Background: Washington School of Ballet, Roxey Ballet, Amy Marshall Dance Company</li>
										<li>Style: Julie brings an energetic and slightly sarcastic style to the barre, holding you accountable for all your hard work but always with a smile!</li>
									</ul>
									<div class="btn-holder">
										<a href="#" class="btn blue">READ MORE</a>
									</div>
									<div class="triangle">
										<svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" preserveAspectRatio="none">
											<polygon fill='#ffffff' points="0,0 100,0 0,100"/>
										</svg>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="img-holder">
					<div class="img-frame">
						<img src="themes/basic/images/home-logged-out/instructor-02.jpg" alt="image description" width="1115" height="840">
					</div>
				</div>
			</div>
			<div class="slide">
				<div class="slide-frame">
					<div class="text-holder">
						<div class="text-frame">
							<div class="container">
								<h2 class="name"><span>Holly</span> Hendricks</h2>
								<div class="box">
									<strong>“You can do anything you put your mind to if you just breathe and believe- you've got this!”</strong>
									<ul>
										<li>Certifications: NASM Personal Trainer, AFAA Group certified, BFA in Dance</li>
										<li>Dance Background: Modern dancer in NYC- Vissi Dance Theater, Amy Marshall Dance Company</li>
										<li>Style: Holly brings you motivation and friendly positivity, but don't be fooled by that big smile; she'll keep you on your toes and get you feeling the burn and conquering your fitness goals in no time!</li>
									</ul>
									<div class="btn-holder">
										<a href="#" class="btn blue">READ MORE</a>
									</div>
									<div class="triangle">
										<svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" preserveAspectRatio="none">
											<polygon fill='#ffffff' points="0,0 100,0 0,100"/>
										</svg>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="img-holder">
					<div class="img-frame">
						<img src="themes/basic/images/home-logged-out/instructor-03.jpg" alt="image description" width="1115" height="840">
					</div>
				</div>
			</div>
			<div class="slide">
				<div class="slide-frame">
					<div class="text-holder">
						<div class="text-frame">
							<div class="container">
								<h2 class="name"><span>Tanya</span> Becker</h2>
								<div class="box">
									<strong>“Don’t quit before the change occurs”</strong>
									<ul>
										<li>Certifications: ACE Personal Trainer</li>
										<li>Dance Background: Performed all over the world, Dr. OZ, Martha Stewart, Good Morning America….</li>
										<li>Style: Tanya give a challenging workout while keeping things light and fun using her sense of humor.</li>
									</ul>
									<div class="btn-holder">
										<a href="#" class="btn blue">READ MORE</a>
									</div>
									<div class="triangle">
										<svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" preserveAspectRatio="none">
											<polygon fill='#ffffff' points="0,0 100,0 0,100"/>
										</svg>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="img-holder">
					<div class="img-frame">
						<img src="themes/basic/images/home-logged-out/instructor-04.jpg" alt="image description" width="1115" height="840">
					</div>
				</div>
			</div>
		</div>
		<a class="btn-prev" href="#"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
		<a class="btn-next" href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
	</div>
</div>