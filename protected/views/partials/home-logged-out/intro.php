<section class="join-section">
	<div class="container">
		<div class="join-box">
			<h1>Get the <br>studio workout <br><strong>at home.</strong></h1>
			<a href="<?= $this->createUrl('customer/create'); ?>" class="btn blue">START A FREE MONTH TRIAL</a>
			<ul>
				<li>$19.95/month After Free Trial</li>
				<li>Cancel Anytime</li>
			</ul>
			<span class="link">Already have an account? <a href="<?= $this->createUrl('user/users/login'); ?>">Sign In</a></span>
		</div>
	</div>
</section>