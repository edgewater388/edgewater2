<section class="magazine-section">
	<div class="container">
		<div class="col-holder">
			<div class="col">
				<div class="img-holder">
					<img src="/themes/basic/images/home-logged-out/magazine-01.jpg" alt="image description" width="151" height="209">
				</div>
				<blockquote>
					<q>“It tones my butt, thighs, and abs like no other workout.”</q>
				</blockquote>
			</div>
			<div class="col">
				<div class="img-holder">
					<img src="/themes/basic/images/home-logged-out/magazine-02.jpg" alt="image description" width="151" height="209">
				</div>
				<blockquote>
					<q>“This fun workout changed my life.”</q>
				</blockquote>
			</div>
			<div class="col">
				<div class="img-holder">
					<img src="/themes/basic/images/home-logged-out/magazine-03.jpg" alt="image description" width="151" height="209">
				</div>
				<blockquote>
					<q>“My thighs are more toned, my butt's lifted, and now I can say my saddlebags are a non-issue.”</q>
				</blockquote>
			</div>
		</div>
	</div>
</section>