<div class="mashups-section">
	<div class="img-holder">
		<div class="img-frame">
			<img src="themes/basic/images/home-logged-out/mashups-01.jpg" alt="image description" width="1439" height="733">
		</div>
		<div class="headline">
			<div class="container">
				<h1>CREATE CUSTOM MASHUPs</h1>
				<p>Arrange short workouts into custom full-length <br>workouts and save your favorites.</p>
				<div class="divider-color"></div>
			</div>
		</div>
	</div>
	<div class="container">
		<ol class="list">
			<li>
				<strong class="title"><span>Select your</span> Favorite Worksout</strong>
				<div class="frame">
					<div class="img">
						<img src="themes/basic/images/home-logged-out/create-thumb-01.jpg" alt="image description" width="305" height="155">
					</div>
					<p>Browse our worksouts and choose targeted exercises based on your goals.</p>
				</div>
			</li>
			<li>
				<strong class="title"><span>Organize workouts</span> Into Playlist</strong>
				<div class="frame">
					<div class="img">
						<img src="themes/basic/images/home-logged-out/create-thumb-02.jpg" alt="image description" width="254" height="192">
					</div>
					<p>Structure your video playlist based on your personal style, daily schedule, and workout preferences.</p>
				</div>
			</li>
			<li>
				<strong class="title"><span>Combine short workouts</span> into one Full Workout</strong>
				<div class="frame">
					<div class="img">
						<img src="themes/basic/images/home-logged-out/create-thumb-03.jpg" alt="image description" width="305" height="224">
					</div>
					<p>Link together your favorite exercises to create a workout that makes the most out of your time.</p>
				</div>
			</li>
		</ol>
	</div>
</div>