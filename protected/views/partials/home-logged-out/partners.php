<section class="partners-section">
    <div class="container">
        <ul>
            <li>
                <a href="http://www.vogue.in/content/new-yorks-latest-export-to-india-is-a-killer-fat-zapping-workout"
                   target="_blank">
                    <img src="/themes/basic/images/home-logged-out/vogue-logo.png" alt="image description" width="90"
                         height="24">
                </a>
            </li>
            <li>
                <a href="http://www.shape.com/celebrities/interviews/10-shameless-reasons-we-love-emmy-rossum"
                   target="_blank">
                    <img src="/themes/basic/images/home-logged-out/shape-logo.png" alt="image description" width="93"
                         height="24">
                </a>
            </li>
            <li>
                <a href="https://physique57.com/wp-content/uploads/2016/06/Cosmo_June2016_ArticleFinal.pdf"
                   target="_blank">
                    <img src="/themes/basic/images/home-logged-out/cosmopolitan-logo.png" alt="image description"
                         width="126" height="31">
                </a>
            </li>
            <li>
                <a href="https://www.popsugar.com/fitness/What-Physique-57-42815544" target="_blank">
                    <img src="/themes/basic/images/home-logged-out/popsugar-logo.png" alt="image description"
                         width="168" height="53">
                </a>
            </li>
            <li>
                <a href="http://www.self.com/story/the-barre-move-thatll-set-your-butt-and-legs-on-fire"
                   target="_blank">
                    <img src="/themes/basic/images/home-logged-out/self-logo.png" alt="image description"
                         width="97" height="24">
                </a>
            </li>
            <li>
                <a href="http://www.harpersbazaar.com/beauty/diet-fitness/a10666/bazaar-editors-favorite-trainers"
                   target="_blank">
                    <img src="/themes/basic/images/home-logged-out/bazaar-logo.png" alt="image description"
                         width="109" height="28">
                </a>
            </li>
        </ul>
    </div>
</section>