<section class="programs-section">
	<div class="container">
		<!-- <h1>ON DEMAND LIBRARY</h1>
		<p class="subtitle">24/7 UNLIMITED WORKOUTS. 40+ VIDEOS. <br>10,15,30 AND 57-MINUTE VIDEOS <br>STRENGHT TRAINING, CARDIO & RESTORATIVE WORKOUTS. <br>WORKOUTS WITH NO EQUIPMENT REQUIRED.</p> -->
		<div class="program-holder">
			<div class="program">
				<div class="img-holder">
					<a href="#" data-modal="#program-01">
						<i class="icon-plus"></i>
						<img src="https://ondemand.physique57.com/files/thumb/regimen/56afaf1b354b6_99271.jpg" alt="image description" width="792" height="392">
					</a>
				</div>
				<strong class="duration">Lose 10 in 10</strong>
				<div class="text-holder">
					<p>This 10 week program is so much more than just another weight lose regimen. its about transformation, its about achieving the best version of you, and creating healthy habits.</p>
				</div>
			</div>
			<div class="program">
				<div class="img-holder">
					<a href="#" data-modal="#program-02">
						<i class="icon-plus"></i>
						<img src="https://ondemand.physique57.com/files/thumb/regimen/57508e983b436_83748.jpg" alt="image description" width="792" height="392">
					</a>
				</div>
				<strong class="duration">Best Beach Body</strong>
				<div class="text-holder">
					<p>This 4 week program is specifically designed to help our viewers feel confident, sexy, and strong. Exercises focus on areas specific to wearing a bathing suit: e.g. lifting and sculpting glutes….</p>
				</div>
			</div>
			<div class="program">
				<div class="img-holder">
					<a href="#" data-modal="#program-03">
						<i class="icon-plus"></i>
						<img src="https://ondemand.physique57.com/files/thumb/regimen/56afaf1f0b968_51277.jpg" alt="image description" width="792" height="392">
					</a>
				</div>
				<strong class="duration">Slim Down, Tone Up</strong>
				<div class="text-holder">
					<p>In these eight-weeks you will get cardio and strength training combinations that target your trouble spots, and boosts your metabolism between workouts.</p>
				</div>
			</div>
			<div class="program">
				<div class="img-holder">
					<a href="#" data-modal="#program-04">
						<i class="icon-plus"></i>
						<img src="https://ondemand.physique57.com/files/thumb/regimen/56afaf2e0d1d2_19290.jpg" alt="image description" width="792" height="392">
					</a>
				</div>
				<strong class="duration">Best Body Ever</strong>
				<div class="text-holder">
					<p>This twelve-week program is ideal for resetting your fitness routine, metabolism, and physique from the ground up. </p>
				</div>
			</div>
			<div class="program">
				<div class="img-holder">
					<a href="#" data-modal="#program-05">
						<i class="icon-plus"></i>
						<img src="https://ondemand.physique57.com/files/thumb/regimen/570ffbd4350a6_92067.jpg" alt="image description" width="792" height="392">
					</a>
				</div>
				<strong class="duration">Two Weeks to Hard Core</strong>
				<div class="text-holder">
					<p>Your two-week program will not only cinch every inch of your core, but also leave you feeling stronger, taller, and more confident. </p>
				</div>
			</div>
			<div class="program">
				<div class="img-holder">
					<a href="#" data-modal="#program-06">
						<i class="icon-plus"></i>
						<img src="https://ondemand.physique57.com/files/thumb/regimen/56afaf28c4549_37204.jpg" alt="image description" width="792" height="392">
					</a>
				</div>
				<strong class="duration">Total Body Blast</strong>
				<div class="text-holder">
					<p>This is four-weeks of high intensity cardio that will whip you into shape.</p>
				</div>
			</div>
		</div>
	</div>
</section>