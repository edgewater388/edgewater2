<section class="result-section">
	<div class="container">
		<div class="side-lines cream">
			<h1>real results</h1>
		</div>
		<h5 class="do-something">Do something today that your future self will thank you for.</h5>
		<div class="carousel">
			<div class="mask">
				<div class="slideset">
					<div class="slide">
						<div class="img-holder">
							<img src="/themes/basic/images/home-logged-out/result-slide-02.png" alt="image description" width="597" height="431">
							<div class="share-box">
								<span>share</span>
								<ul>
									<li id="facebook" class="soc-link"><a href="https://www.facebook.com/Physique57" target="_blank" class="icon-facebook"></a></li>
									<li id="twitter" class="soc-link"><a href="https://twitter.com/Physique57" target="_blank" class="icon-twitter"></a></li>
								</ul>
							</div>
						</div>
						<div class="text-holder">
							<div class="frame">
								<p>Shelly loved doing the Physique 57 DVDs, but after having two children, her devotion to exercise faded. It wasn’t until she discovered the Physique 57 On Demand’s monthly challenges that she got back on board, committing herself to the burn. She posted a daily schedule to her wall and before long, she gained energy, motivation and strength while losing two dress sizes. To this day, Physique 57 continues to give her that feeling of accomplishment as well as a toned, fit body she never thought possible.</p>
								<div class="success-stories-btn">
									<div class="blue-dot-btn gray large">
										<a href="https://ondemand.physique57.com/success-stories" id="see-more-btn">
											<div class="text">SUCCESS STORIES</div>
											<div class="border tl"></div>
											<div class="border tr"></div>
											<div class="border br"></div>
											<div class="border bl"></div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="img-holder">
							<img src="/themes/basic/images/home-logged-out/result-slide-01.png" alt="image description" width="597" height="431">
							<div class="share-box">
								<span>share</span>
								<ul>
									<li id="facebook" class="soc-link"><a href="https://www.facebook.com/Physique57" target="_blank" class="icon-facebook"></a></li>
									<li id="twitter" class="soc-link"><a href="https://twitter.com/Physique57" target="_blank" class="icon-twitter"></a></li>
								</ul>
							</div>
						</div>
						<div class="text-holder">
							<div class="frame">
								<p>Amy turned to Physique 57 after realizing she was gaining weight. She was determined to fit back into her original size but what she got was a fun, challenging workout with quick and motivating results. With a regular practice of five to six workouts per week, Amy discovered a stronger, leaner, fitter and more energetic version of herself. She not only got back to her size, but looked better than she ever had. Thanks to Physique 57, she realized that strong is a good look.</p>
								<div class="success-stories-btn">
									<div class="blue-dot-btn gray large">
										<a href="https://ondemand.physique57.com/success-stories" id="see-more-btn">
											<div class="text">SUCCESS STORIES</div>
											<div class="border tl"></div>
											<div class="border tr"></div>
											<div class="border br"></div>
											<div class="border bl"></div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="img-holder">
							<img src="/themes/basic/images/home-logged-out/result-slide-03.png" alt="image description" width="597" height="431">
							<div class="share-box">
								<span>share</span>
								<ul>
									<li id="facebook" class="soc-link"><a href="https://www.facebook.com/Physique57" target="_blank" class="icon-facebook"></a></li>
									<li id="twitter" class="soc-link"><a href="https://twitter.com/Physique57" target="_blank" class="icon-twitter"></a></li>
								</ul>
							</div>
						</div>
						<div class="text-holder">
							<div class="frame">
								<p>Jennifer loved doing barre workouts, but with small children at home, she couldn’t find time to make it to the studio. She discovered Physique 57 On Demand and began her journey to achieving the long, lean, toned body she craved without having to leave the house. With the always-changing workouts, she feels challenged everyday and loves the energy and strength it gives her to tackle life head on. </p>
								<div class="success-stories-btn">
									<div class="blue-dot-btn gray large">
										<a href="https://ondemand.physique57.com/success-stories" id="see-more-btn">
											<div class="text">SUCCESS STORIES</div>
											<div class="border tl"></div>
											<div class="border tr"></div>
											<div class="border br"></div>
											<div class="border bl"></div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<a class="btn-prev" href="#"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
			<a class="btn-next" href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
		</div>
	</div>
</section>