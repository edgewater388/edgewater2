<section id="stream-workouts">
	<header>
		<div class="lockout">
			<div class="side-lines cream">
				<h4>WHEREVER & WHENEVER</h4>
			</div>
			<h1>STREAM WORKOUTS ON DEMAND</h1>
			<div class="subtitle">
				We’ve created a no-nonsense, on-demand streaming service that gives you a great workout anywhere,
				anytime.
			</div>
			<!--            <div class="num">02</div>-->
		</div>
	</header>
	<div class="lockout">
		<img class="devices" src="/themes/basic/images/home-logged-out/stream-devices@2x.png">
		<a href="/sign-up" class="sign-up-btn btn blue">sign up for free trial</a>
	</div>
</section>