<section class="stream-section">
	<div class="headline">
		<div class="container">
			<strong class="subtitle">WHEREVER & WHENEVER</strong>
			<h1>STREAM WORKOUTS ON DEMAND</h1>
			<p>We’ve created a no-nonsense, on-demand streaming service <br>that gives you a great workout anywhere, anytime.</p>
			<div class="divider"></div>
		</div>
		<div class="triangle top">
			<svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" preserveAspectRatio="none">
				<polygon fill='#ffffff' points="0,0 100,0 100,100"/>
			</svg>
		</div>
		<div class="triangle bottom">
			<svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" preserveAspectRatio="none">
				<polygon fill='#00d0f8' points="0,-1 100,-1 0,100"/>
			</svg>
		</div>
	</div>
	<div class="container">
		<div class="img-holder">
			<img src="themes/basic/images/home-logged-out/device.png" alt="image description" width="678" height="439">
		</div>
		<a href="<?= $this->createUrl('customer/create'); ?>" class="btn blue">SIGN UP FOR FREE TRIAL</a>
	</div>
</section>