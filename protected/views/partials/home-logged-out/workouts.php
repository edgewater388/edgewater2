<section class="workout-program">
	<div class="triangle">
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" preserveAspectRatio="none">
			<polygon fill='#ffffff' points="0,102 100,0 100,102"/>
		</svg>
	</div>
	<div class="text-holder">
		<div class="container">
			<h1>workout programs</h1>
			<p>Do something today that your future self will thank you for.</p>
			<i class="divider"></i>
			<strong>START SCULPTING. Workout programs that fit <br>you schedule and get you fit fast.</strong>
		</div>
	</div>
</section>