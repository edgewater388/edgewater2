<?php
    $next_billing_date = null;

    if ($membership = Yii::app()->user->membership) {
        // membership is still active
        if ($membership->valid_to && $membership->valid_to >= date('Y-m-d'))
            $next_billing_date = date('n/j/Y', strtotime($membership->valid_to));
        // membership will be billed right away
        else
            $next_billing_date = date('n/j/Y');
    }

    // new membership
    if (!$next_billing_date)
        $next_billing_date = date('n/j/Y', strtotime(Membership::getTrialEndDate()));
?>

<p>
    I agree to the terms of the Physique 57 Three Month contract. I understand that this contract is a
    minimum three (3) month commitment which will
    automatically renew on a monthly basis at the end of the three (3) month term. I authorize Physique
    57 to automatically debit/charge my credit card once a
    month for the price of the Three Month Membership plus applicable.
</p>
<p>
    Membership Terms
</p>
<p>
    BILLING: Each month, beginning on <?php echo $next_billing_date; ?>, you will be charged $19.95 using the
    debit/credit card
    on file for the Three Month Membership. After the three
    (3) month commitment is met, this membership automatically renews on a monthly basis at the rate of
    $19.95. You will be charged this amount every month
    until you opt out of auto renew per the auto renew cancellation policy or you cancel in accordance
    with the membership cancellation policy.
</p>
<p>
    DISHONORED BANK DRAFT: If any debit/credit card charge payable to Physique 57 is not honored,
    Management will: (a) assess a $25 charge for each credit card
    payment rejected to reimburse Physique 57 for the cost of collection; and (b) collect the current
    and past-due balance in any subsequent month.
</p>
<p>
    UNPAID BALANCES: All balances in arrears of 30 days are subject to a monthly service charge. Any
    unpaid balance for membership fees, goods or services past
    30 days may result in automatic suspension of Studio privileges. You are obligated to pay any
    collection and/or legal costs incurred by Management for
    collection. Physique 57 reserves the right to charge balances and overdue balances to their current
    account under the Electronic Funds Transfer
    Authorization.
</p>
<p class="bold">
    AUTO RENEW CANCELLATION POLICY: This contract is a monthly commitment and will continue to
    remain active unless you cancel it prior to the next billing
    date.
</p>
<p>
    CANCELLATION POLICY: Clients will have the option to cancel this contract within any 90 day period
    for any of the following reasons listed below.
    Reasonable and sufficient notice must be given to Physique 57 in order to receive a refund.
</p>
<ul>
    <li>
        If upon a doctor’s order, you cannot physically receive the services because of significant
        physical disability for a period in excess of two months.
    </li>
    <li>
        If you die, your estate shall be relieved of any further obligation for payment under the
        contract not then due and owing.
    </li>
    <li>
        All monies paid pursuant to such contract cancelled for the reasons in this paragraph shall be
        refunded within fifteen (15) days of such notice of
        cancellation; provided however that Physique 57 may retain the expenses incurred and the portion
        of
        the total price representing the services used or
        completed, and further provided that Physique 57 shall demand the reasonable costs of goods and
        services which you consumed or wish to retain after
        cancellation of the contract. In no instance shall Physique 57 demand more than the full
        contract
        price from you. If you had executed any credit or loan
        agreement to pay for all or part of the health studio services, such negotiable instrument
        executed
        by you shall also be returned within fifteen (15) days.
        Promotional Months: You agree that if you received any free months as an inducement to enter
        into
        this Agreement or as a result of referring new members,
        such free months shall not be considered in computing the amount of any refund to which you
        shall be
        entitled.
    </li>
</ul>

<p>
    Physique 57 reserves the right to cancel this membership at any time in the event that the member
    engages in objectionable behavior.
</p>
<p>
    MEMBERSHIP FREEZE POLICY: The freeze policy allows you to temporarily suspend your membership in
    accordance with the following terms: (A) You may freeze at
    any point for medical reasons. Duration of a medical freeze will be determined by client’s
    individual circumstance. (B) If you are medically unable to use
    workout, you must provide a doctor’s letter at the time of requesting the freeze. The freeze will
    not begin earlier than on the date your doctor’s letter
    is received by Physique 57.
</p>
<p>
    I have read and agree with the conditions of the above terms.
</p>