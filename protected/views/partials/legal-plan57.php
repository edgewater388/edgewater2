<p>Your Physique 57 VOD membership, which starts with a <?= Membership::getTrialDuration() ?> day free trial offer,
    will begin when you click Start Membership. Simply cancel anytime within your trial period and you will not be
    charged. To cancel, go to "Your Account" and click on "Cancel Membership." No refunds or credits
    for partial months. By clicking Start Membership, you authorize us to continue your
    month-to-month Physique 57 VOD membership (currently $29.95 per month)
    automatically charged monthly to the payment method provided, until you cancel. Only one free
    trial per member. See Terms of Use for more details.
</p>