<!-- Pixel Tracking -->

<?php if ($this->id == 'site' && $this->action->id == 'index'): ?>

	<?php // home page, logged in, tutorial page ?>
    <?php if (Yii::app()->request->pathInfo == 'home' && isset($_GET['tutorial']) && $_GET['tutorial'] == 1): ?>

	    <!-- Code for Action: Physique57_Conversion Pixel -->
		<!-- Begin Rocket Fuel Conversion Action Tracking Code Version 9 -->
		<script type='text/javascript'>
		(function() {
			var w = window, d = document;
			var s = d.createElement('script');
			s.setAttribute('async', 'true');
			s.setAttribute('type', 'text/javascript');
			s.setAttribute('src', '//c1.rfihub.net/js/tc.min.js');
			var f = d.getElementsByTagName('script')[0];
			f.parentNode.insertBefore(s, f);
			if (typeof w['_rfi'] !== 'function') {
				w['_rfi']=function() {
					w['_rfi'].commands = w['_rfi'].commands || [];
					w['_rfi'].commands.push(arguments);
				};
			}
			_rfi('setArgs', 'ver', '9');
			_rfi('setArgs', 'rb', '27082');
			_rfi('setArgs', 'ca', '20741567');
			_rfi('setArgs', '_o', '27082');
			_rfi('setArgs', '_t', '20741567');
			_rfi('setArgs', 'transid', 'INSERT_TRANSACTION_ID');
			_rfi('track');
		})();
		</script>
		<noscript>
		  <iframe src='//20741567p.rfihub.com/ca.html?rb=27082&ca=20741567&_o=27082&_t=20741567&ra=YOUR_CUSTOM_CACHE_BUSTER&transid=INSERT_TRANSACTION_ID' style='display:none;padding:0;margin:0' width='0' height='0'>
		</iframe>
		</noscript>
		<!-- End Rocket Fuel Conversion Action Tracking Code Version 9 -->
	
	<?php // home page ?>
    <?php else: ?>

	    <!-- Code for Action: Physique57_Landing Pixel -->
		<!-- Begin Rocket Fuel Conversion Action Tracking Code Version 9 -->
		<script type='text/javascript'>
		(function() {
			var w = window, d = document;
			var s = d.createElement('script');
			s.setAttribute('async', 'true');
			s.setAttribute('type', 'text/javascript');
			s.setAttribute('src', '//c1.rfihub.net/js/tc.min.js');
			var f = d.getElementsByTagName('script')[0];
			f.parentNode.insertBefore(s, f);
			if (typeof w['_rfi'] !== 'function') {
				w['_rfi']=function() {
					w['_rfi'].commands = w['_rfi'].commands || [];
					w['_rfi'].commands.push(arguments);
				};
			}
			_rfi('setArgs', 'ver', '9');
			_rfi('setArgs', 'rb', '27082');
			_rfi('setArgs', 'ca', '20741565');
			_rfi('setArgs', '_o', '27082');
			_rfi('setArgs', '_t', '20741565');
			_rfi('track');
		})();
		</script>
		<noscript>
		  <iframe src='//20741565p.rfihub.com/ca.html?rb=27082&ca=20741565&_o=27082&_t=20741565&ra=YOUR_CUSTOM_CACHE_BUSTER' style='display:none;padding:0;margin:0' width='0' height='0'>
		</iframe>
		</noscript>
		<!-- End Rocket Fuel Conversion Action Tracking Code Version 9 -->
        

    <?php endif; ?>
<?php else: ?>
	<?php // all other pages ?>

    <!-- Code for Action: Physique57_Tracking Pixel -->
	<!-- Begin Rocket Fuel Conversion Action Tracking Code Version 9 -->
	<script type='text/javascript'>
	(function() {
		var w = window, d = document;
		var s = d.createElement('script');
		s.setAttribute('async', 'true');
		s.setAttribute('type', 'text/javascript');
		s.setAttribute('src', '//c1.rfihub.net/js/tc.min.js');
		var f = d.getElementsByTagName('script')[0];
		f.parentNode.insertBefore(s, f);
		if (typeof w['_rfi'] !== 'function') {
			w['_rfi']=function() {
				w['_rfi'].commands = w['_rfi'].commands || [];
				w['_rfi'].commands.push(arguments);
			};
		}
		_rfi('setArgs', 'ver', '9');
		_rfi('setArgs', 'rb', '27082');
		_rfi('setArgs', 'ca', '20741566');
		_rfi('setArgs', '_o', '27082');
		_rfi('setArgs', '_t', '20741566');
		_rfi('track');
	})();
	</script>
	<noscript>
	  <iframe src='//20741566p.rfihub.com/ca.html?rb=27082&ca=20741566&_o=27082&_t=20741566&ra=YOUR_CUSTOM_CACHE_BUSTER' style='display:none;padding:0;margin:0' width='0' height='0'>
	</iframe>
	</noscript>
	<!-- End Rocket Fuel Conversion Action Tracking Code Version 9 -->

<?php endif; ?>