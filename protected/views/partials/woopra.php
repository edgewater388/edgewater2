<!-- Start of Woopra Code -->
<script>
(function(){
        var t,i,e,n=window,o=document,a=arguments,s="script",r=["config","track","identify","visit","push","call","trackForm","trackClick"],c=function(){var t,i=this;for(i._e=[],t=0;r.length>t;t++)(function(t){i[t]=function(){return i._e.push([t].concat(Array.prototype.slice.call(arguments,0))),i}})(r[t])};for(n._w=n._w||{},t=0;a.length>t;t++)n._w[a[t]]=n[a[t]]=n[a[t]]||new c;i=o.createElement(s),i.async=1,i.src="//static.woopra.com/js/w.js",e=o.getElementsByTagName(s)[0],e.parentNode.insertBefore(i,e)
})("woopra");
 
woopra.config({
    domain: 'ondemand.physique57.com'
});
woopra.track();
</script>
<!-- End of Woopra Code -->

<?php if (Yii::app()->user->id): ?>
<script>
/* Place this on a template where a customer initially is identified
   or after authentication. (Important: Update these values) */
 
woopra.identify({
        email: "<?php echo Yii::app()->user->email; ?>",
        name: "<?php echo Yii::app()->user->name; ?>",
});
 
// The identify code should be added before the "track()" function
// woopra.track();
</script>
<?php endif; ?>