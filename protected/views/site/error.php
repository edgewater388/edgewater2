<?php
$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>


<?php $this->bodyClass = 'account error short-nav short-nav-perm'; ?>

<section>
    <div class="lockout">
        <div class="side-lines cream">
            <h4><?php echo $code; ?> ERROR</h4>
        </div>
        <h1><?php echo CHtml::encode($message); ?></h1>
    </div>
</section>



