<?php $this->bodyClass = 'home no-top-padding'; ?>
<?php $this->renderPartial('//partials/home-logged-out/intro'); ?>
<?php $this->renderPartial('//partials/home-logged-out/partners'); ?>
<?php $this->renderPartial('//partials/home-logged-out/entry'); ?>
<?php $this->renderPartial('//partials/home-logged-out/how-it-works'); ?>
<?php $this->renderPartial('//partials/home-logged-out/results'); ?>
<?php $this->renderPartial('//partials/home-logged-out/workouts'); ?>
<?php $this->renderPartial('//partials/home-logged-out/programs'); ?>
<?php $this->renderPartial('//partials/home-logged-out/instructors'); ?>
<?php $this->renderPartial('//partials/home-logged-out/mashups'); ?>
<?php $this->renderPartial('//partials/home-logged-out/stream'); ?>

<!-- invisible div used for tutorial deeplink -->
<div data-modal="#tutorial" style="display: none;"></div>

<?php $this->renderPartial('//modals/program-01'); ?>
<?php $this->renderPartial('//modals/program-02'); ?>
<?php $this->renderPartial('//modals/program-03'); ?>
<?php $this->renderPartial('//modals/program-04'); ?>
<?php $this->renderPartial('//modals/program-05'); ?>
<?php $this->renderPartial('//modals/program-06'); ?>

<?php
    if (Settings::getSettingValue(Settings::SETTING_DEMO_ACCESS)) {
        $this->renderPartial('//modals/start-demo');
    }
?>

<script type="text/javascript">
  $('#yw0').submit(function () {
    _kmq.push(['identify', $('#LoginForm_email').val()]);
    _kmq.push(['record', 'Login', {'email': $('#LoginForm_email').val()}]);
  });
</script>