<?php $this->bodyClass = 'account'; ?>

<section>
    <div class="lockout">
        <div class="side-lines">
            <h1>my account</h1>
        </div>
    </div>
    <div class="content">
        <div class="lockout">
            <div class="account-group">
                <h4>membership & billing</h4>
                <hr>
                <div class="row">
                    <div class="left">
						<?php if ($membership): ?>
							<?php if ($membership->cancelled_date): ?>
                                <p>Your membership was cancelled
                                    on <?php echo date('M j, Y', strtotime($membership->cancelled_date)); ?> and will
                                    remain active
                                    until <?php echo date('M j, Y', strtotime($membership->valid_to)); ?></p>
							<?php elseif ($membership->isActive()): ?>
								<?php if (Membership::checkUserCancelDiscount()): ?>
                                    <button class="btn" type="button" data-modal="#lowerPriceOffering">
                                        cancel membership
                                    </button>
								<?php else: ?>
                                    <a class="btn" href="/cancel-account">cancel membership</a>
								<?php endif; ?>
							<?php endif; ?>
						<?php endif; ?>
                    </div>
                    <div class="center">
                        <ul>
                            <li><?php echo $model->email; ?></li>
							<?php if (!Users::checkIfSocial()): ?>
                                <li>PASSWORD: ********</li>
							<?php endif; ?>
                        </ul>
                    </div>
                    <div class="right">
                        <ul>
                            <li>
                                <a href="/change-email">change email</a>
                            </li>
							<?php if (!Users::checkIfSocial()): ?>
                                <li>
                                    <a href="/change-password">change password</a>
                                </li>
							<?php endif; ?>
                            <li>
                                <a href="/email-preferences">email preferences</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <hr class="short right">
                <div class="row">
                    <div class="left"></div>
                    <div class="center">
                        <ul>
                            <li>
								<?php if ($customer): ?>
									<?php if (in_array($customer->card_type, ['Amex', 'Visa', 'MasterCard', 'Discover Card'])): ?>
                                        <img src="/themes/basic/images/global/cc-<?php echo strtolower(str_ireplace(' ', '-', $customer->card_type)); ?>@2x.png"
                                             class="cc-icon">
									<?php endif; ?>
                                    <span class="cc-num-mobile">**** <?php echo $customer->card_number; ?></span>
                                    <span class="cc-num">**** **** **** <?php echo $customer->card_number; ?></span>
								<?php else: ?>
                                    <!-- <a href="/update-payment-info" class="btn update-payment-btn" id="update-payment-btn">Update Payment Info</a> -->
								<?php endif; ?>
                            </li>
                        </ul>
                    </div>
                    <div class="right">
                        <ul>
                            <li>
                                <a href="/update-payment-info">update payment info</a>
                            </li>
                            <li>
                                <a href="/billing-details">billing details</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="account-group">
                <h4>plan details</h4>
                <hr>
                <div class="row">
                    <div class="left"></div>
                    <div class="center">
                        <ul>
							<?php if ($membership): ?>
								<?php if ($membership->isActive()): ?>
									<?php if ($membership->plan_id): ?>
                                        <li><?php echo strtoupper($membership->interval); ?> -
                                            $<?php echo $membership->subtotal; ?></li>
									<?php else: ?>
                                        <li>No Plan - $<?php echo $membership->subtotal; ?></li>
									<?php endif; ?>
								<?php else: ?>
                                    <li>Membership Expired</li>
								<?php endif; ?>
								<?php if (!$membership->canChangeOrCancel()): ?>
                                    <li><?php echo Yii::t('app', '1 Month|{n} Month', ($membership->min_term - $membership->plan_renewal_count)); ?>
                                        Commitment Remaining
                                    </li>
								<?php endif; ?>
							<?php else: ?>
                                <li>No Plan</li>
							<?php endif; ?>

							<?php if ($membership && $membership->promo_amount !== null): ?>
                                <li>
									<?php if ($membership->promoCode): ?>
                                        <div><?php echo $membership->promoCode->description; ?></div>
									<?php endif; ?>
                                    Promo: $<?php echo number_format($membership->promo_amount, 2); ?>
									<?php if ($membership->promo_end_date): ?>
                                        <br>
                                        <span style="font-size: .9em;">(Ends <?php echo $membership->promo_end_date; ?>
                                            )</span>
									<?php endif; ?>
                                </li>
							<?php endif; ?>
                        </ul>
                    </div>
                    <div class="right">
                        <ul>
                            <li><a href="/change-plan">change plan</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="account-group">
                <h4>my profile</h4>
                <hr>
                <div class="row">
                    <div class="left"></div>
                    <div class="center">
                        <ul>
                            <li>
                                <div class="user-pic">
                                    <div class="inner">
                                        <a href="/manage-profile" class="profile-link">
                                            <div class="pic">
                                                <img src="<?php echo $model->profile->getThumbnailUrl(); ?>"
                                                     width="48"
                                                     height="48">
                                            </div>
                                            <div class="user"><?php echo $model->profile->name; ?></div>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="right">
                        <ul>
                            <li>
                                <a href="/manage-profile">EDIT PROFILE</a>
                            </li>
                            <li>
                                <a href="/viewing-activity">VIEWING ACTIVITY</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
			<?php if ($membership): ?>
                <div class="account-group">
                    <h4>promotional offers</h4>
                    <hr>
					<?php $form = $this->beginWidget('ActiveForm', [
						'action'      => Yii::app()->createUrl('/user/users/account'),
						'htmlOptions' => [
							'data-validate' => '',
							'data-submit'   => '#redeem-btn',
						],
					]); ?>
                    <!-- <form action="#" method="post" data-validate data-submit-btn="#redeem-btn"> -->
                    <div class="row">
                        <div class="left redeem">
							<?php /*
                            <input type="text" placeholder="Enter 5 Digit Code" data-validate="min:1"/>
                            */ ?>
							<?php echo $form->textField($membership, 'new_promo_code', ['placeholder' => 'Enter Promotional Code', 'data-validate' => 'min:1']); ?>
							<?php echo $form->error($membership, 'new_promo_code'); ?>
                        </div>
                        <div class="center redeem">
                            <ul>
                                <li>
                                    <input type="submit" class="btn redeem-btn" id="redeem-btn" value="Redeem">
                                    <div class="tooltip"></div>
                                </li>
                            </ul>
                        </div>
                        <div class="right redeem">
                            <ul>
                                <li>
                                    <a href="/billing-details#promotions">current promotions</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- </form> -->
					<?php $this->endWidget(); ?>
                </div>
			<?php endif; ?>

            <a name="referafriend" style="position: relative; top: -100px; visibility: hidden; pointer-events: none;">&nbsp;</a>
            <div class="account-group">
                <h4>refer a friend</h4>
                <hr>
                <p>Refer a friend and get $<?php echo ReferralBank::getRewardAmount(); ?> off your next month's
                    membership.</p>
				<?php $form = $this->beginWidget('ActiveForm', [
					'action'      => Yii::app()->createUrl('/user/users/account'),
					'htmlOptions' => [
						'data-validate' => '',
						'data-submit'   => '#refer-btn',
					],
				]); ?>
                <div class="row">
                    <div class="left redeem">
                        <br>
						<?php echo $form->textField($referral_invite, 'email', ['data-validate' => 'min:5', 'placeholder' => 'Enter a Friend\'s Email']); ?>
                        <p class="note" style="font-size: .8em; margin: 4px 0 0; font-weight: 500;">
                            Enter up to 10 email addresses, comma-separated <br>
                            <span style="font-size: .9em;">(Referrals expire after 30 days)</span>
                        </p>
						<?php echo $form->error($referral_invite, 'email'); ?>
                    </div>
                    <div class="center redeem">
                        <br>
                        <ul>
                            <li>
                                <input type="submit" value="Refer" class="btn redeem-btn" id="refer-btn">
                                <div class="tooltip"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="right redeem">
                        <ul>
							<?php if ($referral_code = Yii::app()->user->referralCode): ?>
                                <li>
                                    Your invite url: <br>
                                    <input type="text"
                                           value="http://<?php echo $_SERVER['HTTP_HOST']; ?>/invite/<?php echo $referral_code; ?>"
                                           style="width: 300px;" onClick="this.select()">
                                </li>
							<?php else: ?>
                                <li><a href="/account?getinvitelink=1">Get your invite link</a></li>
							<?php endif; ?>
                        </ul>
                    </div>
                </div>
				<?php $this->endWidget(); ?>

            </div>
        </div>
    </div>
</section>

<?php
    if (Membership::checkUserCancelDiscount()) {
        $this->renderPartial('//modals/lower-price-offering');
    }
?>

<?php if (Yii::app()->request->getParam('cancelled')): ?>
    <script>
      woopra.track("membership_cancelled", {});
    </script>
<?php endif; ?>

<?php if (Yii::app()->request->getParam('plan_changed')): ?>
    <script>
      woopra.track("plan_changed", {});
    </script>
<?php endif; ?>
