<?php $this->bodyClass = 'account billing-details'; ?>

<section>
    <div class="lockout">
        <div class="side-lines">
            <h1>my account</h1>
        </div>
    </div>
    <div class="content">
        <div class="lockout">
            <h4>billing details</h4>
            <hr>
            <div id="details-wrap">
                <?php if ($membership && $membership->isActive()): ?>
                <ul>
                    <li>
                        <div class="monthly"></div>
                        <div class="date">Next Billing Date</div>
                        <div class="cost">Price</div>
                    </li>
                    <li>
                        <div class="monthly"><?php echo strtoupper($membership->interval); ?></div>
                        <div class="date"><?php echo $membership->next_billing_date ? date('m/d/y', strtotime($membership->next_billing_date)) : '-'; ?></div>
                        <div class="cost">$<?php echo $membership->subtotal; ?> / <span class="mobile"><?php echo strtoupper($membership->getPeriodAbbreviated()); ?></span><span class="desktop"><?php echo strtoupper($membership->getPeriod()); ?></span>  </div>
                        <div class="fine-print">Membership fees are billed at the beginning of each period. Sales tax
                            may apply.
                        </div>
                    </li>
                </ul>
                <?php else: ?>
                    <p>No active membership</p>
                    <br>
                    <div><a class="btn" href="/change-plan">Add a Plan</a></div>
                <?php endif; ?>
            </div>
            <h4>billing history</h4>
            <hr>
            <div id="history-wrap">
                <ul>
                    <?php if ($transactions): ?>
                        <?php foreach ($transactions as $transaction): ?>
                        <li>
                            <div class="date col">
                                <div class="title">Date</div>
                                <div class="text"><?php echo date('m/d/Y', strtotime($transaction->created)); ?></div>
                            </div>
                            <div class="method col">
                                <div class="title">Method</div>
                                <div class="text"><?php echo strtoupper($transaction->cardType); ?> **** ***** <?php echo $transaction->card_number; ?></div>
                            </div>
                            <div class="description col">
                                <div class="title">Desription</div>
                                <div class="text"><?php echo $transaction->description; ?></div>
                            </div>
                            <div class="service-period col">
                                <div class="title">Service Period</div>
                                <div class="text"><?php echo $transaction->membership_period; ?></div>
                            </div>
                            <div class="subtotal col">
                                <div class="title">Subtotal</div>
                                <div class="text">$<?php echo $transaction->subtotal; ?> (+$<?php echo $transaction->tax; ?> tax)</div>
                            </div>
                            <div class="total col">
                                <div class="title">Total</div>
                                <div class="text">$<?php echo $transaction->total; ?></div>
                            </div>
                        </li>
                        <?php endforeach; ?>
                    <?php else: ?>
                        No transaction history
                    <?php endif; ?>
                    <?php /*
                    <li>
                        <div class="date col">
                            <div class="title">Date</div>
                            <div class="text">10/16/15</div>
                        </div>
                        <div class="method col">
                            <div class="title">Method</div>
                            <div class="text">AMEX **** ***** 2004</div>
                        </div>
                        <div class="description col">
                            <div class="title">Desription</div>
                            <div class="text">P57 VOD Service</div>
                        </div>
                        <div class="service-period col">
                            <div class="title">Service Period</div>
                            <div class="text">10/11/15 - 11/11/15</div>
                        </div>
                        <div class="subtotal col">
                            <div class="title">Subtotal</div>
                            <div class="text">$19.99 (+$0.12 tax)</div>
                        </div>
                        <div class="total col">
                            <div class="title">Total</div>
                            <div class="text">$20.11</div>
                        </div>
                    </li>
                    */ ?>
                </ul>
            </div>
            <div id="asterisk-text">
                <p><span class="darker-blue">*</span> We show up to 1 year of billing history</p>
            </div>

            <a name="promotions" style="position: relative; top: -100px; visibility: hidden; pointer-events: none;">&nbsp;</a>
            <h4>Promotions</h4>
            <hr>
            <div id="history-wrap">
                <?php if ($membership && $membership->promo_amount !== null): ?>
                <ul>
                    <li>
                        <div class="date col">
                            <div class="title">Date Redeemed</div>
                            <?php if ($membership->promoCodeRedemptionLog): ?>
                            <div class="text"><?php echo date('m/d/Y', strtotime($membership->promoCodeRedemptionLog->created)); ?></div>
                            <?php endif; ?>
                        </div>
                        <div class="subtotal col">
                            <div class="title">Code</div>
                            <div class="text"><?php echo $membership->promo_code; ?></div>
                        </div>
                        <div class="description col" astyle="width: 33%;">
                            <div class="title">Desription</div>
                            <?php if ($membership->promoCode): ?>
                            <div class="text"><?php echo $membership->promoCode->description; ?></div>
                            <?php endif; ?>
                        </div>
                        <div class="date col" stayle="width: 12%;">
                            <div class="title">Expires</div>
                            <div class="text"><?php echo date('m/d/Y', strtotime($membership->promo_end_date)); ?></div>
                        </div>
                        <div class="subtotal col">
                            <div class="title">Discount</div>
                            <div class="text">$<?php echo number_format($membership->promoDiscount, 2); ?></div>
                        </div>
                    </li>
                </ul>
                <?php else: ?>
                <p>No current promotion</p>
                <?php endif; ?>
            </div>
            <br>
            
            <a class="btn" href="/account">back to account</a>
        </div>

    </div>
</section>
