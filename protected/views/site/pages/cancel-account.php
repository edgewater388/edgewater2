<?php $this->bodyClass = 'account cancel-account'; ?>

<section>
    <div class="lockout">
        <div class="side-lines">
            <h1>my account</h1>
        </div>
    </div>
    <div class="content">
        <div class="lockout">
            <h4>cancel account</h4>
            <hr>
            <div class="text">
                <p>Click “Finish Cancellation” below to cancel your membership.</p>
            </div>
            <ol>
                <li>Cancellation will be effective at the end of your current billing period on <?php echo date('M j, Y', strtotime($membership->valid_to)); ?>.</li>
                <li>Restart your membership anytime. <?php //Your viewing preferences will be saved for 10 months. ?></li>
            </ol>
            <?php $form=$this->beginWidget('ActiveForm', array(
                'action'=>Yii::app()->createUrl('/membership/cancel'),
                'htmlOptions'=>array(
                    'data-submit-btn'=>'#submit-btn',
                ),
            )); ?>
                <?php echo $form->errorSummary($membership); ?>
                <div class="checkbox-wrap">
                    <div class="left">
                        <input type="checkbox" name="email-me" id="email-me" value="1"/>
                    </div>
                    <div class="right">
                        <label class="checkbox" for="email-me">
                            Yes, please email me about newly added workouts & features Physique 57 offers.
                        </label>
                    </div>
                </div>
                <?php echo $form->hiddenField($membership, 'cancellation_date', array('value'=>'1')); ?>
                <div class="buttons">
                    <input type="submit" class="btn blue" id="submit-btn" value="Finish Cancellation">
                    <a class="btn" href="/account">go back</a>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</section>
