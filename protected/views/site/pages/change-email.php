<?php $this->bodyClass = 'account change-email'; ?>

<section>
    <div class="lockout">
        <div class="side-lines">
            <h1>my account</h1>
        </div>
    </div>
    <div class="content">
        <div class="lockout">
            <h4>change email</h4>
            <hr>
            <div class="current-email">current email</div>
            <div class="email-address"><?php echo Yii::app()->user->email; ?></div>
			<?php $form = $this->beginWidget('ActiveForm', [
				'action'      => Yii::app()->createUrl('/user/users/email'),
				'htmlOptions' => [
					'data-validate'   => '',
					'data-submit-btn' => '#submit-btn',
				],
			]); ?>
			<?php //echo $form->errorSummary($model); ?>

            <div class="inputs">
                <div class="input-item">
                    <label>new email</label>
					<?php echo $form->textField($model, 'email', ['data-validate' => 'min:5|email']); ?>
					<?php echo $form->error($model, 'email'); ?>
                </div>
				<?php if (!Users::checkIfSocial()): ?>
                    <div class="input-item">
                        <label>current password</label>
						<?php echo $form->passwordField($model, 'current_password', ['data-validate' => 'min:4']); ?>
						<?php echo $form->error($model, 'current_password'); ?>
                    </div>
				<?php endif; ?>
            </div>
            <div class="buttons">
                <input type="submit" class="btn blue" id="submit-btn" value="save">
                <a class="btn" href="/account">cancel</a>
            </div>
			<?php $this->endWidget(); ?>
        </div>
    </div>
</section>
