<?php $this->bodyClass = 'account change-password'; ?>

<section>
    <div class="lockout">
        <div class="side-lines">
            <h1>my account</h1>
        </div>
    </div>
    <div class="content">
        <div class="lockout">
            <h4>change password</h4>
            <hr>
            <?php $form=$this->beginWidget('ActiveForm', array(
                'action'=>Yii::app()->createUrl('/user/users/password'),
                'htmlOptions'=>array(
                    'data-validate'=>'',
                    'data-submit-btn'=>'.submit-btn',
                ),
            )); ?>
                <?php //echo $form->errorSummary($model); ?>

                <div class="inputs">
                    <div class="input-item">
                        <label>current password</label>
                        <?php echo $form->passwordField($model, 'current_password', array('data-validate'=>'min:4')); ?>
                        <?php echo $form->error($model, 'current_password'); ?>
                    </div>
                    <div class="input-item">
                        <label>new password</label>
                        <?php echo $form->passwordField($model, 'new_password', array('data-validate'=>'min:4')); ?>
                        <?php echo $form->error($model, 'new_password'); ?>
                    </div>
                    <div class="input-item">
                        <label>confirm new password</label>
                        <?php echo $form->passwordField($model, 'password2', array('data-validate'=>'min:4')); ?>
                        <?php echo $form->error($model, 'password2'); ?>
                    </div>
                </div>
                <div class="buttons">
                    <input type="submit" class="btn blue submit-btn" value="save">
                    <a class="btn" href="/account">cancel</a>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</section>