<?php $this->bodyClass = 'account change-plan'; ?>

<section>
    <div class="lockout">
        <div class="side-lines">
            <h1>my account</h1>
        </div>
    </div>
    <div class="content">
        <div class="lockout">
            <?php $form = $this->beginWidget('ActiveForm', array(
                'action' => Yii::app()->createUrl('/membership/change'),
                'htmlOptions' => array(
                    'data-validate' => '',
                    'data-locked-plan'=>(Yii::app()->user->membership && !Yii::app()->user->membership->canChange()) ? '1' : '0',
                ),
            )); ?>

            <?php echo $form->errorSummary($model); ?>
            
            <h4>change plan</h4>
            <hr>
            <ul id="account-types" data-radio-buttons="li">
                <li class="<?php echo ($model->isActive() && $model->plan_id === $plans[3]['id']) ? 'active current ' : ''; ?>" data-plan-id="plan57">
                    <label>
                        <img src="/themes/basic/images/account/renew-plan-thumb1.jpg">
                        <div class="text-overlay">
                            <h3 class="move-up">$<span
                                    class="number"><?php echo number_format($plans[3]['amount'], 2); ?></span></h3>
                            <h5><?php echo $plans[3]['title']; ?></h5>
                        </div>
                        <?php //echo $form->radioButton($model, 'plan_id', array('value'=>$plans[3]['id'])); ?>
                        <input type="radio" name="Membership[plan_id]" id="Membership_plan_id"
                               value="<?php echo $plans[3]['id']; ?>">
                        <?php
                        if ($model->isActive() && $model->plan_id === $plans[3]['id']) {
                            echo('<div class="current-plan" > CURRENT PLAN </div >');
                        }
                        ?>
                    </label>
                </li>
                <li class="<?php echo ($model->isActive() && $model->plan_id === $plans[4]['id']) ? 'active current ' : ''; ?>" data-plan-id="plan30">
                    <label>
                        <img src="/themes/basic/images/account/renew-plan-thumb3.jpg">
                        <div class="text-overlay">
                            <h3 class="move-up">$<span
                                    class="number"><?php echo number_format($plans[4]['amount'], 2); ?></span></h3>
                            <h5>MONTHLY</h5>
                            <h6>(<?php echo $plans[4]['description']; ?>)</h6>
                        </div>
                        <?php //echo $form->radioButton($model, 'plan_id', array('value'=>$plans[1]['id'])); ?>
                        <input type="radio" name="Membership[plan_id]" id="Membership_plan_id"
                               value="<?php echo $plans[4]['id']; ?>">
                        <?php
                        if ($model->isActive() && $model->plan_id === $plans[4]['id']) {
                            echo('<div class="current-plan" > CURRENT PLAN </div >');
                        }
                        ?>
                    </label>
                </li>
                <li class="<?php echo ($model->isActive() && $model->plan_id === $plans[1]['id']) ? 'active current ' : ''; ?>" data-plan-id="plan5">
                    <label>
                        <img src="/themes/basic/images/account/renew-plan-thumb2.jpg">
                        <div class="text-overlay">
                            <h3 class="move-up">$<span
                                    class="number"><?php echo number_format($plans[1]['amount'], 2); ?></span></h3>
                            <h5><?php echo $plans[1]['title']; ?></h5>
                            <h6>(<?php echo $plans[1]['description']; ?>)</h6>
                        </div>
                        <?php //echo $form->radioButton($model, 'plan_id', array('value'=>$plans[1]['id'])); ?>
                        <input type="radio" name="Membership[plan_id]" id="Membership_plan_id"
                               value="<?php echo $plans[1]['id']; ?>">
                    </label>
                    <?php
                        if($model->isActive() && $model->plan_id === $plans[1]['id']) {
                            echo('<div class="current-plan" > CURRENT PLAN </div >');
                        }
                    ?>
                </li>
            </ul> 

            <h4>HAVE A PROMOTIONAL CODE?</h4>
            <hr>
            <div class="inputs">
                <div class="row">
                    <div class="input-item code">
                        <?php /*
                        <input type="text" maxlength="32" data-validate="min:5" placeholder="Enter 5 Digit Code" />
                        */ ?>
                        <?php echo $form->textField($model, 'promo_code', array('maxlength'=>32, 'placeholder'=>'Enter Promotional Code')); ?>
                        <?php //echo $form->error($model, 'promo_code'); ?>

                        <?php if ($promoCode): ?>
                        <div>
                            <br>
                            <p>
                                Promotion: <span style="font-weight: 700;"><?php echo $promoCode->description; ?></span>
                            </p>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <br><br>
            <p class="text">Membership fees are billed at the begining of each period. Sales tax may apply.</p>

            <div class="legal plan57">
                <p>Your Physique 57 VOD membership will begin when you click Continue. To cancel, go to "Your Account"
                    and
                    click on "Cancel Membership." No refunds or credits for partial months. By clicking Start
                    Membership,
                    you authorize us to continue your month-to-month Physique 57 VOD membership (currently $29.95 per
                    month)
                    automatically charged monthly to the payment method provided, until you cancel. See Terms of Use for
                    more details.</p>

            </div>
            <div class="legal plan30">
                <?php $this->renderPartial('//partials/legal-plan30'); ?>
            </div>
            <div class="legal plan5">
                <p>Your Physique 57 VOD membership will begin when you click Continue. By clicking Continue you will
                    have access to Physique 57 VOD for 24 hours.</p>
            </div>


            <div class="checkbox-wrap">
                <div class="left">
                    <input type="checkbox" name="over_18" id="over_18" class="optional" value="1"/>
                    <!--                    --><?php //echo $form->checkbox($model, 'over_18', array('id' => 'over_18')); ?>
                </div>
                <div class="right">
                    <label class="checkbox" for="over_18">
                        <div class="main-checkbox">I agree to the conditions above.</div>
                    </label>
                </div>
            </div>

            <div class="text-danger">
                Please agree to the conditions to continue.
            </div>
            <div id="under-contract">
                Your current plan has a minimum commitment and cannot be changed at this time.
            </div>

            <div class="buttons">
                <div class="btn blue" data-modal="#confirm-plan" id="continue-btn">continue</div>
                <a href="/account" class="btn">cancel</a>
            </div>
            <?php $this->renderPartial('//modals/confirm-plan', array('model' => $model)); ?>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</section>
