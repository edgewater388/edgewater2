<?php $this->bodyClass = 'account contact-us short-nav short-nav-perm'; ?>

<section>
    <div class="lockout">
        <div class="side-lines">
            <h1>contact us</h1>
        </div>
    </div>
    <div class="content">
        <div class="lockout">
            <h4>WE ARE HERE TO HELP!</h4>
            <hr>
            <?php $form=$this->beginWidget('ActiveForm', array(
                'action'=>Yii::app()->createUrl('/site/contact'),
                'htmlOptions'=>array(
                    'data-validate'=>'',
                    'data-submit-btn'=>'#submit-btn',
                ),
            )); ?>
                <?php echo $form->errorSummary($model); ?>
                <div class="inputs">
                    <div class="row">
                        <div class="input-item">
                            <label>FIRST NAME</label>
                            <!-- <input type="text" data-validate="min:2"/> -->
                            <?php echo $form->textField($model, 'first_name', array('data-validate'=>'min:2')); ?>
                        </div>
                        <div class="input-item">
                            <label>LAST NAME</label>
                            <!-- <input type="text" data-validate="min:2"/> -->
                            <?php echo $form->textField($model, 'last_name', array('data-validate'=>'min:2')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-item">
                            <label>EMAIL</label>
                            <!-- <input type="text" data-validate="min:5|email"/> -->
                            <?php echo $form->textField($model, 'email', array('data-validate'=>'min:5|email')); ?>
                        </div>
                        <div class="input-item">
                            <label>PHONE NUMBER</label>
                            <!-- <input type="text" data-validate="min:10"/> -->
                            <?php echo $form->textField($model, 'phone', array('data-validate'=>'min:10')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-item textarea">
                            <label>HOW CAN WE HELP?</label>
                            <!-- <textarea name="text" id="text" cols="30" rows="10" data-validate="min:10"></textarea> -->
                            <?php echo $form->textarea($model, 'body', array('rows'=>10, 'data-validate'=>'min:10')); ?>
                        </div>
                    </div>
                </div>
                <div class="buttons">
                    <input type="submit" class="btn blue" id="submit-btn" value="submit help request">
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</section>
