<?php $this->bodyClass = 'account email-preferences'; ?>

<section>
    <div class="lockout">
        <div class="side-lines">
            <h1>my account</h1>
        </div>
    </div>
    <div class="content">
        <div class="lockout">
            <h4>email preferences</h4>
            <hr>
            <div class="current-email">current email</div>
            <div class="email-address"><?php echo Yii::app()->user->email; ?></div>
            <form action="" method="post" id="form" data-submit-btn="#submit-btn">
                <?php $form=$this->beginWidget('ActiveForm', array(
                    'id'=>'form',
                    'action'=>Yii::app()->createUrl('/user/users/password'),
                    'htmlOptions'=>array(
                        'data-submit-btn'=>'#submit-btn',
                    ),
                )); ?>
                <?php echo $form->errorSummary($model); ?>
                <div class="checkbox-wrap">
                    <div class="left">
                        <!-- <input type="checkbox" name="updates" id="updates" class="optional" value="1" /> -->
                        <?php echo $form->checkbox($model, 'groups[3125864]', array('id'=>'updates', 'class'=>'optional', 'checked'=>(isset($groups['3125864'])))); ?>
                    </div>
                    <div class="right">
                        <label class="checkbox" for="updates">
                            <div class="main-checkbox"><?php echo Yii::app()->params['emma']['groups']['3125864']; ?></div>
                            <div class="sub-checkbox">Receive notifications about new Physique 57 workouts and tips so
                                you can get the most out of your membership.
                            </div>
                        </label>
                    </div>
                </div>
                <div class="checkbox-wrap">
                    <div class="left">
                        <!-- <input type="checkbox" name="offers" id="offers" class="optional" value="1" /> -->
                        <?php echo $form->checkbox($model, 'groups[3126888]', array('id'=>'offers', 'class'=>'optional', 'checked'=>(isset($groups['3126888'])))); ?>
                    </div>
                    <div class="right">
                        <label class="checkbox" for="offers">
                            <div class="main-checkbox"><?php echo Yii::app()->params['emma']['groups']['3126888']; ?></div>
                            <div class="sub-checkbox">Receive special offers and promotions from Physique 57.</div>
                        </label>
                    </div>
                </div>
                <div class="checkbox-wrap">
                    <div class="left">
                        <!-- <input type="checkbox" name="surveys" id="surveys" class="optional" value="1" /> -->
                        <?php echo $form->checkbox($model, 'groups[3127912]', array('id'=>'surveys', 'class'=>'optional', 'checked'=>(isset($groups['3127912'])))); ?>
                    </div>
                    <div class="right">
                        <label class="checkbox" for="surveys">
                            <div class="main-checkbox"><?php echo Yii::app()->params['emma']['groups']['3127912']; ?></div>
                            <div class="sub-checkbox">Make your opinions heard! Give us feedback so we can make Physique
                                57 better suit your needs.
                            </div>
                        </label>
                    </div>
                </div>
                <hr class="short">
                <div class="checkbox-wrap all">
                    <div class="left">
                        <!-- <input type="checkbox" name="unsubscribe" id="unsubscribe" value="1"/> -->
                        <?php echo $form->checkbox($model, 'unsubscribe', array('id'=>'unsubscribe', 'name'=>'unsubscribe')); ?>
                    </div>
                    <div class="right">
                        <label class="checkbox" for="unsubscribe">
                            <div class="main-checkbox">unsubscribe from all lists above</div>
                            <div class="sub-checkbox">Note: You will continue to receive transactional emails related to
                                your account.
                            </div>
                        </label>
                    </div>
                </div>
                <div class="buttons">
                    <div class="btn blue" id="submit-btn">save</div>
                    <a class="btn" href="/account">cancel</a>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</section>
