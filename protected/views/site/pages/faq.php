<?php $this->bodyClass = 'faq short-nav short-nav-perm'; ?>

<section class="header">
    <header>
        <div class="lockout">
            <div class="side-lines">
                <h1>f.a.q.</h1>
            </div>
            <h4 class="sub-header">WHAT DO YOU NEED HELP WITH?</h4>
        </div>
    </header>
    <div class="content">
        <ul class="categories">
            <li class="category">
                <div class="lockout">
                    <h5 class="title">MONTHLY SUBSCRIPTIONS</h5>
                    <ul class="questions">
                        <li>
                            <h5 class="question">How does the monthly subscription work?<a class="arrow"
                                                                                           href="#"><img
                                        src="/themes/basic/images/global/down-arrow.svg" width="13"></a></h5>

                            <div class="answer">
                                <p>Once you sign up for a monthly subscription you will have unlimited access to all
                                    Physique 57 online workouts for a period of 31 days. The subscription is on-going
                                    and will renew automatically at the end of the term.</p>
                            </div>
                        </li>
                        <li>
                            <h5 class="question">How do I manage my monthly subscription?<a class="arrow" href="#"><img
                                        src="/themes/basic/images/global/down-arrow.svg"
                                        width="13"></a>
                            </h5>
                            <div class="answer">
                                <p>You can very easily cancel your active subscription on the My Account page by
                                    clicking the Cancel Subscription button. If you have any questions or would like
                                    additional assistance, please contact <a href="/contact-us">Physique 57 Support</a>.
                                </p>
                            </div>
                        </li>
                        <li>
                            <h5 class="question">Is the 3 month subscription paid in advance?<a class="arrow" href="#"><img
                                        src="/themes/basic/images/global/down-arrow.svg"
                                        width="13"></a>
                            </h5>
                            <div class="answer">
                                <p>No, you will be billed $30 a month for the length of your subscription.</p>
                            </div>
                        </li>
                        <li>
                            <h5 class="question">Does the 3 month subscription end after 3 months?<a class="arrow" href="#"><img
                                        src="/themes/basic/images/global/down-arrow.svg"
                                        width="13"></a>
                            </h5>
                            <div class="answer">
                                <p>No, After 3 months you will continue to be billed $30 monthly until you cancel your
                                    subscription.</p>
                            </div>
                        </li>
                        <li>
                            <h5 class="question">What happens when my 3 months are up?<a class="arrow" href="#"><img
                                        src="/themes/basic/images/global/down-arrow.svg"
                                        width="13"></a>
                            </h5>
                            <div class="answer">
                                <p>After 3 months you will continue to be billed $30 monthly until you cancel your
                                    subscription.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="category">
                <div class="lockout">
                    <h5 class="title">PHYSIQUE 57 ON DEMAND PLATFORM</h5>
                    <ul class="questions">
                        <li>
                            <h5 class="question">How does the on-demand video platform work?
                                <a class="arrow" href="#"><img src="/themes/basic/images/global/down-arrow.svg" width="13"></a></h5>
                            <div class="answer">
                                <p>Similar to pay per view movies on cable TV, our on-demand video platform allows you
                                    to access an online workout for a certain period of time. You can pay per day and
                                    access content for 24 hours or pay for unlimited access by purchasing a monthly
                                    subscription. If you’re a first time subscriber, we offer a week-long free trial.</p>
                            </div>
                        </li>
                        <li>
                            <h5 class="question">What do I need to access the online workouts?<a class="arrow" href="#"><img src="/themes/basic/images/global/down-arrow.svg" width="13"></a>
                            </h5>
                            <div class="answer">
                                <p>Once you purchase the on-demand workout video, you will have instant access to the
                                    internet video stream. The video plays automatically in your browser from the device
                                    you purchase it on. You will need a computer, tablet or smartphone to access the
                                    videos. You must have a proper broadband connection for the video to play.</p>
                            </div>
                        </li>
                        <li>
                            <h5 class="question">Can the workout videos be downloaded?<a class="arrow" href="#"><img
                                        src="/themes/basic/images/global/down-arrow.svg" width="13"></a>
                            </h5>

                            <div class="answer">
                                <p>Our online workouts are currently not available for download. We do offer <a href="http://physique57.com/shop/dvds/">workout
                                    DVDs</a> if you prefer to purchase and own a selection of our workouts.</p>
                            </div>
                        </li>
                        <li>
                            <h5 class="question">How often will new workout videos be posted?<a class="arrow" href="#"><img
                                        src="/themes/basic/images/global/down-arrow.svg" width="13"></a>
                            </h5>

                            <div class="answer">
                                <p>We plan to introduce new online workouts several times a year.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="category">
                <div class="lockout">
                    <h5 class="title">THE VIDEO ON DEMAND DIFFERENCE</h5>
                    <ul class="questions">
                        <li>
                            <h5 class="question">How are these online workouts different from in studio classes?<a class="arrow"
                                                                                             href="#"><img src="/themes/basic/images/global/down-arrow.svg" width="13"></a></h5>

                            <div class="answer">
                                <p>We’ve introduced new 10- minute options and 30-minute options that you’re able to do
                                    at home or on the road using less equipment than we do in studio while still
                                    achieving the same fitness results.</p>
                            </div>
                        </li>
                        <li>
                            <h5 class="question">How are these online workouts different from the dvds?<a class="arrow" href="#"><img src="/themes/basic/images/global/down-arrow.svg" width="13"></a></h5>

                            <div class="answer">
                                <p>Our DVDs feature our classic, signature Physique 57 workouts and targeted boosters.
                                    We’ve adapted all of our DVD content to be accessible online so that you won’t have
                                    to worry about packing DVDs to take with you on your business trip or wherever life
                                    may lead you. Our new online classes feature the latest workouts created by Tanya
                                    Becker and her talented team of instructors to complement the signature Physique 57
                                    technique and offer exciting new choreography so that working out with us always
                                    stays fresh.</p>
                            </div>
                        </li>
                        <li>
                            <h5 class="question">How often should I do the online workouts?<a
                                    class="arrow" href="#"><img src="/themes/basic/images/global/down-arrow.svg" width="13"></a></h5>

                            <div class="answer">
                                <p>For best results, we suggest taking 4 Physique 57 classes per week—whether those are
                                    in our studios, on DVD or online. If you already belong to a gym or studio and enjoy
                                    adding Physique 57 workouts to complement your existing routine, we suggest
                                    incorporating at least 2 Physique 57 workouts per week.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="category">
                <div class="lockout">
                    <h5 class="title">GETTING STARTED WITH VOD SYSTEM</h5>
                    <ul class="questions">
                        <li>
                            <h5 class="question">Why doesn’t my mindbody login information work?<a class="arrow"
                                                                                                  href="#"><img src="/themes/basic/images/global/down-arrow.svg" width="13"></a></h5>

                            <div class="answer">
                                <p>MindBody usernames and passwords only work for their designated studio locations, so
                                    a separate user account is needed to access online workouts and make purchases in
                                    the Shop section of Physique57.com (For example, if you take classes in NY or any
                                    other location, you would need to create another username and password to access
                                    online workouts at physique57.com). To set up a new account, please visit
                                    <a href="https://physique57.com/my-account/">physique57.com/my-account</a>.</p>
                            </div>
                        </li>
                        <li>
                            <h5 class="question">What are the username and password requirements of a Physique 57 account?<a class="arrow" href="#"><img src="/themes/basic/images/global/down-arrow.svg" width="13"></a>
                            </h5>

                            <div class="answer">
                                <p>Your username is your email address. Passwords are case sensitive and must be at
                                    least 6 characters in length.</p>
                            </div>
                        </li>
                        <li>
                            <h5 class="question">Is my credit card secure?<a class="arrow" href="#"><img src="/themes/basic/images/global/down-arrow.svg"
                                                                       width="13"></a>
                            </h5>

                            <div class="answer">
                                <p>Yes! Your profile, balance and access information are securely stored on the Physique
                                    57 Support Center servers. All actual transactions are done on the Physique 57
                                    Support Center servers using 256bits SSL encryption, and payment processing is
                                    compliant with all PCI standards.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="category">
                <div class="lockout">
                    <h5 class="title">ADDITIONAL SUPPORT</h5>
                    <ul class="questions">
                        <li>
                            <h5 class="question">Who do I contact if I have a question about the online workouts?<a class="arrow"
                                                                                           href="#"><img
                                        src="/themes/basic/images/global/down-arrow.svg" width="13"></a></h5>

                            <div class="answer">
                                <p>Click <a href="/contact-us">here</a> to contact Physique 57 Support or click the Contact
                                    Us button below.<br/>
                                    Our Physique 57 Support Hours are<br/>
                                    Mon-Fri: 9am EST – 6pm EST<br/>
                                    Sat-Sun: 11am EST – 5pm EST<br/>
                                    Support team members usually respond to requests within 24 hours, although the
                                    actual response time is often less than 5 hours during the posted business hours.</p>
                            </div>
                        </li>
                    </ul>
                    <div class="contact-faq">
                        <a href="/contact-us" class="btn blue">contact us</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>
