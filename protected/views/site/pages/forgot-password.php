<?php $this->bodyClass = 'account forgot-password short-nav short-nav-perm'; ?>

<section>
    <div class="lockout">
        <div class="side-lines">
            <h1>forgot password</h1>
        </div>
    </div>
    <div class="content">
        <div class="lockout">
            <h4>reset your password</h4>
            <hr>
            <div class="text">
                <p>We will send you an email with instructions on how to reset your password.</p>
            </div>
			<?php $form = $this->beginWidget('ActiveForm', [
				'action'      => Yii::app()->createUrl('/user/users/forgot'),
				'htmlOptions' => [
					'data-validate'   => '',
					'data-submit-btn' => '#submit-btn',
				],
			]); ?>
            <div class="inputs">
                <div class="input-item">
                    <label>EMAIL ADDRESS</label>
					<?php echo $form->textField($model, 'email', ['data-validate' => 'min:5|email']); ?>
					<?php echo $form->error($model, 'email'); ?>
                </div>
            </div>
            <div class="buttons">
                <input type="submit" class="btn blue" id="submit-btn" value="email me"/>
            </div>
			<?php $this->endWidget(); ?>
        </div>
    </div>
</section>
