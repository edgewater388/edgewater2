<?php $this->bodyClass = 'home-logged-in'; ?>

<?php $this->renderPartial('//partials/crazy-egg-tracking'); ?>

<?php $this->renderPartial('//partials/home-logged-in/hero', [
		'stream'            => Stream::getStream(),
		'stream_update_url' => $this->createUrl('stream/status'),
	]
); ?>

<?php $this->renderPartial('//partials/home-logged-in/legend'); ?>
<?php $this->renderPartial('//partials/home-logged-in/workout-regimens', ['regimens' => $regimens]); ?>
<?php $this->renderPartial('//partials/home-logged-in/sample-mashups', ['playlists' => $playlists, 'new_videos' => $new_videos]); ?>
<?php $this->renderPartial('//partials/home-logged-in/workouts', [
	'videos_by_duration' => $videos_by_duration,
	'live_classes'       => $live_classes,
]); ?>
<?php $this->renderPartial('//partials/home-logged-in/bring-ball-home'); ?>

	<!-- invisible div used for tutorial deeplink -->
	<div data-modal="#tutorial" style="display: none;"></div>

<?php //$this->renderPartial('//modals/video'); ?>
<?php $this->renderPartial('//modals/video-popup'); ?>
<?php $this->renderPartial('//modals/mashup'); ?>
<?php $this->renderPartial('//modals/tutorial'); ?>

<?php if (Yii::app()->request->getParam('tutorial') == '1'): ?>
	<?php $this->renderPartial('//partials/google-start-membership'); ?>
	<?php $this->renderPartial('//partials/facebook-pixel-purchase'); ?>

	<?php if (Yii::app()->user->membership && Yii::app()->user->membership->trial_end_date >= date('Y-m-d')): ?>
        <script>
          woopra.track("free_trial_started", {
            'Plan ID': "<?php echo Yii::app()->user->membership->plan_id; ?>",
            'Plan': "<?php echo Yii::app()->user->membership->plan['title']; ?>",
            'Total': "$<?php echo Yii::app()->user->membership->total; ?>",
            'Trial End Date': "<?php echo Yii::app()->user->membership->trial_end_date; ?>"
          });
        </script>
	<?php endif; ?>
<?php endif; ?>