<?php $this->bodyClass = 'account manage-profile'; ?>

<section>
    <div class="lockout">
        <div class="side-lines">
            <h1>my account</h1>
        </div>
    </div>
    <div class="content">
        <div class="lockout">
            <h4>manage profile</h4>
            <hr>
            <?php $form=$this->beginWidget('ActiveForm', array(
                'action'=>Yii::app()->createUrl('/user/profile/manage'),
                'htmlOptions'=>array(
                    'data-validate'=>'',
                    'data-submit-btn'=>'#submit-btn',
                    'enctype' => 'multipart/form-data'
                ),
            )); ?>
            <?php echo $form->errorSummary($model); ?>
            <img id="profile-pic" src="<?php echo Yii::app()->user->profile->getThumbnailUrl('large'); ?>">
            <div id="upload-btn">
                <label class="myLabel">
                    <?php echo $form->fileField($model, 'photo', array('required'=>'')); ?>
                    <!-- <input type="file" required/> -->
                    <div class="icon"></div>
                    <span>SELECT NEW IMAGE</span>
                </label>
            </div>
<!--            <input type="submit" class="btn blue" id="submit-btn" value="upload">-->

            <?php $this->endWidget(); ?>
            <hr class="short">
            <div class="current-name">current name</div>
            <div class="name"><?php echo Yii::app()->user->profile->name; ?></div>
            <?php $form=$this->beginWidget('ActiveForm', array(
                'action'=>Yii::app()->createUrl('/user/profile/manage'),
                'htmlOptions'=>array(
                    'data-validate'=>'',
                    'data-submit-btn'=>'#submit-btn',
                ),
            )); ?>
                <?php echo $form->errorSummary($model); ?>
                <div class="inputs">
                    <div class="input-item">
                        <label>name</label>
                        <!-- <input type="text" data-validate="min:4"/> -->
                        <?php echo $form->textField($model, 'new_name', array('data-validate'=>'min:4')); ?>
                    </div>
                    <div class="input-item">
                        <label for="fitness_level">Date of birth</label>
                        <?php echo $form->textField($model, 'birthday', array('class' => 'datepicker')); ?>
                    </div>
                    <div class="input-item">
                        <label for="fitness_level">Gender</label>
                        <?php echo $form->dropDownList($model, 'gender', $genders); ?>
                    </div>
                    <div class="input-item">
                        <label for="weight">Weight (lb.)</label>
                        <?php echo $form->textField($model, 'weight'); ?>
                    </div>
                    <div class="input-item">
                        <label for="height">Height (feet, e.g. "5.10")</label>
                        <?php echo $form->dropDownList($model, 'height[0]', array_combine(range(4,6), range(4,6)),
                                array('options' => array($model->height[0] => array('selected'=>true)))); ?>
                        <?php echo $form->dropDownList($model, 'height[1]', array_combine(range(0,11), range(0,11)),
                                array('options' => array($model->height[1] => array('selected'=>true)))); ?>
                    </div>
                    <div class="input-item">
                        <label for="fitness_level">Fitness Level</label>
                        <?php echo $form->dropDownList($model, 'fitness_level', $fitnessLevels); ?>
                    </div>
                </div>
                <div class="buttons">
                    <input type="submit" class="btn blue" id="submit-btn" value="save">
                    <a class="btn" href="/account">cancel</a>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</section>
