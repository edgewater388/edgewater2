<?php $this->bodyClass = 'mashup-maker'; ?>

<section id="video-thumbs" class="thumbs-section">
    <div id="add-video-to-mashup">
        <h1>add a video to your mashup</h1>
        <div class="btn white" data-modal="#tutorial">what's a mashup</div>
    </div>
    <div id="mashup-video-editor">
        <div class="title full-width">
            <h1 id="mashup-title"></h1>
            <div class="edit" data-modal="#mashup-name">EDIT</div>
            <div class="btn white" data-modal="#tutorial">view tutorial</div>
        </div>
        <div class="videos">
            <ul class="video-list" id="list-title-bar">
                <li class="titles">
                    <div class="play">
                        <div class="icon"></div>
                    </div>
                    <div class="title">WORKOUT</div>
                    <div class="instructor">INSTRUCTOR</div>
                    <div class="type">TYPE</div>
                    <div class="duration">DURATION</div>
                    <div class="remove"></div>
                    <div class="drag">
                        <div class="icon"></div>
                    </div>
                </li>
            </ul>
            <ul class="video-list" id="video-list"></ul>
            <div class="buttons">
                <div class="btn blue large" id="mashup-preview" data-modal="#mashup">watch and save</div>
                <a href="/my-workouts">
                    <div class="btn blue large" id="mashup-finish">save</div>
                </a>
            </div>
        </div>
    </div>

    <div class="alerts" style="text-align: left;">
        <div class="lockout">
            <div class="inner">
                <?php Flash::displayFlashes(); ?>
            </div>
        </div>
    </div>

    <div class="full-width video-pagination">
        <header>
            <h4 class="title">select your favorite videos to create a mashup!</h4>
            <div class="dropdowns">
                <form action="">
                        <span class="select-wrap">
                            <select class="" name="type" id="type-dd">
                                <option value="">Type</option>
                                <option value="strength">Strength</option>
                                <option value="cardio">Cardio</option>
                                <option value="restorative">Restorative</option>
                            </select>
                        </span>
                        <span class="select-wrap">
                            <select class="" name="level" id="level-dd">
                                <option value="">Level</option>
                                <option value="1">Level 1</option>
                                <option value="2">Level 2</option>
                                <option value="3">Level 3</option>
                            </select>
                        </span>
                            <span class="select-wrap">
                            <select class="" name="duration" id="duration-dd">
                                <option value="">Duration</option>
                                <option value="10">10 min</option>
                                <option value="15">15 min</option>
                                <option value="30">30 min</option>
                                <option value="60">60 min</option>
                            </select>
                        </span>
                    </span>
                </form>
            </div>
        </header>
        <ul class="video-thumbs" data-match-tallest="li" id="video-thumbs">
            <?php foreach ($videos as $video): ?>
                <li>
                    <div class="thumb" data-modal="#video" data-video="<?php echo $video->id; ?>">
                        <img src="<?php echo $video->thumbUrl; ?>">
                        <div class="icon time"><?php echo $video->roundedMinutes; ?> min</div>
                        <div class="title-icon">
                            <div class="title"><?php echo $video->title; ?></div>
                            <?php if ($video->strength): ?>
                                <div
                                    class="icon strength white <?php echo Video::getLevelWord($video->strength); ?>"></div>
                            <?php endif; ?>
                            <?php if ($video->cardio): ?>
                                <div class="icon cardio white <?php echo Video::getLevelWord($video->cardio); ?>"></div>
                            <?php endif; ?>
                            <?php if ($video->restorative): ?>
                                <div
                                    class="icon restorative white <?php echo Video::getLevelWord($video->restorative); ?>"></div>
                            <?php endif; ?>
                        </div>
                        <div class="add-favorite" title="Add to favorites">
                            <div class="icon"></div>
<!--                            <h5>add to favorites</h5>-->
                            <div class="checkmark">
                                <img src="/themes/basic/images/global/icon-checkmark.png">
                            </div>
                        </div>
                    </div>
                    <div class="btn add-to-mashup">add to my mashup</div>
                </li>
            <?php endforeach; ?>
        </ul>
        <?php $this->renderPartial('//partials/no-results'); ?>
    </div>
    <div id="see-more-videos">
        <div class="blue-dot-btn gray large see-more-videos">
            <a href="#" id="see-more-btn">
                <div class="text">SEE MORE VIDEOS</div>
                <div class="loading">LOADING VIDEOS</div>
                <div class="border tl"></div>
                <div class="border tr"></div>
                <div class="border br"></div>
                <div class="border bl"></div>
            </a>
        </div>
    </div>
</section>

<!-- invisible div used for tutorial deeplink -->
<div data-modal="#tutorial" style="display: none;"></div>

<?php $this->renderPartial('//modals/tutorial'); ?>
<?php $this->renderPartial('//modals/video'); ?>
<?php $this->renderPartial('//modals/mashup'); ?>
<?php $this->renderPartial('//templates/video-editor-li'); ?>

