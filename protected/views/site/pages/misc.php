<?php $this->bodyClass = 'account misc short-nav short-nav-perm'; ?>

<section>
    <div class="lockout">
        <div class="side-lines">
            <h4>LOREM IPSUM</h4>
        </div>
            <h1>Maecenas porta odio</h1>

    </div>
    <div class="content">
        <div class="lockout">
            <p>Fusce quis est vitae nisi sodales finibus. Donec varius feugiat metus, ut mollis orci vestibulum ac.
                Vestibulum feugiat laoreet felis, vel vulputate magna bibendum placerat. Aliquam non augue non massa
                pharetra gravida sit amet sit amet metus. Cras ultricies ipsum urna, nec facilisis risus molestie eget.
                Nunc vitae leo felis. Morbi varius diam sagittis sodales viverra. Donec condimentum, mauris eu
                consectetur lobortis, ante lorem ullamcorper elit, at luctus mauris arcu et leo. Nunc eleifend
                condimentum aliquet. Nam eu orci vitae eros molestie tincidunt imperdiet non nunc. Quisque molestie
                risus eget erat consectetur aliquet. Vestibulum ut nibh ac enim pellentesque pharetra. In sapien risus,
                vehicula quis ultrices vel, pulvinar id arcu. Nullam vel mi quis nibh dignissim fringilla molestie
                varius elit. Maecenas diam mauris, maximus et ultricies eu, maximus at quam. Quisque tincidunt sapien
                metus, non cursus eros ultrices vitae.</p>
            <div class="misc-img"><img src="/themes/basic/images/misc/misc1.jpg"</div>
            <p>Fusce quis est vitae nisi sodales finibus. Donec varius feugiat metus, ut mollis orci vestibulum ac.
                Vestibulum feugiat laoreet felis, vel vulputate magna bibendum placerat. Aliquam non augue non massa
                pharetra gravida sit amet sit amet metus. Cras ultricies ipsum urna, nec facilisis risus molestie eget.
                Nunc vitae leo felis. Morbi varius diam sagittis sodales viverra. Donec condimentum, mauris eu
                consectetur lobortis, ante lorem ullamcorper elit, at luctus mauris arcu et leo. Nunc eleifend
                condimentum aliquet. Nam eu orci vitae eros molestie tincidunt imperdiet non nunc. Quisque molestie
                risus eget erat consectetur aliquet. Vestibulum ut nibh ac enim pellentesque pharetra. In sapien risus,
                vehicula quis ultrices vel, pulvinar id arcu. Nullam vel mi quis nibh dignissim fringilla molestie
                varius elit. Maecenas diam mauris, maximus et ultricies eu, maximus at quam. Quisque tincidunt sapien
                metus, non cursus eros ultrices vitae.</p>
            <div class="misc-img"><img src="/themes/basic/images/misc/misc2.jpg"</div>
        </div>
    </div>
</section>
