<?php $this->bodyClass = 'my-workouts'; ?>

<header class="header">
    <div class="lockout">
        <div class="side-lines">
            <h1>my workouts</h1>
        </div>
        <ul class="buttons">
            <li>
                <div class="blue-dot-btn gray">
                    <a href="/mashup-maker">
                        CREATE A MASHUP
                        <div class="border tl"></div>
                        <div class="border tr"></div>
                        <div class="border br"></div>
                        <div class="border bl"></div>
                    </a>
                </div>
            </li>
            <li>
                <div class="blue-dot-btn gray">
                    <a href="#" data-modal="#tutorial">
                        WHAT'S A MASHUP
                        <div class="border tl"></div>
                        <div class="border tr"></div>
                        <div class="border br"></div>
                        <div class="border bl"></div>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</header>
<section id="mashup-videos" class="thumbs-section">
    <div class="full-width">
        <header>
            <div class="side-lines right-line-only">
                <h4>mashup videos</h4>
            </div>
        </header>
        <div class="none-available">
            <h5>You haven't created any mashups yet.</h5>
            <a href="/mashup-maker" class="btn blue">create a mashup</a>
        </div>
        <ul class="video-thumbs" data-match-tallest="li">
            <?php foreach ($playlists as $playlist): ?>
                <li class="<?php echo ($playlist->user_id == Yii::app()->user->id) ? 'mine' : 'notmine'; ?>">
                    <div class="thumb" data-modal="#mashup" data-playlist="<?php echo $playlist->id; ?>">
                        <img src="<?php echo $playlist->thumbUrl; ?>">
                        <div class="title-icon">
                            <div class="title"><?php echo $playlist->title; ?></div>
                        </div>
                        <?php if ($playlist->user_id == Yii::app()->user->id): ?>
                            <div class="edit-delete">
                                <div class="delete" title="delete" data-modal="#confirm-mashup-delete"
                                     data-confirm-mashup-delete></div>
                                <div class="edit" title="edit"></div>
                            </div>
                        <?php endif; ?>
                        <?php if ($playlist->user_id != Yii::app()->user->id): ?>
                            <div class="edit-delete">
                                <div class="unsubscribe" title="remove from my workouts"></div>
                            </div>
                        <?php endif; ?>
                        <div class="p57-mashup"></div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>

<section id="favorite-videos" class="thumbs-section">
    <div class="full-width">
        <header>
            <div class="side-lines right-line-only">
                <h4>favorite videos</h4>
            </div>
        </header>
        <div class="none-available">
            <h5>You haven't marked any videos as favorites yet.</h5>
        </div>
        <ul class="video-thumbs" data-match-tallest="li">
            <?php foreach ($videos as $video): ?>
                <li>
                    <div class="thumb" data-modal="#video" data-video="<?php echo $video->id; ?>">
                        <img src="<?php echo $video->thumbUrl; ?>">
                        <div class="title-icon">
                            <div class="title"><?php echo $video->title; ?></div>
                            <?php if ($video->strength): ?>
                                <div class="icon strength white <?php echo Video::getLevelWord($video->strength); ?>"
                                     title="strength"></div>
                            <?php endif; ?>
                            <?php if ($video->cardio): ?>
                                <div class="icon cardio white <?php echo Video::getLevelWord($video->cardio); ?>"
                                     title="cardio"></div>
                            <?php endif; ?>
                            <?php if ($video->restorative): ?>
                                <div
                                    class="icon restorative white <?php echo Video::getLevelWord($video->restorative); ?>"
                                    title="restorative"></div>
                            <?php endif; ?>
                        </div>
                        <div class="edit-delete">
                            <div class="delete" title="delete"></div>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>


<?php $this->renderPartial('//modals/tutorial'); ?>
<?php $this->renderPartial('//modals/video'); ?>
<?php $this->renderPartial('//modals/mashup'); ?>
<?php $this->renderPartial('//modals/confirm-mashup-delete'); ?>
