<?php $this->bodyClass = 'payment dark-header xno-top-padding'; ?>

<?php $this->renderPartial('//partials/crazy-egg-tracking'); ?>
<?php $this->renderPartial('//partials/facebook-pixel-lead'); ?>

<style>
    body.payment #payment {
        padding-top: 42px;
    }

    @media (min-width: 701px) {
        body.payment #payment {
            padding-top: 72px;
        }
    }
</style>

<section id="payment">

    <div class="lockout">
        <div class="side-lines payment">
            <h1>sign up</h1>
        </div>
        <h4 class="first-week">Your first week is free! Choose the plan that works best for you.</h4>
    </div>
    <!--    <div id="hero">
        <div class="mobile-image" data-mobile="/themes/basic/images/payment/hero-girl-mobile.jpg"
             data-desktop="/themes/basic/images/payment/hero-girl-desktop.jpg">
        </div>
        <div class="left">
            <div class="inner">
                <div class="title">
                    <h3><span class="dollar">$</span><?php /*echo $plans[0]['amount']; */ ?> <span class="term">MONTHLY</span></h3>

                    <div class="free">First week free</div>
                </div>
                <div class="side-lines">&nbsp;</div>
                <div class="body-copy">
                    <p>Work out using the device of your choice.</p>
                    <p> Stream content via your laptop, TV, phone or tablet.</p>
                    <p>Unlimited viewing access.</p>
                    <p>Cancel anytime.</p>
                </div>
            </div>
        </div>
        <div class="right" data-mobile="/themes/basic/images/payment/hero-girl-mobile.jpg"
             data-desktop="/themes/basic/images/payment/hero-girl-desktop.jpg">
        </div>
    </div>-->

	<?php $form = $this->beginWidget('ActiveForm', array(
		'action' => Yii::app()->createUrl('/customer/create'),
		'htmlOptions' => array(
			'id'=>'signup-form',
			'data-validate' => '',
		),
	)); ?>

    <div id="plans">
		<?php if ($selected_plan_id !== '2'): ?>
            <div class="lockout" data-match-tallest=".plan|TABLET">
                <div class="plan <?php echo ($selected_plan_id === '7') ? 'active' : ''; ?>">
                    <div class="photo">
                        <img src="/themes/basic/images/payment/plan1-photo.jpg" alt="">

                    </div>
                    <div class="bottom">
                        <div class="title">
                            <h3 class="move-up">$<span class="number">29.95</span> MONTHLY</h3>
                        </div>
                        <div class="side-lines white">&nbsp;</div>
                        <div class="body-copy">
                            <p>First week free.</p>
                            <p>Work out using the device of your choice.</p>
                            <p> Stream content via your laptop, TV, phone or tablet.</p>
                            <p>Unlimited viewing access.</p>
                            <p>Cancel anytime.</p>
                        </div>
                    </div>
                </div>
                <h5 class="or">OR</h5>
                <div class="plan <?php echo ($selected_plan_id === '8') ? 'active' : ''; ?>">
                    <div class="photo">
                        <img src="/themes/basic/images/payment/plan2-photo.jpg" alt="">
                    </div>
                    <div class="bottom">
                        <div class="title">
                            <h3 class="move-up">$<span class="number">19.95</span> MONTHLY</h3>
                            <h5 class="three-month">With a 3 month commitment</h5>
                        </div>
                        <div class="side-lines white">&nbsp;</div>
                        <div class="body-copy">
                            <p>First week free.</p>
                            <p>Work out using the device of your choice.</p>
                            <p> Stream content via your laptop, TV, phone or tablet.</p>
                            <p>Unlimited viewing access.</p>
                            <p>Save nearly 50% per month.</p>
                            <a href="/faq">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
		<?php else: ?>
            <div class="lockout">
                <div class="plan <?php echo ($selected_plan_id === '2') ? 'active' : ''; ?>" style="margin: 0 auto;">
                    <div class="photo">
                        <img src="/themes/basic/images/payment/plan1-photo.jpg" alt="">
                    </div>
                    <div class="bottom">
                        <div class="title">
                            <h3 class="move-up">$<span class="number">5</span> Daily</h3>
                            <h5 class="three-month">Drop In Membership</h5>
                        </div>
                        <div class="side-lines white">&nbsp;</div>
                        <div class="body-copy">
                            <p>Work out using the device of your choice.</p>
                            <p>Stream content via your laptop, TV, phone or tablet.</p>
                            <p>Unlimited viewing access for 24 hours.</p>
                        </div>
                    </div>
                </div>
            </div>
		<?php endif; ?>
    </div>
    <div id="promo-code">
        <div class="lockout">
			<?php echo $form->errorSummary($model); ?>

            <h4>HAVE A PROMOTIONAL CODE?</h4>
            <hr>
            <div class="inputs">
                <div class="row">
                    <div class="input-item code">
						<?php /*
                        <input type="text" maxlength="32" data-validate="min:5" placeholder="Enter 5 Digit Code" />
                        */ ?>
						<?php echo $form->textField($model, 'promo_code', array('maxlength'=>32, 'placeholder'=>'Enter Promotional Code', 'style'=>'background-image: none;')); ?>

						<?php if ($promoCode): ?>
                            <div>
                                <br>
                                <p>
                                    Promotion: <span style="font-weight: 700;"><?php echo $promoCode->description; ?></span>
                                </p>
                            </div>
						<?php endif; ?>
                    </div>
                    <div class="input-item redeem">
                        <a href="#" class="btn redeem-btn" id="redeem-btn">Redeem</a>
                        <div class="tooltip"><?php echo $promoCode ? $promoCode->description : null; ?></div>
                    </div>

                    <script>
                      jQuery(document).ready(function($) {
                        $('#redeem-btn').unbind('click').bind('click', function(e) {
                          $('<input />').attr('type', 'hidden').attr('name', 'verify_promo_code').attr('value', '1').appendTo('#signup-form');
                          $('form#signup-form').submit();
                          return false;
                        });
                      });
                    </script>
                </div>
            </div>
        </div>
    </div>
    <div id="payment-details">
        <div class="lockout">
			<?php echo Html::radioButtonList('Plan[id]', $selected_plan_id, Html::listData($plans, 'id', 'title')); ?>

			<?php if ($model->scenario == 'complimentary'): ?>
                <h4>Your membership is on us!</h4>
                <hr>
				<?php if ($selected_plan_id != '2'): ?>
                    <div class="top-text">
                        <ol>
                            <li>Your membership will remain active until the end of the promotion.</li>
                            <li>Cancel online anytime.</li>
                            <li>Unlimited streaming for 1 user.</li>
                        </ol>
                    </div>
                    <br><br>
				<?php endif; ?>

                <div class="inputs">
                    <div class="row">
                        <div class="input-item first">
                            <label>FIRST NAME</label>
							<?php echo $form->textField($model, 'first_name', array('data-validate' => 'min:2')); ?>
                        </div>
                        <div class="input-item last">
                            <label>LAST NAME</label>
							<?php echo $form->textField($model, 'last_name', array('data-validate' => 'min:2')); ?>
                        </div>
                    </div>
                </div>
			<?php else: ?>
                <h4>ENTER YOUR PAYMENT DETAILS</h4>
                <hr>
                <div class="top-text">
                    <ol>
                        <li>You will not be charged until after your free trial ends
                            on <?php echo date('m/d/y', strtotime('+1 week')); ?>.
                        </li>
                        <li>No commitments, cancel online anytime with the $29.95 monthly plan.</li>
                        <li>Unlimited streaming for 1 user.</li>
                    </ol>
                </div>


                <div class="credit-card">
                    <p>CREDIT CARD</p>
                    <div class="cards"></div>
                </div>

				<?php echo $form->errorSummary($model); ?>
                <div class="inputs">
                    <div class="row">
                        <div class="input-item first">
                            <label>FIRST NAME <span class="must-match"><span
                                            class="darker-blue">*</span>Must match card</span></label>
							<?php echo $form->textField($model, 'first_name', array('data-validate' => 'min:2')); ?>
                        </div>
                        <div class="input-item last">
                            <label>LAST NAME <span class="must-match"><span
                                            class="darker-blue">*</span>Must match card</span></label>
							<?php echo $form->textField($model, 'last_name', array('data-validate' => 'min:2')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-item street first">
                            <label>STREET ADDRESS</label>
							<?php echo $form->textField($model, 'street', array('data-validate' => 'min:2')); ?>
                        </div>
                        <div class="input-item zip">
                            <label>ZIP CODE</label>
							<?php echo $form->textField($model, 'zipcode', array('maxlength' => '12')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-item expiration-month" style="width: 31.81818%">
                            <label>COUNTRY</label>
							<?php echo $form->dropDownList($model, 'country', Location::countries()); ?>
                            <div class="down-arrow"><img src="/themes/basic/images/global/down-arrow.svg"></div>
                        </div>

                        <script>
                          jQuery(document).ready(function($) {

                            updateZipcode();

                            $('select[name="Customer[country]"]').bind('change', function(e) {
                              updateZipcode();
                            });

                            function updateZipcode()
                            {
                              if ($('select[name="Customer[country]"]').val() == 'US')
                                $('.input-item.zip label').html('Zip Code');
                              else
                                $('.input-item.zip label').html('Postal Code');
                            }
                          });
                        </script>
                    </div>
                    <div class="row">
                        <div class="input-item card lock">
                            <label>CARD NUMBER</label>
							<?php echo $form->textField($model, 'new_card_number', array('data-validate' => 'min:13')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-item expiration-month">
                            <label>EXPIRATION DATE</label>
							<?php echo $form->dropDownList($model, 'card_month', Time::months(), array('id' => 'expiry-month', 'data-validate' => 'min:1')); ?>
                            <div class="down-arrow"><img src="/themes/basic/images/global/down-arrow.svg"></div>
                        </div>
                        <div class="input-item expiration-year">
                            <label>&nbsp;</label>
							<?php echo $form->dropDownList($model, 'card_year', Time::years(), array('id' => 'expiry-year', 'data-validate' => 'min:1')); ?>
                            <div class="down-arrow"><img src="/themes/basic/images/global/down-arrow.svg"></div>
                        </div>
                        <div class="input-item security-code lock">
                            <label>SECURITY CODE</label>
							<?php echo $form->textField($model, 'card_security', array('data-validate' => 'min:2', 'maxlength' => 4)); ?>
                        </div>
                        <div class="input-item qmark">
                                <span class="rollover">
                                    <img src="/themes/basic/images/account/credit-cards-hover@2x.png" width="276">
                                </span>
                        </div>
						<?php /*
                            <div class="input-item coupon">
                                <input type="text" maxlength="30" placeholder="Coupon Code"/>
                            </div>
                            <div class="input-item apply-coupon">
                                <a class="btn" href="#">apply coupon</a>
                            </div>
                            */ ?>
                    </div>
                </div>

                <div class="legal plan57" style="<?php echo ($selected_plan_id === '7') ? 'display: block;' : 'display: none;'; ?>">
					<?php $this->renderPartial('//partials/legal-plan57'); ?>
                </div>
                <div class="legal plan30" style="<?php echo ($selected_plan_id === '8') ? 'display: block;' : 'display: none;'; ?>">
                    <p>
                        Your Physique 57 VOD membership, which starts with a 30 day free trial offer,
                        will begin when you click Start Membership.
                        Simply cancel anytime within your trial period and you will not be charged. To cancel,
                        go to "Your Account" and click on "Cancel Membership."
                    </p>
					<?php $this->renderPartial('//partials/legal-plan30'); ?>
                </div>
                <div class="legal plan5" style="<?php echo ($selected_plan_id === '2') ? 'display: block;' : 'display: none;'; ?>">
                    <p>Your Physique 57 VOD membership will begin when you click Continue. By clicking Continue you will
                        have access to Physique 57 VOD for 24 hours.</p>
                </div>
			<?php endif; ?>


            <div class="checkbox-wrap">
                <div class="left">
                    <!-- <input type="checkbox" name="over_18" id="over_18" class="optional" value="1"/> -->
					<?php echo $form->checkbox($model, 'over_18', array('id' => 'over_18')); ?>
                </div>
                <div class="right">
                    <label class="checkbox" for="over_18">
                        <div
                                class="main-checkbox">I am over 18, and I agree to the above conditions and the <a
                                    href="http://physique57.com/terms-and-conditions/" target="_blank">Terms of
                                Use</a> and <a href="http://physique57.com/privacy-legal/" target="_blank">Privacy /
                                Cookies</a>.
                        </div>
                    </label>
                </div>
            </div>
            <div class="buttons">
				<?php if ($selected_plan_id == '2'): ?>
                    <input type="submit" class="btn blue big active" value="START DAILY MEMBERSHIP" id="start-membership-btn3"/>
				<?php else: ?>
                    <input type="submit" class="btn blue big <?php echo ($selected_plan_id === '7') ? 'active' : ''; ?>"
                           value="START MONTHLY MEMBERSHIP" id="start-membership-btn"/>
                    <input type="submit" class="btn blue big <?php echo ($selected_plan_id === '8') ? 'active' : ''; ?>"
                           value="START 3 MONTH MEMBERSHIP" id="start-membership-btn2"/>
				<?php endif; ?>
            </div>
            <div class="secure">
                <img src="/themes/basic/images/global/icon-lock@2x.png" width="26" alt="Secure">
                <p>Secure Server</p>
                <p class="tell">Tell me more<span class="rollover">
                                We take your privacy seriously We safeguard your payment information (such as credit card number)
                                with SSL (Secure Sockets Layer) when sent over the Internet. SSL encrypts personal information,
                                including your name and payment information, so that it cannot be read in transit by a third party.
                                We also store your payment information in an encrypted format.</span>
                </p>
            </div>
        </div>
    </div>
	<?php $this->endWidget(); ?>

</section>
