<?php $this->bodyClass = 'short-nav short-nav-perm workout-page'; ?>

    <header class="workout-header">
        <div class="full-width">
            <div class="side-lines white right-line-only">
                <h4>
                    <span><?= $model->title; ?></span>
                    <a href="<?= $cancel_program_url; ?>" class="btn blue">Cancel the Program</a>
                </h4>
            </div>
        </div>
    </header>

    <section class="workout-content">
        <div class="user-panel-row">
            <div class="full-width">
                <div class="user-panel">
                    <div class="user-details">
                        <ul class="details">
                            <li class="details-item">
                                <span class="details-label">Age</span>
                                <span class="details-content"><?= $user->getAge(); ?></span>
                            </li>
                            <li class="details-item">
                                <span class="details-label">Weight</span>
                                <span class="details-content"><?= $user->profile->weight; ?></span>
                            </li>
                            <li class="details-item">
                                <span class="details-label">Height</span>
                                <span class="details-content"><?= $user->profile->height; ?>"</span>
                            </li>
                            <li class="details-item">
                                <span class="details-label">Fitness Level</span>
                                <span class="details-content"><?= $user->profile->getFitnessLevel(); ?></span>
                            </li>
                        </ul>
                    </div>

                    <div class="calculator">
                        <div class="calculator-items-container">
                            <div class="calculator-item">
                                <div class="calculator-item-content">
                                    <p class="calculator-value"><?= $completed_workouts_count; ?></p>
                                </div>

                                <footer class="calculator-item-footer">
                                    <p class="calculator-value-description">Completed
                                        <span class="completed-count"></span> workouts out of <span class="total-workouts-count"><?= $total_workouts_count; ?></span>
                                    </p>
                                    <p class="calculator-value-description">Summary of <span class="total-weeks-count"><?= $total_weeks_count; ?></span> weeks workout</p>
                                </footer>
                            </div>

                            <div class="calculator-item time-item">
                                <div class="calculator-item-content">
                                    <p class="calculator-value left-count <?php  if($skipped_count > 0) {echo "left-count-more-zero"; }?>"><?= $skipped_count; ?></p>
                                </div>

                                <footer class="calculator-item-footer">
                                    <?php if ($skipped_count == 0): ?>
                                        <p>You have no skipped days.</p>
                                    <?php else: ?>
                                        <p>You have skipped days.</p>
                                        <p>Want to complete those days? <a href="#" class="is-skipped-days">Click here</a></p>
                                    <?php endif; ?>
                                </footer>
                            </div>

                            <?php
                            if (($weight_change = $user->profile->getWeightChange($program_id)) < 0) {
                                $weight_change = abs($weight_change);
                                $gained = true;
                            }
                            ?>

                            <div class="calculator-item progress-item <?= ($weight_change == 0) ? '' : 'is-weight-lost'; ?>">
                                <div class="calculator-item-content">
                                    <p class="calculator-value weight <?= isset($gained) ? 'gained' : 'lost'; ?>">
                                        <?= $weight_change; ?><span>lb.</span></p>
                                </div>

                                <footer class="calculator-item-footer">
                                    <?php if ($weight_change == 0): ?>
                                        <p class="calculator-value-description">Weight didn't change</p>
                                    <?php else: ?>
                                        <p class="calculator-value-description">Total weight <?= isset($gained) ? 'gained' : 'lost'; ?></p>
                                    <?php endif; ?>
                                    <p class="calculator-value-description">Current weight <span class="weight-info"><?= $user->profile->weight; ?></span> lb.</p>
                                </footer>
                            </div>
                        </div>
                    </div><!-- / .calculator -->
                </div>
            </div>
        </div>

        <div class="full-width">
            <div class="info-panel">
                <div class="column">
                    <div class="week-indicator">
                        <p><?= strtoupper(UserRegimenPlan::weekOrdinal($current_week_num)); ?></p>
                        <p>week</p>
                    </div>

                    <h4 class="heading">Current Workout Calendar<span class="heading-decor"></span></h4>
                </div>

                <div class="column-fullwidth">
                    <div class="workouts-counter">
                        <i class="icon-star"></i>
                        <span><span class="left-count"><?= $days_left_count; ?></span> days left</span>
                    </div>

                    <div class="btn-toolbar">
                        <button type="button" class="btn btn-white btn-refresh">
                            Refresh
                        </button>
                        <button type="button" class="btn btn-white btn-view-all">
                            View All Weeks
                        </button>
                    </div>
                </div>
            </div>
        </div>


        <div class="full-width video-pagination refresh-filter">
            <ul class="video-thumbs<?php if (date('Y-m-d') != $first_workout->date): ?> hidden<?php endif; ?>"
                data-match-tallest="li"
                data-program_id="<?= $model->id; ?>" data-week_num="1">
                <?php
                $j = 1;
                foreach ($regimenWorkouts as $date => $workout): ?>

                <?php if ($date == date('Y-m-d')) {
                    $workout_class = "is-current-workout current-week";
                } else {
                    $workout_class = '';
                } ?>

                <?php if (!empty($workout)): ?>

                    <?php
                    if (is_array($workout)) {
                        $general_workout = $workout[0];
                    } else {
                        $general_workout = $workout;
                    }

                    if (!empty($general_workout)) {
                        if ($general_workout->is_watched) {
                            $workout_class .= " is-finished-workout";
                        } elseif (!$general_workout->is_watched && $general_workout->date < date('Y-m-d')) {
                            $workout_class = "is-skipped-workout";
                        }
                    }
                    ?>

                    <?php if (is_array($workout)): ?>
                        <li class="<?= $workout_class; ?>"
                            data-regimen-id="<?= $general_workout->userRegimen->regimen_id; ?>"
                            <?php
                                $mashup_day = $general_workout->day;

                                if($general_workout->day > $last_workout_day) {
                                    $mashup_day = abs($first_workout->day - $general_workout->day);
                                }
                            ?>
                            data-regimen-day="<?= $mashup_day; ?>">

                            <span class="date"><?= date('M j', strtotime($date)); ?></span>
                            <div class="mashup">
                                <img src="<?= $workout[0]->video->meta->videoStillURL; ?>">
                                <ol data-modal="#mashup">
                                    <?php foreach ($workout as $training): ?>
                                        <li data-video="<?= $training->video_id; ?>">
                                            <div class="title">
                                                <?= $training->video->title; ?> (<?= count($workout); ?> workouts)
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                </ol>
                            </div>
                        </li>
                    <?php else: ?>
                        <li class="<?= $workout_class; ?>"
                            data-regimen-id="<?= $general_workout->userRegimen->regimen_id; ?>"
                            data-regimen-day="<?= $general_workout->day; ?>">
                            <span class="date"><?= date('M j', strtotime($date)); ?></span>
                            <div class="thumb" data-modal="#video" data-video="<?= $workout->video_id; ?>">
                                <img src="<?= $workout->video->meta->videoStillURL; ?>">
                                <div class="icon time"><?= $workout->video->duration; ?> min</div>
                                <div class="title-icon">
                                    <div class="title"><?= $workout->video->title; ?></div>

                                    <?php if (!empty($workout->video->strength)): ?>
                                        <div class="icon strength white <?= Video::getWorkoutStrength($workout->video->strength) ?>"
                                             title="strength"></div>
                                    <?php endif; ?>

                                    <?php if (!empty($workout->video->cardio)): ?>
                                        <div class="icon cardio white <?= Video::getWorkoutStrength($workout->video->cardio) ?>"
                                             title="cardio"></div>
                                    <?php endif; ?>

                                    <?php if (!empty($workout->video->restorative)): ?>
                                        <div class="icon restorative white <?= Video::getWorkoutStrength($workout->video->restorative) ?>"
                                             title="restorative"></div>
                                    <?php endif; ?>

                                </div>
                                <div class="add-favorite" title="Add to favorites">
                                    <div class="icon"></div>
                                    <!-- <h5>add to favorites</h5>-->
                                    <div class="checkmark">
                                        <img src="/themes/basic/images/global/icon-checkmark.png">
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endif; ?>
                <?php else: ?>
                    <?php if (is_array($workout)): ?>
                        <li class="rest-day <?= !empty($workout_class) ? $workout_class : ''; ?>">
                            <span class="date"><?= date('M j', strtotime($date)); ?></span>
                            <div class="empty-day">&nbsp;</div>
                        </li>
                    <?php else: ?>
                        <li class="rest-day <?= !empty($workout_class) ? $workout_class : ''; ?>">
                            <span class="date">
                                <?= date('M j', strtotime($date)); ?>
                            </span>
                            <span class="rest-day-title">REST DAY</span>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if ($j % 7 == 0): ?>
                <?php $i = 1;
                while (empty($workout)) {
                    if (isset($regimenWorkouts[date('Y-m-d', strtotime("{$date} -{$i} days"))]))
                        $workout = $regimenWorkouts[date('Y-m-d', strtotime("{$date} -{$i} days"))];
                    else
                        $workout = $regimenWorkouts[date('Y-m-d', strtotime("{$date} +{$i} days"))];
                    ++$i;
                } ?>

                <?php if (is_array($workout) && !empty($workout)) {
                    $workout = $workout[0];
                } ?>
                <li class="additional-block">
                    <?php if ($j == 7): ?>
                        <button type="button" class="btn btn-white next-week-btn">
                            <span>Next Week</span>
                        </button>
                    <?php endif; ?>

                    <?php if ($workout && RegimenMealPlan::getFilePath($model->id, $workout->getWeekNum())): ?>
                        <a href="/<?= RegimenMealPlan::getFilePath($model->id, $workout->getWeekNum()); ?>"
                           type="button" class="btn blue" target="_blank">Meal Plan</a>
                    <?php endif; ?>
                </li>
                <?php if ($workout->getWeekNum() < $total_weeks_count): ?>
                <?php
                    if (($workout->getWeekNum() + 1) == $current_week_num) {
                        $ul_hidden = false;
                    } else {
                        $ul_hidden = true;
                    }
                ?>
            </ul>
            <ul class="video-thumbs<?php if ($ul_hidden): ?> hidden<?php endif; ?>" data-match-tallest="li"
                data-program_id="<?= $model->id; ?>"
                data-week_num="<?= $workout->getWeekNum() + 1; ?>">
                <?php endif; ?>
                <?php endif; ?>
                <?php ++$j; ?>
                <?php endforeach; ?>
            </ul>
			<?php ?>

            <!--skipped workouts here-->
            <ul class="video-thumbs skipped-workouts hidden"
                data-match-tallest="li"
                data-program_id="<?= $model->id; ?>">
                <?php foreach ($skippedWorkouts as $date => $skippedWorkout): ?>
                    <?php
                    if (is_array($skippedWorkout)) {
                        $general_workout = $skippedWorkout[0];
                    } else {
                        $general_workout = $skippedWorkout;
                    }
                    ?>
                    <li class="is-skipped-workout"
                        data-regimen-id="<?= $general_workout->userRegimen->regimen_id; ?>"
                        data-regimen-day="<?= $general_workout->day; ?>">
                        <?php if (is_array($skippedWorkout)): ?>
                            <span class="date"><?= date('M j', strtotime($date)); ?></span>
                            <div class="mashup">
                                <img src="<?= $skippedWorkout[0]->video->meta->videoStillURL; ?>">
                                <ol data-modal="#mashup">
                                    <?php foreach ($skippedWorkout as $training): ?>
                                        <li data-video="<?= $training->video_id; ?>">
                                            <div class="title">
                                                <?= $training->video->title; ?> (<?= count($skippedWorkout); ?>
                                                workouts)
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                </ol>
                            </div>
                        <?php else: ?>
                            <span class="date"><?= date('M j', strtotime($date)); ?></span>
                            <div class="thumb" data-modal="#video" data-video="<?= $skippedWorkout->video_id; ?>">
                                <img src="<?= $skippedWorkout->video->meta->videoStillURL; ?>">
                                <div class="icon time"><?= $skippedWorkout->video->duration; ?> min</div>
                                <div class="title-icon">
                                    <div class="title"><?= $skippedWorkout->video->title; ?></div>

                                    <?php if (!empty($skippedWorkout->video->strength)): ?>
                                        <div class="icon strength white <?= Video::getWorkoutStrength($skippedWorkout->video->strength) ?>"
                                             title="strength"></div>
                                    <?php endif; ?>

                                    <?php if (!empty($skippedWorkout->video->cardio)): ?>
                                        <div class="icon cardio white <?= Video::getWorkoutStrength($skippedWorkout->video->cardio) ?>"
                                             title="cardio"></div>
                                    <?php endif; ?>

                                    <?php if (!empty($skippedWorkout->video->restorative)): ?>
                                        <div class="icon restorative white <?= Video::getWorkoutStrength($skippedWorkout->video->restorative) ?>"
                                             title="restorative"></div>
                                    <?php endif; ?>

                                </div>
                                <div class="add-favorite" title="Add to favorites">
                                    <div class="icon"></div>
                                    <!-- <h5>add to favorites</h5>-->
                                    <div class="checkmark">
                                        <img src="/themes/basic/images/global/icon-checkmark.png">
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <button class="hidden modal-call" data-modal="#finish-program">&nbsp;</button>
        <button class="hidden modal-call" data-modal="#change-weight">&nbsp;</button>
    </section>

<?php $this->renderPartial('//modals/mashup'); ?>
<?php $this->renderPartial('//modals/video'); ?>
<?php $this->renderPartial('//modals/start-workout-program', ['model' => $workout, 'text' => "Hi everyone, I am starting Physique 57's ".$workout->getWeekNum()." week(s) workout program. Help me stay motivated during these ".$workout->getWeekNum()." week(s) by liking this post!"]); ?>
<?php
$this->renderPartial('//modals/finish-program', array(
        'cancel_program_url' => $cancel_program_url,
        'skipped_count' => $skipped_count
    )
);
?>

<?php if (!(Yii::app()->user->profile->do_not_show_weight_popup)
    && date('N') == 1 && empty(Yii::app()->session['weight_popup_shown'])
) {
    Yii::app()->session['weight_popup_shown'] = true;
    $this->renderPartial('//modals/change-weight', array('program_id' => $program_id));
} ?>