<?php $this->bodyClass = 'promo dark-header no-top-padding short-nav short-nav-perm'; ?>

<section id="payment">
    <?php $form=$this->beginWidget('ActiveForm', array(
        'action'=>Yii::app()->createUrl('/membership/specialOffer', ['slug' => $plan->slug]),
        'htmlOptions'=>array(
            'data-validate'=>''
        ),
    )); ?>

    <div class="lockout">
        <?php echo $form->errorSummary($user); ?>
        <?php echo $form->errorSummary($customer); ?>
        <div class="side-lines promo">
            <h1>sign up</h1>
        </div>
        <h4 class="special-offers">special offer</h4>
    </div>
    <div id="hero">
		<?php if (!empty($image)): ?>
			<div class="mobile-image" data-mobile="<?php echo $image; ?>"
				data-desktop="<?php echo $image; ?>">
			</div>
		<?php else: ?>
			<div class="mobile-image" data-mobile="/themes/basic/images/promo/hero-mobile.jpg"
				data-desktop="/themes/basic/images/promo/hero-desktop.jpg">
			</div>
		<?php endif; ?>
        <div class="left">
            <div class="inner">
                <div class="title">
                    <h3><span class="term"><?php echo $plan->title; ?></span></h3>
                </div>
                <div class="side-lines">&nbsp;</div>
                <div class="body-copy">
                    <?php echo $plan->description; ?>
                </div>
            </div>
        </div>
		<?php if (!empty($image)): ?>
			<div class="right" data-mobile="<?php echo $image; ?>"
				data-desktop="<?php echo $image; ?>">
		    </div>
		<?php else: ?>
			<div class="right" data-mobile="/themes/basic/images/promo/hero-mobile.jpg"
				data-desktop="/themes/basic/images/promo/hero-desktop.jpg">
		    </div>
		<?php endif; ?>
    </div>
    <div id="promo-copy">
        <div class="lockout">
<!--            <p>*Valid for new online workout subscribers only.</p>-->
            <?php /* <p>$<?php echo $plan['amount']; ?> monthly, cancel anytime.</p> */ ?>
        </div>
    </div>
    <a name="register"></a>
    <?php if (!Yii::app()->user->id): ?>
    <div id="user-pass">
        <div class="lockout">
            <div class="side-lines">
                <h1>SIGN UP AND START THE CHALLENGE NOW</h1>
            </div>

            <div class="inputs">
                <?php echo $form->textField($user, 'email', array('maxlength' => 128, 'placeholder' => 'Email Address', 'data-validate' => 'min:5|email', 'class' => 'email')); ?>
                <?php echo $form->passwordField($user, 'password', array('maxlength' => 128, 'placeholder' => 'Password', 'data-validate' => 'min:6', 'class' => 'password')); ?>
                <!--                --><?php //echo $form->textField($model, 'email', array('maxlength' => 128, 'placeholder' => 'Email Address', 'data-validate' => 'min:5|email', 'class' => 'email')); ?>
                <!--                --><?php //echo $form->passwordField($model, 'password', array('maxlength' => 128, 'placeholder' => 'Password', 'data-validate' => 'min:6', 'class' => 'password')); ?>
                <div>
                    <?php //echo $form->error($model,'email'); ?>
                    <?php //echo $form->error($model,'password'); ?>
                </div>
            </div>
            <!--<ul class="error-messages">
                <li class="email">Please enter a valid email!</li>
                <li class="password">Your password must be at least six characters!</li>
            </ul>-->
        </div>
    </div>
    <?php endif; ?>
    <div id="payment-details">
        <div class="lockout">
            <h4>ENTER YOUR PAYMENT DETAILS</h4>
            <hr>
            <div class="top-text">
                <ol>
                    <?php if ($plan['is_auto_renew'] == 0): ?>
					<li>Cancel anytime.</li>
                    <!--<li>Your one month memberships will end on <?php echo date('m/d/y', strtotime('+1 month')); ?>.</li>-->
                    <?php endif; ?>
<!--                    <li>No commitments, cancel online anytime.</li>-->
                    <li>Unlimited streaming for 1 user.</li>
                </ol>
            </div>


            <div class="credit-card">
                <p>CREDIT CARD</p>
                <div class="cards"></div>
            </div>

                <div class="inputs">
                    <div class="row">
                        <div class="input-item first">
                            <label>FIRST NAME <span class="must-match"><span class="darker-blue">*</span>Must match card</span></label>
                            <?php echo $form->textField($customer, 'first_name', array('data-validate'=>'min:2')); ?>
                        </div>
                        <div class="input-item last">
                            <label>LAST NAME <span class="must-match"><span class="darker-blue">*</span>Must match card</span></label>
                            <?php echo $form->textField($customer, 'last_name', array('data-validate'=>'min:2')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-item street first">
                            <label>STREET ADDRESS</label>
                            <?php echo $form->textField($customer, 'street', array('data-validate'=>'min:2')); ?>
                        </div>
                        <div class="input-item zip">
                            <label>ZIP CODE</label>
                            <?php echo $form->textField($customer, 'zipcode', array('maxlength'=>'12')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-item card lock">
                            <label>CARD NUMBER</label>
                            <?php echo $form->textField($customer, 'new_card_number', array('data-validate'=>'min:13')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-item expiration-month">
                            <label>EXPIRATION DATE</label>
                            <?php echo $form->dropDownList($customer, 'card_month', Time::months(), array('id'=>'expiry-month', 'data-validate'=>'min:1')); ?>
                            <div class="down-arrow"><img src="/themes/basic/images/global/down-arrow.svg"></div>
                        </div>
                        <div class="input-item expiration-year">
                            <label>&nbsp;</label>
                            <?php echo $form->dropDownList($customer, 'card_year', Time::years(), array('id'=>'expiry-year', 'data-validate'=>'min:1')); ?>
                            <div class="down-arrow"><img src="/themes/basic/images/global/down-arrow.svg"></div>
                        </div>
                        <div class="input-item security-code lock">
                            <label>SECURITY CODE</label>
                            <?php echo $form->textField($customer, 'card_security', array('data-validate'=>'min:2', 'maxlength'=>4)); ?>
                        </div>
                        <div class="input-item qmark">
                            <span class="rollover">
                                <img src="/themes/basic/images/account/credit-cards-hover@2x.png" width="276">
                            </span>
                        </div>
                        <?php /*
                        <div class="input-item coupon">
                            <input type="text" maxlength="30" placeholder="Coupon Code"/>
                        </div>
                        <div class="input-item apply-coupon">
                            <a class="btn" href="#">apply coupon</a>
                        </div>
                        */ ?>
                    </div>
                </div>

                <div class="legal">
                    <p><?php if ($plan['promo_end_date']): ?>
                        Must sign up by <?php echo date('n/j/Y', strtotime($plan['promo_end_date'])); ?>.
                        <?php endif; ?>

                        <?php if ($plan['is_auto_renew']): ?>
                            Subscription will auto renew at $<?php echo $plan['amount']; ?> until canceled.
                        <?php else: ?>
                            <!--One month subscription does not renew.--> 
                        <?php endif; ?>
                    </p>
                </div>
                <div class="checkbox-wrap">
                    <div class="left">
                        <!-- <input type="checkbox" name="over_18" id="over_18" class="optional" value="1"/> -->
                        <?php echo $form->checkbox($customer, 'over_18', array('id' => 'over_18')); ?>
                    </div>
                    <div class="right">
                        <label class="checkbox" for="over_18">
                            <div
                                class="main-checkbox">I am over 18, and I agree to the above conditions and the <a
                                    href="http://physique57.com/terms-and-conditions/" target="_blank">Terms of
                                    Use</a> and <a href="http://physique57.com/privacy-legal/" target="_blank">Privacy / Cookies</a>.
                            </div>
                        </label>
                    </div>
                </div>
                <div class="buttons">
                    <input type="submit" class="btn blue big active" value="start membership" id="start-membership-btn"/>
                </div>
                <div class="secure">
                    <img src="/themes/basic/images/global/icon-lock@2x.png" width="26" alt="Secure">
                    <p>Secure Server</p>
                    <p class="tell">Tell me more<span class="rollover">
                                We take your privacy seriously We safeguard your payment information (such as credit card number)
                                with SSL (Secure Sockets Layer) when sent over the Internet. SSL encrypts personal information,
                                including your name and payment information, so that it cannot be read in transit by a third party.
                                We also store your payment information in an encrypted format.</span>
                    </p>
                </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</section>

<script>
    jQuery(document).ready(function($) {
        $('a[href="/sign-up"]').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();

            $('html, body').animate({
                scrollTop: $('a[name="register"]').offset().top - 83
            }, 500);
        });
    });
</script>
