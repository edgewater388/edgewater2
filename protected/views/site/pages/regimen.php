<?php $this->bodyClass = 'regimens short-nav short-nav-perm program-description-page '; ?>

<section data-program_id="<?= $model->id; ?>">
    <header class="header header-padding">
        <div class="lockout">
            <div class="side-lines cream">
                <h4>workout program</h4>
            </div>
            <h1><?php echo $model->title; ?></h1>
        </div>
    </header>
    <?php if (!UserRegimen::checkUserSubscribed(Yii::app()->user->id, $model->id)): ?>
        <div id="summary">
            <div class="lockout">
                <h4>LENGTH: <?php echo $model->length; ?></h4>
                <div class="text">
                    <p><?php echo $model->description; ?></p>
                </div>
                <div class="blue-dot-btn gray large">
                    <a href="/program/<?= $model->id; ?>/subscribe" data-modal="#start-program">
                        START THE PROGRAM
                        <div class="border tl"></div>
                        <div class="border tr"></div>
                        <div class="border br"></div>
                        <div class="border bl"></div>
                    </a>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="blue-dot-btn gray large">
            <a href="/program/<?= $model->id; ?>">
                GO TO THE CURRENT WEEK
                <div class="border tl"></div>
                <div class="border tr"></div>
                <div class="border br"></div>
                <div class="border bl"></div>
            </a>
        </div>
    <?php endif; ?>
    <ul id="weeks">
        <?php $week = 0; ?>
        <?php foreach ($videos as $week=>$videosByDay): ?>
        <li class="week">
            <div class="lockout">
                <h1>week <span class="move-up"><?php echo $week; ?></span></h1>
                <hr>
                <ul class="days">
                    <?php foreach ($videosByDay as $day=>$video): ?>
                    <li class="day" data-regimen-id="<?php echo $model->id; ?>" data-regimen-day="<?php echo $day; ?>">
                        <div class="day-title">Day <?php echo $day; ?></div>
                        <?php //var_dump($video); ?>
                        <?php if ($video[0] === null): ?>
                            <div class="description rest-day">Rest Day</div>
                        <?php elseif (count($video) === 1): ?>
                            <div class="description" data-modal="#video" data-video="<?php echo $video[0]->id; ?>"><?php echo $video[0]->title; ?></div>
                        <?php else: ?>
                            <div class="description">Mashup
                                <ol data-modal="#mashup">
                                <?php foreach($video as $v): ?>
                                    <?php if ($v) : ?>
                                        <li><?php echo $v->title; ?></li>
                                    <?php else: ?>
                                        <?php
                                        var_dump($v);
                                        ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                </ol>
                            </div>
                        <?php endif; ?>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </li>
        <?php endforeach; ?>
        <?php /*
        <li class="week">
            <div class="lockout">
                <h1>week 1</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 1</div>
                        <div class="description" data-modal="#video" data-video="24">Beginner Barre</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 2</div>
                        <div class="description" data-modal="#video" data-video="13">P57 Classic 1</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 3</div>
                        <div class="description" data-modal="#video" data-video="26">Bis and buns</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 4</div>
                        <div class="description" >Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 5</div>
                        <div class="description" data-modal="#video" data-video="39">Mat</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 6</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Shannon Arms</li>
                                <li data-modal="#video" data-video="3">Brady Thighs</li>
                                <li data-modal="#video" data-video="">Katie Abs</li>
                                <li data-modal="#video" data-video="">Katie Stretch</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 7</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 2</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 8</div>
                        <div class="description" data-modal="#video" data-video="13">Muscle Sculpting Medley</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 9</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Christinas Arms</li>
                                <li data-modal="#video" data-video="8">30 min express</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 10</div>
                        <div class="description" data-modal="#video" data-video="24">Beginner Barre</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 11</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 12</div>
                        <div class="description" data-modal="#video" data-video="13">P57 Classic 1</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 13</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Christinas Arms</li>
                                <li data-modal="#video" data-video="">Shelly Thighs</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 14</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        */ ?>

        <?php if ($model->hasCompleted()): ?>
        <div class="congratulations">
            <h4>You’ve completed this workout program and you’re looking great!</h4>
            <h2>Congratulations!</h2>
        </div>
        <?php endif; ?>
    </ul>


</section>

<?php $this->renderPartial('//modals/mashup'); ?>
<?php $this->renderPartial('//modals/video'); ?>

<?php
    if (Yii::app()->user->profile->getProfileIsEmpty())
        $this->renderPartial('//modals/start-program');
?>


