<?php $this->bodyClass = 'regimens short-nav short-nav-perm'; ?>

<section>
    <header class="header header-padding">
        <div class="lockout">
            <div class="side-lines cream">
                <h4>workout programs</h4>
            </div>
            <h1>New to the Barre</h1>
        </div>
    </header>
    <div id="summary">
        <div class="lockout">
            <h4>LENGTH: 2 WEEKS</h4>
            <div class="text">
                <p>Notes: Learn our basic positions, technique and the secrets to our muscle transforming method. Gain
                    stamina and endurance as you prepare to move on to other workout programs.</p>
            </div>
        </div>
    </div>
    <ul id="weeks">
        <li class="week">
            <div class="lockout">
                <h1>week 1</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 1</div>
                        <div class="description" data-modal="#video" data-video="24">Beginner Barre</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 2</div>
                        <div class="description" data-modal="#video" data-video="13">P57 Classic 1</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 3</div>
                        <div class="description" data-modal="#video" data-video="26">Bis and buns</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 4</div>
                        <div class="description" >Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 5</div>
                        <div class="description" data-modal="#video" data-video="39">Mat</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 6</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Shannon Arms</li>
                                <li data-modal="#video" data-video="3">Brady Thighs</li>
                                <li data-modal="#video" data-video="">Katie Abs</li>
                                <li data-modal="#video" data-video="">Katie Stretch</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 7</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 2</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 8</div>
                        <div class="description" data-modal="#video" data-video="13">Muscle Sculpting Medley</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 9</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Christinas Arms</li>
                                <li data-modal="#video" data-video="8">30 min express</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 10</div>
                        <div class="description" data-modal="#video" data-video="24">Beginner Barre</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 11</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 12</div>
                        <div class="description" data-modal="#video" data-video="13">P57 Classic 1</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 13</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Christinas Arms</li>
                                <li data-modal="#video" data-video="">Shelly Thighs</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 14</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <div class="congratulations">
            <h4>You’ve completed this workout program and you’re looking great!</h4>
            <h2>Congratulations!</h2>
        </div>
    </ul>


</section>

<?php $this->renderPartial('//modals/video'); ?>


