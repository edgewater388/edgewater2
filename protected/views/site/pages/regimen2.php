<?php $this->bodyClass = 'regimens short-nav short-nav-perm'; ?>

<section>
    <header class="header header-padding">
        <div class="lockout">
            <div class="side-lines cream">
                <h4>workout programs</h4>
            </div>
            <h1>Get Fit Fast</h1>
        </div>
    </header>
    <div id="summary">
        <div class="lockout">
            <h4>LENGTH: 2 WEEKS</h4>
            <div class="text">
                <p>High intensity cardio and strength training workouts that will shed extra pounds and cinch every
                    inch. Great for getting ready for a vacation, reunion, etc.</p>
            </div>
        </div>
    </div>
    <ul id="weeks">
        <li class="week">
            <div class="lockout">
                <h1>week 1</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 1</div>
                        <div class="description" data-modal="#video" data-video="47">Strong and Lean</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 2</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="31">Cardio Burn</li>
                                <li data-modal="#video" data-video="">Sade Cardio</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 3</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="52">Get Fit Quick</li>
                                <li data-modal="#video" data-video="29">Cardio Blast</li>
                            </ol>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 4</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 5</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="34">Cardio Sculpt</li>
                                <li data-modal="#video" data-video="31">Cardio Burn</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 6</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Alicia Full Body</li>
                                <li data-modal="#video" data-video="">Sade Cardio</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 7</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="31">Cardio Burn</li>
                                <li data-modal="#video" data-video="">Chad 10 Min Abs</li>
                                <li data-modal="#video" data-video="29">Cardio Blast</li>
                            </ol>
                        </div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 2</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 8</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="6">360</li>
                                <li data-modal="#video" data-video="">Chad Sculpt</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 9</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 10</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Sade 10 Min Cardio</li>
                                <li data-modal="#video" data-video="31">Cardio Burn</li>
                                <li data-modal="#video" data-video="">Sade Cardio</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 11</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="21">Back Body</li>
                                <li data-modal="#video" data-video="29">Cardio Blast</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 12</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Barre Meets Yoga</li>
                                <li data-modal="#video" data-video="20">Advanced Express</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 13</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="34">Cardio Sculpt</li>
                                <li data-modal="#video" data-video="29">Cardio Blast</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 14</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="52">Get Fit Quick</li>
                                <li data-modal="#video" data-video="">Alicia Full Body</li>
                                <li data-modal="#video" data-video="">Sade Cardio</li>
                                <li data-modal="#video" data-video="45">Katie Stretch</li>
                            </ol>
                        </div>
                    </li>
                </ul>

            </div>
        </li>
        <div class="congratulations">
            <h4>You’ve completed this workout program and you’re looking great!</h4>

            <h2>Congratulations!</h2>
        </div>
    </ul>


</section>

<?php $this->renderPartial('//modals/video'); ?>


