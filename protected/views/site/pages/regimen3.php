<?php $this->bodyClass = 'regimens short-nav short-nav-perm'; ?>

<section>
    <header class="header header-padding">
        <div class="lockout">
            <div class="side-lines cream">
                <h4>workout programs</h4>
            </div>
            <h1>Total Body Blast</h1>
        </div>
    </header>
    <div id="summary">
        <div class="lockout">
            <h4>LENGTH: 4 WEEKS</h4>
            <div class="text">
                <p>High intensity, lots of cardio to burn calories, fat, and metabolic rate.</p>
            </div>
        </div>
    </div>
    <ul id="weeks">
        <li class="week">
            <div class="lockout">
                <h1>week 1</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 1</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="52">Get Fit Quick</li>
                                <li data-modal="#video" data-video="">Sade Cardio 10 Min</li>
                                <li data-modal="#video" data-video="">Ashly Abs</li>
                                <li data-modal="#video" data-video="45">Katie Stretch</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 2</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Barre Meets Yoga</li>
                                <li data-modal="#video" data-video="34">Cardio Sculpt</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 3</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Shannon Arms</li>
                                <li data-modal="#video" data-video="">Katie Seat</li>
                                <li data-modal="#video" data-video="">Katie Abs</li>
                                <li data-modal="#video" data-video="6">Katie 360</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 4</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 5</div>
                        <div class="description" data-modal="#video" data-video="14">P57 Classic 2</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 6</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="21">Back Body</li>
                                <li data-modal="#video" data-video="17">Thigh and Seat</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 7</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 2</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 8</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="18">Arms and Abs</li>
                                <li data-modal="#video" data-video="26">Bis and Buns</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 9</div>
                        <div class="description" data-modal="#video" data-video="">Sade Cardio</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 10</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="47">Strong and Lean</li>
                                <li data-modal="#video" data-video="52">Get Fit Quick</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 11</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 12</div>
                        <div class="description" data-modal="#video" data-video="">Alicia Full Body</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 13</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="29">Cardio Blast</li>
                                <li data-modal="#video" data-video="34">Cardio Sculpt</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 14</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 3</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 15</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Tanya Hips</li>
                                <li data-modal="#video" data-video="">Chad Obliques</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 16</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Shannon Arms</li>
                                <li data-modal="#video" data-video="">Shanna Thighs</li>
                                <li data-modal="#video" data-video="">Chad Abs</li>
                                <li data-modal="#video" data-video="">Julie Cardio</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 17</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Chad Full Body</li>
                                <li data-modal="#video" data-video="">Chad Cardio</li>
                                <li data-modal="#video" data-video="">Chad Abs</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 18</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 19</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Ashly P 15 Min</li>
                                <li data-modal="#video" data-video="">Brady P 15 Min</li>
                                <li data-modal="#video" data-video="">Barre Meets Yoga</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 20</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="50">Trim and Tone</li>
                                <li data-modal="#video" data-video="6">360</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 21</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 4</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 22</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="31">Cardio Burn</li>
                                <li data-modal="#video" data-video="">Sade Cardio</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 23</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Alicia Full Body</li>
                                <li data-modal="#video" data-video="47">Stong and Lean</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 24</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="34">Cardio Sculpt</li>
                                <li data-modal="#video" data-video="20">Tanya Adv Express</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 25</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 26</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Christina Arms</li>
                                <li data-modal="#video" data-video="">Shannon Arms</li>
                                <li data-modal="#video" data-video="">Brady 15 Min</li>
                                <li data-modal="#video" data-video="">Shanna Thighs</li>
                                <li data-modal="#video" data-video="">Katie Seat</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 27</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Sade Cardio</li>
                                <li data-modal="#video" data-video="">Brady Thighs</li>
                                <li data-modal="#video" data-video="">Ashly Abs</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 28</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Ashly 15 min</li>
                                <li data-modal="#video" data-video="">Back Body</li>
                                <li data-modal="#video" data-video="">Cardio Blast</li>
                                <li data-modal="#video" data-video="">Katie Stretch</li>
                            </ol>
                        </div>
                    </li>
                </ul>

            </div>
        </li>
        <div class="congratulations">
            <h4>You’ve completed this workout program and you’re looking great!</h4>

            <h2>Congratulations!</h2>
        </div>
    </ul>


</section>

<?php $this->renderPartial('//modals/video'); ?>


