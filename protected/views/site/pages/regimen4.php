<?php $this->bodyClass = 'regimens short-nav short-nav-perm'; ?>

<section>
    <header class="header header-padding">
        <div class="lockout">
            <div class="side-lines cream">
                <h4>workout programs</h4>
            </div>
            <h1>Slim Down, Tone Up</h1>
        </div>
    </header>
    <div id="summary">
        <div class="lockout">
            <h4>LENGTH: 8 WEEKS</h4>
        </div>
    </div>
    <ul id="weeks">
        <li class="week">
            <div class="lockout">
                <h1>week 1</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 1</div>
                        <div class="description" data-modal="#video" data-video="13">Beginner Barre</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 2</div>
                        <div class="description" data-modal="#video" data-video="">P57 Classic 1</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 3</div>
                        <div class="description" data-modal="#video" data-video="">Muscle Scuplting Medleys</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 4</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 5</div>
                        <div class="description" data-modal="#video" data-video="">Mat</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 6</div>
                        <div class="description" data-modal="#video" data-video="">Classic Express</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 7</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 2</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 8</div>
                        <div class="description" data-modal="#video" data-video="13">Muscle Sculpting Medley</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 9</div>
                        <div class="description" data-modal="#video" data-video="">Arms & Abs</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 10</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Christina's Arms</li>
                                <li data-modal="#video" data-video="">Brady Thigh</li>
                                <li data-modal="#video" data-video="">Christina Seat</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 11</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 12</div>
                        <div class="description" data-modal="#video" data-video="">P57 Classic 1</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 13</div>
                        <div class="description" data-modal="#video" data-video="">Begginer Barre</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 14</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 3</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 15</div>
                        <div class="description" data-modal="#video" data-video="13">Christina Full Body</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 16</div>
                        <div class="description" data-modal="#video" data-video="">Seat & Thigh Booster</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 17</div>
                        <div class="description">P57 Classi 2</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 18</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 19</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Shannon's Arms</li>
                                <li data-modal="#video" data-video="">Shanna Inner Thigh</li>
                                <li data-modal="#video" data-video="">Kati Seat</li>
                                <li data-modal="#video" data-video="">Barre Meets Yoga</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 20</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Body Back</li>
                                <li data-modal="#video" data-video="">Get Fit Quick</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 21</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 4</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 22</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Arms & Abs</li>
                                <li data-modal="#video" data-video="">Thigh & Seat</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 23</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Cardio Sculpt</li>
                                <li data-modal="#video" data-video="">Cardio Blast</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 24</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Ashly Upper Body</li>
                                <li data-modal="#video" data-video="">Brady Lower Body</li>
                                <li data-modal="#video" data-video="">Strong and Lean</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 25</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 26</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Cardio Burn</li>
                                <li data-modal="#video" data-video="">Sade Cardio</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 27</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Christina Bix & Buns</li>
                                <li data-modal="#video" data-video="">360</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 28</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 5</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 29</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Tanya Waist & Hips</li>
                                <li data-modal="#video" data-video="">Chad Full Body</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 30</div>
                        <div class="description" data-modal="#video" data-video="">Christina Arms & Abs</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 31</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Cardio Sculpt</li>
                                <li data-modal="#video" data-video="">Cardio Burn</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 32</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 33</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Ashly Upper Body</li>
                                <li data-modal="#video" data-video="">Brady Lower Body</li>
                                <li data-modal="#video" data-video="">FIT</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 34</div>
                        <div class="description" data-modal="#video" data-video="">Cardio Blast</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 35</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 6</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 36</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Advanced Express</li>
                                <li data-modal="#video" data-video="">Sade Cardio 10 Min</li>
                                <li data-modal="#video" data-video="">Ashly Abs 10</li>
                                <li data-modal="#video" data-video="">Katie Stretch</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 37</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Sade Cardio 30</li>
                                <li data-modal="#video" data-video="">Tanya Express</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 38</div>
                        <div class="description">P57 Classic 2</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 39</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 40</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Barre Meets Yoga</li>
                                <li data-modal="#video" data-video="">Mat</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 41</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Cardio Blast</li>
                                <li data-modal="#video" data-video="">Chad Abs</li>
                                <li data-modal="#video" data-video="">Katie Stretch</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 42</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 7</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 43</div>
                        <div class="description" data-modal="#video" data-video="13">Alicia Full Body</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 44</div>
                        <
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Shannon Arms</li>
                                <li data-modal="#video" data-video="">Katie Seat</li>
                                <li data-modal="#video" data-video="">Katie Abs</li>
                                <li data-modal="#video" data-video="">Katie Stretch</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 45</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Cardio Sculpt</li>
                                <li data-modal="#video" data-video="">Tanya Trim and Tone</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 46</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 47</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Bis and Buns</li>
                                <li data-modal="#video" data-video="">Thighs and Seat</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 48</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Katie's Abs</li>
                                <li data-modal="#video" data-video="">Ashly Abs</li>
                                <li data-modal="#video" data-video="">Chad's Abs</li>
                                <li data-modal="#video" data-video="">Arms and Abs</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 49</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 8</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 50</div>
                        <div class="description" data-modal="#video" data-video="13">360</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 51</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Christina's Arms</li>
                                <li data-modal="#video" data-video="">Brady Thigh</li>
                                <li data-modal="#video" data-video="">Christina Seat</li>
                                <li data-modal="#video" data-video="">Tanya Waist and Hips</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 52</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Get Fit Quick</li>
                                <li data-modal="#video" data-video="">Cardio Burn</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 53</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 54</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Cardio Blast</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 55</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Express</li>
                                <li data-modal="#video" data-video="">Advanced Express</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 56</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Alicia Lower Body</li>
                                <li data-modal="#video" data-video="">Sade Cardio 30 Min</li>
                            </ol>
                        </div>
                    </li>
                </ul>

            </div>
        </li>
        <div class="congratulations">
            <h4>You’ve completed this workout program and you’re looking great!</h4>

            <h2>Congratulations!</h2>
        </div>
    </ul>


</section>

<?php $this->renderPartial('//modals/video'); ?>


