<?php $this->bodyClass = 'regimens short-nav short-nav-perm'; ?>

<section>
    <header class="header header-padding">
        <div class="lockout">
            <div class="side-lines cream">
                <h4>workout programs</h4>
            </div>
            <h1>BEST. BODY. EVER.</h1>
        </div>
    </header>
    <div id="summary">
        <div class="lockout">
            <h4>LENGTH: 12 WEEKS</h4>
            <div class="text">
                <p>1st Month is about building stamina, endurance and gaining basic knowledge of Physique 57 positions.
                    With commitment and consistancey, you can start to see your body transform to your Best. Body. Ever.</p>
                <p>2nd Month increases stamina and endurance. Integrates more challenging workouts with less recovery
                    time between.</p>
                <p>The 3rd month will fully establish the habit of working out. By now your body will have
                    transformed and now its about fine tuning. Here you will learn more positions, further increase
                    stamina through the incorporation cardio elements and strengthen any trouble spots.</p>
            </div>
        </div>
    </div>
    <ul id="weeks">
        <li class="week">
            <div class="lockout">
                <h1>week 1</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 1</div>
                        <div class="description" data-modal="#video" data-video="13">Physique 57 Classic 1st Edition</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 2</div>
                        <div class="description" data-modal="#video" data-video="">Beginner Barre w/ Kim</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 3</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Shannon's Arms</li>
                                <li data-modal="#video" data-video="">Brady’s Thighs</li>
                                <li data-modal="#video" data-video="">Christina’s Seat</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 4</div>
                        <div class="description" data-modal="#video" data-video="">
                            <div class="description">Mashup
                                <ol>
                                    <li data-modal="#video" data-video="">P57 Express in 30</li>
                                    <li data-modal="#video" data-video="">Katie's Abs</li>
                                </ol>
                            </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 5</div>
                        <div class="description" data-modal="#video" data-video="">Katie’s 15 Min Stretch</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 6</div>
                        <div class="description" data-modal="#video" data-video="">Physique 57 Classic 1st Edition</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 7</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 2</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 8</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Holly's 30 Min</li>
                                <li data-modal="#video" data-video="">Muscle Medley</li>
                                <li data-modal="#video" data-video="">Katie's Abs</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 9</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Catsie Mat</li>
                                <li data-modal="#video" data-video="">Christina's Arms</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 10</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 11</div>
                        <div class="description" data-modal="#video" data-video="">Physique 57 Classic 2nd Edition</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 12</div>
                        <div class="description" data-modal="#video" data-video="">Beginner Barre w/ Kim</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 13</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 14</div>
                        <div class="description" data-modal="#video" data-video="">Tanya Express</div>>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 3</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 15</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Shelly Thigh & Seat</li>
                                <li data-modal="#video" data-video="">Shannon's Arms</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 16</div>
                        <div class="description" data-modal="#video" data-video="">Adrianna's Barre Meets Yoga</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 17</div>
                        <div class="description" data-modal="#video" data-video="">Tanya's FIT</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 18</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 19</div>
                        <div class="description" data-modal="#video" data-video="">Christina's Strong and Lean</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 20</div>
                        <div class="description" data-modal="#video" data-video="">Physique 57 Classic 1st Edition</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 21</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 4</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 22</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Ashly 15 Min</li>
                                <li data-modal="#video" data-video="">Brady 15 Min</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 23</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Holly Muscle Medley</li>
                                <li data-modal="#video" data-video="">Ashly Ab Blast</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 24</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 25</div>
                        <div class="description" data-modal="#video" data-video="">Cassie Mat</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 26</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Shelly Arm & Abs</li>
                                <li data-modal="#video" data-video="">Shelly Thigh & Seat</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 27</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 28</div>
                        <div class="description" data-modal="#video" data-video="">Christina's Arm & Abs</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 5</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 29</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Christina's Arms</li>
                                <li data-modal="#video" data-video="">Brady’s Thighs</li>
                                <li data-modal="#video" data-video="">Katie’s Seat</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 30</div>
                        <div class="description" data-modal="#video" data-video="">Tanya P57 Express</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 31</div>
                        <div class="description" data-modal="#video" data-video="">Chad Full Body</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 32</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 33</div>
                        <div class="description" data-modal="#video" data-video="">Tanya FIT</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 34</div>
                        <div class="description" data-modal="#video" data-video="">Adrianna Barre Meets Yoga</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 35</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 6</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 36</div>
                        <div class="description" data-modal="#video" data-video="13">Tanya Get Fit Quick</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 37</div>
                        <div class="description" data-modal="#video" data-video="">Strong and Lean</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 38</div>
                        <div class="description" data-modal="#video" data-video="">Holly's Muscle Scuplting</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 39</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Shelly's Thigh</li>
                                <li data-modal="#video" data-video="">Shanna's Inner Thigh</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 40</div>
                        <div class="description" data-modal="#video" data-video="">P57 Classic 1</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 41</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 42</div>
                        <div class="description" data-modal="#video" data-video="">Barre Meets Yoga</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 7</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 43</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Christina's Arms</li>
                                <li data-modal="#video" data-video="">Shannon's Arms</li>
                                <li data-modal="#video" data-video="">Katie Abs</li>
                                <li data-modal="#video" data-video="">Ashley Abs</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 44</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Back Body</li>
                                <li data-modal="#video" data-video="">Christina’s Seat</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 45</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Shelly Arm & Abs</li>
                                <li data-modal="#video" data-video="">Ashly P 15 Min</li>
                                <li data-modal="#video" data-video="">Brady 15 Min</li>
                                <li data-modal="#video" data-video="">Katie Stretch</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 46</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 47</div>
                        <div class="description" data-modal="#video" data-video="">P57 Classic 2</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 48</div>
                        <div class="description" data-modal="#video" data-video="">Advanced Express</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 49</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 8</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 50</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Chad Full Body</li>
                                <li data-modal="#video" data-video="">Katie's Abs</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 51</div>
                        <div class="description" data-modal="#video" data-video="">Cardio Sculpt</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 52</div>
                        <div class="description">Rest Day</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 53</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Christina's Arms</li>
                                <li data-modal="#video" data-video="">FIT</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 54</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Shannons Arms</li>
                                <li data-modal="#video" data-video="">Shanna's Thighs</li>
                                <li data-modal="#video" data-video="">Christina’s Seat</li>
                                <li data-modal="#video" data-video="">Ashly Abs</li>
                                <li data-modal="#video" data-video="">Katie Stretch</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 55</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Tanya's Waist & Hips</li>
                                <li data-modal="#video" data-video="">Bis and Buns</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 56</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 9</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 57</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Shannons Arms</li>
                                <li data-modal="#video" data-video="">Advance Express</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 58</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Ashly 15 Min</li>
                                <li data-modal="#video" data-video="">Brady 15 Min</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 59</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Shelly Arms/Abs</li>
                                <li data-modal="#video" data-video="">Shelly Thigh/Seat</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 60</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Katie 360</li>
                                <li data-modal="#video" data-video="">Katie Stretch</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 61</div>
                        <div class="description" data-modal="#video" data-video="">Strong and Lean</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 62</div>
                        <div class="description" data-modal="#video" data-video="">Mat</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 63</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 10</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 64</div>
                        <div class="description" data-modal="#video" data-video="13">P57 Classic 2nd Edition</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 65</div>
                        <div class="description" data-modal="#video" data-video="">Back Body</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 66</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Christina Bis/Buns</li>
                                <li data-modal="#video" data-video="">Shelly Arms/Abs</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 67</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Sade Cardio</li>
                                <li data-modal="#video" data-video="">Shanna Thighs</li>
                                <li data-modal="#video" data-video="">Katie Abs</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 68</div>
                        <div class="description" data-modal="#video" data-video="">Tanya Trim and Tone</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 69</div>
                        <div class="description" data-modal="#video" data-video="">Muscle Sculpting Medleys</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 70</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 11</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 71</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">FIT</li>
                                <li data-modal="#video" data-video="">Tanya Waist/Hips</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 72</div>
                        <div class="description" data-modal="#video" data-video="">Chad Cardio</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 73</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Ashly 15 Min</li>
                                <li data-modal="#video" data-video="">Brady 15 Min</li>
                                <li data-modal="#video" data-video="">Cardio Sculpt</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 74</div>
                        <div class="description" data-modal="#video" data-video="">Sade Cardio</div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 75</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Express</li>
                                <li data-modal="#video" data-video="">Advance Express</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 76</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Barre Meets Yoga</li>
                                <li data-modal="#video" data-video="">Cardio Burn</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 77</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <li class="week">
            <div class="lockout">
                <h1>week 12</h1>
                <hr>
                <ul class="days">
                    <li class="day">
                        <div class="day-title">Day 78</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">FIT</li>
                                <li data-modal="#video" data-video="">Cardio Burn</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 79</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Cardio Sculpt</li>
                                <li data-modal="#video" data-video="">Muscle Medley</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 80</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Mat</li>
                                <li data-modal="#video" data-video="">Strong and Lean</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 81</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Chad Full Body</li>
                                <li data-modal="#video" data-video="">Chad Cardio</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 82</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Chad Full Body</li>
                                <li data-modal="#video" data-video="">Katie's Abs</li>
                                <li data-modal="#video" data-video="">Katie Stretch</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 83</div>
                        <div class="description">Mashup
                            <ol>
                                <li data-modal="#video" data-video="">Alicia Full Body</li>
                                <li data-modal="#video" data-video="">Ashly 15 Min</li>
                                <li data-modal="#video" data-video="">Brady 15 Min</li>
                            </ol>
                        </div>
                    </li>
                    <li class="day">
                        <div class="day-title">Day 84</div>
                        <div class="description">Rest Day</div>
                    </li>
                </ul>

            </div>
        </li>
        <div class="congratulations">
            <h4>You’ve completed this workout program and you’re looking great!</h4>

            <h2>Congratulations!</h2>
        </div>
    </ul>


</section>

<?php $this->renderPartial('//modals/video'); ?>


