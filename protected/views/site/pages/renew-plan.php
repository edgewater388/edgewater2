<?php $this->bodyClass = 'account renew-plan'; ?>

<section>
    <div class="lockout">
        <div class="side-lines">
            <h1>my account</h1>
        </div>
    </div>
    <div class="content">
        <div class="lockout">
            <h4>renew plan</h4>
            <hr>
            <div id="top-text">
                <p>We’re so glad you’re back!</p>
                <p>You’re just a few clicks away from beginning your transformation!!</p>
            </div>
            <ul id="account-types" data-radio-buttons="li">
                <li>
                    <img src="/themes/basic/images/account/renew-plan-thumb1.jpg">
                    <div class="text-overlay">
                        <h3><span class="dollar">$</span>57.00</h3>
                        <h5>monthly</h5>
                    </div>
                </li>
                <li class="active">
                    <img src="/themes/basic/images/account/renew-plan-thumb2.jpg">
                    <div class="text-overlay">
                        <h3><span class="dollar">$</span>5.00</h3>
                        <h5>daily rate</h5>
                        <h6>(drop in membership)</h6>
                    </div>
                </li>
            </ul>
            <p class="text">Membership fees are billed at the begining of each period. Sales tax may apply.</p>
            <div class="buttons">
                <div class="btn blue">continue</div>
            </div>

        </div>
    </div>
</section>
