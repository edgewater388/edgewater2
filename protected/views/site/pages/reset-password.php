<?php $this->bodyClass = 'account change-email short-nav short-nav-perm'; ?>

<section>
    <div class="lockout">
        <div class="side-lines">
            <h1>my account</h1>
        </div>
    </div>
    <div class="content">
        <div class="lockout">
            <h4>reset password</h4>
            <hr>
            <!-- <form action="" data-validate data-submit-btn="#submit-btn"> -->
            <?php $form=$this->beginWidget('CActiveForm', array(
                // 'action'=>Yii::app()->createUrl('/user/users/reset', array('email'=>Yii::app()->request->getParam('email'), 'code'=>Yii::app()->request->getParam('code'))),
                'htmlOptions'=>array(
                    'data-validate'=>'',
                    'data-submit-btn'=>'#submit-btn',
                ),
            )); ?>
                <div class="inputs">
                    <div class="input-item">
                        <!-- <label>new password</label> -->
                        <?php echo $form->labelEx($model,'new_password',array('label'=>'new password')); ?>
                        <!-- <input type="password" data-validate="min:5" /> -->
                        <?php echo $form->passwordField($model,'new_password',array('data-validate'=>'min:5')); ?>
                        <!--                        --><?php //echo $form->textField($model, 'email', array('data-validate'=>'min:5|email')); ?>
                        <!--                        --><?php //echo $form->error($model, 'email'); ?>
                    </div>
                    <div class="input-item">
                        <!-- <label>re-enter password</label> -->
                        <?php echo $form->labelEx($model, 'password2',array('label'=>'re-enter password')); ?>
                        <!-- <input type="password" data-validate="min:5"/> -->
                        <?php echo $form->passwordField($model, 'password2',array('data-validate'=>'min:5')); ?>
                        <!--                        --><?php //echo $form->passwordField($model, 'current_password', array('data-validate'=>'min:4')); ?>
                        <!--                        --><?php //echo $form->error($model, 'current_password'); ?>
                    </div>
                </div>
                <div class="buttons">
                    <input type="submit" class="btn blue" id="submit-btn" value="reset password">
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</section>
