<?php $this->bodyClass = 'sign-up no-top-padding'; ?>

<?php $this->renderPartial('//partials/crazy-egg-tracking'); ?>
<?php $this->renderPartial('//partials/google-start-free-trial'); ?>

<script src="https://cdn.optimizely.com/js/3845780290.js"></script>

<section class="signup-section" data-trial="1">
    <div class="container">
        <div class="headline">
            <h1>SIGN UP NOW TO GET <span>YOUR FREE MONTH TRIAL</span></h1>
        </div>
        <div class="row">
            <div class="col">
                <div class="plan-info"<?php if ($countdown_timestamp): ?> data-countdown-to="<?= $countdown_timestamp; ?>"<?php endif; ?>>
                    <h2>Your Plan</h2>
                    <span class="title">Your Free Trial includes:</span>
                    <ul class="check-list">
                        <li>
                            <span class="check"></span>
                            <strong>Unlimited access to hundreds of workouts</strong>
                        </li>
                        <li>
                            <span class="check"></span>
                            <strong>Stream on any device</strong>
                        </li>
                        <li>
                            <span class="check"></span>
                            <strong>Customizable programs designed to reach your goals</strong>
                        </li>
                    </ul>
                    <div class="price-info">
                        <div class="frame">
                            <span>Regular Price</span>
                            <span class="price">
                                <del>
                                    $<span class="number">30</span>/month
                                </del>
                            </span>
                        </div>
                        <div class="frame frame--strong">
                            <span>Limited time</span>
                            <span class="price">
                                $<span class="number"><?= $trial_price; ?></span>
                            </span>
                        </div>
                        <div class="frame">
                            <strong>Total Today</strong>
                            <strong>$0</strong>
                        </div>
                    </div>
                    <div class="wrapper-timer-plan">
                        <h4>OFFER ENDS IN</h4>
                        <div class="timer-plan">
                            <div class="number-container">
                                <span class="days number"></span>
                                <div class="smalltext">Days</div>
                            </div>
                            <p class="colon">:</p>
                            <div class="number-container">
                                <span class="hours number"></span>
                                <div class="smalltext">Hours</div>
                            </div>
                            <p class="colon">:</p>
                            <div class="number-container">
                                <span class="minutes number"></span>
                                <div class="smalltext">Minutes</div>
                            </div>
                            <p class="colon">:</p>
                            <div class="number-container">
                                <span class="seconds number"></span>
                                <div class="smalltext">Seconds</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php $form = $this->beginWidget('ActiveForm', [
				'action'      => '/sign-up-trial',
				'htmlOptions' => [
					'id'    => 'signup-form',
					'class' => 'col',
				],
			]); ?>
            <div class="step-box">
                <div class="head">
                    <span class="step-number">Step 1</span>
                    <div class="frame">
                        <h2>Account info</h2>
						<?php if ($is_guest): ?><a href="#" class="link hidden">Edit</a><?php endif; ?>
                    </div>
                </div>

                <div class="slide">
					<?php if ($is_guest): ?>
                        <div id="step1" class="login-form">
                            <p class="errors">
								<?= $form->errorSummary($model_user); ?>
                            </p>
                            <div class="input-holder">
								<?= $form->textField($model_user, 'email', [
									'maxlength'    => 128,
									'placeholder'  => 'Email Address',
									'class'        => 'email',
									'autocomplete' => 'off',
								]); ?>
                            </div>
                            <div class="input-holder">
								<?= $form->passwordField($model_user, 'password', [
									'maxlength'    => 128,
									'placeholder'  => 'Password',
									'class'        => 'form-control password',
									'autocomplete' => 'off',
								]); ?>
                            </div>
							<?= $form->hiddenField($model_user, 'referred_by'); ?>

                            <div class="sign-in-socials-wr">
                                <div class="sign-in-top"><span>OR</span></div>
                                <a class="social-btn social-btn--facebook" href="<?= $facebook_auth_link; ?>">
                                    <span class="icon-facebook-logo"></span>SIGN IN WITH FACEBOOK
                                </a>
                                <a class="social-btn social-btn--gplus" href="<?= $google_auth_link; ?>">
                                    <span class="icon-google"></span>SIGN IN WITH GOOGLE+
                                </a>
                            </div>

                            <button type="button" class="btn blue"><i class="icon-arrow-dotted"></i>
                                <span>Continue to next step</span>
                            </button>
                        </div>
					<?php endif; ?>
                </div>
                <strong class="text-complete <?php if ($is_guest): ?>hidden<?php endif; ?>">
                    COMPLETED
                </strong>
            </div>
            <div class="step-box">
                <div class="head">
                    <span class="step-number">Step 2</span>
                    <div class="frame">
                        <h2>profile info</h2>
                        <a href="#" class="link hidden">Edit</a>
                    </div>
                </div>
                <div class="slide <?php if ($is_guest): ?>hide-form<?php endif; ?>">
                    <div id="step2" class="payment-form">
                        <p class="errors">
							<?= $form->errorSummary($model_customer); ?>
                        </p>
						<?php if (empty($model_user->facebook_id || $model_user->google_id)): ?>
                            <div class="holder">
                                <div class="column">
                                    <div class="form-group">
										<?= $form->textField($model_customer, 'first_name', [
											'class'       => 'first_name',
											'placeholder' => 'First Name',
										]); ?>
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="form-group">
										<?= $form->textField($model_customer, 'last_name', [
											'class'       => 'last_name',
											'placeholder' => 'Last Name',
										]); ?>
                                    </div>
                                </div>
                            </div>
						<?php endif; ?>
                        <div class="input-holder mb25">
							<?= $form->textField($model_customer, 'promo_code', [
								'maxlength'   => 32,
								'placeholder' => 'Enter Promotional Code',
							]); ?>
                        </div>
                        <div class="checkbox-wrap">
                            <label for="over_18">
								<?= $form->checkbox($model_customer, 'over_18', [
									'id'    => 'over_18',
									'class' => 'over_18',
								]); ?>
                                <span class="fake-input"></span>
                                <span class="fake-label">
                                    I am over 18, and I agree to the above conditions and the
                                    <a href="http://physique57.com/terms-and-conditions/"
                                       target="_blank">Terms of Use</a> and
                                    <a href="http://physique57.com/privacy-legal/" target="_blank">Privacy / Cookies</a>.
                                </span>
                            </label>
                        </div>
                        <button type="submit" class="btn blue submit">
                            <i class="icon-arrow-dotted"></i><span>start free trial</span>
                        </button>
                    </div>
                </div>
            </div>
			<?php $this->endWidget(); ?>
        </div>
    </div>
</section>

<script type="text/javascript">
  $('#yw0').submit(function () {
    _kmq.push(['identify', $('#Users_email').val()]);
    _kmq.push(['record', 'Starts free trial', {'email': $('#Users_email').val()}]);
  });
</script>

<script>
  $('#yw0').submit(function () {
    woopra.identify({
      email: $('#Users_email').val(),
      name: "New User"
    });
    woopra.track("join_now", {});
  });
</script>
