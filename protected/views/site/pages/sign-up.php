<?php $this->bodyClass = 'sign-up no-top-padding'; ?>

<?php $this->renderPartial('//partials/crazy-egg-tracking'); ?>
<?php $this->renderPartial('//partials/google-start-free-trial'); ?>

<script src="https://cdn.optimizely.com/js/3845780290.js"></script>

<section class="signup-section" data-trial="0">
    <div class="container">
        <div class="headline">
            <h1>SIGN UP NOW TO GET <span>YOUR FREE MONTH TRIAL</span></h1>
        </div>
        <div class="row">
            <div class="col">
                <div class="plan-info"<?php if ($countdown_timestamp): ?> data-countdown-to="<?= $countdown_timestamp; ?>"<?php endif; ?>>
                    <h2>Your Plan</h2>
                    <span class="title">Your Free Trial includes:</span>
                    <ul class="check-list">
                        <li>
                            <span class="check"></span>
                            <strong>Unlimited access to hundreds of workouts</strong>
                        </li>
                        <li>
                            <span class="check"></span>
                            <strong>Stream on any device</strong>
                        </li>
                        <li>
                            <span class="check"></span>
                            <strong>Customizable programs designed to reach your goals</strong>
                        </li>
                    </ul>
                    <div class="price-info">
                        <div class="frame">
                            <span>Regular Price</span>
                            <span class="price">
                                <del>
                                    $<span class="number"><?= !empty($old_plan_cost = Plan::getPlan($selected_plan_id)['old_amount']) ? $old_plan_cost : 30; ?></span>/month
                                </del>
                            </span>
                        </div>
                        <div class="frame frame--strong">
                            <span>Limited time</span>
                            <span class="price">
                                $<span class="number"><?= !empty($plan_cost = Plan::getPlan($selected_plan_id)['amount']) ? $plan_cost : 19.95; ?></span>
                            </span>
                        </div>
                        <div class="frame">
                            <strong>Total Today</strong>
                            <strong>$0</strong>
                        </div>
                    </div>
                    <div class="wrapper-timer-plan">
                        <h4>OFFER ENDS IN</h4>
                        <div class="timer-plan">
                            <div class="number-container">
                                <span class="days number"></span>
                                <div class="smalltext">Days</div>
                            </div>
                            <p class="colon">:</p>
                            <div class="number-container">
                                <span class="hours number"></span>
                                <div class="smalltext">Hours</div>
                            </div>
                            <p class="colon">:</p>
                            <div class="number-container">
                                <span class="minutes number"></span>
                                <div class="smalltext">Minutes</div>
                            </div>
                            <p class="colon">:</p>
                            <div class="number-container">
                                <span class="seconds number"></span>
                                <div class="smalltext">Seconds</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php $form = $this->beginWidget('ActiveForm', [
				'action'      => '/sign-up',
				'htmlOptions' => [
					'id'    => 'signup-form',
					'class' => 'col',
				],
			]); ?>
            <div class="step-box">
                <div class="head">
                    <span class="step-number">Step 1</span>
                    <div class="frame">
                        <h2>Account info</h2>
						<?php if ($is_guest): ?><a href="#" class="link hidden">Edit</a><?php endif; ?>
                    </div>
                </div>

                <div class="slide">
					<?php if ($is_guest): ?>
                        <div id="step1" class="login-form">
                            <p class="errors">
								<?= $form->errorSummary($model_user); ?>
                            </p>
                            <div class="input-holder">
								<?= $form->textField($model_user, 'email', [
									'maxlength'    => 128,
									'placeholder'  => 'Email Address',
									'class'        => 'email',
									'autocomplete' => 'off',
								]); ?>
                            </div>
                            <div class="input-holder">
								<?= $form->passwordField($model_user, 'password', [
									'maxlength'    => 128,
									'placeholder'  => 'Password',
									'class'        => 'form-control password',
									'autocomplete' => 'off',
								]); ?>
                            </div>
							<?= $form->hiddenField($model_user, 'referred_by'); ?>

                            <div class="sign-in-socials-wr">
                                <div class="sign-in-top"><span>OR</span></div>
                                <a class="social-btn social-btn--facebook" href="<?= $facebook_auth_link; ?>">
                                    <span class="icon-facebook-logo"></span>SIGN IN WITH FACEBOOK
                                </a>
                                <a class="social-btn social-btn--gplus" href="<?= $google_auth_link; ?>">
                                    <span class="icon-google"></span>SIGN IN WITH GOOGLE+
                                </a>
                            </div>

                            <button type="button" class="btn blue"><i class="icon-arrow-dotted"></i>
                                <span>Continue to next step</span>
                            </button>
                        </div>
					<?php endif; ?>
                </div>
                <strong class="text-complete <?php if ($is_guest): ?>hidden<?php endif; ?>">COMPLETED</strong>
            </div>
            <div class="step-box">
                <div class="head">
                    <span class="step-number">Step 2</span>
                    <div class="frame">
                        <h2>payment info</h2>
                        <a href="#" class="link hidden">Edit</a>
                    </div>
                </div>

                <a href="#" data-modal="#payment">Why are asking for payment information now?</a>
                <div class="slide <?php if ($is_guest): ?>hide-form<?php endif; ?>">
                    <div id="step2" class="payment-form">
                        <p class="errors">
							<?= $form->errorSummary($model_customer); ?>
                        </p>
						<?php if (empty($model_user->facebook_id || $model_user->google_id)): ?>
                            <div class="holder">
                                <div class="column">
                                    <div class="form-group">
										<?= $form->textField($model_customer, 'first_name', [
											'class'       => 'first_name',
											'placeholder' => 'First Name',
										]); ?>
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="form-group">
										<?= $form->textField($model_customer, 'last_name', [
											'class'       => 'last_name',
											'placeholder' => 'Last Name',
										]); ?>
                                    </div>
                                </div>
                            </div>
						<?php endif; ?>
                        <div class="holder">
                            <div class="column">
                                <div class="form-group">
									<?= $form->textField($model_customer, 'street', [
										'class'       => 'street',
										'placeholder' => 'Street Address',
									]); ?>
                                </div>
                            </div>
                            <div class="column">
                                <div class="form-group">
									<?= $form->textField($model_customer, 'zipcode', [
										'maxlength'   => '12',
										'class'       => 'zipcode',
										'placeholder' => 'Zip Code',
									]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="holder">
                            <div class="column">
                                <div class="form-group">
                                    <span class="fake-select">
                                        <?= $form->dropDownList($model_customer, 'country', Location::countries()); ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="holder">
                            <div class="column">
                                <div class="form-group">
                                    <div class="input-frame">
										<?= $form->textField($model_customer, 'new_card_number', [
											'class'       => 'new_card_number',
											'placeholder' => 'Card Number',
										]); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="column">
                                <img class="cards" src="themes/basic/images/sign-up/credit-cards.png"
                                     alt="image description" width="146" height="22">
                            </div>
                        </div>
                        <div class="holder pt-10">
                            <div class="column lg">
                                <div class="form-group">
                                    <label for="input-07">Expiration Day</label>
                                    <div class="clearfix">
                                        <div class="select-col">
                                            <span class="fake-select">
                                                <?= $form->dropDownList($model_customer, 'card_month',
													Time::months(),
													[
														'id'    => 'expiry-month',
														'class' => 'card_month',
													]); ?>
                                            </span>
                                        </div>
                                        <div class="select-col">
                                            <span class="fake-select">
                                                <?= $form->dropDownList($model_customer, 'card_year', Time::years(), [
													'id' => 'expiry-year',
												]); ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column-holder">
                                <div class="column sm">
                                    <div class="form-group">
                                        <label for="input-06">Security Code</label>
                                        <div class="input-frame">
											<?= $form->passwordField($model_customer, 'card_security', [
												'class'     => 'form-control card_security',
												'maxlength' => 4,
											]); ?>
                                        </div>
                                        <div class="qmark">
                                            <span class="rollover">
                                                <img src="/themes/basic/images/account/credit-cards-hover@2x.png"
                                                     width="276">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="input-holder mb25">
							<?= $form->textField($model_customer, 'promo_code', [
								'maxlength'   => 32,
								'placeholder' => 'Enter Promotional Code',
							]); ?>
                        </div>
                        <button type="button" class="btn blue">
                            <i class="icon-arrow-dotted"></i>
                            <span>Continue to next step</span>
                        </button>
                        <p class="note">We take your privacy seriously We safeguard your payment information (such as
                            credit card number) with SSL (Secure Sockets Layer) when sent over the Internet. SSL
                            encrypts personal information, including your name and payment information, so that it
                            cannot be read in transit by a third party. We also store your payment information in an
                            encrypted format.</p>
                    </div>
                </div>
                <strong class="text-complete hidden">COMPLETED</strong>
            </div>
            <div class="step-box">
                <div class="head">
                    <span class="step-number">Step 3</span>
                    <div class="frame">
                        <h2>PLANS</h2>
                        <a href="#" class="link hidden">Edit</a>
                    </div>
                    <div class="frame pricing">
                        <p>Pricing applied after your free trial.</p>
                    </div>
                </div>
                <div class="slide hide-form">
                    <div id="step3">
                        <p class="errors">
							<?php
							if (isset($model_customer->errors['over_18']))
								echo $form->errorSummary($model_customer);
							?>
                        </p>
                        <div class="radio-holder">
							<?= Html::hiddenField('Plan[id]', 0); ?>
							<?php foreach ($plans as $plan): ?>
                                <div class="label-holder">
                                    <label for="radio-<?= $plan['id']; ?>">
                                        <input
                                                id="radio-<?= $plan['id']; ?>"
                                                type="radio"
                                                name="Plan[id]"
                                                class="plan_id"
                                                value="<?= $plan['id']; ?>"
                                                data-amount="<?= $plan['amount']; ?>"
                                                data-old-amount="<?= $plan['old_amount']; ?>"
												<?php if ($plan->is_default && !$plan->is_main_default): ?>checked="checked"<?php endif; ?>
                                        >
                                        <span class="fake-input"></span>
                                        <span class="fake-label">
                                            <?= $plan['title']; ?>
                                            <em>(<?= $plan['description']; ?>)</em>
                                        </span>
                                    </label>
                                </div>
							<?php endforeach; ?>
                        </div>
                        <div class="legal plan57"
                             style="<?= ($plan->is_main_default) ? 'display: block;' : 'display: none;'; ?>">
							<?php $this->renderPartial('//partials/legal-plan57'); ?>
                        </div>
                        <div class="legal plan30"
                             style="<?= ($plan->is_default && !$plan->is_main_default) ? 'display: block;' : 'display: none;'; ?>">
                            <p>
                                Your Physique 57 VOD membership, which starts with
                                a <?= Membership::getTrialDuration(); ?>
                                day free trial offer, will begin when you click Start Membership. Simply cancel
                                anytime within your trial period and you will not be charged. To cancel, go to "Your
                                Account" and click on "Cancel Membership."
                            </p>
							<?php $this->renderPartial('//partials/legal-plan30'); ?>
                        </div>
                        <div class="checkbox-wrap">
                            <label for="over_18">
								<?= $form->checkbox($model_customer, 'over_18', [
									'id'    => 'over_18',
									'class' => 'over_18',
								]); ?>
                                <span class="fake-input"></span>
                                <span class="fake-label">
                                    I am over 18, and I agree to the above conditions and the
                                    <a href="http://physique57.com/terms-and-conditions/"
                                       target="_blank">Terms of Use</a> and
                                    <a href="http://physique57.com/privacy-legal/" target="_blank">Privacy / Cookies</a>.
                                </span>
                            </label>
                        </div>
                        <button type="submit" class="btn blue submit">
                            <i class="icon-arrow-dotted"></i><span>start free trial</span>
                        </button>
                    </div>
                </div>
            </div>
			<?php $this->endWidget(); ?>
        </div>
    </div>
</section>

<?php $this->renderPartial('//modals/payment'); ?>

<script type="text/javascript">
  $('#yw0').submit(function () {
    _kmq.push(['identify', $('#Users_email').val()]);
    _kmq.push(['record', 'Starts free trial', {'email': $('#Users_email').val()}]);
  });
</script>

<script>
  $('#yw0').submit(function () {
    woopra.identify({
      email: $('#Users_email').val(),
      name: "New User"
    });
    woopra.track("join_now", {});
  });
</script>
