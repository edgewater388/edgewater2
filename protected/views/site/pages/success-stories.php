<?php $this->bodyClass = 'home success-stories no-top-padding'; ?>
<?php $this->css[] = '/themes/basic/css/success-stories.css'; ?>


<section id="hero">
    <div class="centered-text">
        <div class="inner">
            <h4 class="top-text">The best success story</h4>
            <h1 class="title">is your own</h1>
            <p>
	            Hitting the barre at Physique 57 means pushing your limits and opening yourself up to change. But trust when we tell you - it’s worth it. Our clients tell us all the time that they’ve never experienced workouts - or seen results - like ours. (And we’ll note here that they’re smiling when they do it!)
            </p>
            <div class="blue-dot-btn">
                <a href="#stories">
                    SUCCESS STORIES
                    <div class="border tl"></div>
                    <div class="border tr"></div>
                    <div class="border br"></div>
                    <div class="border bl"></div>
                </a>
            </div>
            <div class="scroll-cta mobile white">
                <div class="dot one animated"></div>
                <div class="dot two animated"></div>
                <div class="dot three animated"></div>
                <div class="arrow four animated"></div>
            </div>
        </div>
    </div>
    <div class="scroll-cta desktop white">
        <div class="dot one animated"></div>
        <div class="dot two animated"></div>
        <div class="dot three animated"></div>
        <div class="arrow four animated"></div>
    </div>
</section>

<section id="stories" class="padding">
	<a name="stories"></a>
    <header>
        <div class="lockout">
            <div class="side-lines">
                <h1>Success Stories</h1>
            </div>
        </div>
    </header>
    <div class="stories">
    	<div class="story">
    		<div class="photos">
	    		<div class="photo before">
	    			<img src="/themes/basic/images/success-stories/amanda-before.jpg" alt="">
	    			<div class="caption">before</div>
	    		</div>
	    		<div class="photo after">
	    			<img src="/themes/basic/images/success-stories/amanda-after.jpg" alt="">
	    			<div class="caption">after</div>
	    		</div>
    		</div>
    		<div class="details">
    			<div class="inner">
	    			<h4>Amanda G. / Age 31</h4>
	    			<div class="summary">From: New York / Age: 31 / Height: 5'7" / Weight Before: 228 / Weight After: 175 / Pounds Lost: 53*</div>
	    			<div class="side-lines"></div>
	    			<div class="weight-info">
	    				<div class="lost">lost</div>
	    				<div class="amount">53</div>
	    				<div class="lbs">lbs</div>
	    			</div>
	    			<div class="description">Amanda shares with us how Physique 57 helped her lose 53 lbs and 4 dress sizes before her wedding.*</div>

					<div class="buttons">
			            <div class="blue-dot-btn gray large">
			                <a href="http://physique57.com/blog/success-story-bride-barre/" id="see-more-btn" target="_blank">
			                    <div class="text">READ MORE</div>
			                    <div class="border tl"></div>
			                    <div class="border tr"></div>
			                    <div class="border br"></div>
			                    <div class="border bl"></div>
			                </a>
			            </div>
			        </div>
    			</div>
    		</div>
			<div class="clearfix"></div>
    	</div>

    	<div class="story">
    		<div class="photos">
	    		<div class="photo before">
	    			<img src="/themes/basic/images/success-stories/marissa-before.jpg" alt="">
	    			<div class="caption">before</div>
	    		</div>
	    		<div class="photo after">
	    			<img src="/themes/basic/images/success-stories/marissa-after.jpg" alt="">
	    			<div class="caption">after</div>
	    		</div>
    		</div>
    		<div class="details">
    			<div class="inner">
	    			<h4>Marissa G. / Age 27</h4>
	    			<div class="summary">From: New York, NY / Age: 27 / Height: 5'5" / Weight Before: 175 / Weight After: 140 Pounds / Lost: 35*</div>
	    			<div class="side-lines"></div>
	    			<div class="weight-info">
	    				<div class="lost">lost</div>
	    				<div class="amount">35</div>
	    				<div class="lbs">lbs</div>
	    			</div>
	    			<div class="description">After seeing a success story that we posted last month, Marissa G. emailed us with her personal success story of how she lost 35 lbs and 2 pant sizes from taking class in NYC. Here, she shares her story.*</div>

					<div class="buttons">
			            <div class="blue-dot-btn gray large">
			                <a href="http://physique57.com/blog/success-story-dropping-35-lbs-at-the-barre/" id="see-more-btn" target="_blank">
			                    <div class="text">READ MORE</div>
			                    <div class="border tl"></div>
			                    <div class="border tr"></div>
			                    <div class="border br"></div>
			                    <div class="border bl"></div>
			                </a>
			            </div>
			        </div>
    			</div>
    		</div>
			<div class="clearfix"></div>
    	</div>

    	<div class="story">
    		<div class="photos">
	    		<div class="photo before">
	    			<img src="/themes/basic/images/success-stories/amy-before.jpg" alt="">
	    			<div class="caption">before</div>
	    		</div>
	    		<div class="photo after">
	    			<img src="/themes/basic/images/success-stories/amy-after.jpg" alt="">
	    			<div class="caption">after</div>
	    		</div>
    		</div>
    		<div class="details">
    			<div class="inner">
	    			<h4>Amy R. / Age 32</h4>
	    			<div class="summary">From: Richmond, VA / Age: 32 / Height: 5'7" / Weight Before: 140 / Weight After: 130 / Pounds Lost: 10*</div>
	    			<div class="side-lines"></div>
	    			<div class="weight-info">
	    				<div class="lost">lost</div>
	    				<div class="amount">10</div>
	    				<div class="lbs">lbs</div>
	    			</div>
	    			<div class="description">After slowly gaining weight over the course of a few years, Amy decided it was time to make a change. Amy wanted to fit back into her “skinny” jeans. What she accomplished was so much more.*</div>

					<div class="buttons">
			            <div class="blue-dot-btn gray large">
			                <a href="http://physique57.com/blog/success-story-amy-r/" id="see-more-btn" target="_blank">
			                    <div class="text">READ MORE</div>
			                    <div class="border tl"></div>
			                    <div class="border tr"></div>
			                    <div class="border br"></div>
			                    <div class="border bl"></div>
			                </a>
			            </div>
			        </div>
    			</div>
    		</div>
			<div class="clearfix"></div>
    	</div>

    	<div class="story">
    		<div class="photos">
	    		<div class="photo before">
	    			<img src="/themes/basic/images/success-stories/lauren-before.jpg" alt="">
	    			<div class="caption">before</div>
	    		</div>
	    		<div class="photo after">
	    			<img src="/themes/basic/images/success-stories/lauren-after.jpg" alt="">
	    			<div class="caption">after</div>
	    		</div>
    		</div>
    		<div class="details">
    			<div class="inner">
	    			<h4>Lauren M. / Age 24</h4>
	    			<div class="summary">From: New York, NY / Age: 24 / Height: 5'4" / Pounds lost: 32*</div>
	    			<div class="side-lines"></div>
	    			<div class="weight-info">
	    				<div class="lost">lost</div>
	    				<div class="amount">32</div>
	    				<div class="lbs">lbs</div>
	    			</div>
	    			<div class="description">Lauren was looking to find a low impact workout that would allow her to lose 15 pounds, despite her knee injury. After four months of taking 5-6 Physique 57 classes a week, she lost double her goal—30 pounds, strengthened the muscles around her knees, and learned to love her body again!*</div>

					<div class="buttons">
			            <div class="blue-dot-btn gray large">
			                <a href="http://physique57.com/blog/success-story-lauren-m/" id="see-more-btn" target="_blank">
			                    <div class="text">READ MORE</div>
			                    <div class="border tl"></div>
			                    <div class="border tr"></div>
			                    <div class="border br"></div>
			                    <div class="border bl"></div>
			                </a>
			            </div>
			        </div>
    			</div>
    		</div>
			<div class="clearfix"></div>
    	</div>
    </div>

    <p id="disclaimer">*Results may vary based on each client's physical health, diet and exercise, and adherence to the Physique 57 program.</p>
</section>

<?php $this->renderPartial('//partials/home-logged-out/stream-workouts'); ?>

<?php return; ?>

<?php $this->renderPartial('//partials/home-logged-out/hero'); ?>
<?php $this->renderPartial('//partials/home-logged-out/real-results'); ?>
<?php $this->renderPartial('//partials/home-logged-out/regimens'); ?>
<?php $this->renderPartial('//partials/home-logged-out/create-custom-mashups'); ?>
<?php $this->renderPartial('//partials/home-logged-out/start-sculpting'); ?>

<?php $this->renderPartial('//modals/video'); ?>