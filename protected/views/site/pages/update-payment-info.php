<?php $this->bodyClass = 'account update-payment-info'; ?>

<section>
    <div class="lockout">
        <div class="side-lines">
            <h1>my account</h1>
        </div>
    </div>
    <div class="content">
        <div class="lockout">
            <h4>update your payment information</h4>
            <hr>
            <p>Your new payment method will be applied to your next billing cycle. Your monthly membership is
                billed on the first day of each billing period.</p>
            <div class="credit-card">
                <p>CREDIT CARD</p>
                <div class="cards"></div>
            </div>

            <?php $form=$this->beginWidget('ActiveForm', array(
                'action'=>Yii::app()->createUrl('/customer/update'),
                'htmlOptions'=>array(
                    'data-validate'=>'',
                ),
            )); ?>
                <?php echo $form->errorSummary($model); ?>
                <div class="inputs">
                    <div class="row">
                        <div class="input-item first">
                            <label>FIRST NAME <span class="must-match"><span class="darker-blue">*</span>Must match card</span></label>
                            <?php echo $form->textField($model, 'first_name', array('data-validate'=>'min:2')); ?>
                        </div>
                        <div class="input-item last">
                            <label>LAST NAME <span class="must-match"><span class="darker-blue">*</span>Must match card</span></label>
                            <?php echo $form->textField($model, 'last_name', array('data-validate'=>'min:2')); ?>
                        </div>
                    </div>
                    <div class="row">
                    <div class="input-item street first">
                        <label>STREET ADDRESS</label>
                        <?php echo $form->textField($model, 'street', array('data-validate' => 'min:2')); ?>
                    </div>
                    <div class="input-item zip">
                        <label>ZIP CODE</label>
                        <?php echo $form->textField($model, 'zipcode', array('maxlength' => '12')); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="input-item expiration-month" style="width: 31.81818%">
                        <label>COUNTRY</label>
                        <?php echo $form->dropDownList($model, 'country', Location::countries()); ?>
                        <div class="down-arrow"><img src="/themes/basic/images/global/down-arrow.svg"></div>
                    </div>

                    <script>
                    jQuery(document).ready(function($) {
                        updateZipcode();
                        $('select[name="Customer[country]"]').bind('change', function(e) {
                            updateZipcode();
                        });

                        function updateZipcode()
                        {
                            if ($('select[name="Customer[country]"]').val() == 'US')
                                $('.input-item.zip label').html('Zip Code');
                            else
                                $('.input-item.zip label').html('Postal Code');
                        }
                    });
                    </script>
                </div>
                    <div class="row">
                        <div class="input-item card lock">
                            <label>CARD NUMBER</label>
                            <?php echo $form->textField($model, 'new_card_number', array('data-validate'=>'min:13')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-item expiration-month">
                            <label>EXPIRATION DATE</label>
                            <?php echo $form->dropDownList($model, 'card_month', Time::months(), array('id'=>'expiry-month', 'data-validate'=>'min:1')); ?>
                            <div class="down-arrow"><img src="/themes/basic/images/global/down-arrow.svg"></div>
                        </div>
                        <div class="input-item expiration-year">
                            <label>&nbsp;</label>
                            <?php echo $form->dropDownList($model, 'card_year', Time::years(), array('id'=>'expiry-year', 'data-validate'=>'min:1')); ?>
                            <div class="down-arrow"><img src="/themes/basic/images/global/down-arrow.svg"></div>
                        </div>
                        <div class="input-item security-code lock">
                            <label>SECURITY CODE</label>
                            <!-- <input type="text" maxlength="4" data-validate="min:2"/> -->
                            <?php echo $form->textField($model, 'card_security', array('data-validate'=>'min:2')); ?>
                        </div>
                        <div class="input-item qmark">
                            <span class="rollover">
                                <img src="/themes/basic/images/account/credit-cards-hover@2x.png" width="276">
                </span>
                        </div>

                    </div>
                </div>

                <div class="buttons">
                    <input type="submit" class="btn blue" value="update payment method"/>
                    <a href="/account" class="btn">cancel</a>
                </div>
            <?php $this->endWidget(); ?>
            <div class="secure">
                <p>Secure Server <img src="/themes/basic/images/global/icon-lock@2x.png" width="26" alt="Secure"></p>
                <p class="tell">Tell me more
                <span class="rollover">
                    We take your privacy seriously We safeguard your payment information (such as credit card number)
                    with SSL (Secure Sockets Layer) when sent over the Internet. SSL encrypts personal information,
                    including your name and payment information, so that it cannot be read in transit by a third party.
                    We also store your payment information in an encrypted format.
                </span>
                </p>
            </div>

        </div>
    </div>
</section>
<?php
    if (empty(Yii::app()->user->customer) && !empty(Yii::app()->user->membership) && empty(Yii::app()->user->membership->prev_billing_date) && ($userMemberShipTrialEndDate = Yii::app()->user->membership->trial_end_date) != null && $userMemberShipTrialEndDate < date('Y-m-d')) {
        $this->renderPartial('//modals/trial-expired');
    }
