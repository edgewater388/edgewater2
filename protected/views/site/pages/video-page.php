<div class="modal-video" id="video">
    <div class="lockout">
        <h1><?php echo $video->title; ?></h1>
        <div class="modal-row modal-row--video">
            
<!--            <div class="inner video">
                <div class="player bc-player"></div>
            </div>-->
			<div>
				<img src="<?php echo $video->meta->thumbnailURL; ?>"
            </div>
            <div class="modal-video__description">
				<?php if (!empty($video->instructor->profile)):  ?>
                <div class="description-item">
                    <h3>YOUR INSTRUCTOR</h3>
                    <ul class="description-list">
                        <li><?php echo $video->instructor->profile->name ; ?></li>
                    </ul>
                </div>
				<?php endif; ?>
                <div class="description-item">
                    <h3>ABOUT THE WORKOUT</h3>
                    <dl class="definition-list">
                        <dt>Focus:</dt>
                        <dd><?php echo str_replace(',', ', ', $video->body_part); ?></dd>
                        <dt>Time:</dt>
                        <dd><?php echo $video->duration; ?> Minutes</dd>
                        <dt>Props:</dt>
                        <dd><?php echo str_replace(',', ', ', $video->equipment); ?></dd>
                    </dl>
                </div>
				<div class="description-item">
                    <h3>SHARE THIS WORKOUT</h3>
                    <?php $this->widget('application.widgets.socials.Socials', [
						'text' => "Hi everyone, I just did Physique 57's $video->title video. Help me stay motivated by liking this post!",
						'url' => $this->createAbsoluteUrl('videoPage/show', ['id' => $video->id]),
						'shares' => [
							[
								'share' => "facebook",
								'logo' => "fa fa-facebook",
								'label' => "facebook"
							],
							[
								'share' => "twitter",
								'logo' => "fa fa-twitter",
								'label' => "twitter",
								'shareUrl' =>  "https://twitter.com/share?url=''&text={text}"
							],
						]
					]); ?>
                </div>
<!--                <div class="description-item">
                    <h3>SHARE THIS WORKOUT</h3>
                    
                    <ul class="socials">
                        <li><a href="#" target="_blank" rel="noopener noreferrer"><img src="../../../themes/basic/images/modal-video/ico-facebook.png" alt="facebook" /></a></li>
                        <li><a href="#" target="_blank" rel="noopener noreferrer"><img src="../../../themes/basic/images/modal-video/ico-twitter.png" alt="facebook" /></a></li>
                    </ul>
                </div>-->
            </div>
        </div>
    </div>
</div>