<?php $this->bodyClass = 'account viewing-activity'; ?>

<section>
    <div class="lockout">
        <div class="side-lines">
            <h1>my account</h1>
        </div>
    </div>
    <div class="content">
        <div class="lockout">
            <h4>viewing activity</h4>
            <hr>
            <ul>
                <li>
                    <div class="date">Date</div>
                    <div class="video-title">Video</div>
                </li>
                <?php foreach ($models as $model): ?>
                    <li>
                        <div class="date"><?php echo Time::timeAgo(strtotime($model->created)); ?></div>
                        <div class="video-title"><?php echo $model->video ? $model->video->title : 'Video no longer availble'; ?></div>
                        <div class="problem"><a href="/contact-us?video_id=<?php echo $model->video_id; ?>">report a problem</a></div>
                    </li>
                <?php endforeach; ?>
                
                <?php if ($total_count == 0): ?>
                <li>
                    <span style="text-transform: none;">You haven't watched any videos yet</span>
                </li>
                <?php endif; ?>
            </ul>


            <?php $cur_page = isset($_GET['page']) ? $_GET['page'] : 1; ?>
            <?php if ($total_count > 0 && $total_count > ($cur_page * 10)): ?>
            <a class="btn" id="load-more-btn" href="<?php echo $this->createUrl('/user/users/activity', array('page'=>isset($_GET['page']) ? $_GET['page'] + 1 : 2)); ?>">load more</a>
            <?php endif; ?>

            <hr>
            <p class="asterisk"><span class="blue">*</span> We only show up to 3 months of viewing history</p>
            <a href="/account" class="btn">
                back to account
            </a>
        </div>
    </div>
</section>
