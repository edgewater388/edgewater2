<div class="modal-video" id="workout-program">
    <div class="lockout">
        <h1><?php echo $program->userRegimen->regimen->title; ?></h1>
        <div class="modal-row modal-row--video">
			<div>
				<img src="<?= $program->video->meta->videoStillURL; ?>">		
				<div class="modal-video__description">
					<div class="description-item">
						<h3>SHARE THIS WORKOUT PROGRAM</h3>
						<?php $this->widget('application.widgets.socials.Socials', [
							'text' => "Hi everyone, I am starting Physique 57's ".$program->getWeekNum()." week(s) workout program. Help me stay motivated during these ".$program->getWeekNum()." week(s) by liking this post!",
							'url' => $this->createAbsoluteUrl('regimenProgram/show', ['id' => $program->id]),
							'shares' => [
								[
									'share' => "facebook",
									'logo' => "fa fa-facebook",
									'label' => "facebook"
								],
								[
									'share' => "twitter",
									'logo' => "fa fa-twitter",
									'label' => "twitter",
									'shareUrl' =>  "https://twitter.com/share?url=''&text={text}"
								],
							]
						]);?>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>