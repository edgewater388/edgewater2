<?php
/* @var $this StripeCustomerController */
/* @var $model StripeCustomer */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('ActiveForm', array(
    'id'=>'stripe-customer-form',
    'enableAjaxValidation'=>false,
)); ?>

    <?php //echo $form->errorSummary($model); ?>

    <div class="row-fluid">
        <div class="span6">
			<div class="control-group">
				<?php echo $form->labelEx($model,'name'); ?>
				<div class="controls">
					<?php echo $form->textField($model,'name', array('class'=>'span12')); ?>
					<?php echo $form->error($model,'name'); ?>
				</div>
			</div>

            <div class="control-group">
				<?php echo $form->labelEx($model,'email'); ?>
				<div class="controls">
					<?php echo $form->textField($model,'email', array('class'=>'span12')); ?>
					<?php echo $form->error($model,'email'); ?>
				</div>
			</div>

			<div class="control-group">
				<?php echo $form->labelEx($model,'phone'); ?>
				<div class="controls">
					<?php echo $form->textField($model,'phone', array('class'=>'span12')); ?>
					<?php echo $form->error($model,'phone'); ?>
				</div>
			</div>
        </div>
        <div class="span6">
            <div class="control-group">
				<?php echo $form->labelEx($model,'address_line1'); ?>
				<div class="controls">
					<?php echo $form->textField($model,'address_line1', array('class'=>'span12')); ?>
					<?php echo $form->error($model,'address_line1'); ?>
				</div>
			</div>

            <div class="control-group">
				<?php echo $form->labelEx($model,'address_city'); ?>
				<div class="controls">
					<?php echo $form->textField($model,'address_city', array('class'=>'span12')); ?>
					<?php echo $form->error($model,'address_city'); ?>
				</div>
			</div>

			<div class="row-fluid">
				<div class="span3">
					<div class="control-group">
						<?php echo $form->labelEx($model,'address_state'); ?>
						<div class="controls">
							<?php echo $form->dropDownList($model,'address_state', array(''=>'')+Location::states()+Location::provinces(), array('class'=>'span12')); ?>
							<?php echo $form->error($model,'address_state'); ?>
						</div>
					</div>
				</div>
				<div class="span4">
					<div class="control-group">
						<?php echo $form->labelEx($model,'address_zip'); ?>
						<div class="controls">
							<?php echo $form->textField($model,'address_zip', array('class'=>'span12')); ?>
							<?php echo $form->error($model,'address_zip'); ?>
						</div>
					</div>
				</div>
				<div class="span5">
					<div class="control-group">
						<?php echo $form->labelEx($model,'address_country'); ?>
						<div class="controls">
							<?php echo $form->dropDownList($model,'address_country', Location::countries(), array('class'=>'span12')); ?>
							<?php echo $form->error($model,'address_country'); ?>
						</div>
					</div>
				</div>
			</div>
			
        </div>
    </div>

	<div class="divider"></div>

	<?php echo $form->hiddenField($model,'card_token'); ?>
	<?php echo $form->hiddenField($model,'fingerprint'); ?>
	<?php echo $form->hiddenField($model,'card_type'); ?>
    <?php echo $form->hiddenField($model,'exp_month'); ?>
    <?php echo $form->hiddenField($model,'exp_year'); ?>
    <?php echo $form->hiddenField($model,'last4'); ?>

    <div class="control-group text-center">
		<div class="controls form-inline">
			<?php echo $form->checkBox($model,'terms_accepted', array('style'=>'margin-top: 0;')); ?> 
			<?php echo CHtml::label('I accept the <a href="'.Yii::app()->createUrl('/site/page', array('view'=>'terms')).'" target="_blank">Terms of Use Agreement</a>', 'StripeCustomer_terms_accepted', array('uncheckValue' => '')); ?>
			<div class="clearfix"></div>
			<?php echo $form->error($model,'terms_accepted'); ?>
    	</div>
    </div>

    <div class="form-controls text-center buttons">
    	<a href="<?php echo Yii::app()->createUrl('/user/users/settings', array('id'=>Yii::app()->user->id)); ?>" class="btn btn-large btn-default" style="margin-right: 20px;">Cancel</a>
        <?php echo Html::submitButton('Save', array('class'=>'btn btn-large btn-alt')); ?>
    </div>

	<div class="divider divider-large"></div>
    <div class="divider divider-large"></div>

<?php $this->endWidget(); ?>

</div><!-- form -->

