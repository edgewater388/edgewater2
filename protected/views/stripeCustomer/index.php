<?php if ($model->hasErrors()): ?>
	<?php 
		$form = new ActiveForm;
		echo $form->errorSummary($model); 
	?>
<?php endif; ?>

<div class="pull-right secure-payment-wrapper">
	<?php $this->renderPartial('//blocks/securePayment'); ?>
</div>

<!-- <h3>Payment Information</h3> -->

<h4>Enter Your Payment Details</h4>
<?php $cardForm = new CardForm; ?>
<?php $this->renderPartial('//forms/card',array('model'=>$cardForm)); ?>

<div class="divider"></div>

<h4 style="display: inline; vertical-align: bottom;">Enter Your Billing Details</h4>
<span class="small italic light">(Please make sure billing details match what is on your credit card statement)</span>
<div class="divider divider-small"></div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>