<script type="text/template" id="video-editor-li-tpl">
    <li class="drag-li">
        <div class="play" data-modal="#video" data-video="">
            <div class="icon"></div>
        </div>
        <div class="title"></div>
        <div class="instructor"></div>
        <div class="type">
            <div class="icon strength white" title="strength"></div>
            <div class="icon cardio white" title="cardio"></div>
            <div class="icon restorative white" title="restorative"></div>
        </div>
        <div class="duration"> MIN</div>
        <div class="remove">
            <div class="text">REMOVE</div>
        </div>
        <div class="drag">
            <div class="icon drag-handle"></div>
        </div>
    </li>
</script>