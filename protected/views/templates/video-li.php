<script type="text/template" id="video-li-tpl">
    <li>
        <div class="thumb" data-modal="#video" data-video="">
            <img class="thumb-img" src="">
            <div class="icon time"> min</div>
            <div class="title-icon">
                <div class="title"></div>
                <div class="icon strength white" title="strength"></div>
                <div class="icon cardio white" title="cardio"></div>
                <div class="icon restorative white" title="restorative"></div>
            </div>
            <div class="add-favorite" title="Add to favorites">
                <div class="icon"></div>
<!--                <h5>add to favorites</h5>-->
                <div class="checkmark">
                    <img src="/themes/basic/images/global/icon-checkmark.png">
                </div>
            </div>
        </div>
        <div class="btn add-to-mashup">add to my mashup</div>
    </li>
</script>