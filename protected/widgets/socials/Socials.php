<?php

class Socials extends CWidget
{
	 private $_id;
	 private static $_counter = 0;
	 public $showLabel = false;
     public $showCount = false;
	 public	$shareIn = "popup";
	 public $disabled = false;
	 public $shares = [];
	 public $modalId = '';
	 public $text = '';
	 public $url = '';

	 public function init()
	{
		parent::init();

	}
	 
	/**
	 * @inheritdoc
	 */
	public function run()
	{
		parent::run();
		$clientScript = Yii::app()->getClientScript();
		$clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/socials/jssocials.js', CClientScript::POS_END);
		if (!$this->disabled) {
			$clientScript->registerScript($this->getId(), '	
			$("#'.$this->getId().'").jsSocials({
				showLabel: "'.$this->showLabel.'",
				showCount: "'.$this->showCount.'",
				shareIn: "'.$this->shareIn.'",
				text: "'.$this->text.'",
				url: "'.$this->url.'",	
				shares: '.json_encode($this->shares, true).'
			});
			', CClientScript::POS_END);
		}
		$this->render('socials_view', [
			'id' => $this->getId(),
		]);
	}
	
	public function getId($autoGenerate = true)
    {
        if ($this->_id !== null) {
            return $this->_id;
        } else {
            return $this->_id = 'social_widget_' . self::$_counter++;
        }
    }
}
