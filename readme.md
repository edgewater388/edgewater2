
# Backend #

URl: /backend

Database setup:
Create a new database, add a new user / password
Copy ./protected/config/database-sample.php to ./protected/config/database.php
Set the database name, user, password in database.php

Environment setup:
Copy ./protected/config/environment-sample.php to ./protected/config/environment.php
Nothing to change here.

Changes to the Database:
- Create migration script
From the command line and webroot, type: 
./protected/yiic migrate create DESCRIPTION_OF_CHANGE 
- Run the migration script
./protected/yiic migrate
- Reverse the migration script (please always test this)
./protected/yiic migrate down 1
* See ./protected/migrations for examples how to create a migration script (UP and DOWN)
* Note: when deploying changes to staging and production, the migration scripts will 
not automatically be run. You will need to run the migration script on the server after 
doing a git pull

# Front-End #

### Environment ###

Make sure you have these installed:

* Typescript $ npm install -g typescript
* Bower: $ npm install -g bower
* Gulp: $ npm install -g gulp
* Node-Sass: $ npm install -g node-sass

Init npm and bower:

* $ npm install
* $ bower install

### How to compile SASS ###

* $ node-sass --source-map true themes/basic/css/main.scss themes/basic/css/main.css
* Note: All Sass is compiled into two files (main.css and main.css.map)
* You can optionally use gulp-sass (https://www.npmjs.com/package/gulp-sass) as a watcher

### How to compile Typescript ###

* $ tsc --sourceMap -out themes/basic/js/global.js themes/basic/js/global.ts --target ES5
* Note: All Typescript is compiled into a single file (global.js)

### How to add additional 3rd party JavaScript plugins ###

* Add new library to bower such as: $ bower install some-packpage-name -S
* This adds the package to bower.json and installs files to the bower_components folder on the root
* Now run: $ gulp compileVendorJS
* This loops through all packages in bower.json and concats/minifies all the JavaScript into themes/basic/js/vendor/vendor.js
* Now the new library is ready to be used.
* If the typescript compiler complains about your newly added library when you go to use it, you can declare the class in themes/basic/js/typings/imports.d.ts. You can see examples in that file of other 3rd party libraries and how to declare them for typescript.

### REST API ##

* Implemented for users only

* View all users: api/users/code/offset/limit (HTTP method GET)
* View a single user: api/users/id/code (also GET)
* Create a new user: api/users/code (POST), attributes: email, password, first_name, last_name 
* Update a user: api/users/123/code (PUT)
* Delete a user: api/users/123/code (DELETE)
* Create/update payment info about user: api/customer/email/code (POST), attributes: 'service', 'token', 'customer_profile_id', 'customer_payment_profile_id',	'card_number', 'card_expiry', 'card_type', 'first_name', 'last_name', 'street',	'street1', 'city', 'state',	'country', 'zipcode' 
