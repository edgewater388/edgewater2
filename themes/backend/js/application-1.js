$(document).ready(function() { 

	$('textarea').autoResize({
	    // On resize:
	    onResize : function() {
	        //$(this).css({opacity:0.8});
	    },
	    // After resize:
	    animateCallback : function() {
	        //$(this).css({opacity:1});
	    },
	    // Quite slow animation:
	    animateDuration : 00,
	    // More extra space:
	    extraSpace : 20
	});
	
	$('span[rel=tooltip]').tooltip({
		placement: 'left',
		live: true
	});

	$('#modal').bind('hidden', function () {
		resetModal();
	})
	
	$('.dropdown-toggle').dropdown();
	
	$('#sidebar div.list div.yrow')
		.popover({
			placement: 'left',
			trigger: 'manual',
			html: true,
			offset: 0
		})
		.click( function() {
			if ($(this).hasClass('active-popover')) {
				$(this).popover('hide').removeClass('active-popover');
			}
			else if ($('div.popover').length > 0) {
				$('div.active-popover').popover('hide').removeClass('active-popover');
				$(this).addClass('active-popover');
				$(this).popover('show');
			}
			else {
			 	$(this).addClass('active-popover');
				$(this).popover('show');
			}
		});
		
	// Resize sidebar height to full content height
	// ie. sidebar height = container height less any padding (+height -innerHeight)
	// var container_height = $('section#content .container').height();
	// $('section#content #sidebar').css('min-height', container_height);

	
	////////////////////////////////////////////////////////
	// Ajaxify List Remove Link
	//
	$('div.list a.remove').die('click').live('click', function() {
		var elem = $(this);
		elem.parents('div.yrow').css('opacity',.5);
		$.getJSON(elem.attr('href'), function(data) {
			if (data.status == 'success') {
				elem.parents('div.yrow').fadeOut(300, function() {				
				//$(this).remove();
				});
			}
		});
		return false;
	});
	
});

function resetModal() {
	var modal = '<div id="modal" class="modal hide fade"><div class="modal-header"><a class="close" data-dismiss="modal">×</a><h3>Title</h3></div><div class="modal-body"><p>Body</p></div><div class="modal-footer"><a href="#" class="btn btn-primary primary">Primary</a><a href="#" class="btn secondary">Secondary</a></div></div>';
	$('#modal').replaceWith(modal);
	$('#modal').unbind('hidden').bind('hidden', function () {
		resetModal();
	})
}