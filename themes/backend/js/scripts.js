$(document).ready(function() { 

	$('textarea').autoResize({
	    // On resize:
	    onResize : function() {
	        //$(this).css({opacity:0.8});
	    },
	    // After resize:
	    animateCallback : function() {
	        //$(this).css({opacity:1});
	    },
	    // Quite slow animation:
	    animateDuration : 00,
	    // More extra space:
	    extraSpace : 20
	});
	
	$('span[rel=tooltip]').tooltip({
		placement: 'left',
		live: true
	});

	$('#modal').bind('hidden', function () {
		resetModal();
	})
	
	$('.dropdown-toggle').dropdown();	

	$('[data-toggle="clearForm"]').on('click', function(e) {
		clearForm($(this).parents('form'));
		e.preventDefault();
	});

	bindLinkColumnLinks();

	$('[data-toggle="tooltip"]').tooltip({
		trigger: 'hover'
	});

	$('ul.nav-tabs a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	});

	bindScrollableContainers();
});

function resetModal() {
	var modal = '<div id="modal" class="modal hide fade"><div class="modal-header"><a class="close" data-dismiss="modal">×</a><h3>Title</h3></div><div class="modal-body"><p>Body</p></div><div class="modal-footer"><a href="#" class="btn btn-primary primary">Primary</a><a href="#" class="btn secondary">Secondary</a></div></div>';
	$('#modal').replaceWith(modal);
	$('#modal').unbind('hidden').bind('hidden', function () {
		resetModal();
	})
}

function clearForm(form) {
	form.find('input[type="text"]').val('');
	form.find('input[type="hidden"]').val('');
	form.find('select').val('');
}

function bindLinkColumnLinks()
{
	var newTab = false;

	$(document).keydown(function(e) {
		// 91 = mac command, left
		// 93 = mac command, right
		if (e.keyCode == 91 || e.keyCode == 93 || e.ctrlKey)
			newTab = true;
	});

	$(document).keyup(function(e) {
		if (e.keyCode == 91 || e.keyCode == 93 || e.ctrlKey)
			newTab = false;
	});

	$.each($('td.link-column a'), function(i, elem) {
		$(elem).parents('tr').addClass('clickable').bind('click', function(e) {
			if (newTab)
				window.open($(elem).attr('href'));
			else
				window.location = $(elem).attr('href');
		});
	});

	$('tr.clickable').find('a').bind('click', function(e) {
		e.stopPropagation();
	});
}

function bindScrollableContainers()
{
	// $('.filters .form-content').hover(function(e) {
	// 	$('body').addClass('no-scroll');
	// }, function(e) {
	// 	$('body').removeClass('no-scroll');
	// });
}