/// <reference path="../typings/imports.d.ts" />

class Breakpoints {

    public static MOBILE:number = 0;
    public static BREAK_480:number = 480;
    public static TABLET:number = 700;
    public static DESKTOP:number = 769;
    public static LARGE:number = 1025;
    public static EXTRA_LARGE:number = 1300;

    public static mobileEntered:Signal = new signals.Signal();
    public static mobileExited:Signal = new signals.Signal();
    public static tabletEntered:Signal = new signals.Signal();
    public static tabletExited:Signal = new signals.Signal();
    public static desktopEntered:Signal = new signals.Signal();
    public static desktopExited:Signal = new signals.Signal();
    public static largeEntered:Signal = new signals.Signal();
    public static largeExited:Signal = new signals.Signal();
    public static extraLargeEntered:Signal = new signals.Signal();
    public static extraLargeExitedExited:Signal = new signals.Signal();
    public static landscapeEntered:Signal = new signals.Signal();
    public static landscapeExited:Signal = new signals.Signal();
    public static portraitEntered:Signal = new signals.Signal();
    public static portraitExited:Signal = new signals.Signal();

    public static breakpointChanged:Signal = new signals.Signal();
    public static current:number;
    public static isLandscape:boolean;
    public static isPortrait:boolean;

    public static init():void {

        // mobile
        enquire.register("screen and (min-width: " + Breakpoints.MOBILE + "px) and (max-width: " + Breakpoints.TABLET + "px)", {
            match: function () {
                Breakpoints.mobileEntered.dispatch();
                Breakpoints.current = Breakpoints.MOBILE;
                Breakpoints.breakpointChanged.dispatch(Breakpoints.current);
            },
            unmatch: function () {
                Breakpoints.mobileExited.dispatch();
            }
        });

        // tablet
        enquire.register("screen and (min-width: " + Breakpoints.TABLET + "px) and (max-width: " + Breakpoints.DESKTOP + "px)", {
            match: function () {
                Breakpoints.tabletEntered.dispatch();
                Breakpoints.current = Breakpoints.TABLET;
                Breakpoints.breakpointChanged.dispatch(Breakpoints.current);
            },
            unmatch: function () {
                Breakpoints.tabletExited.dispatch();
            }
        });

        // desktop
        enquire.register("screen and (min-width: " + Breakpoints.DESKTOP + "px) and (max-width: " + Breakpoints.LARGE + "px)", {
            match: function () {
                Breakpoints.desktopEntered.dispatch();
                Breakpoints.current = Breakpoints.DESKTOP;
                Breakpoints.breakpointChanged.dispatch(Breakpoints.current);
            },
            unmatch: function () {
                Breakpoints.desktopExited.dispatch();
            }
        });

        // large
        enquire.register("screen and (min-width: " + Breakpoints.LARGE + "px) and (max-width: " + Breakpoints.EXTRA_LARGE + "px)", {
            match: function () {
                Breakpoints.largeEntered.dispatch();
                Breakpoints.current = Breakpoints.LARGE;
                Breakpoints.breakpointChanged.dispatch(Breakpoints.current);
            },
            unmatch: function () {
                Breakpoints.largeExited.dispatch();
            }
        });

        // extra-large
        enquire.register("screen and (min-width: " + Breakpoints.EXTRA_LARGE + "px)", {
            match: function () {
                Breakpoints.extraLargeEntered.dispatch();
                Breakpoints.current = Breakpoints.EXTRA_LARGE;
                Breakpoints.breakpointChanged.dispatch(Breakpoints.current);
            },
            unmatch: function () {
                Breakpoints.extraLargeExitedExited.dispatch();
            }
        });

        // landscape
        enquire.register("screen and (orientation: landscape)", {
            match: function () {
                Breakpoints.landscapeEntered.dispatch();
                Breakpoints.isLandscape = true;
            },
            unmatch: function () {
                Breakpoints.landscapeExited.dispatch();
                Breakpoints.isLandscape = false;
            }
        });

        // portrait
        enquire.register("screen and (orientation: portrait)", {
            match: function () {
                Breakpoints.portraitEntered.dispatch();
                Breakpoints.isPortrait = true;
            },
            unmatch: function () {
                Breakpoints.portraitExited.dispatch();
                Breakpoints.isPortrait = false;
            }
        });

    }

}
