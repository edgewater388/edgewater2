class Environment {

    public static devDomains:string[] = [
        '10.0.1.45'
    ];

    public static init():void {
        Environment.addHomesteadDomain();
    }

    public static get dev():boolean {
        for (var i = 0; i < Environment.devDomains.length; i++) {
            if (window.location.href.indexOf(Environment.devDomains[i]) > -1) {
                return true;
            }
        }
        return false;
    }

    private static addHomesteadDomain():void {
        if (window.location.href.indexOf('.app:8000') > -1) {
            Environment.devDomains.push(document.domain)
        }
    }

}
