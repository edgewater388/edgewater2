class ArrayUtil {

    public static shuffle(array:any):any {
        for (var j, x, i = array.length; i; j = Math.floor(Math.random() * i), x = array[--i], array[i] = array[j], array[j] = x);
        return array;
    }

    public static toggleItem(array, value):any {
        var index = array.indexOf(value);
        if (index === -1) {
            array.push(value);
        } else {
            array.splice(index, 1);
        }
    }

    public static isInArray(value, array):boolean {
        return array.indexOf(value) > -1;
    }

}
