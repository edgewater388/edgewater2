class ButtonHelper {

    static init():void {
        ButtonHelper.initRadioButtons();
    }

    private static initRadioButtons():void {
        $('[data-radio-buttons]').each((i, el)=> {
            var $radioButtons:JQuery = $(el).children();
            $radioButtons.on('click', (e)=>{
                Tick.callback(()=>{
                    if ($(e.currentTarget).parent().data('radio-buttons') == "deselect") {
                        var clickedOnActiveButton:boolean = $(e.currentTarget).hasClass('active');
                        if (clickedOnActiveButton) {
                            $(e.currentTarget).removeClass('active');
                        } else {
                            $(e.currentTarget).parent().find('li').removeClass('active');
                            $(e.currentTarget).toggleClass('active');
                        }
                    } else {
                        $(e.currentTarget).parent().find('li').removeClass('active');
                        $(e.currentTarget).toggleClass('active');
                    }
                });
            });
        });

    }

}
