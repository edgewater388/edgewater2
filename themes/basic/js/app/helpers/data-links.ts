class DataLinks extends View {
  private $links:any;
  private $internalLink:any;

  public init():void {
    this.initSelectors();
    this.initDatepicker();
  }

  private initSelectors():void {
    this.$links = $("[data-href]");
  }

  private initDatepicker():void {
    this.$links.on("click", (e:any) => {
      this.$internalLink = $(e.target);
      this.pageReload(e, this.$internalLink.attr("data-href"));
    });
  }

  private pageReload(e:any, link:any):void {
    e.preventDefault();

    window.location.href = window.location.origin + "" + link;
  }
}
