var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Datepicker = (function (_super) {
    __extends(Datepicker, _super);
    function Datepicker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Datepicker.prototype.init = function () {
        this.initSelectors();
        this.initDatepicker();
    };
    Datepicker.prototype.initSelectors = function () {
        this.$datepicker = $(".datepicker");
    };
    Datepicker.prototype.initDatepicker = function () {
        var _this = this;
        var date = new Date(), year = date.getFullYear();
        yearEnd = new Date().getFullYear() - 18;
        this.$datepicker.each(function (index, el) {
            _this.$datepickerLocal = $(el);
            _this.$datepickerLocal.datepicker({
                changeYear: true,
                changeMonth: true,
                yearRange: '1930:' + yearEnd
            });
        });
    };
    return Datepicker;
}(View));
