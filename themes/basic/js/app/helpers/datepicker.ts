class Datepicker extends View {
  private $datepicker:any;
  private $datepickerLocal:any;

  public init():void {
    this.initSelectors();
    this.initDatepicker();
  }

  private initSelectors():void {
    this.$datepicker = $(".datepicker");
  }

  private initDatepicker():void {
    let date:any = new Date(),
      year:number = date.getFullYear(),
      yearEnd = new Date().getFullYear() -18;

    date.setFullYear(yearEnd);
    
    this.$datepicker.each((index:number, el:any) => {
      this.$datepickerLocal = $(el);

      this.$datepickerLocal.datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: '1930:' + yearEnd,
        defaultDate: date,
        onChangeMonthYear: function(year, monts) {
          if (typeof this.value !== 'undefined' && this.value.length > 0) {
            let dateArr = this.value.split('/');
            let selectedMonth = ""+monts;
            selectedMonth = selectedMonth.length === 1 ? '0'+selectedMonth : selectedMonth;

            this.value = `${selectedMonth}/${dateArr[1]}/${year}`;
          }
        }
      });
    });
  }
}
