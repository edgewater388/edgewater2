class FormHelper {

    static init():void {
        FormHelper.initSubmitButtons();
        FormHelper.initFormValidation();
        FormHelper.initFocusBlur();
    }

    private static initSubmitButtons():void {
        var $forms = $('[data-submit-btn]');
        $forms.each((i, el)=> {
            var $curForm:JQuery = $(el);
            var $submitBtn:JQuery = $curForm.find($($(el).data('submit-btn')));
            $submitBtn.on('click', (e)=> {
                $curForm.submit();
            });
        });
    }

    private static initFormValidation():void {
        $('[data-validate]').each(function(i, el) {
            var $form:JQuery = $(el);
            $form.submit(function(e, forceSubmit){
                if(forceSubmit) return;
                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();
                FormValidator.validate($form, function($form) {
                    $form.trigger('submit', [true]);
                })
            })
        });
    }

    private static initFocusBlur():void {
        $('input, textarea').on('focus', (e)=> {
            $(e.currentTarget).addClass('focused');
        });
        $('input, textarea').on('blur', (e)=> {
            $(e.currentTarget).removeClass('focused').removeClass('error');
        });
    }

}
