class FormSubmit {

    static init():void {
        var $forms = $('[data-submit-btn]');
        $forms.each((i, el)=> {
            var $curForm:JQuery = $(el);
            var $submitBtn:JQuery = $curForm.find($($(el).data('submit-btn')));
            $submitBtn.on('click', (e)=> {
                $curForm.submit();
            });
        });
    }

}
