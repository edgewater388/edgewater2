class FormValidator {

    public static errors:JQuery[];

    public static validate($form:JQuery, $successCallback:any):void {

        FormValidator.errors = [];
        var $errorMessages:JQuery = $form.find('.error-messages');
        $errorMessages.find("li").hide();
        $errorMessages.hide();

        var $inputs:JQuery = $form.find('input,textarea,select');
        $inputs.removeClass('error');

        var isValid:boolean = true;
        $inputs.each((i, val) => {
            var $input:JQuery = $(val);
            if (!FormValidator.isValidInput($input)) {
                isValid = false;
                FormValidator.errors.push($input);
                $input.addClass('error');
            }
        });

        if(FormValidator.errors.length) {
            $errorMessages.show();
            for (var i = 0; i < FormValidator.errors.length; i++) {
                var $input:JQuery = FormValidator.errors[i];
                var className:string = $input.attr('class').split(' ')[0];
                $errorMessages.find('.' + className).show();
            }
        } else {
            $successCallback($form);
        }
    }

    private static isValidInput($el:JQuery):boolean {
        try {
            if (!$el.data("validate")) {
                return true;
            }
            var isValid:boolean = true;
            var value:string = $el.val();
            var data = $el.data("validate");
            var validators:string[] = this.extractValidatorStrings(data);

            for (var i = 0; i < validators.length; i++) {
                var validator:string = validators[i];
                if (!FormValidator.isValidEmail(value, validator)) {
                    isValid = false
                }
                if (!FormValidator.isMinimumLength(value, validator)) {
                    isValid = false
                }
                if (!FormValidator.isMaximumLength(value, validator)) {
                    isValid = false
                }
                if (!FormValidator.isMatch(value, validator)) {
                    isValid = false
                }
                if (!FormValidator.isPhone(value, validator)) {
                    isValid = false
                }
                if (!FormValidator.isChecked($el, validator)) {
                    isValid = false
                }
                if (!FormValidator.isNumber(value, validator)) {
                    isValid = false;
                }
                if (!FormValidator.hasNumber(value, validator)) {
                    isValid = false;
                }
                if (!FormValidator.isMinValue(value, validator)) {
                    isValid = false;
                }
                if (!FormValidator.isMaxValue(value, validator)) {
                    isValid = false;
                }
            }

            return isValid;

        } catch (e) {
            return false;
        }
    }

    private static extractValidatorStrings(data:string):string[] {
        return data.split("|");
    }

    private static isValidEmail(email:string, validator:string):boolean {
        if (validator != "email") return true;
        var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(email);
    }

    private static isMinimumLength(value:string, validator:string):boolean {
        if (validator.indexOf("min:") == -1) return true;
        var minimum:number = <any>validator.split(":")[1];
        return value.length >= minimum;
    }

    private static isMaximumLength(value:string, validator:string):boolean {
        if (validator.indexOf("max:") == -1) return true;
        var maximum:number = <any>validator.split(":")[1];
        return value.length <= maximum;
    }

    private static isMatch(value:string, validator:string):boolean {
        if (validator.indexOf("matches:") == -1) return true;
        var selector:string = <any>validator.split(":")[1];
        return value == $(selector).val();
    }

    private static isChecked($el:JQuery, validator:string):boolean {
        if (validator.indexOf("checked") == -1) return true;
        return $el.is(":checked");
    }

    private static isPhone(value:string, validator:string):boolean {
        if (validator.indexOf("phone") == -1) return true;
        var re = /\D+/g;
        var cleanphone = value.replace(re, "");
        return (cleanphone.length >= 10);
    }

    private static isNumber(value:any, validator:string):boolean {
        if (validator != "number") return true;
        return !isNaN(parseInt(value));
    }

    private static hasNumber(value:any, validator:string):boolean {
        if (validator != "has-number") return true;
        var matches = value.match(/\d+/g);
        if (matches) {
            return true;
        }
        return false;
    }

    private static isMinValue(value:any, validator:string):boolean {
        if (validator.indexOf("minValue:") == -1) return true;
        var minimum:number = <any>validator.split(":")[1];
        return !isNaN(parseInt(value)) && parseInt(value) >= minimum;
    }

    private static isMaxValue(value:any, validator:string):boolean {
        if (validator.indexOf("maxValue:") == -1) return true;
        var maximum:number = <any>validator.split(":")[1];
        return !isNaN(parseInt(value)) && parseInt(value) <= maximum;
    }

}
