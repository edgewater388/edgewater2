class Guide {

    private guideImagePath:string;
    private $guideParent:JQuery;
    private $guide:JQuery;
    private $dimensions:JQuery;
    protected top:number = 0;
    protected left:number = 0;

    public constructor() {
        //if (window.location.href.indexOf('.app:8000') == -1) return;
        this.initVars();
        this.initEvents();
        this.createGuide();
        this.createDimensions();

    }

    private initVars():void {
        this.$guideParent = $('[data-guide]');
        this.guideImagePath = this.$guideParent.data('guide');
        this.top = this.$guideParent.data('guide-top') ? parseInt(this.$guideParent.data('guide-top')) : this.top;
        this.left = this.$guideParent.data('guide-left') ? parseInt(this.$guideParent.data('guide-left')) : this.left;
    }

    private initEvents():void {
        $(document).keydown((evt)=> {
            if (evt.keyCode == 13) {
                this.showGuide();
                this.showDimensions();
            }
        }).keyup((evt)=> {
            evt.preventDefault();
            if (evt.keyCode == 13) {
                this.hideGuide();
                this.hideDimensions();
            }
        });
        $(window).on('resize', ()=>this.onResize());
    }

    private createGuide():void {
        this.$guide = $('<img>').attr({
            'src': this.guideImagePath,
            'id': 'guide'
        }).hide().css({top: this.top, left: this.left, position: 'absolute', zIndex: 10000});
        this.$guideParent.remove('#guide').append(this.$guide).css({position: 'relative'});
    }

    private createDimensions():void {
        this.$dimensions = $('<div>').attr({
            'id': 'dimensions'
        }).hide().css({
            top: 0,
            right: 0,
            position: 'fixed',
            zIndex: 10001,
            padding: '12px 20px',
            backgroundColor: '#3d99c8',
            color: '#fff',
            fontSize: 24,
            fontFamily: 'arial'
        });
        this.$guideParent.remove('#dimensions').append(this.$dimensions).css({position: 'relative'});
    }

    private showGuide():void {
        this.$guide.show();
    }

    private hideGuide():void {
        this.$guide.hide();
    }

    private showDimensions():void {
        this.udpateDimensions();
        this.$dimensions.show();
    }

    private hideDimensions():void {
        this.$dimensions.hide();
    }

    private udpateDimensions():void {
        this.$dimensions.html($(window).width() + ' x ' + $(window).height());
    }

    private onResize():any {
        this.udpateDimensions();
    }
}
