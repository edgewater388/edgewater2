/// <reference path="../../typings/imports.d.ts" />

class Helpers {

    public static init():void {
        new Guide();
        Environment.init();
        ResponsiveImages.init();
        SizingHelper.init();
        Parallax.init();
        Breakpoints.init();
        ButtonHelper.init();
        FormHelper.init();
        VideoHelper.init();
        Lightbox.initThumbs();
        ScrollTo.init();
        $('input').placeholder();
        $(window).on('load', ()=> {
            new FastClick(document.body);
        })
        $('.fade-in-on-load').on('load', function (e) {
            $(e.currentTarget).css({visibility: 'visible', opacity: 0}).transition({opacity: 1}, 1500);
        })

    }

}
