class Lightbox {

    private $lightbox:JQuery;
    private $closeBtn:JQuery;
    private documentKeyUp:any;
    private lightboxClick:any;
    private closeBtnClick:any;
    private prevBtnClick:any;
    private nextBtnClick:any;
    private index:number;
    private totalItems:number;
    private $prevBtn:JQuery;
    private $nextBtn:JQuery;

    public static initThumbs():void {
        $('[data-lightbox]').each((i, el)=> {
            var $lightboxRootElement:JQuery = $(el);
            var isLi:boolean = $lightboxRootElement.is('li');
            var $thumbs:JQuery = isLi ? $lightboxRootElement.parent().find('li') : $lightboxRootElement;
            $lightboxRootElement.on('click', (e)=> {
                new Lightbox($(e.currentTarget), $thumbs);
            });
        });
    }

    public constructor(private $currentThumbTarget:JQuery, private $thumbs:JQuery) {
        this.index = $currentThumbTarget.index();
        this.totalItems = $thumbs.length;
        this.appendMarkup();
        this.setSelectors();
        this.initEvents();
        this.loadImage();
        this.fadeInLightbox();
        this.hidePrevNextArrowsIfOneItem();
    }

    private appendMarkup():void {
        var html:string = $('#lightbox-tpl').html();
        this.$lightbox = $(html);
        $('body').append(this.$lightbox);
    }

    private setSelectors():void {
        this.$closeBtn = this.$lightbox.find('.close-btn');
        this.$prevBtn = this.$lightbox.find('.prev-btn');
        this.$nextBtn = this.$lightbox.find('.next-btn');
    }

    private initEvents():void {
        this.closeBtnClick = this.$closeBtn.on('click', ()=>this.fadeOutLightbox());
        this.prevBtnClick = this.$prevBtn.on('click', ()=>this.previous());
        this.nextBtnClick = this.$nextBtn.on('click', ()=>this.next());
        this.lightboxClick = this.$lightbox.on('click', (e)=>this.onAnyClickInLightbox(e));
        this.documentKeyUp = $(document).on('keyup', (e)=>this.onKeyUp(e));
    }

    private fadeInLightbox():void {
        this.$lightbox.transition({opacity: 1}, 500);
    }

    private fadeOutLightbox():void {
        this.$lightbox.transition({opacity: 0}, 500, ()=>this.destroy());
    }

    private loadImage():void {
        var imagePath:string = $(this.$thumbs.get(this.index)).data('lightbox')
        var $img:JQuery = this.$lightbox.find('figure').find('img');
        $img.fadeOut(500, ()=> {
            $img.attr('src', imagePath);
            $img.load(()=> {
                $img.fadeIn(500);
            })
        });
    }

    private onAnyClickInLightbox(e:any):any {
        if (!$(e.target).is('img')) {
            this.fadeOutLightbox();
        }
    }

    private onKeyUp(e):void {
        switch (e.which) {
            case 27: // esc
                this.fadeOutLightbox();
                break;

            case 37: // left
                this.previous();
                break;

            case 39: // right
                this.next();
                break;
        }
    }

    private previous():void {
        if (this.totalItems == 1) return;
        this.index = this.index == 0 ? this.totalItems - 1 : this.index -= 1;
        this.loadImage();
    }

    private next():void {
        if (this.totalItems == 1) return;
        this.index = this.index == this.totalItems - 1 ? 0 : this.index += 1;
        this.loadImage();
    }

    private destroy():void {
        this.closeBtnClick.unbind();
        this.prevBtnClick.unbind();
        this.nextBtnClick.unbind();
        this.lightboxClick.unbind();
        this.documentKeyUp.unbind();
        this.$lightbox.remove();
        this.$lightbox = null;
    }

    private hidePrevNextArrowsIfOneItem():void {
        if (this.totalItems == 1) {
            this.$prevBtn.hide();
            this.$nextBtn.hide();
        }
    }
}
