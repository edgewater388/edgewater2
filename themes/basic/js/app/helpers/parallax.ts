class Parallax {

    private static $window:JQuery = $(window);
    private static $els:JQuery;

    public static init():void {
        if (cssua.ua.safari < 8) return;
        if(Browser.isTouch) return;
        Parallax.$els = $('[data-parallax]');
        Parallax.initEvents();
        Parallax.$els.each((i, el)=> {
            var $item:JQuery = $(el);
            Parallax.setParallax($item);
        });
    }

    public static initEvents():void {
        //this.$window.scroll($.throttle(20, (e)=>this.onScroll(e)));
        Parallax.$window.scroll((e)=>Parallax.onScroll(e));
        //if (cssua.ua.safari < 8) {
        //    this.$window.on('touchmove', (e)=>Parallax.onScroll(e))
        //}
    }

    private static onScroll(e):void {
        Parallax.$els.each((i, el)=> {
            Parallax.setParallax($(el));
        });
    }

    private static setParallax($el:JQuery):void {
        if ($el.data('parallax-mobile') == false) {
            if(Breakpoints.current == Breakpoints.MOBILE) return;
        }
        if (Parallax.$window.scrollTop() < Parallax.$window.height()) {
            var targetY:number = Parallax.$window.scrollTop() / $el.data('parallax');
            //var targetY:number = $el.position().top - Parallax.$window.scrollTop() / $el.data('parallax');
            var force3D:boolean = $el.data('force3d');
            if(force3D == undefined) {
                force3D = true;
            }
            //TweenMax.set($el, {css: {y: -targetY, force3D: force3D}});
            $el.css({y:-targetY});
        }
        //  var targetY:number = (this.$winATrip.position().top - this.$window.scrollTop()) / 4;
    }

}

