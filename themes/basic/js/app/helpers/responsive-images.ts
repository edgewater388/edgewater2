class ResponsiveImages {

    private static $imgTags:JQuery;
    private static $backgroundDivs:JQuery;

    public static init():void {
        ResponsiveImages.initImages();
        ResponsiveImages.initBackgroundImages();
    }

    private static initImages() {
        ResponsiveImages.$imgTags = $('img[data-mobile]');
        if (ResponsiveImages.$imgTags.length) {
            Breakpoints.breakpointChanged.add(function (currentBreakpoint) {
                if (currentBreakpoint == Breakpoints.MOBILE) {
                    ResponsiveImages.$imgTags.each(function (i, val) {
                        var $this:JQuery = $(this);
                        if ($this.attr('src') && $this.attr('src').indexOf($this.data('mobile')) > -1) return;
                        $this.attr('src', $this.data('mobile'));
                    });
                } else {
                    ResponsiveImages.$imgTags.each(function (i, val) {
                        var $this:JQuery = $(this);
                        if ($this.attr('src') && $this.attr('src').indexOf($this.data('desktop')) > -1) return;
                        $this.attr('src', $this.data('desktop'));
                    });
                }
            });
        }
    }

    private static initBackgroundImages() {
        ResponsiveImages.$backgroundDivs = $('div[data-mobile], section[data-mobile], nav[data-mobile], footer[data-mobile]');
        if (ResponsiveImages.$backgroundDivs.length) {
            Breakpoints.breakpointChanged.add(function (currentBreakpoint) {
                if (currentBreakpoint == Breakpoints.MOBILE) {
                    ResponsiveImages.$backgroundDivs.each(function (i, val) {
                        var $this:JQuery = $(this);
                        if ($this.css('backgroundImage').indexOf($this.data('mobile')) > -1) return;
                        $this.css({backgroundImage: 'url(' + $this.data('mobile') + ')'});
                    });
                } else {
                    ResponsiveImages.$backgroundDivs.each(function (i, val) {
                        var $this:JQuery = $(this);
                        if ($this.css('backgroundImage').indexOf($this.data('desktop')) > -1) return;
                        $this.css({backgroundImage: 'url(' + $this.data('desktop') + ')'});
                    });
                }
            });
        }
    }

}
