class ScrollTo {

    public static init():void {
        var $els = $('[data-scroll-to]');
        $els.each((i, el)=> {
            var $item:JQuery = $(el);
            $item.on('click', (e)=>{
                var scrollTarget = $(e.currentTarget).data('scroll-to');
                TweenMax.to(window, 0.8, { scrollTo:{y:$(scrollTarget).offset().top + 1}, ease:Quint.easeInOut});
            });
        });
    }

}
