class SizingHelper {

    public static init(holder = $('[data-match-tallest]')):void {
        SizingHelper.matchTallest(holder);
        SizingHelper.matchWidest();
    }

    public static matchTallest(holder):void {
        var $els = holder;
        var sizeHeight = function() {
            $els.each((i, el)=> {
                var $el:JQuery = $($(el).data('match-tallest').split('|')[0], el);
                var breakpoint:string = $(el).data('match-tallest').split('|')[1];
                if (Browser.matchMediaLessThan(Breakpoints[breakpoint])) {
                    $el.css({"height": "auto"});
                } else {
                    var tallest:number = 0;
                    $el.css({"height": "auto"});
                    for (var i = 0; i < $el.length; i++) {
                        var $li = $($el[i]);
                        if ($li.outerHeight() > tallest) tallest = $li.outerHeight();
                    }
                    $el.css({"height": tallest});
                }
            });
        }
        $els.imagesLoaded().done(() => {
            sizeHeight();
        });
        $(window).on('resize', <any>$.throttle(250, sizeHeight));
        $(window).on('resize', <any>$.debounce(250, sizeHeight));
        $(window).on('load', ()=> {
            sizeHeight();
        });
    }

    public static matchWidest():void {
        var $els = $('[data-match-widest]');
        var sizeWidth = function() {
            $els.each((i, el)=> {
                var $el:JQuery = $($(el).data('match-widest').split('|')[0], el);
                var breakpoint:string = $(el).data('match-widest').split('|')[1];
                if (Browser.matchMediaLessThan(Breakpoints[breakpoint])) {
                    $el.css({"width": "auto"});
                } else {
                    var tallest:number = 0;
                    $el.css({"width": "auto"});
                    for (var i = 0; i < $el.length; i++) {
                        var $li = $($el[i]);
                        if ($li.outerWidth() > tallest) tallest = $li.outerWidth();
                    }
                    $el.css({"width": tallest});
                }
            });
        }
        sizeWidth();
        $(window).on('resize', <any>$.throttle(250, sizeWidth));
        $(window).on('resize', <any>$.debounce(250, sizeWidth));
        $(window).on('load', ()=> {
            sizeWidth();
        });
    }

}
