class Template {

    public static mustache(template:string, data:any):string {
        var pattern = new RegExp("{{" + "\\s*(" + "[a-z0-9_$][\\.a-z0-9_]*" + ")\\s*" + "}}", "gi");
        return template.replace(pattern, function (tag, token) {
            var path = token.split("."),
                len = path.length,
                lookup = data,
                i = 0;

            for (; i < len; i++) {
                lookup = lookup[path[i]];
                if (i === len - 1) {
                    return lookup;
                }
            }
        });

    }
}