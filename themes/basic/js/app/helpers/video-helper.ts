class VideoHelper {

    public static init():void {
        VideoHelper.maintainIframeAspectRatio();
    }

    private static maintainIframeAspectRatio():void {
        $('[data-maintain-aspect-ratio]').each((i, el)=> {
            var $item:JQuery = $(el);
            $item.wrap('<div class="maintain-aspect-ratio"></div>');
        });
        
        $('.maintain-aspect-ratio').each((i, el)=> {
            var $item:JQuery = $(el);
            var $iframe:JQuery = $item.find('iframe');
            var bottomPadding:number = (parseInt($iframe.attr('height')) / parseInt($iframe.attr('width'))) * 100;
            $item.css({paddingBottom: bottomPadding + '%'});
        });
    }

}