class Application {
    private static _instance:Application = null;
    private models:any[] = [];
    private views:any[] = [];
    private controllers:any[] = [];

    constructor() {
        if (Application._instance) {
            throw new Error("Error: Instantiation failed: Use application.getInstance() instead of new.");
        }
        Application._instance = this;
    }

    public static getInstance():Application {
        if (Application._instance === null) {
            Application._instance = new Application();
        }
        return Application._instance;
    }

    public createModel(model:any):any {
        if (this.getModel(model)) throw new Error("Attempted to add duplicate model:" + model);
        var modelInstance = new model();
        this.models.push(modelInstance);
        modelInstance.instantiate();
        return modelInstance;
    }

    public createView(view:any):any {
        if (this.getView(view)) throw new Error("Attempted to add duplicate view:" + view);
        var viewInstance = new view();
        this.views.push(viewInstance);
        viewInstance.instantiate();
        return viewInstance;
    }

    public createController(controller:any):any {
        if (this.getController(controller)) throw new Error("Attempted to add duplicate controller:" + controller);
        var controllerInstance = new controller();
        this.controllers.push(controllerInstance);
        controllerInstance.instantiate();
        return controllerInstance;
    }

    public getModel(model):any {
        for (var i = 0; i < this.models.length; i++) {
            if (this.models[i] instanceof model) return this.models[i];
        }
        return null;
    }

    public getView(view):any {
        for (var i = 0; i < this.views.length; i++) {
            if (this.views[i] instanceof view) return this.views[i];
        }
        return null;
    }

    public getController(controller):any {
        for (var i = 0; i < this.controllers.length; i++) {
            if (this.controllers[i] instanceof controller) return this.controllers[i];
        }
        return null;
    }
}

class BaseClass {
    public app:Application = Application.getInstance();
    constructor() {
        this['init']();
    }
}

class Model implements Subscribable {
    public app:Application = Application.getInstance();
    public subscribers:Updatable[] = [];

    public instantiate():void {
        if (this['init']) this['init']();
    }

    subscribe(handler:any, priority:number = 1000):void {
        var handler:any = new Handler(handler, priority);
        this.subscribers.push(handler)
    }

    notify(data?:any):void {
        this.subscribers.sort(this.sortSubscribersByPriority);
        for (var i = 0; i < this.subscribers.length; i++) {
            this.subscribers[i].update(data);
        }
    }

    private sortSubscribersByPriority(a:Updatable, b:Updatable):number {
        return (a.priority - b.priority);
    }

    unsubscribe(subscriber:Updatable):void {
        for (var i = 0; i < this.subscribers.length; i++) {
            if (this.subscribers[i] === subscriber) {
                this.subscribers.splice(i, 1);
                return;
            }
        }
    }
}

interface Initable {
    init():void
}

interface Subscribable {
    subscribe(subscriber:Updatable, Initable):void;
    unsubscribe(subscriber:Updatable):void;
    notify(data?:any):void
}

interface Updatable {
    priority?:number;
    update(data?:any):void;
    id?:string;
}

class Controller {
    public app:Application = Application.getInstance();
    public instantiate():void {
        if (this['init']) this['init']();
    }
}

class Handler implements Updatable {
    update(data:any):void {
        this.callback(data);
    }
    constructor(public callback:any, public priority:number = 10000) {
    }
}

class View {
    public app:Application = Application.getInstance();
    protected $window:JQuery = $(window);
    protected $document:JQuery = $(document);
    protected $body:JQuery = $('body');
    protected $pageWrap:JQuery = $('#page-wrap');
    public instantiate():void {
        if (this['init']) this['init']();
        //this.$window.on('resize', ()=>this.onResize());
        //this.$window.on('load', (e)=>this.onWindowLoad(e));
    }

}
