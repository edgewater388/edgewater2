var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Application = (function () {
    function Application() {
        this.models = [];
        this.views = [];
        this.controllers = [];
        if (Application._instance) {
            throw new Error("Error: Instantiation failed: Use application.getInstance() instead of new.");
        }
        Application._instance = this;
    }
    Application.getInstance = function () {
        if (Application._instance === null) {
            Application._instance = new Application();
        }
        return Application._instance;
    };
    Application.prototype.createModel = function (model) {
        if (this.getModel(model))
            throw new Error("Attempted to add duplicate model:" + model);
        var modelInstance = new model();
        this.models.push(modelInstance);
        modelInstance.instantiate();
        return modelInstance;
    };
    Application.prototype.createView = function (view) {
        if (this.getView(view))
            throw new Error("Attempted to add duplicate view:" + view);
        var viewInstance = new view();
        this.views.push(viewInstance);
        viewInstance.instantiate();
        return viewInstance;
    };
    Application.prototype.createController = function (controller) {
        if (this.getController(controller))
            throw new Error("Attempted to add duplicate controller:" + controller);
        var controllerInstance = new controller();
        this.controllers.push(controllerInstance);
        controllerInstance.instantiate();
        return controllerInstance;
    };
    Application.prototype.getModel = function (model) {
        for (var i = 0; i < this.models.length; i++) {
            if (this.models[i] instanceof model)
                return this.models[i];
        }
        return null;
    };
    Application.prototype.getView = function (view) {
        for (var i = 0; i < this.views.length; i++) {
            if (this.views[i] instanceof view)
                return this.views[i];
        }
        return null;
    };
    Application.prototype.getController = function (controller) {
        for (var i = 0; i < this.controllers.length; i++) {
            if (this.controllers[i] instanceof controller)
                return this.controllers[i];
        }
        return null;
    };
    Application._instance = null;
    return Application;
}());
var BaseClass = (function () {
    function BaseClass() {
        this.app = Application.getInstance();
        this['init']();
    }
    return BaseClass;
}());
var Model = (function () {
    function Model() {
        this.app = Application.getInstance();
        this.subscribers = [];
    }
    Model.prototype.instantiate = function () {
        if (this['init'])
            this['init']();
    };
    Model.prototype.subscribe = function (handler, priority) {
        if (priority === void 0) { priority = 1000; }
        var handler = new Handler(handler, priority);
        this.subscribers.push(handler);
    };
    Model.prototype.notify = function (data) {
        this.subscribers.sort(this.sortSubscribersByPriority);
        for (var i = 0; i < this.subscribers.length; i++) {
            this.subscribers[i].update(data);
        }
    };
    Model.prototype.sortSubscribersByPriority = function (a, b) {
        return (a.priority - b.priority);
    };
    Model.prototype.unsubscribe = function (subscriber) {
        for (var i = 0; i < this.subscribers.length; i++) {
            if (this.subscribers[i] === subscriber) {
                this.subscribers.splice(i, 1);
                return;
            }
        }
    };
    return Model;
}());
var Controller = (function () {
    function Controller() {
        this.app = Application.getInstance();
    }
    Controller.prototype.instantiate = function () {
        if (this['init'])
            this['init']();
    };
    return Controller;
}());
var Handler = (function () {
    function Handler(callback, priority) {
        if (priority === void 0) { priority = 10000; }
        this.callback = callback;
        this.priority = priority;
    }
    Handler.prototype.update = function (data) {
        this.callback(data);
    };
    return Handler;
}());
var View = (function () {
    function View() {
        this.app = Application.getInstance();
        this.$window = $(window);
        this.$document = $(document);
        this.$body = $('body');
        this.$pageWrap = $('#page-wrap');
    }
    View.prototype.instantiate = function () {
        if (this['init'])
            this['init']();
        //this.$window.on('resize', ()=>this.onResize());
        //this.$window.on('load', (e)=>this.onWindowLoad(e));
    };
    return View;
}());
var Environment = (function () {
    function Environment() {
    }
    Environment.init = function () {
        Environment.addHomesteadDomain();
    };
    Object.defineProperty(Environment, "dev", {
        get: function () {
            for (var i = 0; i < Environment.devDomains.length; i++) {
                if (window.location.href.indexOf(Environment.devDomains[i]) > -1) {
                    return true;
                }
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Environment.addHomesteadDomain = function () {
        if (window.location.href.indexOf('.app:8000') > -1) {
            Environment.devDomains.push(document.domain);
        }
    };
    Environment.devDomains = [
        '10.0.1.45'
    ];
    return Environment;
}());
/// <reference path="../typings/imports.d.ts" />
var Breakpoints = (function () {
    function Breakpoints() {
    }
    Breakpoints.init = function () {
        // mobile
        enquire.register("screen and (min-width: " + Breakpoints.MOBILE + "px) and (max-width: " + Breakpoints.TABLET + "px)", {
            match: function () {
                Breakpoints.mobileEntered.dispatch();
                Breakpoints.current = Breakpoints.MOBILE;
                Breakpoints.breakpointChanged.dispatch(Breakpoints.current);
            },
            unmatch: function () {
                Breakpoints.mobileExited.dispatch();
            }
        });
        // tablet
        enquire.register("screen and (min-width: " + Breakpoints.TABLET + "px) and (max-width: " + Breakpoints.DESKTOP + "px)", {
            match: function () {
                Breakpoints.tabletEntered.dispatch();
                Breakpoints.current = Breakpoints.TABLET;
                Breakpoints.breakpointChanged.dispatch(Breakpoints.current);
            },
            unmatch: function () {
                Breakpoints.tabletExited.dispatch();
            }
        });
        // desktop
        enquire.register("screen and (min-width: " + Breakpoints.DESKTOP + "px) and (max-width: " + Breakpoints.LARGE + "px)", {
            match: function () {
                Breakpoints.desktopEntered.dispatch();
                Breakpoints.current = Breakpoints.DESKTOP;
                Breakpoints.breakpointChanged.dispatch(Breakpoints.current);
            },
            unmatch: function () {
                Breakpoints.desktopExited.dispatch();
            }
        });
        // large
        enquire.register("screen and (min-width: " + Breakpoints.LARGE + "px) and (max-width: " + Breakpoints.EXTRA_LARGE + "px)", {
            match: function () {
                Breakpoints.largeEntered.dispatch();
                Breakpoints.current = Breakpoints.LARGE;
                Breakpoints.breakpointChanged.dispatch(Breakpoints.current);
            },
            unmatch: function () {
                Breakpoints.largeExited.dispatch();
            }
        });
        // extra-large
        enquire.register("screen and (min-width: " + Breakpoints.EXTRA_LARGE + "px)", {
            match: function () {
                Breakpoints.extraLargeEntered.dispatch();
                Breakpoints.current = Breakpoints.EXTRA_LARGE;
                Breakpoints.breakpointChanged.dispatch(Breakpoints.current);
            },
            unmatch: function () {
                Breakpoints.extraLargeExitedExited.dispatch();
            }
        });
        // landscape
        enquire.register("screen and (orientation: landscape)", {
            match: function () {
                Breakpoints.landscapeEntered.dispatch();
                Breakpoints.isLandscape = true;
            },
            unmatch: function () {
                Breakpoints.landscapeExited.dispatch();
                Breakpoints.isLandscape = false;
            }
        });
        // portrait
        enquire.register("screen and (orientation: portrait)", {
            match: function () {
                Breakpoints.portraitEntered.dispatch();
                Breakpoints.isPortrait = true;
            },
            unmatch: function () {
                Breakpoints.portraitExited.dispatch();
                Breakpoints.isPortrait = false;
            }
        });
    };
    Breakpoints.MOBILE = 0;
    Breakpoints.BREAK_480 = 480;
    Breakpoints.TABLET = 700;
    Breakpoints.DESKTOP = 769;
    Breakpoints.LARGE = 1025;
    Breakpoints.EXTRA_LARGE = 1300;
    Breakpoints.mobileEntered = new signals.Signal();
    Breakpoints.mobileExited = new signals.Signal();
    Breakpoints.tabletEntered = new signals.Signal();
    Breakpoints.tabletExited = new signals.Signal();
    Breakpoints.desktopEntered = new signals.Signal();
    Breakpoints.desktopExited = new signals.Signal();
    Breakpoints.largeEntered = new signals.Signal();
    Breakpoints.largeExited = new signals.Signal();
    Breakpoints.extraLargeEntered = new signals.Signal();
    Breakpoints.extraLargeExitedExited = new signals.Signal();
    Breakpoints.landscapeEntered = new signals.Signal();
    Breakpoints.landscapeExited = new signals.Signal();
    Breakpoints.portraitEntered = new signals.Signal();
    Breakpoints.portraitExited = new signals.Signal();
    Breakpoints.breakpointChanged = new signals.Signal();
    return Breakpoints;
}());
var Guide = (function () {
    function Guide() {
        this.top = 0;
        this.left = 0;
        //if (window.location.href.indexOf('.app:8000') == -1) return;
        this.initVars();
        this.initEvents();
        this.createGuide();
        this.createDimensions();
    }
    Guide.prototype.initVars = function () {
        this.$guideParent = $('[data-guide]');
        this.guideImagePath = this.$guideParent.data('guide');
        this.top = this.$guideParent.data('guide-top') ? parseInt(this.$guideParent.data('guide-top')) : this.top;
        this.left = this.$guideParent.data('guide-left') ? parseInt(this.$guideParent.data('guide-left')) : this.left;
    };
    Guide.prototype.initEvents = function () {
        var _this = this;
        $(document).keydown(function (evt) {
            if (evt.keyCode == 13) {
                _this.showGuide();
                _this.showDimensions();
            }
        }).keyup(function (evt) {
            evt.preventDefault();
            if (evt.keyCode == 13) {
                _this.hideGuide();
                _this.hideDimensions();
            }
        });
        $(window).on('resize', function () { return _this.onResize(); });
    };
    Guide.prototype.createGuide = function () {
        this.$guide = $('<img>').attr({
            'src': this.guideImagePath,
            'id': 'guide'
        }).hide().css({ top: this.top, left: this.left, position: 'absolute', zIndex: 10000 });
        this.$guideParent.remove('#guide').append(this.$guide).css({ position: 'relative' });
    };
    Guide.prototype.createDimensions = function () {
        this.$dimensions = $('<div>').attr({
            'id': 'dimensions'
        }).hide().css({
            top: 0,
            right: 0,
            position: 'fixed',
            zIndex: 10001,
            padding: '12px 20px',
            backgroundColor: '#3d99c8',
            color: '#fff',
            fontSize: 24,
            fontFamily: 'arial'
        });
        this.$guideParent.remove('#dimensions').append(this.$dimensions).css({ position: 'relative' });
    };
    Guide.prototype.showGuide = function () {
        this.$guide.show();
    };
    Guide.prototype.hideGuide = function () {
        this.$guide.hide();
    };
    Guide.prototype.showDimensions = function () {
        this.udpateDimensions();
        this.$dimensions.show();
    };
    Guide.prototype.hideDimensions = function () {
        this.$dimensions.hide();
    };
    Guide.prototype.udpateDimensions = function () {
        this.$dimensions.html($(window).width() + ' x ' + $(window).height());
    };
    Guide.prototype.onResize = function () {
        this.udpateDimensions();
    };
    return Guide;
}());
var Tick = (function () {
    function Tick() {
    }
    Tick.callback = function (callback) {
        setTimeout(function () {
            callback();
        }, 100);
    };
    return Tick;
}());
var Template = (function () {
    function Template() {
    }
    Template.mustache = function (template, data) {
        var pattern = new RegExp("{{" + "\\s*(" + "[a-z0-9_$][\\.a-z0-9_]*" + ")\\s*" + "}}", "gi");
        return template.replace(pattern, function (tag, token) {
            var path = token.split("."), len = path.length, lookup = data, i = 0;
            for (; i < len; i++) {
                lookup = lookup[path[i]];
                if (i === len - 1) {
                    return lookup;
                }
            }
        });
    };
    return Template;
}());
var SizingHelper = (function () {
    function SizingHelper() {
    }
    SizingHelper.init = function (holder) {
        if (holder === void 0) { holder = $('[data-match-tallest]'); }
        SizingHelper.matchTallest(holder);
        SizingHelper.matchWidest();
    };
    SizingHelper.matchTallest = function (holder) {
        var $els = holder;
        var sizeHeight = function () {
            $els.each(function (i, el) {
                var $el = $($(el).data('match-tallest').split('|')[0], el);
                var breakpoint = $(el).data('match-tallest').split('|')[1];
                if (Browser.matchMediaLessThan(Breakpoints[breakpoint])) {
                    $el.css({ "height": "auto" });
                }
                else {
                    var tallest = 0;
                    $el.css({ "height": "auto" });
                    for (var i = 0; i < $el.length; i++) {
                        var $li = $($el[i]);
                        if ($li.outerHeight() > tallest)
                            tallest = $li.outerHeight();
                    }
                    $el.css({ "height": tallest });
                }
            });
        };
        $els.imagesLoaded().done(function () {
            sizeHeight();
        });
        $(window).on('resize', $.throttle(250, sizeHeight));
        $(window).on('resize', $.debounce(250, sizeHeight));
        $(window).on('load', function () {
            sizeHeight();
        });
    };
    SizingHelper.matchWidest = function () {
        var $els = $('[data-match-widest]');
        var sizeWidth = function () {
            $els.each(function (i, el) {
                var $el = $($(el).data('match-widest').split('|')[0], el);
                var breakpoint = $(el).data('match-widest').split('|')[1];
                if (Browser.matchMediaLessThan(Breakpoints[breakpoint])) {
                    $el.css({ "width": "auto" });
                }
                else {
                    var tallest = 0;
                    $el.css({ "width": "auto" });
                    for (var i = 0; i < $el.length; i++) {
                        var $li = $($el[i]);
                        if ($li.outerWidth() > tallest)
                            tallest = $li.outerWidth();
                    }
                    $el.css({ "width": tallest });
                }
            });
        };
        sizeWidth();
        $(window).on('resize', $.throttle(250, sizeWidth));
        $(window).on('resize', $.debounce(250, sizeWidth));
        $(window).on('load', function () {
            sizeWidth();
        });
    };
    return SizingHelper;
}());
var FormValidator = (function () {
    function FormValidator() {
    }
    FormValidator.validate = function ($form, $successCallback) {
        FormValidator.errors = [];
        var $errorMessages = $form.find('.error-messages');
        $errorMessages.find("li").hide();
        $errorMessages.hide();
        var $inputs = $form.find('input,textarea,select');
        $inputs.removeClass('error');
        var isValid = true;
        $inputs.each(function (i, val) {
            var $input = $(val);
            if (!FormValidator.isValidInput($input)) {
                isValid = false;
                FormValidator.errors.push($input);
                $input.addClass('error');
            }
        });
        if (FormValidator.errors.length) {
            $errorMessages.show();
            for (var i = 0; i < FormValidator.errors.length; i++) {
                var $input = FormValidator.errors[i];
                var className = $input.attr('class').split(' ')[0];
                $errorMessages.find('.' + className).show();
            }
        }
        else {
            $successCallback($form);
        }
    };
    FormValidator.isValidInput = function ($el) {
        try {
            if (!$el.data("validate")) {
                return true;
            }
            var isValid = true;
            var value = $el.val();
            var data = $el.data("validate");
            var validators = this.extractValidatorStrings(data);
            for (var i = 0; i < validators.length; i++) {
                var validator = validators[i];
                if (!FormValidator.isValidEmail(value, validator)) {
                    isValid = false;
                }
                if (!FormValidator.isMinimumLength(value, validator)) {
                    isValid = false;
                }
                if (!FormValidator.isMaximumLength(value, validator)) {
                    isValid = false;
                }
                if (!FormValidator.isMatch(value, validator)) {
                    isValid = false;
                }
                if (!FormValidator.isPhone(value, validator)) {
                    isValid = false;
                }
                if (!FormValidator.isChecked($el, validator)) {
                    isValid = false;
                }
                if (!FormValidator.isNumber(value, validator)) {
                    isValid = false;
                }
                if (!FormValidator.hasNumber(value, validator)) {
                    isValid = false;
                }
                if (!FormValidator.isMinValue(value, validator)) {
                    isValid = false;
                }
                if (!FormValidator.isMaxValue(value, validator)) {
                    isValid = false;
                }
            }
            return isValid;
        }
        catch (e) {
            return false;
        }
    };
    FormValidator.extractValidatorStrings = function (data) {
        return data.split("|");
    };
    FormValidator.isValidEmail = function (email, validator) {
        if (validator != "email")
            return true;
        var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(email);
    };
    FormValidator.isMinimumLength = function (value, validator) {
        if (validator.indexOf("min:") == -1)
            return true;
        var minimum = validator.split(":")[1];
        return value.length >= minimum;
    };
    FormValidator.isMaximumLength = function (value, validator) {
        if (validator.indexOf("max:") == -1)
            return true;
        var maximum = validator.split(":")[1];
        return value.length <= maximum;
    };
    FormValidator.isMatch = function (value, validator) {
        if (validator.indexOf("matches:") == -1)
            return true;
        var selector = validator.split(":")[1];
        return value == $(selector).val();
    };
    FormValidator.isChecked = function ($el, validator) {
        if (validator.indexOf("checked") == -1)
            return true;
        return $el.is(":checked");
    };
    FormValidator.isPhone = function (value, validator) {
        if (validator.indexOf("phone") == -1)
            return true;
        var re = /\D+/g;
        var cleanphone = value.replace(re, "");
        return (cleanphone.length >= 10);
    };
    FormValidator.isNumber = function (value, validator) {
        if (validator != "number")
            return true;
        return !isNaN(parseInt(value));
    };
    FormValidator.hasNumber = function (value, validator) {
        if (validator != "has-number")
            return true;
        var matches = value.match(/\d+/g);
        if (matches) {
            return true;
        }
        return false;
    };
    FormValidator.isMinValue = function (value, validator) {
        if (validator.indexOf("minValue:") == -1)
            return true;
        var minimum = validator.split(":")[1];
        return !isNaN(parseInt(value)) && parseInt(value) >= minimum;
    };
    FormValidator.isMaxValue = function (value, validator) {
        if (validator.indexOf("maxValue:") == -1)
            return true;
        var maximum = validator.split(":")[1];
        return !isNaN(parseInt(value)) && parseInt(value) <= maximum;
    };
    return FormValidator;
}());
var FormHelper = (function () {
    function FormHelper() {
    }
    FormHelper.init = function () {
        FormHelper.initSubmitButtons();
        FormHelper.initFormValidation();
        FormHelper.initFocusBlur();
    };
    FormHelper.initSubmitButtons = function () {
        var $forms = $('[data-submit-btn]');
        $forms.each(function (i, el) {
            var $curForm = $(el);
            var $submitBtn = $curForm.find($($(el).data('submit-btn')));
            $submitBtn.on('click', function (e) {
                $curForm.submit();
            });
        });
    };
    FormHelper.initFormValidation = function () {
        $('[data-validate]').each(function (i, el) {
            var $form = $(el);
            $form.submit(function (e, forceSubmit) {
                if (forceSubmit)
                    return;
                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();
                FormValidator.validate($form, function ($form) {
                    $form.trigger('submit', [true]);
                });
            });
        });
    };
    FormHelper.initFocusBlur = function () {
        $('input, textarea').on('focus', function (e) {
            $(e.currentTarget).addClass('focused');
        });
        $('input, textarea').on('blur', function (e) {
            $(e.currentTarget).removeClass('focused').removeClass('error');
        });
    };
    return FormHelper;
}());
var ResponsiveImages = (function () {
    function ResponsiveImages() {
    }
    ResponsiveImages.init = function () {
        ResponsiveImages.initImages();
        ResponsiveImages.initBackgroundImages();
    };
    ResponsiveImages.initImages = function () {
        ResponsiveImages.$imgTags = $('img[data-mobile]');
        if (ResponsiveImages.$imgTags.length) {
            Breakpoints.breakpointChanged.add(function (currentBreakpoint) {
                if (currentBreakpoint == Breakpoints.MOBILE) {
                    ResponsiveImages.$imgTags.each(function (i, val) {
                        var $this = $(this);
                        if ($this.attr('src') && $this.attr('src').indexOf($this.data('mobile')) > -1)
                            return;
                        $this.attr('src', $this.data('mobile'));
                    });
                }
                else {
                    ResponsiveImages.$imgTags.each(function (i, val) {
                        var $this = $(this);
                        if ($this.attr('src') && $this.attr('src').indexOf($this.data('desktop')) > -1)
                            return;
                        $this.attr('src', $this.data('desktop'));
                    });
                }
            });
        }
    };
    ResponsiveImages.initBackgroundImages = function () {
        ResponsiveImages.$backgroundDivs = $('div[data-mobile], section[data-mobile], nav[data-mobile], footer[data-mobile]');
        if (ResponsiveImages.$backgroundDivs.length) {
            Breakpoints.breakpointChanged.add(function (currentBreakpoint) {
                if (currentBreakpoint == Breakpoints.MOBILE) {
                    ResponsiveImages.$backgroundDivs.each(function (i, val) {
                        var $this = $(this);
                        if ($this.css('backgroundImage').indexOf($this.data('mobile')) > -1)
                            return;
                        $this.css({ backgroundImage: 'url(' + $this.data('mobile') + ')' });
                    });
                }
                else {
                    ResponsiveImages.$backgroundDivs.each(function (i, val) {
                        var $this = $(this);
                        if ($this.css('backgroundImage').indexOf($this.data('desktop')) > -1)
                            return;
                        $this.css({ backgroundImage: 'url(' + $this.data('desktop') + ')' });
                    });
                }
            });
        }
    };
    return ResponsiveImages;
}());
var Parallax = (function () {
    function Parallax() {
    }
    Parallax.init = function () {
        if (cssua.ua.safari < 8)
            return;
        if (Browser.isTouch)
            return;
        Parallax.$els = $('[data-parallax]');
        Parallax.initEvents();
        Parallax.$els.each(function (i, el) {
            var $item = $(el);
            Parallax.setParallax($item);
        });
    };
    Parallax.initEvents = function () {
        //this.$window.scroll($.throttle(20, (e)=>this.onScroll(e)));
        Parallax.$window.scroll(function (e) { return Parallax.onScroll(e); });
        //if (cssua.ua.safari < 8) {
        //    this.$window.on('touchmove', (e)=>Parallax.onScroll(e))
        //}
    };
    Parallax.onScroll = function (e) {
        Parallax.$els.each(function (i, el) {
            Parallax.setParallax($(el));
        });
    };
    Parallax.setParallax = function ($el) {
        if ($el.data('parallax-mobile') == false) {
            if (Breakpoints.current == Breakpoints.MOBILE)
                return;
        }
        if (Parallax.$window.scrollTop() < Parallax.$window.height()) {
            var targetY = Parallax.$window.scrollTop() / $el.data('parallax');
            //var targetY:number = $el.position().top - Parallax.$window.scrollTop() / $el.data('parallax');
            var force3D = $el.data('force3d');
            if (force3D == undefined) {
                force3D = true;
            }
            //TweenMax.set($el, {css: {y: -targetY, force3D: force3D}});
            $el.css({ y: -targetY });
        }
        //  var targetY:number = (this.$winATrip.position().top - this.$window.scrollTop()) / 4;
    };
    Parallax.$window = $(window);
    return Parallax;
}());
var ButtonHelper = (function () {
    function ButtonHelper() {
    }
    ButtonHelper.init = function () {
        ButtonHelper.initRadioButtons();
    };
    ButtonHelper.initRadioButtons = function () {
        $('[data-radio-buttons]').each(function (i, el) {
            var $radioButtons = $(el).children();
            $radioButtons.on('click', function (e) {
                Tick.callback(function () {
                    if ($(e.currentTarget).parent().data('radio-buttons') == "deselect") {
                        var clickedOnActiveButton = $(e.currentTarget).hasClass('active');
                        if (clickedOnActiveButton) {
                            $(e.currentTarget).removeClass('active');
                        }
                        else {
                            $(e.currentTarget).parent().find('li').removeClass('active');
                            $(e.currentTarget).toggleClass('active');
                        }
                    }
                    else {
                        $(e.currentTarget).parent().find('li').removeClass('active');
                        $(e.currentTarget).toggleClass('active');
                    }
                });
            });
        });
    };
    return ButtonHelper;
}());
var Browser = (function () {
    function Browser() {
    }
    Browser.detectTouch = function () {
        var check = false;
        (function (a) {
            if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|android|blackberry|blazer|ipad|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.toLowerCase().substr(0, 4))) {
                check = true;
            }
            else {
                check = false;
            }
        })(navigator.userAgent || navigator.vendor || window['opera']);
        var hasTouchPoints = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));
        if (check && hasTouchPoints) {
            return true;
        }
        return false;
    };
    Browser.width = function () {
        return $(window).width(); // todo replace with more accurate method
    };
    Browser.height = function () {
        return $(window).height();
    };
    Browser.matchMediaLessThan = function (num) {
        return matchMedia('only screen and (max-width: ' + num + 'px)').matches;
    };
    Browser.isFirefox = cssua.ua.firefox != undefined;
    Browser.isMobile = cssua.ua.mobile != undefined;
    Browser.isChrome = cssua.ua.chrome != undefined;
    Browser.isIE = cssua.ua.ie != undefined;
    Browser.isIE7 = parseFloat(cssua.ua.ie) == 7;
    Browser.isIE8 = parseFloat(cssua.ua.ie) == 8;
    Browser.isIE9 = parseFloat(cssua.ua.ie) == 9;
    Browser.isIE10 = parseFloat(cssua.ua.ie) == 10;
    Browser.isIE11 = parseFloat(cssua.ua.ie) == 11;
    Browser.isOpera = cssua.ua.opera != undefined;
    Browser.isIpad = cssua.ua.mobile == 'ipad';
    Browser.isIPhone = cssua.ua.mobile == 'iphone';
    Browser.isIOS = Browser.isIpad || Browser.isIPhone;
    Browser.isAndroid = cssua.ua.mobile == 'android';
    Browser.isMac = cssua.ua.desktop == 'macintosh';
    Browser.isWindows = cssua.ua.desktop == 'windows';
    Browser.isTouch = Browser.detectTouch();
    return Browser;
}());
var FormSubmit = (function () {
    function FormSubmit() {
    }
    FormSubmit.init = function () {
        var $forms = $('[data-submit-btn]');
        $forms.each(function (i, el) {
            var $curForm = $(el);
            var $submitBtn = $curForm.find($($(el).data('submit-btn')));
            $submitBtn.on('click', function (e) {
                $curForm.submit();
            });
        });
    };
    return FormSubmit;
}());
var ArrayUtil = (function () {
    function ArrayUtil() {
    }
    ArrayUtil.shuffle = function (array) {
        for (var j, x, i = array.length; i; j = Math.floor(Math.random() * i), x = array[--i], array[i] = array[j], array[j] = x)
            ;
        return array;
    };
    ArrayUtil.toggleItem = function (array, value) {
        var index = array.indexOf(value);
        if (index === -1) {
            array.push(value);
        }
        else {
            array.splice(index, 1);
        }
    };
    ArrayUtil.isInArray = function (value, array) {
        return array.indexOf(value) > -1;
    };
    return ArrayUtil;
}());
var VideoHelper = (function () {
    function VideoHelper() {
    }
    VideoHelper.init = function () {
        VideoHelper.maintainIframeAspectRatio();
    };
    VideoHelper.maintainIframeAspectRatio = function () {
        $('[data-maintain-aspect-ratio]').each(function (i, el) {
            var $item = $(el);
            $item.wrap('<div class="maintain-aspect-ratio"></div>');
        });
        $('.maintain-aspect-ratio').each(function (i, el) {
            var $item = $(el);
            var $iframe = $item.find('iframe');
            var bottomPadding = (parseInt($iframe.attr('height')) / parseInt($iframe.attr('width'))) * 100;
            $item.css({ paddingBottom: bottomPadding + '%' });
        });
    };
    return VideoHelper;
}());
var ScrollTo = (function () {
    function ScrollTo() {
    }
    ScrollTo.init = function () {
        var $els = $('[data-scroll-to]');
        $els.each(function (i, el) {
            var $item = $(el);
            $item.on('click', function (e) {
                var scrollTarget = $(e.currentTarget).data('scroll-to');
                TweenMax.to(window, 0.8, { scrollTo: { y: $(scrollTarget).offset().top + 1 }, ease: Quint.easeInOut });
            });
        });
    };
    return ScrollTo;
}());
/// <reference path="../../typings/imports.d.ts" />
var Helpers = (function () {
    function Helpers() {
    }
    Helpers.init = function () {
        new Guide();
        Environment.init();
        ResponsiveImages.init();
        SizingHelper.init();
        Parallax.init();
        Breakpoints.init();
        ButtonHelper.init();
        FormHelper.init();
        VideoHelper.init();
        Lightbox.initThumbs();
        ScrollTo.init();
        $('input').placeholder();
        $(window).on('load', function () {
            new FastClick(document.body);
        });
        $('.fade-in-on-load').on('load', function (e) {
            $(e.currentTarget).css({ visibility: 'visible', opacity: 0 }).transition({ opacity: 1 }, 1500);
        });
    };
    return Helpers;
}());
var Lightbox = (function () {
    function Lightbox($currentThumbTarget, $thumbs) {
        this.$currentThumbTarget = $currentThumbTarget;
        this.$thumbs = $thumbs;
        this.index = $currentThumbTarget.index();
        this.totalItems = $thumbs.length;
        this.appendMarkup();
        this.setSelectors();
        this.initEvents();
        this.loadImage();
        this.fadeInLightbox();
        this.hidePrevNextArrowsIfOneItem();
    }
    Lightbox.initThumbs = function () {
        $('[data-lightbox]').each(function (i, el) {
            var $lightboxRootElement = $(el);
            var isLi = $lightboxRootElement.is('li');
            var $thumbs = isLi ? $lightboxRootElement.parent().find('li') : $lightboxRootElement;
            $lightboxRootElement.on('click', function (e) {
                new Lightbox($(e.currentTarget), $thumbs);
            });
        });
    };
    Lightbox.prototype.appendMarkup = function () {
        var html = $('#lightbox-tpl').html();
        this.$lightbox = $(html);
        $('body').append(this.$lightbox);
    };
    Lightbox.prototype.setSelectors = function () {
        this.$closeBtn = this.$lightbox.find('.close-btn');
        this.$prevBtn = this.$lightbox.find('.prev-btn');
        this.$nextBtn = this.$lightbox.find('.next-btn');
    };
    Lightbox.prototype.initEvents = function () {
        var _this = this;
        this.closeBtnClick = this.$closeBtn.on('click', function () { return _this.fadeOutLightbox(); });
        this.prevBtnClick = this.$prevBtn.on('click', function () { return _this.previous(); });
        this.nextBtnClick = this.$nextBtn.on('click', function () { return _this.next(); });
        this.lightboxClick = this.$lightbox.on('click', function (e) { return _this.onAnyClickInLightbox(e); });
        this.documentKeyUp = $(document).on('keyup', function (e) { return _this.onKeyUp(e); });
    };
    Lightbox.prototype.fadeInLightbox = function () {
        this.$lightbox.transition({ opacity: 1 }, 500);
    };
    Lightbox.prototype.fadeOutLightbox = function () {
        var _this = this;
        this.$lightbox.transition({ opacity: 0 }, 500, function () { return _this.destroy(); });
    };
    Lightbox.prototype.loadImage = function () {
        var imagePath = $(this.$thumbs.get(this.index)).data('lightbox');
        var $img = this.$lightbox.find('figure').find('img');
        $img.fadeOut(500, function () {
            $img.attr('src', imagePath);
            $img.load(function () {
                $img.fadeIn(500);
            });
        });
    };
    Lightbox.prototype.onAnyClickInLightbox = function (e) {
        if (!$(e.target).is('img')) {
            this.fadeOutLightbox();
        }
    };
    Lightbox.prototype.onKeyUp = function (e) {
        switch (e.which) {
            case 27:// esc
                this.fadeOutLightbox();
                break;
            case 37:// left
                this.previous();
                break;
            case 39:// right
                this.next();
                break;
        }
    };
    Lightbox.prototype.previous = function () {
        if (this.totalItems == 1)
            return;
        this.index = this.index == 0 ? this.totalItems - 1 : this.index -= 1;
        this.loadImage();
    };
    Lightbox.prototype.next = function () {
        if (this.totalItems == 1)
            return;
        this.index = this.index == this.totalItems - 1 ? 0 : this.index += 1;
        this.loadImage();
    };
    Lightbox.prototype.destroy = function () {
        this.closeBtnClick.unbind();
        this.prevBtnClick.unbind();
        this.nextBtnClick.unbind();
        this.lightboxClick.unbind();
        this.documentKeyUp.unbind();
        this.$lightbox.remove();
        this.$lightbox = null;
    };
    Lightbox.prototype.hidePrevNextArrowsIfOneItem = function () {
        if (this.totalItems == 1) {
            this.$prevBtn.hide();
            this.$nextBtn.hide();
        }
    };
    return Lightbox;
}());
var URLHelper = (function () {
    function URLHelper() {
    }
    URLHelper.getParameterByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    URLHelper.updateUrlParameter = function (uri, key, value) {
        var i = uri.indexOf('#');
        var hash = i === -1 ? '' : uri.substr(i);
        uri = i === -1 ? uri : uri.substr(0, i);
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            uri = uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            uri = uri + separator + key + "=" + value;
        }
        return uri + hash;
    };
    return URLHelper;
}());
var Datepicker = (function (_super) {
    __extends(Datepicker, _super);
    function Datepicker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Datepicker.prototype.init = function () {
        this.initSelectors();
        this.initDatepicker();
    };
    Datepicker.prototype.initSelectors = function () {
        this.$datepicker = $(".datepicker");
    };
    Datepicker.prototype.initDatepicker = function () {
        var _this = this;
        var date = new Date(), year = date.getFullYear(), yearEnd = new Date().getFullYear() - 18;
        date.setFullYear(yearEnd);
        this.$datepicker.each(function (index, el) {
            _this.$datepickerLocal = $(el);
            _this.$datepickerLocal.datepicker({
                changeYear: true,
                changeMonth: true,
                yearRange: '1930:' + yearEnd,
                defaultDate: date,
                onChangeMonthYear: function (year, monts) {
                    if (typeof this.value !== 'undefined' && this.value.length > 0) {
                        var dateArr = this.value.split('/');
                        var selectedMonth = "" + monts;
                        selectedMonth = selectedMonth.length === 1 ? '0' + selectedMonth : selectedMonth;
                        this.value = selectedMonth + "/" + dateArr[1] + "/" + year;
                    }
                }
            });
        });
    };
    return Datepicker;
}(View));
var DataLinks = (function (_super) {
    __extends(DataLinks, _super);
    function DataLinks() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DataLinks.prototype.init = function () {
        this.initSelectors();
        this.initDatepicker();
    };
    DataLinks.prototype.initSelectors = function () {
        this.$links = $("[data-href]");
    };
    DataLinks.prototype.initDatepicker = function () {
        var _this = this;
        this.$links.on("click", function (e) {
            _this.$internalLink = $(e.target);
            _this.pageReload(e, _this.$internalLink.attr("data-href"));
        });
    };
    DataLinks.prototype.pageReload = function (e, link) {
        e.preventDefault();
        window.location.href = window.location.origin + "" + link;
    };
    return DataLinks;
}(View));
var Header = (function (_super) {
    __extends(Header, _super);
    function Header() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('#header');
        return _this;
    }
    Header.prototype.init = function () {
        if (!$('body').hasClass('short-nav-perm')) {
            this.setSelectors();
            this.initEvents();
        }
        this.initScrollEvent();
    };
    Header.prototype.initScrollEvent = function () {
        var _this = this;
        var onScroll = function () {
            if (_this.$window.scrollTop() > 0) {
                _this.$body.addClass('scrolled-from-top');
            }
            else {
                _this.$body.removeClass('scrolled-from-top');
            }
        };
        this.$window.on('scroll', $.throttle(250, function () { return onScroll(); }));
        this.$window.on('scroll', $.debounce(100, function () { return onScroll(); }));
    };
    Header.prototype.setSelectors = function () {
    };
    Header.prototype.initEvents = function () {
        var _this = this;
        this.$window.on('scroll', $.throttle(250, function () { return _this.onScroll(); }));
        this.$window.on('scroll', $.debounce(100, function () { return _this.onScroll(); }));
    };
    Header.prototype.onScroll = function () {
        if (this.$window.scrollTop() > 0) {
            this.$body.addClass('short-nav');
        }
        else {
            this.$body.removeClass('short-nav');
        }
    };
    return Header;
}(View));
var Modals = (function (_super) {
    __extends(Modals, _super);
    function Modals() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Modals.prototype.init = function (holder) {
        var _this = this;
        if (holder === void 0) { holder = $('[data-modal]'); }
        this.initModalButtons(holder);
        this.initAutoOpenModals();
        this.preVideoModal();
        this.app.getModel(VideoPaginationModel).pageChanged.add(function () { return _this.initModalButtons(); });
        this.app.getModel(MashupMakerModel).liAdded.add(function () { return _this.initModalButtons(); });
    };
    Modals.prototype.initModalButtons = function (holder) {
        if (holder === void 0) { holder = $('[data-modal]'); }
        holder.each(function (i, el) {
            // debugger
            new Modal($(el));
        });
    };
    Modals.prototype.initAutoOpenModals = function () {
        $('[data-auto-open-modal]').each(function (i, el) {
            var modalEl = $(el);
            var modal = new Modal($('<button>').data('modal', "#" + modalEl.attr('id')));
            modal.fadeInModal();
        });
    };
    Modals.prototype.preVideoModal = function () {
        $(document).on('click', '[data-modal="#video"], [data-modal="#mashup"]', function () {
            var _this = this;
            var $1stVideoModal = $('#first-video-congratulation');
            if ($1stVideoModal) {
                $1stVideoModal.fadeIn(400, function () {
                    $1stVideoModal.addClass('scroll');
                    $('body').addClass('no-scroll');
                    $1stVideoModal.find('.close-modal').fadeIn();
                    $1stVideoModal.find('.inner').fadeIn();
                    TweenMax.set(_this.$inner, { css: { y: -80 } });
                    TweenMax.to(_this.$inner, 0.6, { css: { y: 0, force3D: false }, ease: Power3.easeOut });
                });
            }
        });
    };
    return Modals;
}(View));
var Modal = (function (_super) {
    __extends(Modal, _super);
    function Modal($launchBtn) {
        var _this = _super.call(this) || this;
        _this.$launchBtn = $launchBtn;
        _this.setSelectors();
        _this.initEvents();
        return _this;
    }
    Modal.prototype.setSelectors = function () {
        this.$modal = $(this.$launchBtn.data('modal'));
        this.$closeButton = this.$modal.find('.close-modal');
        this.$body = $('body');
        this.$inner = this.$modal.find('.inner');
        this.$modal.data('modal', this);
    };
    Modal.prototype.initEvents = function () {
        var _this = this;
        this.$closeButton.on('click', function (e) { _this.onCloseButtonClick(e); });
        this.$launchBtn.on('click', function (e) { return _this.onLaunchButtonClick(e); });
        this.$modal.on('click', function (e) { return _this.closeModalOnOutsideClick(e); });
        $(document).on('keyup', function (e) { return _this.onKeyUp(e); });
    };
    Modal.prototype.onLaunchButtonClick = function (e) {
        if (this.$modal.length > 0) {
            e.preventDefault();
            this.fadeInModal();
        }
    };
    Modal.prototype.onCloseButtonClick = function (e) {
        this.fadeOutModal();
    };
    Modal.prototype.fadeInModal = function () {
        var _this = this;
        this.$modal.fadeIn(400, function () {
            _this.$modal.addClass('scroll');
            _this.$body.addClass('no-scroll');
            _this.$closeButton.fadeIn();
            _this.$inner.fadeIn();
            TweenMax.set(_this.$inner, { css: { y: -80 } });
            TweenMax.to(_this.$inner, 0.6, { css: { y: 0, force3D: false }, ease: Power3.easeOut });
        });
    };
    Modal.prototype.fadeOutModal = function () {
        var _this = this;
        this.$closeButton.fadeOut();
        this.$inner.fadeOut(400, function () {
            _this.$modal.removeClass('scroll');
            _this.$body.removeClass('no-scroll');
            _this.$modal.fadeOut(400);
            _this.chengeVideoStatus(_this.$modal);
        });
        this.app.getModel(ModalsModel).modalClosed.dispatch();
    };
    Modal.prototype.onKeyUp = function (e) {
        switch (e.which) {
            case 27:// esc
                this.fadeOutModal();
                break;
        }
    };
    Modal.prototype.closeModalOnOutsideClick = function (e) {
        if ($(e.target).hasClass('modal')) {
            this.fadeOutModal();
        }
    };
    Modal.prototype.chengeVideoStatus = function (modal) {
        if (modal.hasClass('first-video-popup')) {
            $.ajax({
                url: '/user/users/videoSee'
            }).done(function () {
                $('#video').find('.lockout > .overlay').remove();
            });
        }
    };
    return Modal;
}(View));
var ModalsModel = (function (_super) {
    __extends(ModalsModel, _super);
    function ModalsModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.modalClosed = new signals.Signal();
        _this.mashupDeleted = new signals.Signal();
        return _this;
    }
    return ModalsModel;
}(Model));
var Alerts = (function (_super) {
    __extends(Alerts, _super);
    function Alerts() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Alerts.prototype.init = function () {
        this.setSelectors();
        this.initEvents();
        this.$alerts.each(function (i, el) {
            var $item = $(el);
            if ($item.find('ul').length) {
                $item.find('.inner').delay(700).slideDown(500).delay(5000).slideUp(500);
            }
        });
    };
    Alerts.prototype.setSelectors = function () {
        this.$alerts = $('.alerts');
    };
    Alerts.prototype.initEvents = function () {
    };
    return Alerts;
}(View));
var NavMenu = (function (_super) {
    __extends(NavMenu, _super);
    function NavMenu() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('#main-menu');
        return _this;
    }
    NavMenu.prototype.init = function () {
        this.setSelectors();
        this.initEvents();
        if (!String.prototype.startsWith) {
            String.prototype.startsWith = function (searchString, position) {
                position = position || 0;
                return this.indexOf(searchString, position) === position;
            };
        }
    };
    NavMenu.prototype.setSelectors = function () {
        this.$hamburger = $(".hamburger");
        this.$burgerWrap = this.$hamburger.closest(".wrap");
        this.$menuButton = this.$burgerWrap.find("h5");
    };
    NavMenu.prototype.initEvents = function () {
        var _this = this;
        //this.$el.load("p57.stage.realpie.com #nav-wrap");
        $(window).unload(function () { return _this.closeMenu(); });
        $(window).on('beforeunload ', function () { return _this.closeMenu(); });
        this.$burgerWrap.click(function () { return _this.openMenu(); });
        $("#menu-pull-out-nav .menu-item-has-children").mouseenter(function (e) {
            e.stopPropagation();
            if ($(window).width() > 700) {
                _this.menuHoverHandler($(e.currentTarget));
            }
        });
        $("#menu-pull-out-nav  .menu-item-has-children").click(function (e) {
            e.stopPropagation();
            if ($(window).width() <= 700) {
                if (!$(e.currentTarget).hasClass("mobile-active")) {
                    e.preventDefault();
                }
                _this.menuClickHandler($(e.currentTarget));
            }
        });
    };
    NavMenu.prototype.openMenu = function () {
        var _this = this;
        this.$el.show();
        this.$burgerWrap.off('click');
        setTimeout(function () {
            $(".hamburger, #main-menu, .bg-column-1, .bg-column-2, #img-wrap, .column-1, .column-1 li a, #head-wrap, .jumbotron").addClass("open");
            $(".hamburger, .site-map").addClass("col-1-active");
            _this.$menuButton.html("CLOSE").addClass("col-1-active");
            $(".bg-column-1, .bg-column-2, #img-wrap, #nav-wrap ul, #nav-wrap ul li a").removeClass("closed");
            _this.$burgerWrap.click(function () { return _this.closeMenu(); });
        }, 10);
        setTimeout(function () {
            $("body").addClass("no-scroll");
        }, 500);
    };
    NavMenu.prototype.closeMenu = function () {
        var _this = this;
        $(".hamburger, #main-menu, .background, #img-wrap, .column-1 li a, .column-1, .sub-menu, .sub-menu li a, #head-wrap, .jumbotron").removeClass("open");
        $("body").removeClass("no-scroll");
        $(".hamburger").removeClass("col-1-active col-3-active");
        this.$menuButton.html("MENU").removeClass("col-1-active col-3-active");
        $(".background, .column-1, #img-wrap, .column-1 li a, .sub-menu, .sub-menu li a").addClass("closed");
        $(".active").removeClass("active");
        $(".mobile-active").removeClass("mobile-active");
        $("#img-wrap a").removeClass("grey");
        this.$burgerWrap.off('click');
        setTimeout(function () {
            _this.$burgerWrap.click(function () {
                _this.openMenu();
            });
        }, 100);
        setTimeout(function () {
            $("#main-menu").hide();
        }, 500);
    };
    NavMenu.prototype.menuHoverHandler = function (hovered) {
        console.log();
        var classes = hovered.attr("class").split(" ");
        var depthClass = classes.filter(function (foobar) {
            return foobar.startsWith("menu-item-depth");
        });
        var currentColumn = depthClass[0].slice(-1);
        var subID = hovered.attr("id");
        if (currentColumn == 0) {
            $(".bg-column-3, .menu-depth-1, menu-depth-2, .menu-depth-1 li, .menu-depth-2 li, .menu-depth-1 li a, .menu-depth-2 li a").removeClass("open").addClass("closed");
            $(".menu-depth-1 .active, .column-1 .active, .column-1 li a.active").removeClass("active");
            $("#img-wrap a").removeClass("grey");
            this.$hamburger.removeClass("col-3-active").addClass("col-1-active");
            hovered.find('a').first().addClass("active");
            hovered.addClass("active");
            hovered.find('.sub-menu').first().removeClass("closed").addClass("open");
            hovered.find('.sub-menu').first().removeClass("closed").children('li').addClass("open");
            hovered.find('.sub-menu').first().children('li').children('a').removeClass("closed").addClass("open");
        }
        else if (currentColumn == 1) {
            if (hovered.hasClass("open")) {
                $(".menu-depth-1 .active, .menu-depth-1 li a.active").removeClass("active");
                hovered.find('a').first().addClass("active");
                hovered.addClass("active");
                $(".menu-depth-2, .menu-depth-2 li, .menu-depth-2 li a").removeClass("open").addClass("closed");
                $(".bg-column-3").removeClass("closed").addClass("open");
                hovered.find('.sub-menu').first().removeClass("closed").addClass("open");
                //hovered.find('.sub-menu').first().removeClass("closed").children('li').addClass("open");
                hovered.find('.sub-menu').first().children('li').children('a').removeClass("closed").addClass("open");
                $("#img-wrap a").addClass("grey");
                this.$hamburger.removeClass("col-1-active").addClass("col-3-active");
                this.$menuButton.removeClass("col-1-active").addClass("col-3-active");
            }
        }
    };
    NavMenu.prototype.menuClickHandler = function (clicked) {
        var classes = clicked.attr("class").split(" ");
        var depthClass = classes.filter(function (foobar) {
            return foobar.startsWith("menu-item-depth");
        });
        var currentColumn = depthClass[0].slice(-1);
        var subID = clicked.attr("id");
        if (!clicked.hasClass("mobile-active")) {
            if (currentColumn == 0) {
                $(".menu-depth-1, .menu-depth-2, .menu-depth-1 li, .menu-depth-2 li, .menu-depth-1 li a, .menu-depth-2 li a").removeClass("open");
                $(".column-1 .mobile-active").removeClass("mobile-active");
                clicked.addClass("mobile-active");
                clicked.find('.sub-menu').first().addClass("open");
                clicked.find('.sub-menu').first().children('li').addClass("open");
                clicked.find('.sub-menu').first().children('li').children('a').addClass("open");
            }
            else if (currentColumn == 1) {
                if (clicked.hasClass("open")) {
                    $(".menu-depth-1 .mobile-active").removeClass("mobile-active");
                    $(".menu-depth-2, .menu-depth-2 li, .menu-depth-2 li a").removeClass("open");
                    setTimeout(function () {
                        clicked.addClass("mobile-active");
                        clicked.find('.sub-menu').first().addClass("open");
                        clicked.find('.sub-menu').first().children('li').addClass("open");
                        clicked.find('.sub-menu').first().children('li').children('a').addClass("open");
                    }, 100);
                    $(".hamburger").removeClass("col-1-active").addClass("col-3-active");
                }
            }
        }
        else {
            clicked.removeClass("mobile-active");
            clicked.find('.sub-menu').first().removeClass("open");
            clicked.find('.sub-menu').first().children('li').removeClass("open");
            clicked.find('.sub-menu').first().children('li').children('a').removeClass("open");
        }
    };
    return NavMenu;
}(View));
var DeeplinkController = (function (_super) {
    __extends(DeeplinkController, _super);
    function DeeplinkController() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DeeplinkController.prototype.init = function () {
        var _this = this;
        this.appendVideoMarkupIfFound();
        this.appendMashupMarkupIfFound();
        Tick.callback(function () { return _this.launchDeeplinkIfFound(); });
    };
    DeeplinkController.prototype.appendVideoMarkupIfFound = function () {
        var param = URLHelper.getParameterByName('video');
        if (param) {
            var html = "<div id='deeplink' data-modal='#video' data-video='" + param + "' style='display: none;'></div>";
            $('body').append(html);
        }
    };
    DeeplinkController.prototype.appendMashupMarkupIfFound = function () {
        var param = URLHelper.getParameterByName('mashup');
        if (param) {
            var html = "<div id='deeplink' data-modal='#mashup' data-playlist='" + param + "' style='display: none;'></div>";
            $('body').append(html);
        }
    };
    DeeplinkController.prototype.launchDeeplinkIfFound = function () {
        var videoParam = URLHelper.getParameterByName('video');
        var mashupParam = URLHelper.getParameterByName('mashup');
        if (videoParam || mashupParam) {
            $('#deeplink').trigger('click');
        }
    };
    return DeeplinkController;
}(Controller));
var VideoThumb = (function (_super) {
    __extends(VideoThumb, _super);
    function VideoThumb() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('[data-video]');
        return _this;
    }
    VideoThumb.prototype.init = function () {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    };
    VideoThumb.prototype.setSelectors = function () {
        this.$addFavoriteVideoBtns = this.$el.find('.add-favorite');
        this.$addFavoriteMashupBtns = $('.favorite-playlist');
        this.$unfavoriteBtns = this.$el.find('.delete');
    };
    VideoThumb.prototype.initEvents = function () {
        var _this = this;
        this.$addFavoriteVideoBtns.on('click', function (e) { return _this.onAddFavoriteVideoBtnClick(e); });
        this.$addFavoriteMashupBtns.on('click', function (e) { return _this.onAddFavoriteMashupBtnClick(e); });
        this.$unfavoriteBtns.on('click', function (e) { return _this.onUnfavoriteVideoBtnClick(e); });
        this.app.getModel(VideoPaginationModel).pageChanged.add(function () { return _this.onVideoPaginationChange(); });
    };
    VideoThumb.prototype.onAddFavoriteVideoBtnClick = function (e) {
        e.stopPropagation();
        var id = $(e.currentTarget).parent().data('video');
        $.getJSON('/video/favorite/' + id, function (data) {
            Global.redirectToLoginIfSessionTimesout(data);
        }).fail(function () {
            console.error('Could not add video to favorites!');
        });
        $(e.currentTarget).find('.checkmark').css({ display: 'inline-block' }).animate({ width: 20 }, 400);
    };
    VideoThumb.prototype.onUnfavoriteVideoBtnClick = function (e) {
        e.stopPropagation();
        var id = $(e.currentTarget).parent().parent().data('video');
        $.getJSON('/video/unfavorite/' + id, function (data) {
            Global.redirectToLoginIfSessionTimesout(data);
        }).fail(function () {
            //console.error('Could not delete favorite!');
        });
        var $li = $(e.currentTarget).closest('li');
        $li.fadeOut(400, function () {
            var $ul = $li.parent();
            $li.remove();
        });
    };
    VideoThumb.prototype.onVideoPaginationChange = function () {
        this.$el = $('[data-video]');
        this.setSelectors();
        this.initEvents();
    };
    VideoThumb.prototype.onAddFavoriteMashupBtnClick = function (e) {
        e.stopPropagation();
        var id = $(e.currentTarget).parent().data('playlist');
        $.getJSON('/playlist/subscribe/' + id, function (data) {
            Global.redirectToLoginIfSessionTimesout(data);
        }).fail(function () {
            console.error('Could not subscribe to playlist!');
        });
        $(e.currentTarget).find('.checkmark').css({ display: 'inline-block' }).animate({ width: 20 }, 400);
    };
    return VideoThumb;
}(View));
var VideoStatus = (function (_super) {
    __extends(VideoStatus, _super);
    function VideoStatus() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    VideoStatus.prototype.init = function (holder) {
        if (holder === void 0) { holder = $(".video-thumbs"); }
        this.initSelectors(holder);
        this.initEvents();
    };
    VideoStatus.prototype.initSelectors = function (holder) {
        this.programm_id = $("[data-program_id]").data("program_id");
        this.requestLink = window.location.origin + "/program/" + this.programm_id + "/user-video-progress";
        this.$all_list = holder;
        this.$list = holder.not('.skipped-workouts');
        this.$all_list_item = this.$all_list.children().not(".additional-block, .rest-day");
        this.$list_item = this.$list.children().not(".additional-block, .rest-day");
        this.videosArr = [];
        // user info
        this.$calc_value = $(".calculator-item:first").find(".calculator-value");
        this.$completed_value = $(".completed-count");
        this.$left_value = $(".workouts-counter .left-count");
        this.$skipped_days = $(".calculator-item .left-count");
        // modal btn
        this.$modal_btn = $("[data-modal='#finish-program']");
        this.$change_modal_btn = $("[data-modal='#change-weight']");
        this.$close_modal = $(".close-modal");
        // is program finished
        this.isFinished = false;
    };
    VideoStatus.prototype.initEvents = function () {
        var _this = this;
        var self = this;
        this.$all_list_item.on("click", function (e) {
            self.$current_item = $(this);
            self.current_item_was_skipped = self.$current_item.hasClass('is-skipped-workout');
            self.finish_program_btn = self.$current_item.is(self.$list_item.last());
            var $popup = self.$current_item.find("[data-modal]");
            // console.log('$current_item', self.$current_item)
            // console.log('current_item_was_skipped', self.current_item_was_skipped)
            if (!self.$current_item.hasClass("is-finished-workout")) {
                var regimen_id = self.$current_item.data('regimen-id');
                var regimen_day = self.$current_item.data('regimen-day');
                $("li[data-regimen-id='" + regimen_id + "'][data-regimen-day='" + regimen_day + "']").removeClass().addClass("is-finished-workout");
                // self.$current_item.removeClass().addClass("is-finished-workout");
                self.buildVideosArr(self.$current_item.find("[data-video]"), self.$current_item.data("regimen-day"));
            }
        });
        this.$close_modal.on("click", function () {
            if (self.isFinished || self.finish_program_btn) {
                $('finish-#program .has-skipped-workouts').toggleClass('hidden', self.isFinished);
                self.$modal_btn.trigger("click");
            }
        });
        setTimeout(function () {
            if ($("#change-weight").length) {
                _this.$change_modal_btn.trigger("click");
            }
        }, 1000);
    };
    VideoStatus.prototype.buildVideosArr = function (videos, id) {
        var _this = this;
        videos.each(function (index, el) {
            _this.$current_video = $(el);
            _this.videosArr.push(_this.$current_video.data("video"));
        });
        if (this.videosArr.length) {
            this.sendVideoRequest(this.videosArr, id);
        }
    };
    VideoStatus.prototype.updateData = function (data) {
        var current_data = JSON.parse(data);
        this.$calc_value.html(current_data.progress + "%");
        this.$completed_value.html(current_data.completed_workouts_count);
        this.$left_value.html(current_data.days_left_count);
        var current_skipped_days = parseInt(this.$skipped_days.html());
        // console.log(this.current_item_was_skipped)
        if (current_skipped_days > 0 && this.current_item_was_skipped) {
            this.$skipped_days.html(current_skipped_days - 1);
            if (current_skipped_days === 1) {
                this.$skipped_days.removeClass('left-count-more-zero');
            }
        }
        if (current_data.regimen_finished) {
            this.isFinished = true;
        }
    };
    VideoStatus.prototype.sendVideoRequest = function (videos, id) {
        var self = this;
        var settings = {
            method: "POST",
            url: this.requestLink,
            dataType: "json",
            data: { 'videos': JSON.stringify(videos), 'regimen_day': id },
            success: function (data) {
                self.updateData(data.response_data);
            },
            error: function (error) {
                console.log("error", error);
            }
        };
        $.ajax(settings);
        this.videosArr = [];
    };
    return VideoStatus;
}(View));
var VideoFilter = (function (_super) {
    __extends(VideoFilter, _super);
    function VideoFilter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    VideoFilter.prototype.init = function () {
        this.initSelectors();
        this.initEvents();
    };
    VideoFilter.prototype.initSelectors = function () {
        this.$week_indicator = $(".info-panel .week-indicator");
        // week items
        this.$current_week = $(".current-week").first().closest(".video-thumbs");
        this.$current_btn_holder = this.$current_week.find(".additional-block");
        this.$prev_weeks = this.$current_week.prevAll();
        this.$skipped_workouts = $('.skipped-workouts');
        this.$all_weeks = $(".workout-content .video-thumbs").not('.skipped-workouts');
        // holder
        this.$holder = $(".video-pagination");
        // btns
        this.$next_btn = $(".next-week-btn");
        this.$view_all = $(".info-panel .btn-view-all");
        this.$refresh = $(".btn-refresh");
        this.$skipped = $(".is-skipped-days");
        // counter
        this.counter = 0;
        this.isSkipped = false;
    };
    VideoFilter.prototype.initEvents = function () {
        var _this = this;
        // remove all weeks before current
        this.all_length = this.$all_weeks.length - 1;
        this.addCounter();
        // set holder height
        this.$holder.imagesLoaded().done(function () {
            setTimeout(function () {
                _this.$current_week.addClass('current');
                _this.current_week_index = _this.$all_weeks.index(_this.$current_week);
                _this.refreshFilter();
            }, 200);
        });
        // add event to next button
        $(document).on("click", ".next-week-btn", function (e) {
            $(e.currentTarget).addClass('hidden');
            _this.increaseClickCounter();
        });
        // add action to view all btn
        this.$view_all.on("click", function (e) {
            _this.viewAll();
        });
        // refresh filter button event
        this.$refresh.on("click", function (e) {
            _this.refreshFilter();
        });
        // recalculate height on resize
        this.$window.on("resize", function () {
            clearTimeout(_this.timeout);
            _this.timeout = setTimeout(function () {
                if (_this.isSkipped) {
                    _this.toggleSkippedWorkouts();
                }
                else {
                    _this.active_week = _this.$all_weeks.eq(_this.counter);
                    _this.active_height = _this.active_week.outerHeight();
                    _this.setHolderHeight(_this.active_height + _this.active_week.position().top);
                }
            }, 500);
        });
        this.$skipped.on("click", function (e) {
            e.preventDefault();
            _this.isSkipped = !_this.isSkipped;
            _this.toggleSkippedWorkouts();
        });
    };
    VideoFilter.prototype.showSkippedWorkouts = function () {
        this.$all_weeks.addClass("hidden");
        this.$skipped_workouts.removeClass("hidden");
        this.$window.trigger('resize'); // todo: de-jankify. triggers match-tallest li helper
        var skipped_height = this.$skipped_workouts.outerHeight();
        this.$holder.removeClass('refresh-filter');
        this.$holder.removeClass('all-filter');
        this.$holder.addClass('skipped');
        this.setHolderHeight(skipped_height);
    };
    VideoFilter.prototype.hideSkippedWorkouts = function () {
        this.isSkipped = false;
        this.$all_weeks.removeClass("hidden");
        this.$skipped_workouts.addClass("hidden");
        // this.$week_indicator.addClass("hidden");
    };
    VideoFilter.prototype.toggleSkippedWorkouts = function () {
        if (this.isSkipped) {
            this.showSkippedWorkouts();
        }
        else {
            this.hideSkippedWorkouts();
            this.refreshFilter();
        }
    };
    VideoFilter.prototype.addCounter = function () {
        var _this = this;
        this.$all_weeks.each(function (index, el) {
            var $current = $(el);
            if (!$current.hasClass('skipped-workouts')) {
                $current.prepend("<span class='week-counter'><span>" + _this.getGetOrdinal(index + 1) + "</span><span>WEEK</span></span>");
            }
        });
    };
    VideoFilter.prototype.viewAll = function () {
        this.$prev_weeks.removeClass('hidden');
        this.$holder.removeClass('refresh-filter');
        this.$holder.addClass('all-filter');
        this.$holder.removeClass('skipped');
        this.updateWeekIndicator(this.$all_weeks.first());
        this.hideSkippedWorkouts();
        this.counter = this.all_length;
        var last_week = this.$all_weeks.eq(this.counter), last_btn_holder = last_week.find(".additional-block"), last_height = last_week.outerHeight(), last_pos = last_week.position().top;
        this.$window.trigger('resize'); // todo: de-jankify. triggers match-tallest li helper
        this.setHolderHeight(last_pos + last_height);
        // last_btn_holder.prepend(this.$next_btn);
        // this.$week_indicator.removeClass("hidden");
    };
    VideoFilter.prototype.refreshFilter = function () {
        this.counter = this.current_week_index;
        this.$next_btn.removeClass('hidden');
        this.$holder.addClass('refresh-filter');
        this.$holder.removeClass('all-filter');
        this.$holder.removeClass('skipped');
        //add next button
        if (this.counter < this.$all_weeks.length - 1) {
            this.showNextWeekBtn(this.$current_week.find(".additional-block"));
        }
        this.hideSkippedWorkouts();
        this.updateWeekIndicator(this.$current_week);
        this.$window.trigger('resize'); // todo: de-jankify. triggers match-tallest li helper
        this.$prev_weeks.addClass('hidden');
        var refresh_height = this.$current_week.outerHeight();
        this.setHolderHeight(refresh_height);
    };
    VideoFilter.prototype.updateWeekIndicator = function ($week) {
        this.current_week_indicator = $week.find('.week-counter').html();
        this.$week_indicator.html(this.current_week_indicator);
    };
    VideoFilter.prototype.setHolderHeight = function (height) {
        this.$holder.height(height);
    };
    VideoFilter.prototype.increaseClickCounter = function () {
        this.counter++;
        var $next_week = this.$all_weeks.eq(this.counter);
        var $next_btn_holder = $next_week.find(".additional-block");
        var next_height = $next_week.outerHeight();
        var current_heigth = this.$holder.outerHeight();
        this.setHolderHeight(next_height + current_heigth);
        this.$next_btn.addClass('hidden');
        if (this.counter < this.$all_weeks.length - 1) {
            this.showNextWeekBtn($next_btn_holder);
        }
    };
    VideoFilter.prototype.showNextWeekBtn = function ($btn_holder) {
        var $next_week_btn = $btn_holder.find('.next-week-btn');
        if ($next_week_btn.length > 0) {
            $next_week_btn.removeClass('hidden');
        }
        else {
            var $next_btn = this.$next_btn.clone();
            $btn_holder.prepend($next_btn.removeClass('hidden'));
        }
    };
    VideoFilter.prototype.getGetOrdinal = function (n) {
        var s = ["th", "st", "nd", "rd"], v = n % 100;
        return n + (s[(v - 20) % 10] || s[v] || s[0]);
    };
    return VideoFilter;
}(View));
var VideoPagination = (function (_super) {
    __extends(VideoPagination, _super);
    function VideoPagination() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('.video-pagination');
        _this.page = 1;
        _this.loading = false;
        return _this;
    }
    VideoPagination.prototype.init = function () {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    };
    VideoPagination.prototype.setSelectors = function () {
        this.$seeMoreBtn = $('#see-more-btn');
        this.$videoThumbsUl = this.$el.find('.video-thumbs');
        this.$noResults = this.$el.find('.no-results');
    };
    VideoPagination.prototype.initEvents = function () {
        var _this = this;
        this.$seeMoreBtn.on('click', function (e) { return _this.onSeeMoreBtnClick(e); });
        this.$el.find('#type-dd').on('change', function (e) { return _this.onDropdownChange(e); });
        this.$el.find('#level-dd').on('change', function (e) { return _this.onDropdownChange(e); });
        this.$el.find('#duration-dd').on('change', function (e) { return _this.onDropdownChange(e); });
    };
    VideoPagination.prototype.onSeeMoreBtnClick = function (e) {
        e.preventDefault();
        if (this.loading)
            return;
        this.loadNextPage();
    };
    VideoPagination.prototype.loadNextPage = function () {
        var _this = this;
        this.$seeMoreBtn.fadeIn();
        this.$seeMoreBtn.parent().addClass('loading');
        this.page++;
        var query = '/video?type=' + this.$el.find('#type-dd').val()
            + '&level=' + this.$el.find('#level-dd').val()
            + '&duration=' + this.$el.find('#duration-dd').val()
            + '&page=' + this.page;
        //console.log(query);
        $.getJSON(query, function (data) {
            Global.redirectToLoginIfSessionTimesout(data);
            _this.data = data;
            _this.preloadThumbImages();
            _this.$noResults.css({ display: data.length ? 'none' : 'block' });
            if (!_this.data.length) {
                _this.$seeMoreBtn.stop().hide();
            }
        }).fail(function () {
            this.loading = false;
            this.$seeMoreBtn.parent().removeClass('loading');
            console.error('Could not load next page!');
        });
    };
    VideoPagination.prototype.onDropdownChange = function (e) {
        this.page = 0;
        this.$videoThumbsUl.empty();
        this.loadNextPage();
    };
    VideoPagination.prototype.preloadThumbImages = function () {
        var _this = this;
        var loader = new PxLoader();
        for (var i = 0; i < this.data.length; i++) {
            loader.addImage(this.data[i].meta.videoStillURL);
        }
        loader.addCompletionListener(function () {
            _this.$seeMoreBtn.parent().removeClass('loading');
            _this.hideSeeMoreButtonOnLastPage();
            _this.populateThumbs();
            _this.loading = false;
            _this.app.getModel(VideoPaginationModel).pageChanged.dispatch();
            _this.$window.trigger('resize'); // todo: de-jankify. triggers match-tallest li helper
        });
        loader.start();
    };
    VideoPagination.prototype.populateThumbs = function () {
        for (var i = 0; i < this.data.length; i++) {
            var obj = this.data[i];
            var videoLiHtml = $('#video-li-tpl').html();
            var $videoLi = $(videoLiHtml);
            $videoLi.find('.thumb').attr('data-video', obj.id);
            $videoLi.find('.time').prepend(Math.round(obj.length / 60 / 1000).toString());
            $videoLi.find('.thumb-img').attr('src', obj.meta.videoStillURL);
            $videoLi.find('.title').html(obj.title);
            var types = ['strength', 'cardio', 'restorative'];
            for (var j = 0; j < types.length; j++) {
                var className;
                if (obj[types[j]] == '')
                    className = 'none';
                if (obj[types[j]] == 1)
                    className = 'one';
                if (obj[types[j]] == 2)
                    className = 'two';
                if (obj[types[j]] == 3)
                    className = 'three';
                $videoLi.find('.' + types[j]).addClass(className);
            }
            $videoLi.stop(true).css({ opacity: 0, y: 70 }).delay(i * 100).transition({ opacity: 1, y: 0 }, 400);
            //$videoLi.css({opacity:0}).delay(i * 100).animate({opacity:1}, 350);
            this.$videoThumbsUl.append($videoLi);
        }
    };
    VideoPagination.prototype.hideSeeMoreButtonOnLastPage = function () {
        var isLastPage = this.data.length < 9;
        if (isLastPage) {
            this.$seeMoreBtn.stop().fadeOut();
        }
    };
    return VideoPagination;
}(View));
var VideoPaginationModel = (function (_super) {
    __extends(VideoPaginationModel, _super);
    function VideoPaginationModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.pageChanged = new signals.Signal();
        return _this;
    }
    return VideoPaginationModel;
}(Model));
var SignInModal = (function (_super) {
    __extends(SignInModal, _super);
    function SignInModal() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('#sign-in');
        return _this;
    }
    SignInModal.prototype.init = function () {
        this.setSelectors();
        this.initEvents();
        this.checkForDeeplink();
    };
    SignInModal.prototype.setSelectors = function () {
        this.$signInBtn = this.$el.find('.sign-in-btn');
    };
    SignInModal.prototype.initEvents = function () {
        var _this = this;
        this.$signInBtn.on('click', function (e) { return _this.onSignInButtonClick(e); });
    };
    SignInModal.prototype.onSignInButtonClick = function (e) {
        e.preventDefault();
    };
    SignInModal.prototype.checkForDeeplink = function () {
        if (URLHelper.getParameterByName('login') == '1') {
            $('[data-modal=#sign-in]').trigger('click');
        }
    };
    return SignInModal;
}(View));
var PersonalInfoModal = (function (_super) {
    __extends(PersonalInfoModal, _super);
    function PersonalInfoModal() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('#start-program-form');
        return _this;
    }
    PersonalInfoModal.prototype.init = function () {
        this.setSelectors();
        this.initEvents();
        this.buildRequest();
        this.buildRedirect();
    };
    PersonalInfoModal.prototype.buildRequest = function () {
        this.request_link = window.location.origin + this.$el.attr("action");
    };
    PersonalInfoModal.prototype.buildRedirect = function () {
        this.redirect_link = window.location.origin + "/program/" + $("[data-program_id]").data("program_id") + "/subscribe";
    };
    PersonalInfoModal.prototype.setSelectors = function () {
        this.$signInBtn = this.$el.find('.sign-in-btn');
        this.$formItems = this.$el.find("input:text");
    };
    PersonalInfoModal.prototype.initEvents = function () {
        var _this = this;
        this.$signInBtn.on('click', function (e) {
            _this.checkValues(_this.$formItems);
            if (_this.validateForm()) {
                _this.onSignInButtonClick(e);
            }
            else {
                _this.preventFormSubmit(e);
            }
        });
    };
    PersonalInfoModal.prototype.onSignInButtonClick = function (e) {
        e.preventDefault();
    };
    PersonalInfoModal.prototype.checkValues = function (els) {
        var i, $element;
        for (i = 0; i < els.length; i++) {
            $element = $(els[i]);
            if ($element.val() === "") {
                $element.addClass("error");
            }
            else {
                $element.removeClass("error");
            }
        }
    };
    PersonalInfoModal.prototype.validateForm = function () {
        return this.$el.find(".error").length;
    };
    PersonalInfoModal.prototype.preventFormSubmit = function (e) {
        e.preventDefault();
        var self = this;
        var params = {
            method: "POST",
            url: this.request_link,
            data: this.$el.serialize(),
            success: function (data) {
                window.location.replace(self.redirect_link);
            },
            error: function (error) {
                console.log("error", error);
            }
        };
        $.ajax(params);
    };
    return PersonalInfoModal;
}(View));
var TutorialModal = (function (_super) {
    __extends(TutorialModal, _super);
    function TutorialModal() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('#tutorial');
        _this.step = 1;
        return _this;
    }
    TutorialModal.prototype.init = function () {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
            this.checkForDeeplink();
        }
    };
    TutorialModal.prototype.setSelectors = function () {
        this.$el.find('.next-step-btn');
        this.$steps = this.$el.find('.steps').find('li');
        this.$nextBtn = this.$el.find('.next-step-btn');
        this.$navLis = this.$el.find('.nav').find('.dot');
        this.$closeBtns = this.$el.find('.close-modal');
        this.$stepsUl = this.$el.find('.steps');
    };
    TutorialModal.prototype.initEvents = function () {
        var _this = this;
        this.$nextBtn.on('click', function (e) { return _this.onNextBtnClick(e); });
        this.$navLis.on('click', function (e) { return _this.onNavLiClick(e); });
        this.$closeBtns.on('click', function (e) { return _this.onCloseBtnClick(e); });
    };
    TutorialModal.prototype.onNextBtnClick = function (e) {
        if (this.step < 3) {
            this.step++;
            this.gotoStep(this.step);
        }
        else {
            this.$el.find('.close-modal').trigger('click');
            this.resetTutorial();
        }
    };
    TutorialModal.prototype.onNavLiClick = function (e) {
        this.step = $(e.currentTarget).index() + 1;
        this.gotoStep(this.step);
    };
    TutorialModal.prototype.gotoStep = function (step) {
        var _this = this;
        this.$stepsUl.fadeOut(400, function () {
            _this.$steps.hide().eq(step - 1).show();
            _this.$stepsUl.fadeIn(500);
        });
        this.$navLis.removeClass('active').eq(step - 1).addClass('active');
        this.setNextBtnText();
    };
    TutorialModal.prototype.setNextBtnText = function () {
        if (this.step == 3) {
            this.$nextBtn.find('.next-text').hide();
            this.$nextBtn.find('.finish-text').show();
        }
        else {
            this.$nextBtn.find('.next-text').show();
            this.$nextBtn.find('.finish-text').hide();
        }
    };
    TutorialModal.prototype.resetTutorial = function () {
        var _this = this;
        setTimeout(function () {
            _this.step = 1;
            _this.gotoStep(_this.step);
        }, 1500);
    };
    TutorialModal.prototype.onCloseBtnClick = function (e) {
        this.resetTutorial();
    };
    TutorialModal.prototype.checkForDeeplink = function () {
        if (URLHelper.getParameterByName('tutorial') == '1') {
            $('[data-modal=#tutorial]').trigger('click');
        }
    };
    return TutorialModal;
}(View));
var VideoModal = (function (_super) {
    __extends(VideoModal, _super);
    function VideoModal() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('#video');
        return _this;
    }
    VideoModal.prototype.init = function () {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    };
    VideoModal.prototype.setSelectors = function () {
        this.$title = this.$el.find('.video-title');
        this.$instructor = this.$el.find('.video-instructor');
        this.$bodyPart = this.$el.find('.video-body-part');
        this.$duration = this.$el.find('.video-duration');
        this.$equipment = this.$el.find('.video-equipment');
        this.$products = this.$el.find('.recommended-items');
    };
    VideoModal.prototype.initEvents = function () {
        var _this = this;
        $('[data-video]').on('click', function (e) { return _this.onVideoThumbClick(e); });
        $('[data-preview-video]').on('click', function (e) { return _this.onPreviewVideoThumbClick(e); });
        this.app.getModel(VideoPaginationModel).pageChanged.add(function () { return _this.onVideoPaginationChange(); });
        this.app.getModel(MashupMakerModel).liAdded.add(function () { return _this.onMashupEditorLiAdded(); });
    };
    VideoModal.prototype.onVideoThumbClick = function (e) {
        var videoId = $(e.currentTarget).data('video');
        this.loadVideoData(videoId);
    };
    VideoModal.prototype.loadVideoData = function (videoId) {
        var _this = this;
        $.getJSON('/video/' + videoId, function (data) {
            Global.redirectToLoginIfSessionTimesout(data);
            _this.onVideoDataLoaded(data);
        }).fail(function () {
            console.error('Could not load video data!');
        });
    };
    VideoModal.prototype.onVideoDataLoaded = function (data) {
        var _this = this;
        var productsTitle = this.$el.find('.recommended-items-title');
        this.$title.html(data.title);
        this.$bodyPart.html(data.body_part);
        this.$duration.html(data.time);
        this.$equipment.html(data.equipment);
        if (data.instructor_profile_name) {
            this.$instructor.addClass('description-item').html("<div><h3>YOUR INSTRUCTOR</h3><span>" + data.instructor_profile_name + "</span></div>");
        }
        else {
            this.$instructor.removeClass('description-item').html('');
        }
        if (data.products.length > 0) {
            productsTitle.html('<strong class="recom-title">Recommended items to buy:</strong>');
            this.drawRecomendedProducts(data, this.$products);
            this.$el.find('.lockout').addClass('has-product');
        }
        else {
            productsTitle.html('');
            this.$products.html('');
            this.$el.find('.lockout').removeClass('has-product');
        }
        // wait for scrim to be visible before playing video
        if ($('#video').find('.bc-player').is(':visible')) {
            new Equipment(this.$el.find('.equipment-required'), data.products);
            new VideoEmbed([{
                    poster: data.meta.videoStillURL,
                    rmtp: data.meta.rmtp,
                    mobile: data.meta.source_720,
                    id: data.id,
                    mediaid: data.meta.id
                }], this.$el);
        }
        else {
            setTimeout(function () {
                _this.onVideoDataLoaded(data);
            }, 30);
        }
        // Initialize social share buttons
        this.initSocialWidget("#social_widget_0", data);
    };
    VideoModal.prototype.initSocialWidget = function (widgetId, data) {
        var title = this.replaceSpecCharacters(data.title);
        $(widgetId).jsSocials({
            showLabel: "",
            showCount: "",
            shareIn: "popup",
            text: "Hi everyone, I just did Physique 57's " + title + " video. Help me stay motivated by liking this post!",
            url: window.location.origin + "/videoPage/show/" + data.id,
            shares: [{ "share": "facebook", "logo": "fa fa-facebook", "label": "facebook" }, { "share": "twitter", "logo": "fa fa-twitter", "label": "twitter", 'shareUrl': "https://twitter.com/share?url=''&text={text}" }]
        });
    };
    VideoModal.prototype.drawRecomendedProducts = function (data, prodEl) {
        prodEl.html('');
        data.products.forEach(function (element) {
            var price = element.price ? "$ " + element.price : '', priceColor = "style=\"background-color: #" + element.price_color + "\"";
            prodEl.append("<li>\n                                    <a href=\"" + element.url + "\" target=\"_blank\" rel=\"noopener noreferrer\">\n                                        <img src=\"" + element.thumbUrl + "\" alt=\"image description\" />\n                                        <div class=\"recommended-items__description\">\n                                            <span class=\"title\">" + element.title + "</span>\n                                            <span class=\"price\" " + priceColor + ">\n                                                " + price + "\n                                            </span>\n                                        </div>\n                                    </a>\n                                </li>");
        });
    };
    VideoModal.prototype.onPreviewVideoThumbClick = function (e) {
        var videoId = $(e.currentTarget).data('preview-video');
        this.loadPreviewVideoData(videoId);
    };
    VideoModal.prototype.loadPreviewVideoData = function (videoId) {
        var _this = this;
        $.getJSON('/preview/' + videoId, function (data) {
            Global.redirectToLoginIfSessionTimesout(data);
            _this.onVideoDataLoaded(data);
        }).fail(function () {
            console.error('Could not load video data!');
        });
    };
    VideoModal.prototype.onVideoPaginationChange = function () {
        var _this = this;
        $('[data-video]').on('click', function (e) { return _this.onVideoThumbClick(e); });
    };
    VideoModal.prototype.onMashupEditorLiAdded = function () {
        var _this = this;
        $('[data-video]').on('click', function (e) { return _this.onVideoThumbClick(e); });
    };
    VideoModal.prototype.replaceSpecCharacters = function (str) {
        return str.replace(/&amp;/g, '&')
            .replace(/&gt;/g, '>')
            .replace(/&lt;/g, '<')
            .replace(/&quot;/g, '"')
            .replace(/&#039;/g, "'");
    };
    return VideoModal;
}(View));
var MashupModal = (function (_super) {
    __extends(MashupModal, _super);
    function MashupModal() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('#mashup');
        return _this;
    }
    MashupModal.prototype.init = function () {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    };
    MashupModal.prototype.setSelectors = function () {
        this.$title = this.$el.find('.video-title');
        this.$description = this.$el.find('.description').find('p');
        this.$thumbSlider = this.$el.find('.thumb-slider');
    };
    MashupModal.prototype.initEvents = function () {
        var _this = this;
        $('[data-playlist]').on('click', function (e) { return _this.onVideoThumbClick(e); });
        $('[data-regimen-id]').find('ol').on('click', function (e) { return _this.onRegimenClick(e); });
        this.app.getModel(MashupMakerModel).liAdded.add(function () { return _this.onVideoEditorLiAdded(); });
    };
    MashupModal.prototype.onVideoThumbClick = function (e) {
        var playlistId = $(e.currentTarget).data('playlist');
        this.curIndex = 0;
        this.loadPlaylistData(playlistId);
        if (this.$thumbSlider.hasClass('slick-initialized')) {
            this.$thumbSlider.slick('unslick').hide();
        }
    };
    MashupModal.prototype.loadPlaylistData = function (playlistId) {
        var _this = this;
        $.getJSON('/playlist/' + playlistId, function (data) {
            Global.redirectToLoginIfSessionTimesout(data);
            _this.onJsonLoad(data);
        }).fail(function () {
            console.error('Could not load playlist data!');
        });
    };
    MashupModal.prototype.onRegimenClick = function (e) {
        var regimentId = $(e.currentTarget).closest('li').data('regimen-id');
        var day = $(e.currentTarget).closest('li').data('regimen-day');
        this.curIndex = 0;
        this.loadRegimenPlaylistData(regimentId, day);
        if (this.$thumbSlider.hasClass('slick-initialized')) {
            this.$thumbSlider.slick('unslick').hide();
        }
    };
    MashupModal.prototype.loadRegimenPlaylistData = function (regimenId, day) {
        var _this = this;
        $.getJSON('/regimen/mashup?regimen_id=' + regimenId + '&day=' + day, function (data) {
            Global.redirectToLoginIfSessionTimesout(data);
            _this.onJsonLoad(data);
        }).fail(function () {
            console.error('Could not load playlist data!');
        });
    };
    MashupModal.prototype.onJsonLoad = function (data) {
        var _this = this;
        this.data = data;
        this.$title.html(data.videos[0].title + ' (' + data.videos.length + ' workouts)');
        // wait for scrim to be visible before playing video
        if ($('#mashup').find('.bc-player').is(':visible')) {
            this.initVideoThumbs();
            this.initVideoEmbed();
        }
        else {
            setTimeout(function () {
                _this.onJsonLoad(data);
            }, 30);
        }
    };
    MashupModal.prototype.initVideoThumbs = function () {
        var _this = this;
        this.$thumbSlider.empty();
        for (var i = 0; i < this.data.videos.length; i++) {
            var curVideo = this.data.videos[i];
            var thumbHtml = $('#mashup-thumb').html();
            var $thumb = $(thumbHtml);
            $thumb.find('img').attr('src', curVideo.meta.videoStillURL);
            $thumb.find('.title').html(curVideo.title);
            this.$thumbSlider.append($thumb);
        }
        this.$thumbSlider.find('li').on('click', function (e) { return _this.onThumbClick(e); });
        setTimeout(function () {
            _this.initSlickCarousel();
        }, 1000);
    };
    MashupModal.prototype.initSlickCarousel = function () {
        if (this.$thumbSlider.hasClass('slick-initialized')) {
            this.$thumbSlider.slick('unslick');
        }
        this.$thumbSlider.fadeIn().slick({
            infinite: false,
            dots: false,
            arrows: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            accessibility: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 850,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ]
        });
        this.setActiveThumb(0);
    };
    MashupModal.prototype.setActiveThumb = function (index) {
        var $lis = this.$thumbSlider.find('li');
        $lis.removeClass('active').eq(index).addClass('active');
        this.$thumbSlider.slick('slickGoTo', index);
    };
    MashupModal.prototype.showEquipment = function (index) {
        new Equipment(this.$el.find('.equipment-required'), this.data.videos[index].products);
    };
    MashupModal.prototype.onThumbClick = function (e) {
        this.videoEmbed.gotoVideo($(e.currentTarget).index());
    };
    MashupModal.prototype.initVideoEmbed = function () {
        var videos = [];
        for (var i = 0; i < this.data.videos.length; i++) {
            videos.push({
                poster: this.data.videos[i].meta.videoStillURL,
                rmtp: this.data.videos[i].meta.rmtp,
                mobile: this.data.videos[i].meta.source_720,
                id: this.data.videos[i].id,
                mediaid: this.data.videos[i].meta.id
            });
        }
        this.videoEmbed = new VideoEmbed(videos, this.$el, this);
    };
    MashupModal.prototype.onVideoEditorLiAdded = function () {
        var _this = this;
        $('[data-playlist]').on('click', function (e) { return _this.onVideoThumbClick(e); });
    };
    return MashupModal;
}(View));
var VideoEmbed = (function (_super) {
    __extends(VideoEmbed, _super);
    function VideoEmbed(videos, $modal, $mashupRef) {
        if ($mashupRef === void 0) { $mashupRef = null; }
        var _this = _super.call(this) || this;
        _this.videos = videos;
        _this.$modal = $modal;
        _this.$mashupRef = $mashupRef;
        _this.INITIAL_VOLUME = 80;
        _this.index = 0;
        _this.embedPlayer();
        _this.initEvents();
        return _this;
    }
    VideoEmbed.prototype.embedPlayer = function () {
        var $embed = $($('#video-embed').html());
        this.$modal.find('.player').empty().append($embed);
        this.videoPlayer = jwplayer("jw-player");
        var isPlaylist = this.videos.length > 1;
        this.videoPlayer.setup({
            playlist: isPlaylist ? this.getPlaylistVideos() : null,
            sources: isPlaylist ? null : [{ file: this.videos[this.index].rmtp }, { file: this.videos[this.index].mobile }],
            autostart: false,
            height: '100%',
            width: '100%',
            mediaid: isPlaylist ? null : this.videos[this.index].mediaid,
            image: this.videos[this.index].poster
        }).setVolume(this.INITIAL_VOLUME);
    };
    VideoEmbed.prototype.getPlaylistVideos = function () {
        var videos = [];
        for (var i = 0; i < this.videos.length; i++) {
            var curVideo = this.videos[i];
            videos.push({
                sources: [{ file: curVideo.rmtp }, { file: curVideo.mobile }],
                image: curVideo.poster,
                mediaid: curVideo.mediaid,
                title: 'Video ' + (i + 1)
            });
        }
        return videos;
    };
    VideoEmbed.prototype.initEvents = function () {
        var _this = this;
        this.app.getModel(ModalsModel).modalClosed.add(function () { return _this.onModalClosed(); });
        this.videoPlayer.on('complete', function (e) { return _this.onVideoComplete(e); });
        this.videoPlayer.on('firstFrame', function (e) { return _this.onVideoPlay(e); });
        this.videoPlayer.on('playlistItem', function (e) { return _this.onPlaylistItemChanged(e); });
    };
    VideoEmbed.prototype.onVideoComplete = function (e) {
        var _this = this;
        this.trackPlayedVideo();
        if (this.videos.length > 1) {
            if (this.index < this.videos.length - 1) {
                this.videoPlayer.off('playlistItem', function (e) { return _this.onPlaylistItemChanged(e); });
                this.loadVideo(++this.index);
            }
        }
    };
    VideoEmbed.prototype.onPlaylistItemChanged = function (e) {
        //_kmq.push(['record', 'Started Video', {'Started Video ID': this.videos[e.index].id}]);
        this.setActiveThumbAndEquipment(e.index);
    };
    VideoEmbed.prototype.loadVideo = function (index) {
        this.videoPlayer.playlistItem(index);
        this.setActiveThumbAndEquipment(index);
    };
    VideoEmbed.prototype.setActiveThumbAndEquipment = function (index) {
        if (this.$mashupRef) {
            this.$mashupRef.setActiveThumb(index);
            this.$mashupRef.showEquipment(index);
        }
    };
    VideoEmbed.prototype.gotoVideo = function (index) {
        this.index = index;
        this.loadVideo(this.index);
    };
    VideoEmbed.prototype.onModalClosed = function () {
        this.videoPlayer.remove();
        this.$modal.find('.player').empty();
    };
    VideoEmbed.prototype.trackPlayedVideo = function () {
        _kmq.push(['record', 'Finished Video', { 'Finished Video ID': this.videos[this.index].id }]);
        woopra.track("video_played", { 'Finished Video ID': this.videos[this.index].id });
        ga('send', {
            hitType: 'event',
            eventCategory: 'Videos',
            eventAction: 'finished',
            eventLabel: this.videos[this.index].id
        });
        $.getJSON('/video/played/' + this.videos[this.index].id, function (data) {
            //Global.redirectToLoginIfSessionTimesout(data);
            //console.log('video tracked: ' + this.videos[this.index].id);
        }).fail(function () {
            console.error('Could not track video!');
        });
    };
    VideoEmbed.prototype.onVideoPlay = function (e) {
        _kmq.push(['record', 'Started Video', { 'Started Video ID': this.videos[this.index].id }]);
        woopra.track("video_started", { 'Started Video ID': this.videos[this.index].id });
        ga('send', {
            hitType: 'event',
            eventCategory: 'Videos',
            eventAction: 'play',
            eventLabel: this.videos[this.index].id
        });
    };
    return VideoEmbed;
}(View));
var MashupNameModal = (function (_super) {
    __extends(MashupNameModal, _super);
    function MashupNameModal() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('#mashup-name');
        return _this;
    }
    MashupNameModal.prototype.init = function () {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    };
    MashupNameModal.prototype.setSelectors = function () {
        this.$saveBtn = this.$el.find('.btn');
    };
    MashupNameModal.prototype.initEvents = function () {
        var _this = this;
        this.$saveBtn.on('click', function (e) { return _this.onSaveBtnClick(e); });
    };
    MashupNameModal.prototype.onSaveBtnClick = function (e) {
        var mashupName = this.$el.find('input').val();
        $('#mashup-title').html(mashupName);
    };
    return MashupNameModal;
}(View));
var DeleteMashupConfirmationModal = (function (_super) {
    __extends(DeleteMashupConfirmationModal, _super);
    function DeleteMashupConfirmationModal() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$modal = $('#confirm-mashup-delete');
        return _this;
    }
    DeleteMashupConfirmationModal.prototype.init = function () {
        if (this.$modal.length) {
            this.setSelectors();
            this.initEvents();
        }
    };
    DeleteMashupConfirmationModal.prototype.setSelectors = function () {
        this.$confirmBtn = this.$modal.find('.confirm');
    };
    DeleteMashupConfirmationModal.prototype.initEvents = function () {
        var _this = this;
        $('[data-confirm-mashup-delete]').on('click', function (e) { return _this.onDeleteMashupBtnClick(e); });
        this.$confirmBtn.on('click', function (e) { return _this.onConfirmBtnClick(e); });
    };
    DeleteMashupConfirmationModal.prototype.onDeleteMashupBtnClick = function (e) {
        e.preventDefault();
        e.stopPropagation();
        this.playlistId = $(e.currentTarget).closest('.thumb').data('playlist');
        this.$li = $(e.currentTarget).closest('li');
    };
    DeleteMashupConfirmationModal.prototype.onConfirmBtnClick = function (e) {
        var _this = this;
        $.getJSON('/playlist/delete/' + this.playlistId, function (data) {
            Global.redirectToLoginIfSessionTimesout(data);
        }).fail(function () {
            console.error('Could not delete mashup!');
        });
        this.$li.fadeOut(400, function () {
            var $ul = _this.$li.parent();
            _this.$li.remove();
            _this.app.getModel(ModalsModel).mashupDeleted.dispatch();
        });
    };
    return DeleteMashupConfirmationModal;
}(View));
var EmailPreferences = (function (_super) {
    __extends(EmailPreferences, _super);
    function EmailPreferences() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('body.email-preferences');
        return _this;
    }
    EmailPreferences.prototype.init = function () {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    };
    EmailPreferences.prototype.setSelectors = function () {
        this.$updatesInput = this.$el.find('#updates');
        this.$offersInput = this.$el.find('#offers');
        this.$surveysInput = this.$el.find('#surveys');
        this.$unsubscribeInput = this.$el.find('#unsubscribe');
    };
    EmailPreferences.prototype.initEvents = function () {
        var _this = this;
        this.$unsubscribeInput.on('click', function (e) { return _this.onUnsubscribeInputClick(e); });
        this.$el.find('.optional').on('click', function (e) { return _this.onOptionalCheckboxClick(e); });
        ;
    };
    EmailPreferences.prototype.onUnsubscribeInputClick = function (e) {
        this.$updatesInput.prop('checked', false);
        this.$offersInput.prop('checked', false);
        this.$surveysInput.prop('checked', false);
    };
    EmailPreferences.prototype.onOptionalCheckboxClick = function (e) {
        if ($(e.currentTarget).prop('checked')) {
            this.$unsubscribeInput.prop('checked', false);
        }
    };
    return EmailPreferences;
}(View));
var Faq = (function (_super) {
    __extends(Faq, _super);
    function Faq() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('body.faq');
        return _this;
    }
    Faq.prototype.init = function () {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    };
    Faq.prototype.setSelectors = function () {
        this.$questions = this.$el.find('.question');
    };
    Faq.prototype.initEvents = function () {
        var _this = this;
        this.$questions.on('click', function (e) { return _this.onQuestionClick(e); });
    };
    Faq.prototype.onQuestionClick = function (e) {
        e.preventDefault();
        $(e.currentTarget).toggleClass('active').parent().find('.answer').slideToggle();
    };
    return Faq;
}(View));
var ManageProfile = (function (_super) {
    __extends(ManageProfile, _super);
    function ManageProfile() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('body.manage-profile');
        return _this;
    }
    ManageProfile.prototype.init = function () {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    };
    ManageProfile.prototype.setSelectors = function () {
        this.$profilePhotoInput = this.$el.find('#Profile_photo');
        this.$profileForm = this.$el.find('#yw0');
    };
    ManageProfile.prototype.initEvents = function () {
        var _this = this;
        this.$profilePhotoInput.on('change', function (e) { return _this.onProfileImageChange(e); });
    };
    ManageProfile.prototype.onProfileImageChange = function (e) {
        this.$profileForm.submit();
    };
    return ManageProfile;
}(View));
var ChangePlan = (function (_super) {
    __extends(ChangePlan, _super);
    function ChangePlan() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('body.change-plan');
        return _this;
    }
    ChangePlan.prototype.init = function () {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
            this.setCurrentPlanInModal(this.$el.find('.active'));
            if ($('form').data('locked-plan') == '1') {
                $('#under-contract').show();
            }
        }
    };
    ChangePlan.prototype.setSelectors = function () {
        this.$modal = this.$el.find('#confirm-plan');
        this.$continueBtn = this.$el.find('#continue-btn');
        this.$termsCheckbox = this.$el.find('#over_18');
    };
    ChangePlan.prototype.initEvents = function () {
        var _this = this;
        this.$el.find('input[type=radio]').on('change', function (e) { return _this.onRadioChange(e); });
        this.$continueBtn.on('click', function (e) { return _this.onContinueButtonClick(e); });
        this.$termsCheckbox.on('change', function () { return _this.onCheckboxChanged(); });
        $('#account-types').find('label').on('click', function (e) { return _this.onPlanClick(e); });
    };
    ChangePlan.prototype.onRadioChange = function (e) {
        this.$el.find('.text-danger').slideUp();
        var $li = $(e.currentTarget).closest('li');
        if ($li.hasClass('current')) {
            $('.checkbox-wrap, .buttons, .legal').hide();
            return;
        }
        else {
            $('.checkbox-wrap, .buttons').show();
        }
        this.setCurrentPlanInModal($li);
        // show correct legal text
        $('.legal').hide();
        $('.legal').filter('.' + $li.data('plan-id')).show();
    };
    ChangePlan.prototype.setCurrentPlanInModal = function ($selectedLi) {
        this.$modal.find('.confirm-image').attr('src', $selectedLi.find('img').attr('src'));
        this.$modal.find('.confirm-text-overlay').html($selectedLi.find('.text-overlay').html());
    };
    ChangePlan.prototype.onContinueButtonClick = function (e) {
        if (!this.$termsCheckbox.prop('checked')) {
            e.stopImmediatePropagation();
            this.$el.find('.text-danger').slideDown();
        }
    };
    ChangePlan.prototype.onCheckboxChanged = function () {
        if (this.$termsCheckbox.prop('checked')) {
            this.$el.find('.text-danger').slideUp();
        }
    };
    ChangePlan.prototype.onPlanClick = function (e) {
        if ($('form').data('locked-plan') == '1') {
            e.stopPropagation();
            e.stopImmediatePropagation();
            e.preventDefault();
        }
    };
    return ChangePlan;
}(View));
var Account = (function (_super) {
    __extends(Account, _super);
    function Account() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('body.account');
        return _this;
    }
    Account.prototype.init = function () {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    };
    Account.prototype.setSelectors = function () {
        this.$redeemBtn = this.$el.find('#redeem-btn');
        this.$referBtn = this.$el.find('#refer-btn');
    };
    Account.prototype.initEvents = function () {
        var _this = this;
        this.$redeemBtn.on('click', function (e) { return _this.onRedeemBtnClick(e); });
        this.$referBtn.on('click', function (e) { return _this.onReferBtnClick(e); });
        //this.$redeemBtn.on('submit', (e)=>this.onRedeemBtnClick(e));
    };
    Account.prototype.onRedeemBtnClick = function (e) {
    };
    Account.prototype.onReferBtnClick = function (e) {
    };
    return Account;
}(View));
var StreamPlayer = (function () {
    function StreamPlayer($, holder) {
        var _this = this;
        this.STATUS_REQUEST_INTERVAL = 5000;
        this.options = {};
        this.needToStartPlayer = false;
        if (!holder.length) {
            throw 'Invalid Player Element';
        }
        this.holder = holder;
        var interval = setInterval(function () {
            var playerId = _this.holder.find('.player-holder > .jwplayer').attr('id');
            if (playerId) {
                _this.initOptions(_this.options, $, $(holder).data(), _this.render);
                _this.render(true);
                clearInterval(interval);
            }
        }, 500);
    }
    StreamPlayer.prototype.initNativePlayer = function () {
        var playerId = this.holder.find('.player-holder > .jwplayer').attr('id');
        if (!playerId) {
            return;
        }
        var player = jwplayer(playerId);
        player.on('ready', function () {
            if (this.needToStartPlayer) {
                player.play();
            }
            player.setMute(true);
        }).on('fullscreen', function () {
            player.setMute(false);
        }).on('play', function () {
            if ($('[data-role="player-fullscreen"] > span').length) {
                player.setVolume(0);
            }
        });
        this.holder.find('[data-role="player-fullscreen"]').click(function (ev) {
            $(ev.target).remove();
            player.setFullscreen(true);
            player.setMute(false);
        });
        this.player = player;
        return player;
    };
    StreamPlayer.prototype.render = function (isFull) {
        var _this = this;
        if (isFull === void 0) { isFull = false; }
        if ((this.options.time.start.getTime() - this.options.time.startIn) > Date.now()) {
            if (!this.holder.is(':hidden')) {
                this.holder.hide();
                this.options.ui.hideEl.show();
            }
            return;
        }
        else {
            if (this.holder.is(':hidden')) {
                this.holder.show();
                this.options.ui.hideEl.hide();
            }
        }
        if (!this.options.status.enabled || this.options.time.end.getTime() < Date.now()) {
            this.holder.find('.player-content').hide();
            this.holder.data('status', 'disabled');
            if (this.player) {
                this.player.stop();
            }
            this.options.ui.hideEl.show();
            return;
        }
        if (isFull) {
            this.initNativePlayer();
        }
        this.options.ui.hideEl.hide();
        if (Date.now() >= this.options.time.start.getTime() && this.holder.data('status') !== 'streaming') {
            this.holder.find('.message-holder').hide();
            this.holder.find('.player-holder').show().removeClass('hidden');
            this.holder.data('status', 'streaming');
            if (this.player) {
                this.needToStartPlayer = true;
                this.player.play();
                this.player.setMute(true);
            }
        }
        if (this.options.status.enabled &&
            Date.now() < this.options.time.start.getTime() &&
            this.holder.data('status') !== 'waiting') {
            this.holder.find('.player-holder').hide();
            this.holder.find('.message-holder')
                .css('background-image', this.options.backgrounds.countdown ? "url(" + this.options.backgrounds.countdown + ")" : 'none')
                .show().removeClass('hidden');
            if (this.player) {
                this.player.stop();
            }
            if (!this.holder.data('countdown-interval')) {
                var countdownInterval = setInterval(function () {
                    if (_this.holder.data('status') !== 'waiting') {
                        return;
                    }
                    var pad = function (num) {
                        return ('0' + num).substr(-2);
                    };
                    var diffFullSeconds = Math.round((_this.options.time.start.getTime() - Date.now()) / 1000);
                    var diffHours = Math.floor(diffFullSeconds / (60 * 60));
                    var diffMinutes = Math.floor((diffFullSeconds / 60) - (diffHours * 60));
                    var diffSeconds = Math.floor((diffFullSeconds) - (diffHours * (60 * 60)) - (diffMinutes * 60));
                    var minutesTimerMessage = pad(diffHours) + " : " + pad(diffMinutes);
                    var responsiveTimerMessage = pad(diffHours ? diffHours : diffMinutes) + " : " + pad(diffHours ? diffMinutes : diffSeconds);
                    var fullTimerMessage = pad(diffHours) + " : " + pad(diffMinutes) + " : " + pad(diffSeconds);
                    var timerMessage = _this.options.messages.countdown
                        .replace('{{minutes_timer}}', minutesTimerMessage)
                        .replace('{{timer}}', responsiveTimerMessage)
                        .replace('{{full_timer}}', fullTimerMessage);
                    if (timerMessage != _this.holder.data('countdown-timermessage') && diffFullSeconds >= 0) {
                        _this.holder.find('.message-holder .message-wrapper').html(timerMessage);
                        _this.holder.data('countdown-timermessage', timerMessage);
                    }
                }, 1000);
                this.holder.data('countdown-interval', countdownInterval);
            }
            this.holder.data('status', 'waiting');
        }
        if (!this.options.status.onLive &&
            this.options.status.enabled &&
            Date.now() >= this.options.time.start.getTime() &&
            this.holder.data('status') !== 'ready-to-start') {
            if (this.player) {
                this.player.stop();
            }
            this.holder.find('.player-holder').hide();
            this.holder.find('.message-holder')
                .css('background-image', this.options.backgrounds.readyToStart ? "url(" + this.options.backgrounds.readyToStart + ")" : 'none')
                .show().removeClass('hidden');
            this.holder.find('.message-holder .message-wrapper').html(this.options.messages.readyToStart);
            this.holder.data('status', 'ready-to-start');
        }
    };
    StreamPlayer.prototype.assign = function (first, second) {
        for (var key in second) {
            if (second.hasOwnProperty(key)) {
                if (typeof first[key] === 'undefined') {
                    first[key] = second[key];
                }
            }
        }
    };
    StreamPlayer.prototype.initOptions = function (options, $, data, onChangeCb) {
        var _this = this;
        var isOnRequest = false;
        this.assign(options, {
            time: {
                start: new Date(data['startLiveAt'] * 1000),
                startIn: data['startVideoIn'] * 60000,
                end: new Date(data['endLiveAt'] * 1000)
            },
            status: {
                enabled: data['enabled'],
                onLive: true
            },
            messages: {
                countdown: data['countdownMessage'],
                readyToStart: data['readyToStartMessage']
            },
            backgrounds: {
                countdown: data['countdownBackground'],
                readyToStart: data['readyToStartBackground']
            },
            data: {
                embed: data['embed'],
                updateUrl: data['updateUrl']
            },
            ui: {
                hideEl: $(data['hideContent'])
            }
        });
        var updateStatus = function () {
            return $.getJSON(options.data.updateUrl, function (data) {
                options.status.enabled = data.enabled && data.enabled === "1";
                options.status.onLive = true;
                options.time.start = new Date(data.startLiveAt * 1000);
                options.time.startIn = data.startVideoIn * 60000;
                options.time.end = new Date(data.endLiveAt * 1000);
                onChangeCb.bind(_this).call();
            });
        };
        setInterval(function () {
            if (isOnRequest) {
                return;
            }
            isOnRequest = true;
            updateStatus().always(function () {
                isOnRequest = false;
            });
        }, this.STATUS_REQUEST_INTERVAL);
    };
    return StreamPlayer;
}());
var LiveStream = (function () {
    function LiveStream($, holder) {
        this.STATUS_REQUEST_INTERVAL = 5000;
        this.options = {};
        this.needToStartSound = false;
        if (!holder.length) {
            throw 'Invalid Player Element';
        }
        this.holder = holder;
        this.initOptions(this.options, $, $(holder).data(), this.render);
        this.render(true);
    }
    LiveStream.prototype.initNativePlayer = function () {
        var _this = this;
        var _self = this;
        var fullScreenBtn = $('[data-role="player-fullscreen"]');
        fullScreenBtn.click(function () {
            var iframe = document.querySelector('#iframe-stream-player iframe');
            iframe.src = iframe.src.replace(/mute=true/, 'mute=false');
            _this.needToStartSound = true;
            fullScreenBtn.remove();
        });
    };
    LiveStream.prototype.render = function (isFull) {
        var _this = this;
        if (isFull === void 0) { isFull = false; }
        if ((this.options.time.start.getTime() - this.options.time.startIn) > Date.now()) {
            if (!this.holder.is(':hidden')) {
                this.holder.hide();
                this.options.ui.hideEl.show();
            }
            return;
        }
        else {
            if (this.holder.is(':hidden')) {
                this.holder.show();
                this.options.ui.hideEl.hide();
            }
        }
        if (!this.options.status.enabled || this.options.time.end.getTime() < Date.now()) {
            var iframe = document.querySelector('#iframe-stream-player iframe');
            this.holder.find('.player-content').hide();
            if (iframe.src.indexOf('mute=false') !== -1) {
                iframe.src = iframe.src.replace(/mute=false/, 'mute=true');
            }
            this.holder.data('status', 'disabled');
            this.options.ui.hideEl.show();
            return;
        }
        if (isFull) {
            this.initNativePlayer();
        }
        this.options.ui.hideEl.hide();
        if (Date.now() >= this.options.time.start.getTime() && this.holder.data('status') !== 'streaming') {
            this.holder.find('.message-holder').hide();
            this.holder.find('.player-holder').show().removeClass('hidden');
            this.holder.data('status', 'streaming');
            var iframe = document.querySelector('#iframe-stream-player iframe');
            if (this.needToStartSound && iframe.src.indexOf('mute=true') !== -1) {
                iframe.src = iframe.src.replace(/mute=true/, 'mute=false');
            }
        }
        if (this.options.status.enabled && Date.now() < this.options.time.start.getTime() && this.holder.data('status') !== 'waiting') {
            var iframe = document.querySelector('#iframe-stream-player iframe');
            this.holder.find('.player-holder').hide();
            if (iframe.src.indexOf('mute=false') !== -1) {
                iframe.src = iframe.src.replace(/mute=false/, 'mute=true');
            }
            this.holder.find('.message-holder')
                .css('background-image', this.options.backgrounds.countdown ? "url(" + this.options.backgrounds.countdown + ")" : 'none')
                .show().removeClass('hidden');
            if (!this.holder.data('countdown-interval')) {
                var countdownInterval = setInterval(function () {
                    if (_this.holder.data('status') !== 'waiting') {
                        return;
                    }
                    var pad = function (num) {
                        return ('0' + num).substr(-2);
                    };
                    var diffFullSeconds = Math.round((_this.options.time.start.getTime() - Date.now()) / 1000);
                    var diffHours = Math.floor(diffFullSeconds / (60 * 60));
                    var diffMinutes = Math.floor((diffFullSeconds / 60) - (diffHours * 60));
                    var diffSeconds = Math.floor((diffFullSeconds) - (diffHours * (60 * 60)) - (diffMinutes * 60));
                    var minutesTimerMessage = pad(diffHours) + " : " + pad(diffMinutes);
                    var responsiveTimerMessage = pad(diffHours ? diffHours : diffMinutes) + " : " + pad(diffHours ? diffMinutes : diffSeconds);
                    var fullTimerMessage = pad(diffHours) + " : " + pad(diffMinutes) + " : " + pad(diffSeconds);
                    var timerMessage = _this.options.messages.countdown
                        .replace('{{minutes_timer}}', minutesTimerMessage)
                        .replace('{{timer}}', responsiveTimerMessage)
                        .replace('{{full_timer}}', fullTimerMessage);
                    if (timerMessage != _this.holder.data('countdown-timermessage') && diffFullSeconds >= 0) {
                        _this.holder.find('.message-holder .message-wrapper').html(timerMessage);
                        _this.holder.data('countdown-timermessage', timerMessage);
                    }
                }, 1000);
                this.holder.data('countdown-interval', countdownInterval);
            }
            this.holder.data('status', 'waiting');
        }
    };
    LiveStream.prototype.assign = function (first, second) {
        for (var key in second) {
            if (second.hasOwnProperty(key)) {
                if (typeof first[key] === 'undefined') {
                    first[key] = second[key];
                }
            }
        }
    };
    LiveStream.prototype.initOptions = function (options, $, data, onChangeCb) {
        var _this = this;
        var isOnRequest = false;
        this.assign(options, {
            time: {
                start: new Date(data['startLiveAt'] * 1000),
                startIn: data['startVideoIn'] * 60000,
                end: new Date(data['endLiveAt'] * 1000)
            },
            status: {
                enabled: data['enabled'],
                onLive: true
            },
            messages: {
                countdown: data['countdownMessage'],
                readyToStart: data['readyToStartMessage']
            },
            backgrounds: {
                countdown: data['countdownBackground'],
                readyToStart: data['readyToStartBackground']
            },
            data: {
                embed: data['embed'],
                updateUrl: data['updateUrl']
            },
            ui: {
                hideEl: $(data['hideContent'])
            }
        });
        var updateStatus = function () {
            return $.getJSON(options.data.updateUrl, function (data) {
                options.status.enabled = data.enabled && data.enabled === "1";
                options.status.onLive = true;
                options.time.start = new Date(data.startLiveAt * 1000);
                options.time.startIn = data.startVideoIn * 60000;
                options.time.end = new Date(data.endLiveAt * 1000);
                onChangeCb.bind(_this).call();
            });
        };
        setInterval(function () {
            if (isOnRequest) {
                return;
            }
            isOnRequest = true;
            updateStatus().always(function () {
                isOnRequest = false;
            });
        }, this.STATUS_REQUEST_INTERVAL);
    };
    return LiveStream;
}());
/// <reference path="../models/StreamPlayer.ts" />
/// <reference path="../models/LiveStream.ts" />
var HomeLoggedIn = (function (_super) {
    __extends(HomeLoggedIn, _super);
    function HomeLoggedIn() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('body.home-logged-in');
        return _this;
    }
    HomeLoggedIn.prototype.init = function () {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
            this.initStreaming();
        }
    };
    HomeLoggedIn.prototype.setSelectors = function () {
        this.$workoutRegimens = this.$el.find('#workout-regimens');
        this.$workoutThumbs = this.$workoutRegimens.find('li');
        this.$seeMoreRegimensBtn = this.$workoutRegimens.find('#see-more-regimens');
    };
    HomeLoggedIn.prototype.initEvents = function () {
        var _this = this;
        this.$workoutThumbs.on('click', function (e) { return _this.onWorkoutThumbClick(e); });
        this.$document.on('touchstart', function (e) { return _this.onDocumentTouchStart(e); });
        this.$seeMoreRegimensBtn.on('click', function (e) { return _this.onSeeMoreRegimensBtnClick(e); });
    };
    HomeLoggedIn.prototype.initStreaming = function () {
        var jwPlayer = $('#stream-player');
        var liveStreamPlayer = $('#iframe-stream-player');
        if (jwPlayer.length) {
            new StreamPlayer($, $('#stream-player'));
        }
        if (liveStreamPlayer.length) {
            new LiveStream($, $('#iframe-stream-player'));
        }
    };
    HomeLoggedIn.prototype.onWorkoutThumbClick = function (e) {
        if (Browser.isTouch) {
            $(e.currentTarget).find('.hover-overlay').fadeIn(400);
        }
    };
    HomeLoggedIn.prototype.onDocumentTouchStart = function (e) {
        this.$workoutThumbs.find('.hover-overlay').fadeOut(400);
    };
    HomeLoggedIn.prototype.onSeeMoreRegimensBtnClick = function (e) {
        e.preventDefault();
        this.$workoutThumbs.slideDown();
        this.$seeMoreRegimensBtn.fadeOut();
    };
    return HomeLoggedIn;
}(View));
var MyWorkouts = (function (_super) {
    __extends(MyWorkouts, _super);
    function MyWorkouts() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('body.my-workouts');
        return _this;
    }
    MyWorkouts.prototype.init = function () {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
            this.showNoMashupsMessageIfNeeded(this.$mashupVideos.find('ul'));
            this.showNoFavoritesMessageIfNeeded(this.$favoriteVideos.find('ul'));
        }
    };
    MyWorkouts.prototype.setSelectors = function () {
        this.$editMashupBtns = this.$el.find('.edit');
        this.$mashupVideos = this.$el.find('#mashup-videos');
        this.$favoriteVideos = this.$el.find('#favorite-videos');
        this.$deleteMashupBtns = this.$mashupVideos.find('.delete');
        this.$unsubscribeMashupBtns = this.$mashupVideos.find('.unsubscribe');
        this.$unfavoriteBtns = this.$favoriteVideos.find('.delete');
        this.$noMashupVideos = this.$mashupVideos.find('.none-available');
        this.$noFavoriteVideos = this.$favoriteVideos.find('.none-available');
    };
    MyWorkouts.prototype.initEvents = function () {
        var _this = this;
        this.$editMashupBtns.on('click', function (e) { return _this.onEditMashupBtnClick(e); });
        this.$unsubscribeMashupBtns.on('click', function (e) { return _this.onUnsubscribeMashupBtnClick(e); });
        this.$unfavoriteBtns.on('click', function (e) { return _this.onUnfavoriteBtnClick(e); });
        this.app.getModel(ModalsModel).mashupDeleted.add(function () { return _this.onMashupDeleted(); });
    };
    MyWorkouts.prototype.onEditMashupBtnClick = function (e) {
        e.stopPropagation();
        var id = $(e.currentTarget).parent().parent().data('playlist');
        window.location.href = '/mashup-maker?playlist_id=' + id;
    };
    MyWorkouts.prototype.onUnsubscribeMashupBtnClick = function (e) {
        var _this = this;
        e.stopPropagation();
        var id = $(e.currentTarget).parent().parent().data('playlist');
        $.getJSON('/playlist/unsubscribe/' + id, function (data) {
            Global.redirectToLoginIfSessionTimesout(data);
        }).fail(function () {
            console.error('Could not unsubscribe from playlist!');
        });
        var $li = $(e.currentTarget).closest('li');
        $li.fadeOut(400, function () {
            var $ul = $li.parent();
            $li.remove();
            _this.showNoMashupsMessageIfNeeded($ul);
        });
    };
    MyWorkouts.prototype.onUnfavoriteBtnClick = function (e) {
        var _this = this;
        e.stopPropagation();
        var $li = $(e.currentTarget).closest('li');
        $li.fadeOut(400, function () {
            var $ul = $li.parent();
            $li.remove();
            _this.showNoFavoritesMessageIfNeeded($ul);
        });
    };
    MyWorkouts.prototype.showNoFavoritesMessageIfNeeded = function ($ul) {
        if (!$ul.children().length) {
            this.$noFavoriteVideos.fadeIn();
        }
    };
    MyWorkouts.prototype.showNoMashupsMessageIfNeeded = function ($ul) {
        if (!$ul.children().length) {
            this.$noMashupVideos.fadeIn();
        }
    };
    MyWorkouts.prototype.onMashupDeleted = function () {
        console.log('here');
        var $ul = this.$mashupVideos.find('.video-thumbs');
        this.showNoMashupsMessageIfNeeded($ul);
    };
    return MyWorkouts;
}(View));
var MashupMaker = (function (_super) {
    __extends(MashupMaker, _super);
    function MashupMaker() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('body.mashup-maker');
        return _this;
    }
    MashupMaker.prototype.init = function () {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
            this.showPlaylistEditorOrCTA();
            this.initSortableList();
        }
    };
    MashupMaker.prototype.setSelectors = function () {
        this.$videoList = this.$el.find('#video-list');
        this.$addVideosToMashupCTA = this.$el.find('#add-video-to-mashup');
        this.$mashupVideoEditor = this.$el.find('#mashup-video-editor');
        this.$previewBtn = this.$el.find('#mashup-preview');
        this.$updateMashupNameModalBtn = $('#mashup-name').find('.btn');
    };
    MashupMaker.prototype.initEvents = function () {
        var _this = this;
        this.addAddToMashupListener();
        this.app.getModel(VideoPaginationModel).pageChanged.add(function () { return _this.onVideoPaginationChange(); });
        this.$updateMashupNameModalBtn.on('click', function (e) { return _this.onMashupNameSaveBtnClick(e); });
    };
    MashupMaker.prototype.showPlaylistEditorOrCTA = function () {
        this.playlistId = URLHelper.getParameterByName('playlist_id');
        if (this.playlistId) {
            this.$mashupVideoEditor.show();
            this.getPlaylistVideosAndDisplay();
            this.$previewBtn.attr('data-playlist', this.playlistId);
            this.app.getModel(MashupMakerModel).liAdded.dispatch();
        }
        else {
            this.$addVideosToMashupCTA.show();
        }
    };
    MashupMaker.prototype.getPlaylistVideosAndDisplay = function () {
        var _this = this;
        $.getJSON('/playlist/' + this.playlistId, function (data) {
            Global.redirectToLoginIfSessionTimesout(data);
            for (var i = 0; i < data.videos.length; i++) {
                _this.appendLi(data.videos[i]);
            }
            $('#mashup-title').html(data.title);
            _this.addRemoveCheckmark();
        }).fail(function () {
            console.error('Could not load playlist data!');
        });
    };
    MashupMaker.prototype.appendLi = function (videoData) {
        var _this = this;
        var videoLiHtml = $('#video-editor-li-tpl').html();
        var $li = $(videoLiHtml);
        $li.find('.play').attr('data-video', videoData.id);
        $li.find('.title').html(videoData.title);
        $li.find('.instructor').html(videoData.instructor_profile_name ? videoData.instructor_profile_name : '--');
        $li.find('.duration').prepend(Math.ceil(videoData.length / 60 / 1000).toString());
        console.log(videoData);
        var types = ['strength', 'cardio', 'restorative'];
        for (var j = 0; j < types.length; j++) {
            var className;
            if (videoData[types[j]] == '')
                className = 'none';
            if (videoData[types[j]] == null)
                className = 'none';
            if (videoData[types[j]] == 1)
                className = 'one';
            if (videoData[types[j]] == 2)
                className = 'two';
            if (videoData[types[j]] == 3)
                className = 'three';
            $li.find('.type').find('.' + types[j]).addClass(className);
        }
        this.$videoList.append($li);
        this.app.getModel(MashupMakerModel).liAdded.dispatch();
        $li.find('.remove').on('click', function (e) { return _this.onRemoveBtnClick(e); });
    };
    MashupMaker.prototype.onRemoveBtnClick = function (e) {
        var _this = this;
        var $li = $(e.currentTarget).parent();
        var videoId = $li.find('.play').data('video');
        $.getJSON('/playlist/remove?playlist_id=' + this.playlistId + '&video_id=' + videoId, function (data) {
            Global.redirectToLoginIfSessionTimesout(data);
        }).fail(function () {
            console.error('Could not remove video from playlist!');
        });
        $li.fadeOut(300, function () {
            $li.remove();
            _this.addRemoveCheckmark();
        });
    };
    MashupMaker.prototype.addAddToMashupListener = function () {
        var _this = this;
        $('.add-to-mashup').off('click').on('click', function (e) { return _this.onAddToMashupBtnClick(e); });
    };
    MashupMaker.prototype.onAddToMashupBtnClick = function (e) {
        if (this.playlistId.length) {
            this.addVideoToPlaylist(e);
        }
        else {
            this.createNewPlaylist(e);
        }
        $(e.currentTarget).closest('li').addClass('added');
    };
    MashupMaker.prototype.addVideoToPlaylist = function (e) {
        var _this = this;
        var videoId = $(e.currentTarget).parent().find('.thumb').data('video');
        var videoAlreadyInPlaylist = false;
        this.$videoList.find('.play').each(function (i, el) {
            var $item = $(el);
            if ($item.data('video') == videoId) {
                videoAlreadyInPlaylist = true;
            }
        });
        if (videoAlreadyInPlaylist)
            return;
        $.getJSON('/playlist/add?playlist_id=' + this.playlistId + '&video_id=' + videoId, function (data) {
            Global.redirectToLoginIfSessionTimesout(data);
            var dataObj = data.video ? data.video : data;
            _this.appendLi(dataObj);
            _this.addRemoveCheckmark();
        }).fail(function () {
            console.error('Could not add video to playlist!');
        });
    };
    MashupMaker.prototype.createNewPlaylist = function (e) {
        var _this = this;
        $('#mashup-title').html('My Mashup');
        $.getJSON('/playlist/create?title=My%20Mashup', function (data) {
            Global.redirectToLoginIfSessionTimesout(data);
            _this.playlistId = data.model.id;
            _this.$previewBtn.attr('data-playlist', _this.playlistId);
            _this.$mashupVideoEditor.show();
            _this.$addVideosToMashupCTA.hide();
            _this.addVideoToPlaylist(e);
            if (typeof (history.pushState) != "undefined") {
                history.pushState('', document.title, '?playlist_id=' + _this.playlistId);
            }
        }).fail(function () {
            console.error('Could not create new playlist!');
        });
    };
    MashupMaker.prototype.onVideoPaginationChange = function () {
        this.addAddToMashupListener();
        this.addRemoveCheckmark();
    };
    MashupMaker.prototype.onMashupNameSaveBtnClick = function (e) {
        $.getJSON('/playlist/update/' + this.playlistId + '?title=' + encodeURIComponent($('#mashup-name').find('input').val()), function (data) {
            Global.redirectToLoginIfSessionTimesout(data);
        }).fail(function () {
            console.error('Could not update playlist name!');
        });
    };
    MashupMaker.prototype.initSortableList = function () {
        var _this = this;
        var el = document.getElementById('video-list');
        var sortable = Sortable.create(el, {
            animation: 200,
            ghostClass: "sortable-ghost",
            chosenClass: "sortable-chosen",
            filter: ".disable-drag",
            handle: Breakpoints.current == Breakpoints.MOBILE ? ".drag-handle" : ".drag-li",
            onEnd: function () {
                var order = '';
                _this.$videoList.find('li').each(function (i, el) {
                    var $item = $(el);
                    order += $item.find('.play').data('video') + ',';
                });
                $.getJSON('/playlist/reorder/' + _this.playlistId + '?order=' + order, function (data) {
                    Global.redirectToLoginIfSessionTimesout(data);
                }).fail(function () {
                    console.error('Could not reorder playlist!');
                });
            }
        });
    };
    MashupMaker.prototype.addRemoveCheckmark = function () {
        var playlistIds = [];
        this.$videoList.find('li').each(function (i, el) {
            var $item = $(el);
            playlistIds.push($item.find('.play').data('video'));
        });
        this.$el.find('#video-thumbs').find('li').each(function (i, el) {
            var $item = $(el);
            var id = $item.find('.thumb').data('video');
            if (ArrayUtil.isInArray(id, playlistIds)) {
                $item.addClass('added');
            }
            else {
                $item.removeClass('added');
            }
        });
    };
    return MashupMaker;
}(View));
var MashupMakerModel = (function (_super) {
    __extends(MashupMakerModel, _super);
    function MashupMakerModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.liAdded = new signals.Signal();
        return _this;
    }
    return MashupMakerModel;
}(Model));
var Equipment = (function () {
    function Equipment($el, data) {
        this.$el = $el;
        this.data = data;
        this.$el.find('.equipment-thumbs').fadeOut(0);
        if (this.data.length) {
            this.$el.fadeIn();
            this.displayEquipment();
            this.initSlickCarousel();
        }
        else {
            this.$el.hide();
        }
    }
    Equipment.prototype.displayEquipment = function () {
        var $equipmentThumbs = this.$el.find('.equipment-thumbs');
        if ($equipmentThumbs.hasClass('slick-initialized')) {
            $equipmentThumbs.slick('unslick');
        }
        $equipmentThumbs.empty();
        for (var i = 0; i < this.data.length; i++) {
            var obj = this.data[i];
            var $li = $($('#equipment-li-tpl').html());
            $li.find('a').attr('href', obj.url);
            $li.find('img').attr('src', obj.thumbUrl);
            $equipmentThumbs.append($li);
        }
    };
    Equipment.prototype.initSlickCarousel = function () {
        var _this = this;
        var $thumbs = this.$el.find('.equipment-thumbs');
        $thumbs.show();
        if (!$thumbs.is(":visible")) {
            // retry until modal is faded in and we can init.
            // slick needs item to be visible so calculations are correct.
            setTimeout(function () {
                _this.initSlickCarousel();
            }, 100);
            return;
        }
        $thumbs.fadeIn().slick({
            infinite: false,
            dots: false,
            arrows: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            accessibility: false,
            edgeFriction: 0,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 750,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                }
            ]
        });
    };
    return Equipment;
}());
var Payment = (function (_super) {
    __extends(Payment, _super);
    function Payment() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.$el = $('body.payment');
        return _this;
    }
    Payment.prototype.init = function () {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    };
    Payment.prototype.setSelectors = function () {
        this.$plan = this.$el.find("#plans").find('.plan');
        this.$submitBtn = this.$el.find('#payment-details').find('.buttons').find('.btn');
    };
    Payment.prototype.initEvents = function () {
        var _this = this;
        this.$plan.on('click', function (e) { return _this.onPlanClick(e); });
        this.$submitBtn.on('click', function (e) { return _this.onSubmitButtonClick(e); });
    };
    Payment.prototype.onPlanClick = function (e) {
        if ($(e.currentTarget).index() == 0) {
            $('#Plan_id_0').prop('checked', true);
            $('#start-membership-btn').addClass('active');
            $('#start-membership-btn2').removeClass('active');
            $('.plan57').show();
            $('.plan30').hide();
        }
        else {
            $('#Plan_id_1').prop('checked', true);
            $('#start-membership-btn2').addClass('active');
            $('#start-membership-btn').removeClass('active');
            $('.plan57').hide();
            $('.plan30').show();
        }
        this.$plan.removeClass('active');
        $(e.currentTarget).addClass('active');
    };
    Payment.prototype.onSubmitButtonClick = function (e) {
        if (!$(e.currentTarget).hasClass('active')) {
            e.preventDefault();
        }
    };
    return Payment;
}(View));
var ValidateSignNew = (function (_super) {
    __extends(ValidateSignNew, _super);
    function ValidateSignNew() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ValidateSignNew.prototype.init = function () {
        this.initLoginForm();
        this.initPaymentForm();
        this.initAccordeon();
        this.initTermsInfoForm();
    };
    ValidateSignNew.prototype.initLoginForm = function () {
        var submit = $(".login-form > button");
        var that = this;
        submit.click(submitLoginForm);
        function submitLoginForm() {
            var loginForm = $(".login-form > .input-holder > input"), data = loginForm.serializeJSON(), store_step = 1;
            var formData = {
                store_step: store_step,
                store_data: data
            };
            var trial = $('.signup-section').data('trial');
            var accordion = $('.step-box > slide');
            // that.preventForm(true);
            $.post("/register-steps?trial=" + trial, formData).then(function (response) {
                var resp = JSON.parse(response);
                var step = resp.step;
                that.checkValidate(resp.errors, resp.fields, resp.success, store_step);
                if (step) {
                    that.redirectStep(step);
                    that.checkSpepsSelect(step);
                }
            }, function (response) {
                if (window.console !== undefined) {
                    console.error('Status Error', response.status);
                }
            });
        }
    };
    ValidateSignNew.prototype.initPaymentForm = function () {
        var submit = $(".payment-form > button");
        var that = this;
        submit.click(submitPaymentForm);
        function submitPaymentForm() {
            var paymentForm = $(".payment-form input, select"), data = paymentForm.serializeJSON(), store_step = 2;
            var formData = {
                store_step: store_step,
                store_data: data
            };
            var accordion = $('.step-box');
            if ($('.step-box:nth-child(2) .alert-danger').length) {
                $('.step-box:nth-child(2) .alert-danger').remove();
            }
            var trial = $('.signup-section').data('trial');
            $.post("/register-steps?trial=" + trial, formData).then(function (response) {
                var resp = JSON.parse(response);
                var step = resp.step;
                that.checkValidate(resp.errors, resp.fields, resp.success, store_step);
                if (resp.success) {
                    that.redirectStep(step);
                    that.checkSpepsSelect(step);
                    accordion.filter(':nth-child(2)').find('.slide').addClass("step-passed");
                    accordion.filter(':nth-child(3)').find('.slide').removeClass("hide-form");
                }
                else {
                    that.checkValidate(resp.errors, resp.fields, resp.success, store_step);
                }
            }, function (response) {
                if (window.console !== undefined) {
                    console.error('Status Error', response.status);
                }
            });
        }
    };
    ValidateSignNew.prototype.initTermsInfoForm = function () {
        var that = this;
        var priceText = $('.price-info .price del .number');
        var priceLimitedText = $('.price-info .frame--strong .price .number');
        var accordion = $(".step-box:nth-child(3) > .slide"), submit = $(".step-box:nth-child(3) .submit"), radioBtns = accordion.find("[type=radio]");
        $('.over_18').on('change', function () {
            if ($(this).is(":checked")) {
                if (radioBtns.is(":checked")) {
                    $(".step-box:nth-child(3)").find('.errors > span').remove();
                    $(".step-box:nth-child(3)").removeClass('incorect');
                    $(".step-box:nth-child(3)").find('.danger').removeClass('danger');
                }
            }
        });
        radioBtns.on('change', function () {
            var $this = $(this);
            if ($this.is(":checked")) {
                if ($('.over_18').is(":checked")) {
                    $(".step-box:nth-child(3)").find('.errors > span').remove();
                    $(".step-box:nth-child(3)").removeClass('incorect');
                    $(".step-box:nth-child(3)").find('.danger').removeClass('danger');
                }
                if ($('.step-box:nth-child(3)').find('.slide').is(":visible")) {
                    $(".step-box:nth-child(3)").addClass("edit");
                }
                var oldAmount = $this.data('old-amount');
                var amount = $this.data('amount');
                priceText.text('');
                priceText.text(oldAmount.toString());
                priceLimitedText.text('');
                priceLimitedText.text(amount.toString());
                accordion.find(".legal").css('display', 'none');
                accordion.find(".legal.plan" + oldAmount).css('display', 'block');
            }
        });
        submit.click(submitTermsInfoForm);
        function submitTermsInfoForm() {
            var form = $('#signup-form');
            var error = $('.step-box:nth-child(3) .errors');
            var termsInfoForm = $("#step3").find(':input'), data = termsInfoForm.serializeJSON(), store_step = 3;
            var formData = {
                store_step: store_step,
                store_data: data
            };
            if ($('.step-box:nth-child(3) .alert-danger').length) {
                $('.step-box:nth-child(3) .alert-danger').remove();
            }
            var trial = $('.signup-section').data('trial');
            $.post("/register-steps?trial=" + trial, formData).then(function (response) {
                var resp = JSON.parse(response);
                if (resp.success) {
                    error.html('');
                    form.submit();
                    that.checkValidate(resp.errors, resp.fields, resp.success, store_step);
                }
                else {
                    error.html('');
                    that.checkValidate(resp.errors, resp.fields, resp.success, store_step);
                    error.append($('<span>' + resp.errors + '</span>'));
                    if ($('.step-box:nth-child(3) .errors > span').length > 1) {
                        $('.step-box:nth-child(3)').find('.errors > span:nth-child(2)').remove();
                        ;
                    }
                }
            }, function (response) {
                if (window.console !== undefined) {
                    console.error(error);
                }
            });
            return false;
        }
        ;
    };
    ValidateSignNew.prototype.checkValidate = function (errors, fields, success, step) {
        var stepForm = $('.step-box:nth-child(' + step + ')'), errorString = $('.step-box:nth-child(' + step + ') .text-complete');
        if (stepForm.find('.errors > span').length) {
            stepForm.find('.errors > span').remove();
        }
        if (success == true) {
            stepForm.removeClass('incorect');
            stepForm.find('.errors').append($('<span></span>'));
            stepForm.find('.danger').removeClass('danger');
            errorString.removeClass("hidden");
        }
        else if (success !== true) {
            stepForm.addClass('incorect');
            errorString.addClass("hidden");
            stepForm.find('.errors').append($('<span>' + errors + '</span>'));
            if (fields.indexOf(',') > -1) {
                var str = fields.split(',').map(function (item) {
                    return '.' + item;
                }).join(',');
                stepForm.find(str).addClass('danger');
            }
            else {
                stepForm.find('.' + fields).addClass('danger');
            }
        }
    };
    ValidateSignNew.prototype.initAccordeon = function () {
        var that = this, steps = $('.step-box'), headerHeight = $('#header').height();
        $('.step-box .link').click(function (e) {
            e.preventDefault();
            if (!$('.step-box.incorect').length) {
                $('.step-box > .slide').addClass("hide-form");
                $(this).closest('.step-box').find('.slide').removeClass("hide-form");
                if ($('.step-box.edit').length) {
                    $('.step-box').removeClass("edit");
                }
                $(this).closest('.step-box').addClass("edit");
            }
            $("html, body").animate({
                scrollTop: $(this).closest('.step-box').offset().top - headerHeight - 10
            }, 500);
        });
    };
    ValidateSignNew.prototype.redirectToHome = function () {
        var submit = $(".step-box:nth-child(3) button[type='submit']");
        submit.click(redirect);
        function redirect() {
            window.location.replace("/");
        }
    };
    ValidateSignNew.prototype.redirectStep = function (step) {
        // var step = JSON.parse(localStorage.getItem('stepSignNew')),
        var step = step, headerHeight = $('#header').height();
        if (!$('.step-box > .slide').eq(step - 1).hasClass('hide-form')) {
            return;
        }
        else {
            $('.step-box > .slide').addClass("hide-form");
            $('.step-box > .slide').eq(step - 1).removeClass("hide-form");
            $("html, body").animate({
                scrollTop: $('.step-box').eq(step - 1).offset().top - headerHeight - 40
            }, 500);
        }
    };
    ValidateSignNew.prototype.checkLocalStorage = function (val) {
        if (localStorage.getItem(val) !== null) {
            return true;
        }
        else {
            return false;
        }
    };
    ValidateSignNew.prototype.checkSpepsSelect = function (steps) {
        var accordion = $('.step-box');
        if ($('.step-box.edit').length) {
            $('.step-box').removeClass("edit");
        }
        for (var i = 0; i < steps; i++) {
            accordion.filter(':nth-child(' + i + ')').addClass("step-passed");
        }
    };
    return ValidateSignNew;
}(View));
var fixedPlanBlockSignNew = (function (_super) {
    __extends(fixedPlanBlockSignNew, _super);
    function fixedPlanBlockSignNew() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    fixedPlanBlockSignNew.prototype.init = function () {
        this.fixedPlanBlock();
    };
    fixedPlanBlockSignNew.prototype.fixedPlanBlock = function () {
        var planInfo = $('.plan-info');
        var heightHeader = $('#header').height();
        var scrollTopPlanInfo;
        if (planInfo.length) {
            scrollTopPlanInfo = planInfo.offset().top - heightHeader;
            $(window).scroll(function () {
                var srollTopLastBlock = $('.step-box:nth-child(3)').offset().top;
                var heightLastBlock = $('.step-box:nth-child(3)').height();
                var blockSideOne = planInfo.height() + planInfo.offset().top;
                var blockSideTwo = srollTopLastBlock + heightLastBlock;
                var bottomScroll = planInfo.height() + $(window).scrollTop() + 93;
                if (blockSideOne >= blockSideTwo) {
                    planInfo.addClass("absolute-plan-info");
                }
                if (bottomScroll < blockSideOne) {
                    planInfo.removeClass("absolute-plan-info");
                }
                if ($(window).scrollTop() > scrollTopPlanInfo) {
                    planInfo.addClass("fixed-plan-info");
                }
                else {
                    planInfo.removeClass("fixed-plan-info");
                }
            });
        }
    };
    return fixedPlanBlockSignNew;
}(View));
var homeWorkouts = (function (_super) {
    __extends(homeWorkouts, _super);
    function homeWorkouts() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    homeWorkouts.prototype.init = function () {
        this.showAll();
        this.typeFiltering();
    };
    homeWorkouts.prototype.showAll = function () {
        var that = this;
        var showAllBtn = $('.see-more-regimens a');
        showAllBtn.on('click', function (e) {
            var duration = $(this).closest('.workouts-section').attr('data-duration'), el = $(this).closest('.workouts-section').find($('.video-thumbs')), elementsData, markup, test = [], self = this, url, options = $(this).closest('.workouts-section').find($('select'));
            if (el.attr("show-all")) {
                el.removeAttr("show-all");
            }
            e.preventDefault();
            if ($(this).closest('.workouts-section').attr('data-duration')) {
                url = "/video/by-duration/" + ("" + duration) + "/all";
            }
            else {
                url = "/video/live-classes/all";
            }
            $.get(url, function (data) {
                el.fadeOut(1000, function () {
                    el.empty();
                    that.structureCreator(data, el, self);
                    that.loadingAnimate(el, self);
                    (new VideoThumb()).init();
                });
            });
        });
    };
    homeWorkouts.prototype.structureCreator = function (data, el, context) {
        var that = this, titleArray = ['strength', 'cardio', 'restorative'], videoFrameImg = $('.inner.video .slick-track > .slick-slide > a > img'), numberFilters = {
            1: 'one',
            2: 'two',
            3: 'three'
        };
        if (el.attr("show-all")) {
            return;
        }
        else {
            el.empty();
            var _loop_1 = function (i) {
                var test = 0;
                var videoItemData = data[i];
                var preparedVideoItemData = {
                    id: videoItemData.id,
                    duration: Math.floor(videoItemData.length / 1000 / 60) + ' min',
                    imgUrl: videoItemData.meta.thumbnailURL,
                    title: videoItemData.title
                };
                var videoItemTemplate = $('#slider-video-item-template').html();
                var videoItemProcessedTemplate = Object.keys(preparedVideoItemData).reduce(function (template, dataKey) {
                    return template.replace('{{' + dataKey + '}}', preparedVideoItemData[dataKey]);
                }, videoItemTemplate);
                var template = $(videoItemProcessedTemplate);
                var iconTemplate = $('<div class="icon white"></div>');
                var counter = -1;
                titleArray.forEach(function (item) {
                    var filters = videoItemData[item];
                    if (!filters) {
                        return;
                    }
                    if (filters !== '') {
                        counter++;
                        var templateIndicator = iconTemplate.clone();
                        templateIndicator.addClass(item + " " + numberFilters[videoItemData[item]]);
                        template.find('.title-icon').append(templateIndicator);
                    }
                });
                if (videoItemData.products.length) {
                    videoFrameImg.attr('src', "" + videoItemData.products[0].thumbUrl);
                }
                el.append(template);
                el.attr("show-all", "true");
                (new VideoThumb()).init();
            };
            for (var i = 0; i < data.length; i++) {
                _loop_1(i);
            }
        }
        that.videoPopup(el);
    };
    homeWorkouts.prototype.videoPopup = function (el) {
        var dataVideoItem = $('[data-modal]'), videoStatus = new VideoStatus(), modalPopup = new Modals(), videoModal = new VideoModal();
        videoStatus.init(el);
        modalPopup.init(dataVideoItem);
        videoModal.init();
    };
    homeWorkouts.prototype.loadingAnimate = function (el, context) {
        var that = this, templateLoading = $('#loading-video').html(), loading = el.closest('.workouts-section').find('.loading-splash');
        if (!loading.length) {
            $(context).closest('.workouts-section').append($(templateLoading)).fadeIn(1000);
        }
        setTimeout(function () {
            loading = el.closest('.workouts-section').find('.loading-splash');
            loading.remove();
            var videoItems = $(context).closest('.workouts-section').find($('.video-thumbs > li'));
            el.fadeIn();
            videoItems.each(function (index) {
                var that = this;
                setTimeout(function () {
                    $(that).addClass('animate-item');
                }, 200 * index);
            });
            new SizingHelper.init($('[data-match-tallest]'));
        }, 2000);
    };
    homeWorkouts.prototype.typeFiltering = function () {
        var that = this;
        $('.select-holder').change(function (e) {
            var filters = $(this).closest('.workouts-section').find('select').serialize(), duration = $(this).closest('.workouts-section').attr('data-duration'), el = $(this).closest('.workouts-section').find($('.video-thumbs')), allVideos = $(this).closest('.workouts-section').find($('.video-thumbs > li:not(.stub-video)')), self = this, emptyVideosTemplate = $('#video-item-empty').html(), url;
            if ($(this).closest('.workouts-section').attr('data-duration')) {
                url = "/video/by-duration/" + duration + "/filter";
            }
            else {
                url = "/video/live-classes/filter";
            }
            $.ajax({
                url: url,
                type: "POST",
                data: filters,
                success: function (data, textStatus, jqXHR) {
                    var stub = $(self).closest('.workouts-section').find('.stub-video');
                    el.fadeOut(500, function () {
                        that.structureCreator(data, el, self);
                        that.loadingAnimate(el, self);
                    });
                    if (el.attr("show-all")) {
                        el.removeAttr("show-all");
                    }
                    if (data.length) {
                        if (stub.length) {
                            stub.remove();
                        }
                    }
                    else {
                        if (stub.length) {
                            stub.remove();
                        }
                        setTimeout(function () {
                            $(self).closest('.workouts-section').append($(emptyVideosTemplate));
                        }, 2500);
                        setTimeout(function () {
                            el.empty();
                            that.loadingAnimate(el, self);
                        }, 500);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (window.console !== undefined) {
                        console.error(textStatus);
                    }
                }
            });
        });
    };
    return homeWorkouts;
}(View));
var timerPlanSignNew = (function (_super) {
    __extends(timerPlanSignNew, _super);
    function timerPlanSignNew() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    timerPlanSignNew.prototype.init = function () {
        this.timerPlan();
    };
    timerPlanSignNew.prototype.timerPlan = function () {
        // let timestamp = 1495873362;
        function getTimestamp(resolve) {
            var dataTimestamp = $('.plan-info').attr('data-countdown-to'), timestamp;
            if (dataTimestamp) {
                return resolve(dataTimestamp);
            }
            $.get('/countdown/start', function (data) {
                if (data.timestamp) {
                    timestamp = data.timestamp;
                }
                resolve(timestamp);
            });
        }
        function millisAsDays(millis) {
            var seconds = Math.floor(millis / 1000), days = Math.floor(seconds / 24 / 60 / 60), hoursLeft = Math.floor(seconds - days * 86400), hours = Math.floor(hoursLeft / 3600), minutes = Math.floor((hoursLeft - hours * 3600) / 60), secondsLeft = seconds % 60;
            return { 'days': days, 'hours': hours, 'minutes': minutes, 'seconds': secondsLeft };
        }
        function resetTimeStamp(resolve) {
            var timestamp;
            $.get('/countdown/reset', function (data) {
                if (data.timestamp) {
                    // timestamp = data.timestamp;
                    timestamp = data.timestamp;
                }
                resolve(timestamp);
            });
        }
        function createStructureTimer(timerData) {
            $('.days').text(timerData.days);
            $('.hours').text(timerData.hours);
            $('.minutes').text(timerData.minutes < 10 ? '0' + timerData.minutes : timerData.minutes);
            $('.seconds').text(timerData.seconds < 10 ? '0' + timerData.seconds : timerData.seconds);
        }
        var interval;
        var test = 0;
        function initCountDown(timestamp) {
            var b = moment.unix(timestamp);
            return window.setInterval(function () {
                var a = moment.tz('America/New_York');
                var diff = b.diff(a);
                if (diff > 0) {
                    var timerData = millisAsDays(diff);
                    createStructureTimer(timerData);
                }
                else {
                    if ($('body').hasClass("sign-up")) {
                        console.log('its body');
                        resetTimeStamp(function (timestamp) {
                            interval = initCountDown(timestamp);
                            window.clearInterval(interval);
                        });
                    }
                }
            }, 1000);
        }
        getTimestamp(function (timestamp) {
            interval = initCountDown(timestamp);
        });
    };
    return timerPlanSignNew;
}(View));
/// <reference path="typings/imports.d.ts" />
var Global = (function (_super) {
    __extends(Global, _super);
    function Global() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Global.prototype.init = function () {
        Helpers.init();
        this.app.createController(DeeplinkController);
        this.app.createModel(VideoPaginationModel);
        this.app.createModel(MashupMakerModel);
        this.app.createModel(ModalsModel);
        this.app.createView(Header);
        this.app.createView(HomeLoggedIn);
        this.app.createView(EmailPreferences);
        this.app.createView(Faq);
        this.app.createView(ManageProfile);
        this.app.createView(ChangePlan);
        this.app.createView(Alerts);
        this.app.createView(Datepicker);
        // this.app.createView(DataLinks);
        this.app.createView(VideoStatus);
        this.app.createView(VideoFilter);
        this.app.createView(Modals);
        this.app.createView(SignInModal);
        this.app.createView(TutorialModal);
        this.app.createView(DeleteMashupConfirmationModal);
        this.app.createView(VideoModal);
        this.app.createView(MashupModal);
        this.app.createView(MyWorkouts);
        this.app.createView(VideoThumb);
        this.app.createView(VideoPagination);
        this.app.createView(MashupNameModal);
        this.app.createView(MashupMaker);
        this.app.createView(NavMenu);
        this.app.createView(Account);
        this.app.createView(Payment);
        this.app.createView(PersonalInfoModal);
        this.app.createView(ValidateSignNew);
        this.app.createView(fixedPlanBlockSignNew);
        this.app.createView(homeWorkouts);
        this.app.createView(timerPlanSignNew);
    };
    Global.redirectToLoginIfSessionTimesout = function (data) {
        if (data.error) {
            window.location.href = '/?login=1';
        }
    };
    return Global;
}(BaseClass));
new Global();
//# sourceMappingURL=global.js.map