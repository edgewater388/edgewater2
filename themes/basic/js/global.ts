/// <reference path="typings/imports.d.ts" />

class Global extends BaseClass {

    public init():void {
        Helpers.init();
        this.app.createController(DeeplinkController);
        this.app.createModel(VideoPaginationModel);
        this.app.createModel(MashupMakerModel);
        this.app.createModel(ModalsModel);
        this.app.createView(Header);
        this.app.createView(HomeLoggedIn);
        this.app.createView(EmailPreferences);
        this.app.createView(Faq);
        this.app.createView(ManageProfile);
        this.app.createView(ChangePlan);
        this.app.createView(Alerts);
        this.app.createView(Datepicker);
        // this.app.createView(DataLinks);
        this.app.createView(VideoStatus);
        this.app.createView(VideoFilter);
        this.app.createView(Modals);
        this.app.createView(SignInModal);
        this.app.createView(TutorialModal);
        this.app.createView(DeleteMashupConfirmationModal);
        this.app.createView(VideoModal);
        this.app.createView(MashupModal);
        this.app.createView(MyWorkouts);
        this.app.createView(VideoThumb);
        this.app.createView(VideoPagination);
        this.app.createView(MashupNameModal);
        this.app.createView(MashupMaker);
        this.app.createView(NavMenu);
        this.app.createView(Account);
        this.app.createView(Payment);
        this.app.createView(PersonalInfoModal);
        this.app.createView(ValidateSignNew);
        this.app.createView(fixedPlanBlockSignNew);
        this.app.createView(homeWorkouts);
        this.app.createView(timerPlanSignNew);
    }

    public static redirectToLoginIfSessionTimesout(data:any):void {
        if (data.error) {
            window.location.href = '/?login=1';
        }
    }

}

new Global();
