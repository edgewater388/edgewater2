class Alerts extends View {

    private $alerts:JQuery;

    public init():void {
        this.setSelectors();
        this.initEvents();

        this.$alerts.each((i, el)=> {
            var $item:JQuery = $(el);
            if($item.find('ul').length) {
                $item.find('.inner').delay(700).slideDown(500).delay(5000).slideUp(500);
            }
        });

    }

    private setSelectors():void {
        this.$alerts = $('.alerts');
    }

    private initEvents():void {

    }


}
