class DeeplinkController extends Controller {


    public init():void {
        this.appendVideoMarkupIfFound();
        this.appendMashupMarkupIfFound();
        Tick.callback(()=>this.launchDeeplinkIfFound());
    }

    private appendVideoMarkupIfFound():void {
        var param = URLHelper.getParameterByName('video');
        if (param) {
            var html:string = "<div id='deeplink' data-modal='#video' data-video='" + param + "' style='display: none;'></div>";
            $('body').append(html);
        }
    }

    private appendMashupMarkupIfFound():void {
        var param = URLHelper.getParameterByName('mashup');
        if (param) {
            var html:string = "<div id='deeplink' data-modal='#mashup' data-playlist='" + param + "' style='display: none;'></div>";
            $('body').append(html);
        }
    }

    private launchDeeplinkIfFound():void {
        var videoParam = URLHelper.getParameterByName('video');
        var mashupParam = URLHelper.getParameterByName('mashup');
        if (videoParam || mashupParam) {
            $('#deeplink').trigger('click');
        }
    }

}
