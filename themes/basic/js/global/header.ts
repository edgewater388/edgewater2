class Header extends View {

    private $el:JQuery = $('#header');

    public init():void {
        if(!$('body').hasClass('short-nav-perm')) {
            this.setSelectors();
            this.initEvents();
        }

        this.initScrollEvent();
    }

    private initScrollEvent():void {
        const onScroll = () => {
            if (this.$window.scrollTop() > 0) {
                this.$body.addClass('scrolled-from-top');
            } else {
                this.$body.removeClass('scrolled-from-top');
            }
        };

        this.$window.on('scroll', $.throttle(250, ()=>onScroll()));
        this.$window.on('scroll', $.debounce(100, ()=>onScroll()));
    }

    private setSelectors():void {

    }

    private initEvents():void {
        this.$window.on('scroll', $.throttle(250, ()=>this.onScroll()));
        this.$window.on('scroll', $.debounce(100, ()=>this.onScroll()));
    }

    private onScroll():any {
        if (this.$window.scrollTop() > 0) {
            this.$body.addClass('short-nav');
        } else {
            this.$body.removeClass('short-nav');
        }
    }

}