class Modals extends View {

    public init(holder = $('[data-modal]')):void {
        this.initModalButtons(holder);
        this.initAutoOpenModals();
        this.preVideoModal();

        this.app.getModel(VideoPaginationModel).pageChanged.add(()=>this.initModalButtons());
        this.app.getModel(MashupMakerModel).liAdded.add(()=>this.initModalButtons());
    }

    public initModalButtons(holder = $('[data-modal]')):void {
        holder.each((i, el)=> {
            // debugger
            new Modal($(el));
        });
    }

    private initAutoOpenModals():void {
        $('[data-auto-open-modal]').each((i, el)=> {
            const modalEl = $(el);
            const modal = new Modal($('<button>').data('modal', `#${modalEl.attr('id')}`));

            modal.fadeInModal();
        });
    }

    private preVideoModal ():void {
        $(document).on('click', '[data-modal="#video"], [data-modal="#mashup"]', function () {
            let $1stVideoModal = $('#first-video-congratulation');
            if ($1stVideoModal) {
                $1stVideoModal.fadeIn(400, ()=> {
                $1stVideoModal.addClass('scroll');
                $('body').addClass('no-scroll');
                $1stVideoModal.find('.close-modal').fadeIn();
                $1stVideoModal.find('.inner').fadeIn();
                TweenMax.set(this.$inner, {css: {y: -80}});
                TweenMax.to(this.$inner, 0.6, {css: {y: 0, force3D: false}, ease: Power3.easeOut});
            });
            }
        });
    }
}

class Modal extends View {

    private $modal:JQuery;
    private $closeButton:JQuery;
    private $inner:JQuery;

    constructor(private $launchBtn:JQuery) {
        super();
        this.setSelectors();
        this.initEvents();
    }

    private setSelectors():void {
        this.$modal = $(this.$launchBtn.data('modal'));
        this.$closeButton = this.$modal.find('.close-modal');
        this.$body = $('body');
        this.$inner = this.$modal.find('.inner');

        this.$modal.data('modal', this);
    }

    private initEvents():void {
        this.$closeButton.on('click', (e) => { this.onCloseButtonClick(e)});
        this.$launchBtn.on('click', (e)=>this.onLaunchButtonClick(e));
        this.$modal.on('click', (e)=>this.closeModalOnOutsideClick(e));
        $(document).on('keyup', (e)=>this.onKeyUp(e));
    }

    private onLaunchButtonClick(e:any):any {
        if (this.$modal.length > 0) {
            e.preventDefault();
            this.fadeInModal();
        }
    }

    private onCloseButtonClick(e:any):any {
        this.fadeOutModal();
    }

    public fadeInModal():void {
        this.$modal.fadeIn(400, ()=> {
            this.$modal.addClass('scroll');
            this.$body.addClass('no-scroll');
            this.$closeButton.fadeIn();
            this.$inner.fadeIn();
            TweenMax.set(this.$inner, {css: {y: -80}});
            TweenMax.to(this.$inner, 0.6, {css: {y: 0, force3D: false}, ease: Power3.easeOut});
        });
    }


    private fadeOutModal():void {
        this.$closeButton.fadeOut();
        this.$inner.fadeOut(400, ()=> {
            this.$modal.removeClass('scroll');
            this.$body.removeClass('no-scroll');
            this.$modal.fadeOut(400);
            this.chengeVideoStatus(this.$modal);
        });
        this.app.getModel(ModalsModel).modalClosed.dispatch();
    }

    private onKeyUp(e):void {
        switch (e.which) {
            case 27: // esc
                this.fadeOutModal();
                break;
        }
    }

    private closeModalOnOutsideClick(e:any):any {
        if ($(e.target).hasClass('modal')) {
            this.fadeOutModal();
        }
    }

    private chengeVideoStatus (modal:JQuery):void {
        if (modal.hasClass('first-video-popup')) {
            $.ajax({
                url: '/user/users/videoSee'
            }).done(function() {
                $('#video').find('.lockout > .overlay').remove();
            });
        }
    }
}
