interface String {
    startsWith(str:string, pos:any):any;
}

class NavMenu extends View {

	private $el:JQuery = $('#main-menu');
	private $hamburger:JQuery;
	private $burgerWrap:JQuery;
	private $menuButton:JQuery;

	public init():void {
		this.setSelectors();
		this.initEvents();
		if (!String.prototype.startsWith) {
		  	String.prototype.startsWith = function(searchString, position) {
		    	position = position || 0;
		    	return this.indexOf (searchString, position) === position;
		  	};
		}
	}

	private setSelectors():void {
		this.$hamburger = $(".hamburger");
		this.$burgerWrap = this.$hamburger.closest(".wrap");
		this.$menuButton = this.$burgerWrap.find("h5");

	}

	private initEvents(): void {
		//this.$el.load("p57.stage.realpie.com #nav-wrap");
		$(window).unload(() => this.closeMenu());
		$(window).on('beforeunload ', () => this.closeMenu());

		this.$burgerWrap.click(() => this.openMenu())
		$("#menu-pull-out-nav .menu-item-has-children").mouseenter((e)=>{
			e.stopPropagation();
			if($(window).width()>700){
				this.menuHoverHandler($(e.currentTarget));
			}
		})
		$("#menu-pull-out-nav  .menu-item-has-children").click((e)=>{
			e.stopPropagation();
			if($(window).width()<=700){
				if (!$(e.currentTarget).hasClass ("mobile-active")){
					e.preventDefault();
				}
				this.menuClickHandler($(e.currentTarget));
			}
		})
	}

	private openMenu():void {
		this.$el.show();
		this.$burgerWrap.off('click');
		setTimeout(()=>{
			$(".hamburger, #main-menu, .bg-column-1, .bg-column-2, #img-wrap, .column-1, .column-1 li a, #head-wrap, .jumbotron").addClass("open");
			$(".hamburger, .site-map").addClass("col-1-active");
			this.$menuButton.html("CLOSE").addClass("col-1-active");
			$(".bg-column-1, .bg-column-2, #img-wrap, #nav-wrap ul, #nav-wrap ul li a").removeClass("closed");
			this.$burgerWrap.click(() => this.closeMenu());
		},10)
		setTimeout(()=>{
			$("body").addClass("no-scroll");
		},500);
	}

	private closeMenu():void {
		$(".hamburger, #main-menu, .background, #img-wrap, .column-1 li a, .column-1, .sub-menu, .sub-menu li a, #head-wrap, .jumbotron").removeClass("open");
		$("body").removeClass("no-scroll");
		$(".hamburger").removeClass("col-1-active col-3-active");
		this.$menuButton.html("MENU").removeClass("col-1-active col-3-active");
		$(".background, .column-1, #img-wrap, .column-1 li a, .sub-menu, .sub-menu li a").addClass("closed");
		$(".active").removeClass("active");
		$(".mobile-active").removeClass("mobile-active");
		$("#img-wrap a").removeClass("grey");
		this.$burgerWrap.off('click');
		setTimeout(()=>{
			this.$burgerWrap.click(( )=>{
				this.openMenu();
			})
		},100);
		setTimeout(()=>{
			$("#main-menu").hide();
		},500);
	}

	private menuHoverHandler(hovered):any{
		console.log();
		var classes = hovered.attr("class").split(" ");
		var depthClass = classes.filter((foobar)=>{
			return foobar.startsWith("menu-item-depth");
		})
		var currentColumn = depthClass[0].slice(-1);
		var subID = hovered.attr("id");
		
		if(currentColumn==0){
			$(".bg-column-3, .menu-depth-1, menu-depth-2, .menu-depth-1 li, .menu-depth-2 li, .menu-depth-1 li a, .menu-depth-2 li a").removeClass("open").addClass("closed");
			$(".menu-depth-1 .active, .column-1 .active, .column-1 li a.active").removeClass("active");
			$("#img-wrap a").removeClass("grey");
			this.$hamburger.removeClass("col-3-active").addClass("col-1-active");
			hovered.find('a').first().addClass("active");
			hovered.addClass("active");
			hovered.find('.sub-menu').first().removeClass("closed").addClass("open");
			hovered.find('.sub-menu').first().removeClass("closed").children('li').addClass("open");
			hovered.find('.sub-menu').first().children('li').children('a').removeClass("closed").addClass("open");
		}
		else if(currentColumn==1){
			if(hovered.hasClass("open")){
				$(".menu-depth-1 .active, .menu-depth-1 li a.active").removeClass("active");
				hovered.find('a').first().addClass("active");
				hovered.addClass("active");
				$(".menu-depth-2, .menu-depth-2 li, .menu-depth-2 li a").removeClass("open").addClass("closed");
				$(".bg-column-3").removeClass("closed").addClass("open");
				hovered.find('.sub-menu').first().removeClass("closed").addClass("open");
				//hovered.find('.sub-menu').first().removeClass("closed").children('li').addClass("open");
				hovered.find('.sub-menu').first().children('li').children('a').removeClass("closed").addClass("open");
				$("#img-wrap a").addClass("grey");
				this.$hamburger.removeClass("col-1-active").addClass("col-3-active");
				this.$menuButton.removeClass("col-1-active").addClass("col-3-active");
			}
		}
	}

	private menuClickHandler(clicked) {
		var classes = clicked.attr("class").split(" ");
		var depthClass = classes.filter((foobar)=>{
			return foobar.startsWith("menu-item-depth");
		})
		var currentColumn = depthClass[0].slice(-1);
		var subID = clicked.attr("id");
		if(!clicked.hasClass("mobile-active")){
			if (currentColumn == 0) {
				$(".menu-depth-1, .menu-depth-2, .menu-depth-1 li, .menu-depth-2 li, .menu-depth-1 li a, .menu-depth-2 li a").removeClass("open");
				$(".column-1 .mobile-active").removeClass("mobile-active");
				clicked.addClass("mobile-active");
				clicked.find('.sub-menu').first().addClass("open");
				clicked.find('.sub-menu').first().children('li').addClass("open");
				clicked.find('.sub-menu').first().children('li').children('a').addClass("open");

			}
			else if(currentColumn==1){
				if(clicked.hasClass("open")){
					$(".menu-depth-1 .mobile-active").removeClass("mobile-active");
					$(".menu-depth-2, .menu-depth-2 li, .menu-depth-2 li a").removeClass("open");
					setTimeout(()=>{
						clicked.addClass("mobile-active");
						clicked.find('.sub-menu').first().addClass("open");
						clicked.find('.sub-menu').first().children('li').addClass("open");
						clicked.find('.sub-menu').first().children('li').children('a').addClass("open");
					},100)
					
					$(".hamburger").removeClass("col-1-active").addClass("col-3-active");
				}
			}
		}
		else{
			clicked.removeClass("mobile-active");
			clicked.find('.sub-menu').first().removeClass("open");
			clicked.find('.sub-menu').first().children('li').removeClass("open");
			clicked.find('.sub-menu').first().children('li').children('a').removeClass("open");
		}
	}

	
}