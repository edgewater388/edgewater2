var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var VideoFilter = (function (_super) {
    __extends(VideoFilter, _super);
    function VideoFilter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    VideoFilter.prototype.init = function () {
        this.initSelectors();
        this.initEvents();
    };
    VideoFilter.prototype.initSelectors = function () {
        this.$week_indicator = $(".info-panel .week-indicator");
        this.$current_week = $(".current-week").first().closest(".video-thumbs");
        this.$current_btn_holder = this.$current_week.find(".additional-block");
        this.$prev_weeks = this.$current_week.prevAll();
        this.$skipped_workouts = $('.skipped-workouts');
        this.$all_weeks = $(".workout-content .video-thumbs").not('.skipped-workouts');
        this.$holder = $(".video-pagination");
        this.$next_btn = $(".next-week-btn");
        this.$view_all = $(".info-panel .btn-view-all");
        this.$refresh = $(".btn-refresh");
        this.$skipped = $(".is-skipped-days");
        this.counter = 0;
        this.isSkipped = false;
    };
    VideoFilter.prototype.initEvents = function () {
        var _this = this;
        this.all_length = this.$all_weeks.length - 1;
        this.addCounter();
        this.$holder.imagesLoaded().done(function () {
            setTimeout(function () {
                _this.$current_week.addClass('current');
                _this.current_week_index = _this.$all_weeks.index(_this.$current_week);
                _this.refreshFilter();
            }, 200);
        });
        $(document).on("click", ".next-week-btn", function (e) {
            $(e.currentTarget).addClass('hidden');
            _this.increaseClickCounter();
        });
        this.$view_all.on("click", function (e) {
            _this.viewAll();
        });
        this.$refresh.on("click", function (e) {
            _this.refreshFilter();
        });
        this.$window.on("resize", function () {
            clearTimeout(_this.timeout);
            _this.timeout = setTimeout(function () {
                if (_this.isSkipped) {
                    _this.toggleSkippedWorkouts();
                }
                else {
                    _this.active_week = _this.$all_weeks.eq(_this.counter);
                    _this.active_height = _this.active_week.outerHeight();
                    _this.setHolderHeight(_this.active_height + _this.active_week.position().top);
                }
            }, 500);
        });
        this.$skipped.on("click", function (e) {
            e.preventDefault();
            _this.isSkipped = !_this.isSkipped;
            _this.toggleSkippedWorkouts();
        });
    };
    VideoFilter.prototype.showSkippedWorkouts = function () {
        this.$all_weeks.addClass("hidden");
        this.$skipped_workouts.removeClass("hidden");
        this.$window.trigger('resize');
        var skipped_height = this.$skipped_workouts.outerHeight();
        this.$holder.removeClass('refresh-filter');
        this.$holder.removeClass('all-filter');
        this.$holder.addClass('skipped');
        this.setHolderHeight(skipped_height);
    };
    VideoFilter.prototype.hideSkippedWorkouts = function () {
        this.isSkipped = false;
        this.$all_weeks.removeClass("hidden");
        this.$skipped_workouts.addClass("hidden");
    };
    VideoFilter.prototype.toggleSkippedWorkouts = function () {
        if (this.isSkipped) {
            this.showSkippedWorkouts();
        }
        else {
            this.hideSkippedWorkouts();
            this.refreshFilter();
        }
    };
    VideoFilter.prototype.addCounter = function () {
        var _this = this;
        this.$all_weeks.each(function (index, el) {
            var $current = $(el);
            if (!$current.hasClass('skipped-workouts')) {
                $current.prepend("<span class='week-counter'><span>" + _this.getGetOrdinal(index + 1) + "</span><span>WEEK</span></span>");
            }
        });
    };
    VideoFilter.prototype.viewAll = function () {
        this.$prev_weeks.removeClass('hidden');
        this.$holder.removeClass('refresh-filter');
        this.$holder.addClass('all-filter');
        this.$holder.removeClass('skipped');
        this.updateWeekIndicator(this.$all_weeks.first());
        this.hideSkippedWorkouts();
        this.counter = this.all_length;
        var last_week = this.$all_weeks.eq(this.counter), last_btn_holder = last_week.find(".additional-block"), last_height = last_week.outerHeight(), last_pos = last_week.position().top;
        this.$window.trigger('resize');
        this.setHolderHeight(last_pos + last_height);
    };
    VideoFilter.prototype.refreshFilter = function () {
        this.counter = this.current_week_index;
        this.$next_btn.removeClass('hidden');
        this.$holder.addClass('refresh-filter');
        this.$holder.removeClass('all-filter');
        this.$holder.removeClass('skipped');
        if (this.counter < this.$all_weeks.length - 1) {
            this.showNextWeekBtn(this.$current_week.find(".additional-block"));
        }
        this.hideSkippedWorkouts();
        this.updateWeekIndicator(this.$current_week);
        this.$window.trigger('resize');
        this.$prev_weeks.addClass('hidden');
        var refresh_height = this.$current_week.outerHeight();
        this.setHolderHeight(refresh_height);
    };
    VideoFilter.prototype.updateWeekIndicator = function ($week) {
        this.current_week_indicator = $week.find('.week-counter').html();
        this.$week_indicator.html(this.current_week_indicator);
    };
    VideoFilter.prototype.setHolderHeight = function (height) {
        this.$holder.height(height);
    };
    VideoFilter.prototype.increaseClickCounter = function () {
        this.counter++;
        var $next_week = this.$all_weeks.eq(this.counter);
        var $next_btn_holder = $next_week.find(".additional-block");
        var next_height = $next_week.outerHeight();
        var current_heigth = this.$holder.outerHeight();
        this.setHolderHeight(next_height + current_heigth);
        this.$next_btn.addClass('hidden');
        if (this.counter < this.$all_weeks.length - 1) {
            this.showNextWeekBtn($next_btn_holder);
        }
    };
    VideoFilter.prototype.showNextWeekBtn = function ($btn_holder) {
        var $next_week_btn = $btn_holder.find('.next-week-btn');
        if ($next_week_btn.length > 0) {
            $next_week_btn.removeClass('hidden');
        }
        else {
            var $next_btn = this.$next_btn.clone();
            $btn_holder.prepend($next_btn.removeClass('hidden'));
        }
    };
    VideoFilter.prototype.getGetOrdinal = function (n) {
        var s = ["th", "st", "nd", "rd"], v = n % 100;
        return n + (s[(v - 20) % 10] || s[v] || s[0]);
    };
    return VideoFilter;
}(View));
