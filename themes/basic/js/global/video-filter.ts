class VideoFilter extends View {
  // week items
  private $current_week:any;
  private $current_btn_holder:JQuery;
  private $prev_weeks:JQuery;
  private $all_weeks:JQuery;
  private $skipped_workouts:JQuery;

  // holder
  private $holder:JQuery;

  // btns
  private $next_btn:JQuery;
  private $view_all:JQuery;
  private $refresh:JQuery;
  private $skipped:JQuery;

  // counter
  private counter:number;
  private all_length:number;
  private $week_indicator:number;
  private current_week_index:number;
  private current_week_indicator:number;

  // timeout
  private timeout:any;

  // resize helpers items
  private active_week:any;
  private active_height:number;

  private isSkipped:any;

  public init():void {
    this.initSelectors();
    this.initEvents();
  }

  private initSelectors():void {
    this.$week_indicator = $(".info-panel .week-indicator");
    // week items
    this.$current_week = $(".current-week").first().closest(".video-thumbs");

    this.$current_btn_holder = this.$current_week.find(".additional-block");
    this.$prev_weeks = this.$current_week.prevAll();
    this.$skipped_workouts = $('.skipped-workouts');
    this.$all_weeks = $(".workout-content .video-thumbs").not('.skipped-workouts');

    // holder
    this.$holder = $(".video-pagination");

    // btns
    this.$next_btn = $(".next-week-btn");
    this.$view_all = $(".info-panel .btn-view-all");
    this.$refresh = $(".btn-refresh");
    this.$skipped = $(".is-skipped-days");

    // counter
    this.counter = 0;

    this.isSkipped = false;
  }

  private initEvents():void {
    // remove all weeks before current
    this.all_length = this.$all_weeks.length - 1;
    this.addCounter();

    // set holder height
    this.$holder.imagesLoaded().done(() => {
      setTimeout(() => {
        this.$current_week.addClass('current');
        this.current_week_index = this.$all_weeks.index(this.$current_week);
        this.refreshFilter();
      }, 200);
    });

    // add event to next button
    $(document).on("click", ".next-week-btn",(e) => {
      $(e.currentTarget).addClass('hidden');
      this.increaseClickCounter();
    });

    // add action to view all btn
    this.$view_all.on("click", (e:any) => {
      this.viewAll();
    });

    // refresh filter button event
    this.$refresh.on("click", (e:any) => {
      this.refreshFilter();
    });

    // recalculate height on resize
    this.$window.on("resize", () => {
      clearTimeout(this.timeout);
      this.timeout = setTimeout( () => {
        if(this.isSkipped) {
          this.toggleSkippedWorkouts();
        }else{
          this.active_week = this.$all_weeks.eq(this.counter);
          this.active_height = this.active_week.outerHeight();
          this.setHolderHeight(this.active_height + this.active_week.position().top);
        }
      }, 500);
    });

    this.$skipped.on("click", (e) => {
      e.preventDefault();
      this.isSkipped = !this.isSkipped;
      this.toggleSkippedWorkouts();
    });
  }

  private showSkippedWorkouts():void {
    this.$all_weeks.addClass("hidden")
    this.$skipped_workouts.removeClass("hidden");
    this.$window.trigger('resize'); // todo: de-jankify. triggers match-tallest li helper
    const skipped_height:number = this.$skipped_workouts.outerHeight();
    this.$holder.removeClass('refresh-filter');
    this.$holder.removeClass('all-filter');
    this.$holder.addClass('skipped');
    this.setHolderHeight(skipped_height);
  }
  private hideSkippedWorkouts():void {
    this.isSkipped = false;
    this.$all_weeks.removeClass("hidden")
    this.$skipped_workouts.addClass("hidden");
    // this.$week_indicator.addClass("hidden");
  }

  private toggleSkippedWorkouts():void {
    if (this.isSkipped) {
      this.showSkippedWorkouts();
    } else {
      this.hideSkippedWorkouts();
      this.refreshFilter();
    }
  }

  private addCounter():void {
    this.$all_weeks.each((index:number, el:any) => {
      let $current:JQuery = $(el);
      if(!$current.hasClass('skipped-workouts')) {
        $current.prepend("<span class='week-counter'><span>" + this.getGetOrdinal(index+1) + "</span><span>WEEK</span></span>");
      }
    });
  }

  private viewAll():void {
    this.$prev_weeks.removeClass('hidden');
    this.$holder.removeClass('refresh-filter');
    this.$holder.addClass('all-filter');
    this.$holder.removeClass('skipped');

    this.updateWeekIndicator(this.$all_weeks.first());
    this.hideSkippedWorkouts();
    this.counter = this.all_length;


    let last_week:any = this.$all_weeks.eq(this.counter),
      last_btn_holder = last_week.find(".additional-block"),
      last_height:number = last_week.outerHeight(),
      last_pos:number = last_week.position().top;

    this.$window.trigger('resize'); // todo: de-jankify. triggers match-tallest li helper

    this.setHolderHeight(last_pos + last_height);
    // last_btn_holder.prepend(this.$next_btn);
    // this.$week_indicator.removeClass("hidden");
  }

  private refreshFilter():void {
    this.counter = this.current_week_index;
    this.$next_btn.removeClass('hidden');
    this.$holder.addClass('refresh-filter');
    this.$holder.removeClass('all-filter');
    this.$holder.removeClass('skipped');

    //add next button
    if(this.counter < this.$all_weeks.length - 1) {
      this.showNextWeekBtn(this.$current_week.find(".additional-block"));
    }


    this.hideSkippedWorkouts();
    this.updateWeekIndicator(this.$current_week);
    this.$window.trigger('resize'); // todo: de-jankify. triggers match-tallest li helper
    this.$prev_weeks.addClass('hidden');
    let refresh_height:number = this.$current_week.outerHeight();
    this.setHolderHeight(refresh_height);


  }


  private updateWeekIndicator($week):void{
    this.current_week_indicator = $week.find('.week-counter').html();
    this.$week_indicator.html(this.current_week_indicator);
  }

  private setHolderHeight(height:number):void {
    this.$holder.height(height);
  }

  private increaseClickCounter():void {
    this.counter++;
    let $next_week:any = this.$all_weeks.eq(this.counter);
    const $next_btn_holder:any = $next_week.find(".additional-block");
    const next_height:number = $next_week.outerHeight();
    const current_heigth:number = this.$holder.outerHeight();
    this.setHolderHeight(next_height + current_heigth);
    this.$next_btn.addClass('hidden');
    if(this.counter < this.$all_weeks.length - 1) {
      this.showNextWeekBtn($next_btn_holder);
    }
  }

  private showNextWeekBtn($btn_holder:any):void {
    const $next_week_btn = $btn_holder.find('.next-week-btn');
    if($next_week_btn.length > 0) {
      $next_week_btn.removeClass('hidden');
    }else{
      const $next_btn = this.$next_btn.clone();
      $btn_holder.prepend($next_btn.removeClass('hidden'));
    }
  }

  private getGetOrdinal(n:number):void {
   let s:any = ["th","st","nd","rd"],
     v:number = n % 100;

    return n+(s[(v-20)%10]||s[v]||s[0]);
  }
}
