class VideoPagination extends View {

    private $el:JQuery = $('.video-pagination');
    private page:number = 1;
    private $seeMoreBtn:JQuery;
    private $videoThumbsUl:JQuery;
    private $noResults:JQuery;
    private data:any;
    private loading:boolean = false;

    public init():void {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    }

    private setSelectors():void {
        this.$seeMoreBtn = $('#see-more-btn');
        this.$videoThumbsUl = this.$el.find('.video-thumbs');
        this.$noResults = this.$el.find('.no-results');
    }

    private initEvents():void {
        this.$seeMoreBtn.on('click', (e)=>this.onSeeMoreBtnClick(e));
        this.$el.find('#type-dd').on('change', (e)=>this.onDropdownChange(e));
        this.$el.find('#level-dd').on('change', (e)=>this.onDropdownChange(e));
        this.$el.find('#duration-dd').on('change', (e)=>this.onDropdownChange(e));
    }

    private onSeeMoreBtnClick(e:any):any {
        e.preventDefault();
        if (this.loading) return;
        this.loadNextPage();
    }

    private loadNextPage():void {
        this.$seeMoreBtn.fadeIn();
        this.$seeMoreBtn.parent().addClass('loading');
        this.page++;
        var query:string = '/video?type=' + this.$el.find('#type-dd').val()
            + '&level=' + this.$el.find('#level-dd').val()
            + '&duration=' + this.$el.find('#duration-dd').val()
            + '&page=' + this.page;
        //console.log(query);
        $.getJSON(query, (data)=> {
            Global.redirectToLoginIfSessionTimesout(data);
            this.data = data;
            this.preloadThumbImages();
            this.$noResults.css({display: data.length ? 'none': 'block'});
            if (!this.data.length) {
                this.$seeMoreBtn.stop().hide();
            }
        }).fail(function () {
            this.loading = false;
            this.$seeMoreBtn.parent().removeClass('loading');
            console.error('Could not load next page!');
        });
    }

    private onDropdownChange(e:any):any {
        this.page = 0;
        this.$videoThumbsUl.empty();
        this.loadNextPage();
    }

    private preloadThumbImages():void {
        var loader = new PxLoader();
        for (var i = 0; i < this.data.length; i++) {
            loader.addImage(this.data[i].meta.videoStillURL);
        }
        loader.addCompletionListener(()=> {
            this.$seeMoreBtn.parent().removeClass('loading');
            this.hideSeeMoreButtonOnLastPage();
            this.populateThumbs();
            this.loading = false;
            this.app.getModel(VideoPaginationModel).pageChanged.dispatch();
            this.$window.trigger('resize'); // todo: de-jankify. triggers match-tallest li helper
        });
        loader.start();
    }

    private populateThumbs():void {
        for (var i = 0; i < this.data.length; i++) {
            var obj = this.data[i];
            var videoLiHtml:string = $('#video-li-tpl').html();
            var $videoLi:JQuery = $(videoLiHtml);
            $videoLi.find('.thumb').attr('data-video', obj.id);
            $videoLi.find('.time').prepend(Math.round(obj.length / 60 / 1000).toString());
            $videoLi.find('.thumb-img').attr('src', obj.meta.videoStillURL);
            $videoLi.find('.title').html(obj.title);

            var types:string[] = ['strength', 'cardio', 'restorative'];
            for (var j = 0; j < types.length; j++) {
                var className;
                if (obj[types[j]] == '') className = 'none';
                if (obj[types[j]] == 1) className = 'one';
                if (obj[types[j]] == 2) className = 'two';
                if (obj[types[j]] == 3) className = 'three';
                $videoLi.find('.' + types[j]).addClass(className);
            }

            $videoLi.stop(true).css({opacity: 0, y: 70}).delay(i * 100).transition({opacity: 1, y: 0}, 400);
            //$videoLi.css({opacity:0}).delay(i * 100).animate({opacity:1}, 350);
            this.$videoThumbsUl.append($videoLi);
        }
    }

    private hideSeeMoreButtonOnLastPage():void {
        var isLastPage:boolean = this.data.length < 9;
        if (isLastPage) {
            this.$seeMoreBtn.stop().fadeOut();
        }
    }

}
