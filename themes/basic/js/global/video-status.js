var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var VideoStatus = (function (_super) {
    __extends(VideoStatus, _super);
    function VideoStatus() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    VideoStatus.prototype.init = function (holder) {
        if (holder === void 0) { holder = $(".video-thumbs"); }
        this.initSelectors(holder);
        this.initEvents();
    };
    VideoStatus.prototype.initSelectors = function (holder) {
        this.programm_id = $("[data-program_id]").data("program_id");
        this.requestLink = window.location.origin + "/program/" + this.programm_id + "/user-video-progress";
        this.$all_list = holder;
        this.$list = holder.not('.skipped-workouts');
        this.$all_list_item = this.$all_list.children().not(".additional-block, .rest-day");
        this.$list_item = this.$list.children().not(".additional-block, .rest-day");
        this.videosArr = [];
        this.$calc_value = $(".calculator-item:first").find(".calculator-value");
        this.$completed_value = $(".completed-count");
        this.$left_value = $(".workouts-counter .left-count");
        this.$skipped_days = $(".calculator-item .left-count");
        this.$modal_btn = $("[data-modal='#finish-program']");
        this.$change_modal_btn = $("[data-modal='#change-weight']");
        this.$close_modal = $(".close-modal");
        this.isFinished = false;
    };
    VideoStatus.prototype.initEvents = function () {
        var _this = this;
        var self = this;
        this.$all_list_item.on("click", function (e) {
            self.$current_item = $(this);
            self.current_item_was_skipped = self.$current_item.hasClass('is-skipped-workout');
            self.finish_program_btn = self.$current_item.is(self.$list_item.last());
            var $popup = self.$current_item.find("[data-modal]");
            console.log('$current_item', self.$current_item);
            console.log('current_item_was_skipped', self.current_item_was_skipped);
            if (!self.$current_item.hasClass("is-finished-workout")) {
                var regimen_id = self.$current_item.data('regimen-id');
                var regimen_day = self.$current_item.data('regimen-day');
                $("li[data-regimen-id='" + regimen_id + "'][data-regimen-day='" + regimen_day + "']").removeClass().addClass("is-finished-workout");
                self.buildVideosArr(self.$current_item.find("[data-video]"), self.$current_item.data("regimen-day"));
            }
        });
        this.$close_modal.on("click", function () {
            if (self.isFinished || self.finish_program_btn) {
                $('finish-#program .has-skipped-workouts').toggleClass('hidden', self.isFinished);
                self.$modal_btn.trigger("click");
            }
        });
        setTimeout(function () {
            if ($("#change-weight").length) {
                _this.$change_modal_btn.trigger("click");
            }
        }, 1000);
    };
    VideoStatus.prototype.buildVideosArr = function (videos, id) {
        var _this = this;
        videos.each(function (index, el) {
            _this.$current_video = $(el);
            _this.videosArr.push(_this.$current_video.data("video"));
        });
        if (this.videosArr.length) {
            this.sendVideoRequest(this.videosArr, id);
        }
    };
    VideoStatus.prototype.updateData = function (data) {
        var current_data = JSON.parse(data);
        this.$calc_value.html(current_data.progress + "%");
        this.$completed_value.html(current_data.completed_workouts_count);
        this.$left_value.html(current_data.days_left_count);
        var current_skipped_days = parseInt(this.$skipped_days.html());
        console.log(this.current_item_was_skipped);
        if (current_skipped_days > 0 && this.current_item_was_skipped) {
            this.$skipped_days.html(current_skipped_days - 1);
            if (current_skipped_days === 1) {
                this.$skipped_days.removeClass('left-count-more-zero');
            }
        }
        if (current_data.regimen_finished) {
            this.isFinished = true;
        }
    };
    VideoStatus.prototype.sendVideoRequest = function (videos, id) {
        var self = this;
        var settings = {
            method: "POST",
            url: this.requestLink,
            dataType: "json",
            data: { 'videos': JSON.stringify(videos), 'regimen_day': id },
            success: function (data) {
                self.updateData(data.response_data);
            },
            error: function (error) {
                console.log("error", error);
            }
        };
        $.ajax(settings);
        this.videosArr = [];
    };
    return VideoStatus;
}(View));
