class VideoStatus extends View {
  private $list:JQuery;
  private $all_list:JQuery;
  private $list_item:JQuery;
  private $current_item:JQuery;
  private $video:JQuery;
  private $current_video:JQuery;
  private $skipped_days:JQuery;
  private programm_id:number;
  private videosArr:any[];
  private requestLink:any;
  private isFinished:any;
  private finish_program_btn :any;
  private current_item_was_skipped :any;

  // user info
  private $calc_value:JQuery;
  private $completed_value:JQuery;
  private $left_value:JQuery;

  // modal btn
  private $modal_btn:JQuery;
  private $change_modal_btn:JQuery;
  private $close_modal:JQuery;

  public init(holder = $(".video-thumbs")):void {
    this.initSelectors(holder);
    this.initEvents();
  }

  private initSelectors(holder):void {
    this.programm_id = $("[data-program_id]").data("program_id");
    this.requestLink = window.location.origin + "/program/" + this.programm_id + "/user-video-progress";
    this.$all_list = holder;
    this.$list = holder.not('.skipped-workouts');
    this.$all_list_item = this.$all_list.children().not(".additional-block, .rest-day");
    this.$list_item = this.$list.children().not(".additional-block, .rest-day");
    this.videosArr = [];

    // user info
    this.$calc_value = $(".calculator-item:first").find(".calculator-value");
    this.$completed_value = $(".completed-count");
    this.$left_value = $(".workouts-counter .left-count");
    this.$skipped_days = $(".calculator-item .left-count");

    // modal btn
    this.$modal_btn = $("[data-modal='#finish-program']");
    this.$change_modal_btn = $("[data-modal='#change-weight']");
    this.$close_modal = $(".close-modal");

    // is program finished
    this.isFinished = false;
  }

  private initEvents():void {
    var self = this;
    this.$all_list_item.on("click", function(e:any) {
      self.$current_item = $(this);
      self.current_item_was_skipped = self.$current_item.hasClass('is-skipped-workout');
      self.finish_program_btn  = self.$current_item.is(self.$list_item.last());

      let $popup = self.$current_item.find("[data-modal]")
      // console.log('$current_item', self.$current_item)
      // console.log('current_item_was_skipped', self.current_item_was_skipped)
      if (!self.$current_item.hasClass("is-finished-workout")) {
        const regimen_id:number = self.$current_item.data('regimen-id');
        const regimen_day:number = self.$current_item.data('regimen-day');
        $(`li[data-regimen-id='${regimen_id}'][data-regimen-day='${regimen_day}']`).removeClass().addClass("is-finished-workout");
        // self.$current_item.removeClass().addClass("is-finished-workout");
        self.buildVideosArr(self.$current_item.find("[data-video]"), self.$current_item.data("regimen-day"));
      }
    });

    this.$close_modal.on("click", function () {
      if (self.isFinished || self.finish_program_btn ) {
        $('finish-#program .has-skipped-workouts').toggleClass('hidden', self.isFinished);
        self.$modal_btn.trigger("click");
      }
    });

    setTimeout(() => {
      if ($("#change-weight").length) {
        this.$change_modal_btn.trigger("click");
      }
    }, 1000);
  }

  private buildVideosArr(videos:JQuery, id: number):void {
    videos.each((index:number, el:any) => {
      this.$current_video = $(el);

      this.videosArr.push(this.$current_video.data("video"));
    });

    if (this.videosArr.length) {
      this.sendVideoRequest(this.videosArr, id);
    }
  }

  private updateData(data:string):void {
    let current_data = JSON.parse(data);
    this.$calc_value.html(current_data.progress + "%");
    this.$completed_value.html(current_data.completed_workouts_count);
    this.$left_value.html(current_data.days_left_count);
    const current_skipped_days:number = parseInt(this.$skipped_days.html());
    // console.log(this.current_item_was_skipped)

    if(current_skipped_days > 0 && this.current_item_was_skipped) {
      this.$skipped_days.html(current_skipped_days - 1);
      if(current_skipped_days === 1){
        this.$skipped_days.removeClass('left-count-more-zero');
      }
    }

    if (current_data.regimen_finished) {
      this.isFinished = true;
    }
  }

  private sendVideoRequest(videos:any[], id: number):void {
    let self = this;
    let settings:any = {
      method: "POST",
      url: this.requestLink,
      dataType: "json",
      data: {'videos': JSON.stringify(videos), 'regimen_day': id},
      success: function (data:any):void {
        self.updateData(data.response_data);
      },
      error: function (error:any):void {
        console.log("error", error);
      }
    };

    $.ajax(settings);
    this.videosArr = [];
  }
}
