class VideoThumb extends View {

    private $el:JQuery = $('[data-video]');
    private $addFavoriteVideoBtns:JQuery;
    private $addFavoriteMashupBtns:JQuery;
    private $unfavoriteBtns:JQuery;

    public init():void {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    }

    private setSelectors():void {
        this.$addFavoriteVideoBtns = this.$el.find('.add-favorite');
        this.$addFavoriteMashupBtns = $('.favorite-playlist');
        this.$unfavoriteBtns = this.$el.find('.delete');
    }

    private initEvents():void {
        this.$addFavoriteVideoBtns.on('click', (e)=>this.onAddFavoriteVideoBtnClick(e));
        this.$addFavoriteMashupBtns.on('click', (e)=>this.onAddFavoriteMashupBtnClick(e));
        this.$unfavoriteBtns.on('click', (e)=>this.onUnfavoriteVideoBtnClick(e));
        this.app.getModel(VideoPaginationModel).pageChanged.add(()=>this.onVideoPaginationChange());
    }

    private onAddFavoriteVideoBtnClick(e:any):any {
        e.stopPropagation();
        var id:string = $(e.currentTarget).parent().data('video');
        $.getJSON('/video/favorite/' + id, (data)=> {
            Global.redirectToLoginIfSessionTimesout(data);
        }).fail(function () {
            console.error('Could not add video to favorites!');
        });
        $(e.currentTarget).find('.checkmark').css({display:'inline-block'}).animate({width:20}, 400);
    }

    private onUnfavoriteVideoBtnClick(e:any):any {
        e.stopPropagation();
        var id:string = $(e.currentTarget).parent().parent().data('video');
        $.getJSON('/video/unfavorite/' + id, (data)=> {
            Global.redirectToLoginIfSessionTimesout(data);
        }).fail(function () {
            //console.error('Could not delete favorite!');
        });
        var $li:JQuery = $(e.currentTarget).closest('li');
        $li.fadeOut(400, ()=> {
            var $ul:JQuery = $li.parent();
            $li.remove();
        });
    }

    private onVideoPaginationChange():any {
        this.$el = $('[data-video]');
        this.setSelectors();
        this.initEvents();
    }

    private onAddFavoriteMashupBtnClick(e:any):any {
        e.stopPropagation();
        var id:string = $(e.currentTarget).parent().data('playlist');
        $.getJSON('/playlist/subscribe/' + id, (data)=> {
            Global.redirectToLoginIfSessionTimesout(data);
        }).fail(function () {
            console.error('Could not subscribe to playlist!');
        });
        $(e.currentTarget).find('.checkmark').css({display: 'inline-block'}).animate({width: 20}, 400);

    }

}
