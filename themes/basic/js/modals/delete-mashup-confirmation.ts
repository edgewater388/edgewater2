class DeleteMashupConfirmationModal extends View {

    private $modal:JQuery = $('#confirm-mashup-delete');
    private $confirmBtn:JQuery;
    private playlistId:number;
    private $li:JQuery;


    public init():void {
        if (this.$modal.length) {
            this.setSelectors();
            this.initEvents();
        }
    }

    private setSelectors():void {
        this.$confirmBtn = this.$modal.find('.confirm');
    }

    private initEvents():void {
        $('[data-confirm-mashup-delete]').on('click', (e)=>this.onDeleteMashupBtnClick(e));
        this.$confirmBtn.on('click', (e)=>this.onConfirmBtnClick(e));
    }

    private onDeleteMashupBtnClick(e):void {
        e.preventDefault();
        e.stopPropagation();
        this.playlistId = $(e.currentTarget).closest('.thumb').data('playlist');
        this.$li = $(e.currentTarget).closest('li');
    }

    private onConfirmBtnClick(e:any):any {
        $.getJSON('/playlist/delete/' + this.playlistId, (data)=> {
            Global.redirectToLoginIfSessionTimesout(data);
        }).fail(function () {
            console.error('Could not delete mashup!');
        });
        this.$li.fadeOut(400, ()=> {
            var $ul:JQuery = this.$li.parent();
            this.$li.remove();
            this.app.getModel(ModalsModel).mashupDeleted.dispatch();
        });
    }

}