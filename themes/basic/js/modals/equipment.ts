class Equipment {

    constructor(private $el:JQuery, private data:any) {
        this.$el.find('.equipment-thumbs').fadeOut(0);
        if(this.data.length) {
            this.$el.fadeIn();
            this.displayEquipment();
            this.initSlickCarousel();
        } else {
            this.$el.hide();
        }
    }

    private displayEquipment():void {
        var $equipmentThumbs:JQuery = this.$el.find('.equipment-thumbs');
        if ($equipmentThumbs.hasClass('slick-initialized')) {
            $equipmentThumbs.slick('unslick');
        }
        $equipmentThumbs.empty();
        for (var i = 0; i < this.data.length; i++) {
            var obj = this.data[i];
            var $li = $($('#equipment-li-tpl').html());
            $li.find('a').attr('href', obj.url);
            $li.find('img').attr('src', obj.thumbUrl);
            $equipmentThumbs.append($li);
        }
    }

    private initSlickCarousel():void {
        var $thumbs:JQuery = this.$el.find('.equipment-thumbs');
        $thumbs.show();
        if(!$thumbs.is(":visible")) {
            // retry until modal is faded in and we can init.
            // slick needs item to be visible so calculations are correct.
            setTimeout(()=> {
                this.initSlickCarousel();
            }, 100);
            return;
        }
        
        $thumbs.fadeIn().slick({
            infinite: false,
            dots: false,
            arrows: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            accessibility: false,
            edgeFriction: 0,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 750,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                }
            ]
        });
    }

}
