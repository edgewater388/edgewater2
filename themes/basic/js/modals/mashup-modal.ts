class MashupModal extends View {

    private $el:JQuery = $('#mashup');
    private $title:JQuery;
    private $description:JQuery;
    private curIndex:number;
    private $thumbSlider:JQuery;
    private data:any;
    private videoEmbed:VideoEmbed;

    public init():void {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    }

    private setSelectors():void {
        this.$title = this.$el.find('.video-title');
        this.$description = this.$el.find('.description').find('p');
        this.$thumbSlider = this.$el.find('.thumb-slider');
    }

    private initEvents():void {
        $('[data-playlist]').on('click', (e)=>this.onVideoThumbClick(e));
        $('[data-regimen-id]').find('ol').on('click', (e)=>this.onRegimenClick(e));
        this.app.getModel(MashupMakerModel).liAdded.add(()=>this.onVideoEditorLiAdded());
    }

    private onVideoThumbClick(e:any):any {
        var playlistId:string = $(e.currentTarget).data('playlist');
        this.curIndex = 0;
        this.loadPlaylistData(playlistId);
        if (this.$thumbSlider.hasClass('slick-initialized')) {
            this.$thumbSlider.slick('unslick').hide();
        }
    }

    private loadPlaylistData(playlistId:string):void {
        $.getJSON('/playlist/' + playlistId, (data)=> {
            Global.redirectToLoginIfSessionTimesout(data);
            this.onJsonLoad(data);
        }).fail(function () {
            console.error('Could not load playlist data!');
        });
    }

    private onRegimenClick(e):void {
        var regimentId:string = $(e.currentTarget).closest('li').data('regimen-id');
        var day:string = $(e.currentTarget).closest('li').data('regimen-day');
        this.curIndex = 0;
        this.loadRegimenPlaylistData(regimentId, day);
        if (this.$thumbSlider.hasClass('slick-initialized')) {
            this.$thumbSlider.slick('unslick').hide();
        }
    }

    private loadRegimenPlaylistData(regimenId:string, day:string):void {
        $.getJSON('/regimen/mashup?regimen_id=' + regimenId + '&day=' + day, (data)=> {
            Global.redirectToLoginIfSessionTimesout(data);
            this.onJsonLoad(data);
        }).fail(function () {
            console.error('Could not load playlist data!');
        });
    }

    private onJsonLoad(data):void {
        this.data = data;
        this.$title.html(data.videos[0].title + ' (' + data.videos.length + ' workouts)');

        // wait for scrim to be visible before playing video
        if ($('#mashup').find('.bc-player').is(':visible')) {
            this.initVideoThumbs();
            this.initVideoEmbed();
        } else {
            setTimeout(()=> {
                this.onJsonLoad(data);
            }, 30);
        }
    }

    private initVideoThumbs():void {
        this.$thumbSlider.empty();
        for (var i = 0; i < this.data.videos.length; i++) {
            var curVideo = this.data.videos[i];
            var thumbHtml:string = $('#mashup-thumb').html();
            var $thumb:JQuery = $(thumbHtml);
            $thumb.find('img').attr('src', curVideo.meta.videoStillURL);
            $thumb.find('.title').html(curVideo.title);
            this.$thumbSlider.append($thumb);
        }
        this.$thumbSlider.find('li').on('click', (e)=>this.onThumbClick(e));
        setTimeout(()=> {
            this.initSlickCarousel();
        }, 1000);
    }

    private initSlickCarousel():void {
        if (this.$thumbSlider.hasClass('slick-initialized')) {
            this.$thumbSlider.slick('unslick');
        }

        this.$thumbSlider.fadeIn().slick({
            infinite: false,
            dots: false,
            arrows: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            accessibility: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 850,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ]
        });
        this.setActiveThumb(0);
    }

    public setActiveThumb(index):void {
        var $lis:JQuery = this.$thumbSlider.find('li');
        $lis.removeClass('active').eq(index).addClass('active');
        this.$thumbSlider.slick('slickGoTo', index);
    }

    public showEquipment(index):void {
        new Equipment(this.$el.find('.equipment-required'), this.data.videos[index].products);
    }

    private onThumbClick(e:any):any {
        this.videoEmbed.gotoVideo($(e.currentTarget).index());
    }

    private initVideoEmbed():void {
        var videos = [];
        for (var i = 0; i < this.data.videos.length; i++) {
            videos.push({
                poster: this.data.videos[i].meta.videoStillURL,
                rmtp: this.data.videos[i].meta.rmtp,
                mobile: this.data.videos[i].meta.source_720,
                id: this.data.videos[i].id,
                mediaid: this.data.videos[i].meta.id
            });
        }
        this.videoEmbed = new VideoEmbed(videos, this.$el, this);
    }

    private onVideoEditorLiAdded():any {
        $('[data-playlist]').on('click', (e)=>this.onVideoThumbClick(e));
    }

}
