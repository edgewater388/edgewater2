class MashupNameModal extends View {

    private $el:JQuery = $('#mashup-name');
    private $saveBtn:JQuery;

    public init():void {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    }

    private setSelectors():void {
        this.$saveBtn = this.$el.find('.btn');
    }

    private initEvents():void {
        this.$saveBtn.on('click', (e)=>this.onSaveBtnClick(e));
    }

    private onSaveBtnClick(e:any):any {
        var mashupName:string = this.$el.find('input').val();
        $('#mashup-title').html(mashupName);
    }
    
}