class PersonalInfoModal extends View {
    private $el:JQuery = $('#start-program-form');
    private $signInBtn:JQuery;
    private $formItems:JQuery;
    private request_link:string;
    private redirect_link:string;

    public init():void {
        this.setSelectors();
        this.initEvents();
        this.buildRequest();
        this.buildRedirect();
    }

    private buildRequest() {
        this.request_link = window.location.origin + this.$el.attr("action");
    }

    private buildRedirect() {
        this.redirect_link = window.location.origin + "/program/" + $("[data-program_id]").data("program_id") + "/subscribe";
    }

    private setSelectors():void {
        this.$signInBtn = this.$el.find('.sign-in-btn');
        this.$formItems = this.$el.find("input:text");
    }

    private initEvents():void {
        this.$signInBtn.on('click', (e) => {
            this.checkValues(this.$formItems);

            if (this.validateForm()) {
                this.onSignInButtonClick(e);
            } else {
                this.preventFormSubmit(e);
            }
        });
    }

    private onSignInButtonClick(e:any):void {
        e.preventDefault();
    }

    private checkValues(els:any):void {
        var i:number,
            $element:JQuery;

        for (i=0; i<els.length; i++) {
            $element = $(els[i]);

            if ($element.val() === "") {
                $element.addClass("error");
            } else {
                $element.removeClass("error");
            }
        }
    }

    private validateForm():any {
        return this.$el.find(".error").length;
    }

    private preventFormSubmit(e:any):void {
        e.preventDefault();

        let self = this;
        let params = {
            method: "POST",
            url: this.request_link,
            data: this.$el.serialize(),
            success: function (data:any):void {
                window.location.replace(self.redirect_link);
            },
            error: function (error:any):void {
                console.log("error", error);
            }
        };

        $.ajax(params);
    }
}
