class SignInModal extends View {

    private $el:JQuery = $('#sign-in');
    private $signInBtn:JQuery;

    public init():void {
        this.setSelectors();
        this.initEvents();
        this.checkForDeeplink();
    }

    private setSelectors():void {
        this.$signInBtn = this.$el.find('.sign-in-btn');
    }

    private initEvents():void {
        this.$signInBtn.on('click', (e)=>this.onSignInButtonClick(e));
    }

    private onSignInButtonClick(e:any):any {
        e.preventDefault();
    }

    private checkForDeeplink():void {
        if(URLHelper.getParameterByName('login') == '1') {
            $('[data-modal=#sign-in]').trigger('click');
        }    
    }
    
}
