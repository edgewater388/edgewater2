class TutorialModal extends View {

    private $el:JQuery = $('#tutorial');
    private $navLis:JQuery;
    private $nextBtn:JQuery;
    private step:number = 1;
    private $steps:JQuery;
    private $closeBtns:JQuery;
    private $stepsUl:JQuery;

    public init():void {
        if(this.$el.length) {
            this.setSelectors();
            this.initEvents();
            this.checkForDeeplink();
        }
    }

    private setSelectors():void {
        this.$el.find('.next-step-btn');
        this.$steps = this.$el.find('.steps').find('li');
        this.$nextBtn = this.$el.find('.next-step-btn');
        this.$navLis = this.$el.find('.nav').find('.dot');
        this.$closeBtns = this.$el.find('.close-modal');
        this.$stepsUl = this.$el.find('.steps');
    }

    private initEvents():void {
        this.$nextBtn.on('click', (e)=>this.onNextBtnClick(e));
        this.$navLis.on('click', (e)=>this.onNavLiClick(e));
        this.$closeBtns.on('click', (e)=>this.onCloseBtnClick(e));
    }

    private onNextBtnClick(e:any):any {
        if(this.step < 3) {
            this.step++;
            this.gotoStep(this.step);
        } else {
            this.$el.find('.close-modal').trigger('click');
            this.resetTutorial();
        }
    }

    private onNavLiClick(e:any):any {
        this.step = $(e.currentTarget).index() + 1;
        this.gotoStep(this.step);
    }

    private gotoStep(step:number):void {
        this.$stepsUl.fadeOut(400, ()=>{
            this.$steps.hide().eq(step - 1).show();
            this.$stepsUl.fadeIn(500);
        })

        this.$navLis.removeClass('active').eq(step - 1).addClass('active');
        this.setNextBtnText();
    }

    private setNextBtnText():void {
        if (this.step == 3) {
            this.$nextBtn.find('.next-text').hide();
            this.$nextBtn.find('.finish-text').show();
        } else {
            this.$nextBtn.find('.next-text').show();
            this.$nextBtn.find('.finish-text').hide();
        }
    }

    private resetTutorial():void {
        setTimeout(()=> {
            this.step = 1;
            this.gotoStep(this.step);
        }, 1500);
    }

    private onCloseBtnClick(e:any):any {
        this.resetTutorial();
    }

    private checkForDeeplink():void {
        if (URLHelper.getParameterByName('tutorial') == '1') {
            $('[data-modal=#tutorial]').trigger('click');
        }
    }

}
