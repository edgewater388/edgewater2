class VideoEmbed extends View {
    readonly INITIAL_VOLUME = 80;

    private videoPlayer:any;
    private index:number = 0;

    constructor(private videos:any[], private $modal:JQuery, private $mashupRef:any = null) {
        super();
        this.embedPlayer();
        this.initEvents();
    }

    private embedPlayer():void {
        var $embed:JQuery = $($('#video-embed').html());
        this.$modal.find('.player').empty().append($embed);
        this.videoPlayer = jwplayer("jw-player");

        var isPlaylist:boolean = this.videos.length > 1;
        this.videoPlayer.setup({
            playlist: isPlaylist ? this.getPlaylistVideos(): null,
            sources: isPlaylist ? null : [{file: this.videos[this.index].rmtp}, {file: this.videos[this.index].mobile}],
            autostart: false,
            height: '100%',
            width: '100%',
            mediaid: isPlaylist ? null : this.videos[this.index].mediaid,
            image: this.videos[this.index].poster
        }).setVolume(this.INITIAL_VOLUME);
    }

    private getPlaylistVideos():any {
        var videos:any = [];
        for (var i = 0; i < this.videos.length; i++) {
            var curVideo = this.videos[i];
            videos.push({
                sources: [{file: curVideo.rmtp}, {file: curVideo.mobile}],
                image: curVideo.poster,
                mediaid: curVideo.mediaid,
                title: 'Video ' + (i + 1)
            });
        }
        return videos;
    }

    private initEvents():void {
        this.app.getModel(ModalsModel).modalClosed.add(()=>this.onModalClosed());
        this.videoPlayer.on('complete', (e)=>this.onVideoComplete(e));
        this.videoPlayer.on('firstFrame', (e)=>this.onVideoPlay(e));
        this.videoPlayer.on('playlistItem', (e)=>this.onPlaylistItemChanged(e));
    }

    private onVideoComplete(e):any {
        this.trackPlayedVideo();
        if (this.videos.length > 1) {
            if (this.index < this.videos.length - 1) {
                this.videoPlayer.off('playlistItem', (e)=>this.onPlaylistItemChanged(e));
                this.loadVideo(++this.index);
            }
        }
    }

    private onPlaylistItemChanged(e:any):any {
        //_kmq.push(['record', 'Started Video', {'Started Video ID': this.videos[e.index].id}]);
        this.setActiveThumbAndEquipment(e.index);
    }

    private loadVideo(index:number):void {
        this.videoPlayer.playlistItem(index);
        this.setActiveThumbAndEquipment(index);
    }

    private setActiveThumbAndEquipment(index:number):void {
        if (this.$mashupRef) {
            this.$mashupRef.setActiveThumb(index);
            this.$mashupRef.showEquipment(index);
        }
    }

    public gotoVideo(index):void {
        this.index = index;
        this.loadVideo(this.index);
    }

    private onModalClosed():any {
        this.videoPlayer.remove();
        this.$modal.find('.player').empty();
    }

    private trackPlayedVideo():void {
        _kmq.push(['record', 'Finished Video', {'Finished Video ID': this.videos[this.index].id}]);
        woopra.track("video_played", {'Finished Video ID': this.videos[this.index].id});
        ga('send', {
            hitType: 'event',
            eventCategory: 'Videos',
            eventAction: 'finished',
            eventLabel: this.videos[this.index].id
        });
        $.getJSON('/video/played/' + this.videos[this.index].id, (data)=> {
            //Global.redirectToLoginIfSessionTimesout(data);
            //console.log('video tracked: ' + this.videos[this.index].id);
        }).fail(function () {
            console.error('Could not track video!');
        });
    }

    private onVideoPlay(e:any):any {
        _kmq.push(['record', 'Started Video', {'Started Video ID': this.videos[this.index].id}]);
        woopra.track("video_started", {'Started Video ID': this.videos[this.index].id});
        ga('send', {
            hitType: 'event',
            eventCategory: 'Videos',
            eventAction: 'play',
            eventLabel: this.videos[this.index].id
        });
    }

}
