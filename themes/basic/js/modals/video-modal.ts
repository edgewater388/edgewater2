class VideoModal extends View {

    private $el:JQuery = $('#video');
    private $title:JQuery;

    private $instructor:JQuery;
    private $bodyPart:jQuery;
    private $duration:jQuery;
    private $equipment:jQuery;
    private $products:jQuery;

    public init():void {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    }

    private setSelectors():void {
        this.$title = this.$el.find('.video-title');
        this.$instructor = this.$el.find('.video-instructor');
        this.$bodyPart = this.$el.find('.video-body-part');
        this.$duration = this.$el.find('.video-duration');
        this.$equipment = this.$el.find('.video-equipment');
        this.$products = this.$el.find('.recommended-items');
    }

    private initEvents():void {
        $('[data-video]').on('click', (e)=>this.onVideoThumbClick(e));
        $('[data-preview-video]').on('click', (e)=>this.onPreviewVideoThumbClick(e));
        this.app.getModel(VideoPaginationModel).pageChanged.add(()=>this.onVideoPaginationChange());
        this.app.getModel(MashupMakerModel).liAdded.add(()=>this.onMashupEditorLiAdded());
    }

    private onVideoThumbClick(e:any):any {
        var videoId:string = $(e.currentTarget).data('video');
        this.loadVideoData(videoId);
    }

    private loadVideoData(videoId:string):void {
        $.getJSON('/video/' + videoId, (data)=> {
            Global.redirectToLoginIfSessionTimesout(data);
            this.onVideoDataLoaded(data);
        }).fail(function () {
            console.error('Could not load video data!');
        });
    }

    private onVideoDataLoaded(data):void {
        let productsTitle = this.$el.find('.recommended-items-title');
        this.$title.html(data.title);
        this.$bodyPart.html(data.body_part);
        this.$duration.html(data.time);
        this.$equipment.html(data.equipment);

        if (data.instructor_profile_name){
            this.$instructor.addClass('description-item').html(`<div><h3>YOUR INSTRUCTOR</h3><span>${data.instructor_profile_name}</span></div>`);
        } else {
            this.$instructor.removeClass('description-item').html('');
        }
        
        if (data.products.length > 0) {
            productsTitle.html('<strong class="recom-title">Recommended items to buy:</strong>');
            this.drawRecomendedProducts(data, this.$products);
            this.$el.find('.lockout').addClass('has-product')
        } else {
            productsTitle.html('');
            this.$products.html('');
            this.$el.find('.lockout').removeClass('has-product')
        }

        // wait for scrim to be visible before playing video
        if ($('#video').find('.bc-player').is(':visible')) {
            new Equipment(this.$el.find('.equipment-required'), data.products);
            new VideoEmbed([{
                poster: data.meta.videoStillURL,
                rmtp: data.meta.rmtp,
                mobile: data.meta.source_720,
                id: data.id,
                mediaid: data.meta.id
            }], this.$el);
        } else {
            setTimeout(()=> {
                this.onVideoDataLoaded(data);
            }, 30);
        }

        // Initialize social share buttons
        this.initSocialWidget("#social_widget_0", data);
    }

    private initSocialWidget(widgetId:string, data:any):void {
        const title = this.replaceSpecCharacters(data.title);
        $(widgetId).jsSocials({
			showLabel: "",
			showCount: "",
			shareIn: "popup",
			text: `Hi everyone, I just did Physique 57's ${title} video. Help me stay motivated by liking this post!`,
			url: `${window.location.origin}/videoPage/show/${data.id}`,	
			shares: [{"share":"facebook","logo":"fa fa-facebook","label":"facebook"},{"share":"twitter","logo":"fa fa-twitter","label":"twitter", 'shareUrl': "https://twitter.com/share?url=''&text={text}"}]
		});
    }

    private drawRecomendedProducts (data, prodEl:jQuery):void {
        prodEl.html('');
        data.products.forEach(element => {
            let price = element.price ? `$ ${element.price}` : '',
                priceColor = `style="background-color: #${element.price_color}"`;
            prodEl.append( `<li>
                                    <a href="${element.url}" target="_blank" rel="noopener noreferrer">
                                        <img src="${element.thumbUrl}" alt="image description" />
                                        <div class="recommended-items__description">
                                            <span class="title">${element.title}</span>
                                            <span class="price" ${priceColor}>
                                                ${price}
                                            </span>
                                        </div>
                                    </a>
                                </li>` );
        });
    }

    private onPreviewVideoThumbClick(e:any):any {
        var videoId:string = $(e.currentTarget).data('preview-video');
        this.loadPreviewVideoData(videoId);
    }

    private loadPreviewVideoData(videoId:string):void {
        $.getJSON('/preview/' + videoId, (data)=> {
            Global.redirectToLoginIfSessionTimesout(data);
            this.onVideoDataLoaded(data);
        }).fail(function () {
            console.error('Could not load video data!');
        });
    }

    private onVideoPaginationChange():any {
        $('[data-video]').on('click', (e)=>this.onVideoThumbClick(e));
    }

    private onMashupEditorLiAdded():any {
        $('[data-video]').on('click', (e)=>this.onVideoThumbClick(e));
    }

    private replaceSpecCharacters(str:string) {
        return str.replace(/&amp;/g, '&')
                .replace(/&gt;/g, '>')
                .replace(/&lt;/g, '<')
                .replace(/&quot;/g, '"')
                .replace(/&#039;/g, "'");
    }
}
