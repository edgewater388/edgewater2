class LiveStream {
    readonly STATUS_REQUEST_INTERVAL = 5000;

    private options: any = {};
    private holder: JQuery;
    private needToStartSound: boolean = false;

    private initNativePlayer():any {
        let _self = this;
        const fullScreenBtn = $('[data-role="player-fullscreen"]');
        fullScreenBtn.click(() => {
            let iframe = document.querySelector('#iframe-stream-player iframe');
            iframe.src = iframe.src.replace(/mute=true/, 'mute=false');
            this.needToStartSound = true;
            fullScreenBtn.remove();
        });
    }

    private render(isFull:boolean = false):void {
        if ((this.options.time.start.getTime() - this.options.time.startIn) > Date.now()) {
            if (!this.holder.is(':hidden')) {
                this.holder.hide();
                this.options.ui.hideEl.show();
            }
            return;
        }else{
            if (this.holder.is(':hidden')) {
                this.holder.show();
                this.options.ui.hideEl.hide();
            }
        }
        if (!this.options.status.enabled || this.options.time.end.getTime() < Date.now()) {
            let iframe = document.querySelector('#iframe-stream-player iframe');
            this.holder.find('.player-content').hide();
            if(iframe.src.indexOf('mute=false') !== -1){
                iframe.src = iframe.src.replace(/mute=false/, 'mute=true');
            }
            this.holder.data('status', 'disabled');

            this.options.ui.hideEl.show();

            return;
        }

        if (isFull) {
            this.initNativePlayer();
        }

        this.options.ui.hideEl.hide();

        if (Date.now() >= this.options.time.start.getTime() && this.holder.data('status') !== 'streaming') {
            this.holder.find('.message-holder').hide();
            this.holder.find('.player-holder').show().removeClass('hidden');
            this.holder.data('status', 'streaming');
            let iframe = document.querySelector('#iframe-stream-player iframe');
            if (this.needToStartSound && iframe.src.indexOf('mute=true') !== -1) {
                iframe.src = iframe.src.replace(/mute=true/, 'mute=false');
            }
        }

        if (this.options.status.enabled && Date.now() < this.options.time.start.getTime() && this.holder.data('status') !== 'waiting') {
            let iframe = document.querySelector('#iframe-stream-player iframe');
            this.holder.find('.player-holder').hide();
            if(iframe.src.indexOf('mute=false') !== -1){
                iframe.src = iframe.src.replace(/mute=false/, 'mute=true');
            }
            this.holder.find('.message-holder')
                .css('background-image', this.options.backgrounds.countdown ? `url(${this.options.backgrounds.countdown})` : 'none')
                .show().removeClass('hidden');

            if (!this.holder.data('countdown-interval')) {
                const countdownInterval = setInterval(() => {
                    if (this.holder.data('status') !== 'waiting') {
                        return;
                    }

                    const pad = num => {
                        return ('0' + num).substr(- 2);
                    };

                    const diffFullSeconds = Math.round((this.options.time.start.getTime() - Date.now()) / 1000);
                    const diffHours = Math.floor(diffFullSeconds / (60 * 60));
                    const diffMinutes = Math.floor((diffFullSeconds / 60) - (diffHours * 60));
                    const diffSeconds = Math.floor((diffFullSeconds) - (diffHours * (60 * 60)) - (diffMinutes * 60));

                    const minutesTimerMessage = `${pad(diffHours)} : ${pad(diffMinutes)}`;
                    const responsiveTimerMessage = `${pad(diffHours ? diffHours : diffMinutes)} : ${pad(diffHours ? diffMinutes : diffSeconds)}`;
                    const fullTimerMessage = `${pad(diffHours)} : ${pad(diffMinutes)} : ${pad(diffSeconds)}`;

                    const timerMessage = this.options.messages.countdown
                        .replace('{{minutes_timer}}', minutesTimerMessage)
                        .replace('{{timer}}', responsiveTimerMessage)
                        .replace('{{full_timer}}', fullTimerMessage);

                    if (timerMessage != this.holder.data('countdown-timermessage') && diffFullSeconds >= 0) {
                        this.holder.find('.message-holder .message-wrapper').html(timerMessage);
                        this.holder.data('countdown-timermessage', timerMessage)
                    }
                }, 1000);
                this.holder.data('countdown-interval', countdownInterval);
            }

            this.holder.data('status', 'waiting');
        }
    }

    private assign(first, second):void {
        for (let key in second) {
            if (second.hasOwnProperty(key)) {
                if (typeof first[key] === 'undefined') {
                    first[key] = second[key];
                }
            }
        }
    }

    private initOptions(options, $, data, onChangeCb):void {
        let isOnRequest = false;

        this.assign(options, {
            time: {
                start: new Date(data['startLiveAt'] * 1000),
                startIn: data['startVideoIn'] * 60000,
                end: new Date(data['endLiveAt'] * 1000)
            },
            status: {
                enabled: data['enabled'],
                onLive: true
            },
            messages: {
                countdown: data['countdownMessage'],
                readyToStart: data['readyToStartMessage']
            },
            backgrounds: {
                countdown: data['countdownBackground'],
                readyToStart: data['readyToStartBackground']
            },
            data: {
                embed: data['embed'],
                updateUrl: data['updateUrl']
            },
            ui: {
                hideEl: $(data['hideContent'])
            }
        });

        const updateStatus = () => {
            return $.getJSON(options.data.updateUrl, data => {
                options.status.enabled = data.enabled && data.enabled === "1";
                options.status.onLive = true;
                options.time.start = new Date(data.startLiveAt * 1000);
                options.time.startIn = data.startVideoIn * 60000;
                options.time.end = new Date(data.endLiveAt * 1000);

                onChangeCb.bind(this).call();
            });
        };

        setInterval(() => {
            if (isOnRequest) {
                return;
            }

            isOnRequest = true;
            updateStatus().always(function () {
                isOnRequest = false;
            });
        }, this.STATUS_REQUEST_INTERVAL);
    }

    constructor($, holder) {
        if (!holder.length) {
            throw 'Invalid Player Element';
        }

        this.holder = holder;
        this.initOptions(this.options, $, $(holder).data(), this.render);

        this.render(true);
    }
}