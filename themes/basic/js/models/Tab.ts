class Tab {
  private tabsContainer: string;
  private tabItem: string;
  private defaultOpened : number;
  private hideClass: string;

  constructor(selector: string, tabSelector: string, defaultOpened = 0, hideClass: string){
    this.tabsContainer = $(selector);
    this.tabItem = this.tabsContainer.find(tabSelector);
    this.defaultOpened = defaultOpened;
    this.hideClass = hideClass;
  }

  public init():void {
    this.showNeededItem();
    this.eHandling();
  }

  private eHandling():void {
    this.tabsContainer.find('.link').click(function() {

      $('.step-box').addClass("hide-form")
      $(this).closest('.step-box').removeClass("hide-form");
    });
  }

  private showNeededItem():void {
    var neededItem = this.tabItem.eq(this.defaultOpened);
    this.tabItem.addClass(this.hideClass);
    neededItem.removeClass(this.hideClass);
  }

}
