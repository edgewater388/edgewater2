class Account extends View {

    private $el:JQuery = $('body.account');
    private $redeemBtn:JQuery;
    private $referBtn:JQuery;

    public init():void {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    }

    private setSelectors():void {
        this.$redeemBtn = this.$el.find('#redeem-btn');
        this.$referBtn = this.$el.find('#refer-btn');
    }

    private initEvents():void {
        this.$redeemBtn.on('click', (e)=>this.onRedeemBtnClick(e));
        this.$referBtn.on('click', (e)=>this.onReferBtnClick(e));
        //this.$redeemBtn.on('submit', (e)=>this.onRedeemBtnClick(e));
    }

    private onRedeemBtnClick(e:any):any {

    }

    private onReferBtnClick(e:any):any {


    }

}
