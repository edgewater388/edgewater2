class ChangePlan extends View {

    private $el:JQuery = $('body.change-plan');
    private $modal:JQuery;
    private $continueBtn:JQuery;
    private $termsCheckbox:JQuery;

    public init():void {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
            this.setCurrentPlanInModal(this.$el.find('.active'));
            if ($('form').data('locked-plan') == '1') {
                $('#under-contract').show();
            }
        }
    }

    private setSelectors():void {
        this.$modal = this.$el.find('#confirm-plan');
        this.$continueBtn = this.$el.find('#continue-btn');
        this.$termsCheckbox = this.$el.find('#over_18');
    }

    private initEvents():void {
        this.$el.find('input[type=radio]').on('change', (e)=>this.onRadioChange(e));
        this.$continueBtn.on('click', (e)=>this.onContinueButtonClick(e));
        this.$termsCheckbox.on('change', ()=>this.onCheckboxChanged());
        $('#account-types').find('label').on('click', (e)=>this.onPlanClick(e));
    }


    private onRadioChange(e:any):any {
        this.$el.find('.text-danger').slideUp()
        var $li:JQuery = $(e.currentTarget).closest('li');

        if ($li.hasClass('current')) {
            $('.checkbox-wrap, .buttons, .legal').hide();
            return;
        } else {
            $('.checkbox-wrap, .buttons').show();
        }

        this.setCurrentPlanInModal($li);

        // show correct legal text
        $('.legal').hide();
        $('.legal').filter('.' + $li.data('plan-id')).show();

    }

    private setCurrentPlanInModal($selectedLi:JQuery):void {
        this.$modal.find('.confirm-image').attr('src', $selectedLi.find('img').attr('src'));
        this.$modal.find('.confirm-text-overlay').html($selectedLi.find('.text-overlay').html());
    }

    private onContinueButtonClick(e:any):any {
        if (!this.$termsCheckbox.prop('checked')) {
            e.stopImmediatePropagation();
            this.$el.find('.text-danger').slideDown();
        }
    }

    private onCheckboxChanged():any {
        if (this.$termsCheckbox.prop('checked')) {
            this.$el.find('.text-danger').slideUp();
        }
    }

    private onPlanClick(e:any):any {
        if ($('form').data('locked-plan') == '1') {
            e.stopPropagation();
            e.stopImmediatePropagation();
            e.preventDefault();
        }
    }

}
