class EmailPreferences extends View {

    private $el:JQuery = $('body.email-preferences');
    private $updatesInput:JQuery;
    private $offersInput:JQuery;
    private $surveysInput:JQuery;
    private $unsubscribeInput:JQuery;

    public init():void {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    }

    private setSelectors():void {
        this.$updatesInput = this.$el.find('#updates');
        this.$offersInput = this.$el.find('#offers');
        this.$surveysInput = this.$el.find('#surveys');
        this.$unsubscribeInput = this.$el.find('#unsubscribe');
    }

    private initEvents():void {
        this.$unsubscribeInput.on('click', (e)=>this.onUnsubscribeInputClick(e));
        this.$el.find('.optional').on('click', (e)=>this.onOptionalCheckboxClick(e));;
    }

    private onUnsubscribeInputClick(e:any):any {
        this.$updatesInput.prop('checked', false);
        this.$offersInput.prop('checked', false);
        this.$surveysInput.prop('checked', false);
    }

    private onOptionalCheckboxClick(e:any):any {
        if($(e.currentTarget).prop('checked')) {
            this.$unsubscribeInput.prop('checked', false);
        }
    }

}
