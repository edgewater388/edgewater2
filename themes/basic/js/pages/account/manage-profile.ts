class ManageProfile extends View {

    private $el:JQuery = $('body.manage-profile');
    private $profilePhotoInput:JQuery;
    private $profileForm:JQuery;

    public init():void {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    }

    private setSelectors():void {
        this.$profilePhotoInput = this.$el.find('#Profile_photo');
        this.$profileForm = this.$el.find('#yw0');
    }

    private initEvents():void {
        this.$profilePhotoInput.on('change', (e)=>this.onProfileImageChange(e));
    }

    private onProfileImageChange(e:any):any {
        this.$profileForm.submit();
    }
    
}
