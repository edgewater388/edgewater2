class Faq extends View {

    private $el:JQuery = $('body.faq');
    private $questions:JQuery;

    public init():void {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    }

    private setSelectors():void {
        this.$questions = this.$el.find('.question');
    }

    private initEvents():void {
        this.$questions.on('click', (e)=>this.onQuestionClick(e));
    }


    private onQuestionClick(e:any):any {
        e.preventDefault();
        $(e.currentTarget).toggleClass('active').parent().find('.answer').slideToggle();
    }

}
