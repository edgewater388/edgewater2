var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var fixedPlanBlockSignNew = (function (_super) {
    __extends(fixedPlanBlockSignNew, _super);
    function fixedPlanBlockSignNew() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    fixedPlanBlockSignNew.prototype.init = function () {
        this.fixedPlanBlock();
    };
    fixedPlanBlockSignNew.prototype.fixedPlanBlock = function () {
        var planInfo = $('.plan-info');
        var heightHeader = $('#header').height();
        var scrollTopPlanInfo;
        if (planInfo.length) {
            scrollTopPlanInfo = planInfo.offset().top - heightHeader;
            $(window).scroll(function () {
                var srollTopLastBlock = $('.step-box:nth-child(3)').offset().top;
                var heightLastBlock = $('.step-box:nth-child(3)').height();
                var blockSideOne = planInfo.height() + planInfo.offset().top;
                var blockSideTwo = srollTopLastBlock + heightLastBlock;
                var bottomScroll = planInfo.height() + $(window).scrollTop() + 93;
                if (blockSideOne >= blockSideTwo) {
                    planInfo.addClass("absolute-plan-info");
                }
                if (bottomScroll < blockSideOne) {
                    planInfo.removeClass("absolute-plan-info");
                }
                if ($(window).scrollTop() > scrollTopPlanInfo) {
                    planInfo.addClass("fixed-plan-info");
                }
                else {
                    planInfo.removeClass("fixed-plan-info");
                }
            });
        }
    };
    return fixedPlanBlockSignNew;
}(View));
