class fixedPlanBlockSignNew extends View {
  public init():void {
    this.fixedPlanBlock();
  }

  private fixedPlanBlock():void {
    var planInfo = $('.plan-info');
    var heightHeader = $('#header').height();
    var scrollTopPlanInfo;

    if (planInfo.length) {
      scrollTopPlanInfo = planInfo.offset().top - heightHeader

      $(window).scroll(function () {
        var srollTopLastBlock = $('.step-box:nth-child(3)').offset().top;
        var heightLastBlock =  $('.step-box:nth-child(3)').height();
        var blockSideOne = planInfo.height() + planInfo.offset().top;
        var blockSideTwo = srollTopLastBlock + heightLastBlock;
        var bottomScroll = planInfo.height() + $(window).scrollTop() + 93;

        if (blockSideOne >= blockSideTwo) {
          planInfo.addClass("absolute-plan-info");
        }
        if (bottomScroll < blockSideOne) {
          planInfo.removeClass("absolute-plan-info");
        }
        if ($(window).scrollTop() > scrollTopPlanInfo) {
          planInfo.addClass("fixed-plan-info");
        } else {
          planInfo.removeClass("fixed-plan-info");
        }
      });
    }
  }
}
