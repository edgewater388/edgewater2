/// <reference path="../models/StreamPlayer.ts" />
/// <reference path="../models/LiveStream.ts" />

class HomeLoggedIn extends View {

    private $el:JQuery = $('body.home-logged-in');
    private $workoutRegimens:JQuery;
    private $workoutThumbs:JQuery;
    private $seeMoreRegimensBtn:JQuery;

    public init():void {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
            this.initStreaming();
        }
    }

    private setSelectors():void {
        this.$workoutRegimens = this.$el.find('#workout-regimens');
        this.$workoutThumbs = this.$workoutRegimens.find('li');
        this.$seeMoreRegimensBtn = this.$workoutRegimens.find('#see-more-regimens');
    }

    private initEvents():void {
        this.$workoutThumbs.on('click', (e)=>this.onWorkoutThumbClick(e));
        this.$document.on('touchstart', (e)=>this.onDocumentTouchStart(e));
        this.$seeMoreRegimensBtn.on('click', (e)=>this.onSeeMoreRegimensBtnClick(e));
    }

    private initStreaming():void {
        const jwPlayer = $('#stream-player');
        const liveStreamPlayer = $('#iframe-stream-player');
        if (jwPlayer.length) {
            new StreamPlayer($, $('#stream-player'));
        }
        if (liveStreamPlayer.length) {
            new LiveStream($, $('#iframe-stream-player'));
        }
    }

    private onWorkoutThumbClick(e:any):any {
        if(Browser.isTouch) {
            $(e.currentTarget).find('.hover-overlay').fadeIn(400);
        }
    }

    private onDocumentTouchStart(e:any):any {
        this.$workoutThumbs.find('.hover-overlay').fadeOut(400);
    }

    private onSeeMoreRegimensBtnClick(e:any):any {
        e.preventDefault();
        this.$workoutThumbs.slideDown();
        this.$seeMoreRegimensBtn.fadeOut();
    }
    
}   
