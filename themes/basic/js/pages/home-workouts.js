var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var homeWorkouts = (function (_super) {
    __extends(homeWorkouts, _super);
    function homeWorkouts() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    homeWorkouts.prototype.init = function () {
        this.showAll();
        this.typeFiltering();
    };
    homeWorkouts.prototype.showAll = function () {
        var that = this;
        var showAllBtn = $('.see-more-regimens a');
        showAllBtn.on('click', function (e) {
            var duration = $(this).closest('.workouts-section').attr('data-duration'), el = $(this).closest('.workouts-section').find($('.video-thumbs')), elementsData, markup, test = [], self = this, url;
            e.preventDefault();
            if (el.attr('show-all') === 'true') {
                return;
            }
            else {
                if ($(this).closest('.workouts-section').attr('data-duration')) {
                    url = "/video/by-duration/" + ("" + duration) + "/all";
                }
                else {
                    url = "/video/live-classes/all";
                }
                $.get(url, function (data) {
                    el.empty();
                    that.structureCreator(data, el, self);
                    that.loadingAnimate(el, self);
                });
            }
        });
    };
    homeWorkouts.prototype.structureCreator = function (data, el, context) {
        var that = this, titleArray = ['strength', 'cardio', 'restorative'], videoFrameImg = $('.inner.video .slick-track > .slick-slide > a > img'), numberFilters = {
            1: 'one',
            2: 'two',
            3: 'three'
        };
        var _loop_1 = function (i) {
            var videoItemData = data[i];
            var preparedVideoItemData = {
                id: videoItemData.id,
                duration: videoItemData.duration + ' min',
                imgUrl: videoItemData.meta.thumbnailURL,
                title: videoItemData.title
            };
            var videoItemTemplate = $('#slider-video-item-template').html();
            var videoItemProcessedTemplate = Object.keys(preparedVideoItemData).reduce(function (template, dataKey) {
                return template.replace('{{' + dataKey + '}}', preparedVideoItemData[dataKey]);
            }, videoItemTemplate);
            var template = $(videoItemProcessedTemplate);
            var iconTemplate = $('<div class="icon white"></div>');
            titleArray.forEach(function (item) {
                var filters = videoItemData[item];
                if (!filters) {
                    return;
                }
                if (filters !== '') {
                    var icons = template.find('.icon.white');
                    template.find('.title-icon').append(iconTemplate.clone());
                    for (var i_1 = 0; i_1 < icons.length; i_1++) {
                        $(icons[i_1]).addClass(titleArray[i_1] + " " + numberFilters[videoItemData[item]]);
                    }
                }
            });
            if (videoItemData.products.length) {
                videoFrameImg.attr('src', "" + videoItemData.products[0].thumbUrl);
            }
            el.append(template);
            el.attr("show-all", "true");
        };
        for (var i = 0; i < data.length; i++) {
            _loop_1(i);
        }
        that.videoPopup(el);
    };
    homeWorkouts.prototype.videoPopup = function (el) {
        var dataVideoItem = $('[data-modal]'), videoStatus = new VideoStatus(), modalPopup = new Modals(), videoModal = new VideoModal();
        videoStatus.init(el);
        modalPopup.init(dataVideoItem);
        videoModal.init();
    };
    homeWorkouts.prototype.loadingAnimate = function (el, context) {
        var that = this, templateLoading = $('#loading-video').html(), loading = el.closest('.workouts-section').find('.loading-splash');
        el.fadeOut();
        if (!loading.length) {
            setTimeout(function () {
                $(context).closest('.workouts-section').append($(templateLoading));
            }, 500);
        }
        setTimeout(function () {
            loading = el.closest('.workouts-section').find('.loading-splash');
            loading.remove();
            var videoItems = $(context).closest('.workouts-section').find($('.video-thumbs > li'));
            el.fadeIn();
            videoItems.each(function (index) {
                var that = this;
                setTimeout(function () {
                    $(that).addClass('animate-item');
                }, 200 * index);
            });
            new SizingHelper.init($('[data-match-tallest]'));
        }, 2000);
    };
    homeWorkouts.prototype.typeFiltering = function () {
        var that = this;
        $('.select-holder').change(function () {
            var filters = $(this).closest('.workouts-section').find('select').serialize(), duration = $(this).closest('.workouts-section').attr('data-duration'), el = $(this).closest('.workouts-section').find($('.video-thumbs')), allVideos = $(this).closest('.workouts-section').find($('.video-thumbs > li:not(.stub-video)')), self = this, emptyVideosTemplate = $('#video-item-empty').html(), url;
            if ($(this).closest('.workouts-section').attr('data-duration')) {
                url = "/video/by-duration/" + duration + "/filter";
            }
            else {
                url = "/video/live-classes/filter";
            }
            $.ajax({
                url: url,
                type: "POST",
                data: filters,
                success: function (data, textStatus, jqXHR) {
                    var stub = $(self).closest('.workouts-section').find('.stub-video');
                    allVideos.hide();
                    that.structureCreator(data, el, self);
                    if (data.length) {
                        that.loadingAnimate(el, self);
                        if (stub.length) {
                            stub.remove();
                        }
                    }
                    else {
                        if (stub.length) {
                            stub.remove();
                        }
                        $(self).closest('.workouts-section').append($(emptyVideosTemplate));
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });
        });
    };
    return homeWorkouts;
}(View));
