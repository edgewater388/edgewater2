class homeWorkouts extends View {
  public init():void {
    this.showAll();
    this.typeFiltering();
  }

  private showAll():void {
    let that = this;
    let showAllBtn = $('.see-more-regimens a');
    showAllBtn.on('click', function(e) {
      let duration = $(this).closest('.workouts-section').attr('data-duration'),
          el = $(this).closest('.workouts-section').find($('.video-thumbs')),
          elementsData,
          markup,
          test = [],
          self = this,
          url,
          options = $(this).closest('.workouts-section').find($('select'));
          if(el.attr("show-all")) {
            el.removeAttr("show-all")
          }
      e.preventDefault();
        if ($(this).closest('.workouts-section').attr('data-duration')) {
          url = "/video/by-duration/" + `${duration}` + "/all";
        } else {
          url = "/video/live-classes/all";
        }
        $.get( url , function( data ) {
          el.fadeOut(1000, function() {
              el.empty();
              that.structureCreator(data, el, self);
              that.loadingAnimate(el, self);

              (new VideoThumb()).init();
          });
        });
    });
  }

  private structureCreator(data, el, context):void {
    let that = this,
        titleArray = ['strength', 'cardio', 'restorative'],
        videoFrameImg = $('.inner.video .slick-track > .slick-slide > a > img'),
        numberFilters = {
          1: 'one',
          2: 'two',
          3: 'three'
        };
    if (el.attr("show-all")) {
      return;
    } else {
      el.empty();
      for (let i = 0; i < data.length; i++) {
        let test = 0;
        let videoItemData = data[i];
        let preparedVideoItemData = {
          id: videoItemData.id,
          duration: Math.floor(videoItemData.length / 1000 / 60) + ' min',
          imgUrl: videoItemData.meta.thumbnailURL,
          title: videoItemData.title
        };
        let videoItemTemplate = $('#slider-video-item-template').html();

        let videoItemProcessedTemplate = Object.keys(preparedVideoItemData).reduce(function (template, dataKey) {
          return template.replace('{{' + dataKey + '}}', preparedVideoItemData[dataKey]);
        }, videoItemTemplate);

        let template = $(videoItemProcessedTemplate);
        let iconTemplate = $('<div class="icon white"></div>');
        let counter = -1;

        titleArray.forEach(function(item) {
          let filters = videoItemData[item];
          if(!filters) {
            return;
          }
          if (filters !== '') {
            counter++;
            let templateIndicator = iconTemplate.clone();
            templateIndicator.addClass(`${item} ${numberFilters[videoItemData[item]]}`);
            template.find('.title-icon').append(templateIndicator);
          }
        });
        if (videoItemData.products.length) {
          videoFrameImg.attr('src', `${videoItemData.products[0].thumbUrl}`);
        }

        el.append(template);

        el.attr("show-all", "true");

        (new VideoThumb()).init();
      }
    }

    that.videoPopup(el);
  }

  private videoPopup(el):void {
    let dataVideoItem = $('[data-modal]'),
        videoStatus= new VideoStatus(),
        modalPopup = new Modals(),
        videoModal = new VideoModal();

    videoStatus.init(el);
    modalPopup.init(dataVideoItem);
    videoModal.init();
  }

  private loadingAnimate(el, context):void {
    let that = this,
        templateLoading = $('#loading-video').html(),
        loading = el.closest('.workouts-section').find('.loading-splash');
      if (!loading.length) {
          $(context).closest('.workouts-section').append($(templateLoading)).fadeIn(1000);
      }

    setTimeout(function() {
      loading = el.closest('.workouts-section').find('.loading-splash');
      loading.remove();

      let videoItems = $(context).closest('.workouts-section').find($('.video-thumbs > li'));
      el.fadeIn();
      videoItems.each(function(index) {
        let that = this;
        setTimeout(function() {

          $(that).addClass('animate-item');
        }, 200 * index);
      });
      new SizingHelper.init($('[data-match-tallest]'));
    }, 2000)
  }

  private typeFiltering():void {
      let that = this;
      $('.select-holder').change(function (e) {

        let filters =  $(this).closest('.workouts-section').find('select').serialize(),
            duration = $(this).closest('.workouts-section').attr('data-duration'),
            el = $(this).closest('.workouts-section').find($('.video-thumbs')),
            allVideos = $(this).closest('.workouts-section').find($('.video-thumbs > li:not(.stub-video)')),
            self = this,
            emptyVideosTemplate = $('#video-item-empty').html(),
            url;
            if ($(this).closest('.workouts-section').attr('data-duration')) {
              url = `/video/by-duration/${duration}/filter`;
            } else {
              url = "/video/live-classes/filter";
            }

        $.ajax({
            url : url,
            type: "POST",
            data : filters,
            success: function(data, textStatus, jqXHR) {
                let stub = $(self).closest('.workouts-section').find('.stub-video');

                el.fadeOut(500, function() {
                    that.structureCreator(data, el, self);
                    that.loadingAnimate(el, self);
                });
                if(el.attr("show-all")) {
                  el.removeAttr("show-all")
                }
                if (data.length) {
                    if (stub.length) {
                      stub.remove();
                    }
                } else {
                    if (stub.length) {
                    stub.remove();
                    }
                    setTimeout(() => {
                        $(self).closest('.workouts-section').append($(emptyVideosTemplate));
                    },2500);
                    setTimeout(() => {
                        el.empty();
                        that.loadingAnimate(el, self);
                    },500);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
              if(window.console !== undefined) {
                console.error(textStatus);
              }
            }
        });
    });
  }

}
