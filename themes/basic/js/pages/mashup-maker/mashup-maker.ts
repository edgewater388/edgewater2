class MashupMaker extends View {

    private $el:JQuery = $('body.mashup-maker');
    private $videoList:JQuery;
    private playlistId:string;
    private $addVideosToMashupCTA:JQuery;
    private $mashupVideoEditor:JQuery;
    private $previewBtn:JQuery;
    private $updateMashupNameModalBtn:JQuery;

    public init():void {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
            this.showPlaylistEditorOrCTA();
            this.initSortableList();
        }
    }

    private setSelectors():void {
        this.$videoList = this.$el.find('#video-list');
        this.$addVideosToMashupCTA = this.$el.find('#add-video-to-mashup');
        this.$mashupVideoEditor = this.$el.find('#mashup-video-editor');
        this.$previewBtn = this.$el.find('#mashup-preview');
        this.$updateMashupNameModalBtn = $('#mashup-name').find('.btn');
    }

    private initEvents():void {
        this.addAddToMashupListener();
        this.app.getModel(VideoPaginationModel).pageChanged.add(()=>this.onVideoPaginationChange());
        this.$updateMashupNameModalBtn.on('click', (e)=>this.onMashupNameSaveBtnClick(e));
    }

    private showPlaylistEditorOrCTA():void {
        this.playlistId = URLHelper.getParameterByName('playlist_id');
        if (this.playlistId) {
            this.$mashupVideoEditor.show();
            this.getPlaylistVideosAndDisplay();
            this.$previewBtn.attr('data-playlist', this.playlistId);
            this.app.getModel(MashupMakerModel).liAdded.dispatch();
        } else {
            this.$addVideosToMashupCTA.show();
        }
    }

    private getPlaylistVideosAndDisplay():void {
        $.getJSON('/playlist/' + this.playlistId, (data)=> {
            Global.redirectToLoginIfSessionTimesout(data);
            for (var i = 0; i < data.videos.length; i++) {
                this.appendLi(data.videos[i]);
            }
            $('#mashup-title').html(data.title);
            this.addRemoveCheckmark();
        }).fail(function () {
            console.error('Could not load playlist data!');
        });
    }

    private appendLi(videoData:any):void {
        var videoLiHtml:string = $('#video-editor-li-tpl').html();
        var $li:JQuery = $(videoLiHtml);
        $li.find('.play').attr('data-video', videoData.id);
        $li.find('.title').html(videoData.title);
        $li.find('.instructor').html(videoData.instructor_profile_name ? videoData.instructor_profile_name : '--');
        $li.find('.duration').prepend(Math.ceil(videoData.length / 60 / 1000).toString());
        console.log(videoData);
        var types:string[] = ['strength', 'cardio', 'restorative'];
        for (var j = 0; j < types.length; j++) {
            var className;
            if (videoData[types[j]] == '') className = 'none';
            if (videoData[types[j]] == null) className = 'none';
            if (videoData[types[j]] == 1) className = 'one';
            if (videoData[types[j]] == 2) className = 'two';
            if (videoData[types[j]] == 3) className = 'three';
            $li.find('.type').find('.' + types[j]).addClass(className);
        }

        this.$videoList.append($li);
        this.app.getModel(MashupMakerModel).liAdded.dispatch();
        $li.find('.remove').on('click', (e)=>this.onRemoveBtnClick(e));
    }

    private onRemoveBtnClick(e:any):any {
        var $li:JQuery = $(e.currentTarget).parent();
        var videoId:string = $li.find('.play').data('video');
        $.getJSON('/playlist/remove?playlist_id=' + this.playlistId + '&video_id=' + videoId, (data)=> {
            Global.redirectToLoginIfSessionTimesout(data);
        }).fail(function () {
            console.error('Could not remove video from playlist!');
        });
        $li.fadeOut(300, ()=> {
            $li.remove();
            this.addRemoveCheckmark();
        });
    }

    private addAddToMashupListener():void {
        $('.add-to-mashup').off('click').on('click', (e)=>this.onAddToMashupBtnClick(e));
    }

    private onAddToMashupBtnClick(e:any):any {
        if (this.playlistId.length) {
            this.addVideoToPlaylist(e);
        } else {
            this.createNewPlaylist(e);
        }
        $(e.currentTarget).closest('li').addClass('added');
    }

    private addVideoToPlaylist(e:any):void {
        var videoId:string = $(e.currentTarget).parent().find('.thumb').data('video');
        var videoAlreadyInPlaylist:boolean = false;
        this.$videoList.find('.play').each((i, el)=> {
            var $item:JQuery = $(el);
            if ($item.data('video') == videoId) {
                videoAlreadyInPlaylist = true;
            }
        });
        if (videoAlreadyInPlaylist) return;
        $.getJSON('/playlist/add?playlist_id=' + this.playlistId + '&video_id=' + videoId, (data)=> {
            Global.redirectToLoginIfSessionTimesout(data);
            var dataObj:any = data.video ? data.video : data;
            this.appendLi(dataObj);
            this.addRemoveCheckmark();
        }).fail(function () {
            console.error('Could not add video to playlist!');
        });
    }

    private createNewPlaylist(e):void {
        $('#mashup-title').html('My Mashup');
        $.getJSON('/playlist/create?title=My%20Mashup', (data)=> {
            Global.redirectToLoginIfSessionTimesout(data);
            this.playlistId = data.model.id;
            this.$previewBtn.attr('data-playlist', this.playlistId);
            this.$mashupVideoEditor.show();
            this.$addVideosToMashupCTA.hide();
            this.addVideoToPlaylist(e);
            if (typeof (history.pushState) != "undefined") {
                history.pushState('', document.title, '?playlist_id=' + this.playlistId);
            }
        }).fail(function () {
            console.error('Could not create new playlist!');
        });
    }

    private onVideoPaginationChange():any {
        this.addAddToMashupListener();
        this.addRemoveCheckmark();
    }

    private onMashupNameSaveBtnClick(e:any):any {
        $.getJSON('/playlist/update/' + this.playlistId + '?title=' + encodeURIComponent($('#mashup-name').find('input').val()), (data)=> {
            Global.redirectToLoginIfSessionTimesout(data);
        }).fail(function () {
            console.error('Could not update playlist name!');
        });
    }

    private initSortableList():void {
        var el = document.getElementById('video-list');
        var sortable = Sortable.create(el, {
            animation: 200,
            ghostClass: "sortable-ghost",
            chosenClass: "sortable-chosen",
            filter: ".disable-drag",
            handle: Breakpoints.current == Breakpoints.MOBILE ? ".drag-handle" : ".drag-li",
            onEnd: ()=> {
                var order:string = '';
                this.$videoList.find('li').each((i, el)=> {
                    var $item:JQuery = $(el);
                    order += $item.find('.play').data('video') + ',';
                });
                $.getJSON('/playlist/reorder/' + this.playlistId + '?order=' + order, (data)=> {
                    Global.redirectToLoginIfSessionTimesout(data);
                }).fail(function () {
                    console.error('Could not reorder playlist!');
                });
            }
        });
    }

    private addRemoveCheckmark():void {
        var playlistIds:string[] = [];
        this.$videoList.find('li').each((i, el)=> {
            var $item:JQuery = $(el);
            playlistIds.push($item.find('.play').data('video'));
        });
        this.$el.find('#video-thumbs').find('li').each((i, el)=> {
            var $item:JQuery = $(el);
            var id:string = $item.find('.thumb').data('video');
            if(ArrayUtil.isInArray(id, playlistIds)) {
                $item.addClass('added');
            } else {
                $item.removeClass('added');
            }
        });
    }

}
