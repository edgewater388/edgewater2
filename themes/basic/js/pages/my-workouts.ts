class MyWorkouts extends View {

    private $el:JQuery = $('body.my-workouts');
    private $editMashupBtns:JQuery;
    private $deleteMashupBtns:JQuery;
    private $unsubscribeMashupBtns:JQuery;
    private $unfavoriteBtns:JQuery;
    private $noMashupVideos:JQuery;
    private $noFavoriteVideos:JQuery;
    private $mashupVideos:JQuery;
    private $favoriteVideos:JQuery;

    public init():void {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
            this.showNoMashupsMessageIfNeeded(this.$mashupVideos.find('ul'));
            this.showNoFavoritesMessageIfNeeded(this.$favoriteVideos.find('ul'));
        }
    }

    private setSelectors():void {
        this.$editMashupBtns = this.$el.find('.edit');
        this.$mashupVideos = this.$el.find('#mashup-videos');
        this.$favoriteVideos = this.$el.find('#favorite-videos');
        this.$deleteMashupBtns = this.$mashupVideos.find('.delete');
        this.$unsubscribeMashupBtns = this.$mashupVideos.find('.unsubscribe');
        this.$unfavoriteBtns = this.$favoriteVideos.find('.delete');
        this.$noMashupVideos = this.$mashupVideos.find('.none-available');
        this.$noFavoriteVideos = this.$favoriteVideos.find('.none-available');
    }

    private initEvents():void {
        this.$editMashupBtns.on('click', (e)=>this.onEditMashupBtnClick(e));
        this.$unsubscribeMashupBtns.on('click', (e)=>this.onUnsubscribeMashupBtnClick(e));
        this.$unfavoriteBtns.on('click', (e)=>this.onUnfavoriteBtnClick(e));
        this.app.getModel(ModalsModel).mashupDeleted.add(()=>this.onMashupDeleted());
    }

    private onEditMashupBtnClick(e:any):any {
        e.stopPropagation();
        var id:string = $(e.currentTarget).parent().parent().data('playlist');
        window.location.href = '/mashup-maker?playlist_id=' + id;
    }

    private onUnsubscribeMashupBtnClick(e:any):any {
        e.stopPropagation();
        var id:string = $(e.currentTarget).parent().parent().data('playlist');
        $.getJSON('/playlist/unsubscribe/' + id, (data)=> {
            Global.redirectToLoginIfSessionTimesout(data);
        }).fail(function () {
            console.error('Could not unsubscribe from playlist!');
        });
        var $li:JQuery = $(e.currentTarget).closest('li');
        $li.fadeOut(400, ()=> {
            var $ul:JQuery = $li.parent();
            $li.remove();
            this.showNoMashupsMessageIfNeeded($ul);
        });
    }

    private onUnfavoriteBtnClick(e:any):any {
        e.stopPropagation();
        var $li:JQuery = $(e.currentTarget).closest('li');
        $li.fadeOut(400, ()=> {
            var $ul:JQuery = $li.parent();
            $li.remove();
            this.showNoFavoritesMessageIfNeeded($ul);
        });
    }

    private showNoFavoritesMessageIfNeeded($ul:JQuery):void {
        if (!$ul.children().length) {
            this.$noFavoriteVideos.fadeIn();
        }
    }

    private showNoMashupsMessageIfNeeded($ul:JQuery):void {
        if (!$ul.children().length) {
            this.$noMashupVideos.fadeIn();
        }
    }

    private onMashupDeleted():any {
        console.log('here');
        var $ul:JQuery = this.$mashupVideos.find('.video-thumbs');
        this.showNoMashupsMessageIfNeeded($ul);
    }
    
}
