class Payment extends View {

    private $el:JQuery = $('body.payment');
    private $plan:JQuery;
    private $submitBtn:JQuery;

    public init():void {
        if (this.$el.length) {
            this.setSelectors();
            this.initEvents();
        }
    }

    private setSelectors():void {
        this.$plan = this.$el.find("#plans").find('.plan');
        this.$submitBtn = this.$el.find('#payment-details').find('.buttons').find('.btn');
    }

    private initEvents():void {
        this.$plan.on('click', (e)=>this.onPlanClick(e));
        this.$submitBtn.on('click', (e)=>this.onSubmitButtonClick(e));
    }

    private onPlanClick(e:any):any {
        if ($(e.currentTarget).index() == 0) {
            $('#Plan_id_0').prop('checked', true);
            $('#start-membership-btn').addClass('active');
            $('#start-membership-btn2').removeClass('active');
            $('.plan57').show();
            $('.plan30').hide();
        } else {
            $('#Plan_id_1').prop('checked', true);
            $('#start-membership-btn2').addClass('active');
            $('#start-membership-btn').removeClass('active');
            $('.plan57').hide();
            $('.plan30').show();
        }

        this.$plan.removeClass('active');
        $(e.currentTarget).addClass('active');
    }


    private onSubmitButtonClick(e:any):any {
        if (!$(e.currentTarget).hasClass('active')) {
            e.preventDefault();
        }
    }

}
