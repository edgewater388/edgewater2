var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var timerPlanSignNew = (function (_super) {
    __extends(timerPlanSignNew, _super);
    function timerPlanSignNew() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    timerPlanSignNew.prototype.init = function () {
        this.timerPlan();
    };
    timerPlanSignNew.prototype.timerPlan = function () {
        function getTimestamp(resolve) {
            var dataTimestamp = $('.plan-info').attr('data-countdown-to'), timestamp;
            if (dataTimestamp) {
                return resolve(dataTimestamp);
            }
            $.get('/countdown/start', function (data) {
                if (data.timestamp) {
                    timestamp = data.timestamp;
                }
                resolve(timestamp);
            });
        }
        function millisAsDays(millis) {
            var seconds = Math.floor(millis / 1000), days = Math.floor(seconds / 24 / 60 / 60), hoursLeft = Math.floor(seconds - days * 86400), hours = Math.floor(hoursLeft / 3600), minutes = Math.floor((hoursLeft - hours * 3600) / 60), secondsLeft = seconds % 60;
            return { 'days': days, 'hours': hours, 'minutes': minutes, 'seconds': secondsLeft };
        }
        function resetTimeStamp(resolve) {
            var timestamp;
            $.get('/countdown/reset', function (data) {
                if (data.timestamp) {
                    timestamp = data.timestamp;
                }
                resolve(timestamp);
            });
        }
        function createStructureTimer(timerData) {
            $('.days').text(timerData.days);
            $('.hours').text(timerData.hours);
            $('.minutes').text(timerData.minutes < 10 ? '0' + timerData.minutes : timerData.minutes);
            $('.seconds').text(timerData.seconds < 10 ? '0' + timerData.seconds : timerData.seconds);
        }
        var interval;
        var test = 0;
        function initCountDown(timestamp) {
            var b = moment.unix(timestamp);
            return window.setInterval(function () {
                var a = moment.tz('America/New_York');
                var diff = b.diff(a);
                if (diff > 0) {
                    var timerData = millisAsDays(diff);
                    createStructureTimer(timerData);
                }
                else {
                    resetTimeStamp(function (timestamp) {
                        window.clearInterval(interval);
                        interval = initCountDown(timestamp);
                    });
                }
            }, 1000);
        }
        getTimestamp(function (timestamp) {
            interval = initCountDown(timestamp);
        });
    };
    return timerPlanSignNew;
}(View));
