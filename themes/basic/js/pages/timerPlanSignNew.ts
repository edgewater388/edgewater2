class timerPlanSignNew extends View {
  public init():void {
    this.timerPlan();
  }

  private timerPlan():void {
      // let timestamp = 1495873362;
      function getTimestamp(resolve) {
        let dataTimestamp = $('.plan-info').attr('data-countdown-to'),
            timestamp;
        if (dataTimestamp) {
            return resolve(dataTimestamp);
        }

        $.get( '/countdown/start' , function( data ) {
          if (data.timestamp) {
            timestamp = data.timestamp;
          }
          resolve(timestamp);
        });
      }
      function millisAsDays(millis) {
        let seconds = Math.floor(millis / 1000),
            days = Math.floor(seconds / 24 / 60 / 60),
            hoursLeft = Math.floor(seconds - days * 86400),
            hours = Math.floor(hoursLeft / 3600),
            minutes = Math.floor((hoursLeft - hours * 3600) / 60),
            secondsLeft = seconds % 60;

        return {'days': days, 'hours': hours, 'minutes': minutes, 'seconds': secondsLeft};
      }
      function resetTimeStamp(resolve) {
        let timestamp;
        $.get( '/countdown/reset' , function( data ) {
          if (data.timestamp) {
            // timestamp = data.timestamp;
            timestamp = data.timestamp;

          }
          resolve(timestamp);
        });
      }
      function createStructureTimer(timerData) {
        $('.days').text(timerData.days);
        $('.hours').text(timerData.hours);
        $('.minutes').text(timerData.minutes < 10 ? '0' + timerData.minutes : timerData.minutes);
        $('.seconds').text(timerData.seconds < 10 ? '0' + timerData.seconds : timerData.seconds);
      }

      let interval;
      let test = 0;
      function initCountDown(timestamp) {
        let b = moment.unix(timestamp);
        return window.setInterval(function () {
          let a = moment.tz('America/New_York');
          let diff = b.diff(a);

          if (diff > 0) {
            let timerData = millisAsDays(diff);
            createStructureTimer(timerData);
          } else {
            if ($('body').hasClass("sign-up")) {
              console.log('its body')
              resetTimeStamp((timestamp) => {
                interval = initCountDown(timestamp);
                window.clearInterval(interval);
              });
            }
          }
        }, 1000);
      }

      getTimestamp((timestamp) => {
        interval = initCountDown(timestamp);
      });
    }
}
