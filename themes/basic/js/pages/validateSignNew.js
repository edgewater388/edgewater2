var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ValidateSignNew = (function (_super) {
    __extends(ValidateSignNew, _super);
    function ValidateSignNew() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ValidateSignNew.prototype.init = function () {
        this.initLoginForm();
        this.initPaymentForm();
        this.initAccordeon();
        this.initTermsInfoForm();
    };
    ValidateSignNew.prototype.initLoginForm = function () {
        var submit = $(".login-form > button");
        var that = this;
        submit.click(submitLoginForm);
        function submitLoginForm() {
            var loginForm = $(".login-form > .input-holder > input"), data = loginForm.serializeJSON(), store_step = 1;
            var formData = {
                store_step: store_step,
                store_data: data
            };
            var accordion = $('.step-box > slide');
            $.post("/register-steps", formData).then(function (response) {
                var resp = JSON.parse(response);
                var step = resp.step;
                that.checkValidate(resp.errors, resp.fields, resp.success, store_step);
                if (step) {
                    that.redirectStep(step);
                    that.checkSpepsSelect(step);
                }
            }, function (response) {
                console.log('Status Error', response.status);
            });
        }
    };
    ValidateSignNew.prototype.initPaymentForm = function () {
        var submit = $(".payment-form > button");
        var that = this;
        submit.click(submitPaymentForm);
        function submitPaymentForm() {
            var paymentForm = $(".payment-form input, select"), data = paymentForm.serializeJSON(), store_step = 2;
            var formData = {
                store_step: store_step,
                store_data: data
            };
            var accordion = $('.step-box');
            if ($('.step-box:nth-child(2) .alert-danger').length) {
                $('.step-box:nth-child(2) .alert-danger').remove();
            }
            $.post("/register-steps", formData).then(function (response) {
                var resp = JSON.parse(response);
                var step = resp.step;
                that.checkValidate(resp.errors, resp.fields, resp.success, store_step);
                if (resp.success) {
                    that.redirectStep(step);
                    that.checkSpepsSelect(step);
                    accordion.filter(':nth-child(2)').find('.slide').addClass("step-passed");
                    accordion.filter(':nth-child(3)').find('.slide').removeClass("hide-form");
                }
                else {
                    that.checkValidate(resp.errors, resp.fields, resp.success, store_step);
                }
            }, function (response) {
                console.log('Status Error', response.status);
            });
        }
    };
    ValidateSignNew.prototype.initTermsInfoForm = function () {
        var that = this;
        var priceText = $('.price-info > .frame > del');
        var priceLimitedText = $('.price-info > .frame--strong > .number');
        var accordion = $(".step-box:nth-child(3) > .slide"), submit = $(".step-box:nth-child(3) .submit"), radioBtns = accordion.find("[type=radio]"), editBtn = $(".step-box:nth-child(3) > .head"), radio7 = accordion.find("[type=radio]:first"), radio8 = accordion.find("[type=radio]:last"), plan57 = accordion.find(".legal.plan57"), plan30 = accordion.find(".legal.plan30"), oldAmountOnePlan = $('#radio-8').attr('data-old-amount'), amountOnePlan = $('#radio-8').attr('data-amount'), oldAmountTwoPlan = $('#radio-7').attr('data-old-amount'), amountTwoPlan = $('#radio-7').attr('data-amount');
        $('.over_18').on('change', function () {
            if ($(this).is(":checked")) {
                if (radioBtns.is(":checked")) {
                    $(".step-box:nth-child(3)").find('.errors > span').remove();
                    $(".step-box:nth-child(3)").removeClass('incorect');
                    $(".step-box:nth-child(3)").find('.danger').removeClass('danger');
                }
            }
        });
        priceText.text("$ " + oldAmountOnePlan + " /month");
        priceLimitedText.text("$ " + amountOnePlan);
        radioBtns.on('change', function () {
            if ($(this).is(":checked")) {
                if ($('.over_18').is(":checked")) {
                    $(".step-box:nth-child(3)").find('.errors > span').remove();
                    $(".step-box:nth-child(3)").removeClass('incorect');
                    $(".step-box:nth-child(3)").find('.danger').removeClass('danger');
                }
                if ($('.step-box:nth-child(3)').find('.slide').is(":visible")) {
                    $(".step-box:nth-child(3)").addClass("edit");
                }
                if (radio7.is(":checked")) {
                    priceText.text('');
                    priceText.text("$ " + oldAmountOnePlan + " /month");
                    priceLimitedText.text('');
                    priceLimitedText.text("$ " + amountOnePlan);
                    plan30.css('display', 'block');
                    plan57.css('display', 'none');
                }
                else if (radio8.is(":checked")) {
                    priceText.text('');
                    priceText.text("$ " + oldAmountTwoPlan + " /month");
                    priceLimitedText.text('');
                    priceLimitedText.text("$ " + amountTwoPlan);
                    plan57.css('display', 'block');
                    plan30.css('display', 'none');
                }
            }
        });
        submit.click(submitTermsInfoForm);
        function submitTermsInfoForm() {
            var form = $('#signup-form');
            var error = $('.step-box:nth-child(3) .errors');
            var termsInfoForm = $("#step3").find(':input'), data = termsInfoForm.serializeJSON(), store_step = 3;
            var formData = {
                store_step: store_step,
                store_data: data
            };
            if ($('.step-box:nth-child(3) .alert-danger').length) {
                $('.step-box:nth-child(3) .alert-danger').remove();
            }
            $.post("/register-steps", formData).then(function (response) {
                var resp = JSON.parse(response);
                if (resp.success) {
                    error.html('');
                    form.submit();
                    that.checkValidate(resp.errors, resp.fields, resp.success, store_step);
                }
                else {
                    error.html('');
                    that.checkValidate(resp.errors, resp.fields, resp.success, store_step);
                    error.append($('<span>' + resp.errors + '</span>'));
                    if ($('.step-box:nth-child(3) .errors > span').length > 1) {
                        $('.step-box:nth-child(3)').find('.errors > span:nth-child(2)').remove();
                        ;
                    }
                }
            }, function (response) {
                console.log(error);
                console.log('Status Error', response);
            });
            return false;
        }
        ;
    };
    ValidateSignNew.prototype.checkValidate = function (errors, fields, success, step) {
        var stepForm = $('.step-box:nth-child(' + step + ')'), errorString = $('.step-box:nth-child(' + step + ') .text-complete');
        if (stepForm.find('.errors > span').length) {
            stepForm.find('.errors > span').remove();
        }
        if (success == true) {
            stepForm.removeClass('incorect');
            stepForm.find('.errors').append($('<span></span>'));
            stepForm.find('.danger').removeClass('danger');
            errorString.removeClass("hidden");
        }
        else if (success !== true) {
            stepForm.addClass('incorect');
            errorString.addClass("hidden");
            stepForm.find('.errors').append($('<span>' + errors + '</span>'));
            if (fields.indexOf(',') > -1) {
                var str = fields.split(',').map(function (item) {
                    return '.' + item;
                }).join(',');
                stepForm.find(str).addClass('danger');
            }
            else {
                stepForm.find('.' + fields).addClass('danger');
            }
        }
    };
    ValidateSignNew.prototype.initAccordeon = function () {
        var that = this, steps = $('.step-box'), headerHeight = $('#header').height();
        $('.step-box .link').click(function (e) {
            e.preventDefault();
            if (!$('.step-box.incorect').length) {
                $('.step-box > .slide').addClass("hide-form");
                $(this).closest('.step-box').find('.slide').removeClass("hide-form");
                if ($('.step-box.edit').length) {
                    $('.step-box').removeClass("edit");
                }
                $(this).closest('.step-box').addClass("edit");
            }
            $("html, body").animate({
                scrollTop: $(this).closest('.step-box').offset().top - headerHeight - 10
            }, 500);
        });
    };
    ValidateSignNew.prototype.redirectToHome = function () {
        var submit = $(".step-box:nth-child(3) button[type='submit']");
        submit.click(redirect);
        function redirect() {
            window.location.replace("/");
        }
    };
    ValidateSignNew.prototype.redirectStep = function (step) {
        var step = step, headerHeight = $('#header').height();
        $('.step-box > .slide').addClass("hide-form");
        $('.step-box > .slide').eq(step - 1).removeClass("hide-form");
        $("html, body").animate({
            scrollTop: $('.step-box').eq(step - 1).offset().top - headerHeight - 10
        }, 500);
    };
    ValidateSignNew.prototype.checkLocalStorage = function (val) {
        if (localStorage.getItem(val) !== null) {
            return true;
        }
        else {
            return false;
        }
    };
    ValidateSignNew.prototype.checkSpepsSelect = function (steps) {
        var accordion = $('.step-box');
        if ($('.step-box.edit').length) {
            $('.step-box').removeClass("edit");
        }
        for (var i = 0; i < steps; i++) {
            accordion.filter(':nth-child(' + i + ')').addClass("step-passed");
        }
    };
    return ValidateSignNew;
}(View));
