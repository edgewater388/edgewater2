class ValidateSignNew extends View {
    public init():void {
      this.initLoginForm();
      this.initPaymentForm();
      this.initAccordeon();
      this.initTermsInfoForm();
    }

    private initLoginForm():void {
      var submit = $(".login-form > button");
      var that = this;
      submit.click(submitLoginForm);
      function submitLoginForm() {
        var loginForm = $(".login-form > .input-holder > input"),
            data = loginForm.serializeJSON(),
            store_step = 1;
        var formData = {
          store_step: store_step,
          store_data: data
        }
        var trial = $('.signup-section').data('trial');
        var accordion = $('.step-box > slide');
        // that.preventForm(true);
        $.post( `/register-steps?trial=${trial}`, formData).then(
          function(response) {
            var resp = JSON.parse(response);
            var step = resp.step

            that.checkValidate(resp.errors, resp.fields, resp.success, store_step);
            if (step) {
              that.redirectStep(step);
              that.checkSpepsSelect(step);
            }
          }, function(response) {
            if(window.console !== undefined) {
              console.error('Status Error',response.status);
            }
          }
        );
      }
    }
    private initPaymentForm():void {
      var submit = $(".payment-form > button");
      var that = this;

      submit.click(submitPaymentForm);
      function submitPaymentForm() {
        var paymentForm = $(".payment-form input, select" ),
            data = paymentForm.serializeJSON(),
            store_step = 2;
        var formData = {
          store_step: store_step,
          store_data: data
        }
        var accordion = $('.step-box');
        if ($('.step-box:nth-child(2) .alert-danger').length) {
          $('.step-box:nth-child(2) .alert-danger').remove();
        }
        var trial = $('.signup-section').data('trial');
        $.post( `/register-steps?trial=${trial}`, formData).then(
          function(response) {
            var resp = JSON.parse(response);
            var step = resp.step
            that.checkValidate(resp.errors, resp.fields, resp.success, store_step);
            if (resp.success) {
              that.redirectStep(step);
              that.checkSpepsSelect(step);
              accordion.filter(':nth-child(2)').find('.slide').addClass("step-passed");
              accordion.filter(':nth-child(3)').find('.slide').removeClass("hide-form");
            } else {
              that.checkValidate(resp.errors, resp.fields, resp.success, store_step);
            }
          }, function(response) {
            if(window.console !== undefined) {
              console.error('Status Error',response.status);
            }
          }
        );
      }
    }

    private initTermsInfoForm():void {
      var that = this;
      var priceText = $('.price-info .price del .number');
      var priceLimitedText = $('.price-info .frame--strong .price .number');
      var accordion = $(".step-box:nth-child(3) > .slide"),
          submit = $(".step-box:nth-child(3) .submit"),
          radioBtns = accordion.find("[type=radio]");

      $('.over_18').on('change', function() {
        if ($(this).is(":checked")) {
          if (radioBtns.is(":checked")) {
            $(".step-box:nth-child(3)").find('.errors > span').remove();
            $(".step-box:nth-child(3)").removeClass('incorect');
            $(".step-box:nth-child(3)").find('.danger').removeClass('danger');
          }

        }
      });

      radioBtns.on('change', function() {
        var $this = $(this);
        if ($this.is(":checked")) {

          if ($('.over_18').is(":checked")) {
            $(".step-box:nth-child(3)").find('.errors > span').remove();
            $(".step-box:nth-child(3)").removeClass('incorect');
            $(".step-box:nth-child(3)").find('.danger').removeClass('danger');
          }

          if ($('.step-box:nth-child(3)').find('.slide').is(":visible")) {
            $(".step-box:nth-child(3)").addClass("edit");
          }

          var oldAmount = $this.data('old-amount');
          var amount = $this.data('amount');

          priceText.text('');
          priceText.text(oldAmount.toString());

          priceLimitedText.text('');
          priceLimitedText.text(amount.toString());

          accordion.find(".legal").css('display','none');
          accordion.find(`.legal.plan${oldAmount}`).css('display','block');
        }
      });

      submit.click(submitTermsInfoForm);

      function submitTermsInfoForm() {
        var form = $('#signup-form');
        var error = $('.step-box:nth-child(3) .errors');
        var termsInfoForm = $("#step3" ).find(':input'),
            data = termsInfoForm.serializeJSON(),
            store_step = 3;
        var formData = {
          store_step: store_step,
          store_data: data
        }

        if ($('.step-box:nth-child(3) .alert-danger').length) {
          $('.step-box:nth-child(3) .alert-danger').remove();
        }
        var trial = $('.signup-section').data('trial');
        $.post( `/register-steps?trial=${trial}`, formData).then(
          function(response) {
            var resp = JSON.parse(response);
            if (resp.success) {
              error.html('');
              form.submit();
              that.checkValidate(resp.errors, resp.fields, resp.success, store_step);
            } else {
              error.html('');
              that.checkValidate(resp.errors, resp.fields, resp.success, store_step);
              error.append($('<span>' + resp.errors + '</span>'));
              if ($('.step-box:nth-child(3) .errors > span').length > 1) {
                $('.step-box:nth-child(3)').find('.errors > span:nth-child(2)').remove();;
              }
            }
          }, function(response) {
            if(window.console !== undefined) {
              console.error(error);
            }
          }
        );
        return false;
      };
    }

    private checkValidate(errors, fields, success, step):void {
      var stepForm = $('.step-box:nth-child(' + step + ')'),
          errorString = $('.step-box:nth-child(' + step + ') .text-complete');

      if (stepForm.find('.errors > span').length) {
        stepForm.find('.errors > span').remove();
      }
      if (success == true) {
        stepForm.removeClass('incorect');
        stepForm.find('.errors').append($('<span></span>'));
        stepForm.find('.danger').removeClass('danger');
        errorString.removeClass("hidden");
      } else if (success !== true) {
        stepForm.addClass('incorect');
        errorString.addClass("hidden");
        stepForm.find('.errors').append($('<span>' + errors + '</span>'));

        if (fields.indexOf(',') > -1) {
          var str = fields.split(',').map(function(item) {
        		return '.' + item;
        	}).join(',');
          stepForm.find(str).addClass('danger');
        } else {
          stepForm.find('.' + fields).addClass('danger');
        }
      }

    }

    private initAccordeon():void {
      var that = this,
          steps = $('.step-box'),
          headerHeight = $('#header').height();

      $('.step-box .link').click(function(e) {
        e.preventDefault();

        if (!$('.step-box.incorect').length) {

          $('.step-box > .slide').addClass("hide-form");
          $(this).closest('.step-box').find('.slide').removeClass("hide-form");
          if ($('.step-box.edit').length) {
            $('.step-box').removeClass("edit");
          }
          $(this).closest('.step-box').addClass("edit");
        }
        $("html, body").animate({
          scrollTop: $(this).closest('.step-box').offset().top - headerHeight - 10
        }, 500);
      });
    }

    private redirectToHome():void {
      var submit = $(".step-box:nth-child(3) button[type='submit']");
      submit.click(redirect);
      function redirect() {
        window.location.replace("/");
      }
    }

    private redirectStep(step):void {
      // var step = JSON.parse(localStorage.getItem('stepSignNew')),
      var step = step,
          headerHeight = $('#header').height();

      if (!$('.step-box > .slide').eq(step -1).hasClass('hide-form')) {
        return;
      } else {
        $('.step-box > .slide').addClass("hide-form");
        $('.step-box > .slide').eq(step -1).removeClass("hide-form");
        $("html, body").animate({
          scrollTop: $('.step-box').eq(step -1).offset().top - headerHeight - 40
        }, 500);
      }
    }

    private checkLocalStorage(val):boolean {
      if (localStorage.getItem(val) !== null) {
        return true;
      } else {
        return false;
      }
    }

    private checkSpepsSelect(steps):void {
      var accordion = $('.step-box');
      if ($('.step-box.edit').length) {
        $('.step-box').removeClass("edit");
      }
      for (var i = 0; i < steps; i++) {
        accordion.filter(':nth-child(' + i +')').addClass("step-passed");
      }
    }

}
