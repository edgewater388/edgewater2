/// <reference path="../typings/tsd.d.ts" />
/// <reference path="../app/mvc.ts" />
/// <reference path="../app/environment.ts" />
/// <reference path="../app/breakpoints.ts" />
/// <reference path="../app/helpers/guide.ts" />
/// <reference path="../app/helpers/tick.ts" />
/// <reference path="../app/helpers/template.ts" />
/// <reference path="../app/helpers/sizing-helper.ts" />
/// <reference path="../app/helpers/form-validator.ts" />
/// <reference path="../app/helpers/form-helper.ts" />
/// <reference path="../app/helpers/responsive-images.ts" />
/// <reference path="../app/helpers/parallax.ts" />
/// <reference path="../app/helpers/button-helpers.ts" />
/// <reference path="../app/helpers/browser.ts" />
/// <reference path="../app/helpers/form-submit.ts" />
/// <reference path="../app/helpers/array-util.ts" />
/// <reference path="../app/helpers/video-helper.ts" />
/// <reference path="../app/helpers/scroll-to.ts" />
/// <reference path="../app/helpers/helpers.ts" />
/// <reference path="../app/helpers/lightbox.ts" />
/// <reference path="../app/helpers/url-helper.ts" />
/// <reference path="../app/helpers/datepicker.ts" />
/// <reference path="../app/helpers/data-links.ts" />
/// <reference path="../global/header.ts" />
/// <reference path="../global/modals.ts" />
/// <reference path="../global/modals-model.ts" />
/// <reference path="../global/alerts.ts" />
/// <reference path="../global/nav-menu.ts" />
/// <reference path="../global/deeplink-controller.ts" />
/// <reference path="../global/video-thumb.ts" />
/// <reference path="../global/video-status.ts" />
/// <reference path="../global/video-filter.ts" />
/// <reference path="../global/video-pagination.ts" />
/// <reference path="../global/video-pagination-model.ts" />
/// <reference path="../modals/sign-in-modal.ts" />
/// <reference path="../modals/personal-info-modal.ts" />
/// <reference path="../modals/tutorial-modal.ts" />
/// <reference path="../modals/video-modal.ts" />
/// <reference path="../modals/mashup-modal.ts" />
/// <reference path="../modals/video-embed.ts" />
/// <reference path="../modals/mashup-name-modal.ts" />
/// <reference path="../modals/delete-mashup-confirmation.ts" />
/// <reference path="../pages/account/email-preferences.ts" />
/// <reference path="../pages/faq/faq.ts" />
/// <reference path="../pages/account/manage-profile.ts" />
/// <reference path="../pages/account/change-plan.ts" />
/// <reference path="../pages/account/account.ts" />
/// <reference path="../pages/home-logged-in.ts" />
/// <reference path="../pages/my-workouts.ts" />
/// <reference path="../pages/mashup-maker/mashup-maker.ts" />
/// <reference path="../pages/mashup-maker/mashup-maker-model.ts" />
/// <reference path="../modals/equipment.ts" />
/// <reference path="../pages/payment.ts" />
/// <reference path="../pages/validateSignNew.ts" />
/// <reference path="../pages/fixedBlockSignNew.ts" />
/// <reference path="../pages/home-workouts.ts" />
/// <reference path="../pages/timerPlanSignNew.ts" />
// / <reference path="../pages/testBulid.ts" />
//
//

interface JQueryStatic {
    throttle(time:number, callback:any):any;
    debounce(time:number, callback:any):any;
}

declare class PxLoader {
    addImage(opt?:any);
    addCompletionListener(opts?:any);
    start(opts?:any);
}

declare var cssua:any;
declare var enquire:any;
declare var Sortable:any;
declare var jwplayer:any;
declare var _kmq:any;
declare var woopra:any;
declare var ga:any;
declare var moment:any;


interface JQuery {
    itemslide(options?:any);
    reload();
    placeholder():any;
    getActiveIndex();
    gotoSlide(slideIndex:number);
    gotoWithoutAnimation(slideIndex:number);
    animationEnd();
    slick(options1?:any, options2?:any, options3?:any, options4?:any);
    swipe(obj?:any);
    nested(options?:any);
    stalactite(options?:any);
    wookmark(options?:any);
    shuffle(el?:any);
    transition(options?:any, time?:number, easing?:any, callback?:any);
    usmap(options?:any);
    mapSvg(options?:any);
    mCustomScrollbar(opt1?:any, opt2?:any, opt3?:any);
    YTPlayer(options?:any);
    //setup(obj:any);
}
