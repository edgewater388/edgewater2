declare class Application {
    private static _instance;
    private models;
    private views;
    private controllers;
    constructor();
    static getInstance(): Application;
    createModel(model: any): any;
    createView(view: any): any;
    createController(controller: any): any;
    getModel(model: any): any;
    getView(view: any): any;
    getController(controller: any): any;
}
declare class BaseClass {
    app: Application;
    constructor();
}
declare class Model implements Subscribable {
    app: Application;
    subscribers: Updatable[];
    instantiate(): void;
    subscribe(subscriber: Updatable): void;
    notify(data?: any): void;
    private sortSubscribersByPriority(a, b);
    unsubscribe(subscriber: Updatable): void;
}
interface Initable {
    init(): void;
}
interface Subscribable {
    subscribe(subscriber: Updatable, Initable: any): void;
    unsubscribe(subscriber: Updatable): void;
    notify(data?: any): void;
}
interface Updatable {
    priority?: number;
    update(data?: any): void;
    id?: string;
}
declare class Controller {
    app: Application;
    instantiate(): void;
}
declare class Handler implements Updatable {
    callback: any;
    priority: number;
    update(data: any): void;
    constructor(callback: any, priority?: number);
}
declare class View {
    app: Application;
    instantiate(): void;
}
