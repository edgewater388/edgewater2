var Application = (function () {
    function Application() {
        this.models = [];
        this.views = [];
        this.controllers = [];
        if (Application._instance) {
            throw new Error("Error: Instantiation failed: Use application.getInstance() instead of new.");
        }
        Application._instance = this;
    }
    Application.getInstance = function () {
        if (Application._instance === null) {
            Application._instance = new Application();
        }
        return Application._instance;
    };
    Application.prototype.createModel = function (model) {
        if (this.getModel(model))
            throw new Error("Attempted to add duplicate model:" + model);
        var modelInstance = new model();
        this.models.push(modelInstance);
        modelInstance.instantiate();
        return modelInstance;
    };
    Application.prototype.createView = function (view) {
        if (this.getView(view))
            throw new Error("Attempted to add duplicate view:" + view);
        var viewInstance = new view();
        this.views.push(viewInstance);
        viewInstance.instantiate();
        return viewInstance;
    };
    Application.prototype.createController = function (controller) {
        if (this.getController(controller))
            throw new Error("Attempted to add duplicate controller:" + controller);
        var controllerInstance = new controller();
        this.controllers.push(controllerInstance);
        controllerInstance.instantiate();
        return controllerInstance;
    };
    Application.prototype.getModel = function (model) {
        for (var i = 0; i < this.models.length; i++) {
            if (this.models[i] instanceof model)
                return this.models[i];
        }
        return null;
    };
    Application.prototype.getView = function (view) {
        for (var i = 0; i < this.views.length; i++) {
            if (this.views[i] instanceof view)
                return this.views[i];
        }
        return null;
    };
    Application.prototype.getController = function (controller) {
        for (var i = 0; i < this.controllers.length; i++) {
            if (this.controllers[i] instanceof controller)
                return this.controllers[i];
        }
        return null;
    };
    Application._instance = null;
    return Application;
})();
var BaseClass = (function () {
    function BaseClass() {
        this.app = Application.getInstance();
        this['init']();
    }
    return BaseClass;
})();
var Model = (function () {
    function Model() {
        this.app = Application.getInstance();
        this.subscribers = [];
    }
    Model.prototype.instantiate = function () {
        if (this['init'])
            this['init']();
    };
    Model.prototype.subscribe = function (subscriber) {
        this.subscribers.push(subscriber);
    };
    Model.prototype.notify = function (data) {
        this.subscribers.sort(this.sortSubscribersByPriority);
        for (var i = 0; i < this.subscribers.length; i++) {
            this.subscribers[i].update(data);
        }
    };
    Model.prototype.sortSubscribersByPriority = function (a, b) {
        return (a.priority - b.priority);
    };
    Model.prototype.unsubscribe = function (subscriber) {
        for (var i = 0; i < this.subscribers.length; i++) {
            if (this.subscribers[i] === subscriber) {
                this.subscribers.splice(i, 1);
                return;
            }
        }
    };
    return Model;
})();
var Controller = (function () {
    function Controller() {
        this.app = Application.getInstance();
    }
    Controller.prototype.instantiate = function () {
        if (this['init'])
            this['init']();
    };
    return Controller;
})();
var Handler = (function () {
    function Handler(callback, priority) {
        if (priority === void 0) { priority = 10000; }
        this.callback = callback;
        this.priority = priority;
    }
    Handler.prototype.update = function (data) {
        this.callback(data);
    };
    return Handler;
})();
var View = (function () {
    function View() {
        this.app = Application.getInstance();
    }
    View.prototype.instantiate = function () {
        if (this['init'])
            this['init']();
    };
    return View;
})();
